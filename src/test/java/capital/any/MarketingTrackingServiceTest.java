package capital.any;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import capital.any.model.base.MarketingTracking;
import capital.any.service.base.marketingTracking.IMarketingTrackingService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class MarketingTrackingServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(MarketingTrackingServiceTest.class);
	
	@Autowired
	private IMarketingTrackingService marketingTrackingService;
	
	@Test
	@Transactional
	public void insert() {
		logger.debug("About to test the service insert in marketingTrackingService.");
		MarketingTracking marketingTracking = new MarketingTracking();
		marketingTracking.setCampaignId(1);
		marketingTracking.setQueryString("queryString");
		marketingTrackingService.insert(marketingTracking);
	}
}
