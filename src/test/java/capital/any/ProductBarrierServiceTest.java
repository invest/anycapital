package capital.any;

import java.util.Map;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import capital.any.model.base.ProductBarrier;
import capital.any.service.base.productBarrier.IProductBarrierService;

/**
 * @author Eyal G
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
   
public class ProductBarrierServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(ProductBarrierServiceTest.class);

	@Autowired
	private IProductBarrierService productBarrierService;
	
	@Test
    public void getProductBarrier() {		 
		Map<Long, ProductBarrier> hm = productBarrierService.get();
		logger.debug(hm.toString());
		Assert.assertNotNull(hm);	
	}		
}
