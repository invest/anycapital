package capital.any;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import capital.any.model.base.MarketingAttribution;
import capital.any.model.base.User;
import capital.any.service.base.marketingAttribution.IMarketingAttributionService;
import capital.any.service.base.user.IUserService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)
public class MarketingAttributionServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(MarketingAttributionServiceTest.class);
	
	@Autowired
	private IMarketingAttributionService marketingAttributionService;
	@Autowired
	private IUserService userService;
	
	private User user;
	
	@Value("${user.exist}")
	private String userExist;
	
	@Before
	public void init() {
		user = userService.getUserByEmail(userExist);	
	}
	
	@Test
	@Transactional
	public void insert() {
		logger.debug("About to test insert into marketing attribution.");
		MarketingAttribution marketingAttribution = new MarketingAttribution();
		marketingAttribution.setUserId(user.getId());
		marketingAttribution.setTableId(1);
		marketingAttribution.setReferenceId(111111);
		marketingAttribution.setTrackingId(1);
		marketingAttributionService.insert(marketingAttribution);
	}
}
