package capital.any;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import capital.any.model.base.Issue;
import capital.any.model.base.IssueChannel;
import capital.any.model.base.IssueDirection;
import capital.any.model.base.IssueReachedStatus;
import capital.any.model.base.IssueReaction;
import capital.any.model.base.IssueSubject;
import capital.any.service.base.issue.IIssueService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)
public class IssueServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(IssueServiceTest.class);
	
	@Autowired
	private IIssueService issueService;
	
	@Test
	@Transactional
	public void insert() {
		logger.debug("About to start insert issue.");
		Issue issue = new Issue();
		issue.setIssueChannel(new IssueChannel(1));
		issue.setIssueSubject(new IssueSubject(1));
		issue.setIssueDirection(new IssueDirection(1));
		issue.setIssueReachedStatus(new IssueReachedStatus(1));
		issue.setIssueReaction(new IssueReaction(1));
		issue.setComments("test");
		issue.setSignificantNote(true);
		issue.setUserId(10103);
		issue.setWriterId(6);
		issueService.insert(issue);
	}
	
	@Test
	public void getIssuesByUserId() {
		Assert.assertNotNull(issueService.getIssuesByUserId(10103));
	}
}
