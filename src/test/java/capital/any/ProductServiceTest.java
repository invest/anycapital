package capital.any;

import java.util.List;
import java.util.Map;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import capital.any.base.enums.ActionSource;
import capital.any.base.enums.ProductStatusEnum;
import capital.any.model.base.Product;
import capital.any.model.base.ProductHistory;
import capital.any.service.base.product.IProductService;

/**
 * @author eranl
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
   
public class ProductServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(ProductServiceTest.class);

	@Autowired
	private IProductService productService;
	
	private long testProductId;
	
	@Before
	public void setup() {
		Map<Long, Product> hm = productService.getProducts();
		try {
			for (Product product : hm.values()) {
				testProductId = product.getId();
				break;
			}
		} catch (Exception e) {
			logger.debug("cant find product id for test", e);
		}
	}
	
	@Test
    public void getProducts() {		 
		Assert.assertNotNull(productService.getProducts());		
	}
	
	@Test
    public void getProduct() {
		logger.info("get product");
		try {
			Product p = productService.getProduct(testProductId);
			logger.info(p.toString());
			Assert.assertNotNull(p);
		} catch (Exception e) {
			logger.info("cant load product ", e);
			throw e;
		}		
	}
	
	@Test
    public void getHomePageProducts() {
		logger.info("get Home Page Products");
		try {
			List<Product> p = productService.getHomePageProducts();
			logger.info(p.toString());
			Assert.assertNotNull(p);
		} catch (Exception e) {
			logger.info("cant load home page products ", e);
			throw e;
		}		
	}
	
	@Test
	@Transactional
    public void updateStatus() throws Exception {	 
		productService.updateStatus(testProductId, ProductStatusEnum.SUBSCRIPTION, new ProductHistory(ActionSource.JOB));	
	}	
	
}
