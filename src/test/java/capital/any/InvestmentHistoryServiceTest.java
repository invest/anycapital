package capital.any;

import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import capital.any.base.enums.ActionSource;
import capital.any.base.enums.InvestmentStatusEnum;
import capital.any.model.base.InvestmentHistory;
import capital.any.model.base.InvestmentStatus;
import capital.any.service.base.investmentHistory.IInvestmentHistoryService;

/**
 * @author Eyal G
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
public class InvestmentHistoryServiceTest {
	
	private static final Logger logger = LoggerFactory.getLogger(InvestmentHistoryServiceTest.class);

	@Autowired
	private IInvestmentHistoryService investmentHistoryService;
	
	@Test
    public void getInvestmentsHistory() {
		List<InvestmentHistory> investments = investmentHistoryService.get();
		logger.debug("investments " + investments);
		Assert.assertTrue(investments != null);
	}
	
	@Test
	@Transactional
	@Ignore
    public void insertInvestmentHistory() {
		InvestmentHistory investmentHistory = new InvestmentHistory();
		investmentHistory.setActionSource(ActionSource.WEB);
		investmentHistory.setInvestmentStatus(new InvestmentStatus(InvestmentStatusEnum.OPEN.getId()));
		investmentHistory.setInvestmntId(8);
		investmentHistory.setServerName("123");
		investmentHistory.setTimeCreated(new Date());
		investmentHistory.setWriterId(1);
		investmentHistoryService.insert(investmentHistory);
		logger.debug("investment History " + investmentHistory);
	}
	
}
