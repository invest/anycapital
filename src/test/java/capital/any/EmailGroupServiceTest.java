package capital.any;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import capital.any.base.enums.EmailGroup;
import capital.any.service.base.emailGroup.IEmailGroupService;

/**
 * @author Eyal Goren
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
   
public class EmailGroupServiceTest {
	
	private static final Logger logger = LoggerFactory.getLogger(EmailGroupServiceTest.class);

	@Autowired
	private IEmailGroupService emailGroupService;
	
	@Test
    public void get() {
		String email = emailGroupService.get(EmailGroup.DAILY_REPORT.getId());
		Assert.assertNotNull(email);		
	}
		
}
