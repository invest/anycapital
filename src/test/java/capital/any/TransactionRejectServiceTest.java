package capital.any;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import capital.any.base.enums.ActionSource;
import capital.any.base.enums.TransactionRejectType;
import capital.any.model.base.Transaction.TransactionOperationEnum;
import capital.any.model.base.Transaction.TransactionPaymentTypeEnum;
import capital.any.model.base.TransactionReject;
import capital.any.model.base.User;
import capital.any.service.base.transactionReject.ITransactionRejectService;
import capital.any.service.base.user.IUserService;

/**
 * @author LioR SoLoMoN
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class TransactionRejectServiceTest {

	private static final Logger logger = LoggerFactory.getLogger(TransactionRejectServiceTest.class);
	@Autowired
	private ITransactionRejectService transactionRejectService;
	@Autowired
	private IUserService userService;
	
	private User user;
	
	@Value("${user.exist}")
	private String userExist;
	
	@Before
	public void init() {
		user = userService.getUserByEmail(userExist);	
	}
	
	@Test
	@Transactional
	public void insert() {
		TransactionReject transactionReject = 
				new TransactionReject(user.getId(),
						TransactionPaymentTypeEnum.WIRE, 
						TransactionRejectType.BALANCE,
						TransactionOperationEnum.DEBIT,
						200,
						"session",
						ActionSource.WEB,
						-1,
						"info",
						"server-name");

		transactionRejectService.insert(transactionReject);
		logger.debug(transactionReject.toString());

	}
}
