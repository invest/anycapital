package capital.any;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import capital.any.base.enums.ActionSource;
import capital.any.model.base.File;
import capital.any.model.base.FileStatus;
import capital.any.model.base.FileType;
import capital.any.model.base.User;
import capital.any.model.base.Writer;
import capital.any.service.base.file.IFileService;
import capital.any.service.base.user.IUserService;

/**
 * @author eran.levy
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class FileServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(FileServiceTest.class);
	
	@Autowired
	private IFileService fileService;
	@Autowired
	private IUserService userService;
	
	@Value("${user.exist}")
	private String userExist;
	
	private User user;
	
	@Before
	public void init() {
		user = userService.getUserByEmail(userExist);	
	}
	
	@Test
	@Transactional
	public void insert() {
		logger.debug("Going to insert user file");
		File file = new File();
		FileType fileType = new FileType();
		fileType.setId(1);		
		file.setFileType(fileType);
		file.setUserId(user.getId());
		file.setActionSource(ActionSource.CRM);
		file.setWriter(new Writer(3));
		file.setName("aaaaaaa.png");
		fileService.insert(file);
	}
	
	@Test
	public void getAllByUserId() {
		logger.debug("Going to get all user files");
		fileService.getByUserId(10124);
	}
	
	@Test
	@Transactional
	public void update() throws Exception {
		logger.debug("Going to update file");
		List<File> files = fileService.getByUserId(user.getId());
		
		File file = new File();
		FileType fileType = new FileType();
		fileType.setId(1);
		FileStatus fileStatus = new FileStatus();
		fileStatus.setId(3);
		file.setFileType(fileType);
		file.setId(files.get(0).getId());
		file.setPrimary(true);
		file.setUserId(user.getId());
		file.setFileStatus(fileStatus);
		Assert.isTrue(fileService.updateFile(file));
	}
}
