package capital.any;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


import capital.any.model.base.ProductMarket;
import capital.any.service.base.productMarket.IProductMarketService;

/**
 * @author eranl
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
   
public class ProductMarketServiceTest {

	@Autowired
	private IProductMarketService productMarketService;
	
	@Test
    public void getProductMarkets() {
		Assert.assertNotNull(productMarketService.get());		
	}
	
	@Test
    public void getProductMarketsByProductId() {
		List<ProductMarket> productMarkets = productMarketService.getProductMarkets(12);
		System.out.println(productMarkets);
		Assert.assertNotNull(productMarkets);		
	}
		
}
