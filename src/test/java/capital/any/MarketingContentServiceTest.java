package capital.any;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import capital.any.model.base.MarketingContent;
import capital.any.service.base.marketingContent.IMarketingContentService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class MarketingContentServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(MarketingContentServiceTest.class);
	
	@Autowired
	private IMarketingContentService marketingContentService;
	
	@Test
	@Transactional
	public void insert() throws Exception {
		logger.debug("About to test the service insert in marketingContentService.");
		MarketingContent marketingContent = new MarketingContent();
		marketingContent.setName("Content test");
		marketingContent.setComments("comments");
		marketingContent.setPageContent("pageContent");
		marketingContent.setWriterId(6);
		marketingContent.setHTMLFilePath("1.html");
		marketingContentService.insert(marketingContent);
	}
	
	@Test
	@Transactional
	public void update() throws Exception {
		logger.debug("About to test the service update in marketingContentService.");
		MarketingContent marketingContent = new MarketingContent();
		marketingContent.setId(1);
		marketingContent.setName("Content test updated");
		marketingContent.setComments("comments");
		marketingContent.setPageContent("pageContent");
		marketingContent.setWriterId(6);
		marketingContentService.update(marketingContent);
	}
	
	@Test
	public void getAll() {
		Assert.assertNotNull(marketingContentService.getAll());
	}
	
	@Test
	public void getMarketingContent() {
		Assert.assertNotNull(marketingContentService.getMarketingContent(1));
	}
}
