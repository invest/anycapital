package capital.any;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import capital.any.model.base.QmQuestion;
import capital.any.model.base.QmUserAnswer;
import capital.any.service.base.qmUserAnswer.IQmUserAnswerService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class QmUserAnswerServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(QmUserAnswerServiceTest.class);
	
	@Autowired
	private IQmUserAnswerService qmUserAnswerService;
	
	@Test
	@Transactional
	public void insert() {
		logger.debug("About to test the service insert in QmUserAnswerService.");
		QmUserAnswer qmUserAnswer = new QmUserAnswer();
		qmUserAnswer.setUserId(10138);
		qmUserAnswer.setQuestionId(3);
		qmUserAnswer.setAnswerId(4);
		qmUserAnswer.setTextAnswer("test");
		qmUserAnswer.setActionSourceId(1);
		
		QmUserAnswer qmUserAnswer2 = new QmUserAnswer();
		qmUserAnswer2.setUserId(10138);
		qmUserAnswer2.setQuestionId(4);
		qmUserAnswer2.setAnswerId(8);
		qmUserAnswer2.setTextAnswer("test");
		qmUserAnswer2.setActionSourceId(1);
		
		List<QmUserAnswer> list = new ArrayList<QmUserAnswer>();
		list.add(qmUserAnswer);
		list.add(qmUserAnswer2);
		
		qmUserAnswerService.insertBatch(list);
	}
	
	@Test
	public void get() {
		logger.debug("About to test get user answers.");
		Map<Integer, QmQuestion> map = qmUserAnswerService.get(10168);
		logger.debug("map: " + map.toString());
		Assert.assertNotNull(map);
	}
	
	@Test
	public void getSumScore() {
		logger.debug("Test getSumScore.");
		QmUserAnswer qmUserAnswer = new QmUserAnswer();
		qmUserAnswer.setUserId(10138);
		qmUserAnswer.setQuestionId(3);
		qmUserAnswer.setAnswerId(47);
		qmUserAnswer.setTextAnswer("test");
		qmUserAnswer.setActionSourceId(1);
		
		QmUserAnswer qmUserAnswer2 = new QmUserAnswer();
		qmUserAnswer2.setUserId(10138);
		qmUserAnswer2.setQuestionId(4);
		qmUserAnswer2.setAnswerId(1);
		qmUserAnswer2.setTextAnswer("test");
		qmUserAnswer2.setActionSourceId(1);
		
		List<QmUserAnswer> list = new ArrayList<QmUserAnswer>();
		list.add(qmUserAnswer);
		list.add(qmUserAnswer2);
		
		long score = qmUserAnswerService.getSumScore(list);
		logger.info("Score = " + score);
		Assert.assertNotNull(score);
		logger.debug("Test getSumScore end.");
	}
}
