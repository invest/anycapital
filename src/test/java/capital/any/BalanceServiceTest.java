package capital.any;

import java.util.Date;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import capital.any.model.base.BalanceHistory.BalanceHistoryCommand;
import capital.any.base.enums.ActionSource;
import capital.any.model.base.BalanceRequest;
import capital.any.model.base.Transaction;
import capital.any.model.base.Transaction.TransactionOperationEnum;
import capital.any.model.base.Transaction.TransactionPaymentTypeEnum;
import capital.any.model.base.Transaction.TransactionStatusEnum;
import capital.any.model.base.TransactionOperation;
import capital.any.model.base.TransactionPaymentType;
import capital.any.model.base.TransactionStatus;
import capital.any.model.base.BalanceRequest.OperationType;
import capital.any.model.base.User;
import capital.any.model.base.UserControllerDetails;
import capital.any.service.base.IBalanceService;
import capital.any.service.base.transaction.ITransactionService;
import capital.any.service.base.user.IUserService;

/**
 * @author LioR SoLoMoN
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
   
public class BalanceServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(BalanceServiceTest.class);
	@Autowired
	private IBalanceService balanceService; 
	@Autowired
	private IUserService userService;
	@Autowired
	private ITransactionService transactionService;
	private User user;
	
	@Value("${user.exist}")
	private String userExist;
	
	@Before
	public void init() {
		user = userService.getUserByEmail(userExist);	
	}
	
	@Test
	//@Ignore
	@Transactional
	public void credit() throws Exception {
		final int AMOUNT = 1000;
		Transaction transaction = new Transaction();
		transaction.setUser(user);
		transaction.setAmount(AMOUNT);
		transaction.setStatus(new TransactionStatus(TransactionStatusEnum.SUCCEED.getId()));
		transaction.setIp("127.0.0.1");
		//transaction.setTimeSettled();
		transaction.setComments("test credit");
		transaction.setOperation(new TransactionOperation(TransactionOperationEnum.CREDIT.getId()));
		transaction.setPaymentType(new TransactionPaymentType(TransactionPaymentTypeEnum.WIRE.getId()));
		transaction.setRate(1);	
		transaction.setTimeCreated(new Date());
		transactionService.insertTransaction(transaction); 	
		
		BalanceRequest br = new BalanceRequest();
		br.setAmount(transaction.getAmount());
		br.setCommandHistory(BalanceHistoryCommand.SUCCESS_TRANSACTION);
		br.setOperationType(OperationType.CREDIT);
		br.setReferenceId(transaction.getId());
		br.setUser(user);
		br.setUserControllerDetails(new UserControllerDetails(ActionSource.CRM));
		try {
			logger.info("balance before credit: " + br.getUser().getBalance());
			balanceService.changeBalance(br);
		} catch (Exception e) {
			logger.error("ERROR! balance: " + br.getUser().getBalance());
			throw new Exception();
		}
		logger.info("balance after credit: " + br.getUser().getBalance());
	}
	
	@Test
	@Ignore
	@Transactional
	public void debit() throws Exception {
		final int AMOUNT = 1000;
		Transaction transaction = new Transaction();
		transaction.setUser(user);
		transaction.setAmount(AMOUNT);
		transaction.setStatus(new TransactionStatus(TransactionStatusEnum.SUCCEED.getId()));
		transaction.setIp("127.0.0.1");
		//transaction.setTimeSettled();
		transaction.setComments("test credit");
		transaction.setOperation(new TransactionOperation(TransactionOperationEnum.CREDIT.getId()));
		transaction.setPaymentType(new TransactionPaymentType(TransactionPaymentTypeEnum.WIRE.getId()));
		transactionService.insertTransaction(transaction); 	
		
		BalanceRequest br = new BalanceRequest();
		br.setAmount(transaction.getAmount());
		br.setCommandHistory(BalanceHistoryCommand.SUCCESS_TRANSACTION);
		br.setOperationType(OperationType.DEBIT);
		br.setReferenceId(transaction.getId());
		br.setUser(user);
		br.setUserControllerDetails(new UserControllerDetails(ActionSource.CRM));
		balanceService.changeBalance(br);
	}
}
