package capital.any;

import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import capital.any.model.base.MarketingCampaign;
import capital.any.service.base.marketingCampaign.IMarketingCampaignService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class MarketingCampaignServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(MarketingCampaignServiceTest.class);
	
	@Autowired
	private IMarketingCampaignService marketingCampaignService;
	
	@Test
	@Transactional
	public void insert() {
		logger.debug("About to test the service insert in marketingCampaignService.");
		MarketingCampaign marketingCampaign = new MarketingCampaign();
		marketingCampaign.setName("test campaign");
		marketingCampaign.setSourceId(1);
		marketingCampaign.setDomainId(1);
		marketingCampaign.setLandingPageId(1);
		marketingCampaign.setContentId(1);
		marketingCampaign.setWriterId(6);
		marketingCampaignService.insert(marketingCampaign);
	}
	
	@Test
	@Transactional
	public void update() {
		logger.debug("About to test the service update in marketingCampaignService.");
		MarketingCampaign marketingCampaign = new MarketingCampaign();
		marketingCampaign.setId(1);
		marketingCampaign.setName("test campaign update");
		marketingCampaign.setSourceId(1);
		marketingCampaign.setDomainId(1);
		marketingCampaign.setLandingPageId(1);
		marketingCampaign.setContentId(1);
		marketingCampaign.setWriterId(6);
		marketingCampaignService.update(marketingCampaign);
	}
	
	@Test
	public void getMarketingCampaigns() {
		Map<Long, MarketingCampaign> map = marketingCampaignService.getMarketingCampaigns();
		logger.info("map: " + map.toString());
		Assert.assertNotNull(map);
	}
	
	@Test
	public void getMarketingCampaign() {
		Assert.assertNotNull(marketingCampaignService.getMarketingCampaign(1));
	}
}
