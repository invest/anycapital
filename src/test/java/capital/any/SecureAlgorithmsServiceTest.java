package capital.any;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import capital.any.base.security.ISecureAlgorithmsService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)
public class SecureAlgorithmsServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(SecureAlgorithmsServiceTest.class);
	
	@Autowired
	private ISecureAlgorithmsService secureAlgorithmsService; 
	
	@Value("${user.exist}")
	private String userExist;
	
	@Test
	public void encryptAES() {
		try {
			String userIdEncrypted = secureAlgorithmsService.encryptAES("1111111");
			String emailEncrypted = secureAlgorithmsService.encryptAES("eyal.ohana@anyoption.com");
			logger.debug("userIdEncrypted: " + userIdEncrypted);
			logger.debug("emailEncrypted: " + emailEncrypted);
			Assert.assertNotNull(userIdEncrypted);
			Assert.assertNotNull(emailEncrypted);
		} catch (Exception e) {
			logger.error("Problem to encrypt info. ", e);
		}
	}
	
	@Test
	public void decryptAES() {
		try {
			String userIdEncrypted = secureAlgorithmsService.encryptAES("1111111");
			String emailEncrypted = secureAlgorithmsService.encryptAES(userExist);
			String userIdDecrypted = secureAlgorithmsService.decryptAES("HfrkkxrPgfuuBZFc80sqNQ==");						
			String emailDecrypted = secureAlgorithmsService.decryptAES("n9gH6HvcIQOCIsQxZ2dcEIDgtwNaCmzj/9BBXCJ8kX0=");
			logger.debug("userIdDecrypted: " + userIdDecrypted);
			logger.debug("emailDecrypted: " + emailDecrypted);
			Assert.assertNotNull(userIdDecrypted);
			Assert.assertNotNull(emailDecrypted);
		} catch (Exception e) {
			logger.error("Problem to encrypt info. ", e);
		}
	}
	
	@Test
	public void encryptDecryptAES() {
		try {
			String userIdEncrypted = secureAlgorithmsService.encryptAES("123456");
			String emailEncrypted = secureAlgorithmsService.encryptAES(userExist);
			logger.debug("userIdEncrypted: " + userIdEncrypted);
			logger.debug("emailEncrypted: " + emailEncrypted);
			String userIdDecrypted = secureAlgorithmsService.decryptAES(userIdEncrypted);						
			String emailDecrypted = secureAlgorithmsService.decryptAES(emailEncrypted);
			logger.debug("userIdDecrypted: " + userIdDecrypted);
			logger.debug("emailDecrypted: " + emailDecrypted);
			Assert.assertNotNull(userIdDecrypted);
			Assert.assertNotNull(emailDecrypted);
		} catch (Exception e) {
			logger.error("Problem with encryptAndDecryptAESUrl. ", e);
		}
	}
	
	@Test
	public void encryptAESUrl() {
		try {
			String userIdEncrypted = secureAlgorithmsService.encryptAESUrl("1111111");
			String emailEncrypted = secureAlgorithmsService.encryptAESUrl(userExist);
			logger.debug("userIdEncrypted: " + userIdEncrypted);
			logger.debug("emailEncrypted: " + emailEncrypted);
			Assert.assertNotNull(userIdEncrypted);
			Assert.assertNotNull(emailEncrypted);
		} catch (Exception e) {
			logger.error("Problem to encrypt info. ", e);
		}
	}
}
