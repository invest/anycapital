package capital.any;

import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import capital.any.model.base.Market;
import capital.any.service.base.market.IMarketService;

/**
 * @author eranl
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
   
public class MarketServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(MarketServiceTest.class);

	@Autowired
	private IMarketService marketService;
	
	@Test
    public void getMarketsWithLastPrice() {		 
		Assert.assertNotNull(marketService.getMarketsWithLastPrice());		
	}	
	
	@Test
    public void getMarketsThin() {		 
		Assert.assertNotNull(marketService.getThin());		
	}
	
	@Test
	public void getUnderlyingAssets() {
		logger.debug("getUnderlyingAssets - start.");
		Map<Integer, Market> map = marketService.getUnderlyingAssets();
		Assert.assertNotNull(map);
		logger.debug("getUnderlyingAssets - end.");
	}
}
