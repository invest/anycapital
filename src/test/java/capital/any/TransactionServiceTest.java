package capital.any;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import java.util.Date;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;

import capital.any.model.base.SqlFilters;
import capital.any.model.base.Transaction;
import capital.any.model.base.Transaction.TransactionOperationEnum;
import capital.any.model.base.Transaction.TransactionPaymentTypeEnum;
import capital.any.model.base.Transaction.TransactionStatusEnum;
import capital.any.model.base.TransactionOperation;
import capital.any.model.base.TransactionPaymentType;
import capital.any.model.base.TransactionStatus;
import capital.any.model.base.User;
import capital.any.service.base.transaction.ITransactionService;
import capital.any.service.base.user.IUserService;

/**
 * @author eranl
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
   
public class TransactionServiceTest {

	@Autowired
	private ITransactionService transactionService;
	@Autowired
	private IUserService userService;
	
	@Value("${user.with.money}")
	private String userWithMoney;
	
	private User user;
	
	@Before
	public void init() {
		user = userService.getUserByEmail(userWithMoney);	
	}
			
	@Test
	public void getTransactionByIdExist() {
		SqlFilters filter = new SqlFilters();
		filter.setUserId(user.getId());
		List<Transaction> transactions = transactionService.getTransactions(filter);
		
		Assert.assertNotNull(transactionService.getTransactionById(transactions.get(0).getId()));		
	}
	
	@Test
	public void getTransactionByIdNotExist() {
		Assert.assertNull(transactionService.getTransactionById(0));		
	}
	
	@Test
	public void getTransactionsByUserId() {
		SqlFilters filter = new SqlFilters();
		filter.setUserId(1L);
		List<Transaction> transactions = transactionService.getTransactions(filter);
		Assert.assertNotNull(transactions);
	}
	
	@Test
	@Transactional
	public void UpdateTransactionById() {
		SqlFilters filter = new SqlFilters();
		filter.setUserId(user.getId());
		List<Transaction> transactions = transactionService.getTransactions(filter);
		
		Transaction transaction = transactionService.getTransactionById(transactions.get(0).getId());
		transaction.setStatus(new TransactionStatus(TransactionStatusEnum.SUCCEED.getId()));
		transactionService.updateTransaction(transaction);
	}
		
	@Test
	@Transactional
	public void insertTransaction() {
		Transaction transaction = new Transaction();
		transaction.setUser(user);
		transaction.setAmount(10000);
		transaction.setStatus(new TransactionStatus(TransactionStatusEnum.FAILED.getId()));
		transaction.setIp("127.0.0.1");
		transaction.setComments("insert junit test ");
		transaction.setOperation(new TransactionOperation(TransactionOperationEnum.CREDIT.getId()));
		transaction.setPaymentType(new TransactionPaymentType(TransactionPaymentTypeEnum.WIRE.getId()));
		transaction.setTimeCreated(new Date());
		transactionService.insertTransaction(transaction); 		
	}
	
	@Test
	@Transactional
	public void isUserHaveOpenWithdrawal() {
		Transaction transaction = new Transaction();
		transaction.setUser(user);
		transaction.setAmount(10000);
		transaction.setStatus(new TransactionStatus(TransactionStatusEnum.STARTED.getId()));
		transaction.setIp("127.0.0.1");
		transaction.setComments("insert junit test ");
		transaction.setOperation(new TransactionOperation(TransactionOperationEnum.DEBIT.getId()));
		transaction.setPaymentType(new TransactionPaymentType(TransactionPaymentTypeEnum.WIRE.getId()));
		transaction.setTimeCreated(new Date());
		transactionService.insertTransaction(transaction);
		
		boolean result = transactionService.isUserHaveOpenWithdraw(user.getId());
		Assert.assertTrue(result);
	}
}
