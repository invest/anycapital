package capital.any;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import capital.any.base.enums.ActionSource;
import capital.any.model.base.EmailAction;
import capital.any.model.base.EmailActionRequest;
import capital.any.model.base.ForgetPasswordEmailAction;
import capital.any.model.base.Investment;
import capital.any.model.base.InvestmentEmailAction;
import capital.any.model.base.Product;
import capital.any.model.base.SqlFilters;
import capital.any.model.base.Transaction;
import capital.any.model.base.User;
import capital.any.service.base.emailAction.IEmailActionService;
import capital.any.service.base.investment.IInvestmentService;
import capital.any.service.base.product.IProductService;
import capital.any.service.base.transaction.ITransactionService;
import capital.any.service.base.user.IUserService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)
public class EmailActionServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(EmailActionServiceTest.class);
	
	@Autowired
	private IEmailActionService emailActionService;
	@Autowired
	private IUserService userService;
	@Autowired
	private IInvestmentService investmentService;
	@Autowired
	private IProductService productService;
	@Autowired
	private ITransactionService transactionService;
	
	private User user;
	
	@Value("${user.with.money}")
	private String userWithMoney;
	
	@Before
	public void init() {
		user = userService.getUserByEmail(userWithMoney);	
	}
	
	@Test
	public void get() {
		Assert.assertNotNull(emailActionService.get());
	}
	
	@Test
	@Transactional
	public void insertEmailByActionRegister() {
		User user = userService.getUserById(1);
		EmailActionRequest<?> emailActionRequest = new EmailActionRequest<>(user, new EmailAction(capital.any.base.enums.EmailAction.REGISTER.getId()), null);
		emailActionRequest.setActionSource(ActionSource.CRM);
		try {
			emailActionService.insertEmailByAction(emailActionRequest);
		} catch (Exception e) {
			logger.error("Problem with insert email by action.", e);
		}
	}
	
	@Test
	@Transactional
	public void insertEmailByActionInvestment() {
		SqlFilters filters = new SqlFilters();
		filters.setUserId(user.getId());;
		List<Investment> investments = investmentService.get(filters);
		
		User user = userService.getUserById(1);
		
		InvestmentEmailAction investmentEmailAction = new InvestmentEmailAction();
		Investment investment = investmentService.getInvestmentById(investments.get(0).getId());
		Product product = productService.getProduct(investment.getProductId());
		investmentEmailAction.setInvestment(investment);
		investmentEmailAction.setProduct(product);
		
		EmailActionRequest<InvestmentEmailAction> emailActionRequest = new EmailActionRequest<InvestmentEmailAction>();
		emailActionRequest.setData(investmentEmailAction);
		emailActionRequest.setUser(user);
		emailActionRequest.setEmailAction(new EmailAction(capital.any.base.enums.EmailAction.INSERT_INVESTMENT_OPEN.getId()));
		emailActionRequest.setActionSource(ActionSource.CRM);
		
		try {
			emailActionService.insertEmailByAction(emailActionRequest);
		} catch (Exception e) {
			logger.error("Problem with insert email by action.");
		}
	}
	
	@Test
	@Transactional
	public void insertEmailByActionForgetPassword() {
		User user = userService.getUserById(1);
		
		ForgetPasswordEmailAction forgetPasswordEmailAction = new ForgetPasswordEmailAction();
		forgetPasswordEmailAction.setGoogleShortUrlId("test url");
		
		EmailActionRequest<ForgetPasswordEmailAction> emailActionRequest = new EmailActionRequest<ForgetPasswordEmailAction>();
		emailActionRequest.setData(forgetPasswordEmailAction);
		emailActionRequest.setUser(user);
		emailActionRequest.setEmailAction(new EmailAction(capital.any.base.enums.EmailAction.FORGET_PASSWORD.getId()));
		emailActionRequest.setActionSource(ActionSource.CRM);
		
		try {
			emailActionService.insertEmailByAction(emailActionRequest);
		} catch (Exception e) {
			logger.error("Problem with insert email by action.");
		}
	}
	
	@Test
	public void insertEmailByActionDeposit() {
		User user = userService.getUserById(10105);
		Transaction transaction = transactionService.getTransactionById(10902);
		
		EmailActionRequest<Transaction> emailActionRequest = new EmailActionRequest<Transaction>();
		emailActionRequest.setData(transaction);
		emailActionRequest.setUser(user);
		emailActionRequest.setEmailAction(new EmailAction(capital.any.base.enums.EmailAction.DEPOSIT_PENDING_BANK_WIRE.getId()));
		emailActionRequest.setActionSource(ActionSource.CRM);
		try {
			emailActionService.insertEmailByAction(emailActionRequest);
		} catch (Exception e) {
			logger.error("Problem with insert email by action.", e);
		}
	}
}
