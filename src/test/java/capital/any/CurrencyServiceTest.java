package capital.any;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import capital.any.service.base.currency.ICurrencyService;

/**
 * @author eranl
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
   
public class CurrencyServiceTest {

	@Autowired
	private ICurrencyService currencyService;
	
	@Test
    public void getCurrencyList() {
		Assert.assertNotNull(currencyService.get());		
	}
	
	@Test
    public void getCurrencyByIdExist() {		 
		Assert.assertNotNull(currencyService.get().get(3));		
	}
	
	@Test
    public void getCurrencyByIdNotExist() {		 
		Assert.assertNull(currencyService.get().get(0));		
	}
	
	@Test
    public void getCurrencyRateEuro() {		 
		Assert.assertEquals(1d, currencyService.getRate(capital.any.base.enums.Currency.EURO.getId()), 0.1);		
	}
	
	@Test
    public void getCurrencyRateDollar() {		 
		double rate = currencyService.getRate(capital.any.base.enums.Currency.DOLLAR.getId());
//		System.out.println("rate " + rate);
		Assert.assertTrue(rate > 0);	
	}
}
