package capital.any;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import capital.any.service.base.aws.IAwsService;

/**
 * @author Eyal G
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)
public class AwsServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(AwsServiceTest.class);
	
	@Autowired
	private IAwsService awsServiceService;

//	@Test
//	public void ListBuckets() {
//		awsServiceService.getBuckets();
//	}
//	
//	@Test
//	public void ListFilesInBuckets() {
//		awsServiceService.getFilesInBucket();
//	}
	
	@Test
	public void getFile() {
//		awsServiceService.getFile();
	}
	
	@Test
	public void uploadFile() {
//		awsServiceService.uploadFile();;
	}
}
