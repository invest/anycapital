package capital.any;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import capital.any.base.enums.GBGStatus;
import capital.any.service.base.gbg.score.IGBGScoreService;

/**
 * @author LioR SoLoMoN
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)
public class GBGScoreServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(GBGScoreServiceTest.class);
	
	@Autowired
	private IGBGScoreService gbgScoreService;
	
	@Test
	public void getStatus() {
		GBGStatus status = gbgScoreService.getStatus(1112);
		logger.info(status.toString());
	}
	
}
