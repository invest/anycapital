package capital.any;

import java.util.Iterator;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import capital.any.model.base.Country;
import capital.any.service.base.country.ICountryService;

/**
 * @author eranl
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
   
public class CountryServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(CountryServiceTest.class);

	@Autowired
	private ICountryService countryService;
	
	@Test
    public void getCountriesList() {		 
		Map<Integer, Country> countries = countryService.get(); 
		logger.info("countries = " + countries.toString());
		for (Iterator<Integer> iter = countries.keySet().iterator(); iter.hasNext();) {
			int countryId = iter.next();
			Country c = countries.get(countryId);
			logger.info("Country = " + c.toString());
		}
		Assert.assertNotNull(countries);		
	}
	
	@Test
    public void getCountryByIdExists() {		 
		Assert.assertNotNull(countryService.get().get(1));		
	}
	
	@Test
    public void getCountryByIdNotExists() {		 
		Assert.assertNull(countryService.get().get(0));		
	}
	
	@Test
    public void getIdByIP() {		 
		countryService.getByIp("127.0.0.1");		
	}	
}
