package capital.any;

import java.util.Map;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import capital.any.model.base.ProductBarrierType;
import capital.any.service.base.productBarrierType.IProductBarrierTypeService;


/**
 * @author Eyal G
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
   
public class ProductBarrierTypeServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(ProductBarrierTypeServiceTest.class);

	@Autowired
	private IProductBarrierTypeService productBarrierTypeService;
	
	@Test
    public void getProductBarrierTypes() {		 
		Map<Integer, ProductBarrierType> hm = productBarrierTypeService.get();
		logger.debug(hm.toString());
		Assert.assertNotNull(hm);		
	}
		
}
