package capital.any;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import capital.any.service.base.actionSource.IActionSourceService;

/**
 * 
 * @author Eyal.o
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)
public class ActionSourceServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(ActionSourceServiceTest.class);
	
	@Autowired
	private IActionSourceService actionSourceService;
	
	@Test
	public void get() {
		logger.debug("About to test get from actionSourceService.");
		Assert.assertNotNull(actionSourceService.get());
	}
}
