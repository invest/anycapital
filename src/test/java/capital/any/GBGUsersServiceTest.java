package capital.any;

import java.util.Date;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import capital.any.model.base.GBGUser;
import capital.any.model.base.User;
import capital.any.service.base.gbg.users.IGBGUsersService;
import capital.any.service.base.user.IUserService;


/**
 * @author LioR SoLoMoN
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)
public class GBGUsersServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(GBGUsersServiceTest.class);
	
	@Autowired
	private IGBGUsersService gbgUsersService;
	@Autowired
	private IUserService userService;
	
	@Value("${user.exist}")
	private String userExist;
	
	private User user;
	
	@Before
	public void init() {
		user = userService.getUserByEmail(userExist);	
	}
	
	@Test
	public void get() {
		Assert.assertNotNull(gbgUsersService.get());
	}
	
	@Test
	@Transactional
	public void getByUser() {
		GBGUser gbgUser = new GBGUser(user.getId(), 0, null, null, new Date(), new Date());
		gbgUsersService.insert(gbgUser);
		Assert.assertNotNull(gbgUsersService.get(user.getId()));
	}
	
	@Test
	@Transactional
	public void insert() {
		GBGUser gbgUser = new GBGUser(user.getId(), 0, null, null, new Date(), new Date());
		boolean result = gbgUsersService.insert(gbgUser);
		Assert.assertTrue(result);
	}
	
	
	@Test
	@Transactional
	public void update() {
		GBGUser gbgUser = new GBGUser(user.getId(), 0, null, null, new Date(), new Date());
		boolean result = gbgUsersService.insert(gbgUser);
		GBGUser gbgUserUpdate = new GBGUser(1, null, new Date());
		gbgUsersService.update(gbgUserUpdate);
		Assert.assertTrue(result);
	}
}
