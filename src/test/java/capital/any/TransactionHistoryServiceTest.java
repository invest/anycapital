package capital.any;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.Repeat;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import capital.any.base.enums.ActionSource;
import capital.any.model.base.NullWriter;
import capital.any.model.base.SqlFilters;
import capital.any.model.base.Transaction;
import capital.any.model.base.Transaction.TransactionOperationEnum;
import capital.any.model.base.Transaction.TransactionPaymentTypeEnum;
import capital.any.model.base.Transaction.TransactionStatusEnum;
import capital.any.model.base.TransactionHistory;
import capital.any.model.base.TransactionOperation;
import capital.any.model.base.TransactionPaymentType;
import capital.any.model.base.TransactionStatus;
import capital.any.model.base.User;
import capital.any.service.base.transaction.ITransactionService;
import capital.any.service.base.transaction.history.ITransactionHistoryService;
import capital.any.service.base.user.IUserService;

/**
 * @author LioR SoLoMoN
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
   
public class TransactionHistoryServiceTest {
	@Autowired
	private ITransactionHistoryService transactionHistoryService;
	@Autowired
	private ITransactionService transactionService;
	@Autowired
	private IUserService userService;
	
	@Value("${user.with.money}")
	private String userWithMoney;
	
	private User user;
	
	@Before
	public void init() {
		user = userService.getUserByEmail(userWithMoney);	
	}
	
	@Test
	@Transactional
	public void getTransaction() {
		TransactionHistory th = insert();
		Assert.assertNotNull(transactionHistoryService.get(th));		
	}
	
	/*@Test
	public void getTransactions() {
		Assert.assertNotNull(transactionHistoryService.get());		
	}*/
		
	@Test
	//@Repeat(100)
	@Transactional
	@Ignore
	public void insertTransactionHistory() {
		insert();
	}
	
	public TransactionHistory insert() {
		Transaction transaction = new Transaction();
		transaction.setUser(user);
		transaction.setAmount(30000);
		transaction.setStatus(new TransactionStatus(TransactionStatusEnum.WAITING_FOR_FIRST_APPROVE.getId()));
		transaction.setIp("127.0.0.1");
		transaction.setComments("insert test null");
		transaction.setOperation(new TransactionOperation(TransactionOperationEnum.CREDIT.getId()));
		transaction.setPaymentType(new TransactionPaymentType(TransactionPaymentTypeEnum.WIRE.getId()));
		transaction.setTimeCreated(new Date());
		transactionService.insertTransaction(transaction);
		
		TransactionHistory th = new TransactionHistory();
		th.setActionSource(ActionSource.CRM);
		th.setServerName("SERVER-JUNIT");
		th.setStatus(new TransactionStatus(TransactionStatusEnum.STARTED.getId()));
		th.setTimeCreated(new Date());
		th.setTransactionId(transaction.getId());
		th.setWriter(new NullWriter());
		transactionHistoryService.insert(th);
		return th;
	}
	
}
