package capital.any;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import capital.any.service.base.IServerConfiguration;

/**
 * @author LioR SoLoMoN
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)    
public class ServerConfigurationTest {
	private static final Logger logger = LoggerFactory.getLogger(ServerConfigurationTest.class);
	@Autowired
	private IServerConfiguration serverConfiguration; 

	@Test
	public void serverName() {
		String serverName = serverConfiguration.getServerName();
		logger.info("server name: " + serverName);
	}
	
	@Test
	public void geoLocation() {
		String[] geoLocation = serverConfiguration.getGeoLocation("127.0.0.1");
		if (geoLocation != null && geoLocation.length > 0) {
			for (String str : geoLocation) {
				logger.info("geo Location: " + str);
			}
		}
	}
	
	@Test
	public void isValidEmail() {
		boolean validEmail = serverConfiguration.isValidEmail("lior@anycapital.com");
		logger.info("valid email " + validEmail);
	}
}
