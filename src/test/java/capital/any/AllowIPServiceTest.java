package capital.any;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import capital.any.service.base.allowIP.IAllowIPService;

/**
 * @author eranl
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
   
public class AllowIPServiceTest {

	@Autowired
	private IAllowIPService allowIPService;
	
	@Test
    public void getAllowIPList() {	
		Assert.assertNotNull(allowIPService.get());		
	}
	
	
}
