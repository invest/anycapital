package capital.any;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import capital.any.base.enums.Table;
import capital.any.model.base.ProviderRequestResponse;
import capital.any.service.base.providerRequestResponse.IProviderRequestResponseService;

/**
 * @author eran.levy
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
   
public class ProviderRequestResponseServiceTest {
	
	private static final Logger logger = LoggerFactory.getLogger(ProviderRequestResponseServiceTest.class);
	
	@Autowired
	private IProviderRequestResponseService providerRequestResponseService;
	
	@Test
	@Transactional
	public void insertSMSProviderRequest() {
		ProviderRequestResponse providerRequestResponse = new ProviderRequestResponse();
		providerRequestResponse.setTableId(Table.SMS.getId());
		providerRequestResponse.setRequest("test");
		providerRequestResponseService.insert(providerRequestResponse); 		
	}
	
}
