package capital.any;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import capital.any.model.base.MarketingSource;
import capital.any.service.base.marketingSource.IMarketingSourceService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class MarketingSourceServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(MarketingSourceServiceTest.class);
	
	@Autowired
	private IMarketingSourceService marketingSourceService;
	
	@Test
	@Transactional
	public void insert() {
		logger.debug("About to test the service insert in marketingSourceService.");
		MarketingSource marketingSource = new MarketingSource();
		marketingSource.setName("source");
		marketingSource.setWriterId(6);
		marketingSourceService.insert(marketingSource);
	}
	
	@Test
	@Transactional
	public void update() {
		logger.debug("About to test the service update in marketingSourceService.");
		MarketingSource marketingSource = new MarketingSource();
		marketingSource.setId(21);
		marketingSource.setName("source updated");
		marketingSource.setWriterId(6);
		marketingSourceService.update(marketingSource);
	}
	
	@Test
	public void getMarketingSources() {
		Assert.assertNotNull(marketingSourceService.getMarketingSources());
	}
	
	@Test
	public void getMarketingSource() {
		Assert.assertNotNull(marketingSourceService.getMarketingSource(1));
	}
	
}
