package capital.any;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import capital.any.base.enums.ActionSource;
import capital.any.base.enums.DBParameter;
import capital.any.model.base.NullWriter;
import capital.any.model.base.Transaction;
import capital.any.model.base.TransactionFee;
import capital.any.model.base.TransactionHistory;
import capital.any.model.base.TransactionOperation;
import capital.any.model.base.TransactionPaymentType;
import capital.any.model.base.TransactionStatus;
import capital.any.model.base.User;
import capital.any.model.base.Transaction.TransactionOperationEnum;
import capital.any.model.base.Transaction.TransactionPaymentTypeEnum;
import capital.any.model.base.Transaction.TransactionStatusEnum;
import capital.any.service.base.dbParameter.IDBParameterService;
import capital.any.service.base.transaction.ITransactionService;
import capital.any.service.base.transactionFee.ITransactionFeeService;
import capital.any.service.base.user.IUserService;

/**
 * @author LioR SoLoMoN
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
   
public class TransactionFeeServiceTest {
	@Autowired
	private ITransactionFeeService transactionFeeService;
	@Autowired
	private ITransactionService transactionService;
	@Autowired
	private IDBParameterService dbParameterService;
	@Autowired
	private IUserService userService;
	
	private User user;
	
	@Value("${user.exist}")
	private String userExist;
	
	@Before
	public void init() {
		user = userService.getUserByEmail(userExist);	
	}
	
	@Test
	@Transactional
	public void test() {
		Transaction transaction = new Transaction();
		transaction.setUser(user);
		transaction.setAmount(10000);
		transaction.setStatus(new TransactionStatus(TransactionStatusEnum.STARTED.getId()));
		transaction.setIp("");
		transaction.setOperation(new TransactionOperation(TransactionOperationEnum.CREDIT.getId()));
		transaction.setPaymentType(new TransactionPaymentType(TransactionPaymentTypeEnum.WIRE.getId()));
		transaction.setRate(0);
		transaction.setTimeCreated(new Date());
		transaction.setComments("JUNIT-test");
		
		TransactionHistory transactionHistory = new TransactionHistory();
		transactionHistory.setActionSource(ActionSource.JOB);
		transactionHistory.setServerName("SERVER-JUNIT");
		transactionHistory.setStatus(transaction.getStatus());
		transactionHistory.setTransactionId(transaction.getId());
		transactionHistory.setWriter(new NullWriter());
		transactionService.insertTransaction(transaction, transactionHistory);
		
		long feeAmount = dbParameterService.get(DBParameter.WITHDRAW_FEE_AMOUNT.getId()).getNumValue();
		TransactionFee transactionFee = new TransactionFee(transaction.getId(), feeAmount , new Date());
		transactionFeeService.insert(transactionFee);	
	}
}
