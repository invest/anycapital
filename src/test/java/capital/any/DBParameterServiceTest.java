package capital.any;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import capital.any.service.base.dbParameter.IDBParameterService;

/**
 * @author eranl
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
   
public class DBParameterServiceTest {

	@Autowired
	private IDBParameterService dbParameterService;
	
	@Test
    public void getDBParameterList() {		 
		Assert.assertNotNull(dbParameterService.get());		
	}
	
	
	@Test
    public void getDBParameterExists() {		 
		Assert.assertNotNull(dbParameterService.get().get(1));		
	}
	
	@Test
    public void getDBParameterNotExists() {		 
		Assert.assertNull(dbParameterService.get().get(9999));		
	}
	
}
