package capital.any;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


import capital.any.service.base.productType.IProductTypeService;

/**
 * @author eranl
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
   
public class ProductTypeServiceTest {

	@Autowired
	private IProductTypeService productTypeService;
	
	@Test
    public void getProductTypes() {		 
		Assert.assertNotNull(productTypeService.get());		
	}
		
}
