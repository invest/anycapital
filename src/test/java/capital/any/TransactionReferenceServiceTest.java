package capital.any;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import capital.any.model.base.Transaction;
import capital.any.model.base.Transaction.TransactionOperationEnum;
import capital.any.model.base.Transaction.TransactionPaymentTypeEnum;
import capital.any.model.base.Transaction.TransactionStatusEnum;
import capital.any.model.base.TransactionOperation;
import capital.any.model.base.TransactionPaymentType;
import capital.any.model.base.TransactionReference;
import capital.any.model.base.TransactionStatus;
import capital.any.model.base.User;
import capital.any.service.base.transaction.ITransactionService;
import capital.any.service.base.transactionReference.ITransactionReferenceService;

/**
 * @author Eyal Goren
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
   
public class TransactionReferenceServiceTest {
	
	@Autowired
	private ITransactionService transactionService;
	
	@Autowired
	private ITransactionReferenceService transactionReferenceService;
		
	@Test
	@Transactional
	public void insertTransactionReference() {
		Transaction transaction = new Transaction();
		transaction.setUser(new User(10002));
		transaction.setAmount(10000);
		transaction.setStatus(new TransactionStatus(TransactionStatusEnum.FAILED.getId()));
		transaction.setIp("127.0.0.1");
		transaction.setComments("insert test");
		transaction.setOperation(new TransactionOperation(TransactionOperationEnum.CREDIT.getId()));
		transaction.setPaymentType(new TransactionPaymentType(TransactionPaymentTypeEnum.WIRE.getId()));
		transaction.setTimeCreated(new Date());
		transactionService.insertTransaction(transaction);
		
		TransactionReference transactionReference = new TransactionReference();
		transactionReference.setReferenceId(123);;
		transactionReference.setTableId(1);
		transactionReference.setTransactionId(transaction.getId());
		transactionReferenceService.insert(transactionReference);
	}
}
