package capital.any;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import capital.any.communication.Response;
import capital.any.model.base.GBGRequest;
import capital.any.model.base.GBGResponse;
import capital.any.service.base.gbg.IGBGService;

/**
 * @author LioR SoLoMoN
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)    
public class GBGServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(GBGServiceTest.class);
	@Autowired
	private IGBGService gbgService;

	@Test
	@Ignore
	public void test() {
		GBGRequest gbgRequest = 
				new GBGRequest(10286, 
						"89d8add5-53eb-4695-816a-9a657e60ff8e", 
						"Giuseppe",
						"Valente", 
						"Male",
						"02",
						"06", 
						"1979",
						"Germany",
						"rossmarkt 11",
						"11", 
						"alzey", 
						"55232");
		Response<GBGResponse> gbgResponse = gbgService.authenticateSP(gbgRequest);
		
		logger.info(gbgResponse.toString());
	}
}
