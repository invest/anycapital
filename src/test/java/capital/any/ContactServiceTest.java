package capital.any;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import capital.any.base.enums.ActionSource;
import capital.any.model.base.Contact;
import capital.any.model.base.Contact.Issue;
import capital.any.model.base.Contact.Type;
import capital.any.model.base.MarketingTracking;
import capital.any.service.base.contact.IContactService;

/**
 * @author eran.levy
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
   
public class ContactServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(ContactServiceTest.class);
	@Autowired
	private IContactService contactService;
	
	@Value("${marketing.default.id}")
	private Integer marketingDefaultId;
	
	@Test
	@Transactional
	public void insertContact() {
		Contact contact = new Contact();
		contact.setFirstName("test");
		contact.setLastName("test");
		contact.setEmail("test2@test.com");
		contact.setIp("127.0.0.1");
		contact.setMobilePhone("12345678");
		contact.setLanguageId(2);
		contact.setUtcOffset(0);
		contact.setType(Type.CONTACT_US);
		//user.setUserAgent(rs.getString("user_agent"));
		//user.setHttpReferer(rs.getString("http_referer"));
		contact.setCountryId(2);
		//user.setWriterId(rs.getInt("writer_id"));
		//user.setAoUserId(rs.getInt("ao_user_id"));
		contact.setIssue(Issue.GENERAL);
		contact.setActionSource(ActionSource.WEB);
		MarketingTracking marketingTracking = new MarketingTracking();
		marketingTracking.setId(marketingDefaultId);
		contact.setMarketingTracking(marketingTracking);
		contactService.insert(contact); 		
	}
	
}
