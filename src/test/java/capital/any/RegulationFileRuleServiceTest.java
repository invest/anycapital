package capital.any;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import capital.any.model.base.RegulationFileRule;
import capital.any.service.base.regulationFileRule.IRegulationFileRuleService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)
public class RegulationFileRuleServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(RegulationFileRuleServiceTest.class);
	
	@Autowired
	private IRegulationFileRuleService regulationFileRuleService;
	
	@Test
	public void getFilesByRule() {
		logger.info("About to test getFilesByRule.");
		RegulationFileRule regulationFileRule = new RegulationFileRule();
		regulationFileRule.setCountryRiskId(1);
		regulationFileRule.setGbgStatusId(1);
		Assert.assertNotNull(regulationFileRuleService.getFilesByRule(regulationFileRule));
	}
}
