package capital.any;

import java.util.Locale;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import capital.any.service.IUtilService;
import capital.any.service.base.locale.ILocaleService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)
public class UtilServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(UtilServiceTest.class);
	
	@Autowired
	private IUtilService utilService;
	@Autowired
	private ILocaleService localeService;

	@Test
	public void displayAmount() {
		Locale locale = localeService.getAvailableLocales().get("IE");
		String amount = utilService.displayAmount(100, locale, null);
		logger.info("amount = " + amount + "locale= " + locale);
		Assert.assertNotNull(amount);
	}
}
