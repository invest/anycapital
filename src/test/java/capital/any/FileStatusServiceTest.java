package capital.any;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import capital.any.service.base.fileStatus.IFileStatusService;

/**
 * 
 * @author Eyal.o
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class FileStatusServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(FileStatusServiceTest.class);
	
	@Autowired
	private IFileStatusService fileStatusService;
	
	@Test
	public void get() {
		logger.info("Get all file statuses. ");
		Assert.notNull(fileStatusService.get());
	}
}
