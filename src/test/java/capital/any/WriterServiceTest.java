package capital.any;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import capital.any.service.base.writer.IWriterService;

/**
 * @author eranl
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)    
public class WriterServiceTest {

	@Autowired
	private IWriterService writerService;
	
	@Value("${writer.email.active}")
	private String writerEmailActive;
	
	@Test
    public void getWriterByIdExist() {		
		Assert.assertNotNull(writerService.getWriterById(new Integer(3)));		
	}
	
	@Test
    public void getWriterByIdNotExist() {
		Assert.assertNull(writerService.getWriterById(new Integer(0)));		
	}

	@Test
    public void getWriterByEmailExist() {		
		Assert.assertNotNull(writerService.getWriterByEmail(writerEmailActive));		
	}
	
	@Test
    public void getWriterByEmailNotExist() {
		Assert.assertNull(writerService.getWriterByEmail("test"));		
	}
	
	@Test
    public void getWriterByUsernameExist() {		
		Assert.assertNotNull(writerService.getWriterByUsername("eran"));		
	}
	
	@Test
    public void getWriterByUsernameNotExist() {
		Assert.assertNull(writerService.getWriterByUsername("test"));		
	}
	
//	@Test
//	//@Transactional
//	public void insertUser() {
//		User user = new User();
//
//		user.setUserName("TEST" +UUID.randomUUID().toString().substring(0,6));
//		user.setPassword("123456");
//		user.setCurrencyId(3);
//		user.setFirstName("test");
//		user.setLastName("test");
//		user.setStreet("test");
//		user.setZipCode("11111");
//		user.setEmail("test2@test.com");
//		user.setIp("127.0.0.1");
//		user.setMobilePhone("12345678");
//		user.setGender("M");
//		user.setStreetNo("1");
//		user.setUtcOffsetModified("UTC+0");
//		user.setLanguageId(2);
//		user.setUtcOffset("UTC+0");
//		user.setCityName("aaaaa");
//		//user.setUserAgent(rs.getString("user_agent"));
//		//user.setHttpReferer(rs.getString("http_referer"));
//		user.setCountryId(2);
//		//user.setWriterId(rs.getInt("writer_id"));
//		//user.setAoUserId(rs.getInt("ao_user_id"));
//		user.setActionSourceId(ActionSource.ACTION_SOURCE_WEB);
//		user.setUtcOffsetCreated("UTC+0");
//	
//		userService.insertUser(user); 		
//	}
}
