package capital.any;

import java.io.FileInputStream;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import capital.any.service.base.xlsx.IXlsxService;

/**
 * 
 * @author Eyal.o
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)
public class XlsxServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(XlsxServiceTest.class);
	
	@Autowired
	private IXlsxService xlsxService;
	
	@Test
	public void readDocument() {
		java.io.File f = new java.io.File("D:/AC-EN-24-09-2017-test.xlsx");
        System.out.println(f.isFile()+"  "+f.getName()+f.exists());
        FileInputStream fi1;
		try {
			fi1 = new FileInputStream(f);
			MockMultipartFile fstmp = new MockMultipartFile("fileUpload", f.getName(), "multipart/form-data", fi1);
			List<List<String>> docInfo = xlsxService.readDocument(fstmp);
			docInfo.forEach(line -> {
				System.out.println(line.get(0));
				System.out.println(line.get(1));
			});
		} catch (Exception e) {
			logger.error("");
		}
	}
}
