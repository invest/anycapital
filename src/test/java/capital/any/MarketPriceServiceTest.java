package capital.any;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import capital.any.base.enums.MarketPriceTypeEnum;
import capital.any.model.base.MarketPrice;
import capital.any.service.base.marketPrice.IMarketPriceService;

/**
 * @author eranl
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
   
public class MarketPriceServiceTest {
	
	private static final Logger logger = LoggerFactory.getLogger(MarketPriceServiceTest.class);

	@Autowired
	private IMarketPriceService marketPriceService;
	
	@Test
	@Transactional
    public void updateMarketPrice() throws Exception {		 
		MarketPrice marketPrice = new MarketPrice();
		marketPrice.setMarketId(1);
		marketPrice.setTypeId(MarketPriceTypeEnum.LAST_PRICE.getId());
		marketPrice.setPrice(100);
		marketPriceService.updateMarketPrice(marketPrice);		
	}
	
	@Test
	@Transactional
    public void insertHistoryMarketPrice() {		 
		MarketPrice marketPrice = new MarketPrice();
		marketPrice.setMarketId(14);
		marketPrice.setTypeId(MarketPriceTypeEnum.HISTORY_PRICE.getId());
		marketPrice.setPrice(100);
		marketPrice.setPriceDate(new Date());
		marketPriceService.insertMarketPrice(marketPrice);		
	}
}
