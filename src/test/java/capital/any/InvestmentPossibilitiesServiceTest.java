package capital.any;

import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import capital.any.model.base.InvestmentPossibility;
import capital.any.service.base.investmentPossibilities.IInvestmentPossibilitiesService;

/**
 * @author eranl
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
   
public class InvestmentPossibilitiesServiceTest {
	
	private static final Logger logger = LoggerFactory.getLogger(InvestmentPossibilitiesServiceTest.class);

	@Autowired
	private IInvestmentPossibilitiesService investmentPossibilitiesService;
	
	@Test
    public void getInvestmentPossibilities() {
		Map<Integer, InvestmentPossibility> map = investmentPossibilitiesService.get();
		for (InvestmentPossibility investmentPossibility : map.values()) {
			logger.debug(investmentPossibility.toString());
		}
		Assert.assertNotNull(map);		
	}
		
}
