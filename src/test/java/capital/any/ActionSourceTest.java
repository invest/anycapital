package capital.any;

import java.io.IOException;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import capital.any.base.enums.ActionSource;

/**
 * @author LioR SoLoMoN
 *
 */
public class ActionSourceTest {
	private static final Logger logger = LoggerFactory.getLogger(ActionSourceTest.class);

	@Test
	public void test1() {
		ObjectMapper mapper = new ObjectMapper();
		ActionSource as = null;
		
		try {
			as = mapper.readValue("2", ActionSource.class);
		} catch (IOException e) {
			logger.error("ERROR! ", e);
		}

		Assert.assertEquals(ActionSource.CRM, as);
	}
	
	/*@Test(expected = IllegalArgumentException.class)
	public void test2() {
		ObjectMapper mapper = new ObjectMapper();
		ActionSource as = null;
		
		try {
			as = mapper.readValue("15542", ActionSource.class);
		} catch (IOException e) {
			logger.error("ERROR! ", e);
		}
	}*/
}
