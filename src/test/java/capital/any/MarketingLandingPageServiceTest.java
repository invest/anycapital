package capital.any;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import capital.any.model.base.MarketingLandingPage;
import capital.any.service.base.marketingLandingPage.IMarketingLandingPageService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class MarketingLandingPageServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(MarketingLandingPageServiceTest.class);
	
	@Autowired
	private IMarketingLandingPageService marketingLandingPageService;
	
	@Test
	@Transactional
	public void insert() {
		logger.debug("About to test the service insert in marketingLandingPageService.");
		MarketingLandingPage marketingLandingPage = new MarketingLandingPage();
		marketingLandingPage.setName("LP test");
		marketingLandingPage.setPath("index");
		marketingLandingPage.setWriterId(6);
		try {
			marketingLandingPageService.insert(marketingLandingPage);
		} catch (Exception e) {
			logger.error("Problem to insert marketingLandingPage.", e);
		}
	}
	
	@Test
	@Transactional
	public void update() {
		logger.debug("About to test the service update in marketingLandingPageService.");
		MarketingLandingPage marketingLandingPage = new MarketingLandingPage();
		marketingLandingPage.setId(1);
		marketingLandingPage.setName("LP test updated");
		marketingLandingPage.setPath("register");
		marketingLandingPage.setWriterId(6);
		try {
			marketingLandingPageService.update(marketingLandingPage);
		} catch (Exception e) {
			logger.error("Problem to update marketingLandingPage.", e);
		}
	}
	
	@Test
	public void getAll() {
		Assert.assertNotNull(marketingLandingPageService.getAll());
	}
	
	@Test
	public void getMarketingLandingPage() {
		Assert.assertNotNull(marketingLandingPageService.getMarketingLandingPage(1));
	}
}
