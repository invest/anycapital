package capital.any;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


import capital.any.service.base.productCallable.IProductCallableService;

/**
 * @author eranl
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
   
public class ProductCallableServiceTest {

	@Autowired
	private IProductCallableService productCallableService;
	
	@Test
    public void getProductCallables() {		 
		Assert.assertNotNull(productCallableService.getProductCallables());		
	}
		
}
