package capital.any;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import capital.any.service.base.issueReactions.IIssueReactionService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)
public class IssueReactionServiceTest {
	
	@Autowired
	private IIssueReactionService issueReactionService;
	
	@Test
	public void getIssueReaction() {
		Assert.assertNotNull(issueReactionService.get());
	}
}
