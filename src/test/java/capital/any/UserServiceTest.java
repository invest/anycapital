package capital.any;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import capital.any.base.enums.ActionSource;
import capital.any.model.base.MarketingTracking;
import capital.any.model.base.NullWriter;
import capital.any.model.base.User;
import capital.any.model.base.User.Clazz;
import capital.any.model.base.User.Gender;
import capital.any.model.base.User.Step;
import capital.any.model.base.UserFilters;
import capital.any.model.base.UserHistory;
import capital.any.model.base.UserStep;
import capital.any.service.base.user.IUserService;

/**
 * @author eranl
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
   
public class UserServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(UserServiceTest.class);
	@Autowired
	private IUserService userService;
	
	@Value("${user.not.exist}")
	private String userNotExist;
	@Value("${user.exist}")
	private String userExist;
	
	private User user;
	
	@Before
	public void init() {
		user = userService.getUserByEmail(userExist);	
	}
	
	@Test
    public void getUserByIdExist() {
		Assert.assertNotNull(userService.getUserById(user.getId()));		
	}
	
	@Test
    public void getUserByIdNotExist() {
		Assert.assertNull(userService.getUserById(0));		
	}

	@Test
    public void getUserByEmailExist() {
		Assert.assertNotNull(userService.getUserByEmail(userExist));		
	}
	
	@Test
    public void getUserByEmailNotExist() {
		Assert.assertNull(userService.getUserByEmail(userNotExist));		
	}

	@Test
	@Transactional
	public void insertUser() {
		User user = new User();
		String rand = UUID.randomUUID().toString().substring(0,6);
		user.setPassword("123456");
		user.setCurrencyId(3);
		user.setFirstName("test");
		user.setLastName("test");
		user.setStreet("test");
		user.setZipCode("11111");
		user.setEmail(rand + "test@test.com");
		user.setIp("127.0.0.1");
		user.setMobilePhone("12345678");
		user.setGender(Gender.MALE);
		user.setStreetNo("1");
		user.setLanguageId(2);
		user.setUtcOffset(0);
		user.setCityName("aaaaa");
		//user.setUserAgent(rs.getString("user_agent"));
		//user.setHttpReferer(rs.getString("http_referer"));
		//user.setWriterId(rs.getInt("writer_id"));
		//user.setAoUserId(rs.getInt("ao_user_id"));
		user.setActionSource(ActionSource.WEB);
		user.setUtcOffsetCreated("UTC+0");
		user.setCountryByPrefix(12);
		
		MarketingTracking marketingTracking = new MarketingTracking();
		marketingTracking.setCampaignId(1);
		marketingTracking.setQueryString("queryString test");
		user.setMarketingTracking(marketingTracking);
	
		try {
			userService.insertUser(user);
		} catch (Exception e) {
			logger.error("Problem with test insertUser", e);
		} 		
	}
	
	@Test
	@Transactional
	public void insertUserStepA() {
		User user = new User();
		String rand = UUID.randomUUID().toString().substring(0,6);
		user.setPassword("123456");
		user.setFirstName("test FirstName A");
		user.setLastName("test LastName A");
		user.setEmail(rand + "test@test.com");
		user.setIp("127.0.0.1");
		user.setMobilePhone("12345678");
		user.setLanguageId(2);
		user.setUtcOffset(0);
		user.setActionSource(ActionSource.WEB);
		user.setUtcOffsetCreated("UTC+0");
		user.setCountryByPrefix(12);
		
		MarketingTracking marketingTracking = new MarketingTracking();
		marketingTracking.setCampaignId(1);
		marketingTracking.setQueryString("queryString test");
		user.setMarketingTracking(marketingTracking);
	
		try {
			userService.insertUser(user);
		} catch (Exception e) {
			logger.error("Problem with test insertUserStepA", e);
		} 		
	}
	
	@Test
	@Transactional
	public void update() {
		String rand = UUID.randomUUID().toString().substring(0,6);
		user.setId(user.getId());
		user.setPassword(rand);
		user.setCurrencyId(3);
		user.setFirstName(rand.substring(2));
		user.setLastName(rand.substring(3));
		user.setStreet("test");
		user.setZipCode("11111");
		user.setEmail("test@test.com");
		user.setIp("127.0.0.1");
		user.setMobilePhone("12345678");
		user.setGender(Gender.MALE);
		user.setStreetNo("1");
		user.setLanguageId(2);
		user.setUtcOffset(0);
		user.setCityName("aaaaa");
		user.setIsActive(1);
		user.setComments("Junit");
		user.setIsContactByEmail(1);
		user.setIsContactByPhone(1);
		user.setIsContactBySms(1);
		user.setIsAcceptedTerms(1);
		user.setLandLinePhone("111111111");
		user.setAoUserId(2189877);
		user.setClazz(Clazz.TESTER);
		
		user.setActionSource(ActionSource.WEB);
		user.setUtcOffsetCreated("UTC+0");
		user.setRegulated(false);
		user.setWriterId(3);
		user.setUserStep(new UserStep(Step.UPDATE.getId()));
		user.setCountryByPrefix(12);
		try {
			userService.update(user, new UserHistory(ActionSource.CRM, new NullWriter()));
		} catch (Exception e) {
			logger.error("Problem with userService.update ", e);
		}
	}
	
	@Test
	@Transactional
	public void updateStepB() {
		User user = new User();
		user.setId(1);
		user.setStreet("test Street step B");
		user.setZipCode("11111");
		user.setTimeBirthDate(new Date());
		user.setStreetNo("1");
		user.setCityName("test city step B");
		user.setCountryByUser(2);
		try {
			userService.updateStepB(user, new UserHistory(ActionSource.CRM, new NullWriter()));
		} catch (Exception e) {
			logger.error("Problem with updateStepB.", e);
		} 		
	}
	
	@Test
    public void getUsersByDatesExists() {
		LocalDateTime today = LocalDateTime.now();
		LocalDateTime yesterday = today.minusDays(200);
		logger.info("today: " + today);
		logger.info("yesterday: " + yesterday);
		UserFilters userFilters = new UserFilters();
		userFilters.setFromDate(yesterday);
		userFilters.setToDate(today);
		Assert.assertNotNull(userService.getUsersByDates(userFilters));		
	}
	
	@Test
	@Transactional
	public void updateStep() {
		User user = new User();
		user.setId(10103);
		user.setUserStep(new UserStep(Step.MISSING.getId()));
		userService.updateStep(user, new UserHistory(ActionSource.CRM, new NullWriter())); 		
	}
}
