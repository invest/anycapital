package capital.any;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import capital.any.base.enums.ActionSource;
import capital.any.base.enums.InvestmentStatusEnum;
import capital.any.base.enums.InvestmentTypeEnum;
import capital.any.communication.Response;
import capital.any.model.base.Investment;
import capital.any.model.base.InvestmentHistory;
import capital.any.model.base.InvestmentMarket;
import capital.any.model.base.InvestmentParamters;
import capital.any.model.base.InvestmentStatus;
import capital.any.model.base.InvestmentType;
import capital.any.model.base.Product;
import capital.any.model.base.SqlFilters;
import capital.any.service.base.investment.IInvestmentService;
import capital.any.service.base.product.IProductService;
import capital.any.service.base.user.IUserService;

/**
 * @author Eyal G
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
public class InvestmentsServiceTest {
	
	private static final Logger logger = LoggerFactory.getLogger(InvestmentsServiceTest.class);

	@Autowired
	private IInvestmentService investmentService;
	
	@Autowired
	private IProductService productService;
	
	@Autowired
	private IUserService userService;
	
	
	@Test
    public void getInvestments() {
		SqlFilters filters = new SqlFilters();
//		filters.setUserClassId(Clazz.TESTER.getToken());
		List<Investment> investments = investmentService.get(filters);
		logger.debug("investments " + investments);
		Assert.assertTrue(investments != null);
	}
	
	@Test
	@Transactional
    public void insertInvestments() throws Exception {
		
		Map<Long, Product> hm = productService.getProducts();
		long productId = 0;
		try {
			for (Product product : hm.values()) {
				productId = product.getId();
				break;
			}
		} catch (Exception e) {
			logger.debug("cant find product id for test", e);
		}
		
		ArrayList<InvestmentMarket> investmentMarkets = new ArrayList<InvestmentMarket>();
		
		InvestmentMarket investmentMarket = new InvestmentMarket();
		investmentMarket.setMarketId(1);
		investmentMarket.setPrice(55);
		
		investmentMarkets.add(investmentMarket);
		
		Investment investment = new Investment();
		investment.setInvestmentMarkets(investmentMarkets);
		investment.setOriginalAmount(1000000);
		investment.setInvestmentStatus((new InvestmentStatus(InvestmentStatusEnum.OPEN.getId())));
		investment.setInvestmentType(new InvestmentType(InvestmentTypeEnum.SUBSCRIPTION.getId()));
		investment.setProductId(productId);
		investment.setTimeCreated(new Date());
		investment.setUser(userService.getUserByEmail("junit.with.money@anyoption.com"));
		
		InvestmentHistory investmentHistory = new InvestmentHistory();
		investmentHistory.setActionSource(ActionSource.WEB);
		investment.setInvestmentHistory(investmentHistory);
		InvestmentParamters investmentParamters = new InvestmentParamters();
		investmentParamters.setInvestment(investment);
		investmentParamters.setSessionId("test session id");
		Response<Investment> response = investmentService.insertWithValidation(investmentParamters);
		logger.debug("investmentParamters " + investmentParamters + " ERROR " + response);
		Assert.assertNotNull(response);
	}
	
}
