package capital.any;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


import capital.any.service.base.productSimulation.IProductSimulationService;

/**
 * @author eranl
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
   
public class ProductSimulationServiceTest {

	@Autowired
	private IProductSimulationService productSimulationService;
	
	@Test
    public void getProductSimulations() {		 
		Assert.assertNotNull(productSimulationService.getProductSimulations());		
	}
		
}
