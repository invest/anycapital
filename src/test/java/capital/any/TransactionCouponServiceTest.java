package capital.any;

import java.util.Date;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import capital.any.model.base.Transaction;
import capital.any.model.base.Transaction.TransactionOperationEnum;
import capital.any.model.base.Transaction.TransactionPaymentTypeEnum;
import capital.any.model.base.Transaction.TransactionStatusEnum;
import capital.any.model.base.TransactionCoupon;
import capital.any.model.base.TransactionOperation;
import capital.any.model.base.TransactionPaymentType;
import capital.any.model.base.TransactionStatus;
import capital.any.model.base.User;
import capital.any.service.base.transaction.ITransactionService;
import capital.any.service.base.transactionCoupon.ITransactionCouponService;

/**
 * @author Eyal Goren
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
   
public class TransactionCouponServiceTest {
	
	@Autowired
	private ITransactionService transactionService;
	
	@Autowired
	private ITransactionCouponService transactionCouponService;
		
	@Test
	@Ignore //need product with coupon id
	@Transactional
	public void insertTransactionCoupon() {
		Transaction transaction = new Transaction();
		transaction.setUser(new User(10002));
		transaction.setAmount(10000);
		transaction.setStatus(new TransactionStatus(TransactionStatusEnum.FAILED.getId()));
		transaction.setIp("127.0.0.1");
		transaction.setComments("insert test");
		transaction.setOperation(new TransactionOperation(TransactionOperationEnum.CREDIT.getId()));
		transaction.setPaymentType(new TransactionPaymentType(TransactionPaymentTypeEnum.WIRE.getId()));
		transaction.setTimeCreated(new Date());
		transactionService.insertTransaction(transaction);
		
		TransactionCoupon transactionCoupon = new TransactionCoupon();
		transactionCoupon.setAmonut(100);
		transactionCoupon.setTransactionId(transaction.getId());
		transactionCouponService.insert(transactionCoupon);
	}
}
