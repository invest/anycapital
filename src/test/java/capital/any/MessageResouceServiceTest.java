package capital.any;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import capital.any.base.enums.ActionSource;
import capital.any.base.enums.LanguageEnum;
import capital.any.base.enums.MsgResTypeEnum;
import capital.any.model.base.MsgRes;
import capital.any.service.base.messageResource.IMessageResourceService;

/**
 * @author eranl
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
   
public class MessageResouceServiceTest {

	@Autowired
	private IMessageResourceService messageResourceService;
	
	//TODO may remove.
	@Test
    public void get() {		 
		Assert.assertNotNull(messageResourceService.get());		
	}
	
	@Test
    public void getMessageResourceServiceList() {	
		MsgRes msgRes = new MsgRes(ActionSource.CRM.getId(), MsgResTypeEnum.REGULAR.getId());
		Assert.assertNotNull(messageResourceService.get());
	}
	
	@Test
    public void getKeyExist() {		 
		MsgRes msgRes = new MsgRes(ActionSource.CRM.getId(), MsgResTypeEnum.REGULAR.getId());
		Assert.assertNotNull(messageResourceService.get()
				.get(ActionSource.CRM.getId())
				.get(LanguageEnum.EN.getId()).get("accept-terms"));		
	}
		
//	@Test
//	@Transactional
//	public void insertSuccess() {
//		MsgRes msgRes = new MsgRes();
//		msgRes.setKey("test6");
//		msgRes.setActionSourceId(ActionSource.CRM.getId());
//		List<MsgResLanguage> msgResLanguages = new ArrayList<MsgResLanguage>();
//		MsgResLanguage msgResLanguage1 = new MsgResLanguage();
//		msgResLanguage1.setMsgResLanguageId(LanguageEnum.EN.getId());
//		msgResLanguage1.setValue("aaaa");
//		msgResLanguages.add(msgResLanguage1);
//		messageResourceService.insertMessageResources(msgRes, msgResLanguages);
//	}
//	
//	
//	
//	@Test(expected = DuplicateKeyException.class)
//	@Transactional
//	public void insertFail() {
//		MsgRes msgRes = new MsgRes();
//		msgRes.setKey("test5");
//		msgRes.setActionSourceId(ActionSource.CRM.getId());
//		List<MsgResLanguage> msgResLanguages = new ArrayList<MsgResLanguage>();
//		MsgResLanguage msgResLanguage1 = new MsgResLanguage();
//		MsgResLanguage msgResLanguage2 = new MsgResLanguage();
//		MsgResLanguage msgResLanguage3 = new MsgResLanguage();
//		msgResLanguage1.setMsgResLanguageId(LanguageEnum.EN.getId());
//		msgResLanguage1.setValue("aaaa");
//		msgResLanguage2.setMsgResLanguageId(LanguageEnum.EN.getId());
//		msgResLanguage2.setValue("bbbb");
//		msgResLanguage3.setMsgResLanguageId(LanguageEnum.EN.getId());
//		msgResLanguage3.setValue("cccc");	
//		msgResLanguages.add(msgResLanguage1);
//		msgResLanguages.add(msgResLanguage2);
//		msgResLanguages.add(msgResLanguage3);
//		messageResourceService.insertMessageResources(msgRes, msgResLanguages);
//	}
	
		
}
