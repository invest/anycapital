package capital.any;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import capital.any.model.base.SMS;
import capital.any.model.base.SMS.Status;
import capital.any.model.base.SMS.Type;
import capital.any.service.base.sms.ISMSService;

/**
 * @author eran.levy
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
   
public class SMSServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(SMSServiceTest.class);
	@Autowired
	private ISMSService SMSService;
	
	@Test
	@Transactional
	public void insertSMS() {
		SMS sms = new SMS();
		sms.setPhone("972526747658");
		sms.setMessage("AC test SMS 5");
		SMSService.insert(sms); 		
	}
	
	@Test
	@Transactional
	public void updateSMS() {
//		SMS sms = new SMS();
//		sms.setType(Type.FREE_TEXT);
//		sms.setPhone("1234567");
//		sms.setSender("anycapital");
//		sms.setSenderNumber("1234567");
//		sms.setStatus(Status.QUEUED);
//		sms.setMessage("test SMS");
//		SMSService.insert(sms); 		
	}
	
}
