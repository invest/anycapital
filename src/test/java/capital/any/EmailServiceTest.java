package capital.any;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.base.enums.ActionSource;
import capital.any.base.enums.EmailAction;
import capital.any.base.enums.EmailParameters;
import capital.any.base.enums.EmailType;
import capital.any.model.base.Email;
import capital.any.model.base.MailInfo;
import capital.any.service.base.email.IEmailService;

/**
 * @author Eyal Goren
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
   
public class EmailServiceTest {
	
	private static final Logger logger = LoggerFactory.getLogger(EmailServiceTest.class);

	@Autowired
	private IEmailService emailService;
	@Autowired
	private ObjectMapper mapper;
	
	@Test
	@Transactional
    public void insert() throws IOException {		
		Map<String, String> map = new HashMap<String, String>();
		map.put(EmailParameters.PARAM_FIRST_NAME.getParam(), "test email");
		/* Mail Info */
		MailInfo mailInfo = new MailInfo();
		mailInfo.setFrom("support.test@anyoption.com");
		mailInfo.setTo("eyal.o@invest.com");
		mailInfo.setSubject("Test AC");
		mailInfo.setTemplateId("mailTemplateTest");
		mailInfo.setMap(map);
		mailInfo.setEmailActionId(EmailAction.CHANGE_PRODUCT_STATUS.getId());
		mailInfo.setTypeId(EmailType.NO_TEMPLATE_TEXT.getId());
		mailInfo.setLanguageId(2);
		mailInfo.setText("Test");
		String emailInfo = mapper.writeValueAsString(mailInfo);
		
		Email email = new Email();
		email.setActionSource(ActionSource.WEB);
		email.setEmailInfo(emailInfo);
		emailService.insert(email);
		Assert.assertNotNull(email);		
	}
		
}
