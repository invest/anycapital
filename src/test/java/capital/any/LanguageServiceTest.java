package capital.any;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


import capital.any.service.base.language.ILanguageService;

/**
 * @author eranl
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
   
public class LanguageServiceTest {

	@Autowired
	private ILanguageService languageService;
	
	@Test
    public void getLanguageList() {		
		Assert.assertNotNull(languageService.get());		
	}
	
	@Test
    public void getLanguageByIdExist() {		 
		Assert.assertNotNull(languageService.get().get(2));		
	}
	
	@Test
    public void getLanguageByIdNotExist() {		 
		Assert.assertNull(languageService.get().get(0));		
	}
}
