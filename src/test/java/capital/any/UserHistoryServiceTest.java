package capital.any;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import capital.any.base.enums.ActionSource;
import capital.any.model.base.NullWriter;
import capital.any.model.base.User;
import capital.any.model.base.UserHistory;
import capital.any.model.base.UserHistoryAction;
import capital.any.service.base.user.IUserService;
import capital.any.service.base.userHistory.IUserHistoryService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class) 
public class UserHistoryServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(UserHistoryServiceTest.class);
	
	@Autowired
	private IUserHistoryService userHistoryService;
	@Autowired
	private IUserService userService;
	
	private User user;
	
	@Value("${user.exist}")
	private String userExist;
	
	@Before
	public void init() {
		user = userService.getUserByEmail(userExist);	
	}
	
	@Test
	@Transactional
	public void insert() {
		UserHistory userHistory = new UserHistory();
		userHistory.setUserId(user.getId());
		userHistory.setUserHistoryAction(new UserHistoryAction(capital.any.base.enums.UserHistoryAction.UPDATE_STEP.getId()));
		userHistory.setActionParamters("{\"STEP\":5}");
		userHistory.setActionSource(ActionSource.WEB);
		userHistory.setWriter(new NullWriter());
		userHistory.setServerName("Test");
		userHistoryService.insert(userHistory);
	}
	
}