package capital.any;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import capital.any.model.base.InvestmentMarket;
import capital.any.service.base.investmentMarkets.IInvestmentMarketService;

/**
 * @author Eyal G
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
public class InvestmentMarketServiceTest {
	
	private static final Logger logger = LoggerFactory.getLogger(InvestmentMarketServiceTest.class);

	@Autowired
	private IInvestmentMarketService investmentMarketService;
	
	@Test
	@Transactional
	@Ignore
    public void insertInvestments() {
		ArrayList<InvestmentMarket> investmentMarkets = new ArrayList<InvestmentMarket>();
		
		InvestmentMarket investmentMarket = new InvestmentMarket();
		investmentMarket.setInvestmentId(14);
		investmentMarket.setMarketId(1);
		investmentMarket.setPrice(55);
		
		InvestmentMarket investmentMarket1 = new InvestmentMarket();
		investmentMarket1.setInvestmentId(14);
		investmentMarket1.setMarketId(2);
		investmentMarket1.setPrice(65);
		
		investmentMarkets.add(investmentMarket);
		investmentMarkets.add(investmentMarket1);
		investmentMarketService.insert(investmentMarkets);
		logger.debug("investment Markets " + investmentMarkets);
	}
	
	@Test
    public void getInvestments() {
		List<InvestmentMarket> investments = investmentMarketService.get(14);
		logger.debug("investments " + investments);
		Assert.assertTrue(investments != null);
	}
	
}
