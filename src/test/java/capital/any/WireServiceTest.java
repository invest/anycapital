package capital.any;

import java.util.Date;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.Repeat;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import capital.any.model.base.Transaction;
import capital.any.model.base.TransactionOperation;
import capital.any.model.base.TransactionPaymentType;
import capital.any.model.base.Transaction.TransactionOperationEnum;
import capital.any.model.base.Transaction.TransactionPaymentTypeEnum;
import capital.any.model.base.Transaction.TransactionStatusEnum;
import capital.any.model.base.TransactionStatus;
import capital.any.model.base.User;
import capital.any.model.base.Wire;
import capital.any.service.base.transaction.ITransactionService;
import capital.any.service.base.wire.IWireService;

/**
 * @author eranl
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)      
public class WireServiceTest {

	@Autowired
	private IWireService wireService;
	
	@Autowired
	private ITransactionService transactionService;
					
	@Test
	@Transactional
	//@Repeat(200)
	public void insertWire() {		
		Transaction transaction = new Transaction();
		transaction.setUser(new User(10002));
		transaction.setAmount(10000);
		transaction.setStatus(new TransactionStatus(TransactionStatusEnum.PENDING.getId()));
		transaction.setIp("127.0.0.1");
		transaction.setComments("insert test");
		transaction.setOperation(new TransactionOperation(TransactionOperationEnum.CREDIT.getId()));
		transaction.setPaymentType(new TransactionPaymentType(TransactionPaymentTypeEnum.WIRE.getId()));
		transaction.setTimeCreated(new Date());
		transactionService.insertTransaction(transaction); 								
		
		Wire wire = new Wire();
		wire.setTransactionId(transaction.getId());
		wire.setBranchAddress("test");
		wire.setBankName("test");
		wire.setIban("test");
		wire.setSwift("test");
		wire.setAccountInfo("test");
		wire.setAccountNum(123456);
		wire.setBranchNumber(1);
		wire.setBeneficiaryName("test");
		//wire.setBankFeeAmount(bankFeeAmount);
		wireService.insertWire(wire); 		
	}
}
