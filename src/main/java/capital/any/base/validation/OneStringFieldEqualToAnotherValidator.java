package capital.any.base.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanWrapperImpl;

/**
 * @author eran.levy
 *
 */
public class OneStringFieldEqualToAnotherValidator implements ConstraintValidator<OneStringFieldEqualToAnother, Object> {

	private static final Logger logger = LoggerFactory.getLogger(OneStringFieldEqualToAnotherValidator.class);
	private String firstFieldName;
	private String secondFieldName;
	
	@Override
	public void initialize(final OneStringFieldEqualToAnother constraintAnnotation) {
		firstFieldName = constraintAnnotation.first();
		secondFieldName = constraintAnnotation.second();
	}
	
	@Override
	public boolean isValid(final Object value, final ConstraintValidatorContext context) {
		boolean result = false;
		try {
			BeanWrapperImpl wrapper = new BeanWrapperImpl(value);
			final Object firstObj = wrapper.getPropertyValue(firstFieldName);
			final Object secondObj = wrapper.getPropertyValue(secondFieldName);
			String firstStr = (String) firstObj;
			String secondStr = (String) secondObj;			
			return (firstStr != null && secondStr != null) && (firstStr.equals(secondStr));				
		} catch (final Exception ignore) {
			logger.error("cannot validate fields", ignore);
		}
		return result;
	}
}
