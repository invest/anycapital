package capital.any.base.validation.withdraw;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import capital.any.base.rule.WithdrawRule;

/**
 * @author LioR SoLoMoN
 *
 */
@Service
public class WithdrawValidationService implements IWithdrawValidationService {
	private List<WithdrawRule> rules;
	@Autowired
	private BalanceWithdrawValidationRule balanceWithdrawValidatationRule;
	@Autowired
	private OpenWithdrawValidationRule openWithdrawValidatationRule;

	@PostConstruct
	public void init() {
		rules = new ArrayList<WithdrawRule>();
		rules.add(balanceWithdrawValidatationRule);
		rules.add(openWithdrawValidatationRule);
	}
	
	public List<WithdrawRule> getRules() {
		return rules;
	}
}
