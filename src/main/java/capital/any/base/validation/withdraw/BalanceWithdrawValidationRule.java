package capital.any.base.validation.withdraw;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import capital.any.base.enums.TransactionRejectType;
import capital.any.base.rule.WithdrawRule;
import capital.any.communication.Error;
import capital.any.communication.Message;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.Payment;
import capital.any.service.base.IBalanceService;

/**
 * @author LioR SoLoMoN
 *
 */
@Service
public class BalanceWithdrawValidationRule implements WithdrawRule {
	private static final Logger logger = LoggerFactory.getLogger(BalanceWithdrawValidationRule.class);
	private static final TransactionRejectType REJECT_TYPE = TransactionRejectType.BALANCE;
	
	@Autowired
	private IBalanceService balanceService;

	@Override
	public Response<?> inspect(Payment<?> payment) {
		logger.debug("BalanceWithdrawValidationRule validate");
		Response<?> response = new Response<Object>(null, ResponseCode.OK);
		if (payment.getPaymentDetails().getAmount() > balanceService.getBalance(payment.getUser().getId())) {
			response.setResponseCode(ResponseCode.WITHDRAW_BALANCE_VALIDATION);
			List<Message> msg = new ArrayList<Message>();
			msg.add(new Message("payment.withdraw.balance.validation", ""));
			response.setError(new Error("withdraw", msg));
		}
		return response;
	}

	@Override
	public TransactionRejectType getRejectType() {
		return REJECT_TYPE;
	}
}
