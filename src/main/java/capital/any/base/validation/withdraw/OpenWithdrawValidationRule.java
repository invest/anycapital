package capital.any.base.validation.withdraw;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import capital.any.base.enums.TransactionRejectType;
import capital.any.base.rule.WithdrawRule;
import capital.any.communication.Error;
import capital.any.communication.Message;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.Payment;
import capital.any.service.base.transaction.ITransactionService;

/**
 * @author LioR SoLoMoN
 *
 */
@Service
public class OpenWithdrawValidationRule implements WithdrawRule {
	private static final TransactionRejectType REJECT_TYPE = TransactionRejectType.OPEN_WITHDRAW;;
	@Autowired
	private ITransactionService transactionService;

	@Override
	public Response<?> inspect(Payment<?> payment) {
		Response<?> response = new Response<Object>(null, ResponseCode.OK);
		if (transactionService.isUserHaveOpenWithdraw(payment.getUser().getId())) {
			response.setResponseCode(ResponseCode.WITHDRAW_OPEN_VALIDATION);
			List<Message> msg = new ArrayList<Message>();
			msg.add(new Message("payment.withdraw.open.validation", ""));
			response.setError(new Error("withdraw", msg));
		}
		return response;
	}
	
	@Override
	public TransactionRejectType getRejectType() {
		return REJECT_TYPE;
	}
}
