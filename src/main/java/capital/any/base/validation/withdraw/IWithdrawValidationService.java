package capital.any.base.validation.withdraw;

import java.util.List;
import capital.any.base.rule.WithdrawRule;

/**
 * @author LioR SoLoMoN
 *
 */
public interface IWithdrawValidationService {
	/**
	 * @return
	 */
	public List<WithdrawRule> getRules();
}
