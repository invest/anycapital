package capital.any.base.security;

import java.net.URLEncoder;
import java.security.Key;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Service;

import capital.any.base.enums.DBParameter;
import capital.any.model.base.GoogleShortURLRequest;
import capital.any.model.base.GoogleShortURLResponse;
import capital.any.model.base.HttpRequest;
import capital.any.model.base.User;
import capital.any.model.base.UserToken;
import capital.any.service.IHttpClientService;
import capital.any.service.base.application.IApplicationService;
import capital.any.service.base.dbParameter.IDBParameterService;
import capital.any.service.base.language.ILanguageService;
import capital.any.service.base.userToken.IUserTokenService;

/**
 * @author LioR SoLoMoN
 *
 */
@Service
public class SecureAlgorithmsService implements ISecureAlgorithmsService {
	private static final Logger logger = LoggerFactory.getLogger(SecureAlgorithmsService.class);
	
	@Autowired
	private IDBParameterService dbParameterService;
	@Autowired
	private IUserTokenService userTokenService;
	@Autowired
	private IApplicationService applicationService;
	@Autowired
	private ILanguageService languageService;
	@Autowired
	private IHttpClientService httpClientService;
	
	/**
	 * convert byte array to Hex String, second implementation option
	 * @param data
	 * @return
	 */
    private String toHexString(byte[] data) {
        StringBuffer sb = new StringBuffer();
        String s = null;
        for (int i = 0; i < data.length; i++) {
            s = Integer.toHexString(data[i] & 0x000000FF);
            if (s.length() == 1) {
                sb.append("0");
            }
            sb.append(s);
        }
        return sb.toString();
    }
    
    /**
     * use UTF-8.
     * 
     * @param algorithm
     * @param text
     * @return
     * @throws Exception
     */
    private String createHash(String algorithm, String text) throws Exception {
		MessageDigest md = MessageDigest.getInstance(algorithm);
		md.update(text.getBytes("UTF-8"), 0, text.length());
		return toHexString(md.digest());
    }

	@Override
	public String SHA1(String text) throws Exception {
		return createHash("SHA-1", text);
	}

	@Override
	public String SHA256(String text) throws Exception {
		return createHash("SHA-256", text);
	}

	@Override
	public String SHA512(String text) throws Exception {
		return createHash("SHA-512", text);
	}

	@Override
	public String MD5(String text) throws Exception {
		return createHash("MD5", text);
	}

	@Override
	public int LShashStyle(String text) throws Exception {
		char[] chars = text.toCharArray();
		int hashCode = 0;
		
		for (char c : chars) {
			hashCode <<= 7;
		    hashCode |= c & 0x7F;
		    hashCode = (hashCode << 1) | (hashCode >> (32 - 1));
		    hashCode ^= c;
		    hashCode = 31 * hashCode + c;
		}
		return -1*hashCode;
	}
	
	@Override
    public String encryptAES(String data) throws Exception {
		logger.info("About to encrypt (AES) the data: " + data);
		/* key for secure */
		String keyValue = dbParameterService.get(DBParameter.KEY_SECURE_AES.getId()).getStringValue();
		/* encrypt */
        String encryptedValue = encryptAES(data, keyValue);
        logger.info("data after encrypt (AES): " + encryptedValue);
        return encryptedValue;
    }
	
    public String encryptAES(String data, String keyValue) throws Exception {
    	Key key = new SecretKeySpec(keyValue.getBytes(), "AES");
        Cipher c = Cipher.getInstance("AES");
        c.init(Cipher.ENCRYPT_MODE, key);
        byte[] encVal = c.doFinal(data.getBytes());
        String encryptedValue = new String(Base64.encode(encVal));
        return encryptedValue;
    }
    
	@Override
    public String decryptAES(String data) throws Exception {
		logger.info("About to decrypt (AES) the data: " + data);
		/* key for secure */
		String keyValue = dbParameterService.get(DBParameter.KEY_SECURE_AES.getId()).getStringValue();
		/* decrypt */
		Key key = new SecretKeySpec(keyValue.getBytes(), "AES");
        Cipher c = Cipher.getInstance("AES");
        c.init(Cipher.DECRYPT_MODE, key);
        byte[] decordedValue = Base64.decode(data.getBytes());
        byte[] decValue = c.doFinal(decordedValue);
        String decryptedValue = new String(decValue);
        logger.info("data after decrypt (AES): " + decryptedValue);
        return decryptedValue;
    }
	
	@Override
    public String encryptAESUrl(String data) throws Exception {
        String encryptedValue = encryptAES(data);
        encryptedValue = URLEncoder.encode(encryptedValue, "UTF-8");
        logger.info("Data after encode : " + encryptedValue);
        return encryptedValue;
    }
	
	@Override
    public List<List<String>> encryptAESUrl(List<List<String>> twoDimensionalList, List<String> columnsForEncrypt, String csvColumnEncrypted) throws Exception {
		logger.info("About to encrypt by AES");
		/* key for secure */
		String keyValue = dbParameterService.get(DBParameter.KEY_SECURE_AES.getId()).getStringValue();
		/* encrypt */
		List<List<String>> twoDimensionalListEncrypted = new ArrayList<List<String>>();
		List<Integer> columnsForEncryptNum = new ArrayList<Integer>();
		for(int row = 0; row < twoDimensionalList.size(); row++) {
			List<String> listEncrypted = new ArrayList<String>();
			List<String> list = twoDimensionalList.get(row);
			listEncrypted.addAll(list);
			if (row == 0) {
				for (int column = 0; column < list.size(); column++) {
					if (columnsForEncrypt.contains(list.get(column))) {
						listEncrypted.add(csvColumnEncrypted + list.get(column).substring(0, 1));
						columnsForEncryptNum.add(column);
					}
				}
			} else {
				for (int column = 0; column < list.size(); column++) {
					if (columnsForEncryptNum.contains(column)) {
						String encryptedValue = encryptAES(list.get(column), keyValue);
				        encryptedValue = URLEncoder.encode(encryptedValue, "UTF-8");
				        listEncrypted.add(encryptedValue);
					}
				}
			}
			twoDimensionalListEncrypted.add(listEncrypted);
		}
		return twoDimensionalListEncrypted;
    }
	
	@Override
	public GoogleShortURLResponse generateTokenAndLink(User user) throws Exception {
		String token = userTokenService.generateToken(user);
		userTokenService.insert(new UserToken(token, user.getId()));
		//TODO constants...
		StringBuilder link = new StringBuilder(applicationService.getDomainURL())
				.append("listener/forgotPassword")
				.append("?token=")
				.append(encryptAESUrl(token))
				.append("&userId=")
				.append(encryptAESUrl(String.valueOf(user.getId())))
				.append("&lang=")
				.append(languageService.get().get(user.getLanguageId()).getCode());

		GoogleShortURLResponse generateGoogleShortURL = 
				generateGoogleShortURL(new GoogleShortURLRequest(link.toString()));
		return generateGoogleShortURL;
	}
	
	public GoogleShortURLResponse generateGoogleShortURL(GoogleShortURLRequest googleShortURLRequest) throws Exception {
		String postURL = dbParameterService.get(DBParameter.GOOGLE_SHORT_URL_API.getId()).getStringValue();
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		HttpRequest<GoogleShortURLResponse> request = 
		new HttpRequest<GoogleShortURLResponse>(postURL, 
				googleShortURLRequest, 
				GoogleShortURLResponse.class, 
				headers);
		request.setJson(true);
		GoogleShortURLResponse response = httpClientService.doPost(request);
		return response;
	}
}

