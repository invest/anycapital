package capital.any.base.security;

import java.util.List;

import capital.any.model.base.GoogleShortURLResponse;
import capital.any.model.base.User;

/**
 * @author LioR SoLoMoN
 *
 */
public interface ISecureAlgorithmsService {
	
	 /**
	  * 
     * SHA is a cryptographic message digest algorithm similar to MD5. SHA-1 hash considered to be one
     * of the most secure hashing functions, producing a 160-bit digest (40 hex numbers) from any data
     * with a maximum size of 264 bits
     *
	 * @param text
	 * @return
	 * @throws Exception
	 */
	public String SHA1(String text) throws Exception;
	
	/**
     * SHA-256 and SHA-512 are novel hash functions computed with 32-bit and 64-bit words, respectively. 
     * They use different shift amounts and additive constants, 
     * but their structures are otherwise virtually identical, differing only in the number of rounds.
     * 
	 * @param text
	 * @return
	 * @throws Exception
	 */
	public String SHA256(String text) throws Exception;
	
	
	/**
	 * SHA-256 and SHA-512 are novel hash functions computed with 32-bit and 64-bit words, respectively. 
	 * They use different shift amounts and additive constants, 
	 * but their structures are otherwise virtually identical, differing only in the number of rounds.
	 * 
	 * @param text
	 * @return
	 * @throws Exception
	 */
	public String SHA512(String text) throws Exception;
	
	/**
	 * The MD5 message-digest algorithm is a widely used cryptographic hash function producing a 128-bit (16-byte) hash value,
	 * typically expressed in text format as a 32 digit hexadecimal number. 
	 * 
	 * @param text
	 * @return
	 * @throws Exception
	 */
	public String MD5(String text) throws Exception;
	
	/**
	 * 
	 * :)
	 * 
	 * @param text
	 * @return
	 * @throws Exception
	 */
	public int LShashStyle(String text) throws Exception;
	
	/**
	 * @param data
	 * @param keyValue
	 * @return
	 * @throws Exception
	 */
	public String decryptAES(String data) throws Exception;

	/**
	 * @param data
	 * @param keyValue
	 * @return
	 * @throws Exception
	 */
	public String encryptAES(String data) throws Exception;
	
	/**
	 * @param data
	 * @param keyValue
	 * @return String
	 * @throws Exception
	 */
	public String encryptAESUrl(String data) throws Exception;
	
	/**
	 * @param twoDimensionalList
	 * @return
	 * @throws Exception
	 */
	public List<List<String>> encryptAESUrl(List<List<String>> twoDimensionalList, List<String> columnsForEncrypt, String csvColumnEncrypted) throws Exception;

	/**
	 * Generate token and generate link
	 * @param user
	 * @return link as String
	 * @throws Exception
	 */
	GoogleShortURLResponse generateTokenAndLink(User user) throws Exception;
	
}
