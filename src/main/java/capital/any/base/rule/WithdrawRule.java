package capital.any.base.rule;

import capital.any.base.enums.TransactionRejectType;
import capital.any.communication.Response;
import capital.any.model.base.Payment;

/**
 * @author LioR SoLoMoN
 *
 */
public interface WithdrawRule {
	
	/**
	 * @param payment
	 * @return the suitable response code 
	 */
	Response<?> inspect(Payment<?> payment);
	
	/**
	 * @return
	 */
	TransactionRejectType getRejectType();
}
