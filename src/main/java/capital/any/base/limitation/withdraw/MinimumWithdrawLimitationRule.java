package capital.any.base.limitation.withdraw;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.base.enums.DBParameter;
import capital.any.base.enums.TransactionRejectType;
import capital.any.base.rule.WithdrawRule;
import capital.any.communication.Error;
import capital.any.communication.Message;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.Payment;
import capital.any.service.base.dbParameter.IDBParameterService;

/**
 * @author LioR SoLoMoN
 *
 */
@Service
public class MinimumWithdrawLimitationRule implements WithdrawRule {
	private static final Logger logger = LoggerFactory.getLogger(MinimumFeeWithdrawLimitationRule.class);
	private static final TransactionRejectType REJECT_TYPE = TransactionRejectType.MINIMUM_AMOUNT;
	
	@Autowired
	private IDBParameterService dbParameterService;
	
	@Override
	public Response<?> inspect(Payment<?> payment) {
		logger.debug("MinimumWithdrawLimitationRule validate");
		Response<?> response = new Response<Object>(null, ResponseCode.OK);
		Long minimumAmount = dbParameterService.get(DBParameter.MINIMUM_WITHDRAW_AMOUNT.getId()).getNumValue();
		if (payment.getPaymentDetails().getAmount() <
			minimumAmount ) {
			response.setResponseCode(ResponseCode.WITHDRAW_MINIMUM_AMOUNT_LIMITATION);
			List<Message> msg = new ArrayList<Message>();
			msg.add(new Message("payment.withdraw.minimum.amount.limitation", ""));
			response.setError(new Error("withdraw", msg));
			payment.getPaymentResponse().setMinimumAmount(minimumAmount);
		}	
		return response;
	}

	@Override
	public TransactionRejectType getRejectType() {
		return REJECT_TYPE;
	}
}
