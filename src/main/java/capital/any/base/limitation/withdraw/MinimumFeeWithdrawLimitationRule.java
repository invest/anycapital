package capital.any.base.limitation.withdraw;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.base.enums.DBParameter;
import capital.any.base.enums.TransactionRejectType;
import capital.any.base.rule.WithdrawRule;
import capital.any.communication.Error;
import capital.any.communication.Message;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.Payment;
import capital.any.service.base.dbParameter.IDBParameterService;

/**
 * @author LioR SoLoMoN
 *
 */
@Service
public class MinimumFeeWithdrawLimitationRule implements WithdrawRule {
	private static final Logger logger = LoggerFactory.getLogger(MinimumFeeWithdrawLimitationRule.class);
	private static final TransactionRejectType REJECT_TYPE = TransactionRejectType.MINIMUM_FEE;
	
	@Autowired
	private IDBParameterService dbParameterService;

	@Override
	public Response<?> inspect(Payment<?> payment) {
		logger.debug("MinimumFeeWithdrawLimitationRule validate");
		Response<?> response = new Response<Object>(null, ResponseCode.OK);
		Long minimumFeeAmount = dbParameterService.get(DBParameter.WITHDRAW_FEE_AMOUNT.getId()).getNumValue();
		if (payment.getPaymentDetails().getAmount() <
			minimumFeeAmount ) {
			response.setResponseCode(ResponseCode.WITHDRAW_FEE_AMOUNT_LIMITATION);
			List<Message> msg = new ArrayList<Message>();
			msg.add(new Message("payment.withdraw.minimum.fee.limitation", ""));
			response.setError(new Error("withdraw", msg));
			payment.getPaymentResponse().setMinimumFeeAmount(minimumFeeAmount);
		}
		
		return response;
	}

	@Override
	public TransactionRejectType getRejectType() {
		return REJECT_TYPE; 
	}
}
