package capital.any.base.limitation.withdraw;

import java.util.List;
import capital.any.base.rule.WithdrawRule;

/**
 * @author LioR SoLoMoN
 *
 */
public interface IWithdrawLimitationService {
	/**
	 * @return
	 */
	public List<WithdrawRule> getRules();
}
