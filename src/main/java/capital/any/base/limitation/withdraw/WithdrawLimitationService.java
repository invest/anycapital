package capital.any.base.limitation.withdraw;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import capital.any.base.rule.WithdrawRule;

/**
 * @author LioR SoLoMoN
 *
 */
@Service
public class WithdrawLimitationService implements IWithdrawLimitationService {
	private List<WithdrawRule> rules;
	@Autowired
	private MinimumFeeWithdrawLimitationRule minimumFeeWithdrawLimitationRule;
	@Autowired
	private MinimumWithdrawLimitationRule minimumWithdrawLimitationRule;

	@PostConstruct
	public void init() {
		rules = new ArrayList<WithdrawRule>();
		rules.add(minimumFeeWithdrawLimitationRule);
		rules.add(minimumWithdrawLimitationRule);
	}
	
	public List<WithdrawRule> getRules() {
		return rules;
	}
}
