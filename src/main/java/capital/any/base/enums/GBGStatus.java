package capital.any.base.enums;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @author LioR SoLoMoN
 *
 */
@JsonFormat(shape = JsonFormat.Shape.NUMBER)
public enum GBGStatus {
	/**
	 * {@code 1}
	 */
	PASS(1),
	/**
	 * {@code 2}
	 */
	FAIL(2),
	/**
	 * {@code 3}
	 */
	REFER(3)
	
	;
	
	private int id;
	
	/**
	 * @param id
	 */
	private GBGStatus(int id) {
		this.id = id;
	}

	private static final Map<Integer, GBGStatus> ID_STATUS_MAP = new HashMap<Integer, GBGStatus>(GBGStatus.values().length);
	static {
		for (GBGStatus s : GBGStatus.values()) {
			ID_STATUS_MAP.put(s.getId(), s);
		}
	}
	
	/**
	 * @param code
	 * @return
	 */
	public static GBGStatus get(int id) {
		GBGStatus s = ID_STATUS_MAP.get(id);
		if (s != null) {
			return s;
		} else {
			throw new IllegalArgumentException("No GBG Status with id: " + id);
		}
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
}
