package capital.any.base.enums;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Eyal G
 *
 */
public enum EmailParameters {
	
	PARAM_FIRST_NAME("[%firstName%]"),
	PARAM_LAST_NAME("[%lastName%]"),
	RESET_LINK("[%resetLink%]"),
	USER_NAME("[%userName%]"),
	PARAM_CONTENT("[%content%]"),
	USER_COUNTRY_BY_PREFIX("[%countryPrefix%]"),
	SUPPORT_EMAIL("[%supportEmail%]"),
	SUPPORT_PHONE("[%supportPhone%]"),
	DOCUMENTS_EMAIL("[%docEmail%]"),
	PRODUCT_NAME("[%productName%]"),
	ASSET("[%asset%]"),
	PRODUCT_ID("[%productID%]"),
	USER_CURRENCY("[%userCurrency%]"),
	INV_AMOUNT("[%invAmount%]"),
	SUBSCRIPTION_ENDDATE("[%SubscriptionEndDate%]"),
	INV_ID("[%investmentID%]"),
	AMOUNT("[%amount%]");
	
	
	private static final Logger logger = LoggerFactory.getLogger(EmailParameters.class);
	
	private String param;
	
	private EmailParameters(String param) {
		this.param = param;
	}

	/**
	 * @return the param
	 */
	public String getParam() {
		return param;
	}

	/**
	 * @param param the param to set
	 */
	public void setParam(String param) {
		this.param = param;
	}
}
