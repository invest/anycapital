package capital.any.base.enums;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 
 * @author eyal.ohana
 *
 */
@JsonFormat(shape = JsonFormat.Shape.NUMBER)
public enum UserHistoryAction {
	INSERT_USER(1),
	UPDATE_ADDITIONAL_INFO(2),
	UPDATE_STEP(3),
	UPDATE_USER(4),
	CHANGE_PASSWORD(5),
	ACCEPT_TERMS(6);
	
	private static final Logger logger = LoggerFactory.getLogger(UserHistoryAction.class);
	private static final Map<Integer, UserHistoryAction> ID_USER_HISTORY_ACTION_MAP = new HashMap<Integer, UserHistoryAction>(UserHistoryAction.values().length);
	static {
		for (UserHistoryAction userHistoryAction : UserHistoryAction.values()) {
			ID_USER_HISTORY_ACTION_MAP.put(userHistoryAction.getId(), userHistoryAction);
		}
	}
	
	private int id;

	private UserHistoryAction(int id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	@JsonValue
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @param id
	 * @return InvestmentType by a given id
	 */
	@JsonCreator 
    public static UserHistoryAction get(String id) {
		UserHistoryAction userHistoryAction = null;
		try {
			userHistoryAction = ID_USER_HISTORY_ACTION_MAP.get(Integer.valueOf(id));
	        if (userHistoryAction == null) {
	            throw new IllegalArgumentException(id + " has no corresponding value for user History Action");
	        }
		} catch (NumberFormatException ne) {
			logger.error("ERROR! user History Action; send a number!");
			throw new IllegalArgumentException(id + " has no corresponding value for user History Action");
		}
		
        return userHistoryAction;
    }
	
	public static UserHistoryAction get(int id) {
		UserHistoryAction userHistoryAction = ID_USER_HISTORY_ACTION_MAP.get(id);
		if (userHistoryAction == null) {
			throw new IllegalArgumentException("No user History Action with id: " + id);
		} 
		return userHistoryAction;
	}
}
