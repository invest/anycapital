package capital.any.base.enums;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author Eyal G
 *
 */
@JsonFormat(shape = JsonFormat.Shape.NUMBER)
public enum Table {
	INVESTMENTS(1),
	TRANSACTIONS(2),
	USERS(3),
	SMS(4);
	
	private static final Logger logger = LoggerFactory.getLogger(Table.class);
	private static final Map<Integer, Table> ID_TABLE_MAP = new HashMap<Integer, Table>(Table.values().length);
	static {
		for (Table table : Table.values()) {
			ID_TABLE_MAP.put(table.getId(), table);
		}
	}

	private int id;

	private Table(int id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	@JsonValue
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @param id
	 * @return InvestmentStatus by a given id
	 */
	@JsonCreator 
    public static Table get(String id) {
		Table table = null;
		try {
			table = ID_TABLE_MAP.get(Integer.valueOf(id));
	        if (table == null) {
	            throw new IllegalArgumentException(id + " has no corresponding value for table");
	        }
		} catch (NumberFormatException ne) {
			logger.error("ERROR! table; send a number!");
			throw new IllegalArgumentException(id + " has no corresponding value for table");
		}
		
        return table;
    }
	
	public static Table get(int id) {
		Table table = ID_TABLE_MAP.get(id);
		if (table == null) {
			throw new IllegalArgumentException("No table with id: " + id);
		} 
		return table;
	}
}
