package capital.any.base.enums;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @author eranl
 *
 */
@JsonFormat(shape = JsonFormat.Shape.NUMBER)
public enum LanguageEnum {
	EN(2, "en"), DE(1, "de");
	
	private static final Map<Integer, LanguageEnum> ID_LANGUAGE_ENUM_MAP = new HashMap<Integer, LanguageEnum>(LanguageEnum.values().length);
	static {
		for (LanguageEnum g : LanguageEnum.values()) {
			ID_LANGUAGE_ENUM_MAP.put(g.getId(), g);
		}
	}

	private int id;
	private String locale;
	
	public static LanguageEnum get(int id) {
		LanguageEnum languageEnum = ID_LANGUAGE_ENUM_MAP.get(id);
		if (languageEnum == null) {
			throw new IllegalArgumentException("No languageEnum with id: " + id);
		} 
		return languageEnum;
	}
	
	/**
	 * @param id
	 * @param locale
	 */
	private LanguageEnum(int id, String locale) {
		this.setId(id);
		this.setLocale(locale);
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the locale
	 */
	public String getLocale() {
		return locale;
	}

	/**
	 * @param locale the locale to set
	 */
	public void setLocale(String locale) {
		this.locale = locale;
	}	
}
