package capital.any.base.enums;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @author eranl
 *
 */
@JsonFormat(shape = JsonFormat.Shape.NUMBER)
public enum MsgResTypeEnum {
	REGULAR(1), LARGE_VALUE(2);
	
	private static final Map<Integer, MsgResTypeEnum> ID_MSG_RES_TYPE_ENUM_MAP = new HashMap<Integer, MsgResTypeEnum>(MsgResTypeEnum.values().length);
	static {
		for (MsgResTypeEnum g : MsgResTypeEnum.values()) {
			ID_MSG_RES_TYPE_ENUM_MAP.put(g.getId(), g);
		}
	}

	private int id;
	
	public static MsgResTypeEnum get(int id) {
		MsgResTypeEnum msgResTypeEnum = ID_MSG_RES_TYPE_ENUM_MAP.get(id);
		if (msgResTypeEnum == null) {
			throw new IllegalArgumentException("No MsgResTypeEnum with id: " + id);
		} 
		return msgResTypeEnum;
	}
	
	
	/**
	 * @param id
	 */
	private MsgResTypeEnum(int id) {
		this.setId(id);
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}		
}
