package capital.any.base.enums;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @author LioR SoLoMoN
 *
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Currency {
	EURO(3, "EUR", "currencies.eur","€"),
	DOLLAR(2, "USD", "currencies.usd","$");
	
	private int id;
	private String code;
	private String displayName;
	private String symbol;
	
	/**
	 * @param id
	 * @param code
	 * @param displayName
	 * @param symbol
	 */
	private Currency(int id, String code, String displayName, String symbol) {
		this.setId(id);
		this.setCode(code);
		this.setDisplayName(displayName);
		this.setSymbol(symbol);
	}

	private static final Map<Integer, Currency> ID_CURRENCY_MAP = new HashMap<Integer, Currency>(Currency.values().length);
	static {
		for (Currency c : Currency.values()) {
			ID_CURRENCY_MAP.put(c.getId(), c);
		}
	}
	
	private static final Map<String, Currency> CODE_CURRENCY_MAP = new HashMap<String, Currency>(Currency.values().length);
	static {
		for (Currency c : Currency.values()) {
			CODE_CURRENCY_MAP.put(c.getCode(), c);
		}
	}
	
	/**
	 * @param code
	 * @return
	 */
	public static Currency getByCode(String code) {
		Currency c = CODE_CURRENCY_MAP.get(code);
		if (c == null) {
			throw new IllegalArgumentException("No Currency with code: " + code);
		} else {
			return c;
		}
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}
	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	/**
	 * @return the symbol
	 */
	public String getSymbol() {
		return symbol;
	}
	/**
	 * @param symbol the symbol to set
	 */
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
}
