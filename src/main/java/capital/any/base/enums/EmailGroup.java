package capital.any.base.enums;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @author Eyal G
 *
 */
@JsonFormat(shape = JsonFormat.Shape.NUMBER)
public enum EmailGroup {
	TRADERS(1),
	DAILY_REPORT(2);
	
	private static final Logger logger = LoggerFactory.getLogger(EmailGroup.class);
	private static final Map<Integer, EmailGroup> ID_EMAILGROUP_MAP = new HashMap<Integer, EmailGroup>(EmailGroup.values().length);
	static {
		for (EmailGroup table : EmailGroup.values()) {
			ID_EMAILGROUP_MAP.put(table.getId(), table);
		}
	}

	private Integer id;
	
	private EmailGroup(Integer id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @param id
	 * @return InvestmentStatus by a given id
	 */
	@JsonCreator 
    public static EmailGroup get(String id) {
		EmailGroup emailGroup = null;
		emailGroup = ID_EMAILGROUP_MAP.get(id);
        if (emailGroup == null) {
            throw new IllegalArgumentException(id + " has no corresponding value for email group");
        }		
        return emailGroup;
    }
}
