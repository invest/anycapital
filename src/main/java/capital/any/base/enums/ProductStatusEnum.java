package capital.any.base.enums;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author Eyal G
 *
 */
@JsonFormat(shape = JsonFormat.Shape.NUMBER)
public enum ProductStatusEnum {
	UNPUBLISH(1),
	SUBSCRIPTION(2),
	SECONDARY(3),
	WAITING_FOR_SETTLE(4),
	SETTLED(5),
	CANCELED(6);
	
	private static final Logger logger = LoggerFactory.getLogger(ProductStatusEnum.class);
	private static final Map<Integer, ProductStatusEnum> ID_PRODUCT_STATUS_MAP = 
			new HashMap<Integer, ProductStatusEnum>(ProductStatusEnum.values().length);
	static {
		for (ProductStatusEnum pse : ProductStatusEnum.values()) {
			ID_PRODUCT_STATUS_MAP.put(pse.getId(), pse);
		}
	}

	private int id;

	private ProductStatusEnum(int id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	@JsonValue
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @param id
	 * @return InvestmentType by a given id
	 */
	@JsonCreator 
    public static ProductStatusEnum get(String id) {
		ProductStatusEnum pse = null;
		try {
			pse = ID_PRODUCT_STATUS_MAP.get(Integer.valueOf(id));
	        if (pse == null) {
	            throw new IllegalArgumentException(id + " has no corresponding value for ProductStatusEnum");
	        }
		} catch (NumberFormatException ne) {
			logger.error("ERROR! ProductStatusEnum; send a number!");
			throw new IllegalArgumentException(id + " has no corresponding value for ProductStatusEnum");
		}
		
        return pse;
    }
	
	public static ProductStatusEnum getById(int id) {
		ProductStatusEnum pse = ID_PRODUCT_STATUS_MAP.get(id);
		if (pse == null) {
			throw new IllegalArgumentException("No ProductStatusEnum with id: " + id);
		} else {
			return pse;
		}
	}
}
