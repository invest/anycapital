package capital.any.base.enums;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @author Eyal G
 *
 */
@JsonFormat(shape = JsonFormat.Shape.NUMBER)
public enum ProductBarrierTypeEnum {
	American(1), European(2);
	
	private static final Map<Integer, ProductBarrierTypeEnum> ID_PRODUCT_BARRIER_TYPE_ENUM_MAP = new HashMap<Integer, ProductBarrierTypeEnum>(ProductBarrierTypeEnum.values().length);
	static {
		for (ProductBarrierTypeEnum g : ProductBarrierTypeEnum.values()) {
			ID_PRODUCT_BARRIER_TYPE_ENUM_MAP.put(g.getId(), g);
		}
	}

	private int id;
	
	public static ProductBarrierTypeEnum get(int id) {
		ProductBarrierTypeEnum languageEnum = ID_PRODUCT_BARRIER_TYPE_ENUM_MAP.get(id);
		if (languageEnum == null) {
			throw new IllegalArgumentException("No Product Barrier Type Enum with id: " + id);
		} 
		return languageEnum;
	}
	
	/**
	 * @param id
	 */
	private ProductBarrierTypeEnum(int id) {
		this.setId(id);
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

}
