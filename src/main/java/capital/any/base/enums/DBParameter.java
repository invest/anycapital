package capital.any.base.enums;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author Eyal G
 *
 */
@JsonFormat(shape = JsonFormat.Shape.NUMBER)
public enum DBParameter {
	
	MAX_INV(1),
	MINIMUM_WITHDRAW_AMOUNT(2),
	WITHDRAW_FEE_AMOUNT(3),
	USER_TOKENS_EXPIRED(4),
	GOOGLE_SHORT_URL_API(5),
	MIN_INV(6),
	SENDGRID_API_KEY(7),
	SENDGRID_SERVICE_URL(8),
	KEY_SECURE_AES(9),
	EMAIL_SENDER_CONSUMERS(14),
	EMAIL_SENDER_QUEUE_SIZE(15),
	SUPPORT_PHONE(16),
	SUPPORT_EMAIL(17),
	COMPANY_ADDRESS(18),
	HC_TTL_ANALYTICS(19),
	DOCUMENTS_EMAIL(20);
	
	private static final Logger logger = LoggerFactory.getLogger(DBParameter.class);
	private static final Map<Integer, DBParameter> ID_DB_PARAMETER_MAP = new HashMap<Integer, DBParameter>(DBParameter.values().length);
	static {
		for (DBParameter table : DBParameter.values()) {
			ID_DB_PARAMETER_MAP.put(table.getId(), table);
		}
	}

	private int id;

	private DBParameter(int id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	@JsonValue
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @param id
	 * @return InvestmentStatus by a given id
	 */
	@JsonCreator 
    public static DBParameter get(String id) {
		DBParameter dbParameter = null;
		try {
			dbParameter = ID_DB_PARAMETER_MAP.get(Integer.valueOf(id));
	        if (dbParameter == null) {
	            throw new IllegalArgumentException(id + " has no corresponding value for dbParameter");
	        }
		} catch (NumberFormatException ne) {
			logger.error("ERROR! dbParameter; send a number!");
			throw new IllegalArgumentException(id + " has no corresponding value for dbParameter");
		}
		
        return dbParameter;
    }
	
	public static DBParameter get(int id) {
		DBParameter dbParameter = ID_DB_PARAMETER_MAP.get(id);
		if (dbParameter == null) {
			throw new IllegalArgumentException("No dbParameter with id: " + id);
		} 
		return dbParameter;
	}
}
