package capital.any.base.enums;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 
 * @author eyal.ohana
 *
 */
@JsonFormat(shape = JsonFormat.Shape.NUMBER)
public enum QmUserStatusEnum {
	QUESTIONNAIRE(1),
	PENDING(2),
	APPROVED(3);
	
	private static final Logger logger = LoggerFactory.getLogger(QmUserStatusEnum.class);
	private static final Map<Integer, QmUserStatusEnum> ID_QM_USER_STATUS_MAP = new HashMap<Integer, QmUserStatusEnum>(QmUserStatusEnum.values().length);
	static {
		for (QmUserStatusEnum qmUserStatusEnum : QmUserStatusEnum.values()) {
			ID_QM_USER_STATUS_MAP.put(qmUserStatusEnum.getId(), qmUserStatusEnum);
		}
	}
	
	private int id;
	
	private QmUserStatusEnum(int id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	@JsonValue
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * 
	 * @param id
	 * @return QmUserStatusEnum
	 */
	@JsonCreator 
    public static QmUserStatusEnum get(String id) {
		QmUserStatusEnum qmUserStatusEnum = null;
		try {
			qmUserStatusEnum = ID_QM_USER_STATUS_MAP.get(Integer.valueOf(id));
	        if (qmUserStatusEnum == null) {
	            throw new IllegalArgumentException(id + " has no corresponding value for QmUserStatus");
	        }
		} catch (NumberFormatException ne) {
			logger.error("ERROR! QmUserStatus; send a number!");
			throw new IllegalArgumentException(id + " has no corresponding value for QmUserStatus");
		}
        return qmUserStatusEnum;
    }
	
	public static QmUserStatusEnum get(int id) {
		QmUserStatusEnum qmUserStatusEnum = ID_QM_USER_STATUS_MAP.get(id);
		if (qmUserStatusEnum == null) {
			throw new IllegalArgumentException("No QmUserStatus with id: " + id);
		} 
		return qmUserStatusEnum;
	}
}
