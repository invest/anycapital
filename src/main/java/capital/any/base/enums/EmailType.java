package capital.any.base.enums;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * @author Eyal G
 *
 */
public enum EmailType {
	TEMPLATE(1),
	NO_TEMPLATE_TEXT(2),
	NO_TEMPLATE_TEXT_HTML(3);
	
	private static final Logger logger = LoggerFactory.getLogger(EmailType.class);
	private static final Map<Integer, EmailType> ID_EMAILTYPES_MAP = new HashMap<Integer, EmailType>(EmailType.values().length);
	static {
		for (EmailType table : EmailType.values()) {
			ID_EMAILTYPES_MAP.put(table.getId(), table);
		}
	}

	private int id;
	
	private EmailType(int id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @param id
	 * @return InvestmentStatus by a given id
	 */
	@JsonCreator 
    public static EmailType get(String id) {
		EmailType emailType = null;
		emailType = ID_EMAILTYPES_MAP.get(id);
        if (emailType == null) {
            throw new IllegalArgumentException(id + " has no corresponding value for email type");
        }		
        return emailType;
    }
}
