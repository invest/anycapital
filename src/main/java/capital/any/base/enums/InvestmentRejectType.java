package capital.any.base.enums;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author Eyal G
 *
 */
@JsonFormat(shape = JsonFormat.Shape.NUMBER)
public enum InvestmentRejectType {
	PRODUCT_SUSPENDED(1),
	DEVIATION_ASK(2),
	MIN_INV_LIMIT(3),
	MAX_INV_LIMIT(4),
	EXPOSURE(5),
	INSUFFICIENT_BALANCE_MORE_THEN_MIN_INV(6),
	INSUFFICIENT_BALANCE_LESS_THEN_MIN_INV(7),
	INVESTMENT_PRAIMERY_PRODUCT_SECONDARY(8),
	LIMIT_OF_ONE_PENDING_INVESTMENT_PER_PRODUCT_IN_PRIMARY(9),
	PRODUCT_IS_CURRENTLY_UNAVAILABLE(10),
	INVESTMENT_SECONDARY_PRODUCT_PRAIMERY(11),
	USER_NOT_REGULATED(12);
	
	private static final Logger logger = LoggerFactory.getLogger(InvestmentRejectType.class);
	private static final Map<Integer, InvestmentRejectType> ID_INVESTMENT_REJECT_TYPE_MAP = new HashMap<Integer, InvestmentRejectType>(InvestmentRejectType.values().length);
	static {
		for (InvestmentRejectType investmentStatus : InvestmentRejectType.values()) {
			ID_INVESTMENT_REJECT_TYPE_MAP.put(investmentStatus.getId(), investmentStatus);
		}
	}

	private int id;

	private InvestmentRejectType(int id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	@JsonValue
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @param id
	 * @return InvestmentStatus by a given id
	 */
	@JsonCreator 
    public static InvestmentRejectType get(String id) {
		InvestmentRejectType investmentRejectType = null;
		try {
			investmentRejectType = ID_INVESTMENT_REJECT_TYPE_MAP.get(Integer.valueOf(id));
	        if (investmentRejectType == null) {
	            throw new IllegalArgumentException(id + " has no corresponding value for InvestmentRejectType");
	        }
		} catch (NumberFormatException ne) {
			logger.error("ERROR! InvestmentRejectType; send a number!");
			throw new IllegalArgumentException(id + " has no corresponding value for InvestmentRejectType");
		}
		
        return investmentRejectType;
    }
	
	public static InvestmentRejectType get(int id) {
		InvestmentRejectType investmentRejectType = ID_INVESTMENT_REJECT_TYPE_MAP.get(id);
		if (investmentRejectType == null) {
			throw new IllegalArgumentException("No InvestmentRejectType with id: " + id);
		} 
		return investmentRejectType;
	}
}
