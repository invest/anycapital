package capital.any.base.enums;

import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author LioR SoLoMoN
 *
 */
@JsonFormat(shape = JsonFormat.Shape.NUMBER)
public enum TransactionRejectType {
	/**
	 * 
	 */
	MINIMUM_FEE(1),
	/**
	 * 
	 */
	MINIMUM_AMOUNT(2),
	/**
	 * 
	 */
	OPEN_WITHDRAW(3),
	/**
	 * 
	 */
	BALANCE(4);

	
	private static final Logger logger = LoggerFactory.getLogger(TransactionRejectType.class);
	private static final Map<Integer, TransactionRejectType> ID_TRANSACTION_REJECT_TYPE_MAP = new HashMap<Integer, TransactionRejectType>(TransactionRejectType.values().length);
	static {
		for (TransactionRejectType tt : TransactionRejectType.values()) {
			ID_TRANSACTION_REJECT_TYPE_MAP.put(tt.getId(), tt);
		}
	}

	private int id;

	private TransactionRejectType(int id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	@JsonValue
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @param id
	 * @return transactionRejectType by a given id
	 */
	@JsonCreator 
    public static TransactionRejectType get(String id) {
		TransactionRejectType transactionRejectType = null;
		try {
			transactionRejectType = ID_TRANSACTION_REJECT_TYPE_MAP.get(Integer.valueOf(id));
	        if (transactionRejectType == null) {
	            throw new IllegalArgumentException(id + " has no corresponding value for transactionRejectType");
	        }
		} catch (NumberFormatException ne) {
			logger.error("ERROR! transactionRejectType; send a number!");
			throw new IllegalArgumentException(id + " has no corresponding value for transactionRejectType");
		}
		
        return transactionRejectType;
    }
	
	public static TransactionRejectType get(int id) {
		TransactionRejectType transactionRejectType = ID_TRANSACTION_REJECT_TYPE_MAP.get(id);
		if (transactionRejectType == null) {
			throw new IllegalArgumentException("No transactionRejectType with id: " + id);
		} 
		return transactionRejectType;
	}
}
