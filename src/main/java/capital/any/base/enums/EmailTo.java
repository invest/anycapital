package capital.any.base.enums;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 
 * @author eyal.ohana
 *
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum EmailTo {		
	USER(1, "send-to-user"),
	WRITER(2, "send-to-writer"),
	BOTH(3, "send-to-both");
	
	private static final Logger logger = LoggerFactory.getLogger(EmailTo.class);
	
	private int id;
	private String displayName;
	
	/**
	 * @param id
	 * @param displayName
	 */
	private EmailTo(int id, String displayName) {
		this.setId(id);
		this.setDisplayName(displayName);
	}
	
	public static final Map<Integer, EmailTo> ID_EMAIL_TO_MAP = new HashMap<Integer, EmailTo>(EmailTo.values().length);
	static {
		for (EmailTo emailTo : EmailTo.values()) {
			ID_EMAIL_TO_MAP.put(emailTo.getId(), emailTo);
		}
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}
	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	
}
