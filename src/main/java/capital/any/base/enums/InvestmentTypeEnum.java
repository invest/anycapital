package capital.any.base.enums;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author Eyal G
 *
 */
@JsonFormat(shape = JsonFormat.Shape.NUMBER)
public enum InvestmentTypeEnum {	
	SUBSCRIPTION(1),
	SECONDARY(2);
	
	private static final Logger logger = LoggerFactory.getLogger(InvestmentTypeEnum.class);
	private static final Map<Integer, InvestmentTypeEnum> ID_INVESTMENT_TYPE_MAP = new HashMap<Integer, InvestmentTypeEnum>(InvestmentTypeEnum.values().length);
	static {
		for (InvestmentTypeEnum investmentType : InvestmentTypeEnum.values()) {
			ID_INVESTMENT_TYPE_MAP.put(investmentType.getId(), investmentType);
		}
	}
	
	private int id;

	private InvestmentTypeEnum(int id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	@JsonValue
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @param id
	 * @return InvestmentType by a given id
	 */
	@JsonCreator 
    public static InvestmentTypeEnum get(String id) {
		InvestmentTypeEnum investmentType = null;
		try {
			investmentType = ID_INVESTMENT_TYPE_MAP.get(Integer.valueOf(id));
	        if (investmentType == null) {
	            throw new IllegalArgumentException(id + " has no corresponding value for InvestmentType");
	        }
		} catch (NumberFormatException ne) {
			logger.error("ERROR! InvestmentType; send a number!");
			throw new IllegalArgumentException(id + " has no corresponding value for InvestmentType");
		}
		
        return investmentType;
    }
	
	public static InvestmentTypeEnum get(int id) {
		InvestmentTypeEnum investmentType = ID_INVESTMENT_TYPE_MAP.get(id);
		if (investmentType == null) {
			throw new IllegalArgumentException("No InvestmentType with id: " + id);
		} 
		return investmentType;
	}
}
