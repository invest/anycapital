package capital.any.base.enums;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author Eyal G
 *
 */
@JsonFormat(shape = JsonFormat.Shape.NUMBER)
public enum InvestmentStatusEnum {
	PENDING(1),
	OPEN(2),
	SETTLED(3),
	CANCELED(4),
	CANCELED_INSUFFICIENT_FUNDS(5);
	
	private static final Logger logger = LoggerFactory.getLogger(InvestmentStatusEnum.class);
	private static final Map<Integer, InvestmentStatusEnum> ID_INVESTMENT_STATUS_MAP = new HashMap<Integer, InvestmentStatusEnum>(InvestmentStatusEnum.values().length);
	static {
		for (InvestmentStatusEnum investmentStatus : InvestmentStatusEnum.values()) {
			ID_INVESTMENT_STATUS_MAP.put(investmentStatus.getId(), investmentStatus);
		}
	}

	private int id;

	private InvestmentStatusEnum(int id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	@JsonValue
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @param id
	 * @return InvestmentStatus by a given id
	 */
	@JsonCreator 
    public static InvestmentStatusEnum get(String id) {
		InvestmentStatusEnum investmentStatus = null;
		try {
			investmentStatus = ID_INVESTMENT_STATUS_MAP.get(Integer.valueOf(id));
	        if (investmentStatus == null) {
	            throw new IllegalArgumentException(id + " has no corresponding value for InvestmentStatus");
	        }
		} catch (NumberFormatException ne) {
			logger.error("ERROR! InvestmentStatus; send a number!");
			throw new IllegalArgumentException(id + " has no corresponding value for InvestmentStatus");
		}
		
        return investmentStatus;
    }
	
	public static InvestmentStatusEnum get(int id) {
		InvestmentStatusEnum investmentStatus = ID_INVESTMENT_STATUS_MAP.get(id);
		if (investmentStatus == null) {
			throw new IllegalArgumentException("No InvestmentStatus with id: " + id);
		} 
		return investmentStatus;
	}
}
