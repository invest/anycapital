package capital.any.base.enums;

/**
 * @author LioR SoLoMoN
 *
 */
public enum CommunicationChannel {
	/**
	 * 
	 */
	EMAIL(1),
	/**
	 * 
	 */
	SMS(2);
	
	
	private int id;

	private CommunicationChannel(int id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}	
}
