package capital.any.base.enums;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author Eyal G
 *
 */
@JsonFormat(shape = JsonFormat.Shape.NUMBER)
public enum ProductHistoryAction {	
	INSERT_PRODUCT(1),
	UPDATE_ASK_BID(2),
	UPDATE_SUSPEND(3),
	UPDATE_STATUS(4),
	UPDATE_PRODUCT(5),
	UPDATE_PRIORTY(6),
	UPDATE_PRODUCT_MARKET_START_LEVEL(7),
	UPDATE_PRODUCT_MARKET_END_LEVEL(8),
	UPDATE_PRODUCT_BARRIER(9),
	UPDATE_PRODUCT_OBSERVATION_LEVEL(10),
	UPDATE_PRODUCT_COUPON_PAID(11),
	UPDATE_PRODUCT_KID(12);
	
	private static final Logger logger = LoggerFactory.getLogger(ProductHistoryAction.class);
	private static final Map<Integer, ProductHistoryAction> ID_PRODUCT_HISTORY_ACTION_MAP = new HashMap<Integer, ProductHistoryAction>(ProductHistoryAction.values().length);
	static {
		for (ProductHistoryAction productHistoryAction : ProductHistoryAction.values()) {
			ID_PRODUCT_HISTORY_ACTION_MAP.put(productHistoryAction.getId(), productHistoryAction);
		}
	}
	
	private int id;

	private ProductHistoryAction(int id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	@JsonValue
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @param id
	 * @return InvestmentType by a given id
	 */
	@JsonCreator 
    public static ProductHistoryAction get(String id) {
		ProductHistoryAction productHistoryAction = null;
		try {
			productHistoryAction = ID_PRODUCT_HISTORY_ACTION_MAP.get(Integer.valueOf(id));
	        if (productHistoryAction == null) {
	            throw new IllegalArgumentException(id + " has no corresponding value for product History Action");
	        }
		} catch (NumberFormatException ne) {
			logger.error("ERROR! product History Action; send a number!");
			throw new IllegalArgumentException(id + " has no corresponding value for product History Action");
		}
		
        return productHistoryAction;
    }
	
	public static ProductHistoryAction get(int id) {
		ProductHistoryAction productHistoryAction = ID_PRODUCT_HISTORY_ACTION_MAP.get(id);
		if (productHistoryAction == null) {
			throw new IllegalArgumentException("No product History Action with id: " + id);
		} 
		return productHistoryAction;
	}
}
