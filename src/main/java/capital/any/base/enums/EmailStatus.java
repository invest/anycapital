package capital.any.base.enums;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * @author Eyal G
 *
 */
public enum EmailStatus {
	QUEUED(1),
	PROCESSING(2),
	SENT(3),
	FAILED(4);
	
	private static final Logger logger = LoggerFactory.getLogger(EmailStatus.class);
	private static final Map<Integer, EmailStatus> ID_EMAILSTATUS_MAP = new HashMap<Integer, EmailStatus>(EmailStatus.values().length);
	static {
		for (EmailStatus table : EmailStatus.values()) {
			ID_EMAILSTATUS_MAP.put(table.getStatusId(), table);
		}
	}

	private int statusId;
	
	private EmailStatus(int statusId) {
		this.statusId = statusId;
	}

	/**
	 * @return the statusId
	 */
	public int getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId the statusId to set
	 */
	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	/**
	 * @param id
	 * @return InvestmentStatus by a given id
	 */
	@JsonCreator 
    public static EmailStatus get(String id) {
		EmailStatus emailStatus = null;
		emailStatus = ID_EMAILSTATUS_MAP.get(id);
        if (emailStatus == null) {
            throw new IllegalArgumentException(id + " has no corresponding value for email status");
        }		
        return emailStatus;
    }
}
