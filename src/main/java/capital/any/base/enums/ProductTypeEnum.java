package capital.any.base.enums;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author Eyal G
 *
 */
@JsonFormat(shape = JsonFormat.Shape.NUMBER)
public enum ProductTypeEnum {
	CAPITAL_PROTECTION_CARTIFICATE_WITH_PARTICIPATION(1),
	BARRIER_CAPITAL_PROTECTION_CERTIFICATE(2),
	CAPITAL_PROTECTION_CERTIFICATE_WITH_COUPON(3),
	REVERSE_CONVERTIBLE(4),
	BARRIER_REVERSE_CONVERTIBLE(5),
	INVERSE_BARRIER_REVERSE_CONVERTIBLE(6),
	EXPRESS_CERTIFICATE(7),
	OUTPERFORMANCE_CERTIFICATE(8),
	BONUS_CERTIFICATE(9),
	BONUS_OUTPERFORMANCE_CERTIFICATE(10);
	
	private static final Logger logger = LoggerFactory.getLogger(ProductTypeEnum.class);
	private static final Map<Integer, ProductTypeEnum> ID_PRODUCT_TYPE_MAP = 
			new HashMap<Integer, ProductTypeEnum>(ProductTypeEnum.values().length);
	static {
		for (ProductTypeEnum productTypeEnum : ProductTypeEnum.values()) {
			ID_PRODUCT_TYPE_MAP.put(productTypeEnum.getId(), productTypeEnum);
		}
	}
	
	private int id;

	private ProductTypeEnum(int id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	@JsonValue
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @param id
	 * @return InvestmentType by a given id
	 */
	@JsonCreator 
    public static ProductTypeEnum get(String id) {
		ProductTypeEnum productTypeEnum = null;
		try {
			productTypeEnum = ID_PRODUCT_TYPE_MAP.get(Integer.valueOf(id));
	        if (productTypeEnum == null) {
	            throw new IllegalArgumentException(id + " has no corresponding value for ProductTypeEnum");
	        }
		} catch (NumberFormatException ne) {
			logger.error("ERROR! ProductTypeEnum; send a number!");
			throw new IllegalArgumentException(id + " has no corresponding value for ProductTypeEnum");
		}
		
        return productTypeEnum;
    }
	
	public static ProductTypeEnum getById(int id) {
		ProductTypeEnum productTypeEnum = ID_PRODUCT_TYPE_MAP.get(id);
		if (productTypeEnum == null) {
			throw new IllegalArgumentException("No ProductTypeEnum with id: " + id);
		} else {
			return productTypeEnum;
		}
	}
}
