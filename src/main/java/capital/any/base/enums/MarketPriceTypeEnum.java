package capital.any.base.enums;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @author eranl
 *
 */
@JsonFormat(shape = JsonFormat.Shape.NUMBER)
public enum MarketPriceTypeEnum {
	LAST_PRICE(1), HISTORY_PRICE(2);
	
	private static final Map<Integer, MarketPriceTypeEnum> ID_MARKET_PRICE_TYPE_ENUM_MAP = new HashMap<Integer, MarketPriceTypeEnum>(MarketPriceTypeEnum.values().length);
	static {
		for (MarketPriceTypeEnum g : MarketPriceTypeEnum.values()) {
			ID_MARKET_PRICE_TYPE_ENUM_MAP.put(g.getId(), g);
		}
	}

	private int id;
	
	public static MarketPriceTypeEnum get(int id) {
		MarketPriceTypeEnum languageEnum = ID_MARKET_PRICE_TYPE_ENUM_MAP.get(id);
		if (languageEnum == null) {
			throw new IllegalArgumentException("No MarketPriceTypeEnum with id: " + id);
		} 
		return languageEnum;
	}
	
	/**
	 * @param id
	 */
	private MarketPriceTypeEnum(int id) {
		this.setId(id);
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

}
