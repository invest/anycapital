package capital.any.base.enums;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * @author Eyal O
 *
 */
public enum EmailAction {
	REGISTER(1),
	FORGET_PASSWORD(2),
	MARKET_DAILY_HISTORY_JOB(3),
	PRODUCT_SETTLE(4),
	CHANGE_PRODUCT_STATUS(5),
	DAILY_REPORT(6),
	INSERT_INVESTMENT_OPEN(7),
	INSERT_INVESTMENT_PENDING(8),
	DOCS_APPROVED(9),
	DEPOSIT_PENDING_BANK_WIRE(10),
	RISK_DISCLOSURE(11),
	RECEIPT_ASKING_DOCS(12),
	DOCUMENTS_REJECTED(13),
	HIGH_RISK_PRODUCTS(14);
	
	private static final Logger logger = LoggerFactory.getLogger(EmailAction.class);
	private static final Map<Integer, EmailAction> ID_EMAIL_ACTION_MAP = new HashMap<Integer, EmailAction>(EmailAction.values().length);
	static {
		for (EmailAction table : EmailAction.values()) {
			ID_EMAIL_ACTION_MAP.put(table.getId(), table);
		}
	}

	private int id;
	
	private EmailAction(int id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @param id
	 * @return EmailAction by a given id
	 */
	@JsonCreator 
    public static EmailAction get(String id) {
		EmailAction emailType = null;
		emailType = ID_EMAIL_ACTION_MAP.get(id);
        if (emailType == null) {
            throw new IllegalArgumentException(id + " has no corresponding value for email action");
        }		
        return emailType;
    }
}
