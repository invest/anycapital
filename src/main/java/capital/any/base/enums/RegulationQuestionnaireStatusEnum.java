package capital.any.base.enums;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 
 * @author eyal.ohana
 *
 */
@JsonFormat(shape = JsonFormat.Shape.NUMBER)
public enum RegulationQuestionnaireStatusEnum {
	UNFITTING(1),
	FITTING(2),
	FITTING_AFTER_TRAINING(3),
	UNFITTING_RESTRICTED(4);
	
	private static final Logger logger = LoggerFactory.getLogger(RegulationQuestionnaireStatusEnum.class);
	private static final Map<Integer, RegulationQuestionnaireStatusEnum> ID_REGULATION_QUESTIONNAIRE_STATUS_MAP = new HashMap<Integer, RegulationQuestionnaireStatusEnum>(RegulationQuestionnaireStatusEnum.values().length);
	static {
		for (RegulationQuestionnaireStatusEnum regulationQuestionnaireStatusEnum : RegulationQuestionnaireStatusEnum.values()) {
			ID_REGULATION_QUESTIONNAIRE_STATUS_MAP.put(regulationQuestionnaireStatusEnum.getId(), regulationQuestionnaireStatusEnum);
		}
	}
	
	private int id;
	
	private RegulationQuestionnaireStatusEnum(int id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	@JsonValue
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * 
	 * @param id
	 * @return QmRegulationStatusEnum
	 */
	@JsonCreator 
    public static RegulationQuestionnaireStatusEnum get(String id) {
		RegulationQuestionnaireStatusEnum regulationQuestionnaireStatusEnum = null;
		try {
			regulationQuestionnaireStatusEnum = ID_REGULATION_QUESTIONNAIRE_STATUS_MAP.get(Integer.valueOf(id));
	        if (regulationQuestionnaireStatusEnum == null) {
	            throw new IllegalArgumentException(id + " has no corresponding value for RegulationQuestionnaireStatus");
	        }
		} catch (NumberFormatException ne) {
			logger.error("ERROR! RegulationQuestionnaireStatus; send a number!");
			throw new IllegalArgumentException(id + " has no corresponding value for RegulationQuestionnaireStatus");
		}
        return regulationQuestionnaireStatusEnum;
    }
	
	public static RegulationQuestionnaireStatusEnum get(int id) {
		RegulationQuestionnaireStatusEnum regulationQuestionnaireStatusEnum = ID_REGULATION_QUESTIONNAIRE_STATUS_MAP.get(id);
		if (regulationQuestionnaireStatusEnum == null) {
			throw new IllegalArgumentException("No RegulationQuestionnaireStatus with id: " + id);
		} 
		return regulationQuestionnaireStatusEnum;
	}
}
