package capital.any.base.enums;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * 
 * @author Eyal.o
 *
 */
public enum TransactionPaymentType {
	BANK_WIRE(1),
	INVESTMENT_COUPON(2),
	ADMIN(3);
	
	private static final Logger logger = LoggerFactory.getLogger(TransactionPaymentType.class);
	private static final Map<Integer, TransactionPaymentType> ID_TRANSACTION_PAYMENT_TYPE_MAP = new HashMap<Integer, TransactionPaymentType>(TransactionPaymentType.values().length);
	static {
		for (TransactionPaymentType table : TransactionPaymentType.values()) {
			ID_TRANSACTION_PAYMENT_TYPE_MAP.put(table.getId(), table);
		}
	}
	
	private int id;
	
	private TransactionPaymentType(int id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @param id
	 * @return InvestmentStatus by a given id
	 */
	@JsonCreator 
    public static TransactionPaymentType get(String id) {
		TransactionPaymentType transactionPaymentType = null;
		transactionPaymentType = ID_TRANSACTION_PAYMENT_TYPE_MAP.get(id);
        if (transactionPaymentType == null) {
            throw new IllegalArgumentException(id + " has no corresponding value for transaction payment type");
        }		
        return transactionPaymentType;
    }
}
