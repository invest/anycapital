package capital.any.base.filter;

import java.util.Map;

/**
 * @author LioR SoLoMoN
 *
 */
public interface FilterMethod {
	/**
	 * @return
	 */
	Map<Integer, ?> method();
}
