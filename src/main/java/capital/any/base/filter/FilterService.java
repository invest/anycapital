package capital.any.base.filter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.model.base.User.Clazz;
import capital.any.model.base.User.Gender;
import capital.any.model.base.ProductCategory;
import capital.any.model.base.ProductType;
import capital.any.service.base.actionSource.IActionSourceService;
import capital.any.service.base.country.ICountryService;
import capital.any.service.base.currency.ICurrencyService;
import capital.any.service.base.dayCountConvention.IDayCountConventionService;
import capital.any.service.base.fileType.IFileTypeService;
import capital.any.service.base.investmentStatus.IInvestmentStatusService;
import capital.any.service.base.issueChannel.IIssueChannelService;
import capital.any.service.base.issueDirections.IIssueDirectionService;
import capital.any.service.base.issueReachedStatuses.IIssueReachedStatusService;
import capital.any.service.base.issueReactions.IIssueReactionService;
import capital.any.service.base.issueSubjects.IIssueSubjectService;
import capital.any.service.base.language.ILanguageService;
import capital.any.service.base.market.IMarketService;
import capital.any.service.base.marketingContent.IMarketingContentService;
import capital.any.service.base.marketingDomain.IMarketingDomainService;
import capital.any.service.base.marketingLandingPage.IMarketingLandingPageService;
import capital.any.service.base.marketingSource.IMarketingSourceService;
import capital.any.service.base.productBarrierType.IProductBarrierTypeService;
import capital.any.service.base.productCategory.IProductCategoryService;
import capital.any.service.base.productCouponFrequency.IProductCouponFrequencyService;
import capital.any.service.base.productIssuerInsuranceType.IProductIssuerInsuranceTypeService;
import capital.any.service.base.productMaturity.IProductMaturityService;
import capital.any.service.base.productStatus.IProductStatusService;
import capital.any.service.base.productType.IProductTypeService;
import capital.any.service.base.transactionPaymentType.ITransactionPaymentTypeService;
import capital.any.service.base.transactionStatus.ITransactionStatusService;

/**
 * @author LioR SoLoMoN
 *
 */
@Service
public class FilterService {
	public static final String MAP_COUNTRIES = "countries";
	public static final String MAP_CURRENCIES = "currencies";
	public static final String MAP_LANGUAGES = "languages";
	public static final String MAP_MARKETS = "markets";
	public static final String MAP_PRODUCT_CATEGORIES = "product_categories";
	public static final String MAP_PRODUCT_STATUSES = "product_statuses";
	public static final String MAP_PRODUCT_TYPES = "product_types";
	public static final String MAP_PRODUCT_COUPON_FREQUENCIES = "product_coupon_frequencies";
	public static final String MAP_DAY_COUNT_CONVENTION = "day_count_convention";
	public static final String MAP_PRODUCT_MATURITY = "product_maturity";
	public static final String MAP_PRODUCT_BARRIER_TYPE = "product_barrier_type";
	public static final String MAP_TRANSACTION_STATUSES = "transaction_statuses";
	public static final String MAP_TRANSACTION_PAYMENT_TYPES = "transaction_payment_types";
	public static final String MAP_USER_GENDER = "user_gender";
	public static final String MAP_USER_CLASS = "user_class";
	public static final String MAP_PRODUCT_TYPE_PROTECTION_LEVEL = "product_type_protection_level";
	public static final String MAP_PRODUCT_TYPE_GROUP = "product_type_group";
	public static final String MAP_MARKETING_SOURCES = "marketing_sources";
	public static final String MAP_MARKETING_CONTENTS = "marketing_contents";
	public static final String MAP_MARKETING_LANDING_PAGES = "marketing_landing_pages";
	public static final String MAP_MARKETING_DOMAINS = "marketing_domains";
	public static final String MAP_PRODUCT_ISSUER_INSURANCE_TYPE = "product_issuer_insurance_type";
	public static final String MAP_INVESTMENT_STATUSES = "investment_statuses";
	public static final String MAP_ISSUE_CHANNELS = "issue_channels";
	public static final String MAP_ISSUE_SUBJECTS = "issue_subjects";
	public static final String MAP_ISSUE_DIRECTIONS = "issue_directions";
	public static final String MAP_REACHED_STATUSES = "issue_Reached_Statuses";
	public static final String MAP_ISSUE_REACTIONS = "issue_reaction";
	public static final String MAP_FILE_TYPES = "file_types";
	public static final String MAP_ACTION_SOURCES = "action_sources";
	//if we add new filter we need to write it here https://anyoption.atlassian.net/wiki/display/DEV/anycapital+filters
	
	protected Map<String, FilterMethod> methodMap = new HashMap<String, FilterMethod>();
	@Autowired
	private IProductTypeService productTypeService;
	@Autowired
	private ICountryService countryService; 
	@Autowired
	private ICurrencyService currencyService; 
	@Autowired
	private ILanguageService languageService; 
	@Autowired
	private IMarketService marketService; 
	@Autowired
	private IProductCategoryService productCategoryService; 
	@Autowired
	private IProductStatusService productStatusService; 
	@Autowired
	private IProductCouponFrequencyService productCouponFrequencyService; 
	@Autowired
	private IDayCountConventionService dayCountConventionService; 
	@Autowired
	private IProductMaturityService productMaturityService;
	@Autowired
	private IProductBarrierTypeService productBarrierTypeService;
	@Autowired
	private ITransactionStatusService transactionStatusService;
	@Autowired
	private ITransactionPaymentTypeService transactionPaymentTypeService;
	@Autowired
	private IMarketingSourceService marketingSourceService;
	@Autowired
	private IMarketingContentService marketingContentService;
	@Autowired
	private IMarketingLandingPageService marketingLandingPageService;
	@Autowired
	private IMarketingDomainService marketingDomainService;
	@Autowired
	private IProductIssuerInsuranceTypeService productIssuerInsuranceTypeService;
	@Autowired
	private IInvestmentStatusService investmentStatusService;
	@Autowired
	private IIssueChannelService issueChannelService;
	@Autowired
	private IIssueSubjectService issueSubjectService;
	@Autowired
	private IIssueDirectionService issueDirectionService;
	@Autowired
	private IIssueReachedStatusService issueReachedStatusService;
	@Autowired
	private IIssueReactionService issueReactionService;
	@Autowired
	private IFileTypeService FileTypeService;
	@Autowired
	private IActionSourceService actionSourceService;
	
	public FilterService() {
		methodMap.put(MAP_COUNTRIES, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				return countryService.get();
			}
		});
		
		methodMap.put(MAP_CURRENCIES, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				return currencyService.get();
			}
		});
		
		methodMap.put(MAP_LANGUAGES, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				return languageService.get();
			}
		});
		
		methodMap.put(MAP_MARKETS, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				return marketService.getThin();
			}
		});
				
		methodMap.put(MAP_PRODUCT_CATEGORIES, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				return productCategoryService.get();
			}
		});
		
		methodMap.put(MAP_PRODUCT_STATUSES, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				return productStatusService.get();
			}
		});
		
		methodMap.put(MAP_PRODUCT_TYPES, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				return productTypeService.get();
			}
		});
		
		methodMap.put(MAP_PRODUCT_COUPON_FREQUENCIES, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				return productCouponFrequencyService.get();
			}
		});
		
		methodMap.put(MAP_DAY_COUNT_CONVENTION, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				return dayCountConventionService.get();
			}
		});
		
		methodMap.put(MAP_PRODUCT_MATURITY, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				return productMaturityService.get();
			}
		});
		
		methodMap.put(MAP_PRODUCT_BARRIER_TYPE, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				return productBarrierTypeService.get();
			}
		});
		
		methodMap.put(MAP_TRANSACTION_STATUSES, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				return transactionStatusService.get();
			}
		});
		
		methodMap.put(MAP_TRANSACTION_PAYMENT_TYPES, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				return transactionPaymentTypeService.get();
			}
		});
		
		methodMap.put(MAP_USER_GENDER, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				return Gender.getByCode();
			}
		});
		
		methodMap.put(MAP_USER_CLASS, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				return Clazz.get();
			}
		});		
		
		methodMap.put(MAP_PRODUCT_TYPE_PROTECTION_LEVEL, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				Map<Integer, FilterGroupOptions<ProductType>> options = new HashMap<Integer, FilterGroupOptions<ProductType>>();
				for (ProductType productType : productTypeService.get().values()) {
					int productTypeProtectionLevelId = productType.getProductTypeProtectionLevel().getId();
					FilterGroupOptions<ProductType> option = options.get(productTypeProtectionLevelId);
					if (option == null) {
						option = new FilterGroupOptions<ProductType>();
						option.setGroupName(productType.getProductTypeProtectionLevel().getDisplayName());
						option.setList(new ArrayList<ProductType>());
						options.put(productTypeProtectionLevelId, option);
					}
					option.getList().add(productType);
				}
				return options;
			}
		});
		
		methodMap.put(MAP_PRODUCT_TYPE_GROUP, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				Map<Integer, FilterGroupOptions<ProductType>> options = new HashMap<Integer, FilterGroupOptions<ProductType>>();
				for (ProductCategory productCategory : productCategoryService.get().values()) {
					FilterGroupOptions<ProductType> option = new FilterGroupOptions<ProductType>();
					option.setGroupName(productCategory.getDisplayName());
					option.setList(new ArrayList<ProductType>());
					options.put(productCategory.getId(), option);
				}				
				for (ProductType productType : productTypeService.get().values()) {
					int productProductCategoryId = productType.getProductCategory().getId();
					FilterGroupOptions<ProductType> option = options.get(productProductCategoryId);
					option.getList().add(productType);
				}
				return options;
			}
		});
		
		methodMap.put(MAP_MARKETING_SOURCES, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				return marketingSourceService.getMarketingSources();
			}
		});
		
		methodMap.put(MAP_MARKETING_CONTENTS, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				return marketingContentService.getAll();
			}
		});
		
		methodMap.put(MAP_MARKETING_LANDING_PAGES, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				return marketingLandingPageService.getAll();
			}
		});
		
		methodMap.put(MAP_MARKETING_DOMAINS, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				return marketingDomainService.getMarketingDomains();
			}
		});
		
		methodMap.put(MAP_PRODUCT_ISSUER_INSURANCE_TYPE, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				return productIssuerInsuranceTypeService.getProductIssuerInsuranceType();
			}
		});
		
		methodMap.put(MAP_INVESTMENT_STATUSES, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				return investmentStatusService.get();
			}
		});
		
		methodMap.put(MAP_ISSUE_CHANNELS, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				return issueChannelService.get();
			}
		});
		
		methodMap.put(MAP_ISSUE_SUBJECTS, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				return issueSubjectService.get();
			}
		});
		
		methodMap.put(MAP_ISSUE_DIRECTIONS, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				return issueDirectionService.get();
			}
		});
		
		methodMap.put(MAP_REACHED_STATUSES, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				return issueReachedStatusService.get();
			}
		});
		
		methodMap.put(MAP_ISSUE_REACTIONS, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				return issueReactionService.get();
			}
		});
		
		methodMap.put(MAP_FILE_TYPES, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				return FileTypeService.get();
			}
		});
		
		methodMap.put(MAP_ACTION_SOURCES, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				return actionSourceService.get();
			}
		});
		
	}
	
	public Map<Integer, ?> get(String name) {
		return methodMap.get(name).method();
	}
}
