package capital.any.base.filter;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * for filter with group options
 * 
 * @author eyal.goren
 *
 * @param <T>
 */
public class FilterGroupOptions<T> implements Serializable, Comparable<FilterGroupOptions<T>> {
	
	private static final long serialVersionUID = -8998997231466416198L;
	
	public String groupName;
	public List<T> list;
	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}
	/**
	 * @param groupName the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	/**
	 * @return the list
	 */
	public List<T> getList() {
		return list;
	}
	/**
	 * @param list the list to set
	 */
	public void setList(List<T> list) {
		this.list = list;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FilterGroupOptions [groupName=" + groupName + ", list=" + list + "]";
	}
	
	@Override
	public int compareTo(FilterGroupOptions<T> filterGroupOptions) {
		return this.groupName.compareTo(filterGroupOptions.getGroupName());
	}

}
