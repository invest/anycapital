package capital.any.utils.base;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class ApplicationContext implements ApplicationContextAware {

	private static org.springframework.context.ApplicationContext ctx;

	@Override
	public void setApplicationContext(org.springframework.context.ApplicationContext appContext) throws BeansException {
	    ctx = appContext;
	}
	
	public static org.springframework.context.ApplicationContext getApplicationContext() {
		return ctx;
	}
}
