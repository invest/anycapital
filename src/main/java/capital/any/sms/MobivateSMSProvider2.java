package capital.any.sms;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Hashtable;

import javax.annotation.PostConstruct;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import capital.any.model.base.HttpRequest;
import capital.any.model.base.SMS;
import capital.any.model.base.SMSProvider;
import capital.any.service.IHttpClientService;

/**
 * @author eran.levy
 *
 */
@Service
public class MobivateSMSProvider2 {			
	
	private static final long serialVersionUID = -8256381863480044836L;
	
	private static final Logger logger = LoggerFactory.getLogger(MobivateSMSProvider2.class);
		
	private Hashtable<String, String> ccToRoutingID;
	
	private final String USER_ROUTE_PRICING_URL = "entity/user.UserRoutePricing/ALL/visible"; 
	private final String SEND_SINGLE_URL		= "send/sms/single?";
	
	@Autowired
	public IHttpClientService httpClientService;
	
	public void init() {
		try {
			HttpRequest httpRequest = new HttpRequest<>();
			// "http://app.mobivatebulksms.com/bulksms/xmlapi/eran.levy@anyoption.com:anyopt123/entity/user.UserRoutePricing/ALL/visible");
			//httpRequest.setUrl(url + "/" + userName + ":" + password + "/" + USER_ROUTE_PRICING_URL);
			Object getAllCCandRouteIDs = httpClientService.doGet(httpRequest);
			System.out.println(getAllCCandRouteIDs.toString());
			Document doc = parseXMLToDocument(getAllCCandRouteIDs.toString());
			ccToRoutingID = parseUserRouting(doc, "userroutepricing", "countryCode", "userRouteId");
		} catch (Exception e) {
			throw new RuntimeException("Mobivate error" + e.getMessage());
		}
	}

	public void send(SMS sms) {
		String request;
		try {
			request = "xml=" + URLEncoder.encode(composeTextRequest(sms), "UTF-8");
			HashMap<String, String> headers;
			headers = new HashMap<String, String>();
			// To send Unicode messages, make sure the following header is set:
			headers.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
			HttpRequest httpRequest = new HttpRequest<>();
			httpRequest.setHeaders(headers);
			httpRequest.setData(request);
			//http://app.mobivatebulksms.com/bulksms/xmlapi/eran.levy@anyoption.com:anyopt123/send/sms/single?			
			//httpRequest.setUrl(url + "/" + userName + ":" + password + "/" + SEND_SINGLE_URL);
			Object response = httpClientService.doPost(httpRequest);
		} catch (Exception e) {

		}
	}
	
    private Hashtable<String, String> parseUserRouting(Document rootEl, String userRoutePricingTag,
    	    String countryCodeTag, String userRouteIdTag) {
    		Hashtable<String, String> routing = new Hashtable<String, String>();
    	
    		NodeList allChildRootEldocument = rootEl.getElementsByTagName(userRoutePricingTag);
    	
    		for (int i = 0; i < allChildRootEldocument.getLength(); i++) {
    		    Node node = allChildRootEldocument.item(i);
    		    if (node.getNodeType() == Node.ELEMENT_NODE) {
    				Element eElement = (Element) node;
    				String countryCode = eElement.getElementsByTagName(countryCodeTag).item(0).getTextContent();
    				String userRouteId = eElement.getElementsByTagName(userRouteIdTag).item(0).getTextContent();
    				//in case the given first tag has more than one item in it separated by a comma
    				if (countryCode.contains(",")) {
    				    String[] ccSinOneEl = countryCode.split(",");
    				    for (String xtraCC : ccSinOneEl) {
    			    		if(routing.get(xtraCC) == null) {
    			    			routing.put(xtraCC, userRouteId);
    			    		} else {
    			    			//log.info("duplicate routing for:"+xtraCC+ " value:" + userRouteId);
    			    		}
    			    	}
    				} else {
    		    		if(routing.get(countryCode) == null) {
    		    			routing.put(countryCode, userRouteId);
    		    		} else {
    		    			//log.info("duplicate routing for["+countryCode+ "] value [" + userRouteId + "]");
    		    		}
    				}
    		    }
    		}
    		return routing;
        }
    
    public static Document parseXMLToDocument(String xml) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        byte[] xmldata = xml.getBytes();
        ByteArrayInputStream bais = new ByteArrayInputStream(xmldata);
        Document resp = docBuilder.parse(bais);
        try {
        	bais.close();
        } catch (Exception e) {
        }
        return resp;
    }
    
    private String getRouteID(String msgPhoneNumber) {
		int getRouteIDtries = 1;
		String routeID = null;
		do {
		    routeID = ccToRoutingID.get(msgPhoneNumber.substring(0, getRouteIDtries));
		    getRouteIDtries++;
		} while (routeID == null && getRouteIDtries < 4);//max Country codes are 3(4) digits
	
		return routeID;
    }
    
    private String composeTextRequest(SMS sms) {
		String request =
	        "<message>\n" + 
	        "    <originator>"	+ "test"	+ "</originator>\n" + 
	        "    <recipient>"	+ sms.getPhone()	+ "</recipient>\n" + 
	        "    <body>"		+ sms.getMessage() 	+ "</body>\n" +
	        //REFERENCE -> Used to correlate message with client reference
	        "    <reference>"	+	sms.getId()		+ "</reference>\n" ;
		
	        if(sms.getStatus() == SMS.Status.NOT_VERIFIED) {
	        	//HLR route ID
	        	logger.info("SENDING HLR MESSAGE:" + sms.getId());
	        	request += "    <routeId>mhlrglobal</routeId>\n";
	        } else {
	        	//ROUTE -> UUID which identifies the route used for delivering the message
	        	String routeId = getRouteID(sms.getPhone());
	        	request += "    <routeId>"		+ routeId +"</routeId>\n" ;	        	
	        }
	        request += "</message>" ;	        
	        return request;
    }
			
}
