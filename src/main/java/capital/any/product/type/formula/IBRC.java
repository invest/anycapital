package capital.any.product.type.formula;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Inverse Barrier Reverse Convertible
 * @author eyal.goren
 *
 */
public class IBRC extends Formula implements Serializable {

	private static final long serialVersionUID = -6754952869784761517L;

	@Override
	public BigDecimal getFormulaResult(Parameters parameters) {
		if (!parameters.isBarrierOccur() || parameters.getFinalFixingLevel().doubleValue() < parameters.getInitialFixingLevel().doubleValue()) {
			return new BigDecimal(1);			
		} else {   
			BigDecimal result = new BigDecimal(1).add(
					(parameters.getInitialFixingLevel().subtract(parameters.getFinalFixingLevel()))
					.divide(parameters.getInitialFixingLevel(), 20, RoundingMode.HALF_UP));
			return result.doubleValue() > 0 ? result : new BigDecimal("0");
		}
	}

	@Override
	public boolean IsValidate(Parameters parameters) {
		return parameters.getFinalFixingLevel().doubleValue() > 0 &&
				parameters.getInitialFixingLevel().doubleValue() > 0 &&
				parameters.getStrikeLevel().doubleValue() > 0;
	}
	
	@Override
	public BigDecimal getScenario1Result(Parameters parameters) {
		parameters.setFinalFixingLevel(getScenario1FinalFixingLevel(parameters));
		parameters.setBarrierOccur(false);
		return getFormulaResult(parameters);
	}
	
	@Override
	public BigDecimal getScenario1FinalFixingLevel(Parameters parameters) {
		return parameters.getBarrierLevel().multiply(parameters.getInitialFixingLevel().multiply(new BigDecimal(String.valueOf(scenarioPercentages.get(0))).divide(new BigDecimal("100"))));
	}
	
	@Override
	public double getScenario1FinalFixingLevelForTxt(Parameters parameters) {
		return formatFormulaResultForTxt(parameters.getBarrierLevel());
	}
	
	@Override
	public BigDecimal getScenario2FinalFixingLevel(Parameters parameters) {
		if (parameters.isBarrierOccur()) {
			return new BigDecimal("1.40");
		}
		return new BigDecimal("1").add((new BigDecimal(String.valueOf(scenarioPercentages.get(1))).divide(new BigDecimal("100"))).multiply(parameters.getBarrierLevel().subtract(new BigDecimal("1"))));
	}
	
	@Override
	public double getScenario2FinalFixingLevelForTxt(Parameters parameters) {
		if (parameters.isBarrierOccur()) {
			return new BigDecimal("40").doubleValue();
		}
		return super.getScenario2FinalFixingLevelForTxt(parameters);
	}
}
