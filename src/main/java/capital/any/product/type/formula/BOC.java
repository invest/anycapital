package capital.any.product.type.formula;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Bonus Outperformance Certificate
 * @author eyal.goren
 *
 */
public class BOC extends Formula implements Serializable {

	private static final long serialVersionUID = 4453552024071916081L;

	@Override
	public BigDecimal getFormulaResult(Parameters parameters) {
		if (parameters.getFinalFixingLevel().doubleValue() > parameters.getInitialFixingLevel().multiply(parameters.getBonusPrecentage()).doubleValue()) {
			return parameters.getBonusPrecentage().add(
					parameters.getLevelOfParticipation().multiply(
					 (parameters.getFinalFixingLevel().divide(
					  parameters.getInitialFixingLevel(), 20, RoundingMode.HALF_UP)).subtract(
					   parameters.getBonusPrecentage())));
		} else {
			if (parameters.isBarrierOccur()) {
				return parameters.getFinalFixingLevel().divide(parameters.getInitialFixingLevel(), 20, RoundingMode.HALF_UP);
			} else {
				return parameters.getBonusPrecentage();
			}
		}
	}

	@Override
	public boolean IsValidate(Parameters parameters) {
		return parameters.getFinalFixingLevel().doubleValue() > 0 &&
				parameters.getInitialFixingLevel().doubleValue() > 0 &&
				parameters.getLevelOfParticipation().doubleValue() > 0 &&
				parameters.getBonusPrecentage().doubleValue() > 0;
	}
	
	@Override
	public BigDecimal getScenario1FinalFixingLevel(Parameters parameters) {
		return parameters.getBonusPrecentage().multiply(parameters.getInitialFixingLevel().multiply(new BigDecimal(String.valueOf(scenarioPercentages.get(0))).divide(new BigDecimal("100"))));
	}
	
	@Override
	public BigDecimal getScenario2FinalFixingLevel(Parameters parameters) {
		return parameters.getBonusPrecentage().multiply(parameters.getInitialFixingLevel().multiply(new BigDecimal(String.valueOf(scenarioPercentages.get(1))).divide(new BigDecimal("100"))));
	}
	
	@Override
	public BigDecimal getScenario3FinalFixingLevel(Parameters parameters) {
		return parameters.getBarrierLevel().multiply(parameters.getInitialFixingLevel().multiply(new BigDecimal(String.valueOf(scenarioPercentages.get(2))).divide(new BigDecimal("100"))));
	}

}
