package capital.any.product.type.formula;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

import capital.any.base.enums.ProductBarrierTypeEnum;

/**
 * Bonus Certificate
 * @author eyal.goren
 *
 */
public class BC extends Formula implements Serializable {

	private static final long serialVersionUID = -5945297829621107660L;

	@Override
	public BigDecimal getFormulaResult(Parameters parameters) {
		if (parameters.isBarrierOccur() || 
				parameters.getFinalFixingLevel().doubleValue() > parameters.getInitialFixingLevel().multiply(parameters.getBonusPrecentage()).doubleValue()) {
			return parameters.getFinalFixingLevel().divide(parameters.getInitialFixingLevel(), 20, RoundingMode.HALF_UP);
		} else {
			return parameters.getBonusPrecentage();
		}
	}

	@Override
	public boolean IsValidate(Parameters parameters) {
		return parameters.getFinalFixingLevel().doubleValue() > 0 &&
				parameters.getInitialFixingLevel().doubleValue() > 0 &&
				parameters.getBonusPrecentage().doubleValue() > 0;
	}
	
	@Override
	public BigDecimal getScenario2FinalFixingLevel(Parameters parameters) {
		if (parameters.isBarrierOccur()) {
			return new BigDecimal("60");
		}
		return parameters.getStrikeLevel().multiply(parameters.getInitialFixingLevel().multiply(new BigDecimal(String.valueOf(scenarioPercentages.get(1))).divide(new BigDecimal("100"))));
	}
	
	@Override
	public double getScenario2FinalFixingLevelForTxt(Parameters parameters) {
		if (parameters.isBarrierOccur()) {
			return new BigDecimal(-40).doubleValue();
		}
		double result = (new BigDecimal("1").subtract(parameters.getBarrierLevel())).multiply(new BigDecimal("100")).doubleValue(); 
		if (parameters.getProductBarrierType() != null && parameters.getProductBarrierType().getId() == ProductBarrierTypeEnum.European.getId()) {
			result = super.getScenario2FinalFixingLevelForTxt(parameters);
		}
		return result; 
	}
	
	@Override
	public BigDecimal getScenario3FinalFixingLevel(Parameters parameters) {
		return parameters.getBarrierLevel().multiply(parameters.getInitialFixingLevel().multiply(new BigDecimal(String.valueOf(scenarioPercentages.get(2))).divide(new BigDecimal("100"))));
	}
	
	public double getScenario3FinalFixingLevelForTxt(Parameters parameters) {
		return (new BigDecimal("1").subtract((parameters.getBarrierLevel().multiply(new BigDecimal(String.valueOf(scenarioPercentages.get(2))).divide(new BigDecimal("100")))))).multiply(new BigDecimal("100")).multiply(new BigDecimal("-1")).doubleValue();
	}

}
