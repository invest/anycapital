package capital.any.product.type.formula;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Capital Protection Certificate with Coupon
 * @author eyal.goren
 *
 */
public class CPCC extends Formula implements Serializable {

	private static final long serialVersionUID = -6754952869784761517L;

	@Override
	public BigDecimal getFormulaResult(Parameters parameters) {
		return parameters.getLevelOfProtection();
	}

	@Override
	public boolean IsValidate(Parameters parameters) {
		return parameters.getFinalFixingLevel().doubleValue() > 0 &&
				parameters.getInitialFixingLevel().doubleValue() > 0 &&
				 parameters.getLevelOfProtection().doubleValue() > 0;
	}

}
