package capital.any.product.type.formula;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Outperformance Certificate
 * @author eyal.goren
 *
 */
public class OC extends Formula implements Serializable {

	private static final long serialVersionUID = 2650319861888620713L;

	@Override
	public BigDecimal getFormulaResult(Parameters parameters) {
		if (parameters.getFinalFixingLevel().doubleValue() <= parameters.getInitialFixingLevel().doubleValue()) {
			return parameters.getFinalFixingLevel().divide(parameters.getInitialFixingLevel());
		} else {
			BigDecimal result = new BigDecimal("1").add(
					parameters.getLevelOfParticipation().multiply(
							(parameters.getFinalFixingLevel().subtract(
									parameters.getInitialFixingLevel()))).divide(
											parameters.getInitialFixingLevel(), 20, RoundingMode.HALF_UP));
			if (parameters.getCapLevel().doubleValue() > 0) {
				return parameters.getCapLevel().min(result);
			} else {
				return result;
			}
		}
	}

	@Override
	public boolean IsValidate(Parameters parameters) {
		return parameters.getFinalFixingLevel().doubleValue() > 0 &&
				parameters.getInitialFixingLevel().doubleValue() > 0;
	}
	
	@Override
	public BigDecimal getScenario1FinalFixingLevel(Parameters parameters) {
		if (parameters.getCapLevel().doubleValue() > 0) { //with cap
			return new BigDecimal("1").add(new BigDecimal(String.valueOf(scenarioPercentages.get(3))).divide(new BigDecimal("100")).multiply(
					parameters.getCapLevel().subtract(new BigDecimal("1")))); 
		} //no cap
		return super.getScenario1FinalFixingLevel(parameters);
	}
		
	@Override
	public BigDecimal getScenario2FinalFixingLevel(Parameters parameters) {
		if (parameters.getCapLevel().doubleValue() > 0) { //with cap
			return new BigDecimal("1").add(new BigDecimal(String.valueOf(scenarioPercentages.get(4))).divide(new BigDecimal("100")).multiply(
					(parameters.getCapLevel().subtract(new BigDecimal("1"))).divide(parameters.getLevelOfParticipation(), 20, RoundingMode.HALF_UP))); 
		} //no cap
		return super.getScenario2FinalFixingLevel(parameters);
	}
}
