package capital.any.product.type.formula;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Barrier Capital Protection Certificate
 * @author eyal.goren
 *
 */
public class BCPC extends Formula implements Serializable {

	private static final long serialVersionUID = -6754952869784761517L;

	@Override
	public BigDecimal getFormulaResult(Parameters parameters) {
		if (parameters.isBarrierOccur() || parameters.getFinalFixingLevel().doubleValue() <= parameters.getInitialFixingLevel().doubleValue()) {
			return parameters.getLevelOfProtection();
		} else {
			return parameters.getLevelOfProtection().add(
					parameters.getLevelOfParticipation().multiply(
					 (parameters.getFinalFixingLevel().subtract(
					  parameters.getInitialFixingLevel()))).divide(
					   parameters.getInitialFixingLevel(), 20, RoundingMode.HALF_UP));
		}
	}

	@Override
	public boolean IsValidate(Parameters parameters) {
		return parameters.getFinalFixingLevel().doubleValue() > 0 &&
				parameters.getInitialFixingLevel().doubleValue() > 0 &&
				parameters.getBarrierLevel().doubleValue() > 0 &&
				parameters.getLevelOfProtection().doubleValue() > 0;
	}
	
	@Override
	public BigDecimal getScenario1FinalFixingLevel(Parameters parameters) {
		return new BigDecimal("1").add(new BigDecimal(String.valueOf(scenarioPercentages.get(0))).divide(new BigDecimal("100")).multiply(parameters.getBarrierLevel().subtract(new BigDecimal("1"))));
	}
	
	@Override
	public BigDecimal getScenario2FinalFixingLevel(Parameters parameters) {
		return new BigDecimal("1").add(new BigDecimal(String.valueOf(scenarioPercentages.get(1))).divide(new BigDecimal("100")).multiply(parameters.getBarrierLevel().subtract(new BigDecimal("1"))));
	}

}
