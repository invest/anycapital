package capital.any.product.type.formula;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Reverse Convertible
 * @author eyal.goren
 *
 */
public class RC extends Formula implements Serializable {

	private static final long serialVersionUID = -6754952869784761517L;

	@Override
	public BigDecimal getFormulaResult(Parameters parameters) {
		if (parameters.getFinalFixingLevel().doubleValue() <= parameters.getStrikeLevel().multiply(parameters.getInitialFixingLevel()).doubleValue()) {
			return parameters.getFinalFixingLevel().divide(parameters.getStrikeLevel().multiply(parameters.getInitialFixingLevel()), 20, RoundingMode.HALF_UP);
		} else {
			return new BigDecimal(1);
		}
	}

	@Override
	public boolean IsValidate(Parameters parameters) {
		return parameters.getFinalFixingLevel().doubleValue() > 0 &&
				parameters.getStrikeLevel().doubleValue() > 0;
	}
	
	@Override
	public BigDecimal getScenario1FinalFixingLevel(Parameters parameters) {
		return parameters.getStrikeLevel().multiply(parameters.getInitialFixingLevel());//.multiply(new BigDecimal(String.valueOf(scenarioPercentages.get(0))).divide(new BigDecimal("100"))));
	}
	
	@Override
	public BigDecimal getScenario2FinalFixingLevel(Parameters parameters) {
		return parameters.getStrikeLevel().multiply(new BigDecimal(String.valueOf(scenarioPercentages.get(1))).divide(new BigDecimal("100")));
	}
	
	@Override
	public BigDecimal getScenario3FinalFixingLevel(Parameters parameters) {
		return parameters.getStrikeLevel().multiply(new BigDecimal(String.valueOf(scenarioPercentages.get(2))).divide(new BigDecimal("100")));
	}
}
