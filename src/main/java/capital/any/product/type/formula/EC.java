package capital.any.product.type.formula;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Express Certificate
 * @author eyal.goren
 *
 */
public class EC extends Formula implements Serializable {

	private static final long serialVersionUID = -6754952869784761517L;

	@Override
	public BigDecimal getFormulaResult(Parameters parameters) {
		if (!parameters.isBarrierOccur() || parameters.getFinalFixingLevel().doubleValue() > parameters.getInitialFixingLevel().doubleValue()) {
			return new BigDecimal(1);
		} else {
			return parameters.getFinalFixingLevel().divide(parameters.getInitialFixingLevel(), 20, RoundingMode.HALF_UP);
		}
	}

	@Override
	public boolean IsValidate(Parameters parameters) {
		return parameters.getFinalFixingLevel().doubleValue() > 0 &&
				parameters.getInitialFixingLevel().doubleValue() > 0;
	}
	
	@Override
	public BigDecimal getScenario2FinalFixingLevel(Parameters parameters) {
		if (parameters.isBarrierOccur()) {
			return new BigDecimal("60");
		}
		return super.getScenario2FinalFixingLevel(parameters);
	}	
	
	@Override
	public double getScenario2FinalFixingLevelForTxt(Parameters parameters) {
		if (parameters.isBarrierOccur()) {
			return new BigDecimal(-40).doubleValue();
		}
		return (new BigDecimal("1").subtract(parameters.getBarrierLevel())).multiply(new BigDecimal("100")).doubleValue();
	}
	
	@Override
	public BigDecimal getScenario3FinalFixingLevel(Parameters parameters) {
		return parameters.getBarrierLevel().multiply(parameters.getInitialFixingLevel().multiply(new BigDecimal(String.valueOf(scenarioPercentages.get(2))).divide(new BigDecimal("100"))));
	}
	
	@Override
	public double getScenario3FinalFixingLevelForTxt(Parameters parameters) {
		return (new BigDecimal("1").subtract((parameters.getBarrierLevel().multiply(new BigDecimal(String.valueOf(scenarioPercentages.get(2))).divide(new BigDecimal("100")))))).multiply(new BigDecimal("100")).multiply(new BigDecimal("-1")).doubleValue();
	}
}
