package capital.any.product.type.formula;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

import capital.any.base.enums.ProductBarrierTypeEnum;

/**
 * Barrier Reverse Convertible
 * @author eyal.goren
 *
 */
public class BRC extends Formula implements Serializable {

	private static final long serialVersionUID = -6754952869784761517L;

	@Override
	public BigDecimal getFormulaResult(Parameters parameters) {
		if (!parameters.isBarrierOccur() || parameters.getFinalFixingLevel().doubleValue() > parameters.getInitialFixingLevel().doubleValue()) {
			return new BigDecimal(1);			
		} else {
			return parameters.getFinalFixingLevel().divide(parameters.getInitialFixingLevel(), 20, RoundingMode.HALF_UP);
		}
	}

	@Override
	public boolean IsValidate(Parameters parameters) {
		return parameters.getFinalFixingLevel().doubleValue() > 0 &&
				parameters.getInitialFixingLevel().doubleValue() > 0;
	}
	
	public BigDecimal getScenario1Result(Parameters parameters) {
		parameters.setFinalFixingLevel(getScenario1FinalFixingLevel(parameters));
		parameters.setBarrierOccur(false);
		return getFormulaResult(parameters);
	}
	
	@Override
	public BigDecimal getScenario1FinalFixingLevel(Parameters parameters) {
		return parameters.getBarrierLevel().multiply(parameters.getInitialFixingLevel().multiply(new BigDecimal(String.valueOf(scenarioPercentages.get(0))).divide(new BigDecimal("100"))));
	}
	
	public double getScenario1FinalFixingLevelForTxt(Parameters parameters) {
		return (new BigDecimal("1").subtract(parameters.getBarrierLevel())).multiply(new BigDecimal("100")).doubleValue();
	}
	
	@Override
	public BigDecimal getScenario2FinalFixingLevel(Parameters parameters) {
		if (parameters.isBarrierOccur()) {
			return new BigDecimal("0.6");
		}
		return parameters.getBarrierLevel().multiply(parameters.getInitialFixingLevel().multiply(new BigDecimal(String.valueOf(scenarioPercentages.get(1))).divide(new BigDecimal("100"))));
	}
	
	@Override
	public double getScenario2FinalFixingLevelForTxt(Parameters parameters) {
		if (parameters.isBarrierOccur()) {
			return new BigDecimal(-40).doubleValue();
		}
		double result = (new BigDecimal("1").subtract((parameters.getBarrierLevel().multiply(new BigDecimal(String.valueOf(scenarioPercentages.get(1))).divide(new BigDecimal("100")))))).multiply(new BigDecimal("100")).doubleValue();
		if (parameters.getProductBarrierType() != null && parameters.getProductBarrierType().getId() == ProductBarrierTypeEnum.European.getId()) {
			result = result * -1;
		}
		return result;
	}	
}
