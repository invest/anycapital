package capital.any.product.type.formula;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import capital.any.base.enums.ProductBarrierTypeEnum;

public abstract class Formula implements Serializable {
	
	private static final Logger logger = LoggerFactory.getLogger(Formula.class);
	private static final long serialVersionUID = 2674519831264087260L;
	
	protected ArrayList<Double> scenarioPercentages;
	
	public Formula() {
		super();
	}

	
	//get the formula result 
	public abstract BigDecimal getFormulaResult(Parameters parameters);
	//check that we have all the paramters for this formula
	public abstract boolean IsValidate(Parameters parameters);
	
	public BigDecimal getScenario1FinalFixingLevel(Parameters parameters) {
		return new BigDecimal(String.valueOf(scenarioPercentages.get(0))).divide(new BigDecimal("100"));
	}
	
	public double getScenario1FinalFixingLevelForTxt(Parameters parameters) {
		return formatFormulaResultForTxt(getScenario1FinalFixingLevel(parameters));
	}
	
	public BigDecimal getScenario1Result(Parameters parameters) {
		parameters.setFinalFixingLevel(getScenario1FinalFixingLevel(parameters));
		parameters.setBarrierOccur(isBarrierOccur(parameters));
		logger.debug("get Formula Result for scenario 1 parameters = " + parameters);
		return getFormulaResult(parameters);
	}
	
	public BigDecimal getScenario2FinalFixingLevel(Parameters parameters) {
		return new BigDecimal(String.valueOf(scenarioPercentages.get(1))).divide(new BigDecimal("100"));
	}
	
	public double getScenario2FinalFixingLevelForTxt(Parameters parameters) {
		return formatFormulaResultForTxt(getScenario2FinalFixingLevel(parameters));
	}
	
	public BigDecimal getScenario2Result(Parameters parameters) {
		parameters.setFinalFixingLevel(getScenario2FinalFixingLevel(parameters));
		parameters.setBarrierOccur(isBarrierOccur(parameters));
		logger.debug("get Formula Result for scenario 2 parameters = " + parameters);
		return getFormulaResult(parameters);
	}
	
	public BigDecimal getScenario3FinalFixingLevel(Parameters parameters) {
		return new BigDecimal(String.valueOf(scenarioPercentages.get(2))).divide(new BigDecimal("100"));
	}
	
	public double getScenario3FinalFixingLevelForTxt(Parameters parameters) {
		return formatFormulaResultForTxt(getScenario3FinalFixingLevel(parameters));
	}
	
	public BigDecimal getScenario3Result(Parameters parameters) {
		parameters.setFinalFixingLevel(getScenario3FinalFixingLevel(parameters));
		parameters.setBarrierOccur(isBarrierOccur(parameters));
		logger.debug("get Formula Result for scenario 3 parameters = " + parameters);
		return getFormulaResult(parameters);
	}
	
	private boolean isBarrierOccur(Parameters parameters) {
		if (parameters.isBarrierOccur()) {
			return true;
		}
		boolean isBarrierOccur = false;
		if (parameters.getBarrierLevel().doubleValue() > 1) {
			if (parameters.getBarrierLevel().multiply(parameters.getInitialFixingLevel()).doubleValue() <= parameters.getFinalFixingLevel().doubleValue()) {
				isBarrierOccur = true;
			}
		} else {
			if (parameters.getBarrierLevel().multiply(parameters.getInitialFixingLevel()).doubleValue() >= parameters.getFinalFixingLevel().doubleValue()) {
				isBarrierOccur = true;
			}
		}
		return isBarrierOccur;
	}

	public BigDecimal getSimulationCashSettlement(Parameters parameters) {
		if (parameters.getProductBarrierType() != null && parameters.getProductBarrierType().getId() == ProductBarrierTypeEnum.European.getId()) {
			parameters.setBarrierOccur(isBarrierOccur(parameters));
		}
		return getFormulaResult(parameters);
	}

	/**
	 * @return the scenarioPercentages
	 */
	public ArrayList<Double> getScenarioPercentages() {
		return scenarioPercentages;
	}

	/**
	 * @param scenarioPercentages the scenarioPercentages to set
	 */
	public void setScenarioPercentages(ArrayList<Double> scenarioPercentages) {
		this.scenarioPercentages = scenarioPercentages;
	}
	
	protected double formatFormulaResultForTxt(BigDecimal finalFixingLevelForTxt) {
		BigDecimal hundred = new BigDecimal("100");
		BigDecimal result = finalFixingLevelForTxt.multiply(hundred).subtract(hundred);
		result = result.setScale(2, RoundingMode.HALF_UP);
		return result.doubleValue();
	}
}
