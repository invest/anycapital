package capital.any.product.type.formula;

import java.io.Serializable;
import java.math.BigDecimal;

import capital.any.model.base.Product;
import capital.any.model.base.ProductBarrierType;

public class Parameters implements Serializable{
	private static final long serialVersionUID = 6270136846629415660L;
	
	private BigDecimal strikeLevel;
	private BigDecimal bonusPrecentage;
	private boolean isBarrierOccur;
	private BigDecimal finalFixingLevel;
	private BigDecimal initialFixingLevel;
	private BigDecimal levelOfProtection;
	private BigDecimal levelOfParticipation;
	private BigDecimal barrierLevel;
	private BigDecimal capLevel;
	private BigDecimal triggerLevel;
	private ProductBarrierType productBarrierType; 
	
	
//	public Parameters(BigDecimal strikeLevel, BigDecimal bonusPrecentage, boolean isBarrierOccur,
//			BigDecimal initialFixingLevel, BigDecimal levelOfProtection,
//			BigDecimal levelOfParticipation, BigDecimal barrierLevel, BigDecimal capLevel,BigDecimal triggerLevel) {
//		super();
//		this.strikeLevel = strikeLevel;
//		this.bonusPrecentage = bonusPrecentage;
//		this.isBarrierOccur = isBarrierOccur;
//		this.initialFixingLevel = initialFixingLevel;
//		this.levelOfProtection = levelOfProtection;
//		this.levelOfParticipation = levelOfParticipation;
//		this.barrierLevel = barrierLevel;
//		this.capLevel = capLevel;
//		this.triggerLevel = triggerLevel;
//	}
	
	public Parameters(Product product) {
		super();
		BigDecimal hundred = new BigDecimal("100");
		this.strikeLevel = new BigDecimal(String.valueOf(product.getStrikeLevel())).divide(hundred);
		this.bonusPrecentage = new BigDecimal(String.valueOf(product.getBonusLevel())).divide(hundred);
		this.isBarrierOccur = product.getProductBarrier() != null ? product.getProductBarrier().isBarrierOccur() : false;
		this.levelOfProtection = new BigDecimal(String.valueOf(product.getLevelOfProtection())).divide(hundred);
		this.levelOfParticipation = new BigDecimal(String.valueOf(product.getLevelOfParticipation())).divide(hundred);
		this.barrierLevel = new BigDecimal(String.valueOf(product.getProductBarrier() != null ? product.getProductBarrier().getBarrierLevel() : 0)).divide(hundred);
		this.capLevel = new BigDecimal(String.valueOf(product.getCapLevel())).divide(hundred);
		this.triggerLevel = new BigDecimal(String.valueOf((product.getProductCoupons() != null && product.getProductCoupons().size() > 0) ?  product.getProductCoupons().get(0).getTriggerLevel() : 0)).divide(hundred);
		this.productBarrierType = product.getProductBarrier() != null ? product.getProductBarrier().getProductBarrierType() : null;
	}
	
	public Parameters(Product product, BigDecimal initialFixingLevel) {
		this(product);
		this.initialFixingLevel = initialFixingLevel;
	}
	
	public Parameters(Product product, BigDecimal initialFixingLevel, BigDecimal finalFixingLevel) {
		this(product, initialFixingLevel);
		this.finalFixingLevel = finalFixingLevel;
	}
	
	public Parameters(Parameters parameters) {
		this.strikeLevel = parameters.strikeLevel;
		this.bonusPrecentage = parameters.bonusPrecentage;
		this.isBarrierOccur = parameters.isBarrierOccur;
		this.levelOfProtection = parameters.levelOfProtection;
		this.levelOfParticipation = parameters.levelOfParticipation;
		this.barrierLevel = parameters.barrierLevel;
		this.capLevel = parameters.capLevel;
		this.triggerLevel = parameters.triggerLevel;
		this.initialFixingLevel = parameters.initialFixingLevel;
		this.finalFixingLevel = parameters.finalFixingLevel;
		this.productBarrierType = parameters.productBarrierType;
	}
	
	/**
	 * @return the barrierLevel
	 */
	public BigDecimal getBarrierLevel() {
		return barrierLevel;
	}

	/**
	 * @param barrierLevel the barrierLevel to set
	 */
	public void setBarrierLevel(BigDecimal barrierLevel) {
		this.barrierLevel = barrierLevel;
	}

	/**
	 * @return the strikeLevel
	 */
	public BigDecimal getStrikeLevel() {
		return strikeLevel;
	}
	
	/**
	 * @param strikeLevel the strikeLevel to set
	 */
	public void setStrikeLevel(BigDecimal strikeLevel) {
		this.strikeLevel = strikeLevel;
	}
	
	/**
	 * @return the bonusPrecentage
	 */
	public BigDecimal getBonusPrecentage() {
		return bonusPrecentage;
	}
	
	/**
	 * @param bonusPrecentage the bonusPrecentage to set
	 */
	public void setBonusPrecentage(BigDecimal bonusPrecentage) {
		this.bonusPrecentage = bonusPrecentage;
	}
	/**
	 * @return the isBarrierOccur
	 */
	public boolean isBarrierOccur() {
		return isBarrierOccur;
	}
	/**
	 * @param isBarrierOccur the isBarrierOccur to set
	 */
	public void setBarrierOccur(boolean isBarrierOccur) {
		this.isBarrierOccur = isBarrierOccur;
	}
	/**
	 * @return the finalFixingLevel
	 */
	public BigDecimal getFinalFixingLevel() {
		return finalFixingLevel;
	}
	/**
	 * @param finalFixingLevel the finalFixingLevel to set
	 */
	public void setFinalFixingLevel(BigDecimal finalFixingLevel) {
		this.finalFixingLevel = finalFixingLevel;
	}
	/**
	 * @return the initialFixingLevel
	 */
	public BigDecimal getInitialFixingLevel() {
		return initialFixingLevel;
	}
	/**
	 * @param initialFixingLevel the initialFixingLevel to set
	 */
	public void setInitialFixingLevel(BigDecimal initialFixingLevel) {
		this.initialFixingLevel = initialFixingLevel;
	}
	/**
	 * @return the levelOfProtection
	 */
	public BigDecimal getLevelOfProtection() {
		return levelOfProtection;
	}
	/**
	 * @param levelOfProtection the levelOfProtection to set
	 */
	public void setLevelOfProtection(BigDecimal levelOfProtection) {
		this.levelOfProtection = levelOfProtection;
	}
	/**
	 * @return the levelOfParticipation
	 */
	public BigDecimal getLevelOfParticipation() {
		return levelOfParticipation;
	}
	/**
	 * @param levelOfParticipation the levelOfParticipation to set
	 */
	public void setLevelOfParticipation(BigDecimal levelOfParticipation) {
		this.levelOfParticipation = levelOfParticipation;
	}

	/**
	 * @return the capLevel
	 */
	public BigDecimal getCapLevel() {
		return capLevel;
	}

	/**
	 * @param capLevel the capLevel to set
	 */
	public void setCapLevel(BigDecimal capLevel) {
		this.capLevel = capLevel;
	}

	/**
	 * @return the triggerLevel
	 */
	public BigDecimal getTriggerLevel() {
		return triggerLevel;
	}

	/**
	 * @param triggerLevel the triggerLevel to set
	 */
	public void setTriggerLevel(BigDecimal triggerLevel) {
		this.triggerLevel = triggerLevel;
	}

	/**
	 * @return the productBarrierType
	 */
	public ProductBarrierType getProductBarrierType() {
		return productBarrierType;
	}

	/**
	 * @param productBarrierType the productBarrierType to set
	 */
	public void setProductBarrierType(ProductBarrierType productBarrierType) {
		this.productBarrierType = productBarrierType;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Parameters [strikeLevel=" + strikeLevel + ", bonusPrecentage=" + bonusPrecentage + ", isBarrierOccur="
				+ isBarrierOccur + ", finalFixingLevel=" + finalFixingLevel + ", initialFixingLevel="
				+ initialFixingLevel + ", levelOfProtection=" + levelOfProtection + ", levelOfParticipation="
				+ levelOfParticipation + ", barrierLevel=" + barrierLevel + ", capLevel=" + capLevel + ", triggerLevel="
				+ triggerLevel + ", productBarrierType=" + productBarrierType + "]";
	}
}
