package capital.any.product.type.formula;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Capital Protection Cartificate with Participation
 * @author eyal.goren
 *
 */
public class CPCP extends Formula implements Serializable {

	private static final long serialVersionUID = -6754952869784761517L;

	@Override
	public BigDecimal getFormulaResult(Parameters parameters) {
		if (parameters.getFinalFixingLevel().doubleValue() <= parameters.getInitialFixingLevel().doubleValue()) {
			return parameters.getLevelOfProtection();
		} else {
			return parameters.getLevelOfProtection().add(
					parameters.getLevelOfParticipation().multiply(
						(parameters.getFinalFixingLevel().subtract(
								parameters.getInitialFixingLevel()))).divide(
										parameters.getInitialFixingLevel(), 20, RoundingMode.HALF_UP));
		}
	}

	@Override
	public boolean IsValidate(Parameters parameters) {
		return parameters.getFinalFixingLevel().doubleValue() > 0 &&
				parameters.getInitialFixingLevel().doubleValue() > 0;
	}	
}
