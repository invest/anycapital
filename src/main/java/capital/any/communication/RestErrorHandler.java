package capital.any.communication;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import capital.any.communication.Error;
import capital.any.communication.Message;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;

/**
 * @author Eyal G
 * exception handler component that processes the exceptions thrown by our controllers
 */
@ControllerAdvice
public class RestErrorHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(RestErrorHandler.class);
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ResponseEntity<Response<Object>> processValidationError(MethodArgumentNotValidException ex) {
        BindingResult result = ex.getBindingResult();
        ArrayList<Message> messages = new ArrayList<Message>();
        for (FieldError fieldError : result.getFieldErrors()) {
        	messages.add(new Message(fieldError.getDefaultMessage(), fieldError.getField()));
		}
        Error error = new Error(messages.get(0).getContent(), messages);
        return new ResponseEntity<Response<Object>>(new Response<Object>(ResponseCode.INVALID_INPUT, null, error), HttpStatus.OK);
    }

}
