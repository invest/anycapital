package capital.any.communication;

import java.io.Serializable;

/**
 * @author LioR SoLoMoN
 *
 */
public class Message implements Serializable {

	private static final long serialVersionUID = 8439514828181305615L;
	
	private String content;
	private String field;
	
	public Message() {
	}
	
	/**
	 * @param content
	 */
	public Message(String content) {
		this.content = content;
	}
	
	/**
	 * @param content
	 * @param field
	 */
	public Message(String content, String field) {
		this.content = content;
		this.field = field;
	}
	
	/**
	 * @return the field
	 */
	public String getField() {
		return field;
	}
	/**
	 * @param field the field to set
	 */
	public void setField(String field) {
		this.field = field;
	}
	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}
	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}
}
