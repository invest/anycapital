package capital.any.communication;

import java.io.Serializable;
import javax.validation.Valid;
import capital.any.base.enums.ActionSource;

/**
 * @author LioR SoLoMoN
 *
 */
public class Request<T> implements Serializable {

	private static final long serialVersionUID = 5869786216855441232L;
	@Valid
	private T data;
	private ActionSource actionSource;
    
    /**
     * empty const'
     */
    public Request() {
    	
    }
    
    /**
     * @param data
     * @param actionSource
     */
    public Request(T data, ActionSource actionSource) { 
    	this.data = data;
    	this.setActionSource(actionSource);
	}

	/**
	 * @return the data
	 */
	public Object getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(T data) {
		this.data = data;
	}

	/**
	 * @return the actionSource
	 */
	public ActionSource getActionSource() {
		return actionSource;
	}

	/**
	 * @param actionSource the actionSource to set
	 */
	public void setActionSource(ActionSource actionSource) {
		this.actionSource = actionSource;
	}

	@Override
	public String toString() {
		return "Request [data=" + data + ", actionSource=" + actionSource + "]";
	}
}
