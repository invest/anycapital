package capital.any.communication;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;


/**
 * @author LioR SoLoMoN
 *
 */
public class Response<T> implements Serializable {

	private static final long serialVersionUID = 5869786216855441232L;
	private T data;
	private ResponseCode responseCode;
    private List<String> messages;
    private Error error;
    
    /**
     * empty const'
     */
    public Response() {
    	
    }
    
    /**
     * @param data
     * @param responseCode
     */
    public Response(T data, ResponseCode responseCode) { 
    	this.data = data;
    	this.responseCode = responseCode;
	}

    /**
     * @param data
     * @param responseCode
     * @param messages
     */
    public Response(T data, ResponseCode responseCode, List<String> messages) { 
    	this(data, responseCode);
		this.messages = messages;
	}
    
    /**
     * @param responseCode
     * @param messages
     * @param error
     */
    public Response(ResponseCode responseCode, List<String> messages, Error error) { 
    	this.responseCode = responseCode;
		this.messages = messages;
		this.error = error;
	}
    
    /**
     * @param data
     * @param responseCode
     * @param messages
     * @param error
     */
    public Response(T data, ResponseCode responseCode, List<String> messages, Error error) {
    	this(data, responseCode, messages);
		this.error = error;
	}

    @Override
	public String toString() {
		return "Response [data=" + data + ", responseCode=" + responseCode + ", messages=" + messages + ", error="
				+ error + "]";
	}

	/**
     * @author LioR SoLoMoN
     *
     */
    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
	public enum ResponseCode {
    	/**
    	 * {@code 0}
    	 */
    	OK(0) 
    	
    	//5xx 	
    	/**
    	 * {@code 501}
    	 */
    	,WITHDRAW_BALANCE_VALIDATION(501)
    	/**
    	 * {@code 502}
    	 */
    	,WITHDRAW_OPEN_VALIDATION(502)
    	/**
    	 * {@code 510}
    	 */
    	,WITHDRAW_VALIDATION_FAILURE(510, WITHDRAW_BALANCE_VALIDATION, WITHDRAW_OPEN_VALIDATION) 	  	
    	/**
    	 * {@code 511}
    	 */
    	,WITHDRAW_MINIMUM_AMOUNT_LIMITATION(511)	
    	/**
    	 * {@code 512}
    	 */
    	,WITHDRAW_FEE_AMOUNT_LIMITATION(512)
       	/**
    	 * {@code 520}
    	 */
    	,WITHDRAW_LIMITATION_FAILURE(520, WITHDRAW_MINIMUM_AMOUNT_LIMITATION, WITHDRAW_FEE_AMOUNT_LIMITATION) 
    	 
    	//6xx Communication
    	/**
    	 * {@code 600}
    	 *  user / writer not have permission to the method
    	 */
    	,NO_PERMISSION(600)
    	/**
    	 * {@code 601}
    	 *  Not logged in - user / writer is not login to the system
    	 */
    	,ACCESS_DENIED(601)
    	
    	/**
    	 * {@code 602}
    	 * User / Writer logging failed
    	 */
    	,AUTHENTICATION_FAILURE(602)
    	/**
    	 * {@code 603}
    	 */
    	,SESSION_INVALID(603)
    	/**
    	 * {@code 604}
    	 */
    	,REGISTER_RESTRICTED(604)
    	/**
    	 * {@code 605}
    	 */
    	,ACCOUNT_LOCKED(605)    	
    	//7xx User input
    	/**
    	 * {@code 701}
    	 */
    	,INVALID_INPUT(701)
    	/**
    	 * {@code 705}
    	 */
    	,INVALID_PASSWORD(705)
    	//8xx Server
    	/**
    	 * {@code 801}
    	 */
    	,OPERATION_FAILED(801)
    	/**
    	 * {@code 802}
    	 */
    	,OPERATION_DENIED(802)
    	/**
    	 * {@code 811}
    	 */
    	,OPERATION_DAO_FAILED(811)
    	/**
    	 * {@code 710}
    	 */
    	,INVESTMENT_ERROR_GENERAL(710)
    	/**
    	 * {@code 711}
    	 */
    	,INVESTMENT_ERROR_INSUFFICIENT_BALANCE_LESS_THEN_MIN_INV(711)
    	/**
    	 * {@code 712}
    	 */
    	,INVESTMENT_ERROR_LIMIT_OF_ONE_PENDING_INVESTMENT_PER_PRODUCT_IN_PRIMARY(712)
    	/**
    	 * {@code 713}
    	 */
    	,INVESTMENT_ERROR_MIN_INV_LIMIT(713)
    	/**
    	 * {@code 714}
    	 */
    	,INVESTMENT_ERROR_MAX_INV_LIMIT(714)
    	/**
    	 * {@code 715}
    	 */
    	,INVESTMENT_ERROR_PRODUCT_IS_CURRENTLY_UNAVAILABLE(715)
    	/**
    	 * {@code 716}
    	 */
    	,INVESTMENT_ERROR_INVESTMENT_PRAIMERY_PRODUCT_SECONDARY(716)
    	/**
    	 * {@code 717}
    	 */
    	,INVESTMENT_ERROR_INVESTMENT_SECONDARY_PRODUCT_PRAIMERY(717)
    	/**
    	 * {@code 718}
    	 */
    	,INVESTMENT_ERROR_PRODUCT_SUSPENDED(718)
    	/**
    	 * {@code 719}
    	 */
    	,INVESTMENT_ERROR_DEVIATION_ASK(719)
    	/**
    	 * {@code 720}
    	 */
    	,INVESTMENT_ERROR_INSUFFICIENT_BALANCE_MORE_THEN_MIN_INV(720)
    	/**
    	 * {@code 721}
    	 */
    	,INVESTMENT_ERROR_USER_NOT_REGULATED(721)
    	/**
    	 * {@code 730}
    	 */
    	,INVESTMENT_ERROR_SELL_NOW_BID(730)
    	/**
    	 * {@code 750}
    	 */
    	,INVESTMENT_ERROR_CANCEL_NEGATIVE_BALANCE(750)
    	
    	// 9xx General / Unknown / Not Specific nor Related.
    	/**
    	 * {@code 999}
    	 */
    	,UNKNOWN_ERROR(999); 
    	
		private static final Logger logger = LoggerFactory.getLogger(ResponseCode.class);
		private static final Map<Integer, ResponseCode> ID_RESPONSE_MAP = new HashMap<Integer, ResponseCode>(ResponseCode.values().length);
		static {
			for (ResponseCode g : ResponseCode.values()) {
				ID_RESPONSE_MAP.put(g.getCode(), g);
			}
		}
		
    	private int code;
    	private ResponseCode[] responseCodes; 
    	
    	private ResponseCode(int code) {
    		this.code = code;
    	}
    	
    	private ResponseCode(int code, ResponseCode... responseCodes) {
    		this.code = code;
    		this.responseCodes = responseCodes;
    	}
    	
    	public boolean isOK() {
    		return code == ResponseCode.OK.code;
    	}

		/**
		 * @return the code
		 */
    	@JsonValue
		public int getCode() {
			return code;
		}
		
		/**
		 * @return the name
		 */
		public String getName() {
			return name();
		}

		/**
		 * @param code the code to set
		 */
		public void setCode(int code) {
			this.code = code;
		}
		
		/**
		 * @param id
		 * @return ActionSource by a given id
		 */
		@JsonCreator 
	    public static ResponseCode get(String id) {
			ResponseCode responseCode = null;
			try {
				responseCode = ID_RESPONSE_MAP.get(Integer.valueOf(id));
		        if (responseCode == null) {
		            throw new IllegalArgumentException(id + " has no corresponding value for ResponseCode");
		        }
			} catch (NumberFormatException ne) {
				logger.error("ERROR! ResponseCode; send a number!");
				throw new IllegalArgumentException(id + " has no corresponding value for ResponseCode");
			}
			
	        return responseCode;
	    }

		/**
		 * @return the responseCode
		 */
		public ResponseCode[] getResponseCodes() {
			return responseCodes;
		}
    }


	/**
	 * @return the error
	 */
	public Error getError() {
		return error;
	}

	/**
	 * @param error the error to set
	 */
	public void setError(Error error) {
		this.error = error;
	}

	/**
	 * @return the responseCode
	 */
	public ResponseCode getResponseCode() {
		return responseCode;
	}

	/**
	 * @param responseCode the responseCode to set
	 */
	public void setResponseCode(ResponseCode responseCode) {
		this.responseCode = responseCode;
	}

	/**
	 * @return the messages
	 */
	public List<String> getMessages() {
		return messages;
	}

	/**
	 * @param messages the messages to set
	 */
	public void setMessages(List<String> messages) {
		this.messages = messages;
	}

	/**
	 * @return the data
	 */
	public Object getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(T data) {
		this.data = data;
	}
}
