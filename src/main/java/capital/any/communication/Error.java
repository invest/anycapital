package capital.any.communication;

import java.io.Serializable;
import java.util.List;

/**
 * @author LioR SoLoMoN
 *
 */
public class Error implements Serializable {

	private static final long serialVersionUID = 2414839418269596678L;
	private String reason;
    private List<Message> messages;
    
    public Error() {
    	
    }
    
    public Error(Error error) {
    	this.reason = error.reason;
    	this.messages = error.messages;
    }

	/**
	 * @param reason
	 * @param messages
	 */
	public Error(String reason, List<Message> messages) {
		this.reason = reason;
		this.messages = messages;
	}

	/**
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * @param reason the reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * @return the messages
	 */
	public List<Message> getMessages() {
		return messages;
	}

	/**
	 * @param messages the messages to set
	 */
	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}
}
