package capital.any.hazelcast.update.base.product;

import capital.any.base.enums.ProductStatusEnum;
import capital.any.hazelcast.update.base.UpdateEntry;
import capital.any.model.base.Product;
import capital.any.model.base.ProductStatus;

/**
 * @author LioR SoLoMoN
 *
 */
public class UpdateProductStatusEntry implements UpdateEntry {
	private ProductStatusEnum productStatus;
	private Product product;
	
	public UpdateProductStatusEntry(ProductStatusEnum productStatus) {
		this.productStatus = productStatus;
	}

	@Override
	public Product update(Object data) {
		Product product = (Product)data;
		product.setProductStatus(new ProductStatus(productStatus.getId()));
		this.product = product;
		return product;	
	}
	
	public Product getProduct() {
		return this.product;
	}
}
