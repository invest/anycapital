package capital.any.hazelcast.update.base.product;

import java.util.ArrayList;
import java.util.List;

import capital.any.hazelcast.update.base.UpdateEntry;
import capital.any.model.base.Product;
import capital.any.model.base.ProductKidLanguage;

/**
 * 
 * @author Eyal.o
 *
 */
public class UpdateProductKidLanguageEntry implements UpdateEntry {

	private Product product;
	private ProductKidLanguage productKidLanguage;
	
	public UpdateProductKidLanguageEntry(ProductKidLanguage productKidLanguage) {
		this.productKidLanguage = productKidLanguage;
	}
	
	@Override
	public Object update(Object data) {
		Product product = (Product)data;
		List<ProductKidLanguage> list = product.getProductKidLanguages();
		if (null == list) {
			list = new ArrayList<ProductKidLanguage>();
		}
		product.getProductKidLanguages().add(this.productKidLanguage);
		this.product = product;
		return product;	
	}

	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}
	
}
