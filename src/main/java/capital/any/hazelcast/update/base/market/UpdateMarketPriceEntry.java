package capital.any.hazelcast.update.base.market;

import capital.any.hazelcast.update.base.UpdateEntry;
import capital.any.model.base.Market;

/**
 * @author LioR SoLoMoN
 *
 */
public class UpdateMarketPriceEntry implements UpdateEntry {
	private Market market;
	private double price;
	
	public UpdateMarketPriceEntry(double price) {
		this.price = price;
	}

	@Override
	public Market update(Object data) {		
		Market market = (Market)data;
		market.getLastPrice().setPrice(price);
		this.market = market;
		return market;	
	}
	
	public Market getMarket() {
		return this.market;
	}
}
