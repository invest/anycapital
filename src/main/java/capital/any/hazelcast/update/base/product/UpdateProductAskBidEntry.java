package capital.any.hazelcast.update.base.product;

import capital.any.hazelcast.update.base.UpdateEntry;
import capital.any.model.base.Product;

/**
 * @author LioR SoLoMoN
 *
 */
public class UpdateProductAskBidEntry implements UpdateEntry {
	private Product product;
	
	public UpdateProductAskBidEntry(Product product) {
		this.product = product;
	}

	@Override
	public Product update(Object data) {		
		Product product = (Product)data;
		product.setAsk(this.product.getAsk());
		product.setBid(this.product.getBid());
		this.product = product;
		return product;	
	}
	
	/**
	 * @return the product
	 */
	public Product getProduct() {
		return this.product;
	}
}
