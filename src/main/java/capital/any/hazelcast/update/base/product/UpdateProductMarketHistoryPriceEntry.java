package capital.any.hazelcast.update.base.product;

import capital.any.hazelcast.update.base.UpdateEntry;
import capital.any.model.base.MarketPrice;
import capital.any.model.base.Product;
import capital.any.model.base.ProductMarket;

/**
 * @author Eyal Goren
 *
 */
public class UpdateProductMarketHistoryPriceEntry implements UpdateEntry {
	private Product product;
	private MarketPrice marketPrice;
	
	public UpdateProductMarketHistoryPriceEntry(MarketPrice marketPrice) {
		this.marketPrice = marketPrice;
	}

	@Override
	public Product update(Object data) {
		Product product = (Product)data;
		for (ProductMarket productMarket : product.getProductMarkets()) {
			if (productMarket.getMarket().getId() == marketPrice.getMarketId()) {
				productMarket.getMarket().getHistoryMarketPrice().add(marketPrice);
				break;
			}
		}
		this.product = product;
		return product;
	}

	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}
}
