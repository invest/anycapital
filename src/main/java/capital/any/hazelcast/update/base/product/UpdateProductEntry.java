package capital.any.hazelcast.update.base.product;

import capital.any.hazelcast.update.base.UpdateEntry;
import capital.any.model.base.Product;

/**
 * @author LioR SoLoMoN
 *
 */
public class UpdateProductEntry implements UpdateEntry {
	private Product product;
	private Product productUpdate;
	
	public UpdateProductEntry(Product product) {
		this.productUpdate = product;
	}

	@Override
	public Product update(Object data) {
		Product product = (Product)data;
		product.setBondFloorAtissuance(productUpdate.getBondFloorAtissuance());
		product.setBonusLevel(productUpdate.getBonusLevel());
		product.setCapLevel(productUpdate.getCapLevel());
		product.setCurrencyId(productUpdate.getCurrencyId());
		product.setDayCountConvention(productUpdate.getDayCountConvention());
		product.setDenomination(productUpdate.getDenomination());
		product.setFinalFixingDate(productUpdate.getFinalFixingDate());
		product.setFirstExchangeTradingDate(productUpdate.getFirstExchangeTradingDate());
		product.setInitialFixingDate(productUpdate.getInitialFixingDate());
		product.setIssueDate(productUpdate.getIssueDate());
		product.setIssuePrice(productUpdate.getIssuePrice());
		product.setIssueSize(productUpdate.getIssueSize());
		product.setLastTradingDate(productUpdate.getLastTradingDate());
		product.setLevelOfParticipation(productUpdate.getLevelOfParticipation());
		product.setLevelOfProtection(productUpdate.getLevelOfProtection());
		product.setMaxYield(productUpdate.getMaxYield());
		product.setMaxYieldPA(productUpdate.getMaxYieldPA());
		product.setProductType(productUpdate.getProductType());
		product.setProductCouponFrequency(productUpdate.getProductCouponFrequency());
		product.setProductInsuranceTypeId(productUpdate.getProductInsuranceTypeId());
		product.setQuanto(productUpdate.isQuanto());
		product.setRedemptionDate(productUpdate.getRedemptionDate());
		product.setStrikeLevel(productUpdate.getStrikeLevel());
		product.setSubscriptionEndDate(productUpdate.getSubscriptionEndDate());
		product.setSubscriptionStartDate(productUpdate.getSubscriptionStartDate());
		product.setUpside(productUpdate.getUpside());
		this.product = product;
		return product;	
	}

	/**
	 * @return the product
	 */
	public Product getProduct() {
		return this.product;
	}
}
