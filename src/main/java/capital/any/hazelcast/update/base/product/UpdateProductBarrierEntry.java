package capital.any.hazelcast.update.base.product;

import capital.any.hazelcast.update.base.UpdateEntry;
import capital.any.model.base.Product;
import capital.any.model.base.ProductBarrier;

/**
 * @author LioR SoLoMoN
 *
 */
public class UpdateProductBarrierEntry implements UpdateEntry {
	private Product product;
	private ProductBarrier productBarrier;
	
	public UpdateProductBarrierEntry(ProductBarrier productBarrier) {
		this.productBarrier = productBarrier;
	}

	@Override
	public Product update(Object data) {
		Product product = (Product)data;
		product.setProductBarrier(productBarrier);
		this.product = product;
		return product;	
	}

	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}
}
