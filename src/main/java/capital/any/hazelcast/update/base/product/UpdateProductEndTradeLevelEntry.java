package capital.any.hazelcast.update.base.product;

import java.util.List;
import capital.any.hazelcast.update.base.UpdateEntry;
import capital.any.model.base.Product;
import capital.any.model.base.ProductMarket;

/**
 * @author LioR SoLoMoN
 *
 */
public class UpdateProductEndTradeLevelEntry implements UpdateEntry {
	private Product product;
	private List<ProductMarket> productMarkets;
	
	public UpdateProductEndTradeLevelEntry(List<ProductMarket> productMarkets) {
		this.productMarkets = productMarkets;
	}

	@Override
	public Product update(Object data) {
		Product product = (Product)data;
		List<ProductMarket> productMarkets = product.getProductMarkets();		
		for (ProductMarket productMarketNew : this.productMarkets) {
			for (ProductMarket productMarket : productMarkets) {
				if (productMarket.getMarket().getId() == productMarketNew.getMarket().getId()) {
					productMarket.setEndTradeLevel(productMarketNew.getEndTradeLevel());
					break;
				}
			}
		}
		this.product = product;
		return product;	
	}

	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}
}
