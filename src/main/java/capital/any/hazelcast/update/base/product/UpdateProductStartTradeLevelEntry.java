package capital.any.hazelcast.update.base.product;

import java.math.BigDecimal;
import java.util.List;
import capital.any.base.enums.ProductStatusEnum;
import capital.any.hazelcast.update.base.UpdateEntry;
import capital.any.model.base.Product;
import capital.any.model.base.ProductMarket;

/**
 * @author LioR SoLoMoN
 *
 */
public class UpdateProductStartTradeLevelEntry implements UpdateEntry {
	private Product product;
	private List<ProductMarket> productMarkets;
	
	public UpdateProductStartTradeLevelEntry(List<ProductMarket> productMarkets) {
		this.productMarkets = productMarkets;
	}

	@Override
	public Product update(Object data) {
		Product product = (Product)data;
		List<ProductMarket> productMarkets = product.getProductMarkets();		
		for (ProductMarket productMarketNew : this.productMarkets) {
			for (ProductMarket productMarket : productMarkets) {
				if (productMarket.getMarket().getId() == productMarketNew.getMarket().getId()) {
					productMarket.setStartTradeLevel(productMarketNew.getStartTradeLevel());
					break;
				}
			}
		}
		product.calculateScenarios();
		if (product.getProductBarrier() != null) {
			if (ProductStatusEnum.SUBSCRIPTION.getId() < product.getProductStatus().getId()) {
				product.getProductBarrier().calculateDistanceToBarrier(new BigDecimal(String.valueOf(product.getProductMarkets().get(0).getMarket().getLastPrice().getPrice())), 
																				new BigDecimal(String.valueOf(product.getProductMarkets().get(0).getStartTradeLevel())));
			}
		}
		this.product = product;
		return product;	
	}

	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}
}
