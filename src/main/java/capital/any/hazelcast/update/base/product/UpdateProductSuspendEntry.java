package capital.any.hazelcast.update.base.product;

import capital.any.hazelcast.update.base.UpdateEntry;
import capital.any.model.base.Product;

/**
 * @author LioR SoLoMoN
 *
 */
public class UpdateProductSuspendEntry implements UpdateEntry {
	private Product product;
	private boolean isSuspend;
	
	public UpdateProductSuspendEntry(boolean isSuspend) {
		this.isSuspend = isSuspend;
	}

	@Override
	public Product update(Object data) {
		Product product = (Product)data;
		product.setSuspend(isSuspend);
		this.product = product;
		return product;	
	}

	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}
}
