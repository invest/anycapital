package capital.any.hazelcast.update.base.dbParameter;

import capital.any.hazelcast.update.base.UpdateEntry;
import capital.any.model.base.DBParameter;

/**
 * update db paramter
 * @author eyal.G
 *
 */
public class UpdateDBParameterEntry implements UpdateEntry {
	private DBParameter dbParameter;
	
	public UpdateDBParameterEntry(DBParameter dbParameter) {
		this.dbParameter = dbParameter;
	}

	@Override
	public DBParameter update(Object data) {
		DBParameter dbParameter = (DBParameter) data;
		dbParameter.setName(this.dbParameter.getName());
		dbParameter.setComments(this.dbParameter.getComments());
		dbParameter.setStringValue(this.dbParameter.getStringValue());
		dbParameter.setDateValue(this.dbParameter.getDateValue());
		dbParameter.setNumValue(this.dbParameter.getNumValue());
		this.dbParameter = dbParameter;
		return dbParameter;
	}

	/**
	 * @return the marketingLandingPage
	 */
	public DBParameter getDBParameter() {
		return dbParameter;
	}
	
}
