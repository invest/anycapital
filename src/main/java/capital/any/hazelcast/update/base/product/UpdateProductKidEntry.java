package capital.any.hazelcast.update.base.product;

import capital.any.hazelcast.update.base.UpdateEntry;
import capital.any.model.base.Product;

/**
 * 
 * @author eyal.o
 *
 */
public class UpdateProductKidEntry implements UpdateEntry {
	private Product product;
	
	public UpdateProductKidEntry(Product product) {
		this.product = product;
	}
	
	@Override
	public Product update(Object data) {		
		Product product = (Product)data;
		product.setKidName(this.product.getKidName());
		this.product = product;
		return product;	
	}

	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}
	
	
}
