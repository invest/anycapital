package capital.any.hazelcast.update.base.product;

import capital.any.hazelcast.update.base.UpdateEntry;
import capital.any.model.base.Product;

/**
 * @author LioR SoLoMoN
 *
 */
public class UpdateProductPriorityEntry implements UpdateEntry {
	private Product product;
	private int priority;
	
	public UpdateProductPriorityEntry(int priority) {
		this.priority = priority;
	}

	@Override
	public Product update(Object data) {		
		Product product = (Product)data;
		product.setPriority(priority);
		this.product = product;
		return product;
	}
	
	/**
	 * @return the product
	 */
	public Product getProduct() {
		return this.product;
	}
}
