package capital.any.hazelcast.update.base;

/**
 * @author LioR SoLoMoN
 *
 */
public interface UpdateEntry {

	/**
	 * @param data
	 * @return
	 */
	Object update(Object data);

}
