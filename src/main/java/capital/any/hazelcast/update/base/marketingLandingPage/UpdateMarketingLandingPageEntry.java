package capital.any.hazelcast.update.base.marketingLandingPage;

import capital.any.hazelcast.update.base.UpdateEntry;
import capital.any.model.base.MarketingLandingPage;

/**
 * 
 * @author eyal.ohana
 *
 */
public class UpdateMarketingLandingPageEntry implements UpdateEntry {
	private MarketingLandingPage marketingLandingPage;
	
	public UpdateMarketingLandingPageEntry(MarketingLandingPage marketingLandingPage) {
		this.marketingLandingPage = marketingLandingPage;
	}

	@Override
	public MarketingLandingPage update(Object data) {
		MarketingLandingPage marketingLandingPage = (MarketingLandingPage) data;
		marketingLandingPage.setName(this.marketingLandingPage.getName());
		marketingLandingPage.setPath(this.marketingLandingPage.getPath());
		this.marketingLandingPage = marketingLandingPage;
		return marketingLandingPage;
	}

	/**
	 * @return the marketingLandingPage
	 */
	public MarketingLandingPage getMarketingLandingPage() {
		return marketingLandingPage;
	}
	
}
