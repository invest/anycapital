package capital.any.hazelcast.update.base.message.resource;

import java.util.List;
import java.util.Map;
import capital.any.hazelcast.update.base.UpdateEntry;
import capital.any.model.base.MsgResLanguage;

/**
 * @author LioR SoLoMoN
 *
 */
public class UpdateMessageResourceEntry implements UpdateEntry {
	private Map<Integer, Map<String, String>> messageResourceLanguages;
	private List<MsgResLanguage> messageResourceLanguagesNew;
	
	public UpdateMessageResourceEntry(List<MsgResLanguage> messageResourceLanguagesNew) {
		this.messageResourceLanguagesNew = messageResourceLanguagesNew;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, Map<String, String>> update(Object data) {		
		Map<Integer, Map<String, String>> messageResourceLanguages = (Map<Integer, Map<String, String>>)data;
		for (MsgResLanguage msgResLanguage : this.messageResourceLanguagesNew) {
			Map<String, String> language = messageResourceLanguages.get(msgResLanguage.getMsgResLanguageId());
			language.put(msgResLanguage.getMsgRes().getKey(),
					msgResLanguage.getLargeValue() == null ? msgResLanguage.getValue() : msgResLanguage.getLargeValue());			
		}
		this.messageResourceLanguages = messageResourceLanguages;
		return messageResourceLanguages;	
	}
	
	/**
	 * @return messageResourceLanguages
	 */
	public Map<Integer, Map<String, String>> getMessageResourceLanguages() {
		return this.messageResourceLanguages;
	}
}
