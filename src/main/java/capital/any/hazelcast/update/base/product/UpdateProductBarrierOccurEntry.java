package capital.any.hazelcast.update.base.product;

import capital.any.hazelcast.update.base.UpdateEntry;
import capital.any.model.base.Product;

/**
 * @author LioR SoLoMoN
 *
 */
public class UpdateProductBarrierOccurEntry implements UpdateEntry {
	private Product product;
	private boolean isBarrierOccur;
	
	public UpdateProductBarrierOccurEntry(boolean isBarrierOccur) {
		this.isBarrierOccur = isBarrierOccur;
	}

	@Override
	public Product update(Object data) {
		Product product = (Product)data;
		product.getProductBarrier().setBarrierOccur(isBarrierOccur);
		product.calculateScenarios();
		this.product = product;
		return product;	
	}

	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}
}
