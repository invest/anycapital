package capital.any.hazelcast;

import com.hazelcast.core.IMap;
import com.hazelcast.map.EntryProcessor;
import com.hazelcast.transaction.TransactionContext;
import capital.any.hazelcast.update.base.UpdateEntry;

/**
 * @author LioR SoLoMoN
 *
 */
public abstract class AbstractHazelCastClient {

	/**
	 * @param mapName
	 * @return
	 */
	public abstract IMap<?, ?> getMap(String mapName);

	/**
	 * @param mapName
	 * @param obj
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public abstract boolean insertIntoTransactionMap(String mapName, Object obj, long id) throws Exception;

	/**
	 * @param mapName
	 * @param obj
	 * @param id
	 * @return
	 */
	public abstract boolean insertEntryToMap(String mapName, Object obj, long id);

	/**
	 * @param id
	 * @param updateEntry
	 * @param mapName
	 * @return
	 * @throws Exception
	 */
	public abstract boolean updateTransactionMap(Long id, UpdateEntry updateEntry, String mapName) throws Exception;

	/**
	 * @param id
	 * @param updateEntry
	 * @param mapName
	 * @return
	 * @throws Exception
	 */
	public abstract boolean updateTransactionMap(Integer id, UpdateEntry updateEntry, String mapName) throws Exception;

	/**
	 * @return
	 */
	public abstract TransactionContext startTransaction();

	/**
	 * @param transactionContext
	 */
	public abstract void commitTransaction(TransactionContext transactionContext);

	/**
	 * @param transactionContext
	 * @throws Exception
	 */
	public abstract void rollbackTransaction(TransactionContext transactionContext) throws Exception;

	/**
	 * @param transactionContext
	 * @param id
	 * @param updateEntry
	 * @param mapName
	 * @return
	 * @throws Exception
	 */
	public abstract boolean updateTransactionMap(TransactionContext transactionContext, Long id, UpdateEntry updateEntry, String mapName) throws Exception;

	/**
	 * @param id
	 * @param ep
	 * @param mapName
	 * @return
	 */
	public abstract boolean updateMap(Long id, EntryProcessor<Long, ?> ep, String mapName);

	/**
	 * @param id
	 * @param ep
	 * @param mapName
	 * @return
	 */
	public abstract boolean updateMap(Integer id, EntryProcessor<Integer, ?> ep, String mapName);
	
}
