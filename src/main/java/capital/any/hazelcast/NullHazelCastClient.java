package capital.any.hazelcast;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.hazelcast.core.IMap;
import com.hazelcast.map.EntryProcessor;
import com.hazelcast.transaction.TransactionContext;
import capital.any.hazelcast.update.base.UpdateEntry;

/**
 * @author LioR SoLoMoN
 *
 */
public class NullHazelCastClient extends AbstractHazelCastClient {
	private static final Logger logger = LoggerFactory.getLogger(NullHazelCastClient.class);
	private static volatile NullHazelCastClient instance;

	static synchronized NullHazelCastClient getInstance() {
		if (instance == null) {
			instance = new NullHazelCastClient();
		}
		return instance;
	}
	
	@Override
	public IMap<?, ?> getMap(String mapName) {
		return null;
	}

	@Override
	public boolean insertIntoTransactionMap(String mapName, Object obj, long id) throws Exception {
		return true;
	}

	@Override
	public boolean insertEntryToMap(String mapName, Object obj, long id) {
		return true;
	}

	@Override
	public boolean updateTransactionMap(Long id, UpdateEntry updateEntry, String mapName) throws Exception {
		return true;
	}

	@Override
	public boolean updateTransactionMap(Integer id, UpdateEntry updateEntry, String mapName) throws Exception {
		return true;
	}

	@Override
	public TransactionContext startTransaction() {
		return null;
	}

	@Override
	public void commitTransaction(TransactionContext transactionContext) {
		
	}

	@Override
	public void rollbackTransaction(TransactionContext transactionContext) throws Exception {
		
	}

	@Override
	public boolean updateTransactionMap(TransactionContext transactionContext, Long id, UpdateEntry updateEntry,
			String mapName) throws Exception {
		return true;
	}

	@Override
	public boolean updateMap(Long id, EntryProcessor<Long, ?> ep, String mapName) {
		return true;
	}

	@Override
	public boolean updateMap(Integer id, EntryProcessor<Integer, ?> ep, String mapName) {
		return true;
	}
}

