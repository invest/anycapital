package capital.any.hazelcast.processor.base.product;

import java.util.Map.Entry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.hazelcast.map.EntryBackupProcessor;
import com.hazelcast.map.EntryProcessor;
import capital.any.hazelcast.processor.base.CopyValueToBackupEntryBackupProcessor;
import capital.any.model.base.Product;

/**
 * @author LioR SoLoMoN
 *
 */
public class AskBidEntryProcessor implements EntryProcessor<Long, Product> {
	private static final long serialVersionUID = -2051279037378427121L;
	private static final Logger logger = LoggerFactory.getLogger(AskBidEntryProcessor.class);
	private Product product;
	private double ask;
	private double bid;

	public AskBidEntryProcessor(double ask, double bid) {
		this.ask = ask;
		this.bid = bid;
	}

	@Override
	public Product process(Entry<Long, Product> entry) {
		Product product = entry.getValue();
		logger.debug("start AskBidEntryProcessor process; product: " + product.getId());
		product.setAsk(ask);
		product.setBid(bid);
		product.calculateSimulation();
		this.product = product;
		entry.setValue(product);		
		logger.debug("End AskBidEntryProcessor process; product: " + product.getId());
		return product;
	}

	@Override
	public EntryBackupProcessor<Long, Product> getBackupProcessor() {
		logger.debug("AskBidEntryProcessor; getBackupProcessor; " + this.product.getId()); 
		return new CopyValueToBackupEntryBackupProcessor<Product>(this.product);
	}	
}