package capital.any.hazelcast.processor.base.product;

import java.util.Map.Entry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.hazelcast.map.EntryBackupProcessor;
import com.hazelcast.map.EntryProcessor;
import capital.any.hazelcast.processor.base.CopyValueToBackupEntryBackupProcessor;
import capital.any.model.base.Product;

/**
 * @author Eyal Goren
 *
 */
public class PriorityEntryProcessor implements EntryProcessor<Long, Product> {
	private static final long serialVersionUID = 6066415433743194593L;
	private static final Logger logger = LoggerFactory.getLogger(PriorityEntryProcessor.class);
	private Product product;
	private int priority;

	public PriorityEntryProcessor(int priority) {
		this.priority = priority;
	}

	@Override
	public Product process(Entry<Long, Product> entry) {
		Product product = entry.getValue();
		logger.debug("start PriorityEntryProcessor process; product: " + product.getId());
		product.setPriority(priority);
		this.product = product;
		entry.setValue(product);		
		logger.debug("End PriorityEntryProcessor process; product: " + product.getId());
		return product;
	}

	@Override
	public EntryBackupProcessor<Long, Product> getBackupProcessor() {
		logger.debug("PriorityEntryProcessor; getBackupProcessor; " + this.product.getId()); 
		return new CopyValueToBackupEntryBackupProcessor<Product>(this.product);
	}	
}