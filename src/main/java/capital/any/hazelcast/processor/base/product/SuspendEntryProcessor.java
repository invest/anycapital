package capital.any.hazelcast.processor.base.product;

import java.util.Map.Entry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.hazelcast.map.EntryBackupProcessor;
import com.hazelcast.map.EntryProcessor;
import capital.any.hazelcast.processor.base.CopyValueToBackupEntryBackupProcessor;
import capital.any.model.base.Product;

/**
 * @author LioR SoLoMoN
 *
 */
public class SuspendEntryProcessor implements EntryProcessor<Long, Product> {
	private static final long serialVersionUID = 6066415433743194593L;
	private static final Logger logger = LoggerFactory.getLogger(SuspendEntryProcessor.class);
	private Product product;
	private boolean isSuspend;

	public SuspendEntryProcessor(boolean isSuspend) {
		this.isSuspend = isSuspend;
	}

	@Override
	public Product process(Entry<Long, Product> entry) {
		Product product = entry.getValue();
		logger.debug("start SuspendEntryProcessor process; product: " + product.getId());
		product.setSuspend(isSuspend);
		this.product = product;
		entry.setValue(product);		
		logger.debug("End SuspendEntryProcessor process; product: " + product.getId());
		return product;
	}

	@Override
	public EntryBackupProcessor<Long, Product> getBackupProcessor() {
		logger.debug("SuspendEntryProcessor; getBackupProcessor; " + this.product.getId()); 
		return new CopyValueToBackupEntryBackupProcessor<Product>(this.product);
	}	
}