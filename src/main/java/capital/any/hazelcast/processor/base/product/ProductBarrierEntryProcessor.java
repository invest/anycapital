package capital.any.hazelcast.processor.base.product;

import java.util.Map.Entry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.hazelcast.map.EntryBackupProcessor;
import com.hazelcast.map.EntryProcessor;
import capital.any.hazelcast.processor.base.CopyValueToBackupEntryBackupProcessor;
import capital.any.model.base.Product;
import capital.any.model.base.ProductBarrier;

/**
 * @author Eyal Goren
 *
 */
public class ProductBarrierEntryProcessor implements EntryProcessor<Long, Product> {
	private static final long serialVersionUID = 6204177107354138705L;
	private static final Logger logger = LoggerFactory.getLogger(ProductBarrierEntryProcessor.class);
	private Product product;
	private ProductBarrier productBarrier;

	public ProductBarrierEntryProcessor(ProductBarrier productBarrier) {
		this.productBarrier = productBarrier;
	}

	@Override
	public Product process(Entry<Long, Product> entry) {
		Product product = entry.getValue();
		logger.info("start ProductBarrierEntryProcessor process; product: " + product.getId());
		product.setProductBarrier(productBarrier);
		this.product = product;
		entry.setValue(product);		
		logger.info("End ProductBarrierEntryProcessor process; product: " + product.getId());
		return product;
	}

	@Override
	public EntryBackupProcessor<Long, Product> getBackupProcessor() {
		logger.info("ProductBarrierEntryProcessor; getBackupProcessor; " + this.product.getId()); 
		return new CopyValueToBackupEntryBackupProcessor<Product>(this.product);
	}	
}