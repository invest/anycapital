package capital.any.hazelcast.processor.base.market;

import java.util.Map.Entry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.hazelcast.map.EntryBackupProcessor;
import com.hazelcast.map.EntryProcessor;
import capital.any.hazelcast.processor.base.CopyValueToBackupEntryBackupProcessorInt;
import capital.any.model.base.Market;

/**
 * @author LioR SoLoMoN
 *
 */
public class MarketPriceEntryProcessor implements EntryProcessor<Integer, Market> {

	private static final long serialVersionUID = 5465458252732962485L;
	
	private static final Logger logger = LoggerFactory.getLogger(MarketPriceEntryProcessor.class);
	private Market market;
	private double price;
	

	public MarketPriceEntryProcessor(double price) {
		this.price = price;
	}

	@Override
	public Market process(Entry<Integer, Market> entry) {
		Market market = entry.getValue();
		logger.trace("start MarketPriceEntryProcessor process; Market: " + market.getId());
		market.getLastPrice().setPrice(price);
		this.market = market;
		entry.setValue(market);		
		logger.trace("End MarketPriceEntryProcessor process; Market: " + market.getId());
		return market;
	}

	@Override
	public EntryBackupProcessor<Integer, Market> getBackupProcessor() {
		logger.info("MarketPriceEntryProcessor; getBackupProcessor; " + this.market.getId()); 
		return new CopyValueToBackupEntryBackupProcessorInt<Market>(this.market); 
	}	
}