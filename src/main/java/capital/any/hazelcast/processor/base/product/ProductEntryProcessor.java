package capital.any.hazelcast.processor.base.product;

import java.util.Map.Entry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.hazelcast.map.EntryBackupProcessor;
import com.hazelcast.map.EntryProcessor;
import capital.any.hazelcast.processor.base.CopyValueToBackupEntryBackupProcessor;
import capital.any.model.base.Product;

/**
 * @author Eyal Goren
 *
 */
public class ProductEntryProcessor implements EntryProcessor<Long, Product> {
	private static final long serialVersionUID = 6204177107354138705L;
	private static final Logger logger = LoggerFactory.getLogger(ProductEntryProcessor.class);
	private Product product;
	private Product productUpdate;

	public ProductEntryProcessor(Product product) {
		this.productUpdate = product;
	}

	@Override
	public Product process(Entry<Long, Product> entry) {
		Product product = entry.getValue();
		logger.debug("start ProductEntryProcessor process; product: " + product.getId());
		product.setBondFloorAtissuance(productUpdate.getBondFloorAtissuance());
		product.setBonusLevel(productUpdate.getBonusLevel());
		product.setCapLevel(productUpdate.getCapLevel());
		product.setCurrencyId(productUpdate.getCurrencyId());
		product.setDayCountConvention(productUpdate.getDayCountConvention());
		product.setDenomination(productUpdate.getDenomination());
		product.setFinalFixingDate(productUpdate.getFinalFixingDate());
		product.setFirstExchangeTradingDate(productUpdate.getFirstExchangeTradingDate());
		product.setInitialFixingDate(productUpdate.getInitialFixingDate());
		product.setIssueDate(productUpdate.getIssueDate());
		product.setIssuePrice(productUpdate.getIssuePrice());
		product.setIssueSize(productUpdate.getIssueSize());
		product.setLastTradingDate(productUpdate.getLastTradingDate());
		product.setLevelOfParticipation(productUpdate.getLevelOfParticipation());
		product.setLevelOfProtection(productUpdate.getLevelOfProtection());
		product.setMaxYield(productUpdate.getMaxYield());
		product.setMaxYieldPA(productUpdate.getMaxYieldPA());
		product.setProductType(productUpdate.getProductType());
		product.setProductCouponFrequency(productUpdate.getProductCouponFrequency());
		product.setProductInsuranceTypeId(productUpdate.getProductInsuranceTypeId());
		product.setQuanto(productUpdate.isQuanto());
		product.setRedemptionDate(productUpdate.getRedemptionDate());
		product.setStrikeLevel(productUpdate.getStrikeLevel());
		product.setSubscriptionEndDate(productUpdate.getSubscriptionEndDate());
		product.setSubscriptionStartDate(productUpdate.getSubscriptionStartDate());
		product.setUpside(productUpdate.getUpside());
		this.product = product;
		entry.setValue(product);
		logger.debug("End ProductEntryProcessor process; product: " + product.getId());
		return product;
	}

	@Override
	public EntryBackupProcessor<Long, Product> getBackupProcessor() {
		logger.debug("ProductBarrierOccurEntryProcessor; getBackupProcessor; " + this.product.getId()); 
		return new CopyValueToBackupEntryBackupProcessor<Product>(this.product);
	}	
}