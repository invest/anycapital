package capital.any.hazelcast.processor.base.product;

import java.util.List;
import java.util.Map.Entry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.hazelcast.map.EntryBackupProcessor;
import com.hazelcast.map.EntryProcessor;
import capital.any.hazelcast.processor.base.CopyValueToBackupEntryBackupProcessor;
import capital.any.model.base.Product;
import capital.any.model.base.ProductMarket;

/**
 * @author LioR SoLoMoN
 *
 */
public class EndTradeLevelEntryProcessor implements EntryProcessor<Long, Product> {
	private static final long serialVersionUID = 7242575295375370747L;
	private static final Logger logger = LoggerFactory.getLogger(EndTradeLevelEntryProcessor.class);
	private Product product;
	private List<ProductMarket> productMarkets;

	public EndTradeLevelEntryProcessor(List<ProductMarket> productMarkets) {
		this.productMarkets = productMarkets;
	}

	@Override
	public Product process(Entry<Long, Product> entry) {
		Product product = entry.getValue();
		logger.debug("start EndTradeLevelEntryProcessor process; product: " + product.getId());
		List<ProductMarket> productMarkets = product.getProductMarkets();		
		for (ProductMarket productMarketNew : this.productMarkets) {
			for (ProductMarket productMarket : productMarkets) {
				if (productMarket.getMarket().getId() == productMarketNew.getMarket().getId()) {
					productMarket.setEndTradeLevel(productMarketNew.getEndTradeLevel());
					break;
				}
			}
		}
		this.product = product;
		entry.setValue(product);
	
		logger.debug("End EndTradeLevelEntryProcessor process; product: " + product.getId());
		return product;
	}

	@Override
	public EntryBackupProcessor<Long, Product> getBackupProcessor() {
		logger.debug("EndTradeLevelEntryProcessor; getBackupProcessor; " + this.product.getId()); 
		return new CopyValueToBackupEntryBackupProcessor<Product>(this.product);
	}	
}