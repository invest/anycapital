package capital.any.hazelcast.processor.base.product;

import java.util.Map.Entry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.hazelcast.map.EntryBackupProcessor;
import com.hazelcast.map.EntryProcessor;
import capital.any.hazelcast.processor.base.CopyValueToBackupEntryBackupProcessor;
import capital.any.model.base.Product;

/**
 * @author LioR SoLoMoN
 *
 */
public class ProductBarrierOccurEntryProcessor implements EntryProcessor<Long, Product> {
	private static final long serialVersionUID = 6204177107354138705L;
	private static final Logger logger = LoggerFactory.getLogger(ProductBarrierOccurEntryProcessor.class);
	private Product product;
	private boolean isBarrierOccur;

	public ProductBarrierOccurEntryProcessor(boolean isBarrierOccur) {
		this.isBarrierOccur = isBarrierOccur;
	}

	@Override
	public Product process(Entry<Long, Product> entry) {
		Product product = entry.getValue();
		logger.debug("start ProductBarrierOccurEntryProcessor process; product: " + product.getId());
		product.getProductBarrier().setBarrierOccur(isBarrierOccur);
		product.calculateScenarios();
		this.product = product;
		entry.setValue(product);		
		logger.debug("End ProductBarrierOccurEntryProcessor process; product: " + product.getId());
		return product;
	}

	@Override
	public EntryBackupProcessor<Long, Product> getBackupProcessor() {
		logger.debug("ProductBarrierOccurEntryProcessor; getBackupProcessor; " + this.product.getId()); 
		return new CopyValueToBackupEntryBackupProcessor<Product>(this.product);
	}	
}