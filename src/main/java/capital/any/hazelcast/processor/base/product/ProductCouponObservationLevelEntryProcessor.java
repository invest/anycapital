package capital.any.hazelcast.processor.base.product;

import java.util.Map.Entry;
import java.util.function.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.hazelcast.map.EntryBackupProcessor;
import com.hazelcast.map.EntryProcessor;
import capital.any.hazelcast.processor.base.CopyValueToBackupEntryBackupProcessor;
import capital.any.model.base.Product;
import capital.any.model.base.ProductCoupon;

/**
 * @author Eyal Goren
 *
 */
public class ProductCouponObservationLevelEntryProcessor implements EntryProcessor<Long, Product> {
	private static final long serialVersionUID = 6204177107354138705L;
	private static final Logger logger = LoggerFactory.getLogger(ProductCouponObservationLevelEntryProcessor.class);
	private Product product;
	private ProductCoupon productCoupon;

	public ProductCouponObservationLevelEntryProcessor(ProductCoupon productCoupon) {
		this.productCoupon = productCoupon;
	}

	@Override
	public Product process(Entry<Long, Product> entry) {
		Product product = entry.getValue();
		logger.debug("start ProductCouponObservationLevelEntryProcessor process; product: " + product.getId() + " coupon " + productCoupon);
		Predicate<ProductCoupon> predicate = c-> c.getId() == this.productCoupon.getId();
		ProductCoupon productCoupon = product.getProductCoupons().stream().filter(predicate).findFirst().get();
		productCoupon.setObservationLevel(this.productCoupon.getObservationLevel());
		this.product = product;
		entry.setValue(product);
		logger.debug("End ProductCouponObservationLevelEntryProcessor process; product: " + product.getId());
		return product;
	}

	@Override
	public EntryBackupProcessor<Long, Product> getBackupProcessor() {
		logger.debug("ProductCouponObservationLevelEntryProcessor; getBackupProcessor; " + this.product.getId()); 
		return new CopyValueToBackupEntryBackupProcessor<Product>(this.product);
	}	
}