package capital.any.hazelcast.processor.base.product;

import java.util.Map.Entry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.hazelcast.map.EntryBackupProcessor;
import com.hazelcast.map.EntryProcessor;
import capital.any.base.enums.ProductStatusEnum;
import capital.any.hazelcast.processor.base.CopyValueToBackupEntryBackupProcessor;
import capital.any.model.base.Product;
import capital.any.model.base.ProductStatus;

/**
 * @author LioR SoLoMoN
 *
 */
public class StatusEntryProcessor implements EntryProcessor<Long, Product> {
	private static final long serialVersionUID = -368342508511319326L;
	private static final Logger logger = LoggerFactory.getLogger(StatusEntryProcessor.class);
	private Product product;
	private ProductStatusEnum productStatus;
	
	public StatusEntryProcessor(ProductStatusEnum productStatus) {
		this.productStatus = productStatus;
	}
	
	@Override
	public Product process(Entry<Long, Product> entry) {
		Product product = entry.getValue();
		logger.debug("start StatusEntryProcessor process; product: " + product.getId());
		product.setProductStatus(new ProductStatus(productStatus.getId()));
		this.product = product;
		entry.setValue(product);		
		logger.debug("End StatusEntryProcessor process; product: " + product.getId());
		return product;
	}

	@Override
	public EntryBackupProcessor<Long, Product> getBackupProcessor() {
		logger.debug("StatusEntryProcessor; getBackupProcessor; " + this.product.getId()); 
		return new CopyValueToBackupEntryBackupProcessor<Product>(this.product);
	}	
}
