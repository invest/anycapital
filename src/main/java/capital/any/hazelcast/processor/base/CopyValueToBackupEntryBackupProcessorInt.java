package capital.any.hazelcast.processor.base;

import java.util.Map.Entry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.hazelcast.map.EntryBackupProcessor;

/**
 * @author LioR SoLoMoN
 * @param <T>
 *
 */
public class CopyValueToBackupEntryBackupProcessorInt<T> implements EntryBackupProcessor<Integer, T> {
	private static final Logger logger = LoggerFactory.getLogger(CopyValueToBackupEntryBackupProcessorInt.class);
	private static final long serialVersionUID = -3726239207531417322L;
	private T data;

	public CopyValueToBackupEntryBackupProcessorInt(T data) {
		this.data = data;
	}

	@Override
	public void processBackup(Entry<Integer, T> entry) {
		logger.info("CopyValueToBackupEntryBackupProcessor; processBackup; " + entry);
		if (entry != null) {
			entry.setValue(data);
		}
	}
}