package capital.any.hazelcast.processor.base.product;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map.Entry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.hazelcast.map.EntryBackupProcessor;
import com.hazelcast.map.EntryProcessor;
import capital.any.base.enums.ProductStatusEnum;
import capital.any.hazelcast.processor.base.CopyValueToBackupEntryBackupProcessor;
import capital.any.model.base.Product;
import capital.any.model.base.ProductMarket;

/**
 * @author LioR SoLoMoN
 *
 */
public class StartTradeLevelEntryProcessor implements EntryProcessor<Long, Product> {
	private static final long serialVersionUID = -5343535324966072033L;
	private static final Logger logger = LoggerFactory.getLogger(StartTradeLevelEntryProcessor.class);
	private Product product;
	private List<ProductMarket> productMarkets;

	public StartTradeLevelEntryProcessor(List<ProductMarket> productMarkets) {
		this.productMarkets = productMarkets;
	}

	@Override
	public Product process(Entry<Long, Product> entry) {
		Product product = entry.getValue();
		logger.debug("start StartTradeLevelEntryProcessor process; product: " + product.getId());
		List<ProductMarket> productMarkets = product.getProductMarkets();		
		for (ProductMarket productMarketNew : this.productMarkets) {
			for (ProductMarket productMarket : productMarkets) {
				if (productMarket.getMarket().getId() == productMarketNew.getMarket().getId()) {
					productMarket.setStartTradeLevel(productMarketNew.getStartTradeLevel());
					break;
				}
			}
		}
		product.calculateScenarios();
		if (product.getProductBarrier() != null) {
			if (ProductStatusEnum.SUBSCRIPTION.getId() < product.getProductStatus().getId()) {
				product.getProductBarrier().calculateDistanceToBarrier(new BigDecimal(String.valueOf(product.getProductMarkets().get(0).getMarket().getLastPrice().getPrice())), 
																				new BigDecimal(String.valueOf(product.getProductMarkets().get(0).getStartTradeLevel())));
			}
		}
		this.product = product;
		entry.setValue(product);
	
		logger.debug("End StartTradeLevelEntryProcessor process; product: " + product.getId());
		return product;
	}

	@Override
	public EntryBackupProcessor<Long, Product> getBackupProcessor() {
		logger.debug("StartTradeLevelEntryProcessor; getBackupProcessor; " + this.product.getId()); 
		return new CopyValueToBackupEntryBackupProcessor<Product>(this.product);
	}	
}