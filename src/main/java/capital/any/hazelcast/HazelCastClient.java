package capital.any.hazelcast;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.core.LifecycleEvent;
import com.hazelcast.core.LifecycleListener;
import com.hazelcast.core.TransactionalMap;
import com.hazelcast.map.EntryProcessor;
import com.hazelcast.transaction.TransactionContext;
import com.hazelcast.transaction.TransactionOptions;
import com.hazelcast.transaction.TransactionOptions.TransactionType;
import capital.any.hazelcast.update.base.UpdateEntry;

/**
 * @author LioR SoLoMoN
 *
 */
public class HazelCastClient extends AbstractHazelCastClient {
	private static final Logger logger = LoggerFactory.getLogger(HazelCastClient.class);
	private static volatile HazelCastClient instance;
	private static volatile HazelcastInstance client;
	
	private HazelCastClient() {		
		PropertiesConfiguration properties = new PropertiesConfiguration();
		String clusterIp = "", clusterPort = "";
		try {
			properties.load("application.properties");
			clusterIp = properties.getString("hazelcast.address.host.name");
			clusterPort = properties.getString("hazelcast.address.port");
		} catch (ConfigurationException e) {
			logger.error("Can't get application.properties");
		}
		ClientConfig config = new ClientConfig();
        List<String> addresses = new ArrayList<String>();
        addresses.add((new StringBuilder(clusterIp).append(":").append(clusterPort)).toString());
        config.getNetworkConfig().setAddresses(addresses);
        client = HazelcastClient.newHazelcastClient(config);
        client.getLifecycleService().addLifecycleListener(new LifecycleListener() {
            @Override
            public void stateChanged(LifecycleEvent event) {
            	if (event.getState() == LifecycleEvent.LifecycleState.CLIENT_DISCONNECTED) {
            		logger.info("Client Disconnected");
				} else if (event.getState() == LifecycleEvent.LifecycleState.CLIENT_CONNECTED) {
					logger.info("Client Connected");
				} else {
					logger.info(event.getState().toString());
				}
            }
        });
	}
	
	static synchronized HazelCastClient getInstance() {
		if (instance == null) {
			instance = new HazelCastClient();
		}
		return instance;
	}
	
	public IMap<?,?> getMap(String mapName) {
		return client.getMap(mapName);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public boolean insertIntoTransactionMap(String mapName, Object obj, long id) throws Exception {
		TransactionOptions options = new TransactionOptions().setTransactionType(TransactionType.TWO_PHASE);
		TransactionContext context = client.newTransactionContext(options);
		context.beginTransaction();
		TransactionalMap map = context.getMap(mapName);
		boolean result = false;
		//TODO should lock?
		try {
			result = (map.put(id, obj) != null);
			context.commitTransaction();
		} catch (Exception e) {
			logger.error("ERROR! hazelcast client insertIntoTransactionMap", e);
			context.rollbackTransaction();
			throw new Exception();
		}
		
		return result;
	}
	
	public boolean insertEntryToMap(String mapName, Object obj, long id) {
		IMap<Long,Object> map = client.getMap(mapName);
		return (map.put(id, obj) != null);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public boolean updateTransactionMap(Long id, UpdateEntry updateEntry, String mapName) throws Exception {
		logger.info("about to updateTransactionMap; id: " + id + "; map name: " + mapName);
		TransactionOptions options = new TransactionOptions().setTransactionType(TransactionType.TWO_PHASE);
		boolean result = false;
		TransactionContext context = client.newTransactionContext(options);
		context.beginTransaction();
		TransactionalMap map = context.getMap(mapName);
		try {
			Object entry = updateEntry.update(map.getForUpdate(id));
			map.replace(id, entry);
			context.commitTransaction();
			result = true;
		} catch (Exception e) {
			logger.error("ERROR! hazelcast client updateTransactionMap", e);
			context.rollbackTransaction();
			result = false;
			throw new Exception();
		}
		
		return result;
	}
	
	//TODO dry
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public boolean updateTransactionMap(Integer id, UpdateEntry updateEntry, String mapName) throws Exception {
		logger.info("about to updateTransactionMap; id: " + id + "; map name: " + mapName);
		TransactionOptions options = new TransactionOptions().setTransactionType(TransactionType.TWO_PHASE);
		boolean result = false;
		TransactionContext context = client.newTransactionContext(options);
		context.beginTransaction();
		TransactionalMap map = context.getMap(mapName);
		try {
			Object entry = updateEntry.update(map.getForUpdate(id));
			map.replace(id, entry);
			context.commitTransaction();
			result = true;
		} catch (Exception e) {
			logger.error("ERROR! hazelcast client updateTransactionMap", e);
			context.rollbackTransaction();
			result = false;
			throw new Exception();
		}
		
		return result;
	}
	
	public TransactionContext startTransaction() {
		TransactionOptions options = new TransactionOptions().setTransactionType(TransactionType.TWO_PHASE);
		TransactionContext context = client.newTransactionContext(options);
		context.beginTransaction();
		return context;
	}
	
	public void commitTransaction(TransactionContext transactionContext) {
		transactionContext.commitTransaction();
	}
	
	public void rollbackTransaction(TransactionContext transactionContext) throws Exception {
		transactionContext.rollbackTransaction();
		throw new Exception();
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public boolean updateTransactionMap(TransactionContext transactionContext, Long id, UpdateEntry updateEntry, String mapName) throws Exception {
		logger.info("about to updateTransactionMap; id: " + id + "; map name: " + mapName);
		boolean result = true;
		TransactionalMap map = transactionContext.getMap(mapName);
		Object entry = updateEntry.update(map.getForUpdate(id));
		map.replace(id, entry);
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public boolean updateMap(Long id, EntryProcessor<Long, ?> ep, String mapName) {
		logger.info("before update executeOnKey; map name: " + mapName + "; id: " + id);
		IMap<Long, ?> map = (IMap<Long, ?>) getMap(mapName);
		Object result = map.executeOnKey(id, ep);
		
		logger.info("after update executeOnKey; " + result);
		
		return result != null;
	}
	
	@SuppressWarnings("unchecked")
	public boolean updateMap(Integer id, EntryProcessor<Integer, ?> ep, String mapName) {
		logger.info("before update executeOnKey; map name: " + mapName + "; id: " + id);
		IMap<Integer, ?> map = (IMap<Integer, ?>) getMap(mapName);
		Object result = map.executeOnKey(id, ep);
		logger.info("after update executeOnKey; " + result);
		
		return result != null;
	}
}
