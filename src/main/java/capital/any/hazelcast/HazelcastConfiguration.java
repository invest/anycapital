package capital.any.hazelcast;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.hazelcast.client.config.ClientConfig;


/**
 * @author LioR SoLoMoN
 *
 */
@Configuration("HazelcastConfigurationCommon")
public class HazelcastConfiguration { 
	
	@Bean
	public ClientConfig config() {
		return new ClientConfig();
	}
}
