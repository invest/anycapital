package capital.any.hazelcast;

/**
 * @author LioR SoLoMoN
 *
 */
public abstract class Constants {
	
	/**
	 * 
	 */
	public static final String MAP_COUNTRIES = "countries";
	/**
	 * 
	 */
	public static final String MAP_CURRENCIES = "currencies";
	/**
	 * 
	 */
	public static final String MAP_LANGUAGES = "languages";
	/**
	 * 
	 */
	public static final String MAP_MARKETS = "markets";
	/**
	 * 
	 */
	public static final String MAP_MARKETS_THIN = "markets_thin";
	/**
	 * 
	 */
	public static final String MAP_MARKET_GROUPS = "market_groups";
	/**
	 * 
	 */
	public static final String MAP_PRODUCTS = "products";
	/**
	 * 
	 */
	public static final String MAP_PRODUCT_CATEGORIES = "product_categories";
	/**
	 * 
	 */
	public static final String MAP_PRODUCT_COUPONS = "product_coupons";
	/**
	 * 
	 */
	public static final String MAP_PRODUCT_MARKETS = "product_markets";
	/**
	 * 
	 */
	public static final String MAP_PRODUCT_STATUSES = "product_statuses";
	/**
	 * 
	 */
	public static final String MAP_PRODUCT_TYPES = "product_types";
	/**
	 * 
	 */
	public static final String MAP_WRITERS = "writers";
	/**
	 * 
	 */
	public static final String MAP_PRODUCT_COUPON_FREQUENCIES = "product_coupon_frequencies";
	/**
	 * 
	 */
	public static final String MAP_DAY_COUNT_CONVENTION = "day_count_convention";
	/**
	 * 
	 */
	public static final String MAP_PRODUCT_MATURITY = "product_maturity";
	/**
	 * 
	 */
	public static final String MAP_PRODUCT_BARRIER_TYPE = "product_barrier_type";
	/**
	 * 
	 */
	public static final String MAP_MESSAGE_RESOURCE = "message_resource";
	/**
	 * 
	 */
	public static final String MAP_TRANSACTION_STATUSES = "transaction_statuses";
	/**
	 * 
	 */
	public static final String MAP_TRANSACTION_PAYMENT_TYPES = "transaction_payment_types";
	/**
	 * 
	 */
	public static final String MAP_TRANSACTION_OPERATION = "map_transaction_operation";
	/**
	 * 
	 */
	public static final String MAP_INVESTMENT_POSSIBILITIES = "investment_possibilities";
	/**
	 * 
	 */
	public static final String MAP_INVESTMENT_STATUSES = "investment_statuses";
	/**
	 * 
	 */
	public static final String MAP_ISSUE_CHANNELS = "issue_channels";
	/**
	 * 
	 */
	public static final String MAP_ISSUE_SUBJECTS = "issue_subjects";
	/**
	 * 
	 */
	public static final String MAP_ISSUE_DIRECTIONS = "issue_directions";
	/**
	 * 
	 */
	public static final String MAP_REACHED_STATUSES = "issue_Reached_Statuses";
	/**
	 * 
	 */
	public static final String MAP_ISSUE_REACTIONS = "issue_reaction";
	/**
	 * 
	 */
	public static final String MAP_FILE_TYPE = "file_type";
	/**
	 * 
	 */
	public static final String MAP_REGULATION_QUESTIONNAIRE = "regulation_questionnaire";
	/**
	 * 
	 */
	public static final String MAP_SCORE_STATUS = "score_status";
	/**
	 * 
	 */
	public static final String MAP_MARKETING_LANDING_PAGE = "marketing_landing_page";
	/**
	 * 
	 */
	public static final String MAP_EMAIL_TEMPLATE = "email_template";
	/**
	 * 
	 */
	public static final String MAP_DB_PARAMETER = "db_parameters";
	/**
	 * 
	 */
	public static final String MAP_ANALYTICS_INVESTMENT = "analytics_investment";
	/**
	 * 
	 */
	public static final String MAP_ANALYTICS_DEPOSIT = "analytics_deposit";
	/**
	 * 
	 */
	public static final String MAP_MARKET_UNDERLYING_ASSETS = "market_underlying_assets";
	/**
	 * 
	 */
	public static final String MAP_ALLOW_IP = "allow_ip";
	/**
	 * 
	 */
	public static final String MAP_EMAIL_ACTION = "email_action";
	/**
	 * 
	 */
	public static final String MAP_LOCALE = "locale";
	/**
	 * 
	 */
	public static final String MAP_FILE_STATUS = "map_file_status";
}
