package capital.any.hazelcast;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author LioR SoLoMoN
 *
 */
public class HazelCastClientFactory {
	private static final Logger logger = LoggerFactory.getLogger(HazelCastClientFactory.class);
	private static final boolean isHazelcastLayerActive = true; //TODO jms
	
	public static synchronized AbstractHazelCastClient getClient() {
		AbstractHazelCastClient hazelcastClient = null; 
		if (!isHazelcastLayerActive) {
			logger.info("hazelcast layer is not active");
			hazelcastClient = NullHazelCastClient.getInstance();
		} else {
			hazelcastClient = HazelCastClient.getInstance();
		}
		
		return hazelcastClient;
	}
}
