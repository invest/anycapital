package capital.any.dao.base.dayCountConvention;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.base.DayCountConvention;

/**
 * @author Eran
 *
 */
@Component
public class DayCountConventionMapper implements RowMapper<DayCountConvention> {
	
	@Override
	public DayCountConvention mapRow(ResultSet rs, int row) throws SQLException {
		DayCountConvention dayCountConvention = new DayCountConvention();
		dayCountConvention.setId(rs.getInt("id"));
		dayCountConvention.setName(rs.getString("name"));
		dayCountConvention.setDisplayName(rs.getString("display_name"));		
		return dayCountConvention;
	}
	
	public DayCountConvention mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
			
}
	

