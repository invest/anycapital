package capital.any.dao.base.dayCountConvention;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.DayCountConvention;

/**
 * @author LioR SoLoMoN
 *
 */
@Repository("DayCountConventionHCDao")
public class DayCountConventionHCDao implements IDayCountConventionDao {	
	private static final Logger logger = LoggerFactory.getLogger(DayCountConventionHCDao.class);
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, DayCountConvention> get() {		
		Map<Integer, DayCountConvention> dayCountConventions = null;		
		try {
			dayCountConventions = (Map<Integer, DayCountConvention>) HazelCastClientFactory.getClient().getMap(Constants.MAP_DAY_COUNT_CONVENTION);
		} catch (Exception e) {
			logger.error("ERROR!", e);
		}
		return dayCountConventions;
	}
}
