package capital.any.dao.base.dayCountConvention;

import java.util.Map;
import capital.any.model.base.DayCountConvention;

/**
 * @author eranl
 *
 */
public interface IDayCountConventionDao {
	
	public static final String GET_DAY_COUNT_CONVENTIONS_LIST = 
			" SELECT " +
			"	* " + 
			" FROM "	+ 
			"	day_count_conventions ";
		
	Map<Integer, DayCountConvention> get();

}
