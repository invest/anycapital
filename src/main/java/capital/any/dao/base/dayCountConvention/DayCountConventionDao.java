package capital.any.dao.base.dayCountConvention;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import capital.any.model.base.DayCountConvention;

/**
 * @author eranl
 *
 */
@Repository
public class DayCountConventionDao implements IDayCountConventionDao {
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private DayCountConventionMapper dayCountConventionMapper;
	
	@Override
	public Map<Integer, DayCountConvention> get() {
		List<DayCountConvention> tmp = jdbcTemplate.query(GET_DAY_COUNT_CONVENTIONS_LIST, dayCountConventionMapper);
		Map<Integer, DayCountConvention> hm = tmp.stream().collect(Collectors.toMap(p-> p.getId(), p -> p));		 		 
		return hm;
	}
}
