package capital.any.dao.base.investmentPossibilities;

import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import capital.any.model.base.InvestmentPossibility;

/**
 * @author EyalG
 *
 */
@Repository
public class InvestmentPossibilitiesDao implements IInvestmentPossibilitiesDao, ApplicationContextAware {
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	private ApplicationContext appContext;
	
	@Override
	public Map<Integer, InvestmentPossibility> get() {
		InvestmentPossibilityCallbackHandler callbackHandler = appContext.getBean(InvestmentPossibilityCallbackHandler.class);
		jdbcTemplate.query(GET_INVESTMENT_POSSIBILITIES, callbackHandler);		 		 
		return callbackHandler.getResult();
	}

	@Override
	public void setApplicationContext(ApplicationContext appContext) throws BeansException {
		this.appContext = appContext;		
	}		
}
