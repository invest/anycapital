package capital.any.dao.base.investmentPossibilities;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.InvestmentPossibility;

/**
 * @author Eyal G
 *
 */
@Repository("InvestmentPossibilitiesDaoHCDao")
public class InvestmentPossibilitiesDaoHCDao implements IInvestmentPossibilitiesDao {
	private static final Logger logger = LoggerFactory.getLogger(InvestmentPossibilitiesDaoHCDao.class);
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, InvestmentPossibility> get() {		
		Map<Integer, InvestmentPossibility> investmentPossibilities = null;		
		try {
			investmentPossibilities = (Map<Integer, InvestmentPossibility>) HazelCastClientFactory.getClient().getMap(Constants.MAP_INVESTMENT_POSSIBILITIES);
		} catch (Exception e) {
			logger.error("ERROR!", e);
		}
		return investmentPossibilities;
	}
}
