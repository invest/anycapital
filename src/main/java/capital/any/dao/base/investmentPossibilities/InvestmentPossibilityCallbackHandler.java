package capital.any.dao.base.investmentPossibilities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Component;

import capital.any.dao.base.investmentPossibilityTabs.InvestmentPossibilityTabsMapper;
import capital.any.model.base.InvestmentPossibility;
import capital.any.model.base.InvestmentPossibilityTab;

@Component
@Scope("prototype")
public class InvestmentPossibilityCallbackHandler implements RowCallbackHandler {
	
	private Map<Integer, InvestmentPossibility> result = new LinkedHashMap<Integer, InvestmentPossibility>();
	
	@Autowired
	private InvestmentPossibilitiesMapper investmentPossibilitiesMapper;
	@Autowired
	private InvestmentPossibilityTabsMapper investmentPossibilityTabsMapper;

	@Override
	public void processRow(ResultSet rs) throws SQLException {
		InvestmentPossibility investmentPossibility = investmentPossibilitiesMapper.mapRow(rs);
		if (result.get(investmentPossibility.getCurrencyId()) != null) {
			investmentPossibility = result.get(investmentPossibility.getCurrencyId());
		} else {
			investmentPossibility.setInvestmentPossibilityTab(new ArrayList<InvestmentPossibilityTab>());
		}
		InvestmentPossibilityTab investmentPossibilityTab = investmentPossibilityTabsMapper.mapRow(rs);
		investmentPossibility.getInvestmentPossibilityTab().add(investmentPossibilityTab);
		result.put(investmentPossibility.getCurrencyId(), investmentPossibility);
	}

	public Map<Integer, InvestmentPossibility> getResult() {
		return result;
	}

	public void setResult(Map<Integer, InvestmentPossibility> result) {
		this.result = result;
	}
}
