package capital.any.dao.base.investmentPossibilities;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.InvestmentPossibility;

/**
 * @author Eyal G
 *
 */
@Component
public class InvestmentPossibilitiesMapper extends MapperBase implements RowMapper<InvestmentPossibility> {

	@Override
	public InvestmentPossibility mapRow(ResultSet rs, int row) throws SQLException {
		InvestmentPossibility investmentPossibility = new InvestmentPossibility();
		investmentPossibility.setCurrencyId(rs.getInt("currency_id"));
		investmentPossibility.setDefaultAmount(rs.getLong("default_amount"));
		investmentPossibility.setId(rs.getInt("id"));
		investmentPossibility.setMaximumAmount(rs.getLong("maximum_amount"));
		investmentPossibility.setMinimumAmount(rs.getLong("minimum_amount"));
		return investmentPossibility;
	}
	
	public InvestmentPossibility mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
			
}
	

