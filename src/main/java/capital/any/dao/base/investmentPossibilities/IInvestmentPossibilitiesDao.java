package capital.any.dao.base.investmentPossibilities;

import java.util.Map;

import capital.any.model.base.InvestmentPossibility;

/**
 * @author EyalG
 *
 */
public interface IInvestmentPossibilitiesDao {
	
	public static final String GET_INVESTMENT_POSSIBILITIES =
			"SELECT " +
				" * " +
			"FROM " +
				"investment_possibilities ip, " +
				"investment_possibility_tabs ipt " +
			"WHERE " +
				"ipt.investment_possibilities_id = ip.id ";
	
	/**
	 * get investment Possibilities with investment Possibility tabs
	 * @return Map<Integer, InvestmentPossibility> currency and his investment Possibilities
	 */
	Map<Integer, InvestmentPossibility> get();
}
