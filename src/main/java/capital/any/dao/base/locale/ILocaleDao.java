package capital.any.dao.base.locale;

import java.util.Locale;
import java.util.Map;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface ILocaleDao {

	Map<String, Locale> get();

}
