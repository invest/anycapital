package capital.any.dao.base.locale;

import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository("LocaleHCDao")
public class LocaleHCDao implements ILocaleDao {
	private static final Logger logger = LoggerFactory.getLogger(LocaleHCDao.class);

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Locale> get() {
		Map<String, Locale> map = null;
		try {
			map = (Map<String, Locale>) HazelCastClientFactory.getClient().getMap(Constants.MAP_LOCALE);
		} catch (Exception e) {
			logger.error("ERROR! get map of locale by HC", e);
		}
		return map;
	}
}
