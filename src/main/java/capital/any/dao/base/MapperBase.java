package capital.any.dao.base;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;

public class MapperBase {
	
	/**
	 * convert java.sql.timestamp to java.util.date
	 * @param timestamp
	 * @return java.util.date
	 */	
	public Date convertTimestampToDate(Timestamp timestamp) {
		if (timestamp != null) {
			return new Date(timestamp.getTime());
		}
		return null;
	}
	
	/**
	 * Convert local date time to timestamp
	 * @param localDate
	 * @return
	 */
	public Timestamp convertLocalDateTimeToTimestamp(LocalDateTime localDateTime) {
		if (localDateTime == null) {
			return null;
		}
		Timestamp timestamp = Timestamp.valueOf(localDateTime);
		return timestamp;
	}
}
