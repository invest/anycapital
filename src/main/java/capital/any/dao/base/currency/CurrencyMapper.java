package capital.any.dao.base.currency;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.base.Currency;

/**
 * @author Eran
 *
 */
@Component
public class CurrencyMapper implements RowMapper<Currency> {

	@Override
	public Currency mapRow(ResultSet rs, int row) throws SQLException {
		Currency currency = new Currency();
		currency.setId(rs.getInt("id"));
		currency.setCode(rs.getString("code"));
		currency.setDisplayName(rs.getString("display_name"));
		currency.setSymbol(rs.getString("symbol"));
		currency.setMarketId(rs.getInt("market_id"));
		return currency;
	}
				
}
