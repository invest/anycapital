package capital.any.dao.base.currency;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.Currency;

/**
 * @author LioR SoLoMoN
 *
 */
@Repository("CurrencyHCDao")
public class CurrencyHCDao implements ICurrencyDao {
	private static final Logger logger = LoggerFactory.getLogger(CurrencyHCDao.class);
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, Currency> get() {		
		Map<Integer, Currency> currencies = null;		
		try {
			currencies = (Map<Integer, Currency>) HazelCastClientFactory.getClient().getMap(Constants.MAP_CURRENCIES);
		} catch (Exception e) {
			logger.error("ERROR!", e);
		}
		return currencies;
	}
}
