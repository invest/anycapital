package capital.any.dao.base.currency;

import java.util.Map;
import capital.any.model.base.Currency;

/**
 * @author eranl
 *
 */
public interface ICurrencyDao {
	
	public static final String GET_CURRENCIES_LIST = 
													" SELECT " +
													"	* " + 
													" FROM "	+ 
													"	currencies ";
					
	Map<Integer, Currency> get();
}
