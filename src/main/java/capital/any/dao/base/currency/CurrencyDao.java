package capital.any.dao.base.currency;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import capital.any.model.base.Currency;

/**
 * @author eranl
 *
 */
@Primary
@Repository
public class CurrencyDao implements ICurrencyDao {
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private CurrencyMapper currencyMapper;
	
	@Override
	public Map<Integer, Currency> get() {		
		List<Currency> tmp = jdbcTemplate.query(GET_CURRENCIES_LIST, new BeanPropertyRowMapper<Currency>(Currency.class));
		Map<Integer, Currency> hm = tmp.stream().collect(Collectors.toMap(p-> p.getId(), p -> p));		 		 
		return hm;
	}
}
