package capital.any.dao.base.wire;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import capital.any.model.base.Wire;

/**
 * @author eranl
 *
 */
@Repository
public class WireDao implements IWireDao {
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private WireMapper wireMapper;
	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Override
	public void insertWire(Wire wire) {				
		KeyHolder keyHolder = new GeneratedKeyHolder();		
		SqlParameterSource namedParameters = 
					new MapSqlParameterSource
							 ("transactionId", wire.getTransactionId())
					.addValue("branchAddress", wire.getBranchAddress())					
					.addValue("bankName", wire.getBankName())
					.addValue("iban", wire.getIban())
					.addValue("swift", wire.getSwift())
					.addValue("accountInfo",  wire.getAccountInfo())
					.addValue("accountNum",  wire.getAccountNum())
					.addValue("beneficiaryName",  wire.getBeneficiaryName())
					.addValue("bankFeeAmount",  wire.getBankFeeAmount())
					.addValue("branchNumber",  wire.getBranchNumber());
					
					
		namedParameterJdbcTemplate.update(INSERT_WIRE, namedParameters, keyHolder, new String[] {"id"});
		wire.setId(keyHolder.getKey().intValue());				
			
	}

}
