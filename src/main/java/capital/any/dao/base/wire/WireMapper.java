package capital.any.dao.base.wire;


import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.Wire;

/**
 * @author eranl
 *
 */
@Component
public class WireMapper extends MapperBase implements RowMapper<Wire>{
	
	@Override
	public Wire mapRow(ResultSet rs, int row) throws SQLException {
		Wire wire = new Wire();
		wire.setId(rs.getLong("id"));
		wire.setTransactionId(rs.getLong("transaction_id"));
		wire.setTimeCreated(convertTimestampToDate(rs.getTimestamp("time_created")));
		wire.setBranchAddress(rs.getString("branch_address"));
		wire.setBankName(rs.getString("bank_name"));
		wire.setIban(rs.getString("iban"));
		wire.setSwift(rs.getString("swift"));
		wire.setAccountInfo(rs.getString("account_info"));
		wire.setAccountNum(rs.getLong("account_num"));
		wire.setBranchNumber(rs.getInt("branch_number"));
		wire.setBeneficiaryName(rs.getString("beneficiary_name"));
		wire.setBankFeeAmount(rs.getLong("bank_fee_amount"));
		return wire;
	}
	
	public Wire mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}

}
