package capital.any.dao.base.wire;

import capital.any.model.base.Wire;

/**
 * @author Eran
 *
 */
public interface IWireDao {
	
	public static final String INSERT_WIRE = 						
			" INSERT INTO wires ( " +
			"		transaction_id, branch_address, bank_name, " +
			"		iban, swift, account_info, account_num, " +
			"		beneficiary_name, bank_fee_amount, branch_number) " +
			" VALUES " +
			"		(:transactionId, :branchAddress, :bankName, " + 
			"		 :iban , :swift, :accountInfo, :accountNum, " +
			"		 :beneficiaryName , :bankFeeAmount, :branchNumber) ";
		
	/**
	 * Insert wire
	 * @param wire
	 */
	void insertWire(Wire wire);
	
}
