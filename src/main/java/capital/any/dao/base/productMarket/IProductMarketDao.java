package capital.any.dao.base.productMarket;

import java.util.List;
import java.util.Map;
import capital.any.model.base.ProductMarket;

/**
 * @author eranl
 *
 */
public interface IProductMarketDao {
	
	public static final String GET_PRODUCT_MARKETS_LIST = 
			" SELECT " +
			"	* " + 
			" FROM "	+ 
			"	product_markets ";
	
	public static final String GET_PRODUCT_MARKETS_LIST_BY_PRODUCT = 
			" SELECT " +
			"	* " + 
			" FROM "	+ 
			"	product_markets pm " +
			" WHERE " +
			"	pm.product_id = :productId";
		
	Map<Long, ProductMarket> get();
	
	List<ProductMarket> getProductMarkets(long productId);

}
