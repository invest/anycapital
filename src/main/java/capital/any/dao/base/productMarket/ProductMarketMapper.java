package capital.any.dao.base.productMarket;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.base.Market;
import capital.any.model.base.ProductMarket;

/**
 * @author Eran
 *
 */
@Component
public class ProductMarketMapper implements RowMapper<ProductMarket> {
	
	@Override
	public ProductMarket mapRow(ResultSet rs, int row) throws SQLException {
		ProductMarket productMarket = new ProductMarket();		
		productMarket.setId(rs.getInt("id"));
		productMarket.setProductId(rs.getLong("product_id"));
		Market market = new Market();
		market.setId(rs.getInt("market_id"));
		productMarket.setMarket(market);
		productMarket.setStartTradeLevel(rs.getDouble("start_trade_level"));
		productMarket.setEndTradeLevel((Double) rs.getObject("end_trade_level"));
		return productMarket;
	}
	
	public ProductMarket mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
			
}
	

