package capital.any.dao.base.productMarket;

import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import capital.any.dao.base.currency.CurrencyHCDao;
import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.ProductMarket;

/**
 * @author LioR SoLoMoN
 *
 */
@Repository("ProductMarketHCDao")
public class ProductMarketHCDao implements IProductMarketDao {
	private static final Logger logger = LoggerFactory.getLogger(CurrencyHCDao.class);
	@SuppressWarnings("unchecked")
	@Override
	public Map<Long, ProductMarket> get() {		
		Map<Long, ProductMarket> productMarkets = null;		
		try {
			productMarkets = (Map<Long, ProductMarket>) HazelCastClientFactory.getClient().getMap(Constants.MAP_PRODUCT_MARKETS);
		} catch (Exception e) {
			logger.error("ERROR!", e);
		}
		return productMarkets;
	}

	@Override
	public List<ProductMarket> getProductMarkets(long productId) {
		logger.info("hazelcast getProductMarkets not implemented");
		return null;
	}
}
