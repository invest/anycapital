package capital.any.dao.base.productMarket;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.base.ProductMarket;

/**
 * @author eranl
 *
 */
@Primary
@Repository
public class ProductMarketDao implements IProductMarketDao {
	
	@Autowired
	protected NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private ProductMarketMapper productMarketMapper;
	
	@Override
	public Map<Long, ProductMarket> get() {
		List<ProductMarket> tmp = jdbcTemplate.query(GET_PRODUCT_MARKETS_LIST, productMarketMapper);
		Map<Long, ProductMarket> hm = tmp.stream().collect(Collectors.toMap(p-> p.getProductId(), p -> p));		 		 
		return hm;
	}

	@Override
	public List<ProductMarket> getProductMarkets(long productId) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("productId", productId);
		return jdbcTemplate.query(GET_PRODUCT_MARKETS_LIST_BY_PRODUCT,
				namedParameters, productMarketMapper);		
	}
}
