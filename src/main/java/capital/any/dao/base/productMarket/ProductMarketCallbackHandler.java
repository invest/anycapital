package capital.any.dao.base.productMarket;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Component;

import capital.any.dao.base.market.MarketMapper;
import capital.any.model.base.Market;
import capital.any.model.base.ProductMarket;

@Component
@Scope("prototype")
public class ProductMarketCallbackHandler implements RowCallbackHandler {
	
	private List<ProductMarket> result = new ArrayList<ProductMarket>();
	
	@Autowired
	private ProductMarketMapper productMarketMapper;
	@Autowired
	private MarketMapper marketMapper;

	@Override
	public void processRow(ResultSet rs) throws SQLException {
		Market market = marketMapper.mapRow(rs);
		ProductMarket productMarket = productMarketMapper.mapRow(rs);
		productMarket.setMarket(market);
		result.add(productMarket);
	}

	public List<ProductMarket> getResult() {
		return result;
	}

	public void setResult(List<ProductMarket> result) {
		this.result = result;
	}
}
