package capital.any.dao.base.issueSubject;

import java.util.Map;

import capital.any.model.base.IssueSubject;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IIssueSubjectDao {
	
	public static final String GET_ISSUE_SUBJECT_LIST =
			" SELECT " +
				" iss_s.id iss_s_id, " +
				" iss_s.name iss_s_name " + 
			" FROM "	+ 
				" issue_subjects iss_s ";
	
	Map<Integer, IssueSubject> get();
	
}
