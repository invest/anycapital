package capital.any.dao.base.issueSubject;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.IssueSubject;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository("IssueSubjectHCDao")
public class IssueSubjectHCDao implements IIssueSubjectDao {
	private static final Logger logger = LoggerFactory.getLogger(IssueSubjectHCDao.class);
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, IssueSubject> get() {
		Map<Integer, IssueSubject> issueSubjects = null;
		try {
			issueSubjects = (Map<Integer, IssueSubject>) HazelCastClientFactory.getClient().getMap(Constants.MAP_ISSUE_SUBJECTS);
		} catch (Exception e) {
			logger.error("Problem to get issueSubjects by HC.");
		}
		return issueSubjects;
	}

}
