package capital.any.dao.base.issueSubject;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.IssueSubject;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
public class IssueSubjectMapper extends MapperBase implements RowMapper<IssueSubject> {

	@Override
	public IssueSubject mapRow(ResultSet rs, int rowNum) throws SQLException {
		IssueSubject issueSubject = new IssueSubject();
		issueSubject.setId(rs.getInt("iss_s_id"));
		issueSubject.setName(rs.getString("iss_s_name"));
		return issueSubject;
	}
	
	public IssueSubject mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}

}
