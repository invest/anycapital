package capital.any.dao.base.regulationUserInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.base.RegulationUserInfo;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository
public class RegulationUserInfoDao implements IRegulationUserInfoDao, ApplicationContextAware {
	private static final Logger logger = LoggerFactory.getLogger(RegulationUserInfoDao.class);
	
	private ApplicationContext appContext;
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private RegulationUserInfoMapper regulationUserInfoMapper; 
	
	@Override
	public void insert(RegulationUserInfo regulationUserInfo) throws Exception {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("userId", regulationUserInfo.getUserId())
								.addValue("regulationQuestionnaireStatusId", regulationUserInfo.getRegulationQuestionnaireStatus().getId());
		jdbcTemplate.update(INSERT, namedParameters);		
	}
	
	@Override
	public void update(RegulationUserInfo regulationUserInfo) throws Exception {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("userId", regulationUserInfo.getUserId())
								.addValue("regulationQuestionnaireStatusId", regulationUserInfo.getRegulationQuestionnaireStatus().getId())
								.addValue("countryRiskId", regulationUserInfo.getCountryRisk().getId() == 0 ? null : regulationUserInfo.getCountryRisk().getId())
								.addValue("writerId", regulationUserInfo.getWriter().getId());
		jdbcTemplate.update(UPDATE, namedParameters);		
	}

	@Override
	public RegulationUserInfo get(long userId) {
		RegulationUserInfoCallbackHandler regulationUserInfoCallbackHandler = appContext.getBean(RegulationUserInfoCallbackHandler.class);
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("userId", userId);
		jdbcTemplate.query(GET, params, regulationUserInfoCallbackHandler);
		return regulationUserInfoCallbackHandler.getResult();
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.appContext = applicationContext;
	}
}
