package capital.any.dao.base.regulationUserInfo;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Component;

import capital.any.dao.base.regulationQuestionnaireStatus.RegulationQuestionnaireStatusMapper;
import capital.any.model.base.RegulationQuestionnaireStatus;
import capital.any.model.base.RegulationUserInfo;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
@Scope("prototype")
public class RegulationUserInfoCallbackHandler implements RowCallbackHandler {

	private RegulationUserInfo result;
	
	@Autowired
	private RegulationUserInfoMapper regulationUserInfoMapper;
	@Autowired
	private RegulationQuestionnaireStatusMapper regulationQuestionnaireStatusMapper;
	
	@Override
	public void processRow(ResultSet rs) throws SQLException {
		RegulationQuestionnaireStatus regulationQuestionnaireStatus = regulationQuestionnaireStatusMapper.mapRow(rs);
		RegulationUserInfo regulationUserInfo = regulationUserInfoMapper.mapRow(rs);
		regulationUserInfo.setRegulationQuestionnaireStatus(regulationQuestionnaireStatus);
		result = regulationUserInfo;
	}

	/**
	 * @return the result
	 */
	public RegulationUserInfo getResult() {
		return result;
	}

}
