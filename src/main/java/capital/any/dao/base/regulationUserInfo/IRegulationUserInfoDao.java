package capital.any.dao.base.regulationUserInfo;

import capital.any.model.base.RegulationUserInfo;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IRegulationUserInfoDao {
	public static final String INSERT = 
			"  INSERT " +
		    "  INTO regulation_user_info " +
			"  ( " +
		    "     user_id, " +
		    "     regulation_questionnaire_status_id " +
		    "  ) " +
		    "  VALUES " +
		    "  ( " +
		    " 	  :userId, " +
		    " 	  :regulationQuestionnaireStatusId " +
		    "  ) ";
	
	public static final String UPDATE = 
			"	UPDATE " +
			" 		regulation_user_info r_ui  " +
			"	SET " +
			"		r_ui.regulation_questionnaire_status_id = :regulationQuestionnaireStatusId, " +
			"		r_ui.country_risk_id = :countryRiskId, " +
			"		r_ui.writer_id = :writerId, " +
			"		r_ui.time_modified = now() " +
			"	WHERE " +
			"		r_ui.user_id = :userId";
	
	public static final String GET = 
			"	SELECT " +
			" 		r_ui.*, " +
			" 		regulation_qs.id regulation_qs_id, " +
			" 		regulation_qs.name regulation_qs_name, " +
			" 		regulation_qs.display_name regulation_qs_display_name " +
			" 	FROM " +
			" 		regulation_user_info r_ui, " +
			" 		regulation_questionnaire_statuses regulation_qs " +
			" 	WHERE " + 
			" 		r_ui.regulation_questionnaire_status_id = regulation_qs.id " +
			" 		and r_ui.user_id = :userId ";

	void insert(RegulationUserInfo regulationUserInfo) throws Exception;

	void update(RegulationUserInfo regulationUserInfo) throws Exception;

	RegulationUserInfo get(long userId);
}
