package capital.any.dao.base.regulationUserInfo;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.dao.base.writer.BaseWriterFactory;
import capital.any.model.base.AbstractWriter;
import capital.any.model.base.Country;
import capital.any.model.base.CountryRisk;
import capital.any.model.base.RegulationQuestionnaireStatus;
import capital.any.model.base.RegulationUserInfo;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
public class RegulationUserInfoMapper extends MapperBase implements RowMapper<RegulationUserInfo> {
	
	@Autowired
	private BaseWriterFactory writerFactory;

	@Override
	public RegulationUserInfo mapRow(ResultSet rs, int rowNum) throws SQLException {
		int writerId = rs.getInt("writer_id");
		AbstractWriter writer = writerFactory.createWriter(writerId);
		writer.setId(writerId);
		RegulationUserInfo regulationUserInfo = new RegulationUserInfo();
		regulationUserInfo.setId(rs.getLong("id"));
		regulationUserInfo.setUserId(rs.getLong("user_id"));
		regulationUserInfo.setRegulationQuestionnaireStatus(new RegulationQuestionnaireStatus(rs.getInt("regulation_questionnaire_status_id")));
		regulationUserInfo.setCountryRisk(new CountryRisk(rs.getInt("country_risk_id")));
		regulationUserInfo.setTimeCreated(convertTimestampToDate(rs.getTimestamp("time_created")));
		regulationUserInfo.setTimeModified(convertTimestampToDate(rs.getTimestamp("time_modified")));
		regulationUserInfo.setWriter(writer);
		return regulationUserInfo;
	}
	
	public RegulationUserInfo mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}

}
