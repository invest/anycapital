package capital.any.dao.base.contact;

import capital.any.model.base.Contact;

/**
 * @author eran.levy
 *
 */
public interface IContactDao {
	
	public static final String INSERT_CONTACT = 
			" INSERT " +
		    " INTO contacts " +
		    " ( " +
		    "   first_name, " +
		    "   last_name, " +
		    "   type_id, " +
		    "   mobile_phone, " +
		    "	email," +
		    "   country_id, " +
		    "   language_id, " +
		    "   writer_id, " +
		    "   utc_offset, " +
		    "   ip, " +
		    "   user_agent, " +
		    "   is_contact_by_email, " +
		    "   is_contact_by_sms, " +
//		    "   class_id, " +
		    "   action_source_id, " +
		    "   http_referer, " +
		    "   issue_id, " +
		    "   comments " +
		    " ) " +
		    " VALUES " +
		    " ( " +
		    "	:first_name, " +
		    "	:last_name, " +
		    "	:type_id, " +
		    "	:mobile_phone, " +
		    "	:email, " +
		    "	:country_id, " +
		    "	:language_id, " +	    
		    "   :writer_id, " +
		    "   :utc_offset, " +
		    "	:ip, " +
		    "   :user_agent, " +
		    "   :is_contact_by_email, " +
		    "   :is_contact_by_sms, " +
//		    "   :class_id, " +
		    "   :action_source_id, " +
		    "   :http_referer, " +
		    "   :issue_id, " +
		    "   :comments " +		    
		    "  )";
		
	/**
	 * insert contact
	 * @param contact
	 */
	void insert(Contact contact);
}
