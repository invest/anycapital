package capital.any.dao.base.contact;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.base.Contact;

/**
 * @author eran.levy
 *
 */
@Repository
public class ContactDao implements IContactDao {
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;

	
	@Override
	public void insert(Contact contact) {		
		SqlParameterSource parameters = new MapSqlParameterSource
				("first_name", contact.getFirstName())
				.addValue("last_name", contact.getLastName())
				.addValue("type_id", contact.getType().getToken())
				.addValue("mobile_phone", contact.getMobilePhone())
				.addValue("email", contact.getEmail())
				.addValue("country_id", contact.getCountryId())
				.addValue("language_id", contact.getLanguageId())				
				.addValue("writer_id", contact.getWriterId() == 0 ? null : contact.getWriterId())
				.addValue("utc_offset", contact.getUtcOffset())
				.addValue("ip", contact.getIp())
				.addValue("user_agent", contact.getUserAgent())
				.addValue("is_contact_by_email", contact.getIsContactByEmail())
				.addValue("is_contact_by_sms", contact.getIsContactBySms())					
				//.addValue("class_id", contact.getClazz().getToken())
				.addValue("action_source_id", contact.getActionSource().getId())
				.addValue("http_referer", contact.getHttpReferer())
				.addValue("issue_id", contact.getIssue() != null ? contact.getIssue().getToken() : contact.getIssue())
				.addValue("comments", contact.getComments());
		
		jdbcTemplate.update(INSERT_CONTACT, parameters);
	}
	
}
