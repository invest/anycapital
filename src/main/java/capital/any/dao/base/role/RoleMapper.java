package capital.any.dao.base.role;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.base.Role;

@Component
public class RoleMapper implements RowMapper<Role> {

	@Override
	public Role mapRow(ResultSet rs, int row) throws SQLException {
		Role role = new Role();
		role.setId(rs.getLong("role.id"));
		role.setRole(rs.getString("role.role_name"));
		return role;
	}
	
	public Role mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
	

}

