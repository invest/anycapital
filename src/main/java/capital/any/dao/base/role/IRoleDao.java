package capital.any.dao.base.role;

import java.util.List;

import capital.any.model.base.Role;

public interface IRoleDao {
	
	public static final String GET_ROLES_BY_USER_ID = 
			"SELECT " +
				"* " + 
			"FROM "	+ 
				"role,user_2_role " +
			"WHERE " +
				"user_2_role.user_id = :userId " +
				"AND role.id = user_2_role.id";
	
	List<Role> getRoleByUserId(long userId);

}
