package capital.any.dao.base.role;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import capital.any.model.base.Role;

@Repository
public class RoleDao implements IRoleDao {
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private RoleMapper roleMapper;

	@Override
	public List<Role> getRoleByUserId(long userId) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("userId", userId);
		List<Role> roles = jdbcTemplate.query(GET_ROLES_BY_USER_ID, params, roleMapper);
		return roles;
	}  

}
