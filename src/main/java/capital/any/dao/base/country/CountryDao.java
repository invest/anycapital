package capital.any.dao.base.country;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import capital.any.model.base.Country;

@Primary
@Repository
public class CountryDao implements ICountryDao {
	private static final Logger logger = LoggerFactory.getLogger(CountryDao.class);
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private CountryMapper countryMapper;
	
	@Override
	public Map<Integer, Country> get() {	
		List<Country> tmp = jdbcTemplate.query(GET_COUNTRIES_LIST, new BeanPropertyRowMapper<Country>(Country.class));
		Map<Integer, Country> hm = tmp.stream().collect(Collectors.toMap(p-> p.getId(), p -> p));		 		 
		return hm;
	}

	@Override
	public Country getByA2(String A2) {
		Country country = null;
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("A2", A2);
		try {
			country = jdbcTemplate.queryForObject(GET_COUNTRY_BY_A2, params, countryMapper);
		} catch (DataAccessException e) {
			logger.error("can't find country by A2: " + A2);
		}
		
		return country;
	}

//	@Override
//	public List<Role> getRoleByUserId(long userId) {
//		MapSqlParameterSource params = new MapSqlParameterSource();
//		params.addValue("userId", userId);
//		List<Role> roles = jdbcTemplate.query(GET_ROLES_BY_USER_ID, params, roleMapper);
//		return roles;
//	} 

}
