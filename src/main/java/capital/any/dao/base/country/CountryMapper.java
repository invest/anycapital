package capital.any.dao.base.country;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.base.Country;
import capital.any.model.base.CountryRisk;

/**
 * @author Eran
 *
 */
@Component
public class CountryMapper implements RowMapper<Country> {
	
	@Override
	public Country mapRow(ResultSet rs, int row) throws SQLException {
		Country country = new Country();
		country.setId(rs.getInt("id"));
		country.setCountryName(rs.getString("country_name"));
		country.setA2(rs.getString("a2"));
		country.setA3(rs.getString("a3"));
		country.setPhoneCode(rs.getString("phone_code"));
		country.setDisplayName(rs.getString("display_name"));
		country.setSupportPhone(rs.getString("support_phone"));
		country.setSupportFax(rs.getString("support_fax"));
		country.setGmtOffset(rs.getString("gmt_offset"));
		country.setCountryRisk(new CountryRisk(rs.getInt("risk_id")));
		country.setDefaultLanguageId(rs.getInt("default_language_id"));
		country.setIsBlocked(rs.getInt("is_blocked"));
		return country;
	}
	
	public Country mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
			
}
	

