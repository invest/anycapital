package capital.any.dao.base.country;

import java.util.ArrayList;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.hazelcast.core.IMap;
import com.hazelcast.query.SqlPredicate;
import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.Country;

/**
 * @author LioR SoLoMoN
 *
 */
@Repository("CountryHCDao")
public class CountryHCDao implements ICountryDao {
	private static final Logger logger = LoggerFactory.getLogger(CountryHCDao.class);
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, Country> get() {	
		logger.info("CountryHCDao get");
		Map<Integer, Country> countries = null;		
		try {
			countries = (Map<Integer, Country>) HazelCastClientFactory.getClient().getMap(Constants.MAP_COUNTRIES);
		} catch (Exception e) {
			logger.error("ERROR!", e);
		}
		return countries;
	}
	@SuppressWarnings("unchecked")
	@Override
	public Country getByA2(String A2) {
		logger.info("CountryHCDao getByA2");
		Country country = null;		
		try {
			SqlPredicate predicate = new SqlPredicate("A2 = " + A2);
			ArrayList<Country> countries =  new ArrayList<Country>(((IMap<Integer, Country>) 
					HazelCastClientFactory.getClient().getMap(Constants.MAP_COUNTRIES))
					.values(predicate));
			if(countries != null && countries.size() > 0) {
				country = countries.get(0);
			}
		
		} catch (Exception e) {
			logger.error("ERROR!get country by A2 HC", e);
		}
		return country;
	}
}
