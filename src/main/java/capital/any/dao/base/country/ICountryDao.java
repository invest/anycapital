package capital.any.dao.base.country;

import java.util.Map;
import capital.any.model.base.Country;

/**
 * @author eranl
 *
 */
public interface ICountryDao {
	
	public static final String GET_COUNTRIES_LIST = 
			" SELECT " +
			"	* " + 
			" FROM "	+ 
			"	countries ";
	
	public static final String GET_COUNTRY_BY_A2 = 
			"SELECT " +
			"	* " + 
			"FROM "	+ 
			"	countries c " + 
			"WHERE " +
			"	upper(c.a2) = upper(:A2)";
	
	/**
	 * @return
	 */
	Map<Integer, Country> get();
	
	/**
	 * @return Country by given A2
	 */
	Country getByA2(String A2);

}
