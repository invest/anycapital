package capital.any.dao.base.fileStatus;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import capital.any.model.base.FileStatus;

/**
 * 
 * @author Eyal.o
 *
 */
@Repository
public class FileStatusDao implements IFileStatusDao {

	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private FileStatusMapper fileStatusMapper;
	
	@Override
	public Map<Integer, FileStatus> get() {
		List<FileStatus> tmp = jdbcTemplate.query(GET, fileStatusMapper);
		Map<Integer, FileStatus> hm = tmp.stream().collect(Collectors.toMap(p-> p.getId(), p -> p));
		return hm;
	}

}
