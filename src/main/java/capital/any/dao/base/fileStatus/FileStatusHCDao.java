package capital.any.dao.base.fileStatus;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.FileStatus;

/**
 * 
 * @author Eyal.o
 *
 */
@Repository("FileStatusHCDao")
public class FileStatusHCDao implements IFileStatusDao {
	private static final Logger logger = LoggerFactory.getLogger(FileStatusHCDao.class);

	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, FileStatus> get() {
		Map<Integer, FileStatus> map = null;
		try {
			map = (Map<Integer, FileStatus>) HazelCastClientFactory.getClient().getMap(Constants.MAP_FILE_STATUS);
		} catch (Exception e) {
			logger.error("ERROR! get map of FileStatus by HC", e);
		}
		return map;
	}
}
