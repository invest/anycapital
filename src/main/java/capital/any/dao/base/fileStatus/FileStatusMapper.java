package capital.any.dao.base.fileStatus;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.FileStatus;

/**
 * 
 * @author Eyal.o
 *
 */
@Component
public class FileStatusMapper extends MapperBase implements RowMapper<FileStatus>{

	@Override
	public FileStatus mapRow(ResultSet rs, int rowNum) throws SQLException {
		FileStatus fileStatus = new FileStatus();
		fileStatus.setId(rs.getInt("fs_id"));
		fileStatus.setName(rs.getString("fs_name"));
		fileStatus.setDisplayName(rs.getString("fs_display_name"));
		fileStatus.setRank(rs.getInt("fs_rank"));
		return fileStatus;
	}

	public FileStatus mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
}
