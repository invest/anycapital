package capital.any.dao.base.fileStatus;

import java.util.Map;

import capital.any.model.base.FileStatus;

/**
 * 
 * @author Eyal.o
 *
 */
public interface IFileStatusDao {
	
	public static final String GET = 
			" SELECT " +
			" 	fs.id as fs_id, " +
			" 	fs.name as fs_name, " +
			" 	fs.display_name as fs_display_name, " +
			" 	fs.rank as fs_rank  " +
			" FROM " +
			" 	file_statuses fs ";

	Map<Integer, FileStatus> get();

}
