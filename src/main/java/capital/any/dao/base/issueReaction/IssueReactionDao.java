package capital.any.dao.base.issueReaction;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import capital.any.model.base.IssueReaction;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository
public class IssueReactionDao implements IIssueReactionDao {

	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private IssueReactionMapper issueReactionMapper;
	
	@Override
	public Map<Integer, IssueReaction> get() {
		List<IssueReaction> tmp = jdbcTemplate.query(GET_ISSUE_REACTION_LIST, issueReactionMapper);
		Map<Integer, IssueReaction> hm = tmp.stream().collect(Collectors.toMap(p-> p.getId(), p -> p));		 		 
		return (HashMap<Integer, IssueReaction>) hm;
	}

}
