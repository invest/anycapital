package capital.any.dao.base.issueReaction;

import java.util.Map;

import capital.any.model.base.IssueReaction;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IIssueReactionDao {
	
	public static final String GET_ISSUE_REACTION_LIST =
			" SELECT " +
				" iss_r.id iss_r_id, " +
				" iss_r.name iss_r_name, " +
				" iss_r.reached_status_id iss_r_reached_status_id " +
			" FROM "	+ 
				" issue_reactions iss_r ";
	
	Map<Integer, IssueReaction> get();
	
}
