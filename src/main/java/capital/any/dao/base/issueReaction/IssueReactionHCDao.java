package capital.any.dao.base.issueReaction;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.IssueReaction;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository("IssueReactionHCDao")
public class IssueReactionHCDao implements IIssueReactionDao {
	private static final Logger logger = LoggerFactory.getLogger(IssueReactionHCDao.class);
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, IssueReaction> get() {
		Map<Integer, IssueReaction> issueReactions = null;
		try {
			issueReactions = (Map<Integer, IssueReaction>) HazelCastClientFactory.getClient().getMap(Constants.MAP_ISSUE_REACTIONS);
		} catch (Exception e) {
			logger.error("Problem to get issueReactions by HC.");
		}
		return issueReactions;
	}

}
