package capital.any.dao.base.issueReaction;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.IssueReaction;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
public class IssueReactionMapper extends MapperBase implements RowMapper<IssueReaction> {

	@Override
	public IssueReaction mapRow(ResultSet rs, int rowNum) throws SQLException {
		IssueReaction issueReaction = new IssueReaction();
		issueReaction.setId(rs.getInt("iss_r_id"));
		issueReaction.setName(rs.getString("iss_r_name"));
		issueReaction.setReachedStatusId(rs.getInt("iss_r_reached_status_id"));
		return issueReaction;
	}
	
	public IssueReaction mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}

}
