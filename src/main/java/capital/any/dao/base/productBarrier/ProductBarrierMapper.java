package capital.any.dao.base.productBarrier;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.ProductBarrier;
import capital.any.model.base.ProductBarrierType;

/**
 * @author Eyal G
 *
 */
@Component
public class ProductBarrierMapper extends MapperBase implements RowMapper<ProductBarrier> {
	
	@Override
	public ProductBarrier mapRow(ResultSet rs, int row) throws SQLException {
		ProductBarrier productBarrier = new ProductBarrier();
		productBarrier.setProductId(rs.getLong("product_id"));
		productBarrier.setBarrierStart(convertTimestampToDate(rs.getTimestamp("barrier_start")));
		productBarrier.setBarrierEnd(convertTimestampToDate(rs.getTimestamp("barrier_end")));
		productBarrier.setBarrierLevel(rs.getDouble("barrier_level"));
		productBarrier.setBarrierOccur(rs.getInt("is_barrier_occur") == 0 ? false : true);
		ProductBarrierType ProductBarrierType = new ProductBarrierType();
		ProductBarrierType.setId(rs.getInt("product_barrier_type_id"));
		productBarrier.setProductBarrierType(ProductBarrierType);
		return productBarrier;
	}
	
	public ProductBarrier mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
			
}
	

