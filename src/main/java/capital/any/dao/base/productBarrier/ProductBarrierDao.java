package capital.any.dao.base.productBarrier;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.base.ProductBarrier;

/**
 * @author Eyal G
 *
 */
@Repository
public class ProductBarrierDao implements IProductBarrierDao {
	private static final Logger logger = LoggerFactory.getLogger(ProductBarrierDao.class);
	
	@Autowired
	protected NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private ProductBarrierMapper productBarrierMapper;
	
	@Override
	public HashMap<Long, ProductBarrier> get() {
		List<ProductBarrier> tmp = jdbcTemplate.query(GET_PRODUCT_BARRIERS, productBarrierMapper);
		Map<Long, ProductBarrier> hm = tmp.stream().collect(Collectors.toMap(p-> p.getProductId(), p -> p));	
		return (HashMap<Long, ProductBarrier>) hm;
	}
	
	@Override
	public ProductBarrier get(long id) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("product_id", id);
		ProductBarrier productBarrier = null;
		try {
			productBarrier = (ProductBarrier)jdbcTemplate.queryForObject(GET_PRODUCT_BARRIER, namedParameters, productBarrierMapper);
		} catch (DataAccessException e) {
			logger.warn("can't find product Barrier! for product id " + id, e.getMessage());
		}
		return productBarrier;
	}
	
}
