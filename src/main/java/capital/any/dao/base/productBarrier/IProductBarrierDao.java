package capital.any.dao.base.productBarrier;

import java.util.HashMap;

import capital.any.model.base.ProductBarrier;

/**
 * @author Eyal G
 *
 */
public interface IProductBarrierDao {
	
	public static final String GET_PRODUCT_BARRIERS = 
			" SELECT " +
			"	* " + 
			" FROM "	+ 
			"	product_barriers ";
		
	public static final String GET_PRODUCT_BARRIER = 
			" SELECT " +
			"	* " + 
			" FROM "	+ 
			"	product_barriers " +
			" WHERE " +
			"	product_id = :product_id";
	
	HashMap<Long, ProductBarrier> get();
	
	ProductBarrier get(long productId);

}
