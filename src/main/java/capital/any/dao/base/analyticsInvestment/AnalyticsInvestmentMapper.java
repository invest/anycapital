package capital.any.dao.base.analyticsInvestment;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.base.analytics.AnalyticsInvestment;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
public class AnalyticsInvestmentMapper implements RowMapper<AnalyticsInvestment> {

	@Override
	public AnalyticsInvestment mapRow(ResultSet rs, int rowNum) throws SQLException {
		AnalyticsInvestment analyticsInvestment = new AnalyticsInvestment();
		analyticsInvestment.setInvestmentCount(rs.getInt("investment_count"));
		analyticsInvestment.setInvestmentSum(rs.getLong("investment_sum"));
		return analyticsInvestment;
	}
	
	public AnalyticsInvestment mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}

}
