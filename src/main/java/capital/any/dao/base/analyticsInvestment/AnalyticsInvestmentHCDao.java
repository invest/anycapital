package capital.any.dao.base.analyticsInvestment;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.analytics.AnalyticsInvestment;
import capital.any.model.base.analytics.AnalyticsInvestmentProduct;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository("AnalyticsInvestmentHCDao")
public class AnalyticsInvestmentHCDao implements IAnalyticsInvestmentDao {
	private static final Logger logger = LoggerFactory.getLogger(AnalyticsInvestmentHCDao.class);

	@Override
	public AnalyticsInvestment get() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<AnalyticsInvestmentProduct> getByProduct() {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, AnalyticsInvestment> getAnalyticsInvestment() {
		Map<Integer, AnalyticsInvestment> map = null;
		try {
			map = (Map<Integer, AnalyticsInvestment>) HazelCastClientFactory.getClient().getMap(Constants.MAP_ANALYTICS_INVESTMENT);
		} catch (Exception e) {
			logger.error("ERROR! get map of AnalyticsInvestment by HC", e);
		}
		return map;
	}

}
