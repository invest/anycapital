package capital.any.dao.base.analyticsInvestment;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.base.analytics.AnalyticsInvestmentProduct;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
public class AnalyticsInvestmentProductMapper implements RowMapper<AnalyticsInvestmentProduct> {

	@Override
	public AnalyticsInvestmentProduct mapRow(ResultSet rs, int rowNum) throws SQLException {
		AnalyticsInvestmentProduct analyticsInvestmentProduct = new AnalyticsInvestmentProduct();
		analyticsInvestmentProduct.setProductTypeId(rs.getInt("product_type_id"));
		analyticsInvestmentProduct.setProductTypeName(rs.getString("product_type_name"));
		analyticsInvestmentProduct.setInvestmentCount(rs.getInt("investment_count"));
		analyticsInvestmentProduct.setInvestmentSum(rs.getLong("investment_sum"));
		return analyticsInvestmentProduct;
	}
	
	public AnalyticsInvestmentProduct mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}

}
