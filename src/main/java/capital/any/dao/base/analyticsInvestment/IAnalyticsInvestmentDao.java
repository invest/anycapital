package capital.any.dao.base.analyticsInvestment;

import java.util.List;
import java.util.Map;

import capital.any.model.base.analytics.AnalyticsInvestment;
import capital.any.model.base.analytics.AnalyticsInvestmentProduct;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IAnalyticsInvestmentDao {
	public static final String GET = 
			" SELECT " + 
				" * " +
			" FROM " +
				" analytics_investment ";
	
	public static final String GET_BY_PRODUCT = 
			" SELECT " + 
				" * " +
			" FROM " +
				" analytics_investment_by_product ";
	
	AnalyticsInvestment get();

	List<AnalyticsInvestmentProduct> getByProduct();
	
	Map<Integer, AnalyticsInvestment> getAnalyticsInvestment();
	
}
