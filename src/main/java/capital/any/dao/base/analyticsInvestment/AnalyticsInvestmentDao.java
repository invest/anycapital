package capital.any.dao.base.analyticsInvestment;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import capital.any.model.base.analytics.AnalyticsInvestment;
import capital.any.model.base.analytics.AnalyticsInvestmentProduct;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository
public class AnalyticsInvestmentDao implements IAnalyticsInvestmentDao {
	private static final Logger logger = LoggerFactory.getLogger(AnalyticsInvestmentDao.class);

	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private AnalyticsInvestmentMapper analyticsInvestmentMapper;
	@Autowired
	private AnalyticsInvestmentProductMapper analyticsInvestmentProductMapper;
	
	@Override
	public AnalyticsInvestment get() {
		AnalyticsInvestment analyticsInvestment = null;
		try {
			analyticsInvestment = jdbcTemplate.queryForObject(GET, new MapSqlParameterSource(), analyticsInvestmentMapper);
		} catch (Exception e) {
			logger.error("Can't find AnalyticsInvestment.");
		}
		return analyticsInvestment;
	}
	
	@Override
	public List<AnalyticsInvestmentProduct> getByProduct() {
		List<AnalyticsInvestmentProduct> list = null;
		try {
			list = jdbcTemplate.query(GET_BY_PRODUCT, new MapSqlParameterSource(), analyticsInvestmentProductMapper);
		} catch (Exception e) {
			logger.error("Can't find list of AnalyticsInvestmentProduct.");
		}
		return list;
	}

	@Override
	public Map<Integer, AnalyticsInvestment> getAnalyticsInvestment() {
		// TODO Auto-generated method stub
		return null;
	}

}
