package capital.any.dao.base.marketingDomains;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import capital.any.model.base.MarketingDomain;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository
public class MarketingDomainDao implements IMarketingDomainDao {

	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private MarketingDomainMapper marketingDomainMapper;

	@Override
	public Map<Integer, MarketingDomain> getMarketingDomains() {
		List<MarketingDomain> tmp = jdbcTemplate.query(GET_MARKETING_DOMAIN_LIST, marketingDomainMapper);
		Map<Integer, MarketingDomain> hm = tmp.stream().collect(Collectors.toMap(p-> p.getId(), p -> p));
		return hm;
	}
}
