package capital.any.dao.base.marketingDomains;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.MarketingDomain;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
public class MarketingDomainMapper extends MapperBase implements RowMapper<MarketingDomain> {
	@Override
	public MarketingDomain mapRow(ResultSet rs, int row) throws SQLException {
		MarketingDomain marketingDomain = new MarketingDomain();
		marketingDomain.setId(rs.getInt("id"));
		marketingDomain.setDomain(rs.getString("domain"));
		return marketingDomain;
	}
	
	public MarketingDomain mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
}
