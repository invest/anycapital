package capital.any.dao.base.marketingDomains;

import java.util.Map;

import capital.any.model.base.MarketingDomain;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IMarketingDomainDao {
	
	public static final String GET_MARKETING_DOMAIN_LIST =
			" SELECT " +
				" * " + 
			" FROM "	+ 
				" marketing_domains ";
	
	/**
	 * Get marketing Domains
	 * @return Map<Long, MarketingDomain>
	 */
	Map<Integer, MarketingDomain> getMarketingDomains();
	
}
