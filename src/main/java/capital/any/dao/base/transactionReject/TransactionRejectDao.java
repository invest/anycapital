package capital.any.dao.base.transactionReject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import capital.any.model.base.TransactionReject;

/**
 * @author LioR SoLoMoN
 *
 */
@Repository
public class TransactionRejectDao implements ITransactionRejectDao {
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;

	@Override
	public void insert(TransactionReject transactionReject) {		
		SqlParameterSource parameters = new MapSqlParameterSource
						("user_id", transactionReject.getUserId())
				.addValue("payment_type_id", transactionReject.getPaymentType().getId())
				.addValue("reject_type_id", transactionReject.getRejectType().getId())
				.addValue("operation_id", transactionReject.getOperation().getId())
				.addValue("amount", transactionReject.getAmount())
				.addValue("action_source_id", transactionReject.getActionSource().getId())
				.addValue("writer_id", transactionReject.getWriterId())
				.addValue("info", transactionReject.getInfo())
				.addValue("session_id", transactionReject.getSessionId())
				.addValue("server_name", transactionReject.getServerName());
		jdbcTemplate.update(INSERT, parameters);
	}	
}
