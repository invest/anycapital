package capital.any.dao.base.investmentHistory;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import capital.any.base.enums.ActionSource;
import capital.any.dao.base.MapperBase;
import capital.any.model.base.InvestmentHistory;
import capital.any.model.base.InvestmentStatus;


/**
 * @author Eyal G
 *
 */
@Component
public class InvestmentHistoryMapper extends MapperBase implements RowMapper<InvestmentHistory> {
	
	@Override
	public InvestmentHistory mapRow(ResultSet rs, int row) throws SQLException {
		InvestmentHistory investmentHistory = new InvestmentHistory();
		investmentHistory.setActionSource(ActionSource.get(rs.getInt("action_source_id")));
		investmentHistory.setInvestmentStatus(new InvestmentStatus(rs.getInt("investment_status_id")));
		investmentHistory.setInvestmntId(rs.getLong("investmnt_id"));
		investmentHistory.setServerName(rs.getString("server_name"));
		investmentHistory.setTimeCreated(convertTimestampToDate(rs.getTimestamp("time_created")));
		investmentHistory.setWriterId(rs.getInt("writer_id"));
		investmentHistory.setIp(rs.getString("ip"));
		investmentHistory.setLoginId(rs.getLong("login_id"));
		return investmentHistory;
	}
	
	public InvestmentHistory mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
			
}
	

