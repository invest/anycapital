package capital.any.dao.base.investmentHistory;

import java.util.List;

import capital.any.model.base.InvestmentHistory;

/**
 * @author EyalG
 *
 */
public interface IInvestmentHistoryDao {
	
	public static final String INSERT_INVESTMENTS_HISTORY = 
			"INSERT " +
		    "INTO investments_history " +
		    "  ( " +
		    "    action_source_id , " +
		    "    writer_id , " +
		    "    investmnt_id , " +
		    "    investment_status_id , " +
		    "    ip , " +
		    "    login_id , " +
		    "    server_name " +
		    "  ) " +
		    "  VALUES " +
		    "  ( " +
		    "    :actionSourceId , " +
		    "    :writerId , " +
		    "    :investmntId , " +
		    "    :investmentStatusId , " +
		    "    :ip , " +
		    "    :loginId , " +
		    "    :serverName " +
		    "   ) ";
	
	public static final String GET_INVESTMENTS_HISTORY =
			"SELECT " +
				" * " +
			"FROM " +
				"investments_history ";
	
	/**
	 * insert investment
	 * @param investment
	 */
	void insert(InvestmentHistory investmentHistory);
	
	/**
	 * select all investments
	 * @return List<Investment>
	 */
	List<InvestmentHistory> get();
}
