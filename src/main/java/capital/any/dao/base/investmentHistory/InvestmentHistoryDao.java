package capital.any.dao.base.investmentHistory;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.base.InvestmentHistory;

/**
 * @author EyalG
 *
 */
@Repository
public class InvestmentHistoryDao implements IInvestmentHistoryDao {
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private InvestmentHistoryMapper investmentHistoryMapper;
	
	@Override
	public void insert(InvestmentHistory investmentHistory) {
		SqlParameterSource parameters = new MapSqlParameterSource
						 ("actionSourceId", investmentHistory.getActionSource().getId())
				.addValue("writerId", investmentHistory.getWriterId() == 0 ? null : investmentHistory.getWriterId())
				.addValue("investmntId", investmentHistory.getInvestmntId())
				.addValue("investmentStatusId", investmentHistory.getInvestmentStatus().getId())
				.addValue("ip", investmentHistory.getIp())
				.addValue("loginId", investmentHistory.getLoginId() == 0 ? null : investmentHistory.getLoginId())
				.addValue("serverName", investmentHistory.getServerName());
		jdbcTemplate.update(INSERT_INVESTMENTS_HISTORY, parameters);
	}

	@Override
	public List<InvestmentHistory> get() {
		return jdbcTemplate.query(GET_INVESTMENTS_HISTORY, investmentHistoryMapper);
	}
	
}
