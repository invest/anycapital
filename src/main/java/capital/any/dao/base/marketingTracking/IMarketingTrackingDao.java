package capital.any.dao.base.marketingTracking;

import capital.any.model.base.MarketingTracking;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IMarketingTrackingDao {
	
	public static final String INSERT_MARKETING_TRACKING = 
			"  INSERT " +
		    "  INTO marketing_tracking " +
			"  ( " +
		    "     campaign_id, " +
		    " 	  query_string, " +
		    "     ab_name, " +
		    "     ab_value " +
		    "  ) " +
		    "  VALUES " +
		    "  ( " +
		    " 	  :campaignId, " +
		    " 	  :queryString, " +
		    "     :abName, " +
		    "     :abValue " +
		    "  ) ";
	
	/**
	 * Insert marketing tracking
	 * @param marketingTracking
	 */
	void insert(MarketingTracking marketingTracking);
	
}
