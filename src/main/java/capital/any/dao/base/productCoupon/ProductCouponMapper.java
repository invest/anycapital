package capital.any.dao.base.productCoupon;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import capital.any.dao.base.MapperBase;
import capital.any.model.base.ProductCoupon;

/**
 * @author Eran
 *
 */
@Component
public class ProductCouponMapper extends MapperBase implements RowMapper<ProductCoupon> {
	
	@Override
	public ProductCoupon mapRow(ResultSet rs, int row) throws SQLException {
		ProductCoupon productCoupon = new ProductCoupon();
		productCoupon.setId(rs.getInt("id"));
		productCoupon.setProductId(rs.getLong("product_id"));
		productCoupon.setObservationDate(convertTimestampToDate(rs.getTimestamp(("observation_date"))));
		productCoupon.setPaymentDate(convertTimestampToDate(rs.getTimestamp(("payment_date"))));
		productCoupon.setTriggerLevel(rs.getDouble("trigger_level"));
		productCoupon.setPayRate(rs.getDouble("pay_rate"));
		productCoupon.setPaid(rs.getInt("is_paid") == 0 ? false : true);
		productCoupon.setObservationLevel(rs.getDouble("observation_level"));
		return productCoupon;
	}
	
	public ProductCoupon mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
}
	

