package capital.any.dao.base.productCoupon;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Component;

import capital.any.model.base.ProductCoupon;

/**
 * @author eranl
 *
 */
@Component
@Scope("prototype")
public class ProductCouponCallbackHandler implements RowCallbackHandler {
	
	private HashMap<Long, List<ProductCoupon>> result = new HashMap<Long, List<ProductCoupon>>();
	@Autowired
	private ProductCouponMapper productCouponMapper;
	
	@Override
	public void processRow(ResultSet rs) throws SQLException {
		ProductCoupon productCoupon = productCouponMapper.mapRow(rs);		
		List<ProductCoupon> productCoupons = result.get(productCoupon.getProductId());
		if (productCoupons == null) {
			productCoupons = new ArrayList<ProductCoupon>();		
		}
		productCoupons.add(productCoupon);
		result.put(productCoupon.getProductId(), productCoupons);		
	}

	/**
	 * @return the result
	 */
	public HashMap<Long, List<ProductCoupon>> getResult() {
		return result;
	}

	/**
	 * @param result the result to set
	 */
	public void setResult(HashMap<Long, List<ProductCoupon>> result) {
		this.result = result;
	}
	
}
