package capital.any.dao.base.productCoupon;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import capital.any.model.base.ProductCoupon;

/**
 * @author eranl
 *
 */
@Repository
public class ProductCouponDao implements IProductCouponDao, ApplicationContextAware {
	
	private ApplicationContext appContext;
	
	@Autowired
	protected NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	protected ProductCouponMapper productCouponMapper;

	@Override
	public Map<Long, List<ProductCoupon>> get() { 
		ProductCouponCallbackHandler callbackHandler = appContext.getBean(ProductCouponCallbackHandler.class);
		jdbcTemplate.query(GET_PRODUCT_COUPONS_LIST, callbackHandler);
		HashMap<Long, List<ProductCoupon>> hm = callbackHandler.getResult();
		return hm;
	}

	@Override
	public List<ProductCoupon> getProductCoupons(long productId) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("productId", productId);
		return jdbcTemplate.query(GET_PRODUCT_COUPONS_LIST_BY_PRODUCT,
				namedParameters, productCouponMapper);
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.appContext = applicationContext;		
	}	
}
