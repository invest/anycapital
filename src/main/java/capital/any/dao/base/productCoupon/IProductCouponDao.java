package capital.any.dao.base.productCoupon;

import java.util.List;
import java.util.Map;
import capital.any.model.base.ProductCoupon;

/**
 * @author eranl
 *
 */
public interface IProductCouponDao {
	
	public static final String GET_PRODUCT_COUPONS_LIST = 
			" SELECT " +
			"	* " + 
			" FROM "	+ 
			"	product_coupons ";
	
	public static final String GET_PRODUCT_COUPONS_LIST_BY_PRODUCT = 
			" SELECT " +
			"	* " + 
			" FROM "	+ 
			"	product_coupons cp " +
			" WHERE " +
			"	cp.product_id = :productId " +
			" ORDER BY " +
			"	cp.payment_date ";
		
	Map<Long, List<ProductCoupon>> get();
	
	List<ProductCoupon> getProductCoupons(long productId);
}
