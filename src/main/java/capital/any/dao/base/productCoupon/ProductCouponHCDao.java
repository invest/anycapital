package capital.any.dao.base.productCoupon;

import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.ProductCoupon;

/**
 * @author LioR SoLoMoN
 *
 */
@Repository("ProductCouponHCDao")
public class ProductCouponHCDao implements IProductCouponDao {
	private static final Logger logger = LoggerFactory.getLogger(ProductCouponHCDao.class);
	@SuppressWarnings("unchecked")
	@Override
	public Map<Long, List<ProductCoupon>> get() { 
		Map<Long, List<ProductCoupon>> productCoupons = null;		
		try {
			productCoupons = (Map<Long, List<ProductCoupon>>) HazelCastClientFactory.getClient().getMap(Constants.MAP_PRODUCT_COUPONS);
		} catch (Exception e) {
			logger.error("ERROR!", e);
		}
		return productCoupons;	
	}

	@Override
	public List<ProductCoupon> getProductCoupons(long productId) {
		logger.info("ProductCouponHC; getProductCoupons return null"); 
		return null;
	}
}
