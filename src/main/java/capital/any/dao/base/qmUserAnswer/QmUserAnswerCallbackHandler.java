package capital.any.dao.base.qmUserAnswer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Component;

import capital.any.dao.base.qmAnswer.QmAnswerMapper;
import capital.any.dao.base.qmQuestion.QmQuestionMapper;
import capital.any.dao.base.qmQuestionGroup.QmQuestionGroupMapper;
import capital.any.model.base.QmAnswer;
import capital.any.model.base.QmQuestion;
import capital.any.model.base.QmQuestionGroup;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
@Scope("prototype")
public class QmUserAnswerCallbackHandler implements RowCallbackHandler {

	private Map<Integer, QmQuestion> result = new HashMap<Integer, QmQuestion>();
	
	@Autowired
	private QmAnswerMapper qmAnswerMapper;
	@Autowired
	private QmQuestionMapper qmQuestionMapper;
	@Autowired
	private QmQuestionGroupMapper qmQuestionGroupMapper;
	
	@Override
	public void processRow(ResultSet rs) throws SQLException {
		QmAnswer qmAnswer = qmAnswerMapper.mapRow(rs);
		qmAnswer.setSelectedAnswer(rs.getInt("selected_answer"));
		qmAnswer.setTextAnswer(rs.getString("ua_text_answer"));
		QmQuestion qmQuestion = result.get(qmAnswer.getQuestionId());
		if (null == qmQuestion) {
			QmQuestionGroup qmQuestionGroup = qmQuestionGroupMapper.mapRow(rs);
			qmQuestion = qmQuestionMapper.mapRow(rs);
			qmQuestion.setQmAnswerList(new ArrayList<QmAnswer>());
			qmQuestion.setQmQuestionGroup(qmQuestionGroup);
			result.put(qmQuestion.getId(), qmQuestion);
		}
		List<QmAnswer> list = qmQuestion.getQmAnswerList();
		list.add(qmAnswer);
	}

	/**
	 * @return the result
	 */
	public Map<Integer, QmQuestion> getResult() {
		return result;
	}

	/**
	 * @param result the result to set
	 */
	public void setResult(Map<Integer, QmQuestion> result) {
		this.result = result;
	}
}
