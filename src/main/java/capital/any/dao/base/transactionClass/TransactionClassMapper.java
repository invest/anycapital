package capital.any.dao.base.transactionClass;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import capital.any.model.base.TransactionClass;

/**
 * @author LioR SoLoMoN
 *
 */
@Component
public class TransactionClassMapper implements RowMapper<TransactionClass> {	
	@Override
	public TransactionClass mapRow(ResultSet rs, int row) throws SQLException {
		TransactionClass transactionClass = new TransactionClass();		
		transactionClass.setId(rs.getInt("tc_id"));
		transactionClass.setName(rs.getString("tc_name"));
		transactionClass.setDisplayName(rs.getString("tc_display_name"));
		return transactionClass;
	}
	
	public TransactionClass mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}			
}
	

