package capital.any.dao.base.transactionFee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import capital.any.model.base.TransactionFee;

/**
 * @author LioR SoLoMoN
 *
 */
@Primary
@Repository
public class TransactionFeeDao implements ITransactionFeeDao {
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private TransactionFeeMapper transactionFeeMapper;

	@Override
	public TransactionFee get(long transactionId) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("transactionId", transactionId);
		return jdbcTemplate.queryForObject(GET_BY_TRANSACTION_ID, namedParameters, transactionFeeMapper);
	}

	@Override
	public boolean insert(TransactionFee transactionFee) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource
				("amount", transactionFee.getAmount())
				.addValue("timeCreated", transactionFee.getTimeCreated())
				.addValue("transactionId", transactionFee.getTransactionId());
				
		return (jdbcTemplate.update(INSERT, namedParameters)) > 0;
	}
}
