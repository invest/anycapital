package capital.any.dao.base.transactionFee;

import capital.any.model.base.TransactionFee;

/**
 * @author LioR SoLoMoN
 *
 */
public interface ITransactionFeeDao {
	
	public static final String GET_BY_TRANSACTION_ID = 
			"SELECT " +
			"	transaction_id  tf_transaction_id " + 
			"	,amount  tf_amount " +
			"	,time_created tf_time_created " +
			"FROM "	+ 
			"	transactions_fee " +
			"WHERE " +
			" transaction_id = :transactionId ";
	
	public static final String INSERT = 
			"INSERT INTO " +
			"	transactions_fee " +
			"	( " +
			"		transaction_id " +
			"		,amount " +
			"		,time_created " +
			"	) " +
			"VALUES " +
			"	(	" +
			"		:transactionId " +
			"		,:amount " +
			"		,:timeCreated " +
			"	)	";
	
	/**
	 * @param transactionId
	 * @return
	 */
	TransactionFee get(long transactionId);
	
	/**
	 * @param transactionFee
	 * @return
	 */
	boolean insert(TransactionFee transactionFee);

}
