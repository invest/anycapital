package capital.any.dao.base.transactionFee;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import capital.any.dao.base.MapperBase;
import capital.any.model.base.TransactionFee;

/**
 * @author LioR SoLoMoN
 *
 */
@Component
public class TransactionFeeMapper extends MapperBase implements RowMapper<TransactionFee> {
	
	@Override
	public TransactionFee mapRow(ResultSet rs, int row) throws SQLException {
		TransactionFee transactionFee = new TransactionFee();		
		transactionFee.setAmount(rs.getLong("tf_amount"));
		transactionFee.setTimeCreated(convertTimestampToDate(rs.getTimestamp("tf_time_created")));
		transactionFee.setTransactionId(rs.getLong("tf_transaction_id"));
		return transactionFee;
	}
	
	public TransactionFee mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}			
}
	

