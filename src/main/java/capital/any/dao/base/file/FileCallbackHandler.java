package capital.any.dao.base.file;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Component;

import capital.any.dao.base.fileStatus.FileStatusMapper;
import capital.any.model.base.File;
import capital.any.model.base.FileStatus;

/**
 * 
 * @author Eyal.o
 *
 */
@Component
@Scope("prototype")
public class FileCallbackHandler implements RowCallbackHandler {

	private List<File> result = new ArrayList<File>();
	
	@Autowired
	private FileMapper fileMapperMapper;
	@Autowired
	private FileStatusMapper fileStatusMapper;
	
	@Override
	public void processRow(ResultSet rs) throws SQLException {
		File f = fileMapperMapper.mapRow(rs);
		f.getFileType().setDisplayName(rs.getString("display_name"));
		if (!f.getWriter().isNull()) {
			f.getWriter().setUserName(rs.getString("user_name"));
		}
		FileStatus fileStatus = fileStatusMapper.mapRow(rs);
		f.setFileStatus(fileStatus);
		result.add(f);
	}

	/**
	 * @return the result
	 */
	public List<File> getResult() {
		return result;
	}

	/**
	 * @param result the result to set
	 */
	public void setResult(List<File> result) {
		this.result = result;
	}
	
}
