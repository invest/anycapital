package capital.any.dao.base.file;

import java.util.List;

import capital.any.model.base.File;

/**
 * @author eranl
 *
 */
public interface IFileDao {
	
	public static final String GET_BY_USER_ID = 
			" SELECT " +
			"	f.*, " +
			"   ft.display_name, " +
			" 	fs.id as fs_id, " +
			" 	fs.name as fs_name, " +
			" 	fs.display_name as fs_display_name, " +
			" 	fs.rank as fs_rank, " +
			"	w.user_name " +
			" FROM "	+ 
			"	files f, " +
			"   file_types ft, " +
			"   file_statuses fs, " +
			"   writers w " +
			" WHERE " +
			"	f.user_id = :userId " +
			"	AND f.file_type_id = ft.id " +
			"	AND f.status_id = fs.id " +
			"	AND w.id = f.writer_id ";
	
	public static final String INSERT = 
			" INSERT INTO files " +
			"( " +
				" name, " +
				" file_type_id, " +
				" user_id, " +
				" action_source_id, " +
				" writer_id, " +
				" is_primary " +
			") " +
			" VALUES " +
			"( " +
				" :name, " +
				" :fileTypeId, " +
				" :userId, " +
				" :actionSourceId, " +
				" :writerId, " +
				" :isPrimary " +
			") ";
	
	public static final String GET_BY_ID_AND_USER_ID = 
			" SELECT " +
			"	f.*, " + 
			"   ft.display_name, " +
			" 	fs.id as fs_id, " +
			" 	fs.name as fs_name, " +
			" 	fs.display_name as fs_display_name, " +
			" 	fs.rank as fs_rank, " +
			"	w.user_name " + 
			" FROM "	+ 
			"	files f, " +
			"   file_types ft, " +
			"   file_statuses fs, " +
			"   writers w " +
			" WHERE " +
			"	f.user_id = :userId " +
			"	AND f.id = :id " +
			"	AND f.file_type_id = ft.id " +
			"	AND f.status_id = fs.id " +
			"	AND w.id = f.writer_id";
	
	public static final String UPDATE_FILE =
			"UPDATE "
				+ "files "
		  + "SET "
		  		+ "file_type_id = :fileTypeId, "
		  		+ "status_id = :fileStatusId, "
		  		+ "is_primary = :isPrimary "
		  + "WHERE "
		  		+ "id = :id ";
	
	public static final String UPDATE_IS_PRIMARY_FALSE_BY_TYPE_AND_USER =
			"UPDATE "
				+ "files "
		  + "SET "
		  		+ "is_primary = 0 "
		  + "WHERE "
		  		+ "user_id = :userId "
		  		+ "AND file_type_id = :fileTypeId";
	
	/**
	 * get all files for this user id
	 * @param userId
	 * @return List<File>
	 */
	List<File> getByUserId(long userId);
	
	/**
	 * insert file to user
	 * @param file
	 */
	void insert(File file);
	
	/**
	 * get file by user id and file id
	 * @param file
	 * @return file
	 */
	File getByIdAndUserId(File file);
	
	/**
	 * update file
	 * @param file
	 * @return true if update else false
	 */
	boolean updateFile(File file);
	
	/**
	 * update is primary to false by user id and file type
	 * @param file
	 * @return true if update else false
	 */
	boolean updateIsPrimary(File file);

}
