package capital.any.dao.base.file;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.base.File;

/**
 * @author eranl
 *
 */
@Repository
public class FileDao implements IFileDao, ApplicationContextAware {
	private static final Logger logger = LoggerFactory.getLogger(FileDao.class);
	
	private ApplicationContext appContext;
	
	@Autowired
	protected NamedParameterJdbcTemplate jdbcTemplate;

	@Override
	public List<File> getByUserId(long userId) {
		FileCallbackHandler fileCallbackHandler = appContext.getBean(FileCallbackHandler.class);
		SqlParameterSource namedParameters = new MapSqlParameterSource("userId", userId);
		jdbcTemplate.query(GET_BY_USER_ID, namedParameters, fileCallbackHandler);
		return fileCallbackHandler.getResult();
	}

	@Override
	public void insert(File file) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("name", file.getName())
						 .addValue("fileTypeId", file.getFileType().getId())
						 .addValue("userId", file.getUserId())
						 .addValue("actionSourceId", file.getActionSource().getId())
						 .addValue("writerId", file.getWriter().getId())
						 .addValue("isPrimary", file.isPrimary() ? 1 : 0);
		jdbcTemplate.update(INSERT, namedParameters);		
	}
	
	@Override
	public File getByIdAndUserId(File file) {
		FileCallbackHandler fileCallbackHandler = appContext.getBean(FileCallbackHandler.class);
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("userId", file.getUserId())
				.addValue("id", file.getId());
		jdbcTemplate.query(GET_BY_ID_AND_USER_ID, namedParameters, fileCallbackHandler);
		return fileCallbackHandler.getResult().get(0);
	}

	@Override
	public boolean updateFile(File file) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("id", file.getId())
				.addValue("fileTypeId", file.getFileType().getId())
				.addValue("fileStatusId", file.getFileStatus().getId())
				.addValue("isPrimary", file.isPrimary() ? 1 : 0);;
		return (jdbcTemplate.update(UPDATE_FILE, namedParameters) > 0);
	}

	@Override
	public boolean updateIsPrimary(File file) {
		SqlParameterSource namedParameters =
				new MapSqlParameterSource("fileTypeId", file.getFileType().getId())
				.addValue("userId", file.getUserId());
		return (jdbcTemplate.update(UPDATE_IS_PRIMARY_FALSE_BY_TYPE_AND_USER, namedParameters) > 0);
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.appContext = applicationContext;
	}
	
}
