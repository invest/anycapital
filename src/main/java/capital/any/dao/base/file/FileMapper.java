package capital.any.dao.base.file;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.base.enums.ActionSource;
import capital.any.dao.base.MapperBase;
import capital.any.dao.base.writer.BaseWriterFactory;
import capital.any.model.base.File;
import capital.any.model.base.FileType;

/**
 * @author Eyal G
 *
 */
@Component
public class FileMapper extends MapperBase implements RowMapper<File> {
	
	@Autowired
	private BaseWriterFactory writerFactory;
	
	@Override
	public File mapRow(ResultSet rs, int row) throws SQLException {		
		File file = new File();
		file.setId(rs.getInt("id"));
		file.setName(rs.getString("name"));
		file.setActionSource(ActionSource.get(rs.getInt("action_source_id")));
		file.setTimeCreated(convertTimestampToDate(rs.getTimestamp("time_created")));
		FileType fileType = new FileType();
		fileType.setId(rs.getInt("file_type_id"));
		file.setFileType(fileType);
		file.setUserId(rs.getLong("user_id"));
		file.setWriter(writerFactory.createWriter(rs.getInt("writer_id")));
		file.setPrimary(rs.getInt("is_primary") == 1 ? true : false);
		return file;
	}
	
	public File mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
			
}
	

