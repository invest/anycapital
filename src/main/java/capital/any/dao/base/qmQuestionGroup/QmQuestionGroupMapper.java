package capital.any.dao.base.qmQuestionGroup;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.QmQuestionGroup;

/**
 * 
 * @author Eyal.o
 *
 */
@Component
public class QmQuestionGroupMapper extends MapperBase implements RowMapper<QmQuestionGroup> {

	@Override
	public QmQuestionGroup mapRow(ResultSet rs, int rowNum) throws SQLException {
		QmQuestionGroup qmQuestionGroup = new QmQuestionGroup();
		qmQuestionGroup.setId(rs.getInt("qg_id"));
		qmQuestionGroup.setName(rs.getString("qg_name"));
		qmQuestionGroup.setDisplayName(rs.getString("qg_display_name"));
		return qmQuestionGroup;
	}
	
	public QmQuestionGroup mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}

}
