package capital.any.dao.base.actionSource;

import java.util.Map;

import capital.any.model.base.ActionSource;

/**
 * 
 * @author Eyal.o
 *
 */
public interface IActionSourceDao {
	
	public static final String GET = 
			" SELECT " +
			" 	* " +
			" FROM " +
			" 	action_source ";
	
	Map<Integer, ActionSource> get();
	
}
