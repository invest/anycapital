package capital.any.dao.base.actionSource;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import capital.any.model.base.ActionSource;

/**
 * 
 * @author Eyal.o
 *
 */
@Repository
public class ActionSourceDao implements IActionSourceDao {

	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private ActionSourceMapper actionSourceMapper;
	
	@Override
	public Map<Integer, ActionSource> get() {
		List<ActionSource> tmp = jdbcTemplate.query(GET, actionSourceMapper);
		Map<Integer, ActionSource> hm = tmp.stream().collect(Collectors.toMap(p-> p.getId(), p -> p));
		return hm;
	}

}
