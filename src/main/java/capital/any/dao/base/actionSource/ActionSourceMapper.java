package capital.any.dao.base.actionSource;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.base.ActionSource;

/**
 * 
 * @author Eyal.o
 *
 */
@Component
public class ActionSourceMapper implements RowMapper<ActionSource> {

	@Override
	public ActionSource mapRow(ResultSet rs, int rowNum) throws SQLException {
		ActionSource actionSource = new ActionSource();
		actionSource.setId(rs.getInt("id"));
		actionSource.setName(rs.getString("name"));
		actionSource.setDisplayName(rs.getString("display_name"));
		return actionSource;
	}
	
	public ActionSource mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs);
	}

}
