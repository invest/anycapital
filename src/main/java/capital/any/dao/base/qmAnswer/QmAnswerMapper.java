package capital.any.dao.base.qmAnswer;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.QmAnswer;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
public class QmAnswerMapper extends MapperBase implements RowMapper<QmAnswer> {

	@Override
	public QmAnswer mapRow(ResultSet rs, int rowNum) throws SQLException {
		QmAnswer qmAnswer = new QmAnswer();
		qmAnswer.setId(rs.getInt("id"));
		qmAnswer.setName(rs.getString("name"));
		qmAnswer.setDisplayName(rs.getString("display_name"));
		qmAnswer.setQuestionId(rs.getInt("question_id"));
		qmAnswer.setOrderId(rs.getInt("order_id"));
		return qmAnswer; 
	}
	
	public QmAnswer mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}

}
