package capital.any.dao.base.investmentStatus;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.InvestmentStatus;

/**
 * @author Eyal Goren
 *
 */
@Repository("InvestmentStatusHCDao")
public class InvestmentStatusHCDao implements IInvestmentStatusDao {
	private static final Logger logger = LoggerFactory.getLogger(InvestmentStatusHCDao.class);

	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, InvestmentStatus> get() {
		Map<Integer, InvestmentStatus> investmentStatuses = null;		
		try {
			investmentStatuses = (Map<Integer, InvestmentStatus>) HazelCastClientFactory.getClient().getMap(Constants.MAP_INVESTMENT_STATUSES);
		} catch (Exception e) {
			logger.error("ERROR!", e);
		}
		return investmentStatuses;
	}
}
