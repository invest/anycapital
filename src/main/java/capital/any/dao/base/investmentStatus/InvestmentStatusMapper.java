package capital.any.dao.base.investmentStatus;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.base.InvestmentStatus;

/**
 * @author Eyal Goren
 *
 */
@Component
public class InvestmentStatusMapper implements RowMapper<InvestmentStatus> {
	
	@Override
	public InvestmentStatus mapRow(ResultSet rs, int row) throws SQLException {
		InvestmentStatus investmentStatus = new InvestmentStatus();
		investmentStatus.setId(rs.getInt("id"));
		investmentStatus.setName(rs.getString("name"));
		investmentStatus.setDisplayName(rs.getString("display_name"));
		return investmentStatus;
	}
	
	public InvestmentStatus mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
			
}
	

