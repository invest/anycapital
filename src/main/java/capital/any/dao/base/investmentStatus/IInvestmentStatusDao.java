package capital.any.dao.base.investmentStatus;

import java.util.Map;

import capital.any.model.base.InvestmentStatus;

/**
 * @author Eyal Goren
 *
 */
public interface IInvestmentStatusDao {
	
	public static final String GET_INVESTMENT_STATUSES_LIST = 
			" SELECT " +
			"	* " + 
			" FROM "	+ 
			"	investment_statuses ";
		
	Map<Integer, InvestmentStatus> get();

}
