package capital.any.dao.base.investmentStatus;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import capital.any.model.base.InvestmentStatus;

/**
 * @author Eyal Goren
 *
 */
@Primary
@Repository
public class InvestmentStatusDao implements IInvestmentStatusDao {
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private InvestmentStatusMapper investmentStatusMapper;

	@Override
	public Map<Integer, InvestmentStatus> get() {
		List<InvestmentStatus> tmp = jdbcTemplate.query(GET_INVESTMENT_STATUSES_LIST, investmentStatusMapper);
		Map<Integer, InvestmentStatus> hm = tmp.stream().collect(Collectors.toMap(p-> p.getId(), p -> p));		 		 
		return hm;
	}
}
