package capital.any.dao.base.productCouponFrequency;

import java.util.Map;
import capital.any.model.base.ProductCouponFrequency;

/**
 * @author Eyal G
 *
 */
public interface IProductCouponFrequencyDao {
	
	public static final String GET_PRODUCT_PRODUCT_COUPON_FREQUENCIES_LIST = 
			" SELECT " +
			"	* " + 
			" FROM "	+ 
			"	product_coupon_frequencies ";
		
	Map<Integer, ProductCouponFrequency> get();

}
