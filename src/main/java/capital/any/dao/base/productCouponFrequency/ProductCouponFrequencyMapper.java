package capital.any.dao.base.productCouponFrequency;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.base.ProductCouponFrequency;

/**
 * @author Eyal G
 *
 */
@Component
public class ProductCouponFrequencyMapper implements RowMapper<ProductCouponFrequency> {
	
	@Override
	public ProductCouponFrequency mapRow(ResultSet rs, int row) throws SQLException {
		ProductCouponFrequency productCouponFrequency = new ProductCouponFrequency();
		productCouponFrequency.setId(rs.getInt("id"));
		productCouponFrequency.setName(rs.getString("name"));
		productCouponFrequency.setDisplayName(rs.getString("display_name"));
		return productCouponFrequency;
	}
	
	public ProductCouponFrequency mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
			
}
	

