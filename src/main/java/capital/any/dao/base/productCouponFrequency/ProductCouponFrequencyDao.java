package capital.any.dao.base.productCouponFrequency;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import capital.any.model.base.ProductCouponFrequency;

/**
 * @author Eyal G
 *
 */
@Primary
@Repository
public class ProductCouponFrequencyDao implements IProductCouponFrequencyDao {
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private ProductCouponFrequencyMapper productCouponFrequencyMapper;

	@Override
	public Map<Integer, ProductCouponFrequency> get() {
		List<ProductCouponFrequency> tmp = jdbcTemplate.query(GET_PRODUCT_PRODUCT_COUPON_FREQUENCIES_LIST, productCouponFrequencyMapper);
		Map<Integer, ProductCouponFrequency> hm = tmp.stream().collect(Collectors.toMap(p-> p.getId(), p -> p));		 		 
		return hm;
	}
}
