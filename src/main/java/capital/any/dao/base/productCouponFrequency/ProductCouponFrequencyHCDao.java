package capital.any.dao.base.productCouponFrequency;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import capital.any.dao.base.currency.CurrencyHCDao;
import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.ProductCouponFrequency;

/**
 * @author LioR SoLoMoN
 *
 */
@Repository("ProductCouponFrequencyHCDao")
public class ProductCouponFrequencyHCDao implements IProductCouponFrequencyDao {
	private static final Logger logger = LoggerFactory.getLogger(CurrencyHCDao.class);
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, ProductCouponFrequency> get() {		
		Map<Integer, ProductCouponFrequency> productCouponFrequencies = null;		
		try {
			productCouponFrequencies = (Map<Integer, ProductCouponFrequency>) HazelCastClientFactory.getClient().getMap(Constants.MAP_PRODUCT_COUPON_FREQUENCIES);
		} catch (Exception e) {
			logger.error("ERROR!", e);
		}
		return productCouponFrequencies;
	}
}
