package capital.any.dao.base.investmentReject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.base.InvestmentReject;

/**
 * @author EyalG
 *
 */
@Repository
public class InvestmentRejectDao implements IInvestmentRejectDao {
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;

	
	@Override
	public void insert(InvestmentReject investmentReject) {		
		SqlParameterSource parameters = new MapSqlParameterSource
				("action_source_id", investmentReject.getActionSource().getId())
				.addValue("investment_type_id", investmentReject.getInvestmentType().getId())
				.addValue("server_name", investmentReject.getServerName())
				.addValue("rate", investmentReject.getRate())
				.addValue("investment_reject_type_id", investmentReject.getInvestmentRejectType().getId())
				.addValue("user_id", investmentReject.getUserId())
				.addValue("product_id", investmentReject.getProductId())
				.addValue("writer_id", investmentReject.getWriterId() == 0 ? null : investmentReject.getWriterId())
				.addValue("amount", investmentReject.getAmount())
				.addValue("reject_additional_info", investmentReject.getRejectAdditionalInfo())
				.addValue("ask", investmentReject.getAsk())
				.addValue("session_id", investmentReject.getSessionId())
				.addValue("bid", investmentReject.getBid());
		jdbcTemplate.update(INSERT_INVESTMENT_REJECT, parameters);
	}
	
}
