package capital.any.dao.base.marketingCampaign;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.MarketingCampaign;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
public class MarketingCampaignMapper extends MapperBase implements RowMapper<MarketingCampaign> {
	@Override
	public MarketingCampaign mapRow(ResultSet rs, int row) throws SQLException {
		MarketingCampaign marketingCampaign = new MarketingCampaign();
		marketingCampaign.setId(rs.getInt("id"));
		marketingCampaign.setName(rs.getString("name"));
		marketingCampaign.setSourceId(rs.getInt("source_id"));
		marketingCampaign.setDomainId(rs.getInt("domain_id"));
		marketingCampaign.setLandingPageId(rs.getInt("landing_page_id"));
		marketingCampaign.setContentId(rs.getInt("content_id"));
		marketingCampaign.setWriterId(rs.getInt("writer_id"));
		marketingCampaign.setTimeCreated(convertTimestampToDate(rs.getTimestamp("time_created")));
		marketingCampaign.setTimeModified(convertTimestampToDate(rs.getTimestamp("time_modified")));
		return marketingCampaign;
	}
	
	public MarketingCampaign mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
}
