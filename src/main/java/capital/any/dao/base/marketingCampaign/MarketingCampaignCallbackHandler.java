package capital.any.dao.base.marketingCampaign;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Component;

import capital.any.model.base.MarketingCampaign;
import capital.any.model.base.MarketingDomain;
import capital.any.model.base.MarketingLandingPage;
import capital.any.service.base.marketingCampaign.IMarketingCampaignService;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
@Scope("prototype")
public class MarketingCampaignCallbackHandler implements RowCallbackHandler {
	
	private Map<Long, MarketingCampaign> result = new HashMap<Long, MarketingCampaign>();
	
	@Autowired
	private MarketingCampaignMapper MarketingCampaignMapper;

	@Override
	public void processRow(ResultSet rs) throws SQLException {
		MarketingDomain marketingDomain = new MarketingDomain();
		marketingDomain.setDomain(rs.getString("domain"));
		MarketingLandingPage marketingLandingPage = new MarketingLandingPage();
		marketingLandingPage.setName(rs.getString("mlp_name"));
		MarketingCampaign marketingCampaign = MarketingCampaignMapper.mapRow(rs);
		marketingCampaign.setMarketingDomain(marketingDomain);
		marketingCampaign.setMarketingLandingPage(marketingLandingPage);
		marketingCampaign.setUrl(marketingDomain.getDomain() + IMarketingCampaignService.PATH
																+ IMarketingCampaignService.CAMPAIGN_ID + "=" + marketingCampaign.getId() 
																+ "&" + IMarketingCampaignService.PAGE_REDIRECT + "=" + marketingLandingPage.getName()
																+ "&" + IMarketingCampaignService.UTM_CAMPAIGN + "=" + marketingCampaign.getId()
																+ "&" + IMarketingCampaignService.UTM_CONTENT + "=" + rs.getString("content_name")
																+ "&" + IMarketingCampaignService.UTM_MEDIUM + "=" + marketingCampaign.getName()
																+ "&" + IMarketingCampaignService.UTM_SOURCE + "=" + rs.getString("source_name")																				
								);
		result.put(marketingCampaign.getId(), marketingCampaign);
	}

	public Map<Long, MarketingCampaign> getResult() {
		return result;
	}

	public void setResult(Map<Long, MarketingCampaign> result) {
		this.result = result;
	}
}
