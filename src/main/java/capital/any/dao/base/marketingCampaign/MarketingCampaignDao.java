package capital.any.dao.base.marketingCampaign;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.dao.base.marketingCampaign.MarketingCampaignCallbackHandler;
import capital.any.model.base.MarketingCampaign;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository
public class MarketingCampaignDao implements IMarketingCampaignDao, ApplicationContextAware {
	private static final Logger logger = LoggerFactory.getLogger(MarketingCampaignDao.class);
	
	private ApplicationContext appContext;
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private MarketingCampaignMapper marketingCampaignMapper;
	
	@Override
	public void insert(MarketingCampaign marketingCampaign) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("name", marketingCampaign.getName())
								.addValue("sourceId", marketingCampaign.getSourceId())
								.addValue("domainId", marketingCampaign.getDomainId())
								.addValue("landingPageId", marketingCampaign.getLandingPageId())
								.addValue("contentId", marketingCampaign.getContentId())
								.addValue("writerId", marketingCampaign.getWriterId());
		jdbcTemplate.update(INSERT_MARKETING_CAMPAIGN, namedParameters);
	}
	
	@Override
	public void update(MarketingCampaign marketingCampaign) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("id", marketingCampaign.getId())
								.addValue("name", marketingCampaign.getName())
								.addValue("sourceId", marketingCampaign.getSourceId())
								.addValue("domainId", marketingCampaign.getDomainId())
								.addValue("landingPageId", marketingCampaign.getLandingPageId())
								.addValue("contentId", marketingCampaign.getContentId())
								.addValue("writerId", marketingCampaign.getWriterId());
		jdbcTemplate.update(UPDATE_MARKETING_CAMPAIGN, namedParameters);
	}

	@Override
	public Map<Long, MarketingCampaign> getMarketingCampaigns() {
		MarketingCampaignCallbackHandler callbackHandler = appContext.getBean(MarketingCampaignCallbackHandler.class);
		jdbcTemplate.query(GET_MARKETING_CAMPAIGN_LIST, callbackHandler);	
		return callbackHandler.getResult();
	}

	@Override
	public MarketingCampaign getMarketingCampaign(long id) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("id", id);
		MarketingCampaign marketingCampaign = null;
		try {
			marketingCampaign = jdbcTemplate.queryForObject(GET_MARKETING_CAMPAIGN, namedParameters, marketingCampaignMapper);
		} catch (Exception e) {
			logger.error("can't find marketing campaign!", e.getMessage());
		}
		return marketingCampaign;
	}
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.appContext = applicationContext;
	}

}
