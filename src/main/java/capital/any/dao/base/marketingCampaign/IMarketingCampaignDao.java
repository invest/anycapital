package capital.any.dao.base.marketingCampaign;

import java.util.Map;

import capital.any.model.base.MarketingCampaign;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IMarketingCampaignDao {
	public static final String INSERT_MARKETING_CAMPAIGN = 
			"  INSERT " +
				    "  INTO marketing_campaigns " +
					"  ( " +
				    "     name, " +
				    " 	  source_id, " +
					" 	  domain_id, " +
					" 	  landing_page_id, " +
					" 	  content_id, " +
				    " 	  writer_id " +
				    "  ) " +
				    "  VALUES " +
				    "  ( " +
				    " 	  :name, " +
				    " 	  :sourceId, " +
					" 	  :domainId, " +
					" 	  :landingPageId, " +
					" 	  :contentId, " +				    
				    " 	  :writerId " +
				    "  ) ";
	
	public static final String UPDATE_MARKETING_CAMPAIGN =
			" UPDATE " +
				" marketing_campaigns mca " +
			" SET " +
				" mca.name = :name, " +
				" mca.source_id = :sourceId, " +
				" mca.domain_id = :domainId, " +
				" mca.landing_page_id = :landingPageId, " +
				" mca.content_id = :contentId, " +
				" mca.writer_id = :writerId, " +
				" mca.time_modified = now() " +
			" WHERE " +
				" mca.id = :id ";
	
	public static final String GET_MARKETING_CAMPAIGN_LIST = 
			" SELECT " +
				" mc.*, " +
				" md.domain, " +
				" mlp.name as mlp_name, " +
                " mco.name content_name, " +
                " ms.name source_name " +				
			" FROM "	+ 
				" marketing_campaigns mc, " +
				" marketing_domains md, " +
				" marketing_landing_pages mlp, " +
				" marketing_contents mco, " +
                " marketing_sources ms " +
			" WHERE " +
		        " mc.domain_id = md.id " +
		        " AND mc.landing_page_id = mlp.id " +
		        " AND mc.content_id = mco.id " +
		        " AND mc.source_id = ms.id " +
			" ORDER BY " +
				" mc.id DESC ";
	
	public static final String GET_MARKETING_CAMPAIGN = 
			" SELECT " +
				" * " + 
			" FROM " + 
				" marketing_campaigns mc " +
			" WHERE " +
				" mc.id = :id ";
	
	/**
	 * Insert marketing campaign
	 * @param marketingCampaign
	 */
	void insert(MarketingCampaign marketingCampaign);
	
	/**
	 * Update marketing campaign
	 * @param marketingCampaign
	 */
	void update(MarketingCampaign marketingCampaign);
	
	/**
	 * Get marketing campaigns
	 * @return Map<Integer, MarketingCampaign>
	 */
	Map<Long, MarketingCampaign> getMarketingCampaigns();
	
	/**
	 * Get marketing campaign
	 * @param id
	 * @return MarketingCampaign
	 */
	MarketingCampaign getMarketingCampaign(long id);
}
