package capital.any.dao.base.job;

import java.util.HashMap;

import capital.any.model.base.Job;

/**
 * @author eranl
 *
 */
public interface IJobDao {
	
	public static final String GET_JOB_LIST = 
												" SELECT " +
												"	* " + 
												" FROM "	+ 
												"	jobs ";
	
	public static String UPDATE_JOB_BY_ID = 
												" UPDATE " +
												"	jobs " +
												" SET " +												
											//	"	running = :running, " +
												"	last_run_time = :lastRunTime " +
												" WHERE " +
												" 	id = :id ";

	HashMap<Integer, Job> getJobs();
	
	void updateJobById(int id);

}
