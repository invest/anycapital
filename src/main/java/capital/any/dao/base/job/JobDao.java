package capital.any.dao.base.job;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.base.Job;

/**
 * @author eranl
 *
 */
@Repository
public class JobDao implements IJobDao {
	
	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Override
	public HashMap<Integer, Job> getJobs() {
		List<Job> tmp = namedParameterJdbcTemplate.query(GET_JOB_LIST, new BeanPropertyRowMapper<Job>(Job.class));
		Map<Integer, Job> hm = tmp.stream().collect(Collectors.toMap(p-> p.getId(), p -> p));	
		return (HashMap<Integer, Job>) hm;		
	}

	@Override
	public void updateJobById(int id) {
		SqlParameterSource namedParameters = new MapSqlParameterSource("id", id)
															 .addValue("lastRunTime", new Date());				
		namedParameterJdbcTemplate.update(UPDATE_JOB_BY_ID, namedParameters);	
	}
	


}
