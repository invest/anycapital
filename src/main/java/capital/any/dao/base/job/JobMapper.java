package capital.any.dao.base.job;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.Job;

/**
 * @author Eran
 *
 */
@Component
public class JobMapper extends MapperBase implements RowMapper<Job> {

	@Override
	public Job mapRow(ResultSet rs, int row) throws SQLException {
		Job job = new Job();
		job.setId(rs.getInt("id"));
		job.setTimeCreated(rs.getDate("time_created"));
		job.setRunning(rs.getInt("running") == 0 ? false : true);
		job.setLastRunTime(rs.getDate("last_run_time"));
		job.setCronExpression(rs.getString("cron_expression"));
		job.setDescription(rs.getString("description"));
		job.setJobClass(rs.getString("job_class"));
		job.setConfig(rs.getString("config"));
		job.setActive(rs.getInt("is_active") == 0 ? false : true);
		return job;
	}
				
}
