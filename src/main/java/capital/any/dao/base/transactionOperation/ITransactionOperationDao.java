package capital.any.dao.base.transactionOperation;

import java.util.Map;
import capital.any.model.base.TransactionOperation;

/**
 * @author LioR SoLoMoN
 *
 */
public interface ITransactionOperationDao {
	
	public static final String GET_TRANSACTION_OPERATION = 
			"SELECT " +
			"	id to_id " + 
			"	,name to_name " +
			"	,display_name to_display_name " +
			"FROM "	+ 
			"	transaction_operations ";
		
	Map<Integer, TransactionOperation> get();

}
