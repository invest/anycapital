package capital.any.dao.base.transactionOperation;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import capital.any.model.base.TransactionOperation;

/**
 * @author LioR SoLoMoN
 *
 */
@Component
public class TransactionOperationMapper implements RowMapper<TransactionOperation> {	
	@Override
	public TransactionOperation mapRow(ResultSet rs, int row) throws SQLException {
		TransactionOperation transactionOperation = new TransactionOperation();		
		transactionOperation.setId(rs.getInt("to_id"));
		transactionOperation.setName(rs.getString("to_name"));
		transactionOperation.setDisplayName(rs.getString("to_display_name"));
		return transactionOperation;
	}
	
	public TransactionOperation mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}			
}
	

