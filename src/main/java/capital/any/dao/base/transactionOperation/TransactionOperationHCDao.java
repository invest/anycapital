package capital.any.dao.base.transactionOperation;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.TransactionOperation;

/**
 * @author LioR SoLoMoN
 *
 */
@Repository("TransactionOperationHCDao")
public class TransactionOperationHCDao implements ITransactionOperationDao {
	private static final Logger logger = LoggerFactory.getLogger(TransactionOperationHCDao.class);
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, TransactionOperation> get() {
		Map<Integer, TransactionOperation> transactionOperation = null;		
		try {
			transactionOperation = (Map<Integer, TransactionOperation>) HazelCastClientFactory.getClient().getMap(Constants.MAP_TRANSACTION_OPERATION);
		} catch (Exception e) {
			logger.error("ERROR!", e);
		}
		return transactionOperation;	
	}
}
