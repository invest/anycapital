package capital.any.dao.base.investment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import capital.any.base.enums.InvestmentStatusEnum;
import capital.any.model.base.Investment;
import capital.any.model.base.MyNote;
import capital.any.model.base.SellNowRequest;
import capital.any.model.base.SqlFilters;

/**
 * @author EyalG
 *
 */
@Repository
public class InvestmentDao implements IInvestmentDao {
	
	@Autowired
	protected NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	protected InvestmentMapper investmentsMapper;
	
	@Override
	public void insert(Investment investment) {		
		KeyHolder keyHolder = new GeneratedKeyHolder();
		SqlParameterSource parameters = new MapSqlParameterSource
				 ("investmentTypeId", investment.getInvestmentType().getId())
		.addValue("amount", investment.getAmount())
		.addValue("originalAmount", investment.getOriginalAmount())
		.addValue("investmentStatusId", investment.getInvestmentStatus().getId())
		.addValue("rate", investment.getRate())
		.addValue("userId", investment.getUser().getId())
		.addValue("productId", investment.getProductId())
		.addValue("ask", investment.getAsk())
		.addValue("bid", investment.getBid());
		jdbcTemplate.update(INSERT_INVESTMENT, parameters, keyHolder, new String[] {"id"});
		investment.setId(keyHolder.getKey().longValue());
	}

	@Override
	public List<Investment> get(SqlFilters filters) {
		SqlParameterSource parameters = new MapSqlParameterSource
				 ("userClassId", filters.getUserClassId())
				 .addValue("from", filters.getFrom())
				 .addValue("to", filters.getTo())
				 .addValue("userClassId", filters.getUserClassId());
		return jdbcTemplate.query(GET_INVESTMENTS, parameters, new RowMapper<Investment>() {

			@Override
			public Investment mapRow(ResultSet rs, int rowNum) throws SQLException {
				Investment investment = investmentsMapper.mapRow(rs, rowNum);
				investment.getInvestmentStatus().setDisplayName(rs.getString("is_display_name"));
				investment.getInvestmentType().setDisplayName(rs.getString("it_display_name"));
				return investment;
			}
			
		});
	}
	
	@Override
	public List<Investment> getByUserId(SqlFilters filters) {
		SqlParameterSource parameters = new MapSqlParameterSource
				 ("userId", filters.getUserId())
				 .addValue("from", filters.getFrom())
				 .addValue("to", filters.getTo());
		return jdbcTemplate.query(GET_INVESTMENTS_BY_USER_ID, parameters, new RowMapper<Investment>() {

			@Override
			public Investment mapRow(ResultSet rs, int rowNum) throws SQLException {
				Investment investment = investmentsMapper.mapRow(rs, rowNum);
				investment.getInvestmentStatus().setDisplayName(rs.getString("is_display_name"));
				investment.getInvestmentType().setDisplayName(rs.getString("it_display_name"));
				return investment;
			}
			
		});
	}
	
	@Override
	public List<MyNote> getByUserId(long userId, boolean isInvOpen, List<Integer> statuses) {
		List<Integer> statusIds = null;
		if (statuses == null) {
			statusIds = new ArrayList<Integer>();
			if (!isInvOpen) {
				statusIds.add(InvestmentStatusEnum.SETTLED.getId());
				statusIds.add(InvestmentStatusEnum.CANCELED_INSUFFICIENT_FUNDS.getId());
			} else {
				statusIds.add(InvestmentStatusEnum.PENDING.getId());
				statusIds.add(InvestmentStatusEnum.OPEN.getId());
			}
		} else {
			statusIds = statuses;
		}
		SqlParameterSource parameters = new MapSqlParameterSource
				 ("userId", userId)
				 .addValue("statusIds", statusIds);
		return jdbcTemplate.query(GET_INVESTMENTS_BY_USER_ID_OPEN_OR_CLOSE, parameters, new RowMapper<MyNote>() {
			@Override
			public MyNote mapRow(ResultSet rs, int rowNum) throws SQLException {
				Investment investment = investmentsMapper.mapRow(rs, rowNum);
				MyNote myNote = new MyNote(investment, rs.getLong("tran_amount"), rs.getLong("tran_coupon_amonut")); 
				return myNote;
			}
		});
	}

	@Override
	public boolean isExistPendingInvestment(Investment investment) {
		SqlParameterSource parameters = new MapSqlParameterSource
				 ("userId", investment.getUser().getId())
				 .addValue("productId", investment.getProductId());
		boolean result = jdbcTemplate.query(IS_EXIST_PENDING_INVESTMENT, parameters, new ResultSetExtractor<Boolean>() {
	        @Override
	        public Boolean extractData(ResultSet rs) throws SQLException, DataAccessException {
	            boolean result = rs.next();
	            return result;
	        }
	    });
	    return result;
	}

	@Override
	public List<Investment> getByStatusesAndProductId(long productId, List<Integer> statuses) {
		SqlParameterSource parameters = new MapSqlParameterSource
				 ("productId", productId)
				 .addValue("statusIds", statuses);
		return jdbcTemplate.query(GET_INVESTMENTS_BY_STATUS_AND_PRODUCT_ID, parameters, new RowMapper<Investment>() {
			@Override
			public Investment mapRow(ResultSet rs, int rowNum) throws SQLException {
				Investment investment = investmentsMapper.mapRow(rs, rowNum);
				investment.getUser().setCurrencyId(rs.getInt("u_currency_id"));
				return investment;
			}
		});
	}

	@Override
	public boolean updateStatus(Investment investment) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("id", investment.getId()).
				addValue("statusId", investment.getInvestmentStatus().getId());
		return (jdbcTemplate.update(UPDATE_INVESTMENT_STATUS, namedParameters) > 0);		
	}

	@Override
	public boolean updateSettle(Investment investment) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("id", investment.getId()).
				addValue("statusId", investment.getInvestmentStatus().getId())
				.addValue("originalReturnAmount", investment.getOriginalReturnAmount())
				.addValue("returnAmount", investment.getReturnAmount());
		return (jdbcTemplate.update(UPDATE_INVESTMENT_SETTLE, namedParameters) > 0);
	}

	@Override
	public List<Investment> getInvestmentToSellNow(SellNowRequest sellNowRequest) {
		SqlParameterSource parameters = new MapSqlParameterSource
				 ("ids", sellNowRequest.getInvestmentsId())
				 .addValue("userId", sellNowRequest.getUser().getId())
				 .addValue("productId", sellNowRequest.getProduct().getId());
		return jdbcTemplate.query(GET_INVESTMENTS_TO_SELL_NOW, parameters, investmentsMapper);
	}

	@Override
	public List<Investment> getInvestmentsToBuy(long userId) {
		SqlParameterSource parameters = new MapSqlParameterSource()
				 .addValue("userId", userId);
		return jdbcTemplate.query(GET_PENDING_INVESTMENTS_TO_BUY, parameters, investmentsMapper);
	}

	@Override
	public Investment getInvestmentById(long id) {
		SqlParameterSource parameters = new MapSqlParameterSource()
				 .addValue("id", id);
		return jdbcTemplate.queryForObject(GET_INVESTMENT_BY_ID, parameters, investmentsMapper);
	}	
}
