package capital.any.dao.base.investment;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import capital.any.dao.base.MapperBase;
import capital.any.model.base.Investment;
import capital.any.model.base.InvestmentStatus;
import capital.any.model.base.InvestmentType;
import capital.any.model.base.User;

/**
 * @author Eyal G
 *
 */
@Component
public class InvestmentMapper extends MapperBase implements RowMapper<Investment> {

	@Override
	public Investment mapRow(ResultSet rs, int row) throws SQLException {
		Investment investment = new Investment();
		investment.setId(rs.getInt("id"));
		investment.setAmount(rs.getLong("amount"));
		investment.setAsk(rs.getDouble("ask"));
		investment.setBid(rs.getDouble("bid"));
		investment.setInvestmentStatus(new InvestmentStatus(rs.getInt("investment_status_id")));
		investment.setInvestmentType(new InvestmentType(rs.getInt("investment_type_id")));
		investment.setProductId(rs.getLong("product_id"));
		investment.setRate(rs.getDouble("rate"));
		investment.setReturnAmount(rs.getLong("return_amount"));
		investment.setTimeCreated(convertTimestampToDate(rs.getTimestamp("time_created")));
		investment.setOriginalAmount(rs.getLong("original_amount"));
		investment.setOriginalReturnAmount(rs.getLong("original_return_amount"));
		investment.setUser(new User());
		investment.getUser().setId(rs.getLong("user_id"));
		return investment;
	}
	
	public Investment mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
			
}
	

