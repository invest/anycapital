package capital.any.dao.base.investment;

import java.util.List;

import capital.any.base.enums.InvestmentStatusEnum;
import capital.any.base.enums.Table;
import capital.any.model.base.Investment;
import capital.any.model.base.MyNote;
import capital.any.model.base.SellNowRequest;
import capital.any.model.base.SqlFilters;

/**
 * @author EyalG
 *
 */
public interface IInvestmentDao {
	
	public static final String INSERT_INVESTMENT = 
			" INSERT " +
			" INTO INVESTMENTS " +
			" ( " +
			"     investment_type_id , " +
			"     amount , " +
			"     original_amount , " +
			"     investment_status_id , " +
			"     rate , " +
			"     user_id , " +
			"     ask , " +
			"     product_id , " +
			"     bid " +
			"   ) " +
			"   VALUES " +
			"   ( " +
			"     :investmentTypeId , " +
			"     :amount , " +
			"     :originalAmount , " +
			"     :investmentStatusId , " +
			"     :rate , " +
			"     :userId , " +
			"     :ask , " +
			"     :productId , " +
			"     :bid " +
			"  ) ";
	
	public static final String GET_INVESTMENTS =
			"SELECT " +
				"iss.display_name is_display_name, " +
				"it.display_name it_display_name, " +
				"i.* " +
			"FROM " +
				"investments i, " +
				"investment_types it, " +
				"investment_statuses iss, " +
				"users u " +
			"WHERE " +
				"it.id = i.investment_type_id " +
				"AND iss.id = i.investment_status_id " +
				"AND u.id = i.user_id " +
				"AND 1 = IF(ISNULL(:userClassId), 1, IF(u.class_id = :userClassId, 1, 0)) " +
				"AND 1 = IF(ISNULL(:from), 1, IF(i.time_created >= :from, 1, 0)) " +
				"AND 1 = IF(ISNULL(:to), 1, IF(i.time_created <= :to, 1, 0)) " + 
				"AND 1 = IF(ISNULL(:userClassId), 1, IF(:userClassId = u.class_id, 1, 0)) ";
	
	public static final String GET_INVESTMENTS_BY_USER_ID =
			"SELECT " +
				"iss.display_name is_display_name, " +
				"it.display_name it_display_name, " +
				"i.* " +
			"FROM " +
				"investments i, " +
				"investment_types it, " +
				"investment_statuses iss " +
			"WHERE " +
				"i.user_id = :userId " +
				"AND 1 = IF(ISNULL(:from), 1, IF(i.time_created >= :from, 1, 0)) " +
				"AND 1 = IF(ISNULL(:to), 1, IF(i.time_created <= :to, 1, 0)) " +
				"AND it.id = i.investment_type_id " +
				"AND iss.id = i.investment_status_id ";
	
	
	public static final String IS_EXIST_PENDING_INVESTMENT =
			"SELECT " + 
			"	1 " +
			"FROM " +
				"investments i " +
			"WHERE " +
				"i.user_id = :userId " +
				"AND i.product_id = :productId " +
				"AND i.investment_status_id = 1";
	
	
	public static final String GET_INVESTMENTS_BY_USER_ID_OPEN_OR_CLOSE =
			" SELECT " +
				" t.amount tran_amount, " +
				" tc.amount tran_coupon_amonut, " +
				" i.* " +
			" FROM " +
				" investments i " +
				" LEFT JOIN transaction_references tr ON tr.reference_id = i.id and tr.table_id = " + Table.INVESTMENTS.getId() + 
				" LEFT JOIN transactions t ON t.id = tr.transaction_id " +
				" LEFT JOIN transaction_coupons tc ON tc.transaction_id = t.id " +
			" WHERE " +
				" i.user_id = :userId " +
				" AND i.investment_status_id in (:statusIds) " +
			" ORDER BY " +
				" i.time_created desc ";
	
	public static final String GET_INVESTMENTS_BY_STATUS_AND_PRODUCT_ID =
			"SELECT " +
				" i.*, " +
				" u.currency_id u_currency_id " +
			"FROM " +
				" investments i, " +
				" users u " +
			"WHERE " +
				" u.id = i.user_id " +
				" AND i.product_id = :productId " +
				" AND i.investment_status_id in (:statusIds) ";
	
	public static final String UPDATE_INVESTMENT_STATUS =
			"UPDATE "
				+ "investments "
		  + "SET "
		  		+ "investment_status_id = :statusId "
		  + "WHERE "
		  		+ "id = :id ";
	
	public static final String UPDATE_INVESTMENT_SETTLE =
			"UPDATE "
				+ "investments "
		  + "SET "
		  		+ "investment_status_id = :statusId, "
		  		+ "original_return_amount = :originalReturnAmount, "
		  		+ "return_amount = :returnAmount "
		  + "WHERE "
		  		+ "id = :id ";
	
	public static final String GET_INVESTMENTS_TO_SELL_NOW =
			"SELECT " +
				" * " +
			"FROM " +
				"investments i " +
			"WHERE " +
				"i.id in (:ids) " +
				"AND i.user_id = :userId " +
				"AND i.product_id = :productId " +
				"AND i.investment_status_id = " + InvestmentStatusEnum.OPEN.getId();
	
	public static final String GET_PENDING_INVESTMENTS_TO_BUY =
			"SELECT " +
				" i.* " +
			"FROM " +
				" investments i, " +
				" users u " +
			"WHERE " +
				" u.id = :userId " +
				" AND u.id = i.user_id " +
				" AND i.investment_status_id = " + InvestmentStatusEnum.PENDING.getId() +
				" AND u.balance >= i.amount " +
			"ORDER BY " +
				"i.time_created asc";
	
	public static final String GET_INVESTMENT_BY_ID =
			" SELECT " +
			" 	* " +
			" FROM " +
			" 	investments i " +
			" WHERE " +
			" 	i.id = :id ";
	
	/**
	 * insert investment
	 * @param investment
	 */
	void insert(Investment investment);
	
	/**
	 * select all investments
	 * @return List<Investment>
	 */
	List<Investment> get(SqlFilters filters);
	
	/**
	 * select all investments for user
	 * @param filters {@link SqlFilters}
	 * @return List<Investment>
	 */
	List<Investment> getByUserId(SqlFilters filters);
	
	/**
	 * check if user already have pending investment for this product
	 * @param investment include user id and product id
	 * @return true if the user have investment in pending for this product else false
	 */
	boolean isExistPendingInvestment(Investment investment);
	
	/**
	 * get all users open or close investments with transaction for coupon
	 * @param userId
	 * @param isInvOpen true open investments false close investments
	 * @return
	 */
	List<MyNote> getByUserId(long userId, boolean isInvOpen, List<Integer> statuses);
	
	/**
	 * get all investments by statuses and product id
	 * @param productId
	 * @param statuses
	 * @return List<Investment>
	 */
	List<Investment> getByStatusesAndProductId(long productId, List<Integer> statuses);
	
	/**
	 * update the investment status
	 * @param investment
	 * @return true if update success else false
	 */
	boolean updateStatus(Investment investment);
	
	/**
	 * update investment to settle
	 * @param investment
	 * @return true if update success else false
	 */
	boolean updateSettle(Investment investment);
	
	/**
	 * get investment to sell now
	 * @param sellNowRequest
	 * @return investment to sell now
	 */
	List<Investment> getInvestmentToSellNow(SellNowRequest sellNowRequest);
	
	/**
	 * get pending investments to buy after balance increase
	 * @param userId
	 * @return investment to buy
	 */
	List<Investment> getInvestmentsToBuy(long userId);
	
	/**
	 * Get investment by id
	 * @param investment
	 * @return
	 */
	Investment getInvestmentById(long id);
}
