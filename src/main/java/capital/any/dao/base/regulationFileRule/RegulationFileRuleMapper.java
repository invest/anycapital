package capital.any.dao.base.regulationFileRule;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.base.RegulationFileRule;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
public class RegulationFileRuleMapper implements RowMapper<RegulationFileRule> {

	@Override
	public RegulationFileRule mapRow(ResultSet rs, int rowNum) throws SQLException {
		RegulationFileRule regulationFileRule = new RegulationFileRule();
		regulationFileRule.setId(rs.getInt("rfr_id"));
		regulationFileRule.setCountryRiskId(rs.getInt("rfr_country_risk_id"));
		regulationFileRule.setGbgStatusId(rs.getInt("rfr_gbg_status_id"));
		regulationFileRule.setFileTypeId(rs.getInt("rfr_file_type_id"));
		return regulationFileRule;
	}

	public RegulationFileRule mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
}
