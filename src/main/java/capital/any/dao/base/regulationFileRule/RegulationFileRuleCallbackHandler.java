package capital.any.dao.base.regulationFileRule;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Component;

import capital.any.dao.base.fileType.FileTypeMapper;
import capital.any.model.base.FileType;
import capital.any.model.base.RegulationFileRule;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
@Scope("prototype")
public class RegulationFileRuleCallbackHandler implements RowCallbackHandler {
	private Map<Integer, RegulationFileRule> result = new HashMap<Integer, RegulationFileRule>();
	@Autowired
	private RegulationFileRuleMapper regulationFileRuleMapper;
	@Autowired
	private FileTypeMapper fileTypeMapper;
	
	@Override
	public void processRow(ResultSet rs) throws SQLException {
		FileType fileType = fileTypeMapper.mapRow(rs);
		RegulationFileRule regulationFileRule = regulationFileRuleMapper.mapRow(rs);
		regulationFileRule.setFileType(fileType);
		result.put(regulationFileRule.getId(), regulationFileRule);
	}

	/**
	 * @return the result
	 */
	public Map<Integer, RegulationFileRule> getResult() {
		return result;
	}

}
