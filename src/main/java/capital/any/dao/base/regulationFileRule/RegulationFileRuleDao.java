package capital.any.dao.base.regulationFileRule;

import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import capital.any.model.base.RegulationFileRule;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository
public class RegulationFileRuleDao implements IRegulationFileRuleDao, ApplicationContextAware {
	protected ApplicationContext appContext;
	@Autowired
	protected NamedParameterJdbcTemplate jdbcTemplate;

	@Override
	public Map<Integer, RegulationFileRule> getFilesByRule(RegulationFileRule regulationFileRule) {
		RegulationFileRuleCallbackHandler regulationFileRuleCallbackHandler = appContext.getBean(RegulationFileRuleCallbackHandler.class);
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("countryRiskId", regulationFileRule.getCountryRiskId());
		params.addValue("gbgStatusId", regulationFileRule.getGbgStatusId());
		jdbcTemplate.query(GET_FILES_BY_RULE, params, regulationFileRuleCallbackHandler);
		return regulationFileRuleCallbackHandler.getResult();
	}
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.appContext = applicationContext;
	}
}
