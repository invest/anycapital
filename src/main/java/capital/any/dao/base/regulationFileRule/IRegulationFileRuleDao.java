package capital.any.dao.base.regulationFileRule;

import java.util.Map;

import capital.any.model.base.RegulationFileRule;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IRegulationFileRuleDao {
	
	public static final String GET_FILES_BY_RULE = 
			" 	SELECT " +
			" 		rfr.id rfr_id, " +
			"		rfr.country_risk_id rfr_country_risk_id, " +
			"		rfr.gbg_status_id rfr_gbg_status_id, " +
			"		rfr.file_type_id rfr_file_type_id, " +
			"		ft.* " +
			"	FROM " +  
			"		regulation_file_rules rfr, " +
			"		file_types ft " +
			"	WHERE " +
			"		rfr.file_type_id = ft.id " +
			"	    and rfr.country_risk_id = :countryRiskId " +
			"		and rfr.gbg_status_id = :gbgStatusId ";

	/**
	 * Get files by rule
	 * @param regulationFileRule
	 * @return
	 */
	Map<Integer, RegulationFileRule> getFilesByRule(RegulationFileRule regulationFileRule);
}
