package capital.any.dao.base.productStatus;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.base.ProductStatus;

/**
 * @author Eran
 *
 */
@Component
public class ProductStatusMapper implements RowMapper<ProductStatus> {
	
	@Override
	public ProductStatus mapRow(ResultSet rs, int row) throws SQLException {
		ProductStatus productStatus = new ProductStatus();
		productStatus.setId(rs.getInt("id"));
		productStatus.setName(rs.getString("name"));
		productStatus.setDisplayName(rs.getString("display_name"));
		return productStatus;
	}
	
	public ProductStatus mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
			
}
	

