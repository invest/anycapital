package capital.any.dao.base.productStatus;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.ProductStatus;

/**
 * @author LioR SoLoMoN
 *
 */
@Repository("ProductStatusHCDao")
public class ProductStatusHCDao implements IProductStatusDao {
	private static final Logger logger = LoggerFactory.getLogger(ProductStatusHCDao.class);

	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, ProductStatus> get() {
		Map<Integer, ProductStatus> productStatuses = null;		
		try {
			productStatuses = (Map<Integer, ProductStatus>) HazelCastClientFactory.getClient().getMap(Constants.MAP_PRODUCT_STATUSES);
		} catch (Exception e) {
			logger.error("ERROR!", e);
		}
		return productStatuses;
	}
}
