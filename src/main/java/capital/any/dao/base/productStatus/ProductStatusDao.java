package capital.any.dao.base.productStatus;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import capital.any.model.base.ProductStatus;

/**
 * @author eranl
 *
 */
@Primary
@Repository
public class ProductStatusDao implements IProductStatusDao {
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private ProductStatusMapper productStatusMapper;

	@Override
	public Map<Integer, ProductStatus> get() {
		List<ProductStatus> tmp = jdbcTemplate.query(GET_PRODUCT_STATUSES_LIST, productStatusMapper);
		Map<Integer, ProductStatus> hm = tmp.stream().collect(Collectors.toMap(p-> p.getId(), p -> p));		 		 
		return hm;
	}
}
