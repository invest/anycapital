package capital.any.dao.base.productStatus;

import java.util.Map;
import capital.any.model.base.ProductStatus;

/**
 * @author eranl
 *
 */
public interface IProductStatusDao {
	
	public static final String GET_PRODUCT_STATUSES_LIST = 
			" SELECT " +
			"	* " + 
			" FROM "	+ 
			"	product_statuses ";
		
	Map<Integer, ProductStatus> get();

}
