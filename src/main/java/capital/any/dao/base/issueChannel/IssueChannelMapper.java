package capital.any.dao.base.issueChannel;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.IssueChannel;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
public class IssueChannelMapper extends MapperBase implements RowMapper<IssueChannel> {

	@Override
	public IssueChannel mapRow(ResultSet rs, int rowNum) throws SQLException {
		IssueChannel issueChannel = new IssueChannel();
		issueChannel.setId(rs.getInt("iss_c_id"));
		issueChannel.setName(rs.getString("iss_c_name"));
		return issueChannel;
	}
	
	public IssueChannel mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}

}
