package capital.any.dao.base.issueChannel;

import java.util.Map;

import capital.any.model.base.IssueChannel;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IIssueChannelDao {
	
	public static final String GET_ISSUE_CHANNEL_LIST =
			" SELECT " +
				" iss_c.id iss_c_id, " +
				" iss_c.name iss_c_name " + 
			" FROM "	+ 
				" issue_channels iss_c ";
	
	Map<Integer, IssueChannel> get();
	
}
