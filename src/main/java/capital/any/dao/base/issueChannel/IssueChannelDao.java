package capital.any.dao.base.issueChannel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import capital.any.model.base.IssueChannel;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository
public class IssueChannelDao implements IIssueChannelDao {

	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private IssueChannelMapper issueChannelMapper;
	
	@Override
	public Map<Integer, IssueChannel> get() {
		List<IssueChannel> tmp = jdbcTemplate.query(GET_ISSUE_CHANNEL_LIST, issueChannelMapper);
		Map<Integer, IssueChannel> hm = tmp.stream().collect(Collectors.toMap(p-> p.getId(), p -> p));		 		 
		return (HashMap<Integer, IssueChannel>) hm;
	}

}
