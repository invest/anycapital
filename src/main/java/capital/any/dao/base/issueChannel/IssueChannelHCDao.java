package capital.any.dao.base.issueChannel;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.IssueChannel;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository("IssueChannelHCDao")
public class IssueChannelHCDao implements IIssueChannelDao {
	private static final Logger logger = LoggerFactory.getLogger(IssueChannelHCDao.class);
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, IssueChannel> get() {
		Map<Integer, IssueChannel> issueChannels = null;
		try {
			issueChannels = (Map<Integer, IssueChannel>) HazelCastClientFactory.getClient().getMap(Constants.MAP_ISSUE_CHANNELS);
		} catch (Exception e) {
			logger.error("Problem to get issueChannels by HC.");
		}
		return issueChannels;
	}

}
