package capital.any.dao.base.productCallable;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.base.ProductCallable;

/**
 * @author eranl
 *
 */
@Repository
public class ProductCallableDao implements IProductCallableDao, ApplicationContextAware {
	
	private ApplicationContext appContext; 
	
	@Autowired
	protected NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private ProductCallableMapper productCallableMapper;
	
	@Override
	public HashMap<Long, List<ProductCallable>> getProductCallables() {
		ProductCallableCallbackHandler callbackHandler = appContext.getBean(ProductCallableCallbackHandler.class);
		jdbcTemplate.query(GET_PRODUCT_CALLABLES_LIST, callbackHandler);
		HashMap<Long, List<ProductCallable>> hm = callbackHandler.getResult();						 		 
		return hm;
	}

	@Override
	public List<ProductCallable> getProductCallables(long productId) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("productId", productId); 
		return jdbcTemplate.query(GET_PRODUCT_CALLABLES_LIST_BY_PRODUCT,
				namedParameters, productCallableMapper);
	}
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.appContext = applicationContext;		
	}
	
}
