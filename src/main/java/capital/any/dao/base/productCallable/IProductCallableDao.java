package capital.any.dao.base.productCallable;

import java.util.HashMap;
import java.util.List;

import capital.any.model.base.ProductCallable;

/**
 * @author eranl
 *
 */
public interface IProductCallableDao {
	
	public static final String GET_PRODUCT_CALLABLES_LIST = 
			" SELECT " +
			"	* " + 
			" FROM "	+ 
			"	product_callable ";
	
	public static final String GET_PRODUCT_CALLABLES_LIST_BY_PRODUCT = 
			" SELECT " +
			"	* " + 
			" FROM "	+ 
			"	product_callable pc " +
			" WHERE " +
			"	pc.product_id = :productId";
		
	HashMap<Long, List<ProductCallable>> getProductCallables();
	
	List<ProductCallable> getProductCallables(long productId);

}
