package capital.any.dao.base.productCallable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Component;

import capital.any.model.base.ProductCallable;

/**
 * @author eranl
 *
 */
@Component
@Scope("prototype")
public class ProductCallableCallbackHandler implements RowCallbackHandler {
	
	private HashMap<Long, List<ProductCallable>> result = new HashMap<Long, List<ProductCallable>>();
	@Autowired
	private ProductCallableMapper productCallableMapper;
	
	@Override
	public void processRow(ResultSet rs) throws SQLException {
		ProductCallable productCallable = productCallableMapper.mapRow(rs);		
		List<ProductCallable> productCallables = result.get(productCallable.getProductId());
		if (productCallables == null) {
			productCallables = new ArrayList<ProductCallable>();		
		}
		productCallables.add(productCallable);
		result.put(productCallable.getProductId(), productCallables);		
	}

	/**
	 * @return the result
	 */
	public HashMap<Long, List<ProductCallable>> getResult() {
		return result;
	}

	/**
	 * @param result the result to set
	 */
	public void setResult(HashMap<Long, List<ProductCallable>> result) {
		this.result = result;
	}
	
}
