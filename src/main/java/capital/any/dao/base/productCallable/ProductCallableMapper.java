package capital.any.dao.base.productCallable;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import capital.any.dao.base.MapperBase;
import capital.any.model.base.ProductCallable;

/**
 * @author Eran
 *
 */
@Component
public class ProductCallableMapper extends MapperBase implements RowMapper<ProductCallable> {
	
	@Override
	public ProductCallable mapRow(ResultSet rs, int row) throws SQLException {
		ProductCallable productCallable = new ProductCallable();
		productCallable.setId(rs.getInt("id"));
		productCallable.setProductId(rs.getInt("product_id"));
		productCallable.setExaminationDate(convertTimestampToDate(rs.getTimestamp(("examination_date"))));
		return productCallable;
	}
	
	public ProductCallable mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
			
}
	

