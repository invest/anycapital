package capital.any.dao.base.regulationQuestionnaireStatus;

import java.util.Map;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IRegulationQuestionnaireStatusDao {

	public static final String GET_SCORE_STATUS = 
			" 	SELECT " +
			" 		* " + 
			" 	FROM " + 
			"		regulation_questionnaire_status_score ";
	
	Map<Long, Integer> getScoreStatus();

}
