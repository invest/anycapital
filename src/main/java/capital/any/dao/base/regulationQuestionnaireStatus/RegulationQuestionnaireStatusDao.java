package capital.any.dao.base.regulationQuestionnaireStatus;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import capital.any.model.base.RegulationQuestionnaireStatusScore;

/**
 * 
 * @author eyal.ohana
 *
 */
@Primary
@Repository
public class RegulationQuestionnaireStatusDao implements IRegulationQuestionnaireStatusDao {
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	@Override
	public Map<Long, Integer> getScoreStatus() {
		List<RegulationQuestionnaireStatusScore> list = jdbcTemplate.query(GET_SCORE_STATUS, new RowMapper<RegulationQuestionnaireStatusScore>() {

			@Override
			public RegulationQuestionnaireStatusScore mapRow(ResultSet rs, int rowNum) throws SQLException {
				RegulationQuestionnaireStatusScore rqss = new RegulationQuestionnaireStatusScore();
				rqss.setScore(rs.getLong("score"));
				rqss.setStatusId(rs.getInt("status_id"));
				return rqss;
			}
		});
		Map<Long, Integer> collect = list.stream().collect(Collectors.toMap(rqss -> rqss.getScore(), rqss -> rqss.getStatusId()));
		return collect;
	}
}
