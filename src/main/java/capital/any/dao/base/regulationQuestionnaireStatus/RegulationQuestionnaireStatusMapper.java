package capital.any.dao.base.regulationQuestionnaireStatus;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.RegulationQuestionnaireStatus;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
public class RegulationQuestionnaireStatusMapper extends MapperBase implements RowMapper<RegulationQuestionnaireStatus> {

	@Override
	public RegulationQuestionnaireStatus mapRow(ResultSet rs, int rowNum) throws SQLException {
		RegulationQuestionnaireStatus regulationQuestionnaireStatus = new RegulationQuestionnaireStatus();
		regulationQuestionnaireStatus.setId(rs.getInt("regulation_qs_id"));
		regulationQuestionnaireStatus.setName(rs.getString("regulation_qs_name"));
		regulationQuestionnaireStatus.setDisplayName(rs.getString("regulation_qs_display_name"));
		return regulationQuestionnaireStatus;
	}
	
	public RegulationQuestionnaireStatus mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}

}
