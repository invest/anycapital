package capital.any.dao.base.regulationQuestionnaireStatus;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository("RegulationQuestionnaireStatusHCDao")
public class RegulationQuestionnaireStatusHCDao implements IRegulationQuestionnaireStatusDao {
	private static final Logger logger = LoggerFactory.getLogger(RegulationQuestionnaireStatusHCDao.class);

	@Override
	public Map<Long, Integer> getScoreStatus() {
		Map<Long, Integer> mapScoreStatus = null;
		try {
			mapScoreStatus = (Map<Long, Integer>) HazelCastClientFactory.getClient().getMap(Constants.MAP_SCORE_STATUS);
		} catch (Exception e) {
			logger.error("ERROR with getScoreStatus by HazelCast!", e);
		}
		return mapScoreStatus;
	}

}
