package capital.any.dao.base.emailAction;

import java.util.ArrayList;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.hazelcast.core.IMap;
import com.hazelcast.query.SqlPredicate;

import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.EmailAction;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository("EmailActionHCDao")
public class EmailActionHCDao implements IEmailActionDao {
	private static final Logger logger = LoggerFactory.getLogger(EmailActionHCDao.class);

	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, EmailAction> get() {
		Map<Integer, EmailAction> map = null;
		try {
			map = (Map<Integer, EmailAction>) HazelCastClientFactory.getClient().getMap(Constants.MAP_EMAIL_ACTION);
		} catch (Exception e) {
			logger.error("ERROR! get map of EmailAction by HC", e);
		}
		return map;
	}

	@SuppressWarnings("unchecked")
	@Override
	public EmailAction getEmailAction(int id) {
		EmailAction emailAction = null;
		try {
			SqlPredicate predicate = new SqlPredicate("id = " + id);
			ArrayList<EmailAction> map = new ArrayList<EmailAction>(((IMap<Integer, EmailAction>)HazelCastClientFactory.getClient().getMap(Constants.MAP_EMAIL_ACTION)).values(predicate));
			if (map != null && map.size() > 0) {
				emailAction = map.get(0);
			}
		} catch (Exception e) {
			logger.error("ERROR! get EmailAction by HC", e);
		}
		return emailAction;
	}

}
