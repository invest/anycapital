package capital.any.dao.base.emailAction;

import java.util.Map;

import capital.any.model.base.EmailAction;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IEmailActionDao {
	
	public static final String GET = 
			" SELECT " +
			" 	* " +
			" FROM " +
			" 	email_actions ea " +
			" WHERE " +
			"	ea.is_display = 1 ";
	
	public static final String GET_EMAIL_ACTION = 
			" SELECT " +
			" 	* " +
			" FROM " +
			" 	email_actions ea " +
			" WHERE " +
			"	ea.id = :id ";
	
	Map<Integer, EmailAction> get();

	EmailAction getEmailAction(int id);
}
