package capital.any.dao.base.emailAction;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.base.EmailAction;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository
public class EmailActionDao implements IEmailActionDao {
	private static final Logger logger = LoggerFactory.getLogger(EmailActionDao.class);

	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private EmailActionMapper emailActionMapper;
	
	@Override
	public Map<Integer, EmailAction> get() {
		List<EmailAction> tmp = jdbcTemplate.query(GET, emailActionMapper);
		Map<Integer, EmailAction> hm = tmp.stream().collect(Collectors.toMap(p-> p.getId(), p -> p));
		return hm;
	}
	
	@Override
	public EmailAction getEmailAction(int id) {
		SqlParameterSource namedParameters = new MapSqlParameterSource("id", id);
		EmailAction emailAction = null;
		emailAction = jdbcTemplate.queryForObject(GET_EMAIL_ACTION, namedParameters, emailActionMapper);
		return emailAction;
	}

}
