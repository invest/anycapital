package capital.any.dao.base.emailAction;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.base.EmailAction;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
public class EmailActionMapper implements RowMapper<EmailAction> {

	@Override
	public EmailAction mapRow(ResultSet rs, int rowNum) throws SQLException {
		EmailAction emailAction = new EmailAction();
		emailAction.setId(rs.getInt("id"));
		emailAction.setName(rs.getString("name"));
		emailAction.setDisplayName(rs.getString("display_name"));
		emailAction.setDisplay(rs.getInt("is_display") == 0 ? false : true);
		emailAction.setClassPath(rs.getString("class_path"));
		return emailAction;
	}

	public EmailAction mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
}
