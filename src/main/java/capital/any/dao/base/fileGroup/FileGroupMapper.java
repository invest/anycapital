package capital.any.dao.base.fileGroup;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.FileGroup;

@Component
public class FileGroupMapper extends MapperBase implements RowMapper<FileGroup> {

	@Override
	public FileGroup mapRow(ResultSet rs, int rowNum) throws SQLException {
		FileGroup fileGroup = new FileGroup();
		fileGroup.setId(rs.getInt("fg_id"));
		fileGroup.setName(rs.getString("fg_name"));
		fileGroup.setDisplayName(rs.getString("fg_display_name"));
		return fileGroup;
	}
	
	public FileGroup mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}

}
