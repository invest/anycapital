package capital.any.dao.base.emailGroups;

import java.util.Map;

import capital.any.model.base.EmailGroup;

/**
 * @author EyalG
 *
 */
public interface IEmailGroupsDao {
	
	public static final String SELECT_ALL =
			"SELECT " +
			"	* " +
			"FROM " + 
			"	email_groups";
	
	/**
	 * select all EmailGroup
	 * @return HashMap<String, EmailGroup>
	 */
	Map<Integer, EmailGroup> get();
}
