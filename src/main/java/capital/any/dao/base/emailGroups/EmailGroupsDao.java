package capital.any.dao.base.emailGroups;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import capital.any.model.base.EmailGroup;

/**
 * @author EyalG
 *
 */
@Repository
public class EmailGroupsDao implements IEmailGroupsDao {
	
	@Autowired
	protected NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	protected EmailGroupsMapper emailGroupsMapper;

	@Override
	public Map<Integer, EmailGroup> get() {
		List<EmailGroup> tmp = jdbcTemplate.query(SELECT_ALL, emailGroupsMapper);
		Map<Integer, EmailGroup> hm = tmp.stream().collect(Collectors.toMap(e-> e.getId(), e -> e));		 		 
		return hm;
	}	
}
