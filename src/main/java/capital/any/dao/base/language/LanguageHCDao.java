package capital.any.dao.base.language;

import java.util.ArrayList;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.hazelcast.core.IMap;
import com.hazelcast.query.SqlPredicate;

import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.Language;

/**
 * @author LioR SoLoMoN
 *
 */
@Repository("LanguageHCDao")
public class LanguageHCDao implements ILanguageDao {
	private static final Logger logger = LoggerFactory.getLogger(LanguageHCDao.class);
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, Language> get() {		
		Map<Integer, Language> languages = null;		
		try {
			languages = (Map<Integer, Language>)HazelCastClientFactory.getClient().getMap(Constants.MAP_LANGUAGES);
		} catch (Exception e) {
			logger.error("ERROR!", e);
		}
		return languages;
	}
	@Override
	public Language getByCode(String code) {
		logger.info("LanguageHCDao getByCode");
		Language language = null;		
		try {
			SqlPredicate predicate = new SqlPredicate("code = " + code);
			ArrayList<Language> languages =  new ArrayList<Language>(((IMap<Integer, Language>) 
					HazelCastClientFactory.getClient().getMap(Constants.MAP_LANGUAGES))
					.values(predicate));
			if(languages != null && languages.size() > 0) {
				language = languages.get(0);
			}
		
		} catch (Exception e) {
			logger.error("ERROR!get language by code HC", e);
		}
		return language;
	}
}
