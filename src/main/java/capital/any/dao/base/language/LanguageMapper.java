package capital.any.dao.base.language;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.base.Language;

/**
 * @author Eran
 *
 */
@Component
public class LanguageMapper implements RowMapper<Language> {

	@Override
	public Language mapRow(ResultSet rs, int row) throws SQLException {
		Language language = new Language();
		language.setId(rs.getInt("id"));
		language.setCode(rs.getString("code"));
		language.setDisplayName(rs.getString("display_name"));
		language.setDefaultCountryId(rs.getInt("default_country_id"));
		return language;
	}
				
}
