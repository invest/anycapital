package capital.any.dao.base.language;

import java.util.Map;
import capital.any.model.base.Language;

/**
 * @author eranl
 *
 */
public interface ILanguageDao {
	
	public static final String GET_LANGUAGES_LIST = 
													" SELECT " +
													"	l.* " + 
													" FROM "	+ 
													"	languages l " +
													" WHERE " +
													" 	l.is_active = 1 ";

	public static final String GET_LANGUAGE_BY_CODE = 
													" SELECT " +
													"	* " + 
													" FROM "	+ 
													"	languages l " + 
													" WHERE " +
													"	upper(l.code) = upper(:code)";
	
	Map<Integer, Language> get();
	
	Language getByCode(String code);

}
