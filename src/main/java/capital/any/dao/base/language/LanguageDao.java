package capital.any.dao.base.language;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import capital.any.model.base.Language;

/**
 * @author eranl
 *
 */
@Repository
public class LanguageDao implements ILanguageDao {
	
	private static final Logger logger = LoggerFactory.getLogger(LanguageDao.class);
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private LanguageMapper languageMapper;
	
	@Override
	public Map<Integer, Language> get() {		
		List<Language> tmp = jdbcTemplate.query(GET_LANGUAGES_LIST, languageMapper);
		Map<Integer, Language> hm = tmp.stream().collect(Collectors.toMap(p-> p.getId(), p -> p));		 		 
		return hm;
	}

	@Override
	public Language getByCode(String code) {
		Language language = null;
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("code", code);
		try {
			language = jdbcTemplate.queryForObject(GET_LANGUAGE_BY_CODE, params, languageMapper);
		} catch (DataAccessException e) {
			logger.error("can't find language by code: " + code);
		}		
		return language;
	}

}
