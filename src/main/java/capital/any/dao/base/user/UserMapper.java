package capital.any.dao.base.user;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import capital.any.base.enums.ActionSource;
import capital.any.dao.base.MapperBase;
import capital.any.model.base.User;
import capital.any.model.base.User.Clazz;
import capital.any.model.base.User.Gender;

/**
 * @author eranl
 *
 */
@Component
public class UserMapper extends MapperBase implements RowMapper<User> {
	
	@Override
	public User mapRow(ResultSet rs, int row) throws SQLException {
		User user = new User();
		user.setId(rs.getLong("id"));
		user.setBalance(rs.getLong("balance"));
		user.setPassword(rs.getString("password"));
		user.setCurrencyId(rs.getInt("currency_id"));
		user.setFirstName(rs.getString("first_name"));
		user.setLastName(rs.getString("last_name"));
		user.setStreet(rs.getString("street"));
		user.setZipCode(rs.getString("zip_code"));
		user.setTimeCreated(convertTimestampToDate(rs.getTimestamp("time_created")));
		user.setTimeLastLogin(rs.getDate("time_last_login"));
		user.setIsActive(rs.getInt("is_active"));		
		user.setEmail(rs.getString("email"));
		user.setComments(rs.getString("comments"));
		user.setIp(rs.getString("ip"));
		user.setTimeBirthDate(convertTimestampToDate(rs.getTimestamp("time_birth_Date")));
		user.setIsContactByEmail(rs.getInt("is_contact_by_email"));
		user.setIsContactBySms(rs.getInt("is_contact_by_sms"));
		user.setIsContactByPhone(rs.getInt("is_contact_by_phone"));
		user.setIsAcceptedTerms(rs.getInt("is_accepted_terms"));
		user.setMobilePhone(rs.getString("mobile_phone"));
		user.setLandLinePhone(rs.getString("land_line_phone"));
		user.setGender(Gender.getByToken(rs.getString("gender")));
		user.setClazz(Clazz.get(rs.getInt("class_id")));
		user.setStreetNo(rs.getString("street_no"));
		user.setLanguageId(rs.getInt("language_id"));
		user.setUtcOffset(rs.getInt("utc_offset"));
		user.setCityName(rs.getString("city_name"));
		user.setUserAgent(rs.getString("user_agent"));
		user.setHttpReferer(rs.getString("http_referer"));
		user.setWriterId(rs.getInt("writer_id"));
		user.setAoUserId(rs.getInt("ao_user_id"));
		user.setActionSource(ActionSource.get(rs.getInt("action_source_id")));
		user.setUtcOffsetCreated(rs.getString("utc_offset_created"));
		user.setCountryByIP(rs.getInt("country_by_ip"));
		user.setCountryByUser(rs.getInt("country_by_user"));
		user.setCountryByPrefix(rs.getInt("country_by_prefix"));
		user.setTIN(rs.getString("tin"));
		return user;
	}
	
	public User mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}

}
