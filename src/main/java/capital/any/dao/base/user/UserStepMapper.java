package capital.any.dao.base.user;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import capital.any.dao.base.MapperBase;
import capital.any.model.base.UserStep;

/**
 * @author LioR SoLoMoN
 *
 */
@Component
public class UserStepMapper extends MapperBase implements RowMapper<UserStep> {

	@Override
	public UserStep mapRow(ResultSet rs, int rowNum) throws SQLException {
		UserStep userStep = new UserStep(rs.getInt("step_id"), rs.getString("step_display_name"));
		return userStep;
	}
	
	public UserStep mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
}
