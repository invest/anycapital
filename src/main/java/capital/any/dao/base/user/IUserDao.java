package capital.any.dao.base.user;

import java.util.List;

import capital.any.model.base.User;
import capital.any.model.base.UserFilters;

/**
 * @author eranl
 *
 */
public interface IUserDao {
	
	public static final String GET_USER_BY_EMAIL = 
			" SELECT " +
			"	u.*, " +
			"	ma.REFERENCE_ID as ftd_id " +
			"	,us.id as step_id " +
			"	,us.display_name as step_display_name " +
			" FROM " +
			"	users u " +
			"   LEFT JOIN marketing_attribution ma on ma.user_id = u.id and ma.table_id = :table_id " +
			"	,user_steps us " +
			" WHERE " +
			"	upper(u.email) = upper(:email) " +
			"	AND u.step = us.id ";
	
	public static final String GET_USER_BY_ID = 
			" SELECT " +
			"	u.*, " +
			"	ma.REFERENCE_ID as ftd_id " +
			"	,us.id as step_id " +
			"	,us.display_name as step_display_name " +
			" FROM " +
			"	users u " +
			"   LEFT JOIN marketing_attribution ma on ma.user_id = u.id and ma.table_id = :table_id " +
			"	,user_steps us " +
			" WHERE " +
			"	u.id = :id " + 
			"	AND u.step = us.id ";	
	
	public static final String INSERT_USER = 
			"INSERT INTO " +
			"	users " +
			"	( " +
			"		password " +
			"		,currency_id " +
			"		,first_name " +
			"		,last_name " +
			"		,street " +
			"		,zip_code " +
			"		,email " +
			"		,comments " +
			"		,ip " +
			"		,time_birth_date " +
			"		,is_contact_by_email " +
			"		,is_contact_by_sms " +
			"		,is_contact_by_phone " +
			"		,is_accepted_terms " +
			"		,mobile_phone " +
			"		,land_line_phone " +
			"		,gender " +
			"		,street_no " +
			"		,language_id " +
			"		,utc_offset " +
			"		,city_name " +
			"		,utc_offset_created " +
			"		,writer_id " +
			"		,ao_user_id " +
			"		,action_source_id " +
			"		,user_agent " +
			"		,http_referer " +
			"		,step " +
			"		,country_by_user " +
			"		,country_by_ip " +
			"		,country_by_prefix " +
			"		,tin " +
			"	) " +
			"VALUES " +
			"	(	" +
			"		:password " +
			"		,:currency_id " +
			"		,:first_name " +
			"		,:last_name " +
			"		,:street " +
			"		,:zip_code " +
			"		,:email " +
			"		,:comments " +
			"		,:ip " +
			"		,:time_birth_date " +
			"		,:is_contact_by_email " +
			"		,:is_contact_by_sms " +
			"		,:is_contact_by_phone " +
			"		,:is_accepted_terms " +
			"		,:mobile_phone " +
			"		,:land_line_phone " +
			"		,:gender " +
			"		,:street_no " +
			"		,:language_id " +
			"		,:utc_offset " +
			"		,:city_name " +
			"		,:utc_offset_created " +
			"		,:writer_id " +
			"		,:ao_user_id " +
			"		,:action_source_id " +
			"		,:user_agent " +
			"		,:http_referer " +
			"		,:step " +
			"		,:country_by_user " +
			"		,:country_by_ip " +
			"		,:country_by_prefix " +
			"		,:tin " +
			"	)	";
	
	public static final String UPDATE = 
			"UPDATE " +
			"	users " +
			"SET " +
			"	 first_name = :first_name " +
			"	,last_name = :last_name " +
			"	,street = :street " +
			"	,zip_code = :zip_code " +
			//"	,time_last_login = :time_last_login " +
			"	,is_active = :is_active " +
			"	,comments = :comments " +
			"	,ip = :ip " +
			//"	,time_birth_date = :time_birth_date " +
			"	,is_contact_by_email = :is_contact_by_email " +
			"	,is_contact_by_sms = :is_contact_by_sms " +
			"	,is_contact_by_phone = :is_contact_by_phone " +
			"	,is_accepted_terms = :is_accepted_terms " +
			"	,mobile_phone = :mobile_phone " +
			"	,land_line_phone = :land_line_phone " +
			"	,gender = :gender " +
			//"	,time_modified = :time_modified " +
			"	,class_id = :class_id " +
			"	,street_no = :street_no " +
			"	,language_id = :language_id " +
			//"	,utc_offset = :utc_offset " +
			"	,city_name = :city_name " +
			"	,writer_id = :writer_id " +
			"	,ao_user_id = :ao_user_id " +
			//"	,user_agent = :user_agent " +
			"	,is_regulated = :is_regulated " +
			"	,step = :step " +
			"	,country_by_prefix = :countryByPrefix " +
			"	,tin = :tin " +
			"WHERE " +
			"	id = :id ";
	
	public static final String UPDATE_STEP_B = 
			"UPDATE " +
			"	users " +
			"SET " +
			"	street = :street " +
			"	,street_no = :street_no " +
			"	,zip_code = :zip_code " +
			"	,time_birth_date = :time_birth_date " +
			"	,city_name = :city_name " +
			"	,step = :step " +
			"	,country_by_user = :country_by_user " +
			"WHERE " +
			"	id = :id ";
	
	public static final String CHANGE_PASSWORD = 
			"UPDATE " +
			"	users " +
			"SET " +
			"	password = :password " +
			"WHERE " +
			"	id = :id ";
		
	public static final String GET_USERS_BY_DATES = 
			" SELECT " +
			"	u.* " +
			" FROM " +
			"	users u " +
			" WHERE " +
			"	u.time_created >= :fromDate " +
			"   and u.time_created <= :toDate ";
	
	public static final String UPDATE_STEP = 
			"UPDATE " +
			"	users " +
			"SET " +
			"	step = :step " +
			"WHERE " +
			"	id = :id ";
	
	public static final String ACCEPT_TERMS = 
			" UPDATE " +
			"	users " +
			" SET " +
			"	is_accepted_terms = :is_accepted_terms " +
			" WHERE " +
			"	id = :id ";		
	
	/**
	 * Get user by email
	 * @param id
	 * @return
	 */
	User getUserByEmail(String email);
		
	/**
	 * Get user by id
	 * @param id
	 * @return
	 */
	User getUserById(long id);
	
	/**
	 * Insert user
	 * @param user
	 * @throws Exception 
	 */
	void insertUser(User user) throws Exception;
	
	/**
	 * update user
	 * @param user
	 */
	void update(User user);

	/**
	 * update user for step b
	 * @param user
	 * @return 
	 */
	boolean updateStepB(User user);
	
	/**
	 * @param user
	 * @return 
	 */
	boolean changePassword(User user);
	
	/**
	 * Get users by dates
	 * @param userFilters
	 * @return List<User>
	 */
	List<User> getUsersByDates(UserFilters userFilters);

	/**
	 * Update user step
	 * @param user
	 * @return
	 */
	boolean updateStep(User user);
	
	/**
	 * Update accept terms
	 * @param user
	 * @return
	 */
	boolean acceptTerms(User user);
		
}
