package capital.any.dao.base.user;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Component;
import capital.any.model.base.User;
import capital.any.model.base.UserStep;

/**
 * @author LioR SoLoMoN
 *
 */
@Component
@Scope("prototype")
public class UserCallbackHandler implements RowCallbackHandler {
	private User result;
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private UserStepMapper userStepMapper;
	
	@Override
	public void processRow(ResultSet rs) throws SQLException {
		UserStep userStep = userStepMapper.mapRow(rs);
		User user = userMapper.mapRow(rs);
		user.setFtdId(rs.getInt("ftd_id"));
		user.setUserStep(userStep);

		result = user;
	}
	
	/**
	 * @return the result
	 */
	public User getResult() {
		return result;
	}
}
