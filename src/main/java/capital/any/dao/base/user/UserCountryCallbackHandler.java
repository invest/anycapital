package capital.any.dao.base.user;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Component;

import capital.any.dao.base.country.CountryMapper;
import capital.any.model.base.Country;
import capital.any.model.base.User;

@Component
@Scope("prototype")
public class UserCountryCallbackHandler implements RowCallbackHandler {
			
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private CountryMapper countryMapper;

	@Override
	public void processRow(ResultSet rs) throws SQLException {
		User user = userMapper.mapRow(rs);
		Country country = countryMapper.mapRow(rs);
	}


}
