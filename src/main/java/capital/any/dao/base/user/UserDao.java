package capital.any.dao.base.user;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import capital.any.base.enums.Table;
import capital.any.model.base.User;
import capital.any.model.base.UserFilters;

@Repository
public class UserDao implements IUserDao, ApplicationContextAware {
	private static final Logger logger = LoggerFactory.getLogger(UserDao.class);
	private ApplicationContext appContext;
	@Autowired
	protected NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private UserMapper userMapper;

//
//	@Override
//	public Map<User, List<Role>> getUsersWithRoles() {
//		UserRoleCallbackHandler callbackHandler = appContext.getBean(UserRoleCallbackHandler.class); 
//		jdbcTemplate.query(GET_USERS_WITH_ROLE, callbackHandler);
//		Map<User, List<Role>> map = callbackHandler.getResult();
//		return map;
//	}
	
	@Override
	public User getUserByEmail(String email) {
		UserCallbackHandler userCallbackHandler = appContext.getBean(UserCallbackHandler.class);
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("email", email);
		params.addValue("table_id", Table.TRANSACTIONS.getId());
		try {
			jdbcTemplate.query(GET_USER_BY_EMAIL, params, userCallbackHandler);
		} catch (DataAccessException e) {
			logger.error("can't find user by email");
		}
		return userCallbackHandler.getResult();
	}
	
	@Override
	public User getUserById(long id) {
		UserCallbackHandler userCallbackHandler = appContext.getBean(UserCallbackHandler.class);
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", id);
		params.addValue("table_id", Table.TRANSACTIONS.getId());
		try { 
			jdbcTemplate.query(GET_USER_BY_ID, params, userCallbackHandler);
		} catch (DataAccessException e) {
			logger.error("can't find user by id");
		}
		return userCallbackHandler.getResult();
	}
	
	@Override
	public void insertUser(User user) throws Exception {	
		KeyHolder keyHolder = new GeneratedKeyHolder();
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource
				("password", user.getPassword())
				.addValue("currency_id", user.getCurrencyId())
				.addValue("first_name", user.getFirstName())
				.addValue("last_name", user.getLastName())
				.addValue("street", user.getStreet())
				.addValue("zip_code", user.getZipCode())
				.addValue("email", user.getEmail())
				.addValue("comments", user.getComments())
				.addValue("ip", user.getIp())
				.addValue("time_birth_date", user.getTimeBirthDate())
				.addValue("is_contact_by_email", user.getIsContactByEmail())
				.addValue("is_contact_by_sms", user.getIsContactBySms())
				.addValue("is_contact_by_phone", user.getIsContactByPhone())
				.addValue("is_accepted_terms", user.getIsAcceptedTerms())
				.addValue("mobile_phone", user.getMobilePhone())
				.addValue("land_line_phone", user.getLandLinePhone())
				.addValue("gender", user.getGender().getToken())
				.addValue("street_no", user.getStreetNo())
				.addValue("language_id", user.getLanguageId())
				.addValue("utc_offset", user.getUtcOffset())
				.addValue("city_name", user.getCityName())
				.addValue("utc_offset_created", user.getUtcOffsetCreated())
				.addValue("writer_id", user.getWriterId() == 0 ? null : user.getWriterId())
				.addValue("ao_user_id", user.getAoUserId())
				.addValue("action_source_id", user.getActionSource().getId())
				.addValue("user_agent", user.getUserAgent())
				.addValue("http_referer", user.getHttpReferer())
				.addValue("step", user.getUserStep().getId())
				.addValue("country_by_user", user.getCountryByUser() == 0 ? null : user.getCountryByUser())
				.addValue("country_by_ip", user.getCountryByIP() == 0 ? null : user.getCountryByIP())
				.addValue("country_by_prefix", user.getCountryByPrefix() == 0 ? null : user.getCountryByPrefix())
				.addValue("tin", user.getTIN())
				;
		jdbcTemplate.update(INSERT_USER, namedParameters, keyHolder, new String[] {"id"});
		user.setId(keyHolder.getKey().longValue());
	}
	
	@Override
	public void update(User user) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("id", user.getId()).
				addValue("first_name", user.getFirstName())
				.addValue("last_name", user.getLastName())
				.addValue("street", user.getStreet())
				.addValue("zip_code", user.getZipCode())
				//.addValue("time_last_login", user.getTimeLastLogin())
				.addValue("is_active", user.getIsActive())
				.addValue("comments", user.getComments())
				.addValue("ip", user.getIp())
				//.addValue("time_birth_date", user.getTimeBirthDate())
				.addValue("is_contact_by_email", user.getIsContactByEmail())
				.addValue("is_contact_by_sms", user.getIsContactBySms())
				.addValue("is_contact_by_phone", user.getIsContactByPhone())
				.addValue("is_accepted_terms", user.getIsAcceptedTerms())
				.addValue("mobile_phone", user.getMobilePhone())
				.addValue("land_line_phone", user.getLandLinePhone())
				.addValue("gender", user.getGender().getToken())
				//.addValue("time_modified", user.getTimeModified())
				.addValue("class_id", user.getClazz().getToken())
				.addValue("street_no", user.getStreetNo())
				.addValue("language_id", user.getLanguageId())
				//.addValue("utc_offset", user.getUtcOffset())
				.addValue("city_name", user.getCityName())
				.addValue("writer_id", user.getWriterId() == 0 ? null : user.getWriterId())
				.addValue("ao_user_id", user.getAoUserId())
				//.addValue("user_agent", user.getUserAgent())
				.addValue("is_regulated", user.isRegulated())
				.addValue("step", user.getUserStep().getId())
				.addValue("countryByPrefix", user.getCountryByPrefix())
				.addValue("tin", user.getTIN())
				;
				jdbcTemplate.update(UPDATE, namedParameters);	
	}
	
	@Override
	public boolean updateStepB(User user) {
		SqlParameterSource namedParameters = 
			new MapSqlParameterSource("id", user.getId())
				.addValue("street", user.getStreet())
				.addValue("street_no", user.getStreetNo())
				.addValue("zip_code", user.getZipCode())
				.addValue("time_birth_date", user.getTimeBirthDate()) 
				.addValue("city_name", user.getCityName())
				.addValue("step", user.getUserStep().getId())
				.addValue("country_by_user", user.getCountryByUser())
				;
		return (jdbcTemplate.update(UPDATE_STEP_B, namedParameters) > 0);		
	}
	
	public boolean changePassword(User user) {
		SqlParameterSource namedParameters = 
			new MapSqlParameterSource("id", user.getId()).
			addValue("password", user.getPassword());
			return (jdbcTemplate.update(CHANGE_PASSWORD, namedParameters) > 0);	
	}
	
	@Override
	public void setApplicationContext(ApplicationContext appContext) throws BeansException {
		this.appContext = appContext;
		
	}

	@Override
	public List<User> getUsersByDates(UserFilters userFilters) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("fromDate", userMapper.convertLocalDateTimeToTimestamp(userFilters.getFromDate()));
		params.addValue("toDate", userMapper.convertLocalDateTimeToTimestamp(userFilters.getToDate()));
		List<User> tmp = jdbcTemplate.query(GET_USERS_BY_DATES, params, userMapper);
		return tmp;
	}
	
	@Override
	public boolean updateStep(User user) {
		SqlParameterSource namedParameters = 
			new MapSqlParameterSource("id", user.getId())
				.addValue("step", user.getUserStep().getId())
				;
		return (jdbcTemplate.update(UPDATE_STEP, namedParameters) > 0);		
	}

	@Override
	public boolean acceptTerms(User user) {
		SqlParameterSource namedParameters = new MapSqlParameterSource("id", user.getId())
				.addValue("is_accepted_terms",
				user.getIsAcceptedTerms());
		return (jdbcTemplate.update(ACCEPT_TERMS, namedParameters) > 0);
	}
}
