package capital.any.dao.base.user;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Component;

import capital.any.dao.base.role.RoleMapper;
import capital.any.model.base.Role;
import capital.any.model.base.User;

@Component
@Scope("prototype")
public class UserRoleCallbackHandler implements RowCallbackHandler {
	
	private Map<User, List<Role>> result = new HashMap<User, List<Role>>();
	
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private RoleMapper roleMapper;

	@Override
	public void processRow(ResultSet rs) throws SQLException {
		User user = userMapper.mapRow(rs);
		List<Role> roles = result.get(user);
		if (roles == null) {
			roles = new ArrayList<Role>();
			result.put(user, roles);
		}
		Role role = roleMapper.mapRow(rs);
		roles.add(role);
		result.put(user, roles);
	}

	public Map<User, List<Role>> getResult() {
		return result;
	}

	public void setResult(Map<User, List<Role>> result) {
		this.result = result;
	}
	
	

}
