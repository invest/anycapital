package capital.any.dao.base.issueDirection;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.IssueDirection;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
public class IssueDirectionMapper extends MapperBase implements RowMapper<IssueDirection> {

	@Override
	public IssueDirection mapRow(ResultSet rs, int rowNum) throws SQLException {
		IssueDirection issueDirection = new IssueDirection();
		issueDirection.setId(rs.getInt("iss_d_id"));
		issueDirection.setName(rs.getString("iss_d_name"));
		return issueDirection;
	}
	
	public IssueDirection mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}

}
