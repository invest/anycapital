package capital.any.dao.base.issueDirection;

import java.util.Map;

import capital.any.model.base.IssueDirection;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IIssueDirectionDao {
	
	public static final String GET_ISSUE_DIRECTION_LIST =
			" SELECT " +
				" iss_d.id iss_d_id, " +
				" iss_d.name iss_d_name " + 
			" FROM "	+ 
				" issue_directions iss_d ";
	
	Map<Integer, IssueDirection> get();
	
}
