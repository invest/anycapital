package capital.any.dao.base.issueDirection;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.IssueDirection;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository("IssueDirectionHCDao")
public class IssueDirectionHCDao implements IIssueDirectionDao {
	private static final Logger logger = LoggerFactory.getLogger(IssueDirectionHCDao.class);
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, IssueDirection> get() {
		Map<Integer, IssueDirection> issueDirections = null;
		try {
			issueDirections = (Map<Integer, IssueDirection>) HazelCastClientFactory.getClient().getMap(Constants.MAP_ISSUE_DIRECTIONS);
		} catch (Exception e) {
			logger.error("Problem to get issueDirections by HC.");
		}
		return issueDirections;
	}

}
