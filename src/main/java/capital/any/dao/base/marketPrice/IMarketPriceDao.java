package capital.any.dao.base.marketPrice;

import java.util.List;
import capital.any.model.base.MarketPrice;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IMarketPriceDao {
	
	public static final String UPDATE_MARKET_PRICE = 
			" UPDATE " + 
				" market_prices mp " +
			" SET " +
				" mp.price = :price, " + 
				" mp.price_date = now() " +
			" WHERE " +
				" mp.market_id = :marketId " +
				" AND type_id = :typeId ";
	public static final String INSERT_MARKET_PRICE = 
			"  INSERT " +
		    "  INTO market_prices " +
			"  ( " +
		    "     market_id, " +
		    "     price, " +
		    " 	  type_id, " +
		    " 	  price_date " + 
		    "  ) " +
		    "  VALUES " +
		    "  ( " +
		    "     :marketId, " +
		    " 	  :price, " +
		    " 	  :typeId, " +
		    "	  :priceDate " +
		    "  ) ";
		
	/**
	 * Update market price in market and in all products that have this market
	 * @param marketPrice
	 * @throws Exception 
	 */
	boolean updateMarketPrice(MarketPrice marketPrice) throws Exception;
	
	/**
	 * insert market price daily history in market and in all products that have this market
	 * @param marketPrice
	 * @throws Exception 
	 */
	boolean updateProductsMarketHistoryPrice(MarketPrice marketPrice) throws Exception;
	
	/**
	 * @param marketPrice
	 */
	void insertMarketPrice(MarketPrice marketPrice);
	
	/**
	 * @param marketPriceList
	 */
	void insertMarketPriceBatch(List<MarketPrice> marketPriceList);

	/**
	 * @param marketPrice
	 * @return
	 * @throws Exception 
	 */
	boolean updateProductsMarketPrice(MarketPrice marketPrice) throws Exception;

}
