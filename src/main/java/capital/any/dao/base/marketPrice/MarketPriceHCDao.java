package capital.any.dao.base.marketPrice;

import java.util.Collection;
import java.util.List;
import org.springframework.stereotype.Repository;
import com.hazelcast.core.IMap;
import com.hazelcast.transaction.TransactionContext;
import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.hazelcast.update.base.market.UpdateMarketPriceEntry;
import capital.any.hazelcast.update.base.product.UpdateProductMarketHistoryPriceEntry;
import capital.any.hazelcast.update.base.product.UpdateProductMarketPriceEntry;
import capital.any.model.base.MarketPrice;
import capital.any.model.base.Product;

/**
 * @author eranl
 *
 */
@Repository("MarketPriceHCDao")
public class MarketPriceHCDao implements IMarketPriceDao {

	@Override
	public boolean updateMarketPrice(MarketPrice marketPrice) throws Exception { 
		return HazelCastClientFactory.getClient().updateTransactionMap(
				marketPrice.getMarketId(),
				new UpdateMarketPriceEntry(marketPrice.getPrice()), 
				Constants.MAP_MARKETS);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean updateProductsMarketPrice(MarketPrice marketPrice) throws Exception {		
		IMap<Long, Product> products = (IMap<Long, Product>) HazelCastClientFactory.getClient().getMap(Constants.MAP_PRODUCTS);
		boolean result = true;
		if (products != null && !products.isEmpty()) {	
			Collection<Product> productsList = products.values(new ProductsWithMarketPredicate(marketPrice));
			TransactionContext transaction = HazelCastClientFactory.getClient().startTransaction();
			try {
				for (Product product : productsList) {
					result = HazelCastClientFactory.getClient().updateTransactionMap(
							transaction,
							product.getId(),
							new UpdateProductMarketPriceEntry(marketPrice), 
							Constants.MAP_PRODUCTS);
				}
			} catch (Exception e) {
				HazelCastClientFactory.getClient().rollbackTransaction(transaction);
				result = false;
			}
			HazelCastClientFactory.getClient().commitTransaction(transaction);
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean updateProductsMarketHistoryPrice(MarketPrice marketPrice) throws Exception {		
		IMap<Long, Product> products = (IMap<Long, Product>) HazelCastClientFactory.getClient().getMap(Constants.MAP_PRODUCTS);
		boolean result = true;
		if (products != null && !products.isEmpty()) {	
			Collection<Product> productsList = products.values(new ProductsWithMarketPredicate(marketPrice));
			TransactionContext transaction = HazelCastClientFactory.getClient().startTransaction();
			try {
				for (Product product : productsList) {
					result = HazelCastClientFactory.getClient().updateTransactionMap(
							transaction,
							product.getId(),
							new UpdateProductMarketHistoryPriceEntry(marketPrice), 
							Constants.MAP_PRODUCTS);
				}
			} catch (Exception e) {
				HazelCastClientFactory.getClient().rollbackTransaction(transaction);
				result = false;
			}
			HazelCastClientFactory.getClient().commitTransaction(transaction);
		}
		return result;
	}

	@Override
	public void insertMarketPrice(MarketPrice marketPrice) {
		
	}

	@Override
	public void insertMarketPriceBatch(List<MarketPrice> marketPriceList) {
	}
}
