package capital.any.dao.base.marketPrice;

import java.util.Map.Entry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.hazelcast.query.Predicate;
import capital.any.base.enums.ProductStatusEnum;
import capital.any.model.base.MarketPrice;
import capital.any.model.base.Product;
import capital.any.model.base.ProductMarket;

/**
 * 
 * @author eyal.goren
 * get only products that open and have this market price
 */
public class ProductsWithMarketPredicate implements Predicate<Long, Product>{
	private static final Logger logger = LoggerFactory.getLogger(ProductsWithMarketPredicate.class);
	private static final long serialVersionUID = -5752092313811515311L;
	private MarketPrice marketPrice;
	
	public ProductsWithMarketPredicate(MarketPrice marketPrice) {
		super();
		this.marketPrice = marketPrice;
	}

	@Override
	public boolean apply(Entry<Long, Product> mapEntry) {
		Product product  = mapEntry.getValue();
		if (product.getProductStatus().getId() < ProductStatusEnum.SETTLED.getId()) {
			for (ProductMarket productMarket : product.getProductMarkets()) {
				if (productMarket.getMarket().getId() == marketPrice.getMarketId()) {
					logger.debug("found product with market " + product.getId() + " market id " + productMarket.getMarket().getId());
					return true;
				}
			}
		}
		logger.debug("NOT found product with market " + product.getId());
		return false;
	}
}
