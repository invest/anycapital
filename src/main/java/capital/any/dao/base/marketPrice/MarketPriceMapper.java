package capital.any.dao.base.marketPrice;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.MarketPrice;

@Component
public class MarketPriceMapper extends MapperBase implements RowMapper<MarketPrice> {

	@Override
	public MarketPrice mapRow(ResultSet rs, int rowNum) throws SQLException {
		MarketPrice marketPrice = new MarketPrice();
		marketPrice.setTypeId(rs.getInt("type_id"));
		marketPrice.setMarketId(rs.getInt("market_id"));
		marketPrice.setPrice(rs.getDouble("price"));
		marketPrice.setPriceDate(convertTimestampToDate(rs.getTimestamp("price_date")));		
		return marketPrice;
	}
	
	public MarketPrice mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}

}
