package capital.any.dao.base.productMaturity;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.base.ProductMaturity;

/**
 * @author Eyal G
 *
 */
@Component
public class ProductMaturityMapper implements RowMapper<ProductMaturity> {
	
	@Override
	public ProductMaturity mapRow(ResultSet rs, int row) throws SQLException {
		ProductMaturity productMaturity = new ProductMaturity();
		productMaturity.setId(rs.getInt("id"));
		productMaturity.setName(rs.getString("name"));
		productMaturity.setDisplayName(rs.getString("display_name"));		
		return productMaturity;
	}
	
	public ProductMaturity mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
			
}
	

