package capital.any.dao.base.productMaturity;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import capital.any.dao.base.currency.CurrencyHCDao;
import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.ProductMaturity;

/**
 * @author LioR SoLoMoN
 *
 */
@Repository("ProductMaturityHCDao")
public class ProductMaturityHCDao implements IProductMaturityDao {	
	private static final Logger logger = LoggerFactory.getLogger(CurrencyHCDao.class);
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, ProductMaturity>  get() {		
		Map<Integer, ProductMaturity> productMaturities = null;		
		try {
			productMaturities = (Map<Integer, ProductMaturity>) HazelCastClientFactory.getClient().getMap(Constants.MAP_PRODUCT_MATURITY);
		} catch (Exception e) {
			logger.error("ERROR!", e);
		}
		return productMaturities;
	}
}
