package capital.any.dao.base.productMaturity;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import capital.any.model.base.ProductMaturity;

/**
 * @author Eyal G
 *
 */
@Primary
@Repository
public class ProductMaturityDao implements IProductMaturityDao {
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private ProductMaturityMapper productMaturityMapper;
	
	@Override
	public Map<Integer, ProductMaturity> get() {
		List<ProductMaturity> tmp = jdbcTemplate.query(GET_PRODUCTMATURITIES, productMaturityMapper);
		Map<Integer, ProductMaturity> hm = tmp.stream().collect(Collectors.toMap(p-> p.getId(), p -> p));		 		 
		return hm;
	}
	
}
