package capital.any.dao.base.productMaturity;

import java.util.Map;
import capital.any.model.base.ProductMaturity;

/**
 * @author Eyal G
 *
 */
public interface IProductMaturityDao {
	
	public static final String GET_PRODUCTMATURITIES = 
			" SELECT " +
			"	* " + 
			" FROM "	+ 
			"	product_maturities ";
		
	Map<Integer, ProductMaturity> get();

}
