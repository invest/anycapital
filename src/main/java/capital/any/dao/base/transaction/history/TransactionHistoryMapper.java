package capital.any.dao.base.transaction.history;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import capital.any.model.base.TransactionHistory;
import capital.any.base.enums.ActionSource;
import capital.any.dao.base.MapperBase;
import capital.any.dao.base.writer.BaseWriterFactory;

/**
 * @author LioR SoLoMoN
 *
 */
@Component
public class TransactionHistoryMapper extends MapperBase implements RowMapper<TransactionHistory> {
	@Autowired
	private BaseWriterFactory writerFactory;
	
	@Override
	public TransactionHistory mapRow(ResultSet rs, int row) throws SQLException {
		TransactionHistory transactionHistory = new TransactionHistory();
		transactionHistory.setTransactionId(rs.getLong("transaction_id"));
		//transactionHistory.setStatus(TransactionStatus.get(rs.getInt("transaction_status_id")));
		transactionHistory.setActionSource(ActionSource.get(rs.getInt("action_source_id")));
		transactionHistory.setServerName(rs.getString("server_name"));
		transactionHistory.setTimeCreated(convertTimestampToDate(rs.getTimestamp("time_created")));
		transactionHistory.setWriter(writerFactory.createWriter(rs.getInt("writer_id")));
		transactionHistory.setLoginId(rs.getLong("login_id"));
		return transactionHistory;
	}
	
	public TransactionHistory mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
}
