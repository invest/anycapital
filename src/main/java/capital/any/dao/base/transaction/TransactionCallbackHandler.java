package capital.any.dao.base.transaction;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Component;
import capital.any.dao.base.transactionOperation.TransactionOperationMapper;
import capital.any.dao.base.transactionPaymentTypes.TransactionPaymentTypeMapper;
import capital.any.dao.base.transactionStatus.TransactionStatusMapper;
import capital.any.model.base.Transaction;
import capital.any.model.base.TransactionOperation;
import capital.any.model.base.TransactionPaymentType;
import capital.any.model.base.TransactionStatus;

/**
 * @author LioR SoLoMoN
 *
 */
@Component
@Scope("prototype")
public class TransactionCallbackHandler implements RowCallbackHandler {
	private Map<Long, Transaction> result = new HashMap<Long, Transaction>();
	@Autowired
	private TransactionMapper transactionMapper;
	@Autowired
	private TransactionOperationMapper transactionOperationMapper;
	@Autowired
	private TransactionPaymentTypeMapper transactionPaymentTypeMapper;
	@Autowired
	private TransactionStatusMapper transactionStatusMapper;
	
	@Override
	public void processRow(ResultSet rs) throws SQLException {
		TransactionOperation transactionOperation = transactionOperationMapper.mapRow(rs);
		TransactionPaymentType transactionPaymentType = transactionPaymentTypeMapper.mapRow(rs);
		TransactionStatus transactionStatus = transactionStatusMapper.mapRow(rs);
		
		Transaction transaction = transactionMapper.mapRow(rs);
		transaction.setOperation(transactionOperation);
		transaction.setPaymentType(transactionPaymentType);
		transaction.setStatus(transactionStatus);

		result.put(transaction.getId(), transaction);
	}
	
	/**
	 * @return the result
	 */
	public Map<Long, Transaction> getResult() {
		return result;
	}
}
