package capital.any.dao.base.transaction.history;

import capital.any.model.base.TransactionHistory;

/**
 * @author LioR SoLoMoN
 *
 */
public interface ITransactionHistoryDao {
	
	public static final String GET_TRANSACTIONS_HISTORY =
			"SELECT " +
			"	* " + 
			"FROM " +
			"	transactions_history";

	public static final String GET_TRANSACTION_HISTORY =
			"SELECT " +
			"	* " + 
			"FROM " +
			"	transactions_history " +
			"WHERE " +
			"	transaction_id = :transactionId " +
			"	AND transaction_status_id = :statusId";

	public static final String INSERT_TRANSACTION_HISTORY =
			"INSERT INTO transactions_history " +
			" ( " +
			"	transaction_id " +
			"	,transaction_status_id " +
			"	,writer_id " +
			"	,server_name " +
			"	,action_source_id " +
			"	,login_id " +
			"	,time_created " +
			" ) " +
			"VALUES " +
			" ( " +
			"	:transactionId " +
			"	,:statusId " +
			"	,:writerId " +
			"	,:serverName " +
			"	,:actionSourceId " +
			"	,:loginId " +
			"	,:timeCreated " +
			" ) ";

	/**
	 * @param transactionId
	 * @param status
	 * @return
	 */
	TransactionHistory get(TransactionHistory transactionHistory);

	/**
	 * @param transactionHistory
	 * @return 
	 */
	boolean insert(TransactionHistory transactionHistory);
	
}
