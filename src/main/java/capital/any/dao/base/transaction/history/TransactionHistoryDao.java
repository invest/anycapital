package capital.any.dao.base.transaction.history;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import capital.any.model.base.TransactionHistory;

/**
 * @author LioR SoLoMoN
 *
 */
@Repository
public class TransactionHistoryDao implements ITransactionHistoryDao {
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private TransactionHistoryMapper transactionHistoryMapper;

	/*@Override
	public Map<Long, TransactionHistory> get() {
		List<TransactionHistory> transactionsHistory = 
				jdbcTemplate.query(GET_TRANSACTIONS_HISTORY, transactionHistoryMapper);
		return transactionsHistory.stream().collect(Collectors.toMap(t -> t.getTransactionId(), t -> t));
	}*/

	@Override
	public TransactionHistory get(TransactionHistory transactionHistory) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("transactionId", transactionHistory.getTransactionId());
		params.addValue("statusId", transactionHistory.getStatus().getId());
		TransactionHistory th = null;
		try {
			th = jdbcTemplate.queryForObject(GET_TRANSACTION_HISTORY, params, transactionHistoryMapper);
		} catch (EmptyResultDataAccessException e) {

		}
		return th;
	}

	@Override
	public boolean insert(TransactionHistory transactionHistory) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource
				("transactionId", transactionHistory.getTransactionId())
				.addValue("statusId", transactionHistory.getStatus().getId())
				.addValue("writerId", transactionHistory.getWriter().getId())
				.addValue("serverName", transactionHistory.getServerName())
				.addValue("actionSourceId", transactionHistory.getActionSource().getId())
				.addValue("timeCreated", transactionHistory.getTimeCreated())
				.addValue("loginId", transactionHistory.getLoginId() == 0 ? null : transactionHistory.getLoginId())
				;

		return (jdbcTemplate.update(INSERT_TRANSACTION_HISTORY, namedParameters)) > 0;
	}
}
