package capital.any.dao.base.transaction;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import capital.any.model.base.SqlFilters;
import capital.any.model.base.Transaction;

/**
 * @author eranl
 *
 */
@Repository
public class TransactionDao implements ITransactionDao, ApplicationContextAware {
	protected ApplicationContext appContext;
	@Autowired
	protected NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	protected TransactionMapper transactionMapper;
	
	@Override
	public void setApplicationContext(ApplicationContext appContext) throws BeansException {
		this.appContext = appContext;
	}
	
	@Override
	public void insertTransaction(Transaction transaction) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		
		SqlParameterSource namedParameters = 
					new MapSqlParameterSource
							 ("userId", transaction.getUser().getId())
					.addValue("amount", transaction.getAmount())	
					.addValue("rate", transaction.getRate())
					.addValue("ip", transaction.getIp())
					.addValue("comments", transaction.getComments())
					.addValue("operationId", transaction.getOperation().getId())
					.addValue("paymentTypeId", transaction.getPaymentType().getId())
					.addValue("statusId", transaction.getStatus().getId())
					.addValue("timeCreated", transaction.getTimeCreated())
					.addValue("timeSettled", transaction.getTimeSettled())
					;
		
		jdbcTemplate.update(INSERT_TRANSACTION, namedParameters, 
				keyHolder, new String[] {"id"});
		transaction.setId(keyHolder.getKey().intValue());
	}
	
	@Override
	public boolean updateTransaction(Transaction transaction) {			
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("id", transaction.getId())
				.addValue("statusId", transaction.getStatus().getId())
				.addValue("ip", transaction.getIp())
				.addValue("timeSettled", transaction.getTimeSettled())
				.addValue("comments", transaction.getComments())
				;
		return (jdbcTemplate.update(UPDATE_TRANSACTION, namedParameters) > 0);			
	}
	
	@Override
	public Transaction getTransaction(long id) {
		TransactionCallbackHandler callbackHandler = appContext.getBean(TransactionCallbackHandler.class);
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", id);
		jdbcTemplate.query(GET_TRANSACTION_BY_ID, params, callbackHandler);
		return callbackHandler.getResult().get(id);
	}

	@Override
	public List<Transaction> getTransactions(SqlFilters filters) {
		TransactionCallbackHandler callbackHandler = appContext.getBean(TransactionCallbackHandler.class);
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("userId", filters.getUserId());
		params.addValue("statusId", filters.getTransactionStatusId());
		params.addValue("from", filters.getFrom());
		params.addValue("to", filters.getTo());
		params.addValue("settledAtFrom", filters.getSettledAtFrom());
		params.addValue("settledAtTo", filters.getSettledAtTo());
		params.addValue("userClassId", filters.getUserClassId());
		params.addValue("paymentTypeId", filters.getTransactionPaymentTypeId());
		jdbcTemplate.query(GET_TRANSACTIONS_WITH_FILTERS, params, callbackHandler);
		return new ArrayList<Transaction>(callbackHandler.getResult().values());
	}

	@Override
	public Map<Long, Transaction> getTransactions() {
		TransactionCallbackHandler callbackHandler = appContext.getBean(TransactionCallbackHandler.class);
		jdbcTemplate.query(GET_TRANSACTIONS, callbackHandler);
		return callbackHandler.getResult();
	}

	@Override
	public List<Transaction> getUserWaitingWithdrawalTransaction(long userId) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("userId", userId);
		return jdbcTemplate.query(GET_USER_WAITING_WITHDRAWAL_TRANSACTION, params, transactionMapper);
	}
}
