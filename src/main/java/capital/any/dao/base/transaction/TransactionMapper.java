package capital.any.dao.base.transaction;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import capital.any.dao.base.MapperBase;
import capital.any.model.base.Transaction;
import capital.any.model.base.TransactionOperation;
import capital.any.model.base.TransactionPaymentType;
import capital.any.model.base.TransactionStatus;
import capital.any.model.base.User;

/**
 * @author eranl
 *
 */
@Component
public class TransactionMapper extends MapperBase implements RowMapper<Transaction>{
	
	@Override
	public Transaction mapRow(ResultSet rs, int row) throws SQLException {
		Transaction transaction = new Transaction();
		transaction.setId(rs.getLong("id"));
		transaction.setUser(new User(rs.getLong("user_id")));
		transaction.setAmount(rs.getLong("amount"));
		transaction.setRate(rs.getDouble("rate"));
		transaction.setIp(rs.getString("ip"));
		transaction.setComments(rs.getString("comments"));
		transaction.setOperation(new TransactionOperation(rs.getInt("transaction_operation_id")));
		transaction.setPaymentType(new TransactionPaymentType(rs.getInt("transaction_payment_type_id")));
		transaction.setStatus(new TransactionStatus(rs.getInt("transaction_status_id")));
		transaction.setTimeCreated(convertTimestampToDate(rs.getTimestamp("time_created")));
		transaction.setTimeSettled(convertTimestampToDate(rs.getTimestamp("time_settled")));		
		return transaction;
	}
	
	public Transaction mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
}
