package capital.any.dao.base.transaction;

import java.util.List;
import java.util.Map;

import capital.any.model.base.SqlFilters;
import capital.any.model.base.Transaction;
import capital.any.model.base.Transaction.TransactionOperationEnum;
import capital.any.model.base.Transaction.TransactionStatusEnum;

/**
 * @author LioR SoLoMoN
 *
 */
public interface ITransactionDao {
	
	public static final String GET_TRANSACTIONS_WITH_FILTERS =
			"SELECT " +
			"	t.* " + 
			"	,top.id to_id " +
			"	,top.name to_name " +
			"	,top.display_name to_display_name " +
			"	,tpt.id tpt_id " +
			"	,tpt.name tpt_name " +
			"	,tpt.transaction_class_id tpt_transaction_class_id " +
			"	,tpt.display_name tpt_display_name " +
			"	,tpt.object_class " +
			"	,tpt.handler_class " +
			"	,ts.id ts_id " +
			"	,ts.name ts_name " +
			"	,ts.display_name ts_display_name " +
			"FROM " +
			"	transactions t " +
			"	,transaction_payment_types tpt " +
			"	,transaction_operations top " +
			"	,transaction_statuses ts " +
			"	,users u " +
			"WHERE " +
			"	t.transaction_operation_id = top.id " +
			"	AND u.id = t.user_id " +
			"	AND t.transaction_payment_type_id = tpt.id " +
			"	AND t.transaction_status_id = ts.id " +
			"   AND 1 = IF(ISNULL(:userId), 1, IF(t.user_id = :userId, 1, 0)) " +
			"   AND 1 = IF(ISNULL(:statusId), 1, IF(ts.id = :statusId, 1, 0)) " +
			"	AND 1 = IF(ISNULL(:from), 1, IF(t.time_created >= :from, 1, 0)) " +
			"	AND 1 = IF(ISNULL(:to), 1, IF(t.time_created <= :to, 1, 0)) " +
			"	AND 1 = IF(ISNULL(:settledAtFrom), 1, IF(t.time_settled >= :settledAtFrom, 1, 0)) " +
			"	AND 1 = IF(ISNULL(:settledAtTo), 1, IF(t.time_settled <= :settledAtTo, 1, 0)) " +
			"	AND 1 = IF(ISNULL(:userClassId), 1, IF(u.class_id = :userClassId, 1, 0)) " +
			"	AND 1 = IF(ISNULL(:paymentTypeId), 1, IF(tpt.id = :paymentTypeId, 1, 0)) "
			;
	
	public static final String GET_TRANSACTION_BY_ID =
			"SELECT " +
			"	t.* " + 
			"	,top.id to_id " +
			"	,top.name to_name " +
			"	,top.display_name to_display_name " +
			"	,tpt.id tpt_id " +
			"	,tpt.name tpt_name " +
			"	,tpt.transaction_class_id tpt_transaction_class_id " +
			"	,tpt.display_name tpt_display_name " +
			"	,tpt.object_class " +
			"	,tpt.handler_class " +
			"	,ts.id ts_id " +
			"	,ts.name ts_name " +
			"	,ts.display_name ts_display_name " +
			"FROM " +
			"	transactions t " +
			"	,transaction_operations top " +
			"	,transaction_payment_types tpt " +
			"	,transaction_statuses ts " +
			"WHERE " +
			"	t.id = :id " +
			"	AND t.transaction_operation_id = top.id " +
			"	AND t.transaction_payment_type_id = tpt.id " +
			"	AND t.transaction_status_id = ts.id ";
		
	public static final String INSERT_TRANSACTION = 
			"INSERT INTO transactions " +
			" ( " +
			"	user_id " +
			"	,amount " +
			"	,rate " +
			"	,ip " +
			"	,comments " +
			"	,transaction_operation_id " +
			"	,transaction_payment_type_id " +
			"	,transaction_status_id " +
			"	,time_created " +
			"	,time_settled " +
			" ) " +
			"VALUES " +
			" ( " +
			"	:userId " +
			"	,:amount" +
			"	,:rate" +
			"	,:ip" +
			"	,:comments" +
			"	,:operationId" +
			"	,:paymentTypeId" +
			"	,:statusId" +
			"	,:timeCreated" +
			"	,:timeSettled" +
			" ) ";

	
	public static final String UPDATE_TRANSACTION = 
			"UPDATE " +
			"	transactions " +
			"SET " +
			"	transaction_status_id = :statusId " +
			"	,ip = :ip " +
			"	,time_settled = :timeSettled " +
			"	,comments = :comments " +
			"WHERE " +
			"	id = :id ";
	
	public static final String GET_TRANSACTIONS = 
			"SELECT " +
			"	t.* " + 
			"	,top.id to_id " +
			"	,top.name to_name " +
			"	,top.display_name to_display_name " +
			"	,tpt.id tpt_id " +
			"	,tpt.name tpt_name " +
			"	,tpt.transaction_class_id tpt_transaction_class_id " +
			"	,tpt.display_name tpt_display_name " +
			"	,tpt.object_class " +
			"	,tpt.handler_class " +
			"	,ts.id ts_id " +
			"	,ts.name ts_name " +
			"	,ts.display_name ts_display_name " +
			"FROM " +
			"	transactions t " +
			"	,transaction_operations top " +
			"	,transaction_payment_types tpt " +
			"	,transaction_statuses ts " +
			"WHERE " + 
			"	t.transaction_operation_id = top.id " + 
			"	AND t.transaction_payment_type_id = tpt.id " + 
			"	AND t.transaction_status_id = ts.id" ;
	
	public static final String GET_USER_WAITING_WITHDRAWAL_TRANSACTION = 
			"SELECT " +
			"	t.* " + 
			"FROM " +
			"	transactions t " +
			"WHERE" +
			"	t.user_id = :userId" +
			"	AND t.transaction_operation_id = " + TransactionOperationEnum.DEBIT.getId() +
			"	AND t.transaction_status_id in " + "(" + 	TransactionStatusEnum.STARTED.getId() + "," +
															TransactionStatusEnum.WAITING_FOR_FIRST_APPROVE.getId()+ "," +
															TransactionStatusEnum.WAITING_FOR_SECOND_APPROVE.getId() + 
													")" +										
			"";
	
	/**
	 * Get transaction by id
	 * @param id
	 * @return
	 */
	Transaction getTransaction(long id);
	
	/**
	 * Get transactions list by filters
	 * @param {@link SqlFilters}
	 * @return {@link List<Transaction>}
	 */
	public List<Transaction> getTransactions(SqlFilters filters);
	
	/**
	 * Insert transaction
	 * @param transaction
	 */
	void insertTransaction(Transaction transaction);
	
	/**
	 * Update transaction
	 * @param transaction
	 * @return
	 */
	boolean updateTransaction(Transaction transaction);

	/**
	 * @return
	 */
	Map<Long, Transaction> getTransactions();
	
	/**
	 * @param userId
	 * @return
	 */
	List<Transaction> getUserWaitingWithdrawalTransaction(long userId);
	
}
