package capital.any.dao.base.market;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.Market;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
public class MarketUnderlyingAssetsMapper extends MapperBase implements RowMapper<Market> {
	
	@Override
	public Market mapRow(ResultSet rs, int rowNum) throws SQLException {
		Market market = new Market();				
		market.setId(rs.getInt("id"));
		market.setDisplayName(rs.getString("display_name"));
		market.setTicker(rs.getString("ticker"));
		market.setMarketPlace(rs.getString("market_place"));
		return market;
	}
	
	public Market mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
}
