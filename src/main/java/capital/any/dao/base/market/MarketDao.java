package capital.any.dao.base.market;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import capital.any.model.base.Market;

/**
 * @author eranl
 *
 */
@Primary
@Repository
public class MarketDao implements IMarketDao, ApplicationContextAware {
	
	private ApplicationContext appContext;
	
	@Autowired
	protected NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private MarketMapper marketMapper;
	@Autowired
	private MarketUnderlyingAssetsMapper marketUnderlyingAssetsMapper;

	@Override
	public Map<Integer, Market> getThin() {
		List<Market> tmp = jdbcTemplate.query(GET_MARKETS_LIST, marketMapper);
		Map<Integer, Market> hm = tmp.stream().collect(Collectors.toMap(p-> p.getId(), p -> p));		 		 
		return hm;
	}

	@Override
	public Map<Integer, Market> getMarketsWithLastPrice() {
		MarketCallbackHandler callbackHandler = appContext.getBean(MarketCallbackHandler.class); 	
		jdbcTemplate.query(GET_MARKETS_WITH_LAST_PRICE_LIST, callbackHandler);	
		return callbackHandler.getResult();
	}
		
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.appContext = applicationContext;		
	}
	
	@Override
	public HashMap<String, Market> getMarketsHMByFeedName() {
		List<Market> tmp = jdbcTemplate.query(GET_MARKETS_LIST, marketMapper);
		Map<String, Market> hm = tmp.stream().collect(Collectors.toMap(p-> p.getFeedName(), p -> p));		 		 
		return (HashMap<String, Market>) hm;		 
	}
	
	@Override
	public Map<Integer, Market> getUnderlyingAssets() {
		List<Market> list = jdbcTemplate.query(GET_MARKETS_LIST, marketUnderlyingAssetsMapper);
		Map<Integer, Market> hm = list.stream().collect(Collectors.toMap(p-> p.getId(), p -> p));
		return hm;
	}
}
