package capital.any.dao.base.market;

import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.Market;

/**
 * @author LioR SoLoMoN
 *
 */
@Repository("MarketHCDao")
public class MarketHCDao implements IMarketDao {
	private static final Logger logger = LoggerFactory.getLogger(MarketHCDao.class);

	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, Market> getMarketsWithLastPrice() {
		Map<Integer, Market> markets = null;		
		try {
			markets = (Map<Integer, Market>) HazelCastClientFactory.getClient().getMap(Constants.MAP_MARKETS);
		} catch (Exception e) {
			logger.error("ERROR!", e);
		}
		return markets;
	}

	@Override
	public HashMap<String, Market> getMarketsHMByFeedName() {
		logger.info("hazelcast getMarketsHMByFeedName not implemented");
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, Market> getThin() {
		Map<Integer, Market> markets = null;		
		try {
			markets = (Map<Integer, Market>) HazelCastClientFactory.getClient().getMap(Constants.MAP_MARKETS_THIN);
		} catch (Exception e) {
			logger.error("ERROR!", e);
		}
		return markets;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, Market> getUnderlyingAssets() {
		Map<Integer, Market> markets = null;		
		try {
			markets = (Map<Integer, Market>) HazelCastClientFactory.getClient().getMap(Constants.MAP_MARKET_UNDERLYING_ASSETS);
		} catch (Exception e) {
			logger.error("ERROR!", e);
		}
		return markets;
	}
}
