package capital.any.dao.base.market;

import java.util.HashMap;
import java.util.Map;

import capital.any.model.base.Market;

/**
 * @author eranl
 *
 */
public interface IMarketDao {
	
	public static final String GET_MARKETS_LIST = 
			" SELECT " +
			"	* " + 
			" FROM " + 
			"	markets m " +
			" WHERE " +
			"	m.is_active = 1 ";
	
	public static final String GET_MARKETS_WITH_LAST_PRICE_LIST = 
			" SELECT " +
			"	* " + 
			" FROM "	+ 
			"	markets m, " +
			" 	market_prices mp " +
			" WHERE " +
			" 	m.id = mp.market_id " +
			"   AND m.is_active = 1 " +
			" ORDER BY " +
			"	m.id, " +
			"	mp.price_date ";
		//	"	AND mp.type_id = :typeId ";
		
	Map<Integer, Market> getThin();
	
	HashMap<String, Market> getMarketsHMByFeedName();
	
	/**
	 * get all markets with their last price
	 * @return hashmap<marketId, market>
	 */
	Map<Integer, Market> getMarketsWithLastPrice();
	
	Map<Integer, Market> getUnderlyingAssets();

}
