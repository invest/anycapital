package capital.any.dao.base.market;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Component;
import capital.any.base.enums.MarketPriceTypeEnum;
import capital.any.dao.base.market.MarketMapper;
import capital.any.dao.base.marketPrice.MarketPriceMapper;
import capital.any.model.base.Market;
import capital.any.model.base.MarketPrice;

@Component
@Scope("prototype")
public class MarketCallbackHandler implements RowCallbackHandler {
	
	private Map<Integer, Market> result = new HashMap<Integer, Market>();
	
	@Autowired
	private MarketMapper marketmapper;
	@Autowired
	private MarketPriceMapper marketpricemapper;

	@Override
	public void processRow(ResultSet rs) throws SQLException {		
		Market tempMarket = marketmapper.mapRow(rs);
		Market market = result.get(tempMarket.getId());
		if (market == null) {
			market = tempMarket; 
		}
		MarketPrice marketPrice = marketpricemapper.mapRow(rs);
		if (marketPrice.getTypeId() == MarketPriceTypeEnum.LAST_PRICE.getId()) {
			MarketPrice lastMarketPrice = new MarketPrice();
			lastMarketPrice.setPrice(marketPrice.getPrice());
			market.setLastPrice(lastMarketPrice);
		} else {			
			List<MarketPrice> historyMarketPricesList = market.getHistoryMarketPrice();
			if (historyMarketPricesList == null) {
				historyMarketPricesList = new ArrayList<MarketPrice>();
			}
			historyMarketPricesList.add(marketPrice);
			market.setHistoryMarketPrice(historyMarketPricesList);
		}
		result.put(market.getId(), market);
	}

	public Map<Integer, Market> getResult() {
		return result;
	}

	public void setResult(Map<Integer, Market> result) {
		this.result = result;
	}
	
	

}
