package capital.any.dao.base.productBarrierType;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.base.ProductBarrierType;

/**
 * @author Eyal G
 *
 */
@Component
public class ProductBarrierTypeMapper implements RowMapper<ProductBarrierType> {
	
	@Override
	public ProductBarrierType mapRow(ResultSet rs, int row) throws SQLException {
		ProductBarrierType productBarrierType = new ProductBarrierType();
		productBarrierType.setId(rs.getInt("id"));
		productBarrierType.setName(rs.getString("name"));
		productBarrierType.setDisplayName(rs.getString("display_name"));		
		return productBarrierType;
	}
	
	public ProductBarrierType mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
			
}
	

