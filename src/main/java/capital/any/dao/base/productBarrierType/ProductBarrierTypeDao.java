package capital.any.dao.base.productBarrierType;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import capital.any.model.base.ProductBarrierType;

/**
 * @author Eyal G
 *
 */
@Primary
@Repository
public class ProductBarrierTypeDao implements IProductBarrierTypeDao {
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private ProductBarrierTypeMapper productBarrierTypeMapper;
	
	@Override
	public Map<Integer, ProductBarrierType> get() {
		List<ProductBarrierType> tmp = jdbcTemplate.query(GET_PRODUCT_BARRIER_TYPE, productBarrierTypeMapper);
		Map<Integer, ProductBarrierType> hm = tmp.stream().collect(Collectors.toMap(p-> p.getId(), p -> p));		 		 
		return hm;
	}
	
}
