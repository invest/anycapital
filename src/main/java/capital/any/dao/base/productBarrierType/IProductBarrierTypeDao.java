package capital.any.dao.base.productBarrierType;

import java.util.Map;
import capital.any.model.base.ProductBarrierType;

/**
 * @author Eyal G
 *
 */
public interface IProductBarrierTypeDao {
	
	public static final String GET_PRODUCT_BARRIER_TYPE = 
			" SELECT " +
			"	* " + 
			" FROM "	+ 
			"	product_barrier_types ";
		
	Map<Integer, ProductBarrierType> get();
}
