package capital.any.dao.base.productBarrierType;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.ProductBarrierType;

/**
 * @author LioR SoLoMoN
 *
 */
@Repository("ProductBarrierTypeHCDao")
public class ProductBarrierTypeHCDao implements IProductBarrierTypeDao {
	private static final Logger logger = LoggerFactory.getLogger(ProductBarrierTypeHCDao.class);
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, ProductBarrierType> get() {
		Map<Integer, ProductBarrierType> productBarrierTypes = null;		
		try {
			productBarrierTypes = (Map<Integer, ProductBarrierType>) HazelCastClientFactory.getClient().getMap(Constants.MAP_PRODUCT_BARRIER_TYPE);
		} catch (Exception e) {
			logger.error("ERROR!", e);
		}
		return productBarrierTypes;
	}	
}
