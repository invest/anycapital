//package capital.any.dao.base.productScenario;
//
//import java.util.HashMap;
//import java.util.List;
//
//import org.springframework.beans.BeansException;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.ApplicationContextAware;
//import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
//import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
//import org.springframework.jdbc.core.namedparam.SqlParameterSource;
//import org.springframework.stereotype.Repository;
//
//import capital.any.model.base.ProductScenario;
//
///**
// * @author eranl
// *
// */
//@Repository
//public class ProductScenarioDao implements IProductScenarioDao, ApplicationContextAware {
//	
//	private ApplicationContext appContext;
//	
//	@Autowired
//	private NamedParameterJdbcTemplate jdbcTemplate;
//	@Autowired
//	private ProductScenarioMapper productScenarioMapper;
//
//	@Override
//	public HashMap<Long, List<ProductScenario>> getProductScenarios() {
//		ProductScenarioCallbackHandler callbackHandler = appContext.getBean(ProductScenarioCallbackHandler.class);
//		jdbcTemplate.query(GET_PRODUCT_SCENARIOS_LIST, callbackHandler);
//		HashMap<Long, List<ProductScenario>> hm = callbackHandler.getResult();
//		return hm;
//	}
//
//	@Override
//	public List<ProductScenario> getProductScenarios(long productId) {
//		SqlParameterSource namedParameters = 
//				new MapSqlParameterSource("productId", productId);
//		return jdbcTemplate.query(GET_PRODUCT_SCENARIOS_LIST_BY_PRODUCT,
//				namedParameters, productScenarioMapper);
//	}
//	
//	@Override
//	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
//		this.appContext = applicationContext;		
//	}
//	
//}
