//package capital.any.dao.base.productScenario;
//
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Scope;
//import org.springframework.jdbc.core.RowCallbackHandler;
//import org.springframework.stereotype.Component;
//
//import capital.any.model.base.ProductScenario;
//
///**
// * @author eranl
// *
// */
//@Component
//@Scope("request")
//public class ProductScenarioCallbackHandler implements RowCallbackHandler {
//	
//	private HashMap<Long, List<ProductScenario>> result = new HashMap<Long, List<ProductScenario>>();
//	@Autowired
//	private ProductScenarioMapper productScenarioMapper;
//	
//	@Override
//	public void processRow(ResultSet rs) throws SQLException {
//		ProductScenario productScenario = productScenarioMapper.mapRow(rs);		
//		List<ProductScenario> productScenarios = result.get(productScenario.getProductId());
//		if (productScenarios == null) {
//			productScenarios = new ArrayList<ProductScenario>();		
//		}
//		productScenarios.add(productScenario);
//		result.put(productScenario.getProductId(), productScenarios);		
//	}
//
//	/**
//	 * @return the result
//	 */
//	public HashMap<Long, List<ProductScenario>> getResult() {
//		return result;
//	}
//
//	/**
//	 * @param result the result to set
//	 */
//	public void setResult(HashMap<Long, List<ProductScenario>> result) {
//		this.result = result;
//	}
//	
//}
