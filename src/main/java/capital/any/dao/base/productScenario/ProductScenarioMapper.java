//package capital.any.dao.base.productScenario;
//
//import java.sql.ResultSet;
//import java.sql.SQLException;
//
//import org.springframework.jdbc.core.RowMapper;
//import org.springframework.stereotype.Component;
//
//import capital.any.model.base.ProductScenario;
//
///**
// * @author Eran
// *
// */
//@Component
//public class ProductScenarioMapper implements RowMapper<ProductScenario> {
//	
//	@Override
//	public ProductScenario mapRow(ResultSet rs, int row) throws SQLException {
//		ProductScenario productScenario = new ProductScenario();
//		productScenario.setProductId(rs.getLong("product_id"));
//		productScenario.setText(rs.getString("text"));
//		productScenario.setPosition(rs.getInt("position"));
//		productScenario.setFinalFixingLevel(rs.getDouble("final_fixing_level"));
//		return productScenario;
//	}
//	
//	public ProductScenario mapRow(ResultSet rs) throws SQLException {
//		return this.mapRow(rs, 0);
//	}
//			
//}
//	
//
