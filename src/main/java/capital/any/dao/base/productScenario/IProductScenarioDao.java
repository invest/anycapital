//package capital.any.dao.base.productScenario;
//
//import java.util.HashMap;
//import java.util.List;
//
//import capital.any.model.base.ProductScenario;
//
///**
// * @author eranl
// *
// */
//public interface IProductScenarioDao {
//	
//	public static final String GET_PRODUCT_SCENARIOS_LIST = 
//			" SELECT " +
//			"	* " + 
//			" FROM "	+ 
//			"	product_scenarios ";
//	
//	public static final String GET_PRODUCT_SCENARIOS_LIST_BY_PRODUCT = 
//			" SELECT " +
//			"	* " + 
//			" FROM "	+ 
//			"	product_scenarios ps " +
//			" WHERE " +
//			"	ps.product_id = :productId";
//	
//	HashMap<Long, List<ProductScenario>> getProductScenarios();
//	
//	List<ProductScenario> getProductScenarios(long productId);
//}
