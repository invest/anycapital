package capital.any.dao.base.userToken;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import capital.any.model.base.UserToken;

/**
 * @author LioR SoLoMoN
 *
 */
@Repository
public class UserTokenDao implements IUserTokenDao {
	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	@Autowired
	private UserTokenMapper userTokenMapper;
	
	@Override
	public boolean insert(UserToken userToken) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("token", userToken.getToken())
				.addValue("userId", userToken.getUserId());	
		return (namedParameterJdbcTemplate.update(INSERT, namedParameters) > 0);
	}

	@Override
	public UserToken get(UserToken userToken) {
		UserToken userTokenResult = null;
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("token", userToken.getToken());
		try {
			userTokenResult = namedParameterJdbcTemplate
					.queryForObject(GET, namedParameters, userTokenMapper); 
		} catch (EmptyResultDataAccessException e) {
			userTokenResult = null;
		}
		return userTokenResult;
	}
}
