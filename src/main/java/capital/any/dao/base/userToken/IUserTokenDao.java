package capital.any.dao.base.userToken;

import capital.any.base.enums.DBParameter;
import capital.any.model.base.UserToken;

/**
 * @author LioR SoLoMoN
 *
 */
public interface IUserTokenDao {
	
	public static final String INSERT = 
			"INSERT " +
		    "INTO user_tokens " +
			" ( " +
		    "	user_id, " +
		    "   token " + 
		    " ) " +
		    "VALUES " +
		    " ( " +
		    "    :userId, " +
		    "    :token " +
		    " ) ";
	
	public static final String GET = 
			"SELECT " +
		    " * " +
			"FROM " +
		    "	user_tokens ut " +
		    "	,db_parameters dp " + 
		    "WHERE " +
		    "	ut.token = :token " +
		    "	AND dp.id = " + DBParameter.USER_TOKENS_EXPIRED.getId() + 
		    "	AND time_created >= DATE_SUB(NOW(), INTERVAL dp.num_value HOUR) ";
		
	/**
	 * @param userToken
	 * @return
	 */
	boolean insert(UserToken userToken);
	
	/**
	 * @param userToken with token only.
	 * @return
	 */
	UserToken get(UserToken userToken);
	
}
