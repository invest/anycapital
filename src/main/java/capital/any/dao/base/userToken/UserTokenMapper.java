package capital.any.dao.base.userToken;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import capital.any.dao.base.MapperBase;
import capital.any.model.base.UserToken;

/**
 * @author LioR SoLoMoN
 *
 */
@Component
public class UserTokenMapper extends MapperBase implements RowMapper<UserToken> {
			
	@Override
	public UserToken mapRow(ResultSet rs, int row) throws SQLException {
		UserToken userToken = new UserToken(
				rs.getString("token"),
				rs.getLong("user_id"),
				convertTimestampToDate(rs.getTimestamp("time_created")));
		return userToken;		
	}
	
	public UserToken mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
}
