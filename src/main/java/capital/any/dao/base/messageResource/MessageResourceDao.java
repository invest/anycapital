package capital.any.dao.base.messageResource;

import java.util.List;
import java.util.Map;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import capital.any.model.base.MsgRes;
import capital.any.model.base.MsgResLanguage;

/**
 * @author eranl
 *
 */
@Primary
@Repository
public class MessageResourceDao implements IMessageResourceDao, ApplicationContextAware {
	
	private ApplicationContext appContext;
	
	@Autowired
	protected NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Override
	public Map<Integer, Map<Integer, Map<String, String>>> get() {
		MessageResourceCallbackHandler callbackHandler = appContext.getBean(MessageResourceCallbackHandler.class);
		namedParameterJdbcTemplate.query(GET, callbackHandler);
		return callbackHandler.getResult();
	}
		
	@Override
	public Map<Integer, Map<Integer, Map<String, String>>> get(MsgRes msgRes) {
		MessageResourceCallbackHandler callbackHandler = appContext.getBean(MessageResourceCallbackHandler.class);
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("typeId", msgRes.getTypeId());	
		namedParameterJdbcTemplate.query(GET_TYPE, namedParameters, callbackHandler);
		return callbackHandler.getResult();
	}
			
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.appContext = applicationContext;		
	}

	@Override
	public void insertMsgRes(MsgRes msgRes) {
		KeyHolder keyHolder = new GeneratedKeyHolder();				
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("key", msgRes.getKey())
								.addValue("actionSourceId", msgRes.getActionSourceId())
								.addValue("typeId", msgRes.getTypeId());		
		namedParameterJdbcTemplate.update(INSERT_MSG_RES, namedParameters, keyHolder, new String[] {"id"});	
		msgRes.setId(keyHolder.getKey().intValue());
	}

	@Override
	public void insertMsgResLanguage(MsgResLanguage msgResLanguage) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("msgResId", msgResLanguage.getMsgRes().getId())
				.addValue("msgResLanguageId", msgResLanguage.getMsgResLanguageId())					
				.addValue("value", msgResLanguage.getValue())
				.addValue("writerId", msgResLanguage.getWriterId())				
				.addValue("largeValue", msgResLanguage.getLargeValue());						
		namedParameterJdbcTemplate.update(INSERT_MSG_RES_LANGUAGE, namedParameters);
	}

	@Override
	public void insertMsgResLanguage(List<MsgResLanguage> msgResLanguages) {
		SqlParameterSource[] batch = 
				SqlParameterSourceUtils.createBatch(msgResLanguages.toArray());
		namedParameterJdbcTemplate.batchUpdate(INSERT_MSG_RES_LANGUAGE,
												batch);
		
	}

}
