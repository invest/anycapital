package capital.any.dao.base.messageResource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.hazelcast.update.base.message.resource.UpdateMessageResourceEntry;
import capital.any.model.base.MsgRes;
import capital.any.model.base.MsgResLanguage;

/**
 * @author LioR SoLoMoN
 *
 */
@Repository("MessageResourceHCDao")
public class MessageResourceHCDao implements IMessageResourceDao {
	private static final Logger logger = LoggerFactory.getLogger(MessageResourceHCDao.class);

	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, Map<Integer, Map<String, String>>> get() {
		Map<Integer, Map<Integer, Map<String, String>>> map = null;		
		try {
			map = (Map<Integer, Map<Integer, Map<String, String>>>) HazelCastClientFactory.getClient().getMap(Constants.MAP_MESSAGE_RESOURCE);
		} catch (Exception e) {
			logger.error("ERROR!", e);
		}
		return map;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, Map<Integer, Map<String, String>>> get(MsgRes msgRes) {
		Map<Integer, Map<Integer, Map<String, String>>> result = null;
		try {
			result = (Map<Integer, Map<Integer, Map<String, String>>>) 
					HazelCastClientFactory.getClient().getMap(Constants.MAP_MESSAGE_RESOURCE);
		} catch (Exception e) {
			logger.warn("cant get MsgRes from hazelcast", e);
		}
		return result;
	}

	@Override
	public void insertMsgRes(MsgRes msgRes) {
		logger.info("hazelcast insertMsgRes not implemented");
	}

	@Override
	public void insertMsgResLanguage(MsgResLanguage msgResLanguage) {
		logger.info("hazelcast insertMsgResLanguage not implemented");
	}

	@Override
	public void insertMsgResLanguage(List<MsgResLanguage> msgResLanguages) throws Exception {
		HazelCastClientFactory.getClient().updateTransactionMap(
				msgResLanguages.get(0).getMsgRes().getActionSourceId(),
				new UpdateMessageResourceEntry(msgResLanguages), 
				Constants.MAP_MESSAGE_RESOURCE);
	}
}
