package capital.any.dao.base.messageResource;

import java.util.List;
import java.util.Map;
import capital.any.model.base.MsgRes;
import capital.any.model.base.MsgResLanguage;

/**
 * @author eranl
 *
 */
public interface IMessageResourceDao {
	
	public static final String GET =
			"SELECT " +
			"	mr.* " +
			"	,mrl.* " +
			 "  ,mrl.msg_res_language_id language_id " +
			"	,mrt.* " + 	
			"	,mrt.id as msg_res_type_id " +
			"FROM " +
			"	msg_res mr " +
			"	,msg_res_language mrl " +
			"	,msg_res_type mrt " +
			"WHERE " +
			"	mr.id = mrl.msg_res_id " +
			"	and mr.type_id = mrt.id ";
	
	public static final String GET_TYPE =
			"SELECT " +
			"	mr.* " +
			"	,mrl.* " +
			 "  ,mrl.msg_res_language_id language_id " +
			"	,mrt.* " + 	
			"	,mrt.id as msg_res_type_id " +
			"FROM " +
			"	msg_res mr " +
			"	,msg_res_language mrl " +
			"	,msg_res_type mrt " +
			"WHERE " +
			"	mr.id = mrl.msg_res_id " +
			"	and mr.type_id = :typeId ";
	
	public static final String GET_MSG_RES_LIST = 
			" SELECT " +
            "	mr.key, " +
            "	mr.type_id, " +
            "   mrla.value, " +
            "   mrla.large_value, " +
            "   mrla.msg_res_language_id language_id " +
			" FROM "	+ 
			"	msg_res mr, " +
			"	msg_res_language mrla " +
			" WHERE	" +
			"	mr.id = mrla.msg_res_id " +
			"	AND action_source_id = :actionSourceId " +
			"	AND mr.type_id = :typeId ";
	
	public static final String INSERT_MSG_RES =
			" INSERT " +
		    " INTO msg_res " +
		    " ( " +
		    "   `key`, " +
		    " 	action_source_id, " +
		    " 	type_id " +		    
		    " ) " +
		    " VALUES " +
		    " ( " +
		    "   :key, " +
		    " 	:actionSourceId, " +
		    "	:typeId " +
		    " ) ";
	
	public static final String INSERT_MSG_RES_LANGUAGE =
			" INSERT " +
		    " INTO msg_res_language " +
		    " ( " +
		    "	msg_res_id, " +
		    "	msg_res_language_id, " +
		    "	value, " +
		    " 	large_value, " +
		    " 	writer_id " +
		    " ) " +
		    " VALUES " +
		    " ( " +
		    "   :msgResId, " +
		    "   :msgResLanguageId, " +
		    "   :value, " +
		    " 	:largeValue, " +
		    "   :writerId " +
		    " ) ";	
					
	/**
	 * @return cartesian product - message resourceses tables  
	 */
	Map<Integer, Map<Integer, Map<String, String>>> get();
	
	/**
	 * @param msgRes
	 * @return
	 */
	Map<Integer, Map<Integer, Map<String, String>>> get(MsgRes msgRes);
	
	/**
	 * @param msgRes
	 */
	void insertMsgRes(MsgRes msgRes);
	
	/**
	 * @param msgResLanguage
	 */
	void insertMsgResLanguage(MsgResLanguage msgResLanguage);
	
	/**
	 * @param msgResLanguages
	 * @throws Exception 
	 */
	void insertMsgResLanguage(List<MsgResLanguage> msgResLanguages) throws Exception;
			
}
