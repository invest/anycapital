package capital.any.dao.base.gbg.users;

import java.util.Map;
import capital.any.model.base.GBGUser;

/**
 * @author LioR SoLoMoN
 *
 */
public interface IGBGUsersDao {
	

	public static final String INSERT = 
			"INSERT INTO " +
			"	gbg_users " +
			"	( " +
			"		user_id " +
			"		,score " +
			"		,request " +
			"		,response " +
			"		,time_created " +
			"		,time_updated " +
			"	) " +
			"VALUES " +
			"	(	" +
			"		:userId " +
			"		,:score " +
			"		,:request " +
			"		,:response " +
			"		,:timeCreated " +
			"		,:timeUpdated " +
			"	)	";
	
	//FIXME maybe
	public static final String UPDATE = 
			"UPDATE " +
			"	gbg_users " +
			"SET " +
			"	score = :score " +
			"	,response = :response " +
			"	,time_updated = :timeUpdated " +
			"WHERE " +
			"	user_id = :userId ";
	
	public static final String GET = 
			"SELECT " +
			"	* " + 
			"FROM "	+ 
			"	gbg_users ";
	
	public static final String GET_BY_USER_ID = 
			"SELECT " +
			"	* " + 
			"FROM "	+ 
			"	gbg_users gu " + 
			"WHERE " +
			"	gu.user_id = :userId";
	
	/**
	 * @param gbgUser
	 * @return
	 */
	boolean insert(GBGUser gbgUser);
	
	/**
	 * @param gbgUser
	 * @return
	 */
	boolean update(GBGUser gbgUser);
	
	/**
	 * @return
	 */
	Map<Long, GBGUser> get();
	
	/**
	 * @param userId
	 * @return
	 */
	GBGUser get(long userId);
	
}
