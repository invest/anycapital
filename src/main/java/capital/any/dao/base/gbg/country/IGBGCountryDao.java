package capital.any.dao.base.gbg.country;

import java.util.Map;
import capital.any.model.base.GBGCountry;

/**
 * @author LioR SoLoMoN
 *
 */
public interface IGBGCountryDao {
	
	public static final String GET = 
			" SELECT " +
			"	* " + 
			" FROM "	+ 
			"	gbg_countries ";
	
	public static final String GET_BY_COUNTRY = 
			"SELECT " +
			"	* " + 
			"FROM "	+ 
			"	gbg_countries gc " + 
			"WHERE " +
			"	gc.country_id = :countryId";
	
	/**
	 * @return
	 */
	Map<Integer, GBGCountry> get();
	
	/**
	 * @param countryId
	 * @return
	 */
	GBGCountry get(int countryId);
	
}
