package capital.any.dao.base.gbg.users;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import capital.any.dao.base.MapperBase;
import capital.any.model.base.GBGUser;

/**
 * @author LioR SoLoMoN
 *
 */
@Component
public class GBGUsersMapper extends MapperBase implements RowMapper<GBGUser> {
	
	@Override
	public GBGUser mapRow(ResultSet rs, int row) throws SQLException {
		GBGUser gbgUser = new GBGUser(
				rs.getLong("user_id"),
				rs.getInt("score"),
				rs.getString("request"),
				rs.getString("response"),
				convertTimestampToDate(rs.getTimestamp("time_created")),
				convertTimestampToDate(rs.getTimestamp("time_updated")));
		return gbgUser;
	}
	
	public GBGUser mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}			
}