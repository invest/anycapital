package capital.any.dao.base.gbg.country;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import capital.any.model.base.GBGCountry;

/**
 * @author LioR SoLoMoN
 *
 */
@Primary
@Repository
public class GBGCountryDao implements IGBGCountryDao {
	private static final Logger logger = LoggerFactory.getLogger(GBGCountryDao.class);
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private GBGCountryMapper gbgCountryMapper;
	
	@Override
	public Map<Integer, GBGCountry> get() {	
		List<GBGCountry> countries = jdbcTemplate.query(GET, new BeanPropertyRowMapper<GBGCountry>(GBGCountry.class));
		return countries.stream().collect(Collectors.toMap(p-> p.getCountryId(), p -> p));
	}

	@Override
	public GBGCountry get(int countryId) {
		GBGCountry gbgCountry = null;
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("countryId", countryId);
		try {
			gbgCountry = jdbcTemplate.queryForObject(GET_BY_COUNTRY, params, gbgCountryMapper);
		} catch (DataAccessException e) {
			logger.error("can't find countryId: " + countryId);
		}

		return gbgCountry;
	}
}
