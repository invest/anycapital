package capital.any.dao.base.gbg.score;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import capital.any.model.base.GBGUser;

/**
 * @author LioR SoLoMoN
 *
 */
@Primary
@Repository
public class GBGScoreDao implements IGBGScoreDao {
	private static final Logger logger = LoggerFactory.getLogger(GBGScoreDao.class);
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	@Override
	public Map<Long, GBGUser> get() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public int getStatus(int score) {
		MapSqlParameterSource parameters =	new MapSqlParameterSource();
		parameters.addValue("score", score);
		return jdbcTemplate.query(GET_STATUS, parameters, new ResultSetExtractor<Integer>() {
			@Override
			public Integer extractData(ResultSet rs) throws SQLException  {
				int statusId = 0;
				if(rs.next()){
					statusId = rs.getInt("gbg_status_id");
				}
				return statusId;
			}
		});
	}
}
