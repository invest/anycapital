package capital.any.dao.base.gbg.country;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import capital.any.model.base.GBGCountry;

/**
 * @author LioR SoLoMoN
 *
 */
@Component
public class GBGCountryMapper implements RowMapper<GBGCountry> {
	
	@Override
	public GBGCountry mapRow(ResultSet rs, int row) throws SQLException {
		GBGCountry gbgCountry = new GBGCountry(rs.getInt("country_id"), rs.getString("profile_id"));
		return gbgCountry;
	}
	
	public GBGCountry mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}			
}