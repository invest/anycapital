package capital.any.dao.base.gbg.users;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import capital.any.model.base.GBGUser;

/**
 * @author LioR SoLoMoN
 *
 */
@Primary
@Repository
public class GBGUsersDao implements IGBGUsersDao {
	private static final Logger logger = LoggerFactory.getLogger(GBGUsersDao.class);
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private GBGUsersMapper gbgUserMapper;
	
	@Override
	public boolean insert(GBGUser gbgUser) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource
				("userId", gbgUser.getUserId())
				.addValue("score", gbgUser.getScore())
				.addValue("request", gbgUser.getRequest())
				.addValue("response", gbgUser.getResponse())
				.addValue("timeCreated", gbgUser.getTimeCreated())
				.addValue("timeUpdated", gbgUser.getTimeUpdated())
				;
		return (jdbcTemplate.update(INSERT, namedParameters, keyHolder, new String[] {"id"}) > 0);
	}

	@Override
	public boolean update(GBGUser gbgUser) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("userId", gbgUser.getUserId())
				.addValue("score", gbgUser.getScore())
				.addValue("response", gbgUser.getResponse())
				.addValue("timeUpdated", gbgUser.getTimeUpdated());
				return (jdbcTemplate.update(UPDATE, namedParameters) > 0);	
	}
	
	@Override
	public Map<Long, GBGUser> get() {	
		List<GBGUser> gbgUsers = jdbcTemplate.query(GET, new BeanPropertyRowMapper<GBGUser>(GBGUser.class));
		return gbgUsers.stream().collect(Collectors.toMap(p-> p.getUserId(), p -> p));		 		 
	}

	@Override
	public GBGUser get(long userId) {
		GBGUser gbgUser = null;
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("userId", userId);
		try {
			gbgUser = jdbcTemplate.queryForObject(GET_BY_USER_ID, params, gbgUserMapper);
		} catch (DataAccessException e) {
			logger.error("can't find by userId: " + userId);
		}

		return gbgUser;
	}
}
