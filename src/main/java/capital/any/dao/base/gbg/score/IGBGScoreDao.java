package capital.any.dao.base.gbg.score;

import java.util.Map;
import capital.any.model.base.GBGUser;

/**
 * @author LioR SoLoMoN
 *
 */
public interface IGBGScoreDao {

	
	public static final String GET = 
			"SELECT " +
			"	* " + 
			"FROM "	+ 
			"	gbg_users ";
	
	public static final String GET_STATUS = 
			"SELECT " +
			"	gs.gbg_status_id " + 
			"FROM "	+ 
			"	gbg_score gs " + 
			"WHERE " +
			"	gs.from <= :score AND gs.to >= :score";
	
	/**
	 * @return
	 */
	Map<Long, GBGUser> get();
	
	/**
	 * @return
	 */
	int getStatus(int score);
	
}
