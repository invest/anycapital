package capital.any.dao.base.userHistory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.base.UserHistory;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository
public class UserHistoryDao implements IUserHistoryDao {
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	@Override
	public void insert(UserHistory userHistory) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource
						 ("userId", userHistory.getUserId())
				.addValue("userHistoryActionId", userHistory.getUserHistoryAction().getId())
				.addValue("actionParamters", userHistory.getActionParamters())
				.addValue("actionSourceId", userHistory.getActionSource().getId())
				.addValue("writerId", userHistory.getWriter().getId())
				.addValue("serverName", userHistory.getServerName());
		jdbcTemplate.update(INSERT, namedParameters);
	}
}
