package capital.any.dao.base.userHistory;

import capital.any.model.base.UserHistory;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IUserHistoryDao {
	
	public static final String INSERT = 
			" INSERT " +
			" INTO users_history " +
			"  ( " +
			"    user_id , " +
			"    users_history_action_id , " +
			"    action_paramters , " +
			"    action_source_id , " +
			"    writer_id , " +
			"    server_name " +
			" ) " +
			" VALUES " +
			" ( " +
			"    :userId , " +
			"    :userHistoryActionId , " +
			"    :actionParamters , " +
			"    :actionSourceId , " +
			"    :writerId , " +
			"    :serverName " +
			" ) ";

	void insert(UserHistory userHistory);
	
	
}
