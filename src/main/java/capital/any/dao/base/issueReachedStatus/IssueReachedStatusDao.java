package capital.any.dao.base.issueReachedStatus;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import capital.any.model.base.IssueReachedStatus;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository
public class IssueReachedStatusDao implements IIssueReachedStatusDao {

	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private IssueReachedStatusMapper issueReachedStatusMapper;
	
	@Override
	public Map<Integer, IssueReachedStatus> get() {
		List<IssueReachedStatus> tmp = jdbcTemplate.query(GET_ISSUE_REACHED_STATUS_LIST, issueReachedStatusMapper);
		Map<Integer, IssueReachedStatus> hm = tmp.stream().collect(Collectors.toMap(p-> p.getId(), p -> p));		 		 
		return (HashMap<Integer, IssueReachedStatus>) hm;
	}

}
