package capital.any.dao.base.issueReachedStatus;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.IssueReachedStatus;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository("IssueReachedStatusHCDao")
public class IssueReachedStatusHCDao implements IIssueReachedStatusDao {
	private static final Logger logger = LoggerFactory.getLogger(IssueReachedStatusHCDao.class);
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, IssueReachedStatus> get() {
		Map<Integer, IssueReachedStatus> issueReachedStatuss = null;
		try {
			issueReachedStatuss = (Map<Integer, IssueReachedStatus>) HazelCastClientFactory.getClient().getMap(Constants.MAP_REACHED_STATUSES);
		} catch (Exception e) {
			logger.error("Problem to get issueReachedStatuss by HC.");
		}
		return issueReachedStatuss;
	}

}
