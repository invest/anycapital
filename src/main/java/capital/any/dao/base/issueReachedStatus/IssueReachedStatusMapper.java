package capital.any.dao.base.issueReachedStatus;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.IssueReachedStatus;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
public class IssueReachedStatusMapper extends MapperBase implements RowMapper<IssueReachedStatus> {

	@Override
	public IssueReachedStatus mapRow(ResultSet rs, int rowNum) throws SQLException {
		IssueReachedStatus issueReachedStatus = new IssueReachedStatus();
		issueReachedStatus.setId(rs.getInt("iss_rs_id"));
		issueReachedStatus.setName(rs.getString("iss_rs_name"));
		return issueReachedStatus;
	}
	
	public IssueReachedStatus mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}

}
