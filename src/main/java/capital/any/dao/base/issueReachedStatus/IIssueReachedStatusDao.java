package capital.any.dao.base.issueReachedStatus;

import java.util.Map;

import capital.any.model.base.IssueReachedStatus;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IIssueReachedStatusDao {
	
	public static final String GET_ISSUE_REACHED_STATUS_LIST =
			" SELECT " +
				" iss_rs.id iss_rs_id, " +
				" iss_rs.name iss_rs_name " + 
			" FROM "	+ 
				" issue_reached_statuses iss_rs ";
	
	Map<Integer, IssueReachedStatus> get();
	
}
