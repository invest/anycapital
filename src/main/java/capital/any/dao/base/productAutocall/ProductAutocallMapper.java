package capital.any.dao.base.productAutocall;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.ProductAutocall;

/**
 * @author Eran
 *
 */
@Component
public class ProductAutocallMapper extends MapperBase implements RowMapper<ProductAutocall> {
	
	@Override
	public ProductAutocall mapRow(ResultSet rs, int row) throws SQLException {
		ProductAutocall productAutocall = new ProductAutocall();
		productAutocall.setId(rs.getInt("id"));
		productAutocall.setProductId(rs.getLong("product_id"));
		productAutocall.setExaminationDate(convertTimestampToDate(rs.getTimestamp(("examination_date"))));
		productAutocall.setExaminationValue(rs.getInt("examination_value"));		
		return productAutocall;
	}
	
	public ProductAutocall mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
			
}
	

