package capital.any.dao.base.productAutocall;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Component;

import capital.any.model.base.ProductAutocall;

/**
 * @author eranl
 *
 */
@Component
@Scope("prototype")
public class ProductAutocallCallbackHandler implements RowCallbackHandler {
	
	private HashMap<Long, List<ProductAutocall>> result = new HashMap<Long, List<ProductAutocall>>();
	@Autowired
	private ProductAutocallMapper productAutocallMapper;
	
	@Override
	public void processRow(ResultSet rs) throws SQLException {
		ProductAutocall productAutocall = productAutocallMapper.mapRow(rs);		
		List<ProductAutocall> productAutocalls = result.get(productAutocall.getProductId());
		if (productAutocalls == null) {
			productAutocalls = new ArrayList<ProductAutocall>();		
		}
		productAutocalls.add(productAutocall);
		result.put(productAutocall.getProductId(), productAutocalls);		
	}

	/**
	 * @return the result
	 */
	public HashMap<Long, List<ProductAutocall>> getResult() {
		return result;
	}

	/**
	 * @param result the result to set
	 */
	public void setResult(HashMap<Long, List<ProductAutocall>> result) {
		this.result = result;
	}
	
}
