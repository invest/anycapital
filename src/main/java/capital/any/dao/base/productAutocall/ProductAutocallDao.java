package capital.any.dao.base.productAutocall;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.base.ProductAutocall;

/**
 * @author eranl
 *
 */
@Repository
public class ProductAutocallDao implements IProductAutocallDao, ApplicationContextAware {
	
	private ApplicationContext appContext;
	
	@Autowired
	protected NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private ProductAutocallMapper productAutocallMapper;
	
	@Override
	public HashMap<Long, List<ProductAutocall>> getProductAutocalls() {
		ProductAutocallCallbackHandler callbackHandler = appContext.getBean(ProductAutocallCallbackHandler.class);
		jdbcTemplate.query(GET_PRODUCT_AUTOCALLS_LIST, callbackHandler);
		HashMap<Long, List<ProductAutocall>> hm = callbackHandler.getResult();
		return hm;	
	}

	@Override
	public List<ProductAutocall> getProductAutocalls(long productId) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("productId", productId);
		return jdbcTemplate.query(GET_PRODUCT_AUTOCALLS_LIST_BY_PRODUCT,
				namedParameters, productAutocallMapper);
	}	
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.appContext = applicationContext;		
	}
}
