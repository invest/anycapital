package capital.any.dao.base.marketGroup;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.base.MarketGroup;

/**
 * @author Eran
 *
 */
@Component
public class MarketGroupMapper implements RowMapper<MarketGroup> {
	
	@Override
	public MarketGroup mapRow(ResultSet rs, int row) throws SQLException {
		MarketGroup marketGroup = new MarketGroup();
		marketGroup.setId(rs.getInt("id"));
		marketGroup.setName(rs.getString("name"));
		marketGroup.setDisplayName(rs.getString("display_name"));	
		return marketGroup;
	}
	
	public MarketGroup mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
			
}
	

