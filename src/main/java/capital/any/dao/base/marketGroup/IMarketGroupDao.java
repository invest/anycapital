package capital.any.dao.base.marketGroup;

import java.util.Map;
import capital.any.model.base.MarketGroup;

/**
 * @author eranl
 *
 */
public interface IMarketGroupDao {
	
	public static final String GET_MARKET_GROUP_LIST = 
			" SELECT " +
			"	* " + 
			" FROM "	+ 
			"	market_groups ";
		
	Map<Integer, MarketGroup> get();

}
