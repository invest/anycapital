package capital.any.dao.base.marketGroup;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.MarketGroup;

/**
 * @author LioR SoLoMoN
 *
 */
@Repository("MarketGroupHCDao")
public class MarketGroupHCDao implements IMarketGroupDao {
	private static final Logger logger = LoggerFactory.getLogger(MarketGroupHCDao.class);
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, MarketGroup> get() {
		Map<Integer, MarketGroup> marketGroups = null;		
		try {
			marketGroups = (Map<Integer, MarketGroup>) HazelCastClientFactory.getClient().getMap(Constants.MAP_MARKET_GROUPS);
		} catch (Exception e) {
			logger.error("ERROR!", e);
		}
		return marketGroups;
	}
}
