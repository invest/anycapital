package capital.any.dao.base.marketGroup;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import capital.any.model.base.MarketGroup;

/**
 * @author eranl
 *
 */
@Primary
@Repository
public class MarketGroupDao implements IMarketGroupDao {
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private MarketGroupMapper marketGroupMapper;
	
	@Override
	public Map<Integer, MarketGroup> get() {
		List<MarketGroup> tmp = jdbcTemplate.query(GET_MARKET_GROUP_LIST, marketGroupMapper);
		Map<Integer, MarketGroup> hm = tmp.stream().collect(Collectors.toMap(p-> p.getId(), p -> p));		 		 
		return hm;
	}
}
