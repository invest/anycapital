package capital.any.dao.base.productHistory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.base.ProductHistory;

/**
 * @author Eyal G
 *
 */
@Primary
@Repository
public class ProductHistoryDao implements IProductHistoryDao {
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;

	@Override
	public void insert(ProductHistory productHistory) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource
						 ("actionParamters", productHistory.getActionParamters())
				.addValue("actionSourceId", productHistory.getActionSource().getId())					
				.addValue("writerId", productHistory.getWriterId() == 0 ? null : productHistory.getWriterId())
				.addValue("serverName", productHistory.getServerName())
				.addValue("productId", productHistory.getProductId())
				.addValue("productsHistoryActionId", productHistory.getProductHistoryAction().getId());
		
		jdbcTemplate.update(INSERT, namedParameters);
	}
	
}
