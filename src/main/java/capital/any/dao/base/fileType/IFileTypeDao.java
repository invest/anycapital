package capital.any.dao.base.fileType;

import java.util.Map;

import capital.any.model.base.FileType;

/**
 * @author Eyal G
 *
 */
public interface IFileTypeDao {
	
	public static final String GET = 
			" SELECT " +
			"	* " + 
			" FROM "	+ 
			"	file_types ";
		
	/**
	 * get all user file types
	 * @return Map<Integer, FileType>
	 */
	Map<Integer, FileType> get();
}
