package capital.any.dao.base.fileType;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.FileType;

/**
 * @author Eyal G
 *
 */
@Repository("FileTypeHCDao")
public class FileTypeHCDao implements IFileTypeDao {
	private static final Logger logger = LoggerFactory.getLogger(FileTypeHCDao.class);
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, FileType> get() {
		Map<Integer, FileType> fileTypes = null;		
		try {
			fileTypes = (Map<Integer, FileType>) HazelCastClientFactory.getClient().getMap(Constants.MAP_FILE_TYPE);
		} catch (Exception e) {
			logger.error("ERROR!", e);
		}
		return fileTypes;
	}	
}
