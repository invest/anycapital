package capital.any.dao.base.fileType;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.base.FileType;

/**
 * @author Eyal G
 *
 */
@Component
public class FileTypeMapper implements RowMapper<FileType> {
	
	@Override
	public FileType mapRow(ResultSet rs, int row) throws SQLException {
		FileType fileType = new FileType();
		fileType.setId(rs.getInt("id"));
		fileType.setName(rs.getString("name"));
		fileType.setDisplayName(rs.getString("display_name"));		
		return fileType;
	}
	
	public FileType mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
}
	

