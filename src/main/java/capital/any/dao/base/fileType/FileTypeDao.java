package capital.any.dao.base.fileType;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import capital.any.model.base.FileType;

/**
 * @author Eyal G
 *
 */
@Primary
@Repository
public class FileTypeDao implements IFileTypeDao {
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private FileTypeMapper fileTypeMapper;
	
	@Override
	public Map<Integer, FileType> get() {
		List<FileType> tmp = jdbcTemplate.query(GET, fileTypeMapper);
		Map<Integer, FileType> hm = tmp.stream().collect(Collectors.toMap(p-> p.getId(), p -> p));		 		 
		return hm;
	}
	
}
