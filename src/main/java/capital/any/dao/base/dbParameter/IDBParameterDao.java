package capital.any.dao.base.dbParameter;

import java.util.Map;

import capital.any.model.base.DBParameter;

/**
 * @author eranl
 *
 */
public interface IDBParameterDao {
	
	public static final String GET_DB_PARAMETERS_LIST = 
			" SELECT " +
			"	* " + 
			" FROM "	+ 
			"	db_parameters ";
	
	Map<Integer, DBParameter> get();
		
}
