package capital.any.dao.base.dbParameter;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.base.DBParameter;

/**
 * @author Eran
 *
 */
@Component
public class DBParameterMapper implements RowMapper<DBParameter> {
	
	@Override
	public DBParameter mapRow(ResultSet rs, int row) throws SQLException {
		DBParameter dbParameter = new DBParameter();
		dbParameter.setId(rs.getInt("id"));
		dbParameter.setName(rs.getString("name"));
		dbParameter.setNumValue(rs.getLong("num_value"));
		dbParameter.setStringValue(rs.getString("string_value"));
		dbParameter.setDateValue(rs.getDate("date_value"));
		dbParameter.setComments(rs.getString("comments"));
		return dbParameter;
	}
	
	public DBParameter mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
			
}