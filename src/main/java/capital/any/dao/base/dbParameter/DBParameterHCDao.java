package capital.any.dao.base.dbParameter;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.DBParameter;

/**
 * @author eyal goren
 *
 */
@Repository("DBParameterHCDao")
public class DBParameterHCDao implements IDBParameterDao {
	private static final Logger logger = LoggerFactory.getLogger(DBParameterHCDao.class);
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, DBParameter> get() {		
		Map<Integer, DBParameter> DBParameters = null;		
		try {
			DBParameters = (Map<Integer, DBParameter>)HazelCastClientFactory.getClient().getMap(Constants.MAP_DB_PARAMETER);
		} catch (Exception e) {
			logger.error("ERROR!", e);
		}
		return DBParameters;
	}
}
