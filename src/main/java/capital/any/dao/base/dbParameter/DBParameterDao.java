package capital.any.dao.base.dbParameter;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import capital.any.model.base.DBParameter;

/**
 * @author eranl
 *
 */
@Primary
@Repository
public class DBParameterDao implements IDBParameterDao {
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	@Autowired
	protected NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Override
	public Map<Integer, DBParameter> get() {
		List<DBParameter> tmp = jdbcTemplate.query(GET_DB_PARAMETERS_LIST, new BeanPropertyRowMapper<DBParameter>(DBParameter.class));
		return (Map<Integer, DBParameter>) tmp.stream().collect(Collectors.toMap(p-> p.getId(), p -> p));		 		 
	}

}
