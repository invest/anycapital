//package capital.any.dao.base.productInfo;
//
//import java.sql.ResultSet;
//import java.sql.SQLException;
//
//import org.springframework.jdbc.core.RowMapper;
//import org.springframework.stereotype.Component;
//
//import capital.any.model.base.DayCountConvention;
//import capital.any.model.base.ProductInfo;
//
///**
// * @author Eran
// *
// */
//@Component
//public class ProductInfoMapper implements RowMapper<ProductInfo> {
//	
//	@Override
//	public ProductInfo mapRow(ResultSet rs, int row) throws SQLException {
//		ProductInfo productInfo = new ProductInfo();		
//		productInfo.setProductId(rs.getLong("product_id"));
//		productInfo.setLanguageId(rs.getInt("language_id"));
//		productInfo.setTitle(rs.getString("title"));
//		productInfo.setDescription(rs.getString("description"));
//		return productInfo;
//	}
//	
//	public ProductInfo mapRow(ResultSet rs) throws SQLException {
//		return this.mapRow(rs, 0);
//	}
//			
//}
//	
//
