//package capital.any.dao.base.productInfo;
//
//import java.util.HashMap;
//import java.util.List;
//
//import capital.any.model.base.ProductInfo;
//
///**
// * @author eranl
// *
// */
//public interface IProductInfoDao {
//	
//	public static final String GET_PRODUCT_INFO_LIST = 
//			" SELECT " +
//			"	* " + 
//			" FROM "	+ 
//			"	product_info ";
//	
//	public static final String GET_PRODUCT_INFO_LIST_BY_PRODUCT = 
//			" SELECT " +
//			"	* " + 
//			" FROM "	+ 
//			"	product_info pi " +
//			" WHERE " +
//			"	pi.product_id = :productId";
//		
//	HashMap<Long, HashMap<Integer, ProductInfo>> getProductInfo();
//	
//	List<ProductInfo> getProductInfo(long productId);
//
//}
