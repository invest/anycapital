//package capital.any.dao.base.productInfo;
//
//import java.util.HashMap;
//import java.util.List;
//
//import org.springframework.beans.BeansException;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.ApplicationContextAware;
//import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
//import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
//import org.springframework.jdbc.core.namedparam.SqlParameterSource;
//import org.springframework.stereotype.Repository;
//
//import capital.any.model.base.ProductInfo;
//
///**
// * @author eranl
// *
// */
//@Repository
//public class ProductInfoDao implements IProductInfoDao, ApplicationContextAware {
//	
//	private ApplicationContext appContext;
//	
//	@Autowired
//	private NamedParameterJdbcTemplate jdbcTemplate;
//	@Autowired
//	private ProductInfoMapper productInfoMapper;
//	
//	@Override
//	public HashMap<Long, HashMap<Integer, ProductInfo>> getProductInfo() {
//		ProductInfoCallbackHandler callbackHandler = appContext.getBean(ProductInfoCallbackHandler.class);
//		jdbcTemplate.query(GET_PRODUCT_INFO_LIST, callbackHandler);
//		HashMap<Long, HashMap<Integer, ProductInfo>> hm = callbackHandler.getResult();
//		return hm;		
//	}
//
//	@Override
//	public List<ProductInfo> getProductInfo(long productId) {
//		SqlParameterSource namedParameters = 
//				new MapSqlParameterSource("productId", productId);
//		return jdbcTemplate.query(GET_PRODUCT_INFO_LIST_BY_PRODUCT,
//				namedParameters, productInfoMapper);
//	}
//	
//	@Override
//	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
//		this.appContext = applicationContext;		
//	}
//	
//}
