//package capital.any.dao.base.productInfo;
//
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.HashMap;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Scope;
//import org.springframework.jdbc.core.RowCallbackHandler;
//import org.springframework.stereotype.Component;
//
//import capital.any.model.base.ProductInfo;
//
///**
// * @author eranl
// *
// */
//@Component
//@Scope("request")
//public class ProductInfoCallbackHandler implements RowCallbackHandler {
//	
//	private HashMap<Long, HashMap<Integer, ProductInfo>> result = new HashMap<Long, HashMap<Integer, ProductInfo>>();
//	@Autowired
//	private ProductInfoMapper productInfoMapper;
//	
//	@Override
//	public void processRow(ResultSet rs) throws SQLException {
//		ProductInfo productInfo = productInfoMapper.mapRow(rs);		
//		HashMap<Integer, ProductInfo> productInfoLang = result.get(productInfo.getProductId());				
//		if (productInfoLang == null) {
//			productInfoLang = new HashMap<Integer, ProductInfo>();	
//			result.put(productInfo.getProductId(), productInfoLang);
//		}
//		productInfoLang.put(productInfo.getLanguageId(), productInfo);
//		result.put(productInfo.getProductId(), productInfoLang);		
//	}
//
//	/**
//	 * @return the result
//	 */
//	public HashMap<Long, HashMap<Integer, ProductInfo>> getResult() {
//		return result;
//	}
//
//	/**
//	 * @param result the result to set
//	 */
//	public void setResult(HashMap<Long, HashMap<Integer, ProductInfo>> result) {
//		this.result = result;
//	}
//	
//}
