package capital.any.dao.base.marketingAttribution;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.base.MarketingAttribution;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository
public class MarketingAttributionDao implements IMarketingAttributionDao {
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;

	@Override
	public void insert(MarketingAttribution marketingAttribution) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("userId", marketingAttribution.getUserId())
								.addValue("tableId", marketingAttribution.getTableId())
								.addValue("referenceId", marketingAttribution.getReferenceId())
								.addValue("trackingId", marketingAttribution.getTrackingId());
		jdbcTemplate.update(INSERT_MARKETING_ATTRIBUTION, namedParameters);
	}

}
