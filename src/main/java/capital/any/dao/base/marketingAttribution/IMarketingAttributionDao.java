package capital.any.dao.base.marketingAttribution;

import capital.any.model.base.MarketingAttribution;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IMarketingAttributionDao {
	
	public static final String INSERT_MARKETING_ATTRIBUTION = 
			"  INSERT " +
		    "  INTO marketing_attribution " +
			"  ( " +
		    "     user_id, " +
		    " 	  table_id, " +
		    " 	  reference_id, " +
		    " 	  tracking_id " +
		    "  ) " +
		    "  VALUES " +
		    "  ( " +
		    " 	  :userId, " +
		    " 	  :tableId, " +
		    "	  :referenceId, " +
		    "	  :trackingId " +
		    "  ) ";
	
	/**
	 * Insert marketing attribution
	 * @param marketingAttribution
	 */
	void insert(MarketingAttribution marketingAttribution);
}
