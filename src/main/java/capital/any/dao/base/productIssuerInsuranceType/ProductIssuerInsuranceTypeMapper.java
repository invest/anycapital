package capital.any.dao.base.productIssuerInsuranceType;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.base.ProductIssuerInsuranceType;

/**
 * @author Eran
 *
 */
@Component
public class ProductIssuerInsuranceTypeMapper implements RowMapper<ProductIssuerInsuranceType> {

	@Override
	public ProductIssuerInsuranceType mapRow(ResultSet rs, int row) throws SQLException {
		ProductIssuerInsuranceType productIssuerInsuranceType = new ProductIssuerInsuranceType();
		productIssuerInsuranceType.setId(rs.getInt("id"));
		productIssuerInsuranceType.setName(rs.getString("name"));
		productIssuerInsuranceType.setDisplayName(rs.getString("display_name"));
		return productIssuerInsuranceType;
	}
				
}
