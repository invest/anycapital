package capital.any.dao.base.productIssuerInsuranceType;

import java.util.HashMap;

import capital.any.model.base.ProductIssuerInsuranceType;

/**
 * @author eranl
 *
 */
public interface IProductIssuerInsuranceTypeDao {
	
	public static final String GET_PRODUCT_ISSUER_INSURANCE_TYPES_LIST = 
													" SELECT " +
													"	* " + 
													" FROM "	+ 
													"	product_issuer_insurance_types ";
												
	HashMap<Integer, ProductIssuerInsuranceType> getProductIssuerInsuranceType();

}
