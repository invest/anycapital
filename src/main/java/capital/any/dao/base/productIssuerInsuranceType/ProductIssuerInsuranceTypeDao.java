package capital.any.dao.base.productIssuerInsuranceType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import capital.any.model.base.ProductIssuerInsuranceType;

/**
 * @author eranl
 *
 */
@Repository
public class ProductIssuerInsuranceTypeDao implements IProductIssuerInsuranceTypeDao {
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private ProductIssuerInsuranceTypeMapper productIssuerInsuranceTypeMapper;

	@Override
	public HashMap<Integer, ProductIssuerInsuranceType> getProductIssuerInsuranceType() {
		List<ProductIssuerInsuranceType> tmp = jdbcTemplate.query(GET_PRODUCT_ISSUER_INSURANCE_TYPES_LIST, productIssuerInsuranceTypeMapper);
		Map<Integer, ProductIssuerInsuranceType> hm = tmp.stream().collect(Collectors.toMap(p-> p.getId(), p -> p));		 		 
		return (HashMap<Integer, ProductIssuerInsuranceType>) hm;
	}

}
