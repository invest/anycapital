package capital.any.dao.base.transactionCoupon;

import capital.any.model.base.TransactionCoupon;

/**
 * @author Eyal Goren
 *
 */
public interface ITransactionCouponDao {
	
	public static final String INSERT = " INSERT " +
									    " INTO transaction_coupons " +
									    " ( " +
									    "    amount , " +
									    "    transaction_id, " +
									    " 	 product_coupon_id " +
									    " ) " +
									    " VALUES " +
									    " ( " +
									    "    :amount , " +
									    "    :transactionId, " +
									    " 	 :productCouponId " +
									    " ) ";
										
	/**
	 * insert {@link TransactionCoupon} into db
	 * @param transactionCoupon
	 */
	void insert(TransactionCoupon transactionCoupon);

}
