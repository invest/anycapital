package capital.any.dao.base.transactionCoupon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.base.TransactionCoupon;

/**
 * @author Eyal Goren
 *
 */
@Repository
public class TransactionCouponDao implements ITransactionCouponDao {
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	@Override
	public void insert(TransactionCoupon transactionCoupon) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("transactionId", transactionCoupon.getTransactionId())
						 .addValue("amount", transactionCoupon.getAmonut())
						 .addValue("productCouponId", transactionCoupon.getProductCouponId());
		
		jdbcTemplate.update(INSERT, namedParameters);
	}
}
