package capital.any.dao.base.transactionCoupon;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.base.TransactionCoupon;

/**
 * @author Eyal Goren
 *
 */
@Component
public class TransactionCouponMapper implements RowMapper<TransactionCoupon> {

	@Override
	public TransactionCoupon mapRow(ResultSet rs, int row) throws SQLException {
		TransactionCoupon transactionCoupon = new TransactionCoupon();
		transactionCoupon.setTransactionId(rs.getLong("tc.transaction_id"));
		transactionCoupon.setAmonut(rs.getLong("tc.amount"));
		return transactionCoupon;
	}
				
}
