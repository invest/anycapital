package capital.any.dao.base.productType;

import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import capital.any.model.base.ProductType;

/**
 * @author eranl
 *
 */
@Primary
@Repository
public class ProductTypeDao implements IProductTypeDao, ApplicationContextAware {
	
	private ApplicationContext appContext;
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;

	@Override
	public Map<Integer, ProductType> get() {
		ProductTypeCallbackHandler callbackHandler = appContext.getBean(ProductTypeCallbackHandler.class);
		jdbcTemplate.query(GET_PRODUCT_TYPES_LIST, callbackHandler);
		Map<Integer, ProductType> hm = callbackHandler.getResult().stream().collect(Collectors.toMap(p-> p.getId(), p -> p));		 		 
		return hm;
	}
	
	@Override
	public void setApplicationContext(ApplicationContext appContext) throws BeansException {
		this.appContext = appContext;
		
	}
}
