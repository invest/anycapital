package capital.any.dao.base.productType;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Component;

import capital.any.dao.base.productTypeProtectionLevel.ProductTypeProtectionLevelMapper;
import capital.any.model.base.ProductType;
import capital.any.model.base.ProductTypeProtectionLevel;

@Component
@Scope("prototype")
public class ProductTypeCallbackHandler implements RowCallbackHandler {
	
	private List<ProductType> result = new ArrayList<ProductType>();
	
	@Autowired
	private ProductTypeMapper productTypeMapper;
	@Autowired
	private ProductTypeProtectionLevelMapper productTypeProtectionLevelMapper;

	@Override
	public void processRow(ResultSet rs) throws SQLException {
		ProductTypeProtectionLevel productTypeProtectionLevel = productTypeProtectionLevelMapper.mapRow(rs);
		ProductType productType = productTypeMapper.mapRow(rs);
		productType.setProductTypeProtectionLevel(productTypeProtectionLevel);
		result.add(productType);
	}

	public List<ProductType> getResult() {
		return result;
	}

	public void setResult(List<ProductType> result) {
		this.result = result;
	}
}
