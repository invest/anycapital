package capital.any.dao.base.productType;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.model.base.ProductCategory;
import capital.any.model.base.ProductType;
import capital.any.product.type.formula.Formula;

/**
 * @author Eran
 *
 */
@Component
public class ProductTypeMapper implements RowMapper<ProductType> {
	private static final Logger logger = LoggerFactory.getLogger(ProductTypeMapper.class);
	
	@Override
	public ProductType mapRow(ResultSet rs, int row) throws SQLException {
		ProductType productType = new ProductType();		
		productType.setId(rs.getInt("id"));
		productType.setName(rs.getString("name"));
		productType.setDisplayName(rs.getString("display_name"));
		ProductCategory productCategory = new ProductCategory();
		productCategory.setId(rs.getInt("product_category_id"));
		productType.setProductCategory(productCategory);
		productType.setCoupon(rs.getInt("is_coupon") == 0 ? false : true);
		productType.setDayCountConvention(rs.getInt("is_day_count_convention") == 0 ? false : true);
		productType.setMaxYield(rs.getInt("is_max_yield") == 0 ? false : true);
		productType.setMaxYieldPA(rs.getInt("is_max_yield_p_a") == 0 ? false : true);
		productType.setParticipation(rs.getInt("is_participation") == 0 ? false : true);
		productType.setStrikeLevel(rs.getInt("is_strike_level") == 0 ? false : true);
		productType.setBonusPrecentage(rs.getInt("is_bonus_precentage") == 0 ? false : true);
		productType.setRiskLevel(rs.getInt("risk_level"));
		ObjectMapper mapper = new ObjectMapper();
		Formula formula = null;
		try {
			formula = (Formula) mapper.readValue(rs.getString("formula_parameters") , Class.forName(rs.getString("formula_class_path")));
		} catch (ClassNotFoundException | IOException e) {
			logger.error("Can't get formula for product type " + productType.getId(), e);
		}
		
		productType.setFormula(formula);
		productType.setDistanceToBarrier(rs.getInt("is_distance_to_barrier") == 0 ? false : true);
		productType.setCapLevel(rs.getInt("is_cap_level") == 0 ? false : true);
		productType.setSliderMin(rs.getInt("slider_min"));
		productType.setSliderMax(rs.getInt("slider_max"));
		return productType;
	}
	
	public ProductType mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
			
}
	

