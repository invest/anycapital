package capital.any.dao.base.productType;

import java.util.Map;
import capital.any.model.base.ProductType;

/**
 * @author eranl
 *
 */
public interface IProductTypeDao {
	
	public static final String GET_PRODUCT_TYPES_LIST = 
			" SELECT " +
			"	pt.*, " +
			"	ptpl.id ptpl_id, " +
			"	ptpl.name ptpl_name, " +
			"	ptpl.display_name ptpl_display_name " +
			" FROM "	+ 
			"	product_types pt, " +
			" 	product_type_protection_level ptpl " +
			" WHERE " +
			"	pt.product_type_p_l_id = ptpl.id " +
			"   AND pt.is_active = 1 ";
		
	Map<Integer, ProductType> get();

}
