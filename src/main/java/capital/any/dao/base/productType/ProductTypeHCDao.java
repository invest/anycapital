package capital.any.dao.base.productType;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.ProductType;

/**
 * @author LioR SoLoMoN
 *
 */
@Repository("ProductTypeHCDao")
public class ProductTypeHCDao implements IProductTypeDao {
	private static final Logger logger = LoggerFactory.getLogger(ProductTypeHCDao.class);
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, ProductType> get() {
		Map<Integer, ProductType> productTypes = null;		
		try {
			productTypes = (Map<Integer, ProductType>) HazelCastClientFactory.getClient().getMap(Constants.MAP_PRODUCT_TYPES);
		} catch (Exception e) {
			logger.error("ERROR!", e);
		}
		return productTypes;	
	}
}
