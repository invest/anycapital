package capital.any.dao.base.qmQuestion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Component;

import capital.any.dao.base.qmAnswer.QmAnswerMapper;
import capital.any.dao.base.qmQuestionGroup.QmQuestionGroupMapper;
import capital.any.model.base.QmAnswer;
import capital.any.model.base.QmQuestion;
import capital.any.model.base.QmQuestionGroup;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
@Scope("prototype")
public class QmQuestionCallbackHandler implements RowCallbackHandler {

	private Map<Integer, QmQuestion> result = new HashMap<Integer, QmQuestion>();
	
	@Autowired
	private QmAnswerMapper qmAnswerMapper;
	@Autowired
	private QmQuestionGroupMapper qmQuestionGroupMapper;
	
	@Override
	public void processRow(ResultSet rs) throws SQLException {
		QmAnswer qmAnswer = qmAnswerMapper.mapRow(rs);
		QmQuestion qmQuestion = result.get(qmAnswer.getQuestionId());
		if (null == qmQuestion) {
			QmQuestionGroup qmQuestionGroup = qmQuestionGroupMapper.mapRow(rs);
			qmQuestion = new QmQuestion();
			qmQuestion.setId(rs.getInt("q_id"));
			qmQuestion.setName(rs.getString("q_name"));
			qmQuestion.setDisplayName(rs.getString("q_display_name"));
			qmQuestion.setActive(rs.getBoolean("q_is_active"));
			qmQuestion.setOrderId(rs.getInt("q_order_id"));
			qmQuestion.setQuestionTypeId(rs.getInt("q_question_type_id"));
			qmQuestion.setQmAnswerList(new ArrayList<QmAnswer>());
			qmQuestion.setQmQuestionGroup(qmQuestionGroup);
			result.put(qmQuestion.getId(), qmQuestion);
		}
		List<QmAnswer> list = qmQuestion.getQmAnswerList();
		list.add(qmAnswer);
	}

	/**
	 * @return the result
	 */
	public Map<Integer, QmQuestion> getResult() {
		return result;
	}

	/**
	 * @param result the result to set
	 */
	public void setResult(Map<Integer, QmQuestion> result) {
		this.result = result;
	}

}
