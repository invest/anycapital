package capital.any.dao.base.qmQuestion;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.QmQuestion;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
public class QmQuestionMapper extends MapperBase implements RowMapper<QmQuestion>{

	@Override
	public QmQuestion mapRow(ResultSet rs, int rowNum) throws SQLException {
		QmQuestion qmQuestion = new QmQuestion();
		qmQuestion.setId(rs.getInt("q_id"));
		qmQuestion.setName(rs.getString("q_name"));
		qmQuestion.setDisplayName(rs.getString("q_display_name"));
		qmQuestion.setActive(rs.getBoolean("q_is_active"));
		qmQuestion.setOrderId(rs.getInt("q_order_id"));
		qmQuestion.setQuestionTypeId(rs.getInt("q_question_type_id"));
		return qmQuestion; 
	}
	
	public QmQuestion mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}

}
