package capital.any.dao.base.qmQuestion;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.QmQuestion;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository("QmQuestionHCDao")
public class QmQuestionHCDao implements IQmQuestionDao {
	private static final Logger logger = LoggerFactory.getLogger(QmQuestionHCDao.class);
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, QmQuestion> getAll() {
		Map<Integer, QmQuestion> mapQmQuestion = null;
		try {
			mapQmQuestion = (Map<Integer, QmQuestion>) HazelCastClientFactory.getClient().getMap(Constants.MAP_REGULATION_QUESTIONNAIRE);
		} catch (Exception e) {
			logger.error("ERROR! with QmQuestionHCDao.getAll ", e);
		}
		return mapQmQuestion;
	}

}
