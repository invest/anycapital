package capital.any.dao.base.emailTemplate;

import java.util.ArrayList;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.hazelcast.core.IMap;
import com.hazelcast.query.SqlPredicate;

import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.EmailTemplate;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository("EmailTemplateHCDao")
public class EmailTemplateHCDao implements IEmailTemplateDao {
	private static final Logger logger = LoggerFactory.getLogger(EmailTemplateHCDao.class);
	
	@SuppressWarnings("unchecked")
	@Override
	public EmailTemplate getTemplate(EmailTemplate et) throws Exception {
		EmailTemplate emailTemplate = null;
		try {
			SqlPredicate predicate = new SqlPredicate("actionId = " + et.getActionId() + " and languageId = " + et.getLanguageId());
			ArrayList<EmailTemplate> map = new ArrayList<EmailTemplate>(((IMap<Integer, EmailTemplate>)HazelCastClientFactory.getClient().getMap(Constants.MAP_EMAIL_TEMPLATE)).values(predicate));
			if (map != null && map.size() > 0) {
				emailTemplate = map.get(0);
			}
		} catch (Exception e) {
			logger.error("ERROR! get EmailTemplate by HC", e);
		}
		return emailTemplate;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, EmailTemplate> get() {
		Map<Integer, EmailTemplate> map = null;
		try {
			map = (Map<Integer, EmailTemplate>) HazelCastClientFactory.getClient().getMap(Constants.MAP_EMAIL_TEMPLATE);
		} catch (Exception e) {
			logger.error("ERROR! get map of EmailTemplate by HC", e);
		}
		return map;
	}
}
