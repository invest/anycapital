package capital.any.dao.base.emailTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.base.EmailTemplate;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository
public class EmailTemplateDao implements IEmailTemplateDao {
	private static final Logger logger = LoggerFactory.getLogger(EmailTemplateDao.class);
	
	@Autowired
	protected NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	protected EmailTemplateMapper emailTemplateMapper;
	
	@Override
	public EmailTemplate getTemplate(EmailTemplate et) throws Exception {
		EmailTemplate emailTemplate = null;
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("actionId", et.getActionId())
								.addValue("languageId", et.getLanguageId());
		try {
			emailTemplate = jdbcTemplate.queryForObject(GET_TEMPLATE, namedParameters, emailTemplateMapper);
		} catch (Exception e) {			
			logger.error("Can't find template. ", e);
			throw e;
		}
		return emailTemplate;
	}
	
	@Override
	public Map<Integer, EmailTemplate> get() {
		List<EmailTemplate> tmp = jdbcTemplate.query(GET, emailTemplateMapper);
		Map<Integer, EmailTemplate> hm = tmp.stream().collect(Collectors.toMap(p-> p.getId(), p -> p));
		return (HashMap<Integer, EmailTemplate>)hm;
	}
	
}
