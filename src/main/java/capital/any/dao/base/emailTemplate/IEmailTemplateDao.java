package capital.any.dao.base.emailTemplate;

import java.util.Map;

import capital.any.model.base.EmailTemplate;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IEmailTemplateDao {
	
	public static final String GET_TEMPLATE =
			" SELECT " +
				" * " +
			" FROM " +
				" email_templates et " +
			" WHERE " +
				" et.action_id = :actionId " +
				" and et.language_id = :languageId " +
				" and et.is_active = 1 ";
	
	public static final String GET = 
			" SELECT " +
				" * " +
			" FROM " +
				" email_templates et ";

	EmailTemplate getTemplate(EmailTemplate et) throws Exception;

	Map<Integer, EmailTemplate> get();

}
