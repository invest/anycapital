package capital.any.dao.base.emailTemplate;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.EmailTemplate;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
public class EmailTemplateMapper extends MapperBase implements RowMapper<EmailTemplate> {

	@Override
	public EmailTemplate mapRow(ResultSet rs, int rowNum) throws SQLException {
		EmailTemplate emailTemplate = new EmailTemplate();
		emailTemplate.setId(rs.getInt("id"));
		emailTemplate.setActionId(rs.getInt("action_id"));
		emailTemplate.setLanguageId(rs.getInt("language_id"));
		emailTemplate.setTemplateId(rs.getString("template_id"));
		return emailTemplate;
	}
	
	public EmailTemplate mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
}
