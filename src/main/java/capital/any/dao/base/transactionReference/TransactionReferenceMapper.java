package capital.any.dao.base.transactionReference;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.base.TransactionReference;

/**
 * @author Eyal Goren
 *
 */
@Component
public class TransactionReferenceMapper implements RowMapper<TransactionReference> {

	@Override
	public TransactionReference mapRow(ResultSet rs, int row) throws SQLException {
		TransactionReference transactionReference = new TransactionReference();
		transactionReference.setTransactionId(rs.getLong("tr.transaction_id"));
		transactionReference.setReferenceId(rs.getLong("tr.reference_id"));
		transactionReference.setTableId(rs.getInt("tr.table_id"));
		return transactionReference;
	}
				
}
