package capital.any.dao.base.transactionReference;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.base.TransactionReference;

/**
 * @author Eyal Goren
 *
 */
@Repository
public class TransactionReferenceDao implements ITransactionReferenceDao {
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	@Override
	public void insert(TransactionReference transactionReference) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("transactionId", transactionReference.getTransactionId())
						 .addValue("referenceId", transactionReference.getReferenceId())
						 .addValue("tableId", transactionReference.getTableId());
		
		jdbcTemplate.update(INSERT, namedParameters);
	}
}
