package capital.any.dao.base.transactionReference;

import capital.any.model.base.TransactionReference;

/**
 * @author Eyal Goren
 *
 */
public interface ITransactionReferenceDao {
	
	public static final String INSERT = " INSERT " +
									    " INTO transaction_references " +
									    " ( " +
									    "    reference_id , " +
									    "    table_id , " +
									    "    transaction_id " +
									    " ) " +
									    " VALUES " +
									    " ( " +
									    "    :referenceId , " +
									    "    :tableId , " +
									    "    :transactionId " +
									    " ) ";
										
	/**
	 * insert {@link TransactionReference} into db
	 * @param transactionReference
	 */
	void insert(TransactionReference transactionReference);

}
