package capital.any.dao.base.sms;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.SMS;

/**
 * @author eran.levy
 *
 */
@Component
public class SMSMapper extends MapperBase implements RowMapper<SMS> {
	
	@Override
	public SMS mapRow(ResultSet rs, int row) throws SQLException {
		SMS sms = new SMS();
		sms.setId(rs.getInt("id"));
		sms.setType(SMS.Type.getByToken(rs.getInt("type_id")));
		sms.setStatus(SMS.Status.getByToken(rs.getInt("status_id")));
		sms.setSender(rs.getString("sender"));
		sms.setPhone(rs.getString("phone"));
		sms.setMessage(rs.getString("message"));
		sms.setProviderId(rs.getInt("provider_id"));
		sms.setTimeCreated(convertTimestampToDate(rs.getTimestamp("time_created")));
		sms.setTimeScheduled(convertTimestampToDate(rs.getTimestamp("time_scheduled")));
		sms.setTimeSent(convertTimestampToDate(rs.getTimestamp("time_sent")));
		sms.setReferenceId(rs.getLong("reference_id"));
		sms.setResponse(rs.getString("response"));
		sms.setSenderNumber(rs.getString("sender_number"));
		return sms;
	}
	
	public SMS mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
			
}
