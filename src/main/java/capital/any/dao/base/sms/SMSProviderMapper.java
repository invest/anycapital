package capital.any.dao.base.sms;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.base.SMSProvider;

/**
 * @author eran.levy
 *
 */
@Component
public class SMSProviderMapper implements RowMapper<SMSProvider> {
	
	private static final Logger logger = LoggerFactory.getLogger(SMSProviderMapper.class);
	
	@Override
	public SMSProvider mapRow(ResultSet rs, int row) throws SQLException {
		SMSProvider smsProvider = null;
		try {
			Class<?> c = Class.forName(rs.getString("provider_class"));
			smsProvider = (SMSProvider) c.newInstance();
			smsProvider.setId(rs.getInt("id"));
			smsProvider.setName(rs.getString("name"));
			smsProvider.setProviderClass(rs.getString("provider_class"));
			smsProvider.setUrl(rs.getString("url"));
			smsProvider.setUserName(rs.getString("user_name"));
			smsProvider.setPassword(rs.getString("password"));
			smsProvider.setProps(rs.getString("props"));
			smsProvider.setDlrUrl(rs.getString("dlr_url"));			
		} catch (Exception e) {
			logger.error("Can't load SMSProvider, error:", e);
		}
		return smsProvider;
	}
	
	public SMSProvider mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
			
}
