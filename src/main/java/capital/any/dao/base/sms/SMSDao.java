package capital.any.dao.base.sms;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.base.SMS;
import capital.any.model.base.SMSProvider;

/**
 * @author eran.levy
 *
 */
@Repository
public class SMSDao implements ISMSDao {
	
	private static final Logger logger = LoggerFactory.getLogger(SMSDao.class);
	
	@Autowired
	protected NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private SMSProviderMapper smsProviderMapper;
	@Autowired
	private SMSMapper smsMapper;

	@Override
	public void insert(SMS sms) {			
		SqlParameterSource parameters = new MapSqlParameterSource
				("type_id", sms.getType().getToken())
				.addValue("sender", sms.getSender())
				.addValue("phone", sms.getPhone())
				.addValue("message", sms.getMessage())
				.addValue("time_scheduled", sms.getTimeScheduled())
				.addValue("status_id", sms.getStatus().getToken())				
				.addValue("reference_id", sms.getReferenceId())
				.addValue("sender_number", sms.getSenderNumber());
		
		jdbcTemplate.update(INSERT_SMS, parameters);
	}

	@Override
	public void update(SMS sms) {
		SqlParameterSource parameters = new MapSqlParameterSource
				("response", sms.getResponse())
				.addValue("status_id", sms.getStatus().getToken())				
				.addValue("time_sent", sms.getTimeSent())
				.addValue("retries", sms.getRetries())
				.addValue("provider_id", sms.getProviderId())
				.addValue("id", sms.getId());
		
		jdbcTemplate.update(UPDATE_SMS, parameters);	
		
	}

	@Override
	public Map<Integer, SMSProvider> getSMSProviders() {
		List<SMSProvider> tmp = jdbcTemplate.query(GET_SMS_PROVIDERS, smsProviderMapper);
		Map<Integer, SMSProvider> hm = tmp.stream().collect(Collectors.toMap(p-> p.getId(), p -> p));		 		 
		return hm;
	}
	
	@Override
	public SMS get(long id) {
		SqlParameterSource parameters = new MapSqlParameterSource("id", id);
		SMS sms = null;
		try {
			sms = (SMS)jdbcTemplate.queryForObject(GET_SMS_BY_ID, parameters, smsMapper);
		} catch (DataAccessException e) {
			logger.error("can't find SMS!", e);
		}
		return sms;				 						
	}
	
	
}
