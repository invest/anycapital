package capital.any.dao.base.sms;

import java.util.Map;

import capital.any.model.base.SMS;
import capital.any.model.base.SMSProvider;

/**
 * @author eran.levy
 *
 */
public interface ISMSDao {
	
	public static final String GET_SMS_PROVIDERS = 
			" SELECT " +
			"	* " +
			" FROM	" +
			"	sms_providers ";
	
	public static final String INSERT_SMS = 
			" INSERT " +
		    " INTO sms " +
		    " ( " +
		    "   type_id, " +
		    "   sender, " +
		    "   phone, " +
		    "	message, " +
		    "	time_scheduled, " +
		    "	status_id, " +
		    "	reference_id, " +
		    "	sender_number " +
		    " ) " +
		    " VALUES " +
		    " ( " +
		    "   :type_id, " +
		    "   :sender, " +
		    "   :phone, " +
		    "	:message, " +
		    "	:time_scheduled, " +
		    "	:status_id, " +
		    "	:reference_id, " +
		    "	:sender_number " +	    
		    "  )";
	
	public static final String UPDATE_SMS = 
			" UPDATE " +
			"	sms " +
			" SET " +
			"	status_id = :status_id, " +
			"	response = :response, " +
			"	time_sent = :time_sent, " +
			"	retries = :retries," +
			"	provider_id = :provider_id " +
			" WHERE " +
			"	id = :id ";
	
	public static final String GET_SMS_BY_ID = 
			" SELECT " +
			"	* " +
			" FROM " +
			"	sms " +
			" WHERE " +
			"	id = :id ";	
			
	/**
	 * Insert SMS
	 * @param sms
	 */
	void insert(SMS sms);
	
	/**
	 * Update SMS
	 * @param sms
	 */
	void update(SMS sms);
	
	/**
	 * Get SMS providers
	 * @return
	 */
	Map<Integer, SMSProvider> getSMSProviders();
	
	/**
	 * Get SMS by ID
	 * @param id
	 * @return
	 */
	SMS get(long id);
	
}
