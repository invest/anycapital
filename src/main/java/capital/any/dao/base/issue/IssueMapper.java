package capital.any.dao.base.issue;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.Issue;
import capital.any.model.base.IssueChannel;
import capital.any.model.base.IssueDirection;
import capital.any.model.base.IssueReachedStatus;
import capital.any.model.base.IssueReaction;
import capital.any.model.base.IssueSubject;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
public class IssueMapper extends MapperBase implements RowMapper<Issue> {

	@Override
	public Issue mapRow(ResultSet rs, int rowNum) throws SQLException {
		Issue issue = new Issue();
		issue.setId(rs.getLong("id"));
		issue.setIssueChannel(new IssueChannel(rs.getInt("channel_id")));
		issue.setIssueSubject(new IssueSubject(rs.getInt("subject_id")));
		issue.setIssueDirection(new IssueDirection(rs.getInt("direction_id")));
		issue.setIssueReachedStatus(new IssueReachedStatus(rs.getInt("reached_status_id")));
		issue.setIssueReaction(new IssueReaction(rs.getInt("reaction_id")));
		issue.setComments(rs.getString("comments"));
		issue.setSignificantNote(rs.getBoolean("significant_note"));
		issue.setUserId(rs.getLong("user_id"));
		issue.setWriterId(rs.getInt("writer_id"));
		issue.setTimeCreated(convertTimestampToDate(rs.getTimestamp("time_created")));
		return issue;
	}
	
	public Issue mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}

}
