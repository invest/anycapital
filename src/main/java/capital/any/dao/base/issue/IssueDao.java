package capital.any.dao.base.issue;

import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.base.Issue;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository
public class IssueDao implements IIssueDao, ApplicationContextAware  {

	private ApplicationContext appContext;
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	@Override
	public void insert(Issue issue) {
		SqlParameterSource namedParameters =
				new MapSqlParameterSource("channelId", issue.getIssueChannel().getId())
								.addValue("subjectId", issue.getIssueSubject().getId())
								.addValue("directionId", issue.getIssueDirection() == null ? null : issue.getIssueDirection().getId())
								.addValue("reachedStatusId", issue.getIssueReachedStatus() == null ? null : issue.getIssueReachedStatus().getId())
								.addValue("reactionId", issue.getIssueReaction() == null ? null : issue.getIssueReaction().getId())
								.addValue("comments", issue.getComments())
								.addValue("significantNote", issue.getSignificantNote())
								.addValue("userId", issue.getUserId())
								.addValue("writerId", issue.getWriterId());
		jdbcTemplate.update(INSERT_ISSUE, namedParameters);
	}
	
	@Override
	public Map<Long, Issue> getIssuesByUserId(long userId) {
		IssueCallbackHandler issueCallbackHandler = appContext.getBean(IssueCallbackHandler.class);
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("userId", userId);
		jdbcTemplate.query(GET_ISSUES_BY_USER_ID, params, issueCallbackHandler);
		return issueCallbackHandler.getResult();
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.appContext = applicationContext;
	}
	
}
