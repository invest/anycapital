package capital.any.dao.base.issue;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Component;

import capital.any.dao.base.issueChannel.IssueChannelMapper;
import capital.any.dao.base.issueDirection.IssueDirectionMapper;
import capital.any.dao.base.issueReachedStatus.IssueReachedStatusMapper;
import capital.any.dao.base.issueReaction.IssueReactionMapper;
import capital.any.dao.base.issueSubject.IssueSubjectMapper;
import capital.any.dao.base.writer.BaseWriterFactory;
import capital.any.model.base.AbstractWriter;
import capital.any.model.base.Issue;
import capital.any.model.base.IssueChannel;
import capital.any.model.base.IssueDirection;
import capital.any.model.base.IssueReachedStatus;
import capital.any.model.base.IssueReaction;
import capital.any.model.base.IssueSubject;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
@Scope("prototype")
public class IssueCallbackHandler implements RowCallbackHandler {

	private Map<Long, Issue> result = new HashMap<Long, Issue>();
	
	@Autowired
	private IssueMapper issueMapper;
	@Autowired
	private IssueChannelMapper issueChannelMapper;
	@Autowired
	private IssueDirectionMapper issueDirectionMapper;
	@Autowired
	private IssueReachedStatusMapper issueReachedStatusMapper;
	@Autowired
	private IssueReactionMapper issueReactionMapper;
	@Autowired
	private IssueSubjectMapper issueSubjectMapper;
	@Autowired
	private BaseWriterFactory writerFactory;
	
	@Override
	public void processRow(ResultSet rs) throws SQLException {
		IssueChannel issueChannel = issueChannelMapper.mapRow(rs);
		IssueDirection issueDirection = issueDirectionMapper.mapRow(rs);
		IssueReachedStatus issueReachedStatus = issueReachedStatusMapper.mapRow(rs);
		IssueReaction issueReaction = issueReactionMapper.mapRow(rs);
		IssueSubject issueSubject = issueSubjectMapper.mapRow(rs);
		int writerId = rs.getInt("writer_id");
		AbstractWriter writer = writerFactory.createWriter(writerId);
		writer.setId(writerId);
		writer.setUserName(rs.getString("writer_name"));
		// Issue
		Issue issue = issueMapper.mapRow(rs);
		issue.setIssueChannel(issueChannel);
		issue.setIssueDirection(issueDirection);
		issue.setIssueReachedStatus(issueReachedStatus);
		issue.setIssueReaction(issueReaction);
		issue.setIssueSubject(issueSubject);
		issue.setWriter(writer);
		result.put(issue.getId(), issue);
	}

	/**
	 * @return the result
	 */
	public Map<Long, Issue> getResult() {
		return result;
	}
}
