package capital.any.dao.base.issue;

import java.util.Map;

import capital.any.model.base.Issue;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IIssueDao {
	public static final String INSERT_ISSUE = 
			"  INSERT " +
				    "  INTO issues " +
					"  ( " +
				    "     channel_id, " +
				    "     subject_id, " +
				    " 	  direction_id, " +
				    "     reached_status_id, " +
				    "     reaction_id, " +
				    "     comments, " +
				    "     significant_note, " +
				    "     user_id, " +
				    "     writer_id " +
				    "  ) " +
				    "  VALUES " +
				    "  ( " +
				    " 	  :channelId, " +
				    " 	  :subjectId, " +
				    " 	  :directionId, " +
				    " 	  :reachedStatusId, " +
				    " 	  :reactionId, " +
				    " 	  :comments, " +
				    " 	  :significantNote, " +
				    " 	  :userId, " +
				    " 	  :writerId " +
				    "  ) ";
	
	public static final String GET_ISSUES_BY_USER_ID = 
			"  SELECT " + 
				"  iss.*, " +
				"  iss_c.id iss_c_id, " +
				"  iss_c.name iss_c_name, " +
				"  iss_s.id iss_s_id, " +
				"  iss_s.name iss_s_name, " +
				"  iss_d.id iss_d_id, " +
				"  iss_d.name iss_d_name, " +
				"  iss_rs.id iss_rs_id, " +
				"  iss_rs.name iss_rs_name, " +
				"  iss_r.id iss_r_id, " +
				"  iss_r.name iss_r_name, " +
				"  iss_r.reached_status_id iss_r_reached_status_id, " +
				"  w.id writer_id, " +
				"  w.user_name writer_name " +
			"  FROM " +
				"  issues iss " +
					"  LEFT JOIN issue_directions iss_d ON iss.direction_id = iss_d.id " +
					"  LEFT JOIN issue_reached_statuses iss_rs ON iss.reached_status_id = iss_rs.id " +
					"  LEFT JOIN issue_reactions iss_r ON iss.reaction_id = iss_r.id, " +
				"  issue_channels iss_c, " +
				"  issue_subjects iss_s, " +
				"  writers w " +
			"  WHERE " +
				"  iss.channel_id = iss_c.id " +
				"  and iss.subject_id = iss_s.id " +
				"  and iss.writer_id = w.id " +
				"  and iss.user_id = :userId ";

	void insert(Issue issue);

	Map<Long, Issue> getIssuesByUserId(long userId);
	
	
}
