package capital.any.dao.base.marketingContent;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.MarketingContent;
import capital.any.service.UtilService;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
public class MarketingContentMapper extends MapperBase implements RowMapper<MarketingContent> {
	
	@Autowired
	private Environment environment;
	@Autowired
	private UtilService utilService;
	
	@Override
	public MarketingContent mapRow(ResultSet rs, int row) throws SQLException {
		MarketingContent marketingContent = new MarketingContent();
		marketingContent.setId(rs.getInt("id"));
		marketingContent.setName(rs.getString("name"));
		marketingContent.setComments(rs.getString("comments"));
		marketingContent.setPageContent(rs.getString("page_content"));
		marketingContent.setWriterId(rs.getInt("writer_id"));
		marketingContent.setTimeCreated(convertTimestampToDate(rs.getTimestamp("time_created")));
		marketingContent.setTimeModified(convertTimestampToDate(rs.getTimestamp("time_modified")));
		marketingContent.setHTMLFilePath(rs.getString("html_file_path"));	
		if (!utilService.isArgumentEmptyOrNull(marketingContent.getHTMLFilePath())) {
			marketingContent.setHTMLURL(environment.getProperty("aws.cloud.url") + marketingContent.getHTMLFilePath());
		}
		return marketingContent;		
	}
	
	public MarketingContent mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
}
