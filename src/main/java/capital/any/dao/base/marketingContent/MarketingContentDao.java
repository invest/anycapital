package capital.any.dao.base.marketingContent;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import capital.any.model.base.MarketingContent;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository
public class MarketingContentDao implements IMarketingContentDao, ApplicationContextAware {
	private static final Logger logger = LoggerFactory.getLogger(MarketingContentDao.class);

	private ApplicationContext appContext;
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private MarketingContentMapper marketingContentMapper;
	
	@Override
	public void insert(MarketingContent marketingContent) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		SqlParameterSource namedParameters =
				new MapSqlParameterSource("name", marketingContent.getName())
								.addValue("comments", marketingContent.getComments())
								.addValue("pageContent", marketingContent.getPageContent())
								.addValue("writerId", marketingContent.getWriterId())
								.addValue("HTMLFilePath", marketingContent.getHTMLFilePath());
		jdbcTemplate.update(INSERT_MARKETING_CONTENT, namedParameters, keyHolder, new String[] {"id"});
		marketingContent.setId(keyHolder.getKey().intValue());
	}

	@Override
	public void update(MarketingContent marketingContent) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("id", marketingContent.getId())
				.addValue("name", marketingContent.getName())
				.addValue("comments", marketingContent.getComments())
				.addValue("pageContent", marketingContent.getPageContent())
				.addValue("writerId", marketingContent.getWriterId())
				.addValue("HTMLFilePath", marketingContent.getHTMLFilePath());
		jdbcTemplate.update(UPDATE_MARKETING_CONTENT, namedParameters);
		
	}

	@Override
	public Map<Integer, MarketingContent> getAll() {
		MarketingContentCallbackHandler callbackHandler = appContext.getBean(MarketingContentCallbackHandler.class);
		jdbcTemplate.query(GET_MARKETING_CONTENTS, callbackHandler);	
		return callbackHandler.getResult();
	}

	@Override
	public MarketingContent getMarketingContent(int id) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("id", id);
		MarketingContent marketingContent = null;
		try {
			marketingContent = jdbcTemplate.queryForObject(GET_MARKETING_CONTENT, namedParameters, marketingContentMapper);
		} catch (Exception e) {
			logger.error("can't find marketing content!", e.getMessage());
		}
		return marketingContent;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.appContext = applicationContext;
	}
}
