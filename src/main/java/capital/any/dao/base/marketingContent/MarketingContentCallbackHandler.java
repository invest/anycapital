package capital.any.dao.base.marketingContent;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Component;

import capital.any.model.base.MarketingContent;
import capital.any.model.base.Writer;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
@Scope("prototype")
public class MarketingContentCallbackHandler implements RowCallbackHandler {
	
	private Map<Integer, MarketingContent> result = new HashMap<Integer, MarketingContent>();
	
	@Autowired
	private MarketingContentMapper MarketingContentMapper;

	@Override
	public void processRow(ResultSet rs) throws SQLException {
		Writer writer = new Writer();
		writer.setUserName(rs.getString("writer_name"));
		MarketingContent marketingContent = MarketingContentMapper.mapRow(rs);
		marketingContent.setWriter(writer);
		result.put(marketingContent.getId(), marketingContent);
	}

	public Map<Integer, MarketingContent> getResult() {
		return result;
	}

	public void setResult(Map<Integer, MarketingContent> result) {
		this.result = result;
	}
}
