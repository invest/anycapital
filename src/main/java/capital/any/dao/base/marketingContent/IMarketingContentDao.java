package capital.any.dao.base.marketingContent;

import java.util.Map;

import capital.any.model.base.MarketingContent;
import capital.any.service.base.marketingContent.IMarketingContentService;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IMarketingContentDao {
	
	public static final String INSERT_MARKETING_CONTENT = 
			"  INSERT " +
		    "  INTO marketing_contents " +
			"  ( " +
		    "     name, " +
		    "     comments, " +
		    "     page_content, " +
		    " 	  writer_id, " +
		    "	  html_file_path" +
		    "  ) " +
		    "  VALUES " +
		    "  ( " +
		    " 	  :name, " +
		    " 	  :comments, " +
		    " 	  :pageContent, " +
		    " 	  :writerId, " + 
		    " 	  concat((SELECT auto_increment FROM INFORMATION_SCHEMA.TABLES WHERE table_name = 'marketing_contents' AND table_schema = 'anycapital'),'" +  IMarketingContentService.NAME_SEPARATOR + "', :HTMLFilePath) " +		    
		    "  ) ";
	
	public static final String UPDATE_MARKETING_CONTENT =
			" UPDATE " +
				" marketing_contents mc " +
			" SET " +
				" mc.name = :name, " +
				" mc.comments = :comments, " +
				" mc.page_content = :pageContent, " +
				" mc.writer_id = :writerId, " +
				" mc.html_file_path = :HTMLFilePath, " +
				" mc.time_modified = now() " +
			" WHERE " +
				" mc.id = :id ";
	
	public static final String GET_MARKETING_CONTENTS =
			" SELECT " +
				" mc.*, " + 
				" w.user_name as writer_name " +
			" FROM "	+ 
				" marketing_contents mc, " +
				" writers w " +
			" WHERE " +
		        " mc.writer_id = w.id " +
			" ORDER BY " +
				" mc.id DESC ";
	
	public static final String GET_MARKETING_CONTENT =
			" SELECT " +
			" 	* " + 
			" FROM "	+ 
			" 	marketing_contents mc " +
			" WHERE " +
			" 	mc.id = :id ";
			
	
	/**
	 * Insert marketing content 
	 * @param marketingContent
	 */
	void insert(MarketingContent marketingContent);
	
	/**
	 * Update marketing content
	 * @param marketingContent
	 */
	void update(MarketingContent marketingContent);
	
	/**
	 * Get all marketing content
	 * @return Map<Integer, MarketingContent>
	 */
	Map<Integer, MarketingContent> getAll();
	
	/**
	 * Get marketing content
	 * @param id
	 * @return MarketingContent
	 */
	MarketingContent getMarketingContent(int id);
}
