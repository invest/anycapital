package capital.any.dao.base.marketingSource;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Component;

import capital.any.model.base.MarketingSource;
import capital.any.model.base.Writer;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
@Scope("prototype")
public class MarketingSourceCallbackHandler implements RowCallbackHandler {
	
	private Map<Integer, MarketingSource> result = new HashMap<Integer, MarketingSource>();
	
	@Autowired
	private MarketingSourceMapper MarketingSourceMapper;

	@Override
	public void processRow(ResultSet rs) throws SQLException {
		Writer writer = new Writer();
		writer.setUserName(rs.getString("writer_name"));
		MarketingSource marketingSource = MarketingSourceMapper.mapRow(rs);
		marketingSource.setWriter(writer);
		result.put(marketingSource.getId(), marketingSource);
	}

	public Map<Integer, MarketingSource> getResult() {
		return result;
	}

	public void setResult(Map<Integer, MarketingSource> result) {
		this.result = result;
	}
}
