package capital.any.dao.base.marketingSource;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.base.MarketingSource;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository
public class MarketingSourceDao implements IMarketingSourceDao, ApplicationContextAware {
	private static final Logger logger = LoggerFactory.getLogger(MarketingSourceDao.class);

	private ApplicationContext appContext;
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private MarketingSourceMapper marketingSourceMapper;
	
	@Override
	public void insert(MarketingSource marketingSource) {
		SqlParameterSource namedParameters =
				new MapSqlParameterSource("name", marketingSource.getName())
								.addValue("writerId", marketingSource.getWriterId());
		jdbcTemplate.update(INSERT_MARKETING_SOURCE, namedParameters);
	}

	@Override
	public void update(MarketingSource marketingSource) {
		SqlParameterSource namedParameters =
				new MapSqlParameterSource("id", marketingSource.getId())
								.addValue("name", marketingSource.getName())
								.addValue("writerId", marketingSource.getWriterId());
		jdbcTemplate.update(UPDATE_MARKETING_SOURCE, namedParameters);
	}

	@Override
	public Map<Integer, MarketingSource> getMarketingSources() {
		MarketingSourceCallbackHandler callbackHandler = appContext.getBean(MarketingSourceCallbackHandler.class);
		jdbcTemplate.query(GET_MARKETING_SOURCE_LIST, callbackHandler);	
		return callbackHandler.getResult();
	}

	@Override
	public MarketingSource getMarketingSource(int id) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("id", id);
		MarketingSource marketingSource = null;
		try {
			marketingSource = jdbcTemplate.queryForObject(GET_MARKETING_SOURCE, namedParameters, marketingSourceMapper);
		} catch (Exception e) {
			logger.error("can't find marketing source!", e.getMessage());
		}
		return marketingSource;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.appContext = applicationContext;
	}
	
}
