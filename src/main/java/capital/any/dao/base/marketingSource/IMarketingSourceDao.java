package capital.any.dao.base.marketingSource;

import java.util.Map;

import capital.any.model.base.MarketingSource;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IMarketingSourceDao {
	
	public static final String INSERT_MARKETING_SOURCE = 
			"  INSERT " +
		    "  INTO marketing_sources " +
			"  ( " +
		    "     name, " +
		    " 	  writer_id " +		    
		    "  ) " +
		    "  VALUES " +
		    "  ( " +
		    " 	  :name, " +
		    " 	  :writerId " +
		    "  ) ";
	
	public static final String UPDATE_MARKETING_SOURCE =
			" UPDATE " +
				" marketing_sources ms " +
			" SET " +
				" ms.name = :name, " +
				" ms.writer_id = :writerId, " +
				" ms.time_modified = now() " +
			" WHERE " +
				" ms.id = :id ";
	
	public static final String GET_MARKETING_SOURCE_LIST =
			" SELECT " +
				" ms.*, " +
				" w.user_name as writer_name " +
			" FROM "	+ 
				" marketing_sources ms, " +
				" writers w " +
			" WHERE " +
				" w.id = ms.writer_id " +
			" ORDER BY " +
				" ms.name ";
	
	public static final String GET_MARKETING_SOURCE = 
			" SELECT " +
				" * " + 
			" FROM " + 
				" marketing_sources ms " +
			" WHERE " +
				" ms.id = :id ";
	
	/**
	 * Insert marketing source
	 * @param marketingSource
	 */
	void insert(MarketingSource marketingSource);
	
	/**
	 * update marketing source
	 * @param marketingSource
	 */
	void update(MarketingSource marketingSource);
	
	/**
	 * Get marketing sources
	 * @return Map<Long, MarketingSource>
	 */
	Map<Integer, MarketingSource> getMarketingSources();
	
	/**
	 * Get marketing source
	 * @param id
	 * @return MarketingSource
	 */
	MarketingSource getMarketingSource(int id);
}
