package capital.any.dao.base.marketingSource;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.MarketingSource;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
public class MarketingSourceMapper extends MapperBase implements RowMapper<MarketingSource> {
	@Override
	public MarketingSource mapRow(ResultSet rs, int row) throws SQLException {
		MarketingSource marketingSource = new MarketingSource();
		marketingSource.setId(rs.getInt("id"));
		marketingSource.setName(rs.getString("name"));
		marketingSource.setWriterId(rs.getInt("writer_id"));
		marketingSource.setTimeCreated(convertTimestampToDate(rs.getTimestamp("time_created")));
		marketingSource.setTimeModified(convertTimestampToDate(rs.getTimestamp("time_modified")));
		return marketingSource;
	}
	
	public MarketingSource mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
}
