package capital.any.dao.base.providerRequestResponse;

import capital.any.model.base.ProviderRequestResponse;

/**
 * @author eran.levy
 *
 */
public interface IProviderRequestResponseDao {
	
	public static final String INSERT_PROVIDER_REQUEST_RESPONSE = 
			" INSERT " +
		    " INTO provider_request_response " +
		    " ( " +
		    "   table_id, " +
		    "   reference_id, " +
		    "   request, " +
		    "   response " +
		    " ) " +
		    " VALUES " +
		    " ( " +
		    "   :table_id, " +
		    "   :reference_id, " +
		    "   :request, " +
		    "   :response " +
		    "  )";

	/**
	 * Insert
	 * @param providerRequestResponse
	 */
	void insert(ProviderRequestResponse providerRequestResponse);
}
