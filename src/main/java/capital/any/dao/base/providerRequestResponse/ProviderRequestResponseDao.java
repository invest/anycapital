package capital.any.dao.base.providerRequestResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.base.ProviderRequestResponse;

/**
 * @author eran.levy
 *
 */
@Repository
public class ProviderRequestResponseDao implements IProviderRequestResponseDao {
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	@Override
	public void insert(ProviderRequestResponse providerRequestResponse) {		
		SqlParameterSource parameters = new MapSqlParameterSource
				("table_id", providerRequestResponse.getTableId())
				.addValue("reference_id", providerRequestResponse.getReferenceId())
				.addValue("request", providerRequestResponse.getRequest())
				.addValue("response", providerRequestResponse.getResponse());
		
		jdbcTemplate.update(INSERT_PROVIDER_REQUEST_RESPONSE, parameters);
	}
	
}
