package capital.any.dao.base.productSimulation;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Component;

import capital.any.model.base.ProductSimulation;

/**
 * @author eranl
 *
 */
@Component
@Scope("prototype")
public class ProductSimulationCallbackHandler implements RowCallbackHandler {
	
	private HashMap<Long, List<ProductSimulation>> result = new HashMap<Long, List<ProductSimulation>>();
	@Autowired
	private ProductSimulationMapper productSimulationMapper;
	
	@Override
	public void processRow(ResultSet rs) throws SQLException {
		ProductSimulation productSimulation = productSimulationMapper.mapRow(rs);		
		List<ProductSimulation> productSimulations = result.get(productSimulation.getProductId());
		if (productSimulations == null) {
			productSimulations = new ArrayList<ProductSimulation>();		
		}
		productSimulations.add(productSimulation);
		result.put(productSimulation.getProductId(), productSimulations);		
	}

	/**
	 * @return the result
	 */
	public HashMap<Long, List<ProductSimulation>> getResult() {
		return result;
	}

	/**
	 * @param result the result to set
	 */
	public void setResult(HashMap<Long, List<ProductSimulation>> result) {
		this.result = result;
	}
	
}
