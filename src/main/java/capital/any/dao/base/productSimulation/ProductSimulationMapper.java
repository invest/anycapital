package capital.any.dao.base.productSimulation;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.base.ProductSimulation;

/**
 * @author Eran
 *
 */
@Component
public class ProductSimulationMapper implements RowMapper<ProductSimulation> {
	
	@Override
	public ProductSimulation mapRow(ResultSet rs, int row) throws SQLException {		
		ProductSimulation productSimulation = new ProductSimulation();
		productSimulation.setId(rs.getInt("id"));
		productSimulation.setProductId(rs.getLong("product_id"));
		productSimulation.setFinalFixingLevel(rs.getDouble("final_fixing_level"));
//		productSimulation.setCashSettlement(rs.getLong("cash_settlement"));
		productSimulation.setPosition(rs.getInt("position"));		
		return productSimulation;
	}
	
	public ProductSimulation mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
			
}
	

