package capital.any.dao.base.productSimulation;

import java.util.HashMap;
import java.util.List;

import capital.any.model.base.ProductSimulation;

/**
 * @author eranl
 *
 */
public interface IProductSimulationDao {
	
	public static final String GET_PRODUCT_SIMULATION_LIST = 
			" SELECT " +
			"	* " + 
			" FROM "	+ 
			"	product_simulation ";
	
	public static final String GET_PRODUCT_SIMULATION_LIST_BY_PRODUCT = 
			" SELECT " +
			"	* " + 
			" FROM "	+ 
			"	product_simulation ps " + 
			" WHERE " +
			"	ps.product_id = :productId";
		
	HashMap<Long, List<ProductSimulation>> getProductSimulations();
	
	List<ProductSimulation> getProductSimulations(long productId);

}
