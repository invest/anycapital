package capital.any.dao.base.productSimulation;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.base.ProductSimulation;

/**
 * @author eranl
 *
 */
@Repository
public class ProductSimulationDao implements IProductSimulationDao, ApplicationContextAware {
	
	private ApplicationContext appContext;
	
	@Autowired
	protected NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private ProductSimulationMapper productSimulationMapper;
	
	@Override
	public HashMap<Long, List<ProductSimulation>> getProductSimulations() {
		ProductSimulationCallbackHandler callbackHandler = appContext.getBean(ProductSimulationCallbackHandler.class);
		jdbcTemplate.query(GET_PRODUCT_SIMULATION_LIST, callbackHandler);
		HashMap<Long, List<ProductSimulation>> hm = callbackHandler.getResult();
		return hm;		
	}

	@Override
	public List<ProductSimulation> getProductSimulations(long productId) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("productId", productId);
		return jdbcTemplate.query(GET_PRODUCT_SIMULATION_LIST_BY_PRODUCT,
				namedParameters, productSimulationMapper);
	}
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.appContext = applicationContext;		
	}
	
}
