package capital.any.dao.base.analyticsLogin;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.base.analytics.AnalyticsLogin;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
public class AnalyticsLoginMapper implements RowMapper<AnalyticsLogin> {

	@Override
	public AnalyticsLogin mapRow(ResultSet rs, int rowNum) throws SQLException {
		AnalyticsLogin analyticsLogin = new AnalyticsLogin();
		analyticsLogin.setCountLogins(rs.getLong("count_logins"));
		return analyticsLogin;
	}

	public AnalyticsLogin mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
}
