package capital.any.dao.base.analyticsLogin;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.base.analytics.AnalyticsLoginTime;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
public class AnalyticsLoginTimeMapper implements RowMapper<AnalyticsLoginTime>{

	@Override
	public AnalyticsLoginTime mapRow(ResultSet rs, int rowNum) throws SQLException {
		AnalyticsLoginTime analyticsLoginTime = new AnalyticsLoginTime();
		analyticsLoginTime.setTimeOfDay(rs.getInt("time_of_day"));
		analyticsLoginTime.setCountLogins(rs.getLong("count_logins"));
		return analyticsLoginTime;
	}

	public AnalyticsLoginTime mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
}
