package capital.any.dao.base.analyticsLogin;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import capital.any.model.base.analytics.AnalyticsLogin;
import capital.any.model.base.analytics.AnalyticsLoginTime;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository
public class AnalyticsLoginDao implements IAnalyticsLoginDao {
	private static final Logger logger = LoggerFactory.getLogger(AnalyticsLoginDao.class);
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private AnalyticsLoginMapper analyticsLoginMapper;
	@Autowired
	private AnalyticsLoginTimeMapper analyticsLoginTimeMapper;
	
	public AnalyticsLogin get() {
		AnalyticsLogin analyticsLogin = null;
		try {
			analyticsLogin = jdbcTemplate.queryForObject(GET, new MapSqlParameterSource(), analyticsLoginMapper);
		} catch (Exception e) {
			logger.error("Can't find AnalyticsLogin. " + e);
		}
		return analyticsLogin;
	}

	@Override
	public List<AnalyticsLoginTime> getByTime() {
		List<AnalyticsLoginTime> list = null;
		try {
			list = jdbcTemplate.query(GET_BY_TIME, new MapSqlParameterSource(), analyticsLoginTimeMapper);
		} catch (Exception e) {
			logger.error("Can't find AnalyticsLoginTime. " + e);
		}
		return list;
	}
}