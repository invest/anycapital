package capital.any.dao.base.productKidLanguage;

import java.util.List;

import org.springframework.stereotype.Repository;

import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.hazelcast.update.base.product.UpdateProductKidLanguageEntry;
import capital.any.model.base.Product;
import capital.any.model.base.ProductKidLanguage;

/**
 * 
 * @author Eyal.o
 *
 */
@Repository("ProductKidLanguageHCDao")
public class ProductKidLanguageHCDao implements IProductKidLanguageDao {

	@Override
	public boolean insert(ProductKidLanguage productKidLanguage) throws Exception {
		return HazelCastClientFactory.getClient().updateTransactionMap(
				productKidLanguage.getProduct().getId(), 
				new UpdateProductKidLanguageEntry(productKidLanguage), 
				Constants.MAP_PRODUCTS);
	}

	@Override
	public List<ProductKidLanguage> get(Product product) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
