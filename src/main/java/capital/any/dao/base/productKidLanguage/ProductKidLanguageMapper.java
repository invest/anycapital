package capital.any.dao.base.productKidLanguage;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.Language;
import capital.any.model.base.Product;
import capital.any.model.base.ProductKidLanguage;
import capital.any.service.base.aws.IAwsService;

/**
 * 
 * @author Eyal.o
 *
 */
@Component
public class ProductKidLanguageMapper extends MapperBase implements RowMapper<ProductKidLanguage> {

	@Autowired
	private IAwsService awsService;
	@Autowired
	private Environment environment;
	
	@Override
	public ProductKidLanguage mapRow(ResultSet rs, int rowNum) throws SQLException {
		Product product = new Product();
		product.setId(rs.getLong("product_id"));
		product.setKidName(rs.getString("p_kid_name"));
		Language language = new Language();
		language.setId(rs.getInt("language_id"));
		language.setDisplayName(rs.getString("lang_display_name"));
		language.setCode(rs.getString("lang_code"));
		ProductKidLanguage productKidLanguage = new ProductKidLanguage();
		productKidLanguage.setId(rs.getLong("id"));
		productKidLanguage.setProduct(product);
		productKidLanguage.setLanguage(language);
		productKidLanguage.setTimeCreated(convertTimestampToDate(rs.getTimestamp("time_created")));
		productKidLanguage.setUrl(environment.getProperty("aws.bucketName.cdn")  + "/" +  awsService.getSourceKeyProductKid(product, language));
		return productKidLanguage;
	}
	
	public ProductKidLanguage mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}

}
