package capital.any.dao.base.productKidLanguage;

import java.util.List;

import capital.any.model.base.Product;
import capital.any.model.base.ProductKidLanguage;

/**
 * 
 * @author Eyal.o
 *
 */
public interface IProductKidLanguageDao {
	
	public static final String INSERT_PRODUCT_KID_LANGUAGE = 
			" INSERT " +
		    " INTO product_kid_languages " +
		    " (" +
		    "    product_id ," +
		    "    language_id " +
		    "  )" +
		    "  VALUES" +
		    "  (" +
		    "    :productId ," +
		    "    :languageId " +
		    "  ) ";
	
	public static final String GET = 
			" SELECT " +
			"	pl.*, " +
			"	p.kid_name p_kid_name, " +
			"	l.code lang_code, " +
			"	l.display_name lang_display_name " +
			" FROM " +
			"	product_kid_languages pl, " +
			"	products p, " +
			"	languages l " +
			" WHERE " +
			"	pl.language_id = l.id " +
			"	and pl.product_id = p.id " +
			" 	and pl.product_id = :productId ";					
	
	boolean insert(ProductKidLanguage ProductKidLanguage) throws Exception;

	List<ProductKidLanguage> get(Product product);
	
}
