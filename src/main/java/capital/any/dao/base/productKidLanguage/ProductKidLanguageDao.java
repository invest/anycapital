package capital.any.dao.base.productKidLanguage;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.base.Product;
import capital.any.model.base.ProductKidLanguage;

/**
 * 
 * @author Eyal.o
 *
 */
@Primary
@Repository
public class ProductKidLanguageDao implements IProductKidLanguageDao {
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private ProductKidLanguageMapper productKidLanguageMapper;

	@Override
	public boolean insert(ProductKidLanguage productKidLanguage) throws Exception {
		SqlParameterSource namedParameters =
				new MapSqlParameterSource("productId", productKidLanguage.getProduct().getId())
								.addValue("languageId", productKidLanguage.getLanguage().getId());
		return (jdbcTemplate.update(INSERT_PRODUCT_KID_LANGUAGE, namedParameters)) > 0;
	}
	
	@Override
	public List<ProductKidLanguage> get(Product product) {
		SqlParameterSource namedParameters = new MapSqlParameterSource
			("productId", product.getId());
		return jdbcTemplate.query(GET, namedParameters, productKidLanguageMapper);
	}

}
