package capital.any.dao.base.productTypeProtectionLevel;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.base.ProductTypeProtectionLevel;

/**
 * @author Eyal Goren
 *
 */
@Component
public class ProductTypeProtectionLevelMapper implements RowMapper<ProductTypeProtectionLevel> {
	
	@Override
	public ProductTypeProtectionLevel mapRow(ResultSet rs, int row) throws SQLException {
		ProductTypeProtectionLevel productTypeProtectionLevel = new ProductTypeProtectionLevel();
		productTypeProtectionLevel.setId(rs.getInt("ptpl_id"));
		productTypeProtectionLevel.setName(rs.getString("ptpl_name"));
		productTypeProtectionLevel.setDisplayName(rs.getString("ptpl_display_name"));		
		return productTypeProtectionLevel;
	}
	
	public ProductTypeProtectionLevel mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
			
}
	

