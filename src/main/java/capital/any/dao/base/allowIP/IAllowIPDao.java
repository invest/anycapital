package capital.any.dao.base.allowIP;

import java.util.Map;

import capital.any.model.base.AllowIP;

/**
 * @author eranl
 *
 */
public interface IAllowIPDao {
	
	public static final String GET = 
			" SELECT " +
			"	* " +
			" FROM "	+ 
			"	allow_ip ";
				
	Map<String, AllowIP> get();
}
