package capital.any.dao.base.allowIP;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.AllowIP;


/**
 * @author eran.levy
 *
 */
@Repository("AllowIPHCDao")
public class AllowIPHCDao implements IAllowIPDao {
	private static final Logger logger = LoggerFactory.getLogger(AllowIPHCDao.class);
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, AllowIP> get() {		
		Map<String, AllowIP> list = null;		
		try {
			list = (Map<String, AllowIP>) HazelCastClientFactory.getClient().getMap(Constants.MAP_ALLOW_IP);
		} catch (Exception e) {
			logger.error("ERROR!", e);
		}
		return list;
	}
}
