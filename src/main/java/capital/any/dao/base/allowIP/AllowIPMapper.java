package capital.any.dao.base.allowIP;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import capital.any.dao.base.MapperBase;
import capital.any.model.base.AllowIP;

/**
 * @author eran.levy
 *
 */
@Component
public class AllowIPMapper extends MapperBase implements RowMapper<AllowIP> {
		
	@Override
	public AllowIP mapRow(ResultSet rs, int row) throws SQLException {		
		AllowIP allowIP = new AllowIP();
		allowIP.setIp(rs.getString("ip"));
		allowIP.setComment(rs.getString("comment"));
		return allowIP;
	}
	
	public AllowIP mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
			
}
	

