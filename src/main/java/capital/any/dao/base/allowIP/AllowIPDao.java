package capital.any.dao.base.allowIP;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import capital.any.model.base.AllowIP;

/**
 * @author eranl
 *
 */
@Repository
public class AllowIPDao implements IAllowIPDao {
	private static final Logger logger = LoggerFactory.getLogger(AllowIPDao.class);
	
	@Autowired
	protected NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private AllowIPMapper allowIPMapper;

	@Override
	public Map<String, AllowIP> get() {
		List<AllowIP> tmp = jdbcTemplate.query(GET, new BeanPropertyRowMapper<AllowIP>(AllowIP.class));
		Map<String, AllowIP> hm = tmp.stream().collect(Collectors.toMap(p-> p.getIp(), p -> p));		 		 
		return hm;
	}
	
}
