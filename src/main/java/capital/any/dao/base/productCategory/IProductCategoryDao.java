package capital.any.dao.base.productCategory;

import java.util.List;
import java.util.Map;
import capital.any.model.base.ProductCategory;

/**
 * @author eranl
 *
 */
public interface IProductCategoryDao {
	
	public static final String GET_PRODUCT_CATEGORIES_LIST = 
			" SELECT " +
			"	* " + 
			" FROM "	+ 
			"	product_categories ";
	
	public static final String GET_PRODUCT_CATEGORIES_LIST_BY_PRODUCT = 
			" SELECT " +
			"	* " + 
			" FROM "	+ 
			"	product_categories pc " +
			" WHERE " +
			"	pc.product_id = :productId";
		
	Map<Integer, ProductCategory> get();
	
	List<ProductCategory> getProductCategories(long productId);
}
