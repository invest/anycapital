package capital.any.dao.base.productCategory;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import capital.any.model.base.ProductCategory;

/**
 * @author eranl
 *
 */
@Primary
@Repository
public class ProductCategoryDao implements IProductCategoryDao {
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private ProductCategoryMapper productCategoryMapper;
	
	@Override
	public Map<Integer, ProductCategory> get() {
		List<ProductCategory> tmp = jdbcTemplate.query(GET_PRODUCT_CATEGORIES_LIST, productCategoryMapper);
		Map<Integer, ProductCategory> hm = tmp.stream().collect(Collectors.toMap(p-> p.getId(), p -> p));		 		 
		return hm;
	}

	@Override
	public List<ProductCategory> getProductCategories(long productId) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("productId", productId);
		return jdbcTemplate.query(GET_PRODUCT_CATEGORIES_LIST_BY_PRODUCT,
				namedParameters, productCategoryMapper);
	}
	
}
