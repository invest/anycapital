package capital.any.dao.base.productCategory;

import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.ProductCategory;

/**
 * @author LioR SoLoMoN
 *
 */
@Repository("ProductCategoryHCDao")
public class ProductCategoryHCDao implements IProductCategoryDao {
	private static final Logger logger = LoggerFactory.getLogger(ProductCategoryHCDao.class);
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, ProductCategory> get() {
		Map<Integer, ProductCategory> productCategories = null;		
		try {
			productCategories = (Map<Integer, ProductCategory>) HazelCastClientFactory.getClient().getMap(Constants.MAP_PRODUCT_CATEGORIES);
		} catch (Exception e) {
			logger.error("ERROR!", e);
		}
		return productCategories;
	}
	
	@Override
	public List<ProductCategory> getProductCategories(long productId) {
		logger.info("hazelcast getProductCategories not implemented");
		return null;
	}	
}
