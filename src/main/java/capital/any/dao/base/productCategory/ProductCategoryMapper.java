package capital.any.dao.base.productCategory;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.base.ProductCategory;

/**
 * @author Eran
 *
 */
@Component
public class ProductCategoryMapper implements RowMapper<ProductCategory> {
	
	@Override
	public ProductCategory mapRow(ResultSet rs, int row) throws SQLException {
		ProductCategory productCategory = new ProductCategory();
		productCategory.setId(rs.getInt("id"));
		productCategory.setName(rs.getString("name"));
		productCategory.setDisplayName(rs.getString("display_name"));
		productCategory.setCapitalProtection(rs.getInt("is_capital_protection") == 0 ? false : true);		
		productCategory.setRiskFactorsKey(rs.getString("risk_factors_key"));
		return productCategory;
	}
	
	public ProductCategory mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
			
}
	

