package capital.any.dao.base.investmentMarket;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.InvestmentMarket;

/**
 * @author Eyal G
 *
 */
@Component
public class InvestmentMarketMapper extends MapperBase implements RowMapper<InvestmentMarket> {

	@Override
	public InvestmentMarket mapRow(ResultSet rs, int row) throws SQLException {
		InvestmentMarket investment = new InvestmentMarket();
		investment.setInvestmentId(rs.getLong("investment_id"));
		investment.setMarketId(rs.getInt("market_id"));
		investment.setPrice(rs.getDouble("price"));
		return investment;
	}
	
	public InvestmentMarket mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
			
}
	

