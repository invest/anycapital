package capital.any.dao.base.investmentMarket;

import java.util.List;

import capital.any.model.base.InvestmentMarket;

/**
 * @author EyalG
 *
 */
public interface IInvestmentMarketDao {
	
	public static final String INSERT_INVESTMENT_MARKETS = 
		"INSERT " +
		"INTO investment_markets " +
	    "( " +
		    "price , " +
		    "investment_id , " +
		    "market_id " +
	    ") " +
	    "VALUES " +
	    "( " +
		    ":price , " +
		    ":investmentId , " +
		    ":marketId " +
	    ")";
	
	public static final String GET_INVESTMENT_MARKETS =
			"SELECT " +
				" * " +
			"FROM " +
				"investment_markets im " +
			"WHERE " +
				"im.investment_id = :investmentId";
	
	/**
	 * insert InvestmentMarket
	 * @param InvestmentMarket
	 */
	void insert(InvestmentMarket investmentMarket);
	
	/**
	 * insert list of investment markets
	 * @param investmentMarkets
	 */
	void insert(List<InvestmentMarket> investmentMarkets);
	
	/**
	 * select all Investment Market
	 * @return List<InvestmentMarket>
	 */
	List<InvestmentMarket> get(long investmentId);
}
