package capital.any.dao.base.investmentMarket;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Repository;

import capital.any.model.base.InvestmentMarket;

/**
 * @author EyalG
 *
 */
@Repository
public class InvestmentMarketDao implements IInvestmentMarketDao {
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private InvestmentMarketMapper investmentMarketMapper;
	
	@Override
	public void insert(InvestmentMarket investmentMarket) {		
		SqlParameterSource parameters = new BeanPropertySqlParameterSource(investmentMarket);
		jdbcTemplate.update(INSERT_INVESTMENT_MARKETS, parameters);
	}
	
	@Override
	public void insert(List<InvestmentMarket> investmentMarkets) {
		SqlParameterSource[] batch = 
				SqlParameterSourceUtils.createBatch(investmentMarkets.toArray());
		jdbcTemplate.batchUpdate(INSERT_INVESTMENT_MARKETS,	batch);
	}

	@Override
	public List<InvestmentMarket> get(long investmentId) {
		SqlParameterSource parameters = new MapSqlParameterSource
				 ("investmentId", investmentId);
		return jdbcTemplate.query(GET_INVESTMENT_MARKETS, parameters, investmentMarketMapper);
	}	
}
