package capital.any.dao.base.email;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.base.Email;

/**
 * @author Eyal Goren
 *
 */
@Repository
public class EmailDao implements IEmailDao {
	private static final Logger logger = LoggerFactory.getLogger(EmailDao.class);
	
	@Autowired
	protected NamedParameterJdbcTemplate jdbcTemplate;

	@Override
	public void insert(Email email) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("emailInfo", email.getEmailInfo())
						 .addValue("actionSourceId", email.getActionSource().getId());
		jdbcTemplate.update(INSERT, namedParameters);
	}	
}
