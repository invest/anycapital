package capital.any.dao.base.email;

import capital.any.model.base.Email;

/**
 * @author eyal.goren
 *
 */
public interface IEmailDao {
	
	public static final String INSERT = 
			" INSERT INTO emails " +
			"( " +
				" email_info, " +
				" action_source_id " +
			") " +
			" VALUES " +
			"( " +
				" :emailInfo, " +
				" :actionSourceId " +
			") ";
	
	/**
	 * insert email to send
	 * @param {@link}Email
	 */
	void insert(Email email);
}
