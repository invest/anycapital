package capital.any.dao.base.marketingLandingPage;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.MarketingLandingPage;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
public class MarketingLandingPageMapper extends MapperBase implements RowMapper<MarketingLandingPage> {
	
	@Override
	public MarketingLandingPage mapRow(ResultSet rs, int row) throws SQLException {
		MarketingLandingPage marketingLandingPage = new MarketingLandingPage();
		marketingLandingPage.setId(rs.getInt("id"));
		marketingLandingPage.setName(rs.getString("name"));
		marketingLandingPage.setPath(rs.getString("path"));
		marketingLandingPage.setWriterId(rs.getInt("writer_id"));
		marketingLandingPage.setTimeCreated(convertTimestampToDate(rs.getTimestamp("time_created")));
		marketingLandingPage.setTimeModified(convertTimestampToDate(rs.getTimestamp("time_modified")));
		return marketingLandingPage;		
	}
	
	public MarketingLandingPage mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
}
