package capital.any.dao.base.marketingLandingPage;

import java.util.ArrayList;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.hazelcast.core.IMap;
import com.hazelcast.query.SqlPredicate;

import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.hazelcast.update.base.marketingLandingPage.UpdateMarketingLandingPageEntry;
import capital.any.model.base.MarketingLandingPage;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository("MarketingLandingPageHCDao")
public class MarketingLandingPageHCDao implements IMarketingLandingPageDao {
	private static final Logger logger = LoggerFactory.getLogger(MarketingLandingPageHCDao.class);

	@Override
	public boolean insert(MarketingLandingPage marketingLandingPage) throws Exception {
		logger.info("MarketingLandingPageHCDao; insert; " + marketingLandingPage.toString());
		boolean result = HazelCastClientFactory.getClient().
				insertIntoTransactionMap(Constants.MAP_MARKETING_LANDING_PAGE, marketingLandingPage, marketingLandingPage.getId());
		return result;
	}

	@Override
	public boolean update(MarketingLandingPage marketingLandingPage) throws Exception {
		return HazelCastClientFactory.getClient().updateTransactionMap(
				marketingLandingPage.getId(), 
				new UpdateMarketingLandingPageEntry(marketingLandingPage), 
				Constants.MAP_MARKETING_LANDING_PAGE);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, MarketingLandingPage> getAll() {
		Map<Integer, MarketingLandingPage> mapMarketingLandingPage = null;
		try {
			mapMarketingLandingPage = (Map<Integer, MarketingLandingPage>) HazelCastClientFactory.getClient().getMap(Constants.MAP_MARKETING_LANDING_PAGE);
		} catch (Exception e) {
			logger.error("ERROR with getAll by HazelCast!", e);
		}
		return mapMarketingLandingPage;
	}

	@Override
	public MarketingLandingPage getMarketingLandingPage(int id) {
		Map<Integer, MarketingLandingPage> map = getAll();
		MarketingLandingPage marketingLandingPage = null;
		if (map != null) {
			marketingLandingPage = map.get(id);
		}
		return marketingLandingPage;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public MarketingLandingPage getByName(String name) {
		MarketingLandingPage marketingLandingPage = null;
		try {
			SqlPredicate predicate = new SqlPredicate("name = " + name);
			ArrayList<MarketingLandingPage> map = new ArrayList<MarketingLandingPage>(((IMap<Integer, MarketingLandingPage>)HazelCastClientFactory.getClient().getMap(Constants.MAP_MARKETING_LANDING_PAGE)).values(predicate));
			if (map != null && map.size() > 0) {
				marketingLandingPage = map.get(0);
			}
		} catch (Exception e) {
			logger.error("ERROR!get MarketingLandingPage by name HC", e);
		}
		return marketingLandingPage;
	}
}
