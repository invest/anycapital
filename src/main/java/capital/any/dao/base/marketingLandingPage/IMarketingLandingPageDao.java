package capital.any.dao.base.marketingLandingPage;

import java.util.Map;

import capital.any.model.base.MarketingLandingPage;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IMarketingLandingPageDao {
	
	public static final String INSERT_MARKETING_LANDING_PAGE = 
			"  INSERT " +
		    "  INTO marketing_landing_pages " +
			"  ( " +
		    "     name, " +
		    "     path, " +
		    " 	  writer_id " +
		    "  ) " +
		    "  VALUES " +
		    "  ( " +
		    " 	  :name, " +
		    " 	  :path, " +
		    " 	  :writerId " +
		    "  ) ";
	
	public static final String UPDATE_MARKETING_LANDING_PAGE =
			" UPDATE " +
				" marketing_landing_pages mlp " +
			" SET " +
				" mlp.name = :name, " +
				" mlp.path = :path, " +
				" mlp.writer_id = :writerId, " +
				" mlp.time_modified = now() " +
			" WHERE " +
				" mlp.id = :id ";
	
	public static final String GET_MARKETING_LANDING_PAGES =
			" SELECT " +
				" mlp.*, " + 
				" w.user_name as writer_name " +
			" FROM "	+ 
				" marketing_landing_pages mlp, " +
				" writers w " +
			" WHERE " +
		        " mlp.writer_id = w.id " +
			" ORDER BY " +
				" mlp.name ";
	
	public static final String GET_MARKETING_LANDING_PAGE =
			" SELECT " +
				" * " + 
			" FROM "	+ 
				" marketing_landing_pages mlp " +
			" WHERE " +
				" mlp.id = :id ";
	
	public static final String GET_BY_NAME =
			" SELECT " +
				" * " + 
			" FROM "	+ 
				" marketing_landing_pages mlp " +
			" WHERE " +
				" mlp.name = :name ";
	
	/**
	 * Insert marketing landing page 
	 * @param marketingLandingPage
	 */
	boolean insert(MarketingLandingPage marketingLandingPage) throws Exception;
	
	/**
	 * Update marketing landing page
	 * @param marketingLandingPage
	 */
	boolean update(MarketingLandingPage marketingLandingPage) throws Exception;
	
	/**
	 * Get all marketing landing page
	 * @return Map<Integer, MarketingLandingPage>
	 */
	Map<Integer, MarketingLandingPage> getAll();
	
	/**
	 * Get marketing landing page
	 * @param id
	 * @return MarketingLandingPage
	 */
	MarketingLandingPage getMarketingLandingPage(int id);

	/**
	 * Get by name
	 * @param name
	 * @return MarketingLandingPage
	 */
	MarketingLandingPage getByName(String name);

}
