package capital.any.dao.base.marketingLandingPage;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.base.MarketingLandingPage;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository
public class MarketingLandingPageDao implements IMarketingLandingPageDao, ApplicationContextAware {
	private static final Logger logger = LoggerFactory.getLogger(MarketingLandingPageDao.class);

	private ApplicationContext appContext;
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private MarketingLandingPageMapper marketingLandingPageMapper;
	
	@Override
	public boolean insert(MarketingLandingPage marketingLandingPage) throws Exception {
		SqlParameterSource namedParameters =
				new MapSqlParameterSource("name", marketingLandingPage.getName())
								.addValue("path", marketingLandingPage.getPath())
								.addValue("writerId", marketingLandingPage.getWriterId()); 
		return (jdbcTemplate.update(INSERT_MARKETING_LANDING_PAGE, namedParameters)) > 0;
	}

	@Override
	public boolean update(MarketingLandingPage marketingLandingPage) throws Exception {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("id", marketingLandingPage.getId())
				.addValue("name", marketingLandingPage.getName())
				.addValue("path", marketingLandingPage.getPath())
				.addValue("writerId", marketingLandingPage.getWriterId());
		return (jdbcTemplate.update(UPDATE_MARKETING_LANDING_PAGE, namedParameters)) > 0;		
	}

	@Override
	public Map<Integer, MarketingLandingPage> getAll() {		
		MarketingLandingPageCallbackHandler callbackHandler = appContext.getBean(MarketingLandingPageCallbackHandler.class);
		jdbcTemplate.query(GET_MARKETING_LANDING_PAGES, callbackHandler);	
		return callbackHandler.getResult();
	}

	@Override
	public MarketingLandingPage getMarketingLandingPage(int id) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("id", id);
		MarketingLandingPage marketingLandingPage = null;
		try {
			marketingLandingPage = jdbcTemplate.queryForObject(GET_MARKETING_LANDING_PAGE, namedParameters, marketingLandingPageMapper);
		} catch (Exception e) {
			logger.error("can't find marketing landing page!", e.getMessage());
		}
		return marketingLandingPage;
	}
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.appContext = applicationContext;
	}

	@Override
	public MarketingLandingPage getByName(String name) {
		MarketingLandingPage marketingLandingPage = null;
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("name", name);
		try {
			marketingLandingPage = jdbcTemplate.queryForObject(GET_BY_NAME, params, marketingLandingPageMapper);
		} catch (Exception e) {
			logger.error("Can't find marketingLandingPage by name: " + name);
		}
		return marketingLandingPage;
	}
}
