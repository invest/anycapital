package capital.any.dao.base.marketingLandingPage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Component;

import capital.any.model.base.MarketingLandingPage;
import capital.any.model.base.Writer;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
@Scope("prototype")
public class MarketingLandingPageCallbackHandler implements RowCallbackHandler {
	
	private Map<Integer, MarketingLandingPage> result = new HashMap<Integer, MarketingLandingPage>();
	
	@Autowired
	private MarketingLandingPageMapper MarketingLandingPageMapper;

	@Override
	public void processRow(ResultSet rs) throws SQLException {
		Writer writer = new Writer();
		writer.setUserName(rs.getString("writer_name"));
		MarketingLandingPage marketingLandingPage = MarketingLandingPageMapper.mapRow(rs);
		marketingLandingPage.setWriter(writer);
		result.put(marketingLandingPage.getId(), marketingLandingPage);
	}

	public Map<Integer, MarketingLandingPage> getResult() {
		return result;
	}

	public void setResult(Map<Integer, MarketingLandingPage> result) {
		this.result = result;
	}
}
