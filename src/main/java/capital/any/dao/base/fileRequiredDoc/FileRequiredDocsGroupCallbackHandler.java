package capital.any.dao.base.fileRequiredDoc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Component;

import capital.any.dao.base.fileGroup.FileGroupMapper;
import capital.any.dao.base.fileStatus.FileStatusMapper;
import capital.any.dao.base.fileType.FileTypeMapper;
import capital.any.model.base.File;
import capital.any.model.base.FileGroup;
import capital.any.model.base.FileRequiredDoc;
import capital.any.model.base.FileRequiredDocGroup;
import capital.any.model.base.FileStatus;
import capital.any.model.base.FileType;

/**
 * 
 * @author Eyal.o
 *
 */
@Component
@Scope("prototype")
public class FileRequiredDocsGroupCallbackHandler implements RowCallbackHandler {
	private Map<Integer, FileRequiredDocGroup> result = new HashMap<Integer, FileRequiredDocGroup>();
	@Autowired
	private FileTypeMapper fileTypeMapper;
	@Autowired
	private FileStatusMapper fileStatusMapper;
	@Autowired
	private FileGroupMapper fileGroupMapper;
	
	@Override
	public void processRow(ResultSet rs) throws SQLException {
		FileType fileType = fileTypeMapper.mapRow(rs);
		FileStatus fileStatus = fileStatusMapper.mapRow(rs);
		FileGroup fileGroup = fileGroupMapper.mapRow(rs);
		File file = new File();
		file.setName(rs.getString("f_name"));
		FileRequiredDoc fileRequiredDoc = new FileRequiredDoc(fileType, fileStatus, file);
		FileRequiredDocGroup fileRequiredDocGroup = result.get(fileGroup.getId());
		if (null == fileRequiredDocGroup) {
			fileRequiredDocGroup = new FileRequiredDocGroup();
			fileRequiredDocGroup.setFileGroup(fileGroup);
			fileRequiredDocGroup.setMapFileRequiredDocs(new HashMap<Integer, FileRequiredDoc>());
			result.put(fileGroup.getId(), fileRequiredDocGroup);
		}
		Map<Integer, FileRequiredDoc> map = fileRequiredDocGroup.getMapFileRequiredDocs();
		map.put(fileType.getId(), fileRequiredDoc);
	}

	/**
	 * @return the result
	 */
	public Map<Integer, FileRequiredDocGroup> getResult() {
		return result;
	}
	
}
