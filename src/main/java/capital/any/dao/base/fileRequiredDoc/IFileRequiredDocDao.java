package capital.any.dao.base.fileRequiredDoc;

import java.util.List;

import capital.any.model.base.FileRequiredDocGroup;

/**
 * 
 * @author Eyal.o
 *
 */
public interface IFileRequiredDocDao {
	
	public static final String GET_REQUIRED_DOCS = 
			" SELECT " + 
				" fg.id as fg_id, " +
				" fg.name as fg_name, " +
				" fg.display_name as fg_display_name, " +
				" ft.*, " +
				" fs.id as fs_id, " +
				" fs.name as fs_name, " +
				" fs.display_name as fs_display_name, " +
				" fs.rank as fs_rank, " +
				" f.name as f_name " +
			" FROM " + 
			    " file_group_types fgt, " +
			    " file_groups fg, " +
			    " file_types ft LEFT JOIN files f on (f.file_type_id = ft.id and f.user_id = :userId and f.is_primary = 1) " +
								" LEFT JOIN file_statuses fs on fs.id = f.status_id " +
			" where " +
				" fgt.type_id = ft.id " +
				" and fgt.group_id = fg.id " +
			" order by " + 
				" fg.id, " +
				" ft.id, " +
				" fs.rank desc";
	
	/**
	 * Get required documents
	 * @param userId
	 * @return List<FileRequiredDocGroup>
	 */
	List<FileRequiredDocGroup> getRequiredDocs(long userId);
}
