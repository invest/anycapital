package capital.any.dao.base.fileRequiredDoc;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import capital.any.model.base.FileRequiredDocGroup;

/**
 * 
 * @author Eyal.o
 *
 */
@Repository
public class FileRequiredDocDao implements IFileRequiredDocDao, ApplicationContextAware {
	
	private ApplicationContext appContext;
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	@Override
	public List<FileRequiredDocGroup> getRequiredDocs(long userId) {
		FileRequiredDocsGroupCallbackHandler fileRequiredDocsGroupCallbackHandler = appContext.getBean(FileRequiredDocsGroupCallbackHandler.class);
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("userId", userId);
		jdbcTemplate.query(GET_REQUIRED_DOCS, params, fileRequiredDocsGroupCallbackHandler);
		return new ArrayList<FileRequiredDocGroup>(fileRequiredDocsGroupCallbackHandler.getResult().values());
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.appContext = applicationContext;
	}
	
}
