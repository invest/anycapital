package capital.any.dao.base.login;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.Login;
import capital.any.model.base.MarketingTracking;

/**
 * @author Eran
 *
 */
@Component
public class LoginMapper extends MapperBase implements RowMapper<Login> {

	@Override
	public Login mapRow(ResultSet rs, int row) throws SQLException {
		Login login = new Login();
		login.setId(rs.getLong("id"));
		login.setUserId(rs.getLong("user_id"));
		login.setActionSourceId(rs.getInt("action_source_id"));
		login.setTimeCreated(convertTimestampToDate(rs.getTimestamp("time_created")));
		login.setUserAgent(rs.getString("user_agent"));
		MarketingTracking marketingTracking = new MarketingTracking();
		marketingTracking.setId(rs.getLong("marketing_tracking_id"));
		login.setMarketingTracking(marketingTracking);
		return login;
	}
				
}
