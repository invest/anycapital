package capital.any.dao.base.login;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import capital.any.model.base.Login;

/**
 * @author eranl
 *
 */
@Repository
public class LoginDao implements ILoginDao {
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private LoginMapper loginMapper;
	
	@Override
	public ArrayList<Login> getLogins() {
		return (ArrayList<Login>) jdbcTemplate.query(GET_LOGINS_LIST, loginMapper);		
	}

}
