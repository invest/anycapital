package capital.any.dao.base.login;

import java.util.ArrayList;

import capital.any.model.base.Login;

/**
 * @author eranl
 *
 */
public interface ILoginDao {
	
	public static final String GET_LOGINS_LIST = 
												" SELECT " +
												"	* " + 
												" FROM "	+ 
												"	logins ";

	ArrayList<Login> getLogins();

}
