package capital.any.dao.base.balanceHistory;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.BalanceHistory;
import capital.any.model.base.BalanceHistory.BalanceHistoryCommand;

/**
 * @author LioR SoLoMoN
 *
 */
@Component
public class BalanceHistoryMapper extends MapperBase implements RowMapper<BalanceHistory>{
	
	@Override
	public BalanceHistory mapRow(ResultSet rs, int row) throws SQLException {
		BalanceHistory balanceHistory = new BalanceHistory();
		balanceHistory.setBalance(rs.getLong("balance"));
		balanceHistory.setBalanceHistoryCommand(BalanceHistoryCommand.getById(rs.getByte("balance_history_command_id")));
		balanceHistory.setReferenceId(rs.getLong("reference_id"));
		balanceHistory.setUserId(rs.getLong("user_id"));
		balanceHistory.setTimeCreated(convertTimestampToDate(rs.getTimestamp("time_created")));
		
		return balanceHistory;
	}
	
	public BalanceHistory mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
}
