package capital.any.dao.base.product;

import java.util.List;
import java.util.Map;
import capital.any.base.enums.ProductStatusEnum;
import capital.any.model.base.Product;

/**
 * @author eranl
 *
 */
public interface IProductDao {
	
	public static final String GET_PRODUCTS_LIST = 
			" SELECT " +
			"	* " + 
			" FROM "	+ 
			"	products ";
	
	public static final String GET_PRODUCT = 
			" SELECT " +
			"	* " + 
			" FROM "	+ 
			"	products p " +
			" WHERE " +
			"	p.id = :id ";
	
	public static final String GET_HOME_PAGE_PRODUCTS = 
			" SELECT " +
			"	* " + 
			" FROM "	+ 
			"	products ";
	
	public static final String UPDATE_PRODUCTS_STATUSES = 
			" UPDATE " +
			" 	products " +
			" SET " +
			"   product_status_id = :statusId " +
			" WHERE "	+ 
			"	id = :id ";
		
	Map<Long, Product> getProducts();
	
	Product getProduct(long id);
	
	List<Product> getHomePageProducts();

	/**
	 * update the product status
	 * @param productId the id of the product to update
	 * @param productStatusId the new status for the product
	 * @throws Exception 
	 */
	boolean updateStatus(long productId, ProductStatusEnum productStatus) throws Exception;

}
