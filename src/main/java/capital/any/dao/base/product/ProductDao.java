package capital.any.dao.base.product;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import capital.any.base.enums.ProductStatusEnum;
import capital.any.model.base.Product;

/**
 * @author eranl
 *
 */
@Primary
@Repository
public class ProductDao implements IProductDao {
	private static final Logger logger = LoggerFactory.getLogger(ProductDao.class);
	
	@Autowired
	protected NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	protected ProductMapper productMapper;

	@Override
	public Map<Long, Product> getProducts() {
		List<Product> tmp = jdbcTemplate.query(GET_PRODUCTS_LIST, productMapper);
		Map<Long, Product> hm = tmp.stream().collect(Collectors.toMap(p-> p.getId(), p -> p));		 		 
		return hm;
	}

	@Override
	public Product getProduct(long id) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("id", id);
		Product product = null;
		try {
			product = (Product)jdbcTemplate.queryForObject(GET_PRODUCT, namedParameters, productMapper);
		} catch (DataAccessException e) {
			logger.error("can't find product!", e);
		}
		return product;
	}

	@Override
	public List<Product> getHomePageProducts() {
		return jdbcTemplate.query(GET_HOME_PAGE_PRODUCTS, productMapper);
	}

	@Override
	public boolean updateStatus(long productId, ProductStatusEnum productStatus) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("id", productId).addValue("statusId", productStatus.getId());
		return (jdbcTemplate.update(UPDATE_PRODUCTS_STATUSES, namedParameters) > 0);
	}
}
