package capital.any.dao.base.product;

import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import capital.any.base.enums.ProductStatusEnum;
import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.hazelcast.update.base.product.UpdateProductStatusEntry;
import capital.any.model.base.Product;

/**
 * @author LioR SoLoMoN
 *
 */
@Repository("ProductHCDao")
public class ProductHCDao implements IProductDao {
	private static final Logger logger = LoggerFactory.getLogger(ProductHCDao.class);

	@SuppressWarnings("unchecked")
	@Override
	public Map<Long, Product> getProducts() {	
		Map<Long, Product> products = null;
		try {
			products = (Map<Long, Product>) HazelCastClientFactory.getClient().getMap(Constants.MAP_PRODUCTS);
		} catch (Exception e) {
			logger.error("ERROR!", e);
		}
		return products;
	}

	@Override
	public Product getProduct(long id) {
		Map<Long, Product> products = getProducts();
		Product product = null;
		if (products != null) {
			product = products.get(id);
		}
		return product;
	}

	@Override
	public List<Product> getHomePageProducts() {
		logger.info("ProductHCDao; getHomePageProducts return null");
		return null;
	}

	@Override
	public boolean updateStatus(long productId, ProductStatusEnum productStatus) throws Exception {	
		return HazelCastClientFactory.getClient().updateTransactionMap(
				productId, 
				new UpdateProductStatusEntry(productStatus), 
				Constants.MAP_PRODUCTS);
	}
}
