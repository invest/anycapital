package capital.any.dao.base.product;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import capital.any.dao.base.MapperBase;
import capital.any.model.base.DayCountConvention;
import capital.any.model.base.Product;
import capital.any.model.base.ProductCouponFrequency;
import capital.any.model.base.ProductMaturity;
import capital.any.model.base.ProductStatus;
import capital.any.model.base.ProductType;

/**
 * @author Eran
 *
 */
@Component
public class ProductMapper extends MapperBase implements RowMapper<Product> {
	@Override
	public Product mapRow(ResultSet rs, int row) throws SQLException {
		Product product = new Product();
		product.setId(rs.getLong("id"));
		product.setTimeCreated(convertTimestampToDate(rs.getTimestamp("time_created")));
		product.setProductStatus(new ProductStatus(rs.getInt("product_status_id")));
		ProductType productType = new ProductType();
		productType.setId(rs.getInt("product_type_id"));
		product.setProductType(productType);
		product.setIssuePrice(rs.getDouble("issue_price"));
		product.setCurrencyId(rs.getInt("currency_id"));
		product.setDenomination(rs.getLong("denomination"));
		ProductCouponFrequency pcf = new ProductCouponFrequency();
		pcf.setId(rs.getInt("product_coupon_frequency_id"));
		product.setProductCouponFrequency(pcf);
		DayCountConvention dayCountConvention = new DayCountConvention();
		dayCountConvention.setId(rs.getInt("day_count_convention_id"));
		product.setDayCountConvention(dayCountConvention);
		product.setMaxYield(rs.getDouble("max_yield"));
		product.setMaxYieldPA(rs.getDouble("max_yield_p_a"));
		product.setLevelOfProtection(rs.getDouble("level_of_protection"));
		product.setLevelOfParticipation(rs.getDouble("level_of_participation"));
		product.setStrikeLevel(rs.getDouble("strike_level"));
		product.setBonusLevel(rs.getDouble("bonus_level"));
		product.setSubscriptionStartDate(convertTimestampToDate(rs.getTimestamp("subscription_start_date")));
		product.setSubscriptionEndDate(convertTimestampToDate(rs.getTimestamp("subscription_end_date")));
		product.setInitialFixingDate(convertTimestampToDate(rs.getTimestamp("initial_fixing_date")));
		product.setIssueDate(convertTimestampToDate(rs.getTimestamp("issue_date")));
		product.setFirstExchangeTradingDate(convertTimestampToDate(rs.getTimestamp("first_exchange_trading_date")));
		product.setLastTradingDate(convertTimestampToDate(rs.getTimestamp("last_trading_date")));
		product.setFinalFixingDate(convertTimestampToDate(rs.getTimestamp("final_fixing_date")));
		product.setRedemptionDate(convertTimestampToDate(rs.getTimestamp("redemption_date")));
		product.setSuspend(rs.getInt("is_suspend") == 0 ? false : true);
		product.setQuanto(rs.getInt("is_quanto") == 0 ? false : true);
		product.setProductInsuranceTypeId(rs.getInt("product_insurance_type_id"));
		product.setBid(rs.getDouble("bid"));
		product.setAsk(rs.getDouble("ask"));
		product.setFormulaResult(new BigDecimal(String.valueOf(rs.getDouble("formula_result"))));
		product.setBondFloorAtissuance(rs.getDouble("bond_floor_at_issuance"));
		product.setIssueSize(rs.getLong("issue_size"));
		product.setPriority(rs.getInt("priority"));
		product.setCapLevel(rs.getDouble("cap_level"));
		ProductMaturity productMaturity = new ProductMaturity();
		productMaturity.setId(rs.getInt("product_maturity_id"));
		product.setProductMaturity(productMaturity);
		return product;
	}
	
	public Product mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
			
}