package capital.any.dao.base.product;

import java.util.Date;
import java.util.Map.Entry;

import org.joda.time.DateTimeComparator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.hazelcast.query.Predicate;
import capital.any.model.base.Product;

/**
 * 
 * @author eyal.goren
 * get only products that open and have this market price
 */
public class ProductsSubscriptionStartDatePredicate implements Predicate<Long, Product>{

	private static final long serialVersionUID = 7160003415099352037L;
	private static final Logger logger = LoggerFactory.getLogger(ProductsSubscriptionStartDatePredicate.class);

	private Date subscriptionStartDate;
	
	public ProductsSubscriptionStartDatePredicate(Date subscriptionStartDate) {
		super();
		this.subscriptionStartDate = subscriptionStartDate;
	}

	@Override
	public boolean apply(Entry<Long, Product> mapEntry) {
		Product product  = mapEntry.getValue();
		logger.debug("compare product " + product.getSubscriptionStartDate() + " with filter " + subscriptionStartDate + " result " + DateTimeComparator.getDateOnlyInstance().compare(product.getSubscriptionStartDate(), subscriptionStartDate));
		if (DateTimeComparator.getDateOnlyInstance().compare(product.getSubscriptionStartDate(), subscriptionStartDate) == 0) {
			logger.debug("found product with subscription Start Date " + subscriptionStartDate);
			return true;
		}
		return false;
	}
}
