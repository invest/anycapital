package capital.any.dao.base.analyticsUser;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import capital.any.model.base.analytics.AnalyticsUser;
import capital.any.model.base.analytics.AnalyticsUserCampaign;
import capital.any.model.base.analytics.AnalyticsUserTime;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository
public class AnalyticsUserDao implements IAnalyticsUserDao {
	private static final Logger logger = LoggerFactory.getLogger(AnalyticsUserDao.class);
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private AnalyticsUserMapper analyticsUserMapper;
	@Autowired
	private AnalyticsUserCampaignMapper analyticsUserCampaignMapper;
	@Autowired
	private AnalyticsUserTimeMapper analyticsUserTimeMapper;

	@Override
	public AnalyticsUser get() {
		AnalyticsUser analyticsUser = null;
		try {
			analyticsUser = jdbcTemplate.queryForObject(GET, new MapSqlParameterSource(), analyticsUserMapper); 
		} catch (Exception e) {
			logger.error("Can't find AnalyticsUser. " + e);
		}
		return analyticsUser;
	}

	@Override
	public List<AnalyticsUserCampaign> getByCampaign() {
		List<AnalyticsUserCampaign> list = null;
		try {
			list = jdbcTemplate.query(GET_BY_CAMPAIGN, new MapSqlParameterSource(), analyticsUserCampaignMapper);
		} catch (Exception e) {
			logger.error("Can't find AnalyticsUserCampaign. " + e);
		}
		return list;
	}

	@Override
	public List<AnalyticsUserTime> getByTime() {
		List<AnalyticsUserTime> list = null;
		try {
			list = jdbcTemplate.query(GET_BY_TIME, new MapSqlParameterSource(), analyticsUserTimeMapper);
		} catch (Exception e) {
			logger.error("Can't find AnalyticsUserTime. " + e);
		}
		return list;
	}

}
