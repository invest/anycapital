package capital.any.dao.base.analyticsUser;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.base.analytics.AnalyticsUserTime;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
public class AnalyticsUserTimeMapper implements RowMapper<AnalyticsUserTime> {

	@Override
	public AnalyticsUserTime mapRow(ResultSet rs, int rowNum) throws SQLException {
		AnalyticsUserTime analyticsUserTime = new AnalyticsUserTime();
		analyticsUserTime.setTimeOfDay(rs.getInt("time_of_day"));
		analyticsUserTime.setCountUsers(rs.getLong("count_users"));
		return analyticsUserTime;
	}

	public AnalyticsUserTime mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
}
