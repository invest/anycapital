package capital.any.dao.base.analyticsUser;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.base.analytics.AnalyticsUser;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
public class AnalyticsUserMapper implements RowMapper<AnalyticsUser> {

	@Override
	public AnalyticsUser mapRow(ResultSet rs, int rowNum) throws SQLException {
		AnalyticsUser analyticsUser = new AnalyticsUser();
		analyticsUser.setCountUsers(rs.getLong("count_users"));
		return analyticsUser;
	}

	public AnalyticsUser mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
}
