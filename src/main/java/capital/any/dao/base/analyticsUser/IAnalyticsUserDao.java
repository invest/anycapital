package capital.any.dao.base.analyticsUser;

import java.util.List;

import capital.any.model.base.analytics.AnalyticsUser;
import capital.any.model.base.analytics.AnalyticsUserCampaign;
import capital.any.model.base.analytics.AnalyticsUserTime;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IAnalyticsUserDao {
	
	public static final String GET = 
			" SELECT " +
				" * " +
			" FROM " +
				" analytics_user ";
	
	public static final String GET_BY_CAMPAIGN = 
			" SELECT " + 
				" * " +
			" FROM " +
				" analytics_user_by_campaign ";
	
	public static final String GET_BY_TIME = 
			" SELECT " + 
				" * " +
			" FROM " +
				" analytics_user_by_time ";
	
	AnalyticsUser get();
	
	List<AnalyticsUserCampaign> getByCampaign();
	
	List<AnalyticsUserTime> getByTime();
}
