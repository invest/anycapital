package capital.any.dao.base.analyticsUser;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.base.analytics.AnalyticsUserCampaign;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
public class AnalyticsUserCampaignMapper implements RowMapper<AnalyticsUserCampaign>{

	@Override
	public AnalyticsUserCampaign mapRow(ResultSet rs, int rowNum) throws SQLException {
		AnalyticsUserCampaign analyticsUserCampaign = new AnalyticsUserCampaign();
		analyticsUserCampaign.setMarketingCampaignId(rs.getLong("campaign_id"));
		analyticsUserCampaign.setMarketingCampaignName(rs.getString("campaign_name"));
		analyticsUserCampaign.setCountUsers(rs.getLong("count_users"));
		return analyticsUserCampaign;
	}

	public AnalyticsUserCampaign mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
}
