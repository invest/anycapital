package capital.any.dao.base.transactionPaymentTypes;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.TransactionPaymentType;

/**
 * @author LioR SoLoMoN
 *
 */
@Repository("TransactionPaymentTypeHCDao")
public class TransactionPaymentTypeHCDao implements ITransactionPaymentTypeDao {
	private static final Logger logger = LoggerFactory.getLogger(TransactionPaymentTypeHCDao.class);
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, TransactionPaymentType> get() {
		Map<Integer, TransactionPaymentType> transactionPaymentType = null;		
		try {
			transactionPaymentType = (Map<Integer, TransactionPaymentType>) HazelCastClientFactory.getClient().getMap(Constants.MAP_TRANSACTION_PAYMENT_TYPES);
		} catch (Exception e) {
			logger.error("ERROR!", e);
		}
		return transactionPaymentType;	
	}
}
