package capital.any.dao.base.transactionPaymentTypes;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import capital.any.model.base.TransactionPaymentType;

/**
 * @author LioR SoLoMoN
 *
 */
@Primary
@Repository
public class TransactionPaymentTypeDao implements ITransactionPaymentTypeDao {	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private TransactionPaymentTypeMapper TransactionPaymentTypeMapper;

	@Override
	public Map<Integer, TransactionPaymentType> get() {
		List<TransactionPaymentType> tmp = jdbcTemplate.query(GET_TRANSACTION_PAYMENT_TYPE, TransactionPaymentTypeMapper);
		return tmp.stream().collect(Collectors.toMap(p-> p.getId(), p -> p));		 		 
	}
}
