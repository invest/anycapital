package capital.any.dao.base.transactionPaymentTypes;

import java.util.Map;
import capital.any.model.base.TransactionPaymentType;

/**
 * @author LioR SoLoMoN
 *
 */
public interface ITransactionPaymentTypeDao {
	
	public static final String GET_TRANSACTION_PAYMENT_TYPE = 
			"SELECT " +
			"	id tpt_id " + 
			"	,name tpt_name " +
			"	,transaction_class_id " +
			"	,display_name tpt_display_name " +
			"	,object_class " +
			"	,handler_class " +
			"FROM "	+ 
			"	transaction_payment_types ";
		
	Map<Integer, TransactionPaymentType> get();

}
