package capital.any.dao.base.transactionPaymentTypes;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import capital.any.model.base.TransactionPaymentType;

/**
 * @author LioR SoLoMoN
 *
 */
@Component
public class TransactionPaymentTypeMapper implements RowMapper<TransactionPaymentType> {	
	@Override
	public TransactionPaymentType mapRow(ResultSet rs, int row) throws SQLException {
		TransactionPaymentType transactionPaymentType = new TransactionPaymentType();		
		transactionPaymentType.setId(rs.getInt("tpt_id"));
		transactionPaymentType.setName(rs.getString("tpt_name"));		
		//transactionPaymentType.setTransactionClassId(TransactionClass.get(rs.getInt("tpt_transaction_class_id")));
		transactionPaymentType.setDisplayName(rs.getString("tpt_display_name"));
		transactionPaymentType.setObjectClass(rs.getString("object_class"));
		transactionPaymentType.setHandlerClass(rs.getString("handler_class"));
		return transactionPaymentType;
	}
	
	public TransactionPaymentType mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}			
}
	

