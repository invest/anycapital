package capital.any.dao.base.investmentPossibilityTabs;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.InvestmentPossibilityTab;

/**
 * @author Eyal G
 *
 */
@Component
public class InvestmentPossibilityTabsMapper extends MapperBase implements RowMapper<InvestmentPossibilityTab> {

	@Override
	public InvestmentPossibilityTab mapRow(ResultSet rs, int row) throws SQLException {
		InvestmentPossibilityTab investmentPossibilityTab = new InvestmentPossibilityTab();
		investmentPossibilityTab.setAmount(rs.getLong("amount"));
		investmentPossibilityTab.setInvestmentPossibilitiesId(rs.getInt("investment_possibilities_id"));
		investmentPossibilityTab.setPosition(rs.getInt("position"));
		return investmentPossibilityTab;
	}
	
	public InvestmentPossibilityTab mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}			
}
	

