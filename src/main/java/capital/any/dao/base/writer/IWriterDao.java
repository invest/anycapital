package capital.any.dao.base.writer;

import java.util.Map;
import capital.any.model.base.Writer;

/**
 * @author Eran
 *
 */
public interface IWriterDao {
	
	public static final String INSERT = 			 
			" INSERT " +
			" INTO writers " +
			" ( " +
			"     user_name, " +
			"     password, " +
			"     first_name, " +
			"     last_name, " +
			"     email, " +
			"     comments, " + 
//			"     utc_offset, " +
			"     group_id " +
			"   ) " +
			"   VALUES " +
			"   ( " +
			"     :user_name, " +
			"     :password, " +
			"     :first_name, " +
			"     :last_name, " +
			"     :email, " +
			"     :comments, " + 
//			"     :utc_offset, " +
			"     :group_id " +
			"  ) ";
	
	public static final String UPDATE = 
			" UPDATE " +
			"	writers " +
			" SET " +
			"	user_name = :user_name " +
			"	,first_name = :first_name " +
			"	,last_name = :last_name " +
			"	,email = :email " +
			"	,comments = :comments " +
			"	,is_active = :is_active " +
			//"	,utc_offset = :utc_offset  " +
			"	,group_id = :group_id " +
			" WHERE " +
			"	id = :id ";
	
	public static final String GET = 
			"	SELECT " +
			"		*	" +	
			"	FROM " +
			"		writers ";
	
	public static final String GET_WRITER_BY_USERNAME = 
			"	SELECT " +
			"		*	" +	
			"	FROM " +
			"		writers " +
			"	WHERE " +
			"		lower(writers.user_name) = lower(:username) ";

	public static final String GET_WRITER_BY_ID = 
			"	SELECT " +
			"		* " +
			"	FROM " +
			"		writers " +
			"	WHERE " +
			"		writers.id = :id ";
	
	public static final String GET_WRITER_BY_EMAIL = 
			"	SELECT " +
			"		* " +
			"	FROM " +
			"		writers " +
			"	WHERE " +
			"		lower(writers.email) = lower(:email) ";	
	
	public static final String GET_WRITER_BY_EMAIL_OR_USER_NAME = 
			"	SELECT " +
			"		* " +
			"	FROM " +
			"		writers w " +
			"	WHERE " +
			"		lower(w.email) = lower(:emailORusername) " +
			"		OR lower(w.user_name) = lower(:emailORusername) " +
			"		AND w.is_active = 1 ";
	
	
	
			
	
	Writer getWriterByUsername(String username);
	
	Writer getWriterById(Integer id);
	
	Writer getWriterByEmail(String email);

	Map<Integer, Writer> get();

	/**
	 * @param emailOrUserName
	 * @return
	 */
	Writer getWriterByEmailOrUserName(String emailOrUserName);
	
	/**
	 * Insert writer
	 * @param writer
	 */
	boolean insert(Writer writer)  throws Exception;
	
	/**
	 * Update writer
	 * @param writer
	 */
	boolean update(Writer writer)  throws Exception;
		
}
