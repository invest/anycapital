package capital.any.dao.base.writer;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import capital.any.model.base.Writer;

/**
 * @author eranl
 *
 */
@Repository
public class WriterDao implements IWriterDao, ApplicationContextAware {
	private static final Logger logger = LoggerFactory.getLogger(WriterDao.class);
	private ApplicationContext appContext;
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private WriterMapper writerMapper;

	@Override
	public Writer getWriterByUsername(String username) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("username", username);
		Writer writer = null;
		try {
			writer = jdbcTemplate.queryForObject(GET_WRITER_BY_USERNAME, params, writerMapper); 
		} catch (EmptyResultDataAccessException e) {
			//logger.error("ERROR! ", e);
		}
		return writer;
	}
	
	@Override
	public Writer getWriterById(Integer id) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("id", id);
		Writer writer = null;
		try {
			writer = jdbcTemplate.queryForObject(GET_WRITER_BY_ID, params, writerMapper); 
		} catch (EmptyResultDataAccessException e) {
			//logger.error("ERROR! ", e);
		}
		return writer;		
	}
	
	@Override
	public Writer getWriterByEmail(String email) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("email", email);
		Writer writer = null;
		try {
			writer = jdbcTemplate.queryForObject(GET_WRITER_BY_EMAIL, params, writerMapper); 
		} catch (EmptyResultDataAccessException e) {
			//logger.error("ERROR! ", e);
		}
		return writer;
	}
	
	@Override
	public Writer getWriterByEmailOrUserName(String emailOrUserName) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("emailORusername", emailOrUserName);
		Writer writer = null;
		try {
			writer = jdbcTemplate.queryForObject(GET_WRITER_BY_EMAIL_OR_USER_NAME, params, writerMapper); 
		} catch (EmptyResultDataAccessException e) {
			//logger.error("ERROR! ", e);
		}
		return writer;
	}
	
	
	
//	@Override
//	public Map<Writer, List<Role>> getWritersWithRoles() {
//		return null;
////		UserRoleCallbackHandler callbackHandler = appContext.getBean(UserRoleCallbackHandler.class); 
////		jdbcTemplate.query(GET_USERS_WITH_ROLE, callbackHandler);
////		Map<User, List<Role>> map = callbackHandler.getResult();
////		return map;
//	}

	@Override
	public void setApplicationContext(ApplicationContext appContext) throws BeansException {
		this.appContext = appContext;
		
	}
	
	@Override
	public Map<Integer, Writer> get() {
		List<Writer> tmp = jdbcTemplate.query(GET, writerMapper);
		return tmp.stream().collect(Collectors.toMap(p-> p.getId(), p -> p));		 		 
	}

	@Override
	public boolean insert(Writer writer) {		
		KeyHolder keyHolder = new GeneratedKeyHolder();
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource
				("user_name", writer.getUserName())
				.addValue("password", writer.getPassword())
				.addValue("first_name", writer.getFirstName())
				.addValue("last_name", writer.getLastName())				
				.addValue("email", writer.getEmail())
				.addValue("comments", writer.getComments())
				.addValue("utc_offset", writer.getUtcOffset())
				.addValue("group_id", writer.getGroupId());
		boolean result = (jdbcTemplate.update(INSERT, namedParameters, keyHolder, new String[] {"id"}) > 0);
		writer.setId(keyHolder.getKey().intValue());
		return result;
	}

	@Override
	public boolean update(Writer writer) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("id", writer.getId())
				.addValue("user_name", writer.getUserName())
				.addValue("first_name", writer.getFirstName())
				.addValue("last_name", writer.getLastName())
				.addValue("email", writer.getEmail())
				.addValue("is_active", writer.isActive())
				.addValue("comments", writer.getComments())	
				.addValue("utc_offset", writer.getUtcOffset())
				.addValue("group_id", writer.getGroupId());
		return (jdbcTemplate.update(UPDATE, namedParameters) > 0);	
	}

}
