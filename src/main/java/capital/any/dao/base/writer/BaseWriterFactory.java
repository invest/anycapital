package capital.any.dao.base.writer;

import capital.any.model.base.AbstractWriter;

/**
 * @author LioR SoLoMoN
 *
 */
public interface BaseWriterFactory {
	/**
	 * @param type
	 * @return
	 */
	public AbstractWriter createWriter(Integer id);
}
