package capital.any.dao.base.writer;

import org.springframework.stereotype.Component;
import capital.any.model.base.AbstractWriter;
import capital.any.model.base.NullWriter;
import capital.any.model.base.Writer;

/**
 * @author LioR SoLoMoN
 *
 */
@Component
public class WriterFactory implements BaseWriterFactory {

	@Override
	public AbstractWriter createWriter(Integer id) {
		AbstractWriter writer = null;
		if (id == 0 || id == null) {
			writer = new NullWriter();
		} else {
			writer = new Writer();
		}
		return writer;
	}
}
