package capital.any.dao.base.writer;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.Writer;

/**
 * @author Eran
 *
 */
@Component
public class WriterMapper  extends MapperBase implements RowMapper<Writer> { 
	
	@Override
	public Writer mapRow(ResultSet rs, int row) throws SQLException {
		Writer writer = new Writer();
		writer.setUserName(rs.getString("user_name"));
		writer.setIsActive(rs.getInt("is_active") == 1 ? true : false);
		writer.setTimeCreated(convertTimestampToDate(rs.getTimestamp("time_created")));
		writer.setEmail(rs.getString("email"));
		writer.setFirstName(rs.getString("first_name"));
		writer.setLastName(rs.getString("last_name"));
		writer.setId(rs.getInt("id"));		
		writer.setPassword(rs.getString("password"));
		writer.setUtcOffset(rs.getInt("utc_offset"));
		writer.setGroupId(rs.getInt("group_id"));
		return writer;
	}
	
	public Writer mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
	
}
	

