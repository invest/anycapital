package capital.any.dao.base.writer;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.hazelcast.update.base.writer.UpdateWriterEntry;
import capital.any.model.base.Writer;

/**
 * @author LioR SoLoMoN
 *
 */
@Repository("WriterHCDao")
public class WriterHCDao implements IWriterDao {
	private static final Logger logger = LoggerFactory.getLogger(WriterHCDao.class);
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, Writer> get() {
		Map<Integer, Writer> writers = null;		
		try {
			writers = (Map<Integer, Writer>) HazelCastClientFactory.getClient().getMap(Constants.MAP_WRITERS);
		} catch (Exception e) {
			logger.error("ERROR!", e);
		}
		return writers;
	}

	@Override
	public Writer getWriterByUsername(String username) {
		logger.info("hazelcast getWriterByUsername not implemetned");
		return null;
	}

	@Override
	public Writer getWriterById(Integer id) {
		logger.info("hazelcast getWriterById not implemetned");
		return null;
	}

	@Override
	public Writer getWriterByEmail(String email) {
		logger.info("hazelcast getWriterByEmail not implemetned");
		return null;
	}

	@Override
	public Writer getWriterByEmailOrUserName(String emailOrUserName) {
		logger.info("hazelcast getWriterByEmailOrUserName not implemetned");
		return null;
	}

	@Override
	public boolean insert(Writer writer) throws Exception {
		logger.info("WriterHCDao; insert; " + writer);
		boolean result = HazelCastClientFactory.getClient().
				insertIntoTransactionMap(Constants.MAP_WRITERS, writer, writer.getId());
		return result;
	}

	@Override
	public boolean update(Writer writer) throws Exception {
		logger.info("WriterHCDao; update; " + writer);
		return HazelCastClientFactory.getClient().updateTransactionMap(
				writer.getId(), 
				new UpdateWriterEntry(writer), 
				Constants.MAP_WRITERS);
	}
}
