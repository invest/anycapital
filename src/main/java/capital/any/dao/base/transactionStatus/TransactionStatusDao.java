package capital.any.dao.base.transactionStatus;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import capital.any.model.base.TransactionStatus;


/**
 * @author LioR SoLoMoN
 *
 */
@Primary
@Repository
public class TransactionStatusDao implements ITransactionStatusDao {
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private TransactionStatusMapper transactionStatusMapper;

	@Override
	public Map<Integer, TransactionStatus> get() {
		List<TransactionStatus> tmp = jdbcTemplate.query(GET_TRANSACTION_STATUS, transactionStatusMapper);
		return tmp.stream().collect(Collectors.toMap(p-> p.getId(), p -> p));		 		 
	}
}
