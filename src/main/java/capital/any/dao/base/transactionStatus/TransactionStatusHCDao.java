package capital.any.dao.base.transactionStatus;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.TransactionStatus;

/**
 * @author LioR SoLoMoN
 *
 */
@Repository("TransactionStatusHCDao")
public class TransactionStatusHCDao implements ITransactionStatusDao {
	private static final Logger logger = LoggerFactory.getLogger(TransactionStatusHCDao.class);
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, TransactionStatus> get() {
		Map<Integer, TransactionStatus> transactionStatus = null;		
		try {
			transactionStatus = (Map<Integer, TransactionStatus>) HazelCastClientFactory.getClient().getMap(Constants.MAP_TRANSACTION_STATUSES);
		} catch (Exception e) {
			logger.error("ERROR!", e);
		}
		return transactionStatus;	
	}
}
