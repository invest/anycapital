package capital.any.dao.base.transactionStatus;

import java.util.Map;
import capital.any.model.base.TransactionStatus;

/**
 * @author LioR SoLoMoN
 *
 */
public interface ITransactionStatusDao {
	
	public static final String GET_TRANSACTION_STATUS = 
			"SELECT " +
			"	id  ts_id " + 
			"	,name  ts_name " +
			"	,display_name ts_display_name " +
			"FROM "	+ 
			"	transaction_statuses ";
		
	Map<Integer, TransactionStatus> get();

}
