package capital.any.dao.base.analyticsDeposit;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.base.analytics.AnalyticsDeposit;

/**
 * 
 * @author eyal.ohana
 *
 */
@Component
public class AnalyticsDepositMapper implements RowMapper<AnalyticsDeposit>{

	@Override
	public AnalyticsDeposit mapRow(ResultSet rs, int rowNum) throws SQLException {
		AnalyticsDeposit analyticsDeposit = new AnalyticsDeposit();
		analyticsDeposit.setFtdCount(rs.getInt("ftd_count"));
		analyticsDeposit.setFtdSum(rs.getLong("ftd_sum"));
		analyticsDeposit.setDespositesCount(rs.getInt("desposites_count"));
		analyticsDeposit.setDespositesSum(rs.getLong("desposites_sum"));
		return analyticsDeposit;
	}

	public AnalyticsDeposit mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
}
