package capital.any.dao.base.analyticsDeposit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import capital.any.model.base.analytics.AnalyticsDeposit;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository
public class AnalyticsDepositDao implements IAnalyticsDepositDao {
	private static final Logger logger = LoggerFactory.getLogger(AnalyticsDepositDao.class);

	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private AnalyticsDepositMapper analyticsDepositMapper;
	
	@Override
	public AnalyticsDeposit get() {
		AnalyticsDeposit analyticsDeposit = null;
		try {
			analyticsDeposit = jdbcTemplate.queryForObject(GET, new MapSqlParameterSource(), analyticsDepositMapper);
		} catch (Exception e) {
			logger.error("Can't find AnalyticsDeposit. " + e);
		}
		return analyticsDeposit;
	}

}
