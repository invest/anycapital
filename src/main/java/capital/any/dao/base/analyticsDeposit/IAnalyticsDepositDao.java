package capital.any.dao.base.analyticsDeposit;

import capital.any.model.base.analytics.AnalyticsDeposit;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IAnalyticsDepositDao {
	
	public static final String GET = 
			" SELECT " +
				" * " +
			" FROM " +
				" analytics_deposit ";

	AnalyticsDeposit get();
}
