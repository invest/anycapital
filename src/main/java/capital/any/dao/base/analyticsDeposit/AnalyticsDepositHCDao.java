package capital.any.dao.base.analyticsDeposit;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.hazelcast.core.IMap;

import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.analytics.AnalyticsDeposit;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository("AnalyticsDepositHCDao")
public class AnalyticsDepositHCDao implements IAnalyticsDepositDao {
	private static final Logger logger = LoggerFactory.getLogger(AnalyticsDepositHCDao.class);

	@SuppressWarnings("unchecked")
	@Override
	public AnalyticsDeposit get() {
		AnalyticsDeposit analyticsDeposit = null;
		try {
			ArrayList<AnalyticsDeposit> list = new ArrayList<AnalyticsDeposit>(((IMap<Integer, AnalyticsDeposit>)HazelCastClientFactory.getClient().getMap(Constants.MAP_ANALYTICS_DEPOSIT)).values());
			if (list != null && list.size() > 0) {
				analyticsDeposit = list.get(0);
			}
		} catch (Exception e) {
			logger.error("Error! get AnalyticsDeposit by HC.");
		}
		return analyticsDeposit;
	}

}
