package capital.any.model.base;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Eyal G
 *
 */
public class ProductHistoryAction implements Serializable {	
	
	private static final long serialVersionUID = 3783833893810503943L;
	private int id;
	private String name;
	
	public ProductHistoryAction(int id) {
		super();
		this.id = id;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	@JsonIgnore
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ProductHistoryAction [id=" + id + ", name=" + name + "]";
	}
	
	
}
