package capital.any.model.base;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonIgnore;

import capital.any.base.enums.InvestmentTypeEnum;

/**
 * @author eyal.goren
 *
 */
public class Investment implements Serializable {
	// empty interface for validation
	public interface InsertInvestmentValidatorGroup{};
	/**
	 * 
	 */
	private static final long serialVersionUID = -7458474787902937009L;
	
	private long id;
	private User user;
	@Min(groups = InsertInvestmentValidatorGroup.class, message ="error.investmet.productId.empty", value = 1)
	private long productId;
	private InvestmentStatus investmentStatus;
	private long amount;
	private long returnAmount;
	@NotNull(groups = InsertInvestmentValidatorGroup.class, message ="error.investmet.investmentType.empty")
	private InvestmentType investmentType;
	private double ask;
	private double bid;
	private double rate;
	private Date timeCreated;
	private List<InvestmentMarket> investmentMarkets;
	private InvestmentHistory investmentHistory;
	private long originalAmount;
	private long originalReturnAmount;
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}
	/**
	 * @return the productId
	 */
	public long getProductId() {
		return productId;
	}
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(long productId) {
		this.productId = productId;
	}

	/**
	 * @return the investmentStatus
	 */
	public InvestmentStatus getInvestmentStatus() {
		return investmentStatus;
	}
	/**
	 * @param investmentStatus the investmentStatus to set
	 */
	public void setInvestmentStatus(InvestmentStatus investmentStatus) {
		this.investmentStatus = investmentStatus;
	}
	/**
	 * @param investmentType the investmentType to set
	 */
	public void setInvestmentType(InvestmentType investmentType) {
		this.investmentType = investmentType;
	}
	/**
	 * @return the investmentType
	 */
	public InvestmentType getInvestmentType() {
		return investmentType;
	}
	/**
	 * @return the amount
	 */
	public long getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(long amount) {
		this.amount = amount;
	}
	/**
	 * @return the returnAmount
	 */
	public long getReturnAmount() {
		return returnAmount;
	}
	/**
	 * @param returnAmount the returnAmount to set
	 */
	public void setReturnAmount(long returnAmount) {
		this.returnAmount = returnAmount;
	}
	
	/**
	 * @return the ask
	 */
	public double getAsk() {
		return ask;
	}
	/**
	 * @param ask the ask to set
	 */
	public void setAsk(double ask) {
		this.ask = ask;
	}
	/**
	 * @return the bid
	 */
	public double getBid() {
		return bid;
	}
	/**
	 * @param bid the bid to set
	 */
	public void setBid(double bid) {
		this.bid = bid;
	}
	/**
	 * @return the rate
	 */
	public double getRate() {
		return rate;
	}
	/**
	 * @param rate the rate to set
	 */
	public void setRate(double rate) {
		this.rate = rate;
	}
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	/**
	 * @return the investmentHistory
	 */
	public InvestmentHistory getInvestmentHistory() {
		return investmentHistory;
	}
	/**
	 * @param investmentHistory the investmentHistory to set
	 */
	public void setInvestmentHistory(InvestmentHistory investmentHistory) {
		this.investmentHistory = investmentHistory;
	}
	
	/**
	 * @return the investmentMarkets
	 */
	public List<InvestmentMarket> getInvestmentMarkets() {
		return investmentMarkets;
	}
	/**
	 * @param investmentMarkets the investmentMarkets to set
	 */
	public void setInvestmentMarkets(List<InvestmentMarket> investmentMarkets) {
		this.investmentMarkets = investmentMarkets;
	}
	/**
	 * @return the originalAmount
	 */
	public long getOriginalAmount() {
		return originalAmount;
	}
	/**
	 * @param originalAmount the originalAmount to set
	 */
	public void setOriginalAmount(long originalAmount) {
		this.originalAmount = originalAmount;
	}
	/**
	 * @return the originalReturnAmount
	 */
	public long getOriginalReturnAmount() {
		return originalReturnAmount;
	}
	/**
	 * @param originalReturnAmount the originalReturnAmount to set
	 */
	public void setOriginalReturnAmount(long originalReturnAmount) {
		this.originalReturnAmount = originalReturnAmount;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Investment [id=" + id + ", user=" + user + ", productId=" + productId + ", investmentStatus="
				+ investmentStatus + ", amount=" + amount + ", returnAmount=" + returnAmount + ", investmentType="
				+ investmentType + ", ask=" + ask + ", bid=" + bid + ", rate=" + rate + ", timeCreated=" + timeCreated
				+ ", investmentMarkets=" + investmentMarkets + ", investmentHistory=" + investmentHistory
				+ ", originalAmount=" + originalAmount + ", originalReturnAmount=" + originalReturnAmount + "]";
	}
	
	@JsonIgnore
	public BigDecimal getDenomination() {
		BigDecimal hunderd = new BigDecimal("100");
		BigDecimal amount = new BigDecimal(String.valueOf(getOriginalAmount()));
		if (investmentType.getId() == InvestmentTypeEnum.SECONDARY.getId()) {
			BigDecimal ask = new BigDecimal(String.valueOf(getAsk())).divide(hunderd);
			amount = amount.divide(ask, 20, RoundingMode.HALF_UP);
		}
		return amount;
	}
	
}