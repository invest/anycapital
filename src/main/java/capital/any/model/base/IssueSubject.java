package capital.any.model.base;

import java.io.Serializable;

/**
 * 
 * @author eyal.ohana
 *
 */
public class IssueSubject implements Serializable {
	private static final long serialVersionUID = 378036623672099582L;
	
	private int id;
	private String name;
	
	/**
	 * 
	 */
	public IssueSubject() {
		
	}
	
	/**
	 * 
	 * @param id
	 */
	public IssueSubject(int id) {
		this.id = id;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "IssueSubject [id=" + id + ", name=" + name + "]";
	}
}
