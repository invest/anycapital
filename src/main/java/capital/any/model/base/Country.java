package capital.any.model.base;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author eranl
 * 
 */
public class Country implements Serializable {


	private static final long serialVersionUID = -8136237684540455801L;
	private int id;
	private String countryName;
	private String a2;
	private String a3;
	private String phoneCode;
	private String displayName;
	private String supportPhone;
	private String supportFax;
	private String gmtOffset;
	private CountryRisk countryRisk;
	private int defaultLanguageId;
	private int isBlocked;
	
	/**
	 * Country
	 */
	public Country() {
		
	}
	
	/**
	 * Country
	 * @param id
	 */
	public Country(int id) {
		this.id = id;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the countryName
	 */
	@JsonIgnore
	public String getCountryName() {
		return countryName;
	}
	/**
	 * @param countryName the countryName to set
	 */
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	/**
	 * @return the a2
	 */
	public String getA2() {
		return a2;
	}
	/**
	 * @param a2 the a2 to set
	 */
	public void setA2(String a2) {
		this.a2 = a2;
	}
	/**
	 * @return the a3
	 */
	public String getA3() {
		return a3;
	}
	/**
	 * @param a3 the a3 to set
	 */
	public void setA3(String a3) {
		this.a3 = a3;
	}
	/**
	 * @return the phoneCode
	 */
	public String getPhoneCode() {
		return phoneCode;
	}
	/**
	 * @param phoneCode the phoneCode to set
	 */
	public void setPhoneCode(String phoneCode) {
		this.phoneCode = phoneCode;
	}
	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}
	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	/**
	 * @return the supportPhone
	 */
	public String getSupportPhone() {
		return supportPhone;
	}
	/**
	 * @param supportPhone the supportPhone to set
	 */
	public void setSupportPhone(String supportPhone) {
		this.supportPhone = supportPhone;
	}
	/**
	 * @return the supportFax
	 */
	public String getSupportFax() {
		return supportFax;
	}
	/**
	 * @param supportFax the supportFax to set
	 */
	public void setSupportFax(String supportFax) {
		this.supportFax = supportFax;
	}
	/**
	 * @return the gmtOffset
	 */
	public String getGmtOffset() {
		return gmtOffset;
	}
	/**
	 * @param gmtOffset the gmtOffset to set
	 */
	public void setGmtOffset(String gmtOffset) {
		this.gmtOffset = gmtOffset;
	}

	/**
	 * @return the countryRisk
	 */
	public CountryRisk getCountryRisk() {
		return countryRisk;
	}

	/**
	 * @param countryRisk the countryRisk to set
	 */
	public void setCountryRisk(CountryRisk countryRisk) {
		this.countryRisk = countryRisk;
	}

	/**
	 * @return the defaultLanguageId
	 */
	public int getDefaultLanguageId() {
		return defaultLanguageId;
	}

	/**
	 * @param defaultLanguageId the defaultLanguageId to set
	 */
	public void setDefaultLanguageId(int defaultLanguageId) {
		this.defaultLanguageId = defaultLanguageId;
	}

	/**
	 * @return the isBlocked
	 */
	public int getIsBlocked() {
		return isBlocked;
	}

	/**
	 * @param isBlocked the isBlocked to set
	 */
	public void setIsBlocked(int isBlocked) {
		this.isBlocked = isBlocked;
	}
	
	/**
	 * 
	 *
	 */
	public enum CountryBlock {
		APPROVE(0), 		
		BLOCKED(1);
				
		private int id;
					
		/**
		 * @param token
		 */
		private CountryBlock(int id) {
			this.id = id;
		}

		/**
		 * @return the id
		 */
		public int getId() {
			return id;
		}

		/**
		 * @param id the id to set
		 */
		public void setId(int id) {
			this.id = id;
		}		
				
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Country [id=" + id + ", countryName=" + countryName + ", a2=" + a2 + ", a3=" + a3 + ", phoneCode="
				+ phoneCode + ", displayName=" + displayName + ", supportPhone=" + supportPhone + ", supportFax="
				+ supportFax + ", gmtOffset=" + gmtOffset + ", countryRisk=" + countryRisk + ", defaultLanguageId="
				+ defaultLanguageId + ", isBlocked=" + isBlocked + "]";
	}	
}
