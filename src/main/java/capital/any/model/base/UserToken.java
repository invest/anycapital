package capital.any.model.base;

import java.io.Serializable;
import java.util.Date;

/**
 * @author LioR SoLoMoN
 *
 */
public class UserToken implements Serializable {
	private static final long serialVersionUID = 386007451295775986L;
	private String token;
	private long userId;
	private Date timeCreated;
	
	/**
	 * @param token
	 */
	public UserToken(String token) {
		this.token = token;
	}
	
	/**
	 * @param token
	 * @param userId
	 */
	public UserToken(String token, long userId) {
		this.token = token;
		this.userId = userId;
	}
	
	/**
	 * @param token
	 * @param userId
	 * @param timeCreated
	 */
	public UserToken(String token, long userId, Date timeCreated) {
		super();
		this.token = token;
		this.userId = userId;
		this.timeCreated = timeCreated;
	}

	@Override
	public String toString() {
		return "UserToken [token=" + token + ", userId=" + userId + "]";
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}

	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
}
