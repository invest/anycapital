package capital.any.model.base.anyoption;

import java.io.Serializable;
import capital.any.model.base.User;

/**
 * @author LioR SoLoMoN
 *
 */
public class ResponseLogin implements Serializable {
	private static final long serialVersionUID = 4688557128596145842L;
	protected User data;
    protected int errorCode;
    protected String apiCode; //API error code
    protected String apiCodeDescription; //API error code description  
    
	/**
	 * @return the user
	 */
	public User getData() {
		return data;
	}
	/**
	 * @param user the user to set
	 */
	public void setData(User user) {
		this.data = user;
	}
	/**
	 * @return the errorCode
	 */
	public int getErrorCode() {
		return errorCode;
	}
	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	/**
	 * @return the apiCode
	 */
	public String getApiCode() {
		return apiCode;
	}
	/**
	 * @param apiCode the apiCode to set
	 */
	public void setApiCode(String apiCode) {
		this.apiCode = apiCode;
	}
	/**
	 * @return the apiCodeDescription
	 */
	public String getApiCodeDescription() {
		return apiCodeDescription;
	}
	/**
	 * @param apiCodeDescription the apiCodeDescription to set
	 */
	public void setApiCodeDescription(String apiCodeDescription) {
		this.apiCodeDescription = apiCodeDescription;
	}
}
