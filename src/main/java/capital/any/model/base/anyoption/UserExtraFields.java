package capital.any.model.base.anyoption;

import java.io.Serializable;

/**
 * @author LioR SoLoMoN
 *
 */
public class UserExtraFields implements Serializable {
	private static final long serialVersionUID = -1208478229387429903L;
	private long id;
	private int aoUserId;
	private boolean isRegulated; //kyc
	private int vipStatusId; 

	@Override
	public String toString() {
		return "UserAnycapitalExtraFields [id=" + id + ", aoUserId=" + aoUserId
				+ ", isRegulated=" + isRegulated + ", vipStatusId="
				+ vipStatusId + "]";
	}
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the aoUserId
	 */
	public int getAoUserId() {
		return aoUserId;
	}
	/**
	 * @param aoUserId the aoUserId to set
	 */
	public void setAoUserId(int aoUserId) {
		this.aoUserId = aoUserId;
	}
	/**
	 * @return the isRegulated
	 */
	public boolean isRegulated() {
		return isRegulated;
	}
	/**
	 * @param isRegulated the isRegulated to set
	 */
	public void setRegulated(boolean isRegulated) {
		this.isRegulated = isRegulated;
	}
	/**
	 * @return the vipStatusId
	 */
	public int getVipStatusId() {
		return vipStatusId;
	}
	/**
	 * @param vipStatusId the vipStatusId to set
	 */
	public void setVipStatusId(int vipStatusId) {
		this.vipStatusId = vipStatusId;
	}
}
