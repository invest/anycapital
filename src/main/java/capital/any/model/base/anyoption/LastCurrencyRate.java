package capital.any.model.base.anyoption;

import java.io.Serializable;
import java.util.Date;

/**
 * @author LioR SoLoMoN
 *
 */
public class LastCurrencyRate implements Serializable {
	private static final long serialVersionUID = 7993178691822605941L;
	private double rate;
	private Date timeCreated;
	private String timeCreatedStr;
	private String currencyCode;	
	
	/**
	 * @return the currencyCode
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}
	/**
	 * @param currencyCode the currencyCode to set
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	/**
	 * @return the rate
	 */
	public double getRate() {
		return rate;
	}
	/**
	 * @param rate the rate to set
	 */
	public void setRate(double rate) {
		this.rate = rate;
	}
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	/**
	 * @return the timeCreatedStr
	 */
	public String getTimeCreatedStr() {
		return timeCreatedStr;
	}
	/**
	 * @param timeCreatedStr the timeCreatedStr to set
	 */
	public void setTimeCreatedStr(String timeCreatedStr) {
		this.timeCreatedStr = timeCreatedStr;
	}
}
