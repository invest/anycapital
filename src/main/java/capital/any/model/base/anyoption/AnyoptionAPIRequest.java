package capital.any.model.base.anyoption;

import java.io.Serializable;
import capital.any.model.base.ApiUser;

/**
 * @author LioR SoLoMoN
 *
 */
public class AnyoptionAPIRequest implements Serializable {
	private static final long serialVersionUID = 5403306591947035777L;
	private ApiUser apiUser; 
	private final String locale = "en";    
	private String userName;
	private String password;
	
	/**
	 * 
	 */
	public AnyoptionAPIRequest() {
		this.apiUser = new ApiUser();
	}

	/**
	 * @param userName
	 * @param password
	 */
	public AnyoptionAPIRequest(String userName, String password) {
		this.apiUser = new ApiUser();
		this.userName = userName;
		this.password = password;
	}

	/**
	 * @return the apiUser
	 */
	public ApiUser getApiUser() {
		return apiUser;
	}

	/**
	 * @return the locale
	 */
	public String getLocale() {
		return locale;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
}
