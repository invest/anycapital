package capital.any.model.base.anyoption;

import java.io.Serializable;

/**
 * @author LioR SoLoMoN
 *
 */
public class AnyoptionAPIResponse<T> implements Serializable {
	private static final long serialVersionUID = 5869786216855441232L;
	private T data;
	private int errorCode;
	private String apiCode; //API error code
	private String apiCodeDescription; //API error code description  
    
	/**
	 * @return the data
	 */
	public Object getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(T data) {
		this.data = data;
	}

	/**
	 * @return the errorCode
	 */
	public int getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * @return the apiCode
	 */
	public String getApiCode() {
		return apiCode;
	}

	/**
	 * @param apiCode the apiCode to set
	 */
	public void setApiCode(String apiCode) {
		this.apiCode = apiCode;
	}

	/**
	 * @return the apiCodeDescription
	 */
	public String getApiCodeDescription() {
		return apiCodeDescription;
	}

	/**
	 * @param apiCodeDescription the apiCodeDescription to set
	 */
	public void setApiCodeDescription(String apiCodeDescription) {
		this.apiCodeDescription = apiCodeDescription;
	}
}
