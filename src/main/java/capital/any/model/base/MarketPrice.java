package capital.any.model.base;

import java.io.Serializable;
import java.util.Date;

public class MarketPrice implements Serializable {
	private static final long serialVersionUID = -4007992513254454697L;

	private int marketId;
	private Date priceDate;
	private int typeId;
	private double price;
	/**
	 * @return the marketId
	 */
	public int getMarketId() {
		return marketId;
	}
	/**
	 * @param marketId the marketId to set
	 */
	public void setMarketId(int marketId) {
		this.marketId = marketId;
	}
	/**
	 * @return the priceDate
	 */
	public Date getPriceDate() {
		return priceDate;
	}
	/**
	 * @param priceDate the priceDate to set
	 */
	public void setPriceDate(Date priceDate) {
		this.priceDate = priceDate;
	}
	/**
	 * @return the typeId
	 */
	public int getTypeId() {
		return typeId;
	}
	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MarketPrice [marketId=" + marketId + ", priceDate=" + priceDate + ", typeId=" + typeId + ", price="
				+ price + "]";
	}
			
}
