package capital.any.model.base;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;

import capital.any.product.type.formula.Formula;
import capital.any.product.type.formula.Parameters;

/**
 * @author eranl
 *
 */
public class ProductType implements Serializable {
	
	private static final Logger logger = LoggerFactory.getLogger(ProductType.class);
	
	private static final long serialVersionUID = 140493404410688043L;
	private int id;
	private String name;
	private String displayName;
	private ProductCategory productCategory;
	private boolean isCoupon;
	private boolean isDayCountConvention;
	private boolean isMaxYield;
	private boolean isMaxYieldPA;
	private boolean isParticipation;
	private boolean isStrikeLevel;
	private boolean isBonusPrecentage;
	private int riskLevel;
	@JsonIgnore
	private Formula formula;
	private boolean isDistanceToBarrier;
	private boolean isCapLevel;
	private ProductTypeProtectionLevel productTypeProtectionLevel;
	private int sliderMin;
	private int sliderMax;
		
	public List<ProductScenario> getProductScenarios(Parameters parameters) {
		List<ProductScenario> list = new ArrayList<ProductScenario>();
		ProductScenario productScenario;
		Method scenarioFinalFixingLevel;
		Method scenarioFinalFixingLevelForTxt;
		Method scenarioResult;
		
		for (int i = 0; i < formula.getScenarioPercentages().size() && i < 3; i++) {
			Parameters paramtersTemp = new Parameters(parameters);
			productScenario = new ProductScenario();
			productScenario.setPosition(i + 1);			
			try {
				scenarioFinalFixingLevel = formula.getClass().getMethod("getScenario" + (i + 1) + "FinalFixingLevel", Parameters.class);
				scenarioFinalFixingLevelForTxt = formula.getClass().getMethod("getScenario" + (i + 1) + "FinalFixingLevelForTxt", Parameters.class);
				scenarioResult = formula.getClass().getMethod("getScenario" + (i + 1) + "Result", Parameters.class);
				productScenario.setFinalFixingLevel(((BigDecimal) scenarioFinalFixingLevel.invoke(formula, paramtersTemp)).doubleValue()); //formula.getScenario1FinalFixingLevel(paramters).doubleValue());
				productScenario.setFinalFixingLevelForTxt((Double)scenarioFinalFixingLevelForTxt.invoke(formula, paramtersTemp));
				productScenario.setFormulaResult(((BigDecimal) scenarioResult.invoke(formula, paramtersTemp)).doubleValue());//formula.getScenario1Result(paramters).doubleValue());
				list.add(productScenario);
			} catch (Exception e) { 
				logger.debug("cant get scenario " + (i + 1), e);
			}
		}
		return list;
	}
	
	public Formula getFormula() {
		return formula;
	}
	
	public void setFormula(Formula formula) {
		this.formula = formula;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	@JsonIgnore
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}
	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	
	public ProductCategory getProductCategory() {
		return productCategory;
	}
	public void setProductCategory(ProductCategory productCategory) {
		this.productCategory = productCategory;
	}
	/**
	 * @return the isCoupon
	 */
	public boolean isCoupon() {
		return isCoupon;
	}
	/**
	 * @param isCoupon the isCoupon to set
	 */
	public void setCoupon(boolean isCoupon) {
		this.isCoupon = isCoupon;
	}
	/**
	 * @return the isDayCountConvention
	 */
	public boolean isDayCountConvention() {
		return isDayCountConvention;
	}
	/**
	 * @param isDayCountConvention the isDayCountConvention to set
	 */
	public void setDayCountConvention(boolean isDayCountConvention) {
		this.isDayCountConvention = isDayCountConvention;
	}
	/**
	 * @return the isMaxYield
	 */
	public boolean isMaxYield() {
		return isMaxYield;
	}
	/**
	 * @param isMaxYield the isMaxYield to set
	 */
	public void setMaxYield(boolean isMaxYield) {
		this.isMaxYield = isMaxYield;
	}
	/**
	 * @return the isMaxYieldPA
	 */
	public boolean isMaxYieldPA() {
		return isMaxYieldPA;
	}
	/**
	 * @param isMaxYieldPA the isMaxYieldPA to set
	 */
	public void setMaxYieldPA(boolean isMaxYieldPA) {
		this.isMaxYieldPA = isMaxYieldPA;
	}
	/**
	 * @return the isParticipation
	 */
	public boolean isParticipation() {
		return isParticipation;
	}
	/**
	 * @param isParticipation the isParticipation to set
	 */
	public void setParticipation(boolean isParticipation) {
		this.isParticipation = isParticipation;
	}
	/**
	 * @return the isStrikeLevel
	 */
	public boolean isStrikeLevel() {
		return isStrikeLevel;
	}
	/**
	 * @param isStrikeLevel the isStrikeLevel to set
	 */
	public void setStrikeLevel(boolean isStrikeLevel) {
		this.isStrikeLevel = isStrikeLevel;
	}
	/**
	 * @return the isBonusPrecentage
	 */
	public boolean isBonusPrecentage() {
		return isBonusPrecentage;
	}
	/**
	 * @param isBonusPrecentage the isBonusPrecentage to set
	 */
	public void setBonusPrecentage(boolean isBonusPrecentage) {
		this.isBonusPrecentage = isBonusPrecentage;
	}
	/**
	 * @return the riskLevel
	 */
	public int getRiskLevel() {
		return riskLevel;
	}
	/**
	 * @param riskLevel the riskLevel to set
	 */
	public void setRiskLevel(int riskLevel) {
		this.riskLevel = riskLevel;
	}
	
	
	/**
	 * @return the isDistanceToBarrier
	 */
	public boolean isDistanceToBarrier() {
		return isDistanceToBarrier;
	}

	/**
	 * @param isDistanceToBarrier the isDistanceToBarrier to set
	 */
	public void setDistanceToBarrier(boolean isDistanceToBarrier) {
		this.isDistanceToBarrier = isDistanceToBarrier;
	}
	
	/**
	 * @return the isCapLevel
	 */
	public boolean isCapLevel() {
		return isCapLevel;
	}

	/**
	 * @param isCapLevel the isCapLevel to set
	 */
	public void setCapLevel(boolean isCapLevel) {
		this.isCapLevel = isCapLevel;
	}

	/**
	 * @return the productTypeProtectionLevel
	 */
	public ProductTypeProtectionLevel getProductTypeProtectionLevel() {
		return productTypeProtectionLevel;
	}

	/**
	 * @param productTypeProtectionLevel the productTypeProtectionLevel to set
	 */
	public void setProductTypeProtectionLevel(ProductTypeProtectionLevel productTypeProtectionLevel) {
		this.productTypeProtectionLevel = productTypeProtectionLevel;
	}

	/**
	 * @return the sliderMin
	 */
	public int getSliderMin() {
		return sliderMin;
	}

	/**
	 * @param sliderMin the sliderMin to set
	 */
	public void setSliderMin(int sliderMin) {
		this.sliderMin = sliderMin;
	}

	/**
	 * @return the sliderMax
	 */
	public int getSliderMax() {
		return sliderMax;
	}

	/**
	 * @param sliderMax the sliderMax to set
	 */
	public void setSliderMax(int sliderMax) {
		this.sliderMax = sliderMax;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ProductType [id=" + id + ", name=" + name + ", displayName=" + displayName + ", productCategory="
				+ productCategory + ", isCoupon=" + isCoupon + ", isDayCountConvention=" + isDayCountConvention
				+ ", isMaxYield=" + isMaxYield + ", isMaxYieldPA=" + isMaxYieldPA + ", isParticipation="
				+ isParticipation + ", isStrikeLevel=" + isStrikeLevel + ", isBonusPrecentage=" + isBonusPrecentage
				+ ", riskLevel=" + riskLevel + ", formula=" + formula + ", isDistanceToBarrier=" + isDistanceToBarrier
				+ ", isCapLevel=" + isCapLevel + ", productTypeProtectionLevel=" + productTypeProtectionLevel
				+ ", sliderMin=" + sliderMin + ", sliderMax=" + sliderMax + "]";
	}	
}
