package capital.any.model.base;

/**
 * @author LioR SoLoMoN
 *
 */
public class GoogleShortURLResponse {
	private String kind;
	private String id;
	private String longUrl;

	@Override
	public String toString() {
		return "GoogleURLShortenerResponse [kind=" + kind + ", id=" + id + ", longUrl=" + longUrl + "]";
	}
	
	/**
	 * 
	 */
	public GoogleShortURLResponse() {

	}

	/**
	 * @param kind
	 * @param id
	 * @param longUrl
	 */
	public GoogleShortURLResponse(String kind, String id, String longUrl) {
		this.kind = kind;
		this.id = id;
		this.longUrl = longUrl;
	}

	/**
	 * @return the kind
	 */
	public String getKind() {
		return kind;
	}

	/**
	 * @param kind the kind to set
	 */
	public void setKind(String kind) {
		this.kind = kind;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the longUrl
	 */
	public String getLongUrl() {
		return longUrl;
	}

	/**
	 * @param longUrl the longUrl to set
	 */
	public void setLongUrl(String longUrl) {
		this.longUrl = longUrl;
	}
}
