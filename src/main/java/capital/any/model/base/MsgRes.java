package capital.any.model.base;

import java.io.Serializable;

/**
 * @author eranl
 *
 */
public class MsgRes implements Serializable {
	
	private static final long serialVersionUID = -7477610465880111327L;
	private int id;
	private String key;
	private int actionSourceId;
	private int typeId;
	
	/**
	 * 
	 */
	public MsgRes() {

	}
	
	/**
	 * @param typeId
	 */
	public MsgRes(int typeId) {
		this.typeId = typeId;
	}
	
	/**
	 * @param actionSourceId
	 * @param typeId
	 */
	public MsgRes(int actionSourceId, int typeId) {
		this.actionSourceId = actionSourceId;
		this.typeId = typeId;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}
	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}
	/**
	 * @return the actionSourceId
	 */
	public int getActionSourceId() {
		return actionSourceId;
	}
	/**
	 * @param actionSourceId the actionSourceId to set
	 */
	public void setActionSourceId(int actionSourceId) {
		this.actionSourceId = actionSourceId;
	}
	/**
	 * @return the typeId
	 */
	public int getTypeId() {
		return typeId;
	}
	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MsgRes [id=" + id + ", key=" + key + ", actionSourceId=" + actionSourceId + ", typeId=" + typeId + "]";
	}
		
}
