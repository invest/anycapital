package capital.any.model.base;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author eyal.ohana
 *
 */
public class RegulationDetails implements Serializable {
	private static final long serialVersionUID = -8296718246274695660L;
	
	private List<QmUserAnswer> qmUserAnswerList;
	private User user;
	private RegulationUserInfo regulationUserInfo;
	
	/**
	 * RegulationDetails
	 */
	public RegulationDetails() {
	
	}
	
	/**
	 * RegulationDetails
	 * @param qmUserAnswerList
	 * @param user
	 * @param regulationUserInfo
	 */
	public RegulationDetails(List<QmUserAnswer> qmUserAnswerList, User user, RegulationUserInfo regulationUserInfo) {
		super();
		this.qmUserAnswerList = qmUserAnswerList;
		this.user = user;
		this.regulationUserInfo = regulationUserInfo;
	}



	/**
	 * @return the qmUserAnswerList
	 */
	public List<QmUserAnswer> getQmUserAnswerList() {
		return qmUserAnswerList;
	}
	/**
	 * @param qmUserAnswerList the qmUserAnswerList to set
	 */
	public void setQmUserAnswerList(List<QmUserAnswer> qmUserAnswerList) {
		this.qmUserAnswerList = qmUserAnswerList;
	}
	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}
	/**
	 * @return the regulationUserInfo
	 */
	public RegulationUserInfo getRegulationUserInfo() {
		return regulationUserInfo;
	}
	/**
	 * @param regulationUserInfo the regulationUserInfo to set
	 */
	public void setRegulationUserInfo(RegulationUserInfo regulationUserInfo) {
		this.regulationUserInfo = regulationUserInfo;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "QmUserAnswerDetails [qmUserAnswerList=" + qmUserAnswerList + ", user=" + user + ", regulationUserInfo="
				+ regulationUserInfo + "]";
	}
}
