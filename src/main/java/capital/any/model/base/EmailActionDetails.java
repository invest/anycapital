package capital.any.model.base;

import java.io.Serializable;

/**
 * 
 * @author eyal.ohana
 * @param <T>
 *
 */
public class EmailActionDetails<T> implements Serializable {
	private static final long serialVersionUID = -7739514149255753138L;
	
	private EmailAction emailAction;
	private EmailActionRequest<T> emailActionRequest;
	
	/**
	 * EmailActionDetails
	 */
	public EmailActionDetails() {
		
	}
	
	/**
	 * EmailActionDetails
	 * @param data
	 */
	public EmailActionDetails(EmailAction emailAction, EmailActionRequest<T> emailActionRequest) {
		super();
		this.emailAction = emailAction;
		this.emailActionRequest = emailActionRequest;
	}

	/**
	 * @return the emailAction
	 */
	public EmailAction getEmailAction() {
		return emailAction;
	}

	/**
	 * @param emailAction the emailAction to set
	 */
	public void setEmailAction(EmailAction emailAction) {
		this.emailAction = emailAction;
	}

	/**
	 * @return the emailActionRequest
	 */
	public EmailActionRequest<T> getEmailActionRequest() {
		return emailActionRequest;
	}

	/**
	 * @param emailActionRequest the emailActionRequest to set
	 */
	public void setEmailActionRequest(EmailActionRequest<T> emailActionRequest) {
		this.emailActionRequest = emailActionRequest;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EmailActionDetails [emailAction=" + emailAction + ", emailActionRequest=" + emailActionRequest + "]";
	}
}
