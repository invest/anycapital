package capital.any.model.base;

import java.io.Serializable;

/**
 * @author LioR SoLoMoN
 *
 */
public class ChangePassword implements Serializable {
	private static final long serialVersionUID = -2427404974226670986L;
	private String currentPassword;
	private String newPassword;
	private String retypePassword;
	private User user;
	private boolean isChanged;
	private String token;
	
	/**
	 * 
	 */
	public ChangePassword() {
		
	}
	
	/**
	 * @param newPassword
	 * @param retypePassword
	 */
	public ChangePassword(String newPassword, String retypePassword) {
		this.newPassword = newPassword;
		this.retypePassword = retypePassword;
	}
	
	/**
	 * @param currentPassword
	 * @param newPassword
	 * @param retypePassword
	 */
	public ChangePassword(String currentPassword, String newPassword, String retypePassword) {
		this.currentPassword = currentPassword;
		this.newPassword = newPassword;
		this.retypePassword = retypePassword;
	}
	
	/**
	 * @param currentPassword
	 * @param newPassword
	 * @param retypePassword
	 * @param token
	 */
	public ChangePassword(String currentPassword, String newPassword, String retypePassword,
			String token) {
		this.currentPassword = currentPassword;
		this.newPassword = newPassword;
		this.retypePassword = retypePassword;
		this.token = token;
	}
	
	/**
	 * @param currentPassword
	 * @param newPassword
	 * @param retypePassword
	 * @param user
	 * @param token
	 */
	public ChangePassword(String currentPassword, String newPassword, String retypePassword, User user,
			String token) {
		this.currentPassword = currentPassword;
		this.newPassword = newPassword;
		this.retypePassword = retypePassword;
		this.user = user;
		this.token = token;
	}
	/**
	 * @return the currentPassword
	 */
	public String getCurrentPassword() {
		return currentPassword;
	}
	/**
	 * @param currentPassword the currentPassword to set
	 */
	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}
	/**
	 * @return the newPassword
	 */
	public String getNewPassword() {
		return newPassword;
	}
	/**
	 * @param newPassword the newPassword to set
	 */
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	/**
	 * @return the retypePassword
	 */
	public String getRetypePassword() {
		return retypePassword;
	}
	/**
	 * @param retypePassword the retypePassword to set
	 */
	public void setRetypePassword(String retypePassword) {
		this.retypePassword = retypePassword;
	}
	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}
	/**
	 * @return the isChanged
	 */
	public boolean isChanged() {
		return isChanged;
	}
	/**
	 * @param isChanged the isChanged to set
	 */
	public void setChanged(boolean isChanged) {
		this.isChanged = isChanged;
	}
	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}
	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}
}

