package capital.any.model.base;

import java.io.Serializable;
import java.util.Date;
import capital.any.base.enums.ActionSource;
public class InvestmentHistory implements Serializable {

	private static final long serialVersionUID = -4041002138563581912L;
	
	private long investmntId;
	private InvestmentStatus investmentStatus;
	private Date timeCreated;
	private int writerId;
	private ActionSource actionSource;
	private String serverName;
	private long loginId;
	private String ip;
	/**
	 * @return the investmntId
	 */
	public long getInvestmntId() {
		return investmntId;
	}
	/**
	 * @param investmntId the investmntId to set
	 */
	public void setInvestmntId(long investmntId) {
		this.investmntId = investmntId;
	}

	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	/**
	 * @return the writerId
	 */
	public int getWriterId() {
		return writerId;
	}
	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(int writerId) {
		this.writerId = writerId;
	}
	/**
	 * @return the actionSource
	 */
	public ActionSource getActionSource() {
		return actionSource;
	}
	/**
	 * @param actionSource the actionSource to set
	 */
	public void setActionSource(ActionSource actionSource) {
		this.actionSource = actionSource;
	}
	/**
	 * @return the serverName
	 */
	public String getServerName() {
		return serverName;
	}
	/**
	 * @param serverName the serverName to set
	 */
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}
		
	/**
	 * @return the investmentStatus
	 */
	public InvestmentStatus getInvestmentStatus() {
		return investmentStatus;
	}
	/**
	 * @param investmentStatus the investmentStatus to set
	 */
	public void setInvestmentStatus(InvestmentStatus investmentStatus) {
		this.investmentStatus = investmentStatus;
	}
	/**
	 * @return the loginId
	 */
	public long getLoginId() {
		return loginId;
	}
	/**
	 * @param loginId the loginId to set
	 */
	public void setLoginId(long loginId) {
		this.loginId = loginId;
	}
	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}
	/**
	 * @param ip the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "InvestmentHistory [investmntId=" + investmntId + ", investmentStatus=" + investmentStatus
				+ ", timeCreated=" + timeCreated + ", writerId=" + writerId + ", actionSource=" + actionSource
				+ ", serverName=" + serverName + ", loginId=" + loginId + ", ip=" + ip + "]";
	}
}
