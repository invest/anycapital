package capital.any.model.base;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.Min;

public class InvestmentPossibility implements Serializable{
	// empty interface for validation
	public interface GetInvestmentPossibilityValidatorGroup{};

	/**
	 * 
	 */
	private static final long serialVersionUID = -1353349796401093292L;
	private int id;
	@Min(groups = GetInvestmentPossibilityValidatorGroup.class, value = 1)
	private int currencyId;
	private long defaultAmount;
	private long maximumAmount;
	private long minimumAmount;
	private List<InvestmentPossibilityTab> investmentPossibilityTab;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the currencyId
	 */
	public int getCurrencyId() {
		return currencyId;
	}
	/**
	 * @param currencyId the currencyId to set
	 */
	public void setCurrencyId(int currencyId) {
		this.currencyId = currencyId;
	}
	/**
	 * @return the defaultAmount
	 */
	public long getDefaultAmount() {
		return defaultAmount;
	}
	/**
	 * @param defaultAmount the defaultAmount to set
	 */
	public void setDefaultAmount(long defaultAmount) {
		this.defaultAmount = defaultAmount;
	}
	/**
	 * @return the maximumAmount
	 */
	public long getMaximumAmount() {
		return maximumAmount;
	}
	/**
	 * @param maximumAmount the maximumAmount to set
	 */
	public void setMaximumAmount(long maximumAmount) {
		this.maximumAmount = maximumAmount;
	}
	/**
	 * @return the minimumAmount
	 */
	public long getMinimumAmount() {
		return minimumAmount;
	}
	/**
	 * @param minimumAmount the minimumAmount to set
	 */
	public void setMinimumAmount(long minimumAmount) {
		this.minimumAmount = minimumAmount;
	}
	
	/**
	 * @return the investmentPossibilityTab
	 */
	public List<InvestmentPossibilityTab> getInvestmentPossibilityTab() {
		return investmentPossibilityTab;
	}
	/**
	 * @param investmentPossibilityTab the investmentPossibilityTab to set
	 */
	public void setInvestmentPossibilityTab(List<InvestmentPossibilityTab> investmentPossibilityTab) {
		this.investmentPossibilityTab = investmentPossibilityTab;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "InvestmentPossibility [id=" + id + ", currencyId=" + currencyId + ", defaultAmount=" + defaultAmount
				+ ", maximumAmount=" + maximumAmount + ", minimumAmount=" + minimumAmount
				+ ", investmentPossibilityTab=" + investmentPossibilityTab + "]";
	}	
}
