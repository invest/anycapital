package capital.any.model.base;

import java.io.Serializable;
import capital.any.model.base.BalanceHistory.BalanceHistoryCommand;

/**
 * @author LioR SoLoMoN
 *
 */
public class BalanceRequest implements Serializable {
	private static final long serialVersionUID = 3884752969080840487L;
	private User user;
	private long amount;
	private BalanceHistoryCommand commandHistory;
	private long referenceId;
	private OperationType operationType;
	private UserControllerDetails userControllerDetails;

	/**
	 * empty const'
	 */
	public BalanceRequest() {
		
	}
	
	/**
	 * @param user
	 * @param amount
	 * @param commandId
	 * @param referenceId
	 * @param operationType
	 */
	public BalanceRequest(User user, long amount, BalanceHistoryCommand commandHistory, 
			long referenceId, OperationType operationType) {
		this.user = user;
		this.amount = amount;
		this.commandHistory = commandHistory;
		this.referenceId = referenceId;
		this.operationType = operationType;
	}

	public BalanceRequest(User user, long amount, BalanceHistoryCommand commandHistory, long referenceId,
			OperationType operationType, UserControllerDetails userControllerDetails) {
		this(user, amount, commandHistory, referenceId, operationType);
		this.userControllerDetails = userControllerDetails;
	}

	@Override
	public String toString() {
		return "BalanceRequest [user=" + user + ", amount=" + amount + ", commandHistory=" + commandHistory
				+ ", referenceId=" + referenceId + ", operationType=" + operationType + ", userControllerDetails="
				+ userControllerDetails + "]";
	}

	public enum OperationType {
		CREDIT, DEBIT;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}
	
	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the amount
	 */
	public long getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(long amount) {
		this.amount = amount;
	}

	/**
	 * @return the commandId
	 */
	public BalanceHistoryCommand getCommandHistory() {
		return commandHistory;
	}

	/**
	 * @param commandId the commandId to set
	 */
	public void setCommandHistory(BalanceHistoryCommand commandHistory) {
		this.commandHistory = commandHistory;
	}

	/**
	 * @return the referenceId
	 */
	public long getReferenceId() {
		return referenceId;
	}

	/**
	 * @param referenceId the referenceId to set
	 */
	public void setReferenceId(long referenceId) {
		this.referenceId = referenceId;
	}

	/**
	 * @return the operationType
	 */
	public OperationType getOperationType() {
		return operationType;
	}

	/**
	 * @param operationType the operationType to set
	 */
	public void setOperationType(OperationType operationType) {
		this.operationType = operationType;
	}

	/**
	 * @return the userControllerDetails
	 */
	public UserControllerDetails getUserControllerDetails() {
		return userControllerDetails;
	}

	/**
	 * @param userControllerDetails the userControllerDetails to set
	 */
	public void setUserControllerDetails(UserControllerDetails userControllerDetails) {
		this.userControllerDetails = userControllerDetails;
	}
}
