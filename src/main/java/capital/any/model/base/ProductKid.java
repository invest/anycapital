package capital.any.model.base;

import java.io.Serializable;

/**
 * 
 * @author eyal.o
 *
 */
public class ProductKid implements Serializable {
	private static final long serialVersionUID = -6381955101313414890L;
	
	private Product product;
	private Language language;
	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}
	/**
	 * @param product the product to set
	 */
	public void setProduct(Product product) {
		this.product = product;
	}
	/**
	 * @return the language
	 */
	public Language getLanguage() {
		return language;
	}
	/**
	 * @param language the language to set
	 */
	public void setLanguage(Language language) {
		this.language = language;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ProductKid [product=" + product + ", language=" + language + "]";
	}
}
