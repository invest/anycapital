package capital.any.model.base.analytics;

import java.io.Serializable;

/**
 * 
 * @author eyal.ohana
 *
 */
public class AnalyticsLoginTime implements Serializable {
	private static final long serialVersionUID = 1908468007107005192L;
	
	private int timeOfDay;
	private long countLogins;
	/**
	 * @return the timeOfDay
	 */
	public int getTimeOfDay() {
		return timeOfDay;
	}
	/**
	 * @param timeOfDay the timeOfDay to set
	 */
	public void setTimeOfDay(int timeOfDay) {
		this.timeOfDay = timeOfDay;
	}
	/**
	 * @return the countLogins
	 */
	public long getCountLogins() {
		return countLogins;
	}
	/**
	 * @param countLogins the countLogins to set
	 */
	public void setCountLogins(long countLogins) {
		this.countLogins = countLogins;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AnalyticsLoginTime [timeOfDay=" + timeOfDay + ", countLogins=" + countLogins + "]";
	}
}
