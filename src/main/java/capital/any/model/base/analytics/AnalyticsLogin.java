package capital.any.model.base.analytics;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author eyal.ohana
 *
 */
public class AnalyticsLogin implements Serializable {
	private static final long serialVersionUID = -5498813244290876323L;

	private long countLogins;
	private List<AnalyticsLoginTime> listAnalyticsLoginTime;
	/**
	 * @return the countLogins
	 */
	public long getCountLogins() {
		return countLogins;
	}
	/**
	 * @param countLogins the countLogins to set
	 */
	public void setCountLogins(long countLogins) {
		this.countLogins = countLogins;
	}
	/**
	 * @return the listAnalyticsLoginTime
	 */
	public List<AnalyticsLoginTime> getListAnalyticsLoginTime() {
		return listAnalyticsLoginTime;
	}
	/**
	 * @param listAnalyticsLoginTime the listAnalyticsLoginTime to set
	 */
	public void setListAnalyticsLoginTime(List<AnalyticsLoginTime> listAnalyticsLoginTime) {
		this.listAnalyticsLoginTime = listAnalyticsLoginTime;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AnalyticsLogin [countLogins=" + countLogins + ", listAnalyticsLoginTime=" + listAnalyticsLoginTime
				+ "]";
	}
}
