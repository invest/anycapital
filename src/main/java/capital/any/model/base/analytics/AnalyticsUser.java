package capital.any.model.base.analytics;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author eyal.ohana
 *
 */
public class AnalyticsUser implements Serializable {
	private static final long serialVersionUID = 1750219382829783083L;

	private long countUsers;
	private List<AnalyticsUserTime> listAnalyticsUserTime;
	private List<AnalyticsUserCampaign> listAnalyticsUserCampaign;
	/**
	 * @return the countUsers
	 */
	public long getCountUsers() {
		return countUsers;
	}
	/**
	 * @param countUsers the countUsers to set
	 */
	public void setCountUsers(long countUsers) {
		this.countUsers = countUsers;
	}
	/**
	 * @return the listAnalyticsUserTime
	 */
	public List<AnalyticsUserTime> getListAnalyticsUserTime() {
		return listAnalyticsUserTime;
	}
	/**
	 * @param listAnalyticsUserTime the listAnalyticsUserTime to set
	 */
	public void setListAnalyticsUserTime(List<AnalyticsUserTime> listAnalyticsUserTime) {
		this.listAnalyticsUserTime = listAnalyticsUserTime;
	}
	/**
	 * @return the listAnalyticsUserCampaign
	 */
	public List<AnalyticsUserCampaign> getListAnalyticsUserCampaign() {
		return listAnalyticsUserCampaign;
	}
	/**
	 * @param listAnalyticsUserCampaign the listAnalyticsUserCampaign to set
	 */
	public void setListAnalyticsUserCampaign(List<AnalyticsUserCampaign> listAnalyticsUserCampaign) {
		this.listAnalyticsUserCampaign = listAnalyticsUserCampaign;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AnalyticsUser [countUsers=" + countUsers + ", listAnalyticsUserTime=" + listAnalyticsUserTime
				+ ", listAnalyticsUserCampaign=" + listAnalyticsUserCampaign + "]";
	}
	
}
