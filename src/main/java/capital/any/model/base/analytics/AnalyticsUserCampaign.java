package capital.any.model.base.analytics;

import java.io.Serializable;

/**
 * 
 * @author eyal.ohana
 *
 */
public class AnalyticsUserCampaign implements Serializable {
	private static final long serialVersionUID = 4605229222572368753L;
	
	private long marketingCampaignId;
	private String marketingCampaignName;
	private long countUsers;
	/**
	 * @return the marketingCampaignId
	 */
	public long getMarketingCampaignId() {
		return marketingCampaignId;
	}
	/**
	 * @param marketingCampaignId the marketingCampaignId to set
	 */
	public void setMarketingCampaignId(long marketingCampaignId) {
		this.marketingCampaignId = marketingCampaignId;
	}
	/**
	 * @return the countUsers
	 */
	public long getCountUsers() {
		return countUsers;
	}
	/**
	 * @param countUsers the countUsers to set
	 */
	public void setCountUsers(long countUsers) {
		this.countUsers = countUsers;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AnalyticsUserCampaign [marketingCampaignId=" + marketingCampaignId + ", marketingCampaignName="
				+ marketingCampaignName + ", countUsers=" + countUsers + "]";
	}
	/**
	 * @return the marketingCampaignName
	 */
	public String getMarketingCampaignName() {
		return marketingCampaignName;
	}
	/**
	 * @param marketingCampaignName the marketingCampaignName to set
	 */
	public void setMarketingCampaignName(String marketingCampaignName) {
		this.marketingCampaignName = marketingCampaignName;
	}
	
}
