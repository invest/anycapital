package capital.any.model.base.analytics;

import java.io.Serializable;

/**
 * 
 * @author eyal.ohana
 *
 */
public class AnalyticsDeposit implements Serializable {
	private static final long serialVersionUID = 3967929461524009539L;
	
	private int ftdCount;
	private long ftdSum;
	private int despositesCount;
	private long despositesSum;
	/**
	 * @return the ftdCount
	 */
	public int getFtdCount() {
		return ftdCount;
	}
	/**
	 * @param ftdCount the ftdCount to set
	 */
	public void setFtdCount(int ftdCount) {
		this.ftdCount = ftdCount;
	}
	/**
	 * @return the ftdSum
	 */
	public long getFtdSum() {
		return ftdSum;
	}
	/**
	 * @param ftdSum the ftdSum to set
	 */
	public void setFtdSum(long ftdSum) {
		this.ftdSum = ftdSum;
	}
	/**
	 * @return the despositesCount
	 */
	public int getDespositesCount() {
		return despositesCount;
	}
	/**
	 * @param despositesCount the despositesCount to set
	 */
	public void setDespositesCount(int despositesCount) {
		this.despositesCount = despositesCount;
	}
	/**
	 * @return the despositesSum
	 */
	public long getDespositesSum() {
		return despositesSum;
	}
	/**
	 * @param despositesSum the despositesSum to set
	 */
	public void setDespositesSum(long despositesSum) {
		this.despositesSum = despositesSum;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AnalyticsDeposit [ftdCount=" + ftdCount + ", ftdSum=" + ftdSum + ", despositesCount=" + despositesCount
				+ ", despositesSum=" + despositesSum + "]";
	}
}
