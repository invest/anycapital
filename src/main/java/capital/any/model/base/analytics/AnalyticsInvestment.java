package capital.any.model.base.analytics;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author eyal.ohana
 *
 */
public class AnalyticsInvestment implements Serializable {
	private static final long serialVersionUID = -3796196024851985565L;
	
	private int investmentCount;
	private long investmentSum;
	private List<AnalyticsInvestmentProduct> listAnalyticsInvestmentProduct;
	/**
	 * @return the investmentCount
	 */
	public int getInvestmentCount() {
		return investmentCount;
	}
	/**
	 * @param investmentCount the investmentCount to set
	 */
	public void setInvestmentCount(int investmentCount) {
		this.investmentCount = investmentCount;
	}
	/**
	 * @return the investmentSum
	 */
	public long getInvestmentSum() {
		return investmentSum;
	}
	/**
	 * @param investmentSum the investmentSum to set
	 */
	public void setInvestmentSum(long investmentSum) {
		this.investmentSum = investmentSum;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AnalyticsInvestment [investmentCount=" + investmentCount + ", investmentSum=" + investmentSum
				+ ", listAnalyticsInvestmentProduct=" + listAnalyticsInvestmentProduct + "]";
	}
	/**
	 * @return the listAnalyticsInvestmentProduct
	 */
	public List<AnalyticsInvestmentProduct> getListAnalyticsInvestmentProduct() {
		return listAnalyticsInvestmentProduct;
	}
	/**
	 * @param listAnalyticsInvestmentProduct the listAnalyticsInvestmentProduct to set
	 */
	public void setListAnalyticsInvestmentProduct(List<AnalyticsInvestmentProduct> listAnalyticsInvestmentProduct) {
		this.listAnalyticsInvestmentProduct = listAnalyticsInvestmentProduct;
	}

}
