package capital.any.model.base.analytics;

import java.io.Serializable;

/**
 * 
 * @author eyal.ohana
 *
 */
public class AnalyticsInvestmentProduct implements Serializable {
	private static final long serialVersionUID = 3196617264107018710L;
	
	public int productTypeId;
	public String productTypeName;
	private int investmentCount;
	private long investmentSum;
	/**
	 * @return the productTypeId
	 */
	public int getProductTypeId() {
		return productTypeId;
	}
	/**
	 * @param productTypeId the productTypeId to set
	 */
	public void setProductTypeId(int productTypeId) {
		this.productTypeId = productTypeId;
	}
	/**
	 * @return the investmentCount
	 */
	public int getInvestmentCount() {
		return investmentCount;
	}
	/**
	 * @param investmentCount the investmentCount to set
	 */
	public void setInvestmentCount(int investmentCount) {
		this.investmentCount = investmentCount;
	}
	/**
	 * @return the investmentSum
	 */
	public long getInvestmentSum() {
		return investmentSum;
	}
	/**
	 * @param investmentSum the investmentSum to set
	 */
	public void setInvestmentSum(long investmentSum) {
		this.investmentSum = investmentSum;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AnalyticsInvestmentProduct [productTypeId=" + productTypeId + ", productTypeName=" + productTypeName
				+ ", investmentCount=" + investmentCount + ", investmentSum=" + investmentSum + "]";
	}
	/**
	 * @return the productTypeName
	 */
	public String getProductTypeName() {
		return productTypeName;
	}
	/**
	 * @param productTypeName the productTypeName to set
	 */
	public void setProductTypeName(String productTypeName) {
		this.productTypeName = productTypeName;
	}

}
