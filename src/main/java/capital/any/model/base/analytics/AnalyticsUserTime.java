package capital.any.model.base.analytics;

import java.io.Serializable;

/**
 * 
 * @author eyal.ohana
 *
 */
public class AnalyticsUserTime implements Serializable {
	private static final long serialVersionUID = -53664314126888956L;

	private int timeOfDay;
	private long countUsers;
	/**
	 * @return the timeOfDay
	 */
	public int getTimeOfDay() {
		return timeOfDay;
	}
	/**
	 * @param timeOfDay the timeOfDay to set
	 */
	public void setTimeOfDay(int timeOfDay) {
		this.timeOfDay = timeOfDay;
	}
	/**
	 * @return the countUsers
	 */
	public long getCountUsers() {
		return countUsers;
	}
	/**
	 * @param countUsers the countUsers to set
	 */
	public void setCountUsers(long countUsers) {
		this.countUsers = countUsers;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AnalyticsUserTime [timeOfDay=" + timeOfDay + ", countUsers=" + countUsers + "]";
	}
}
