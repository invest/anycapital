package capital.any.model.base;

import java.io.Serializable;

/**
 * 
 * @author eyal.ohana
 *
 */
public class ForgetPasswordEmailAction implements Serializable {
	private static final long serialVersionUID = 714308925555871825L;
	
	private String googleShortUrlId;

	/**
	 * ForgetPasswordEmailAction
	 */
	public ForgetPasswordEmailAction() {
		
	}
	
	/**
	 * ForgetPasswordEmailAction
	 * @param googleShortUrlId
	 */
	public ForgetPasswordEmailAction(String googleShortUrlId) {
		this.googleShortUrlId = googleShortUrlId;
	}
	
	/**
	 * @return the googleShortUrlId
	 */
	public String getGoogleShortUrlId() {
		return googleShortUrlId;
	}

	/**
	 * @param googleShortUrlId the googleShortUrlId to set
	 */
	public void setGoogleShortUrlId(String googleShortUrlId) {
		this.googleShortUrlId = googleShortUrlId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ForgetPasswordEmailAction [googleShortUrlId=" + googleShortUrlId + "]";
	}

}
