package capital.any.model.base;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author eyal.ohana
 *
 */
public class QmUserAnswer implements Serializable {
	private static final long serialVersionUID = 808217992264746942L;
	
	private long id;
	private long userId;
	private int questionId;
	private int answerId;
	private String textAnswer;
	private int actionSourceId;
	private Date timeCreated;
	private QmQuestion qmQuestion;
	private QmAnswer qmAnswer;
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the questionId
	 */
	public int getQuestionId() {
		return questionId;
	}
	/**
	 * @param questionId the questionId to set
	 */
	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}
	/**
	 * @return the answerId
	 */
	public int getAnswerId() {
		return answerId;
	}
	/**
	 * @param answerId the answerId to set
	 */
	public void setAnswerId(int answerId) {
		this.answerId = answerId;
	}
	/**
	 * @return the textAnswer
	 */
	public String getTextAnswer() {
		return textAnswer;
	}
	/**
	 * @param textAnswer the textAnswer to set
	 */
	public void setTextAnswer(String textAnswer) {
		this.textAnswer = textAnswer;
	}
	/**
	 * @return the actionSourceId
	 */
	public int getActionSourceId() {
		return actionSourceId;
	}
	/**
	 * @param actionSourceId the actionSourceId to set
	 */
	public void setActionSourceId(int actionSourceId) {
		this.actionSourceId = actionSourceId;
	}
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "QmUserAnswer [id=" + id + ", userId=" + userId + ", questionId=" + questionId + ", answerId=" + answerId
				+ ", textAnswer=" + textAnswer + ", actionSourceId=" + actionSourceId + ", timeCreated=" + timeCreated
				+ ", qmQuestion=" + qmQuestion + ", qmAnswer=" + qmAnswer + "]";
	}
	/**
	 * @return the qmQuestion
	 */
	public QmQuestion getQmQuestion() {
		return qmQuestion;
	}
	/**
	 * @param qmQuestion the qmQuestion to set
	 */
	public void setQmQuestion(QmQuestion qmQuestion) {
		this.qmQuestion = qmQuestion;
	}
	/**
	 * @return the qmAnswer
	 */
	public QmAnswer getQmAnswer() {
		return qmAnswer;
	}
	/**
	 * @param qmAnswer the qmAnswer to set
	 */
	public void setQmAnswer(QmAnswer qmAnswer) {
		this.qmAnswer = qmAnswer;
	}
	
}
