package capital.any.model.base;

import java.io.Serializable;
import javax.servlet.http.HttpServletResponse;

/**
 * @author LioR SoLoMoN
 *
 */
public class CookieInfo implements Serializable {
	private static final long serialVersionUID = -3070137299692376302L;
	private HttpServletResponse response;
	private String attributeName;
	private String attributeValue;
	private int expiry;
	
	/**
	 * @param response
	 * @param attributeName
	 * @param attributeValue
	 * @param expiry
	 */
	public CookieInfo(HttpServletResponse response, String attributeName, String attributeValue, int expiry) {
		this.response = response;
		this.attributeName = attributeName;
		this.attributeValue = attributeValue;
		this.expiry = expiry;
	}

	@Override
	public String toString() {
		return "CookieInfo [response=" + response + ", attributeName=" + attributeName + ", attributeValue="
				+ attributeValue + ", expiry=" + expiry + "]";
	}
	/**
	 * @return the response
	 */
	public HttpServletResponse getResponse() {
		return response;
	}
	/**
	 * @param response the response to set
	 */
	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}
	/**
	 * @return the attributeName
	 */
	public String getAttributeName() {
		return attributeName;
	}
	/**
	 * @param attributeName the attributeName to set
	 */
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}
	/**
	 * @return the attributeValue
	 */
	public String getAttributeValue() {
		return attributeValue;
	}
	/**
	 * @param attributeValue the attributeValue to set
	 */
	public void setAttributeValue(String attributeValue) {
		this.attributeValue = attributeValue;
	}
	/**
	 * @return the expiry
	 */
	public int getExpiry() {
		return expiry;
	}
	/**
	 * @param expiry the expiry to set
	 */
	public void setExpiry(int expiry) {
		this.expiry = expiry;
	}
}
