package capital.any.model.base;

import java.io.Serializable;

/**
 * 
 * @author eyal.ohana
 *
 */
public class IssueDirection implements Serializable {
	private static final long serialVersionUID = 8017876492379701902L;
	
	private int id;
	private String name;
	
	/**
	 * 
	 */
	public IssueDirection() {
		
	}
	
	/**
	 * 
	 * @param id
	 */
	public IssueDirection(int id) {
		this.id = id;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "IssueDirection [id=" + id + ", name=" + name + "]";
	}
}
