package capital.any.model.base;

import java.io.Serializable;

/**
 * 
 * @author eyal.ohana
 * @param <T>
 *
 */
public class EmailActionResponse<T> implements Serializable {
	private static final long serialVersionUID = 8800554783070313223L;
	
	public T data;

	/**
	 * EmailActionResponse
	 */
	public EmailActionResponse() {
		
	}
	
	/**
	 * EmailActionResponse
	 * @param list
	 */
	public EmailActionResponse(T data) {
		this.data = data;
	}

	/**
	 * @return the data
	 */
	public T getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(T data) {
		this.data = data;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EmailActionResponse [data=" + data + "]";
	}
	
	
}
