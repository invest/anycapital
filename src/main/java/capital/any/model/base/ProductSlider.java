package capital.any.model.base;

import java.io.Serializable;

/**
 * @author Eyal G
 *
 */
public class ProductSlider implements Serializable {
	
	private static final long serialVersionUID = 8039571442568631479L;
	
	private int finalFixingLevel;
	private double formulaResult;
	private double formulaResultBarrier;
	
	public ProductSlider() {}
	
	public ProductSlider(int finalFixingLevel) {
		super();
		this.finalFixingLevel = finalFixingLevel;
	}
	
	/**
	 * @return the finalFixingLevel
	 */
	public int getFinalFixingLevel() {
		return finalFixingLevel;
	}
	/**
	 * @param finalFixingLevel the finalFixingLevel to set
	 */
	public void setFinalFixingLevel(int finalFixingLevel) {
		this.finalFixingLevel = finalFixingLevel;
	}
	/**
	 * @return the formulaResult
	 */
	public double getFormulaResult() {
		return formulaResult;
	}
	/**
	 * @param formulaResult the formulaResult to set
	 */
	public void setFormulaResult(double formulaResult) {
		this.formulaResult = formulaResult;
	}
	/**
	 * @return the formulaResultBarrier
	 */
	public double getFormulaResultBarrier() {
		return formulaResultBarrier;
	}
	/**
	 * @param formulaResultBarrier the formulaResultBarrier to set
	 */
	public void setFormulaResultBarrier(double formulaResultBarrier) {
		this.formulaResultBarrier = formulaResultBarrier;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ProductSlider [finalFixingLevel=" + finalFixingLevel + ", formulaResult=" + formulaResult
				+ ", formulaResultBarrier=" + formulaResultBarrier + "]";
	}			
}
