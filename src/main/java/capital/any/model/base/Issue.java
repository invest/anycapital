package capital.any.model.base;

import java.util.Date;

/**
 * 
 * @author eyal.ohana
 *
 */
public class Issue {
	private long id;
	private String comments;
	private Boolean significantNote;
	private Date timeCreated;
	private long userId;
	private long writerId;
	private IssueChannel issueChannel;
	private IssueSubject issueSubject;
	private IssueDirection issueDirection;
	private IssueReachedStatus issueReachedStatus;
	private IssueReaction issueReaction;
	private AbstractWriter writer;
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	/**
	 * @return the significantNote
	 */
	public Boolean getSignificantNote() {
		return significantNote;
	}
	/**
	 * @param significantNote the significantNote to set
	 */
	public void setSignificantNote(Boolean significantNote) {
		this.significantNote = significantNote;
	}
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}
	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}
	/**
	 * @return the issueChannel
	 */
	public IssueChannel getIssueChannel() {
		return issueChannel;
	}
	/**
	 * @param issueChannel the issueChannel to set
	 */
	public void setIssueChannel(IssueChannel issueChannel) {
		this.issueChannel = issueChannel;
	}
	/**
	 * @return the issueSubject
	 */
	public IssueSubject getIssueSubject() {
		return issueSubject;
	}
	/**
	 * @param issueSubject the issueSubject to set
	 */
	public void setIssueSubject(IssueSubject issueSubject) {
		this.issueSubject = issueSubject;
	}
	/**
	 * @return the issueDirection
	 */
	public IssueDirection getIssueDirection() {
		return issueDirection;
	}
	/**
	 * @param issueDirection the issueDirection to set
	 */
	public void setIssueDirection(IssueDirection issueDirection) {
		this.issueDirection = issueDirection;
	}
	/**
	 * @return the issueReachedStatus
	 */
	public IssueReachedStatus getIssueReachedStatus() {
		return issueReachedStatus;
	}
	/**
	 * @param issueReachedStatus the issueReachedStatus to set
	 */
	public void setIssueReachedStatus(IssueReachedStatus issueReachedStatus) {
		this.issueReachedStatus = issueReachedStatus;
	}
	/**
	 * @return the issueReaction
	 */
	public IssueReaction getIssueReaction() {
		return issueReaction;
	}
	/**
	 * @param issueReaction the issueReaction to set
	 */
	public void setIssueReaction(IssueReaction issueReaction) {
		this.issueReaction = issueReaction;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Issue [id=" + id + ", comments=" + comments + ", significantNote=" + significantNote + ", timeCreated="
				+ timeCreated + ", userId=" + userId + ", writerId=" + writerId + ", issueChannel=" + issueChannel
				+ ", issueSubject=" + issueSubject + ", issueDirection=" + issueDirection + ", issueReachedStatus="
				+ issueReachedStatus + ", issueReaction=" + issueReaction + ", writer=" + writer + "]";
	}
	/**
	 * @return the writer
	 */
	public AbstractWriter getWriter() {
		return writer;
	}
	/**
	 * @param writer the writer to set
	 */
	public void setWriter(AbstractWriter writer) {
		this.writer = writer;
	}
	
}
