package capital.any.model.base;

import java.io.Serializable;

/**
 * 
 * @author eyal.ohana
 *
 */
public class IssueReaction implements Serializable {
	private static final long serialVersionUID = 1558850368140543083L;
	
	private int id;
	private String name;
	private int reachedStatusId;
	
	/**
	 * 
	 */
	public IssueReaction() {
		
	}
	
	/**
	 * 
	 * @param id
	 */
	public IssueReaction(int id) {
		this.id = id;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "IssueReaction [id=" + id + ", name=" + name + ", reachedStatusId=" + reachedStatusId + "]";
	}
	/**
	 * @return the reachedStatusId
	 */
	public int getReachedStatusId() {
		return reachedStatusId;
	}
	/**
	 * @param reachedStatusId the reachedStatusId to set
	 */
	public void setReachedStatusId(int reachedStatusId) {
		this.reachedStatusId = reachedStatusId;
	}
}
