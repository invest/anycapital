package capital.any.model.base;

import java.io.Serializable;

public class Role implements Serializable {
	
	private static final long serialVersionUID = -2198708703909926682L;
	private long id;
	private String role;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	@Override
	public String toString() {
		return "Role [id=" + id + ", role=" + role + "]";
	}
}
