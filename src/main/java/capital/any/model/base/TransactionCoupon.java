package capital.any.model.base;

import java.io.Serializable;

/**
 * @author Eyal Goren
 *
 */
public class TransactionCoupon implements Serializable {	
	private static final long serialVersionUID = 6788213015953826469L;
	private long transactionId;
	private long amonut;
	private int productCouponId;
	
	/**
	 * @return the transactionId
	 */
	public long getTransactionId() {
		return transactionId;
	}
	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
	/**
	 * @return the amonut
	 */
	public long getAmonut() {
		return amonut;
	}
	/**
	 * @param amonut the amonut to set
	 */
	public void setAmonut(long amonut) {
		this.amonut = amonut;
	}
	
	/**
	 * @return the productCouponId
	 */
	public int getProductCouponId() {
		return productCouponId;
	}
	/**
	 * @param productCouponId the productCouponId to set
	 */
	public void setProductCouponId(int productCouponId) {
		this.productCouponId = productCouponId;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TransactionCoupon [transactionId=" + transactionId + ", amonut=" + amonut + ", productCouponId="
				+ productCouponId + "]";
	}
}
