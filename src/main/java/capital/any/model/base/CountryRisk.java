package capital.any.model.base;

import java.io.Serializable;

/**
 * 
 * @author eyal.ohana
 *
 */
public class CountryRisk implements Serializable {
	private static final long serialVersionUID = -4525883814123939141L;
	
	private int id;
	private String name;
	private String displayName;
	
	/**
	 * CountryRisk
	 */
	public CountryRisk() {
		
	}
	
	/**
	 * CountryRisk
	 * @param id
	 */
	public CountryRisk(int id) {
		this.id = id;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}
	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CountryRisk [id=" + id + ", name=" + name + ", displayName=" + displayName + "]";
	}
}
