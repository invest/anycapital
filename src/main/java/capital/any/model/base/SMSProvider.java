package capital.any.model.base;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

/**
 * @author eran.levy
 *
 */
@Service
public abstract class SMSProvider {	
		
	protected int id;
	protected String name;
	protected String providerClass;
	protected String url;
	protected String userName;
	protected String password;
	protected String props;
	protected String dlrUrl;
		
	public abstract void init();
	
	public abstract void send(SMS sms) throws Exception;
		
	public enum Provider {			
		/**
		 * 
		 */
		MOBIVATE(1);

		
		private static final Map<Integer, Provider> TOKEN_PROVIDER_MAP = new HashMap<Integer, Provider>(Provider.values().length);
		static {
			for (Provider g : Provider.values()) {
				TOKEN_PROVIDER_MAP.put(g.getToken(), g);
			}
		}
		
		private int token;
		
		/**
		 * @param token
		 * @return
		 */
		public static Provider getByToken(int token) {
			Provider v = TOKEN_PROVIDER_MAP.get(token);
			if (v == null) {
				throw new IllegalArgumentException("No Provider with token: " + token);
			} else {
				return v;
			}
		}
		
		/**
		 * @param token
		 */
		private Provider(int token) {
			this.token = token;
		}
		
		/**
		 * @return
		 */
		public int getToken() {
			return this.token;
		}	
	}
				
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the providerClass
	 */
	public String getProviderClass() {
		return providerClass;
	}
	/**
	 * @param providerClass the providerClass to set
	 */
	public void setProviderClass(String providerClass) {
		this.providerClass = providerClass;
	}
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the props
	 */
	public String getProps() {
		return props;
	}
	/**
	 * @param props the props to set
	 */
	public void setProps(String props) {
		this.props = props;
	}
	/**
	 * @return the dlrUrl
	 */
	public String getDlrUrl() {
		return dlrUrl;
	}
	/**
	 * @param dlrUrl the dlrUrl to set
	 */
	public void setDlrUrl(String dlrUrl) {
		this.dlrUrl = dlrUrl;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SMSProvider [id=" + id + ", name=" + name + ", providerClass=" + providerClass + ", url=" + url +
				", userName=" + userName + ", password=" + password + ", props=" + props + ", dlrUrl=" + dlrUrl + "]";
	}
	

}
