package capital.any.model.base;

import java.util.HashMap;
import java.util.Map;

/**
 * @author LioR SoLoMoN
 *
 */
public class GBGResponse {
	
	private long id;
	private int score;
	private String xmlRequest;
	private String xmlResponse;
	private String errorCode;
	private String errorReason;

	/**
	 * 
	 */
	public GBGResponse() {
		
	}
	
	/**
	 * @param id
	 * @param score
	 * @param xmlRequest
	 * @param xmlResponse
	 * @param errorCode
	 * @param errorReason
	 */
	public GBGResponse(long id, int score, String xmlRequest, String xmlResponse, String errorCode,
			String errorReason) {
		super();
		this.id = id;
		this.score = score;
		this.xmlRequest = xmlRequest;
		this.xmlResponse = xmlResponse;
		this.errorCode = errorCode;
		this.errorReason = errorReason;
	}
	
	@Override
	public String toString() {
		return "GBGResponse [id=" + id + ", score=" + score + ", xmlRequest=" + xmlRequest + ", xmlResponse="
				+ xmlResponse + ", errorCode=" + errorCode + ", errorReason=" + errorReason + "]";
	}
	
	public enum Status {
		/**
		 * 
		 */
		PASS(1), 
		/**
		 * 
		 */
		FAIL(2), 
		/**
		 * 
		 */
		REFER(3); 
		
		private static final Map<Integer, Status> ID_STATUS_MAP = new HashMap<Integer, Status>(Status.values().length);
		static {
			for (Status s : Status.values()) {
				ID_STATUS_MAP.put(s.getId(), s);
			}
		}
		
		private int id;
		
		/**
		 * @param id
		 */
		private Status(int id) {
			this.id = id;
		}
		
		/**
		 * @param id
		 * @return
		 */
		public static Status get(int id) {
			Status s = ID_STATUS_MAP.get(id);
			if (s != null) {
				return s;
			} else {
				throw new IllegalArgumentException("No status with id: " + id);
			}
		}
		
		/**
		 * @return
		 */
		public static Map<Integer, Status> get() {
			return ID_STATUS_MAP;
		}
		
		/**
		 * @return the token
		 */
		public int getId() {
			return this.id;
		}
	}
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the score
	 */
	public int getScore() {
		return score;
	}
	/**
	 * @param score the score to set
	 */
	public void setScore(int score) {
		this.score = score;
	}
	/**
	 * @return the xmlRequest
	 */
	public String getXmlRequest() {
		return xmlRequest;
	}
	/**
	 * @param xmlRequest the xmlRequest to set
	 */
	public void setXmlRequest(String xmlRequest) {
		this.xmlRequest = xmlRequest;
	}
	/**
	 * @return the xmlResponse
	 */
	public String getXmlResponse() {
		return xmlResponse;
	}
	/**
	 * @param xmlResponse the xmlResponse to set
	 */
	public void setXmlResponse(String xmlResponse) {
		this.xmlResponse = xmlResponse;
	}
	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}
	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	/**
	 * @return the errorReason
	 */
	public String getErrorReason() {
		return errorReason;
	}
	/**
	 * @param errorReason the errorReason to set
	 */
	public void setErrorReason(String errorReason) {
		this.errorReason = errorReason;
	}
}
