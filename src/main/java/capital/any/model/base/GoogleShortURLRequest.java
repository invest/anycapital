package capital.any.model.base;

/**
 * @author LioR SoLoMoN
 *
 */
public class GoogleShortURLRequest {
	private String longUrl;
	
	public GoogleShortURLRequest(String longUrl) {
		this.longUrl = longUrl;
	}

	@Override
	public String toString() {
		return "GoogleShortURLRequest [longUrl=" + longUrl + "]";
	}

	/**
	 * @return the longUrl
	 */
	public String getLongUrl() {
		return longUrl;
	}

	/**
	 * @param longUrl the longUrl to set
	 */
	public void setLongUrl(String longUrl) {
		this.longUrl = longUrl;
	}	
}
