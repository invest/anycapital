package capital.any.model.base;

/**
 * @author LioR SoLoMoN
 *
 */
public class GBGCountry {
	private int countryId;
	private String profileId;

	/**
	 * 
	 */
	public GBGCountry() {

	}

	/**
	 * @param countryId
	 * @param profileId
	 */
	public GBGCountry(int countryId, String profileId) {
		this.countryId = countryId;
		this.profileId = profileId;
	}

	/**
	 * @return the countryId
	 */
	public int getCountryId() {
		return countryId;
	}

	/**
	 * @param countryId
	 *            the countryId to set
	 */
	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}

	/**
	 * @return the profileId
	 */
	public String getProfileId() {
		return profileId;
	}

	/**
	 * @param profileId
	 *            the profileId to set
	 */
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}
}
