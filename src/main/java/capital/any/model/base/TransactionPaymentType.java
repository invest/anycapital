package capital.any.model.base;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author LioR SoLoMoN
 *
 */
public class TransactionPaymentType implements Serializable {

	private static final long serialVersionUID = 3331810090201091360L;
	private int id;
	private String name;
	private TransactionClass transactionClass;
	private String displayName;
	private String objectClass;
	private String handlerClass;
	
	/**
	 * 
	 */
	public TransactionPaymentType() {
	
	}
	
	/**
	 * @param id
	 */
	public TransactionPaymentType(int id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return "TransactionPaymentType [id=" + id + ", name=" + name + ", transactionClass=" + transactionClass
				+ ", displayName=" + displayName + ", objectClass=" + objectClass + ", handlerClass=" + handlerClass
				+ "]";
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the transactionClass
	 */
	public TransactionClass getTransactionClass() {
		return transactionClass;
	}
	/**
	 * @param transactionClass the transactionClass to set
	 */
	public void setTransactionClass(TransactionClass transactionClass) {
		this.transactionClass = transactionClass;
	}

	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}
	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	/**
	 * @return the objectClass
	 */
	@JsonIgnore
	public String getObjectClass() {
		return objectClass;
	}
	/**
	 * @param objectClass the objectClass to set
	 */
	public void setObjectClass(String objectClass) {
		this.objectClass = objectClass;
	}
	/**
	 * @return the handlerClass
	 */
	@JsonIgnore
	public String getHandlerClass() {
		return handlerClass;
	}
	/**
	 * @param handlerClass the handlerClass to set
	 */
	public void setHandlerClass(String handlerClass) {
		this.handlerClass = handlerClass;
	}

	public enum TransactionClassEnum {
		/**
		 * 1 
		 */
		REAL(1),
		/**
		 * 2
		 */
		NOT_REAL(2);
		
		private int id;
		
		private static final Map<Integer, TransactionClassEnum> ID_TRANSACTION_CLASS_MAP = new HashMap<Integer, TransactionClassEnum>(TransactionClassEnum.values().length);
		static {
			for (TransactionClassEnum tc : TransactionClassEnum.values()) {
				ID_TRANSACTION_CLASS_MAP.put(tc.getId(), tc);
			}
		}
		
		public static TransactionClassEnum get(Integer id) {
			TransactionClassEnum tc = ID_TRANSACTION_CLASS_MAP.get(id);
			if (tc == null) {
				throw new IllegalArgumentException("No Transaction Class Enum with token: " + id);
			} else {
				return tc;
			}
		}
		
		private TransactionClassEnum(int id) {
			this.id = id;
		}

		/**
		 * @return the id
		 */
		public int getId() {
			return id;
		}
	}
	
	/**
	 * @return true if the type is real, false otherwise.
	 */
	@JsonIgnore
	public boolean isReal() {
		boolean result = false;
		if (transactionClass != null &&
				transactionClass.getId() == TransactionClassEnum.REAL.getId()) {
			result = true;
		}
		return result;
	}
}
