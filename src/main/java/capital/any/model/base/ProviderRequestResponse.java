package capital.any.model.base;

import java.io.Serializable;
import java.util.Date;

/**
 * @author eran.levy
 *
 */
public class ProviderRequestResponse implements Serializable {
	
	private static final long serialVersionUID = -560128572237210107L;
	
	private int tableId;
	private long referenceId;
	private String request;
	private String response;
	private Date timeCreated;
	private Date timeUpdated;
	
	/**
	 * @return the tableId
	 */
	public int getTableId() {
		return tableId;
	}
	/**
	 * @param tableId the tableId to set
	 */
	public void setTableId(int tableId) {
		this.tableId = tableId;
	}
	/**
	 * @return the referenceId
	 */
	public long getReferenceId() {
		return referenceId;
	}
	/**
	 * @param referenceId the referenceId to set
	 */
	public void setReferenceId(long referenceId) {
		this.referenceId = referenceId;
	}
	/**
	 * @return the request
	 */
	public String getRequest() {
		return request;
	}
	/**
	 * @param request the request to set
	 */
	public void setRequest(String request) {
		this.request = request;
	}
	/**
	 * @return the response
	 */
	public String getResponse() {
		return response;
	}
	/**
	 * @param response the response to set
	 */
	public void setResponse(String response) {
		this.response = response;
	}
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	/**
	 * @return the timeUpdated
	 */
	public Date getTimeUpdated() {
		return timeUpdated;
	}
	/**
	 * @param timeUpdated the timeUpdated to set
	 */
	public void setTimeUpdated(Date timeUpdated) {
		this.timeUpdated = timeUpdated;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ProviderRequestResponse [tableId=" + tableId + ", referenceId=" + referenceId + ", request=" + request +
				", response=" + response + ", timeCreated=" + timeCreated + ", timeUpdated=" + timeUpdated + "]";
	}
		
}
