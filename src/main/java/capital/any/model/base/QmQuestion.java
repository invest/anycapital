package capital.any.model.base;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author eyal.ohana
 *
 */
public class QmQuestion implements Serializable {
	private static final long serialVersionUID = 2475637638725624813L;
	
	private int id;
	private String name;
	private String displayName;
	private boolean isActive;
	private int orderId;
	private int questionTypeId;
	private List<QmAnswer> qmAnswerList;
	private QmQuestionGroup qmQuestionGroup;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the isActive
	 */
	public boolean isActive() {
		return isActive;
	}
	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	/**
	 * @return the orderId
	 */
	public int getOrderId() {
		return orderId;
	}
	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	/**
	 * @return the questionTypeId
	 */
	public int getQuestionTypeId() {
		return questionTypeId;
	}
	/**
	 * @param questionTypeId the questionTypeId to set
	 */
	public void setQuestionTypeId(int questionTypeId) {
		this.questionTypeId = questionTypeId;
	}
	/**
	 * @return the qmAnswerList
	 */
	public List<QmAnswer> getQmAnswerList() {
		return qmAnswerList;
	}
	/**
	 * @param qmAnswerList the qmAnswerList to set
	 */
	public void setQmAnswerList(List<QmAnswer> qmAnswerList) {
		this.qmAnswerList = qmAnswerList;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "QmQuestion [id=" + id + ", name=" + name + ", displayName=" + displayName + ", isActive=" + isActive
				+ ", orderId=" + orderId + ", questionTypeId=" + questionTypeId + ", qmAnswerList=" + qmAnswerList
				+ ", qmQuestionGroup=" + qmQuestionGroup + "]";
	}
	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}
	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	/**
	 * @return the qmQuestionGroup
	 */
	public QmQuestionGroup getQmQuestionGroup() {
		return qmQuestionGroup;
	}
	/**
	 * @param qmQuestionGroup the qmQuestionGroup to set
	 */
	public void setQmQuestionGroup(QmQuestionGroup qmQuestionGroup) {
		this.qmQuestionGroup = qmQuestionGroup;
	}
	
}
