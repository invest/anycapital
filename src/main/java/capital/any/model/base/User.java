package capital.any.model.base;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;
import capital.any.base.enums.ActionSource;
import capital.any.service.base.country.CountryBlocked;
import capital.any.service.base.user.EmailExistInDB;

/**
 * @author eranl
 *
 */
@EmailExistInDB.List({
	@EmailExistInDB(first = "email", message = "error-email-exist", groups = User.AddUserValidatorGroup.class),
})

@CountryBlocked.List({
	@CountryBlocked(first = "countryByPrefix", message = "error-country-blocked", groups = User.AddUserValidatorGroup.class),
	@CountryBlocked(first = "countryByUser", message = "error-country-blocked", groups = User.UpdateUserValidatorGroup.class)
})

public class User implements Serializable {
	
	public interface EditUserValidatorGroup{};
	public interface AddUserValidatorGroup{};
	public interface UpdateUserValidatorGroup{};
	public interface SignUpUserValidatorGroup{};
	
	
	private static final long serialVersionUID = -5230064826313895102L;
	@Min(groups = {EditUserValidatorGroup.class},value=1)
	private long id;
	private long balance;
	@Size(groups = {AddUserValidatorGroup.class} , message ="", min = 6 , max=15)
	private String password;
	private int currencyId;
	@Size(groups = {EditUserValidatorGroup.class, AddUserValidatorGroup.class} , message ="", min = 1 , max=20)
	private String firstName;
	@Size(groups = {EditUserValidatorGroup.class, AddUserValidatorGroup.class} , message ="", min = 1 , max=20)
	private String lastName;
	@Size(groups = EditUserValidatorGroup.class , message ="", min = 1 , max=50)
	private String street;
	@Size(groups = EditUserValidatorGroup.class , message ="", min = 1 , max=10)
	private String zipCode;
	private Date timeCreated;
	private Date timeModified;	
	private Date timeLastLogin;
	private int isActive;
	@Email(groups = AddUserValidatorGroup.class)
	private String email;
	@Size(groups = EditUserValidatorGroup.class , message ="", min = 0 , max=100)
	private String comments;
	private String ip;
    @Past(groups = EditUserValidatorGroup.class)
	private Date timeBirthDate;
	private int isContactByEmail;
	private int isContactBySms;
	private int isContactByPhone;
	@Min(groups={SignUpUserValidatorGroup.class},value=1)
	@Max(groups={SignUpUserValidatorGroup.class},value=1)
	private int isAcceptedTerms;
	@Pattern(groups = {EditUserValidatorGroup.class,AddUserValidatorGroup.class} , message ="",regexp="[0-9]{1,20}")  
	private String mobilePhone;
	@Pattern(groups = {EditUserValidatorGroup.class,AddUserValidatorGroup.class} , message ="",regexp="[0-9]{1,20}")
	private String landLinePhone;
	private Gender gender;
	private Clazz clazz;
	@Size(groups = EditUserValidatorGroup.class , message ="", min = 1 , max=6)
	private String streetNo;
	private int languageId;
	private int utcOffset;
	@Size(groups = EditUserValidatorGroup.class , message ="", min = 1 , max=120)
	private String cityName;
	private String userAgent;
	private String httpReferer;
	@NotNull(groups = EditUserValidatorGroup.class)
	private int countryId;
	private int writerId;
	private int aoUserId;
	private ActionSource actionSource;
	private String utcOffsetCreated;
	private boolean isRegulated;
	private MarketingTracking marketingTracking;
	private long ftdId;
	private UserStep step;
	private int countryByUser;
	private int countryByIP;
	private int countryByPrefix;
	private String TIN;
	
	/**
	 * 
	 */
	public User() {}
	
	/**
	 * @param id
	 */
	public User(long id) {
		this.id = id;
	}
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the balance
	 */
	public long getBalance() {
		return balance;
	}
	/**
	 * @param balance the balance to set
	 */
	public void setBalance(long balance) {
		this.balance = balance;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the currencyId
	 */
	public int getCurrencyId() {
		return currencyId;
	}
	/**
	 * @param currencyId the currencyId to set
	 */
	public void setCurrencyId(int currencyId) {
		this.currencyId = currencyId;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}
	/**
	 * @param street the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}
	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}
	/**
	 * @param zipCode the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	/**
	 * @return the timeModified
	 */
	public Date getTimeModified() {
		return timeModified;
	}
	/**
	 * @param timeModified the timeModified to set
	 */
	public void setTimeModified(Date timeModified) {
		this.timeModified = timeModified;
	}
	/**
	 * @return the timeLastLogin
	 */
	public Date getTimeLastLogin() {
		return timeLastLogin;
	}
	/**
	 * @param timeLastLogin the timeLastLogin to set
	 */
	public void setTimeLastLogin(Date timeLastLogin) {
		this.timeLastLogin = timeLastLogin;
	}
	/**
	 * @return the isActive
	 */
	public int getIsActive() {
		return isActive;
	}
	/**
	 * @param isActive the isActive to set
	 */
	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}
	/**
	 * @param ip the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}
	/**
	 * @return the timeBirthDate
	 */
	public Date getTimeBirthDate() {
		return timeBirthDate;
	}
	/**
	 * @param timeBirthDate the timeBirthDate to set
	 */
	public void setTimeBirthDate(Date timeBirthDate) {
		this.timeBirthDate = timeBirthDate;
	}
	/**
	 * @return the isContactBySms
	 */
	public int getIsContactBySms() {
		return isContactBySms;
	}
	/**
	 * @param isContactBySms the isContactBySms to set
	 */
	public void setIsContactBySms(int isContactBySms) {
		this.isContactBySms = isContactBySms;
	}
	/**
	 * @return the isAcceptedTerms
	 */
	public int getIsAcceptedTerms() {
		return isAcceptedTerms;
	}
	/**
	 * @param isAcceptedTerms the isAcceptedTerms to set
	 */
	public void setIsAcceptedTerms(int isAcceptedTerms) {
		this.isAcceptedTerms = isAcceptedTerms;
	}
	/**
	 * @return the mobilePhone
	 */
	public String getMobilePhone() {
		return mobilePhone;
	}
	/**
	 * @param mobilePhone the mobilePhone to set
	 */
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	/**
	 * @return the landLinePhone
	 */
	public String getLandLinePhone() {
		return landLinePhone;
	}
	/**
	 * @param landLinePhone the landLinePhone to set
	 */
	public void setLandLinePhone(String landLinePhone) {
		this.landLinePhone = landLinePhone;
	}
	/**
	 * @return the gender
	 */
	public Gender getGender() {
		if (gender == null) {
			gender = Gender.NOT_KNOWN; 
		}
		return gender;
	}
	/**
	 * @param gender the gender to set
	 */
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	/**
	 * @return the classId
	 */
	public Clazz getClazz() {
		return clazz;
	}
	/**
	 * @param classId the classId to set
	 */
	public void setClazz(Clazz clazz) {
		this.clazz = clazz;
	}
	/**
	 * @return the streetNo
	 */
	public String getStreetNo() {
		return streetNo;
	}
	/**
	 * @param streetNo the streetNo to set
	 */
	public void setStreetNo(String streetNo) {
		this.streetNo = streetNo;
	}
	/**
	 * @return the languageId
	 */
	public int getLanguageId() {
		return languageId;
	}
	/**
	 * @param languageId the languageId to set
	 */
	public void setLanguageId(int languageId) {
		this.languageId = languageId;
	}
	/**
	 * @return the utcOffset
	 */
	public int getUtcOffset() {
		return utcOffset;
	}

	/**
	 * @param utcOffset the utcOffset to set
	 */
	public void setUtcOffset(int utcOffset) {
		this.utcOffset = utcOffset;
	}

	/**
	 * @return the cityName
	 */
	public String getCityName() {
		return cityName;
	}
	/**
	 * @param cityName the cityName to set
	 */
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	/**
	 * @return the userAgent
	 */
	public String getUserAgent() {
		return userAgent;
	}
	/**
	 * @param userAgent the userAgent to set
	 */
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}
	/**
	 * @return the httpReferer
	 */
	public String getHttpReferer() {
		return httpReferer;
	}
	/**
	 * @param httpReferer the httpReferer to set
	 */
	public void setHttpReferer(String httpReferer) {
		this.httpReferer = httpReferer;
	}
	/**
	 * @return the writerId
	 */
	public int getWriterId() {
		return writerId;
	}
	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(int writerId) {
		this.writerId = writerId;
	}
	/**
	 * @return the aoUserId
	 */
	public int getAoUserId() {
		return aoUserId;
	}
	/**
	 * @param aoUserId the aoUserId to set
	 */
	public void setAoUserId(int aoUserId) {
		this.aoUserId = aoUserId;
	}
	/**
	 * @return the utcOffsetCreated
	 */
	public String getUtcOffsetCreated() {
		return utcOffsetCreated;
	}
	/**
	 * @param utcOffsetCreated the utcOffsetCreated to set
	 */
	public void setUtcOffsetCreated(String utcOffsetCreated) {
		this.utcOffsetCreated = utcOffsetCreated;
	}
	/**
	 * @return the isContactByEmail
	 */
	public int getIsContactByEmail() {
		return isContactByEmail;
	}
	/**
	 * @param isContactByEmail the isContactByEmail to set
	 */
	public void setIsContactByEmail(int isContactByEmail) {
		this.isContactByEmail = isContactByEmail;
	}
	/**
	 * @return the isContactByPhone
	 */
	public int getIsContactByPhone() {
		return isContactByPhone;
	}
	/**
	 * @param isContactByPhone the isContactByPhone to set
	 */
	public void setIsContactByPhone(int isContactByPhone) {
		this.isContactByPhone = isContactByPhone;
	}
	
	/**
	 * ISO/IEC 5218
	 *
	 */
	public enum Gender {
		MALE("M", 1), 
		
		FEMALE("F", 2), 
		
		NOT_KNOWN("K", 0), 
		
		//future
		//NOT_APPLICABLE("A", 9)
		
		;
		
		private static final Map<String, Gender> TOKEN_GENDER_MAP = new HashMap<String, Gender>(Gender.values().length);
		static {
			for (Gender g : Gender.values()) {
				TOKEN_GENDER_MAP.put(g.getToken(), g);
			}
		}
		
		private static final Map<Integer, Gender> ID_GENDER_MAP = new HashMap<Integer, Gender>(Gender.values().length);
		static {
			for (Gender g : Gender.values()) {
				ID_GENDER_MAP.put(g.code, g);
			}
		}
		
		private String token;
		private int code;
		
		/**
		 * @param token
		 * @param code
		 */
		private Gender(String token, int code) {
			this.token = token;
			this.code = code;
		}
		
		/**
		 * @param token
		 * @return
		 */
		public static Gender getByToken(String token) {
			Gender g = TOKEN_GENDER_MAP.get(token);
			if (g == null) {
				throw new IllegalArgumentException("No Gender with token: " + token);
			} else {
				return g;
			}
		}
		
		/**
		 * @return
		 */
		public static Map<Integer, Gender> getByCode() {
			return ID_GENDER_MAP;
		}
		
		/**
		 * @return the token
		 */
		public String getToken() {
			return this.token;
		}
		
		/**
		 * @return the code
		 */
		public int getCode() {
			return this.code;
		}
	}
	
	/**
	 * 
	 *
	 */
	public enum Clazz {
		TESTER(1), 
		
		REAL(2),
		
		COMPANY(3);
		
		private static final Map<Integer, Clazz> TOKEN_CLAZZ_MAP = new HashMap<Integer, Clazz>(Clazz.values().length);
		static {
			for (Clazz g : Clazz.values()) {
				TOKEN_CLAZZ_MAP.put(g.getToken(), g);
			}
		}
		
		private int token;
		
		/**
		 * @param token
		 * @return
		 */
		public static Clazz get(int token) {
			Clazz c = TOKEN_CLAZZ_MAP.get(token);
			if (c == null) {
				throw new IllegalArgumentException("No Clazz with token: " + token);
			} else {
				return c;
			}
		}
		
		/**
		 * @return
		 */
		public static Map<Integer, Clazz> get() {
			return TOKEN_CLAZZ_MAP;
		}
		
		/**
		 * @param token
		 */
		private Clazz(int token) {
			this.token = token;
		}
		
		/**
		 * @return
		 */
		public int getToken() {
			return this.token;
		}		
	}
	
	@JsonFormat(shape = JsonFormat.Shape.NUMBER)
	public enum Step {
		NEW(1), 
		
		UPDATE(2), 
		
		MISSING(3),
		
		PENDING(4),
		
		APPROVE(5),
			
		;
		
		private static final Map<Integer, Step> ID_STEP_MAP = new HashMap<Integer, Step>(Step.values().length);
		static {
			for (Step g : Step.values()) {
				ID_STEP_MAP.put(g.id, g);
			}
		}
	
		private int id;
		
		@JsonCreator
		public static Step get(int id) {
			Step step = ID_STEP_MAP.get(id);
			if (step == null) {
				throw new IllegalArgumentException("No step with id: " + id);
			} 
			return step;
		}
		
		private Step(int id) {
			this.id = id;
		}
		
		/**
		 * @return the code
		 */
		@JsonValue
		public int getId() {
			return this.id;
		}
	}
		
	/**
	 * @return the actionSource
	 */
	public ActionSource getActionSource() {
		return this.actionSource;
	}
	/**
	 * @param actionSource the actionSource to set
	 */
	public void setActionSource(ActionSource actionSource) {
		this.actionSource = actionSource;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "User [id=" + id + ", balance=" + balance + ", password=" + password + ", currencyId=" + currencyId
				+ ", firstName=" + firstName + ", lastName=" + lastName + ", street=" + street + ", zipCode=" + zipCode
				+ ", timeCreated=" + timeCreated + ", timeModified=" + timeModified + ", timeLastLogin=" + timeLastLogin
				+ ", isActive=" + isActive + ", email=" + email + ", comments=" + comments + ", ip=" + ip
				+ ", timeBirthDate=" + timeBirthDate + ", isContactByEmail=" + isContactByEmail + ", isContactBySms="
				+ isContactBySms + ", isContactByPhone=" + isContactByPhone + ", isAcceptedTerms=" + isAcceptedTerms
				+ ", mobilePhone=" + mobilePhone + ", landLinePhone=" + landLinePhone + ", gender=" + gender
				+ ", clazz=" + clazz + ", streetNo=" + streetNo + ", languageId=" + languageId + ", utcOffset="
				+ utcOffset + ", cityName=" + cityName + ", userAgent=" + userAgent + ", httpReferer=" + httpReferer
				+ ", countryId=" + countryId + ", writerId=" + writerId + ", aoUserId=" + aoUserId + ", actionSource="
				+ actionSource + ", utcOffsetCreated=" + utcOffsetCreated + ", isRegulated=" + isRegulated
				+ ", marketingTracking=" + marketingTracking + ", ftdId=" + ftdId + ", step=" + step
				+ ", countryByUser=" + countryByUser + ", countryByIP=" + countryByIP + ", countryByPrefix="
				+ countryByPrefix + ", TIN=" + TIN + "]";
	}

	/**
	 * @return the isRegulated
	 */
	public boolean isRegulated() {
		return isRegulated;
	}
	/**
	 * @param isRegulated the isRegulated to set
	 */
	public void setRegulated(boolean isRegulated) {
		this.isRegulated = isRegulated;
	}

	/**
	 * @return the marketingTracking
	 */
	public MarketingTracking getMarketingTracking() {
		return marketingTracking;
	}

	/**
	 * @param marketingTracking the marketingTracking to set
	 */
	public void setMarketingTracking(MarketingTracking marketingTracking) {
		this.marketingTracking = marketingTracking;
	}

	/**
	 * @return the ftdId
	 */
	public long getFtdId() {
		return ftdId;
	}

	/**
	 * @param ftdId the ftdId to set
	 */
	public void setFtdId(long ftdId) {
		this.ftdId = ftdId;
	}

	/**
	 * @return the step
	 */
	public UserStep getUserStep() {
		return step;
	}

	/**
	 * @param step the step to set
	 */
	public void setUserStep(UserStep step) {
		this.step = step;
	}

	/**
	 * @return the countryByUser
	 */
	public int getCountryByUser() {
		return countryByUser;
	}

	/**
	 * @param countryByUser the countryByUser to set
	 */
	public void setCountryByUser(int countryByUser) {
		this.countryByUser = countryByUser;
	}

	/**
	 * @return the countryByIP
	 */
	public int getCountryByIP() {
		return countryByIP;
	}

	/**
	 * @param countryByIP the countryByIP to set
	 */
	public void setCountryByIP(int countryByIP) {
		this.countryByIP = countryByIP;
	}

	/**
	 * @return the countryByPrefix
	 */
	public int getCountryByPrefix() {
		return countryByPrefix;
	}

	/**
	 * @param countryByPrefix the countryByPrefix to set
	 */
	public void setCountryByPrefix(int countryByPrefix) {
		this.countryByPrefix = countryByPrefix;
	}

	/**
	 * @return the tIN
	 */
	public String getTIN() {
		return TIN;
	}

	/**
	 * @param tIN the tIN to set
	 */
	public void setTIN(String tIN) {
		TIN = tIN;
	}
	
}
