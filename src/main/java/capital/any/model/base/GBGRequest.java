package capital.any.model.base;

import scala.collection.mutable.StringBuilder;

/**
 * @author LioR SoLoMoN
 *
 */
public class GBGRequest {
	
	private long id;
	private long userId;
	private String profileId;
	private long version;
	private String firstName;
	private String lastName;
	private String gender;
	private String DOBDay;
	private String DOBMonth;
	private String DOBYear;
	private String countryName;
	private String street;
	private String city;
	private String zipCode;
	private String streetNo;
	
	/**
	 * 
	 */
	public GBGRequest() {
		
	}

	/**
	 * @param userId
	 * @param profileId
	 * @param version
	 * @param firstName
	 * @param lastName
	 * @param gender
	 * @param dOBDay
	 * @param dOBMonth
	 * @param dOBYear
	 * @param countryName
	 * @param street
	 * @param city
	 * @param zipCode
	 * @param streetNo
	 */
	public GBGRequest(long userId, String profileId, String firstName, String lastName,
			String gender, String dOBDay, String dOBMonth, String dOBYear, String countryName, String street,
			String city, String zipCode, String streetNo) {
		super();
		this.userId = userId;
		this.profileId = profileId;
		this.version = 0;
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.DOBDay = dOBDay;
		this.DOBMonth = dOBMonth;
		this.DOBYear = dOBYear;
		this.countryName = countryName;
		this.street = street;
		this.city = city;
		this.zipCode = zipCode;
		this.streetNo = streetNo;
	}
	
	@Override
	public String toString() {
		return "GBGRequest [id=" + id + ", userId=" + userId + ", profileId=" + profileId + ", version=" + version
				+ ", firstName=" + firstName + ", lastName=" + lastName + ", gender=" + gender + ", DOBDay=" + DOBDay
				+ ", DOBMonth=" + DOBMonth + ", DOBYear=" + DOBYear + ", countryName=" + countryName + ", street="
				+ street + ", city=" + city + ", zipCode=" + zipCode + ", streetNo=" + streetNo + "]";
	}
	
	public String getAddressLine1() {
		return (new StringBuilder(street).append(" ").append(streetNo)).toString();
	}
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the profileId
	 */
	public String getProfileId() {
		return profileId;
	}
	/**
	 * @param profileId the profileId to set
	 */
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}
	/**
	 * @return the version
	 */
	public long getVersion() {
		return version;
	}
	/**
	 * @param version the version to set
	 */
	public void setVersion(long version) {
		this.version = version;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}
	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}
	/**
	 * @return the dOBDay
	 */
	public String getDOBDay() {
		return DOBDay;
	}
	/**
	 * @param dOBDay the dOBDay to set
	 */
	public void setDOBDay(String dOBDay) {
		DOBDay = dOBDay;
	}
	/**
	 * @return the dOBMonth
	 */
	public String getDOBMonth() {
		return DOBMonth;
	}
	/**
	 * @param dOBMonth the dOBMonth to set
	 */
	public void setDOBMonth(String dOBMonth) {
		DOBMonth = dOBMonth;
	}
	/**
	 * @return the dOBYear
	 */
	public String getDOBYear() {
		return DOBYear;
	}
	/**
	 * @param dOBYear the dOBYear to set
	 */
	public void setDOBYear(String dOBYear) {
		DOBYear = dOBYear;
	}
	/**
	 * @return the countryName
	 */
	public String getCountryName() {
		return countryName;
	}
	/**
	 * @param countryName the countryName to set
	 */
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}
	/**
	 * @param street the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}
	/**
	 * @param zipCode the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	/**
	 * @return the streetNo
	 */
	public String getStreetNo() {
		return streetNo;
	}
	/**
	 * @param streetNo the streetNo to set
	 */
	public void setStreetNo(String streetNo) {
		this.streetNo = streetNo;
	}
}
