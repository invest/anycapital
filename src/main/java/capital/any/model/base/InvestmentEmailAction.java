package capital.any.model.base;

import java.io.Serializable;

/**
 * 
 * @author eyal.ohana
 *
 */
public class InvestmentEmailAction implements Serializable {
	private static final long serialVersionUID = 1210296340558878734L;
	
	private Investment investment;
	private Product product;
	
	/**
	 * InvestmentEmailAction
	 */
	public InvestmentEmailAction() {
		
	}
	
	/**
	 * InvestmentEmailAction
	 */
	public InvestmentEmailAction(Investment investment, Product product) {
		super();
		this.investment = investment;
		this.product = product;
	}
	
	/**
	 * @return the investment
	 */
	public Investment getInvestment() {
		return investment;
	}
	/**
	 * @param investment the investment to set
	 */
	public void setInvestment(Investment investment) {
		this.investment = investment;
	}
	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}
	/**
	 * @param product the product to set
	 */
	public void setProduct(Product product) {
		this.product = product;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "InvestmentEmailAction [investment=" + investment + ", product=" + product + "]";
	}
	
}
