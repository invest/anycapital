package capital.any.model.base;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author eranl
 *
 */
public class Market implements Serializable {
	
	private static final long serialVersionUID = -2201432952914991699L;
	private int id;
	private String name;
	private String displayName;
	private int marketGroupId;
	private String ticker;
	private String currency;
	private String feedName;
	private int decimalPoint;
	private int marketIdAo;
	private MarketPrice lastPrice;
	private List<MarketPrice> historyMarketPrice;
	private String marketPlace;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	@JsonIgnore
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}
	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	/**
	 * @return the marketGroupId
	 */
	public int getMarketGroupId() {
		return marketGroupId;
	}
	/**
	 * @param marketGroupId the marketGroupId to set
	 */
	public void setMarketGroupId(int marketGroupId) {
		this.marketGroupId = marketGroupId;
	}
	/**
	 * @return the ticker
	 */
	public String getTicker() {
		return ticker;
	}
	/**
	 * @param ticker the ticker to set
	 */
	public void setTicker(String ticker) {
		this.ticker = ticker;
	}
	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}
	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	/**
	 * @return the feedName
	 */
	public String getFeedName() {
		return feedName;
	}
	/**
	 * @param feedName the feedName to set
	 */
	public void setFeedName(String feedName) {
		this.feedName = feedName;
	}	
	/**
	 * @return the historyMarketPrice
	 */
	public List<MarketPrice> getHistoryMarketPrice() {
		return historyMarketPrice;
	}
	/**
	 * @param historyMarketPrice the historyMarketPrice to set
	 */
	public void setHistoryMarketPrice(List<MarketPrice> historyMarketPrice) {
		this.historyMarketPrice = historyMarketPrice;
	}
	/**
	 * @return the lastPrice
	 */
	public MarketPrice getLastPrice() {
		return lastPrice;
	}
	/**
	 * @param lastPrice the lastPrice to set
	 */
	public void setLastPrice(MarketPrice lastPrice) {
		this.lastPrice = lastPrice;
	}
	
	
	/**
	 * @return the decimalPoint
	 */
	public int getDecimalPoint() {
		return decimalPoint;
	}
	/**
	 * @param decimalPoint the decimalPoint to set
	 */
	public void setDecimalPoint(int decimalPoint) {
		this.decimalPoint = decimalPoint;
	}
	
	/**
	 * @return the marketIdAo
	 */
	public int getMarketIdAo() {
		return marketIdAo;
	}
	/**
	 * @param marketIdAo the marketIdAo to set
	 */
	public void setMarketIdAo(int marketIdAo) {
		this.marketIdAo = marketIdAo;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Market [id=" + id + ", name=" + name + ", displayName=" + displayName + ", marketGroupId="
				+ marketGroupId + ", ticker=" + ticker + ", currency=" + currency + ", feedName=" + feedName
				+ ", decimalPoint=" + decimalPoint + ", marketIdAo=" + marketIdAo + ", lastPrice=" + lastPrice
				+ ", historyMarketPrice=" + historyMarketPrice + ", marketPlace=" + marketPlace + "]";
	}
	/**
	 * @return the marketPlace
	 */
	public String getMarketPlace() {
		return marketPlace;
	}
	/**
	 * @param marketPlace the marketPlace to set
	 */
	public void setMarketPlace(String marketPlace) {
		this.marketPlace = marketPlace;
	}
	
		
}