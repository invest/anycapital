package capital.any.model.base;

import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * @author LioR SoLoMoN
 *
 */
@JsonDeserialize(as=Writer.class)
public abstract class AbstractWriter implements Serializable {
	private static final long serialVersionUID = 6988530898475713192L;
	
	protected Integer id;
	protected Date timeCreated;
	protected String userName;
	protected String password;
	protected String firstName;
	protected String lastName;
	protected String email;
	protected String comments;
	protected boolean isActive;
	protected int utcOffset;
	protected int groupId;
	
	/**
	 * @param str
	 */
	public abstract void setNull(String str);
	
	/**
	 * @param str
	 */
	public abstract void setActive(String str);
	
	/**
	 * @return false if it's a real writer, true otherwise
	 */
	public abstract boolean isNull();
	
	/**
	 * @return writer id
	 */
	public abstract Integer getId();	

	/**
	 * @param id the id to set
	 */
	public abstract void setId(Integer id);
	
	/**
	 * @return the timeCreated
	 */
	public abstract Date getTimeCreated();
	
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public abstract void setTimeCreated(Date timeCreated);
	
	/**
	 * @return the userName
	 */
	public abstract String getUserName();
	
	/**
	 * @param userName the userName to set
	 */
	public abstract void setUserName(String userName);
	
	/**
	 * @return the password
	 */
	public abstract String getPassword();
	
	/**
	 * @param password the password to set
	 */
	public abstract void setPassword(String password);
	
	/**
	 * @return the firstName
	 */
	public abstract String getFirstName();
	/**
	 * @param firstName the firstName to set
	 */
	public abstract void setFirstName(String firstName);
	
	/**
	 * @return the lastName
	 */
	public abstract String getLastName();
	
	/**
	 * @param lastName the lastName to set
	 */
	public abstract void setLastName(String lastName);
	
	/**
	 * @return the email
	 */
	public abstract String getEmail();
	
	/**
	 * @param email the email to set
	 */
	public abstract void setEmail(String email);
	
	/**
	 * @return the comments
	 */
	public abstract String getComments();
	
	/**
	 * @param comments the comments to set
	 */
	public abstract void setComments(String comments);
	
	/**
	 * @return the isActive
	 */
	public abstract boolean isActive();
	
	/**
	 * @param isActive the isActive to set
	 */
	public abstract void setIsActive(boolean isActive);
		
	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the utcOffset
	 */
	public abstract int getUtcOffset();

	/**
	 * @param utcOffset the utcOffset to set
	 */
	public abstract void setUtcOffset(int utcOffset);

	/**
	 * @return the groupId
	 */
	public int getGroupId() {
		return groupId;
	}

	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AbstractWriter [id=" + id + ", timeCreated=" + timeCreated + ", userName=" + userName + ", password="
				+ password + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + ", comments="
				+ comments + ", isActive=" + isActive + ", utcOffset=" + utcOffset + ", groupId=" + groupId + "]";
	}
	
}
