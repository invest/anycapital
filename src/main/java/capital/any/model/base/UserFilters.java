package capital.any.model.base;

import java.time.LocalDateTime;

/**
 * 
 * @author eyal.ohana
 *
 */
public class UserFilters {
	private LocalDateTime fromDate;
	private LocalDateTime toDate;
	
	/**
	 * @return the fromDate
	 */
	public LocalDateTime getFromDate() {
		return fromDate;
	}
	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(LocalDateTime fromDate) {
		this.fromDate = fromDate;
	}
	/**
	 * @return the toDate
	 */
	public LocalDateTime getToDate() {
		return toDate;
	}
	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(LocalDateTime toDate) {
		this.toDate = toDate;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UserFilters [fromDate=" + fromDate + ", toDate=" + toDate + "]";
	}
}
