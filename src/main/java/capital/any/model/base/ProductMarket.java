package capital.any.model.base;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author eranl
 *
 */
public class ProductMarket implements Serializable {

	private static final long serialVersionUID = 421870753189756693L;
	
	private int id;
	private long productId;
	private Market market;
	private double startTradeLevel;
	private Double endTradeLevel;
	
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the productId
	 */
	public long getProductId() {
		return productId;
	}
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(long productId) {
		this.productId = productId;
	}
	
	
	public Market getMarket() {
		return market;
	}
	public void setMarket(Market market) {
		this.market = market;
	}
	/**
	 * @return the startTradeLevel
	 */
	public double getStartTradeLevel() {
		return startTradeLevel;
	}
	/**
	 * @param startTradeLevel the startTradeLevel to set
	 */
	public void setStartTradeLevel(double startTradeLevel) {
		this.startTradeLevel = startTradeLevel;
	}
	
	/**
	 * @return the endTradeLevel
	 */
	public Double getEndTradeLevel() {
		return endTradeLevel;
	}
	/**
	 * @param endTradeLevel the endTradeLevel to set
	 */
	public void setEndTradeLevel(Double endTradeLevel) {
		this.endTradeLevel = endTradeLevel;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ProductMarkets [productId= " + productId + ", market= " + market + ", startTradeLevel="
				+ startTradeLevel + ", endTradeLevel=" + endTradeLevel + "]";
	}
	
	/**
	 * DONT USE IT! only in use for insert to db
	 * @return market id
	 */
	@JsonIgnore
	public int getMarketId() {
		return market.getId();
	}
}
