package capital.any.model.base;

import java.io.Serializable;
import java.util.Date;
import capital.any.base.enums.ActionSource;

/**
 * @author Eyal G
 *
 */
public class ProductHistory implements Serializable {	

	private static final long serialVersionUID = 3912283101656738155L;

	private long productId;
	private ProductHistoryAction productHistoryAction;
	private String actionParamters;
	private Date timeCreated;
	private ActionSource actionSource;
	private int writerId;
	private String serverName;
	
	public ProductHistory(ActionSource actionSource) {
		super();
		this.actionSource = actionSource;
	}
	
	public ProductHistory(ActionSource actionSource, int writerId) {
		this(actionSource);
		this.writerId = writerId;
	}
	
	public ProductHistory(long productId, ProductHistoryAction productHistoryAction, String actionParamters,
			ActionSource actionSource, int writerId, String serverName) {
		this(actionSource, writerId);
		this.productId = productId;
		this.productHistoryAction = productHistoryAction;
		this.actionParamters = actionParamters;
		this.serverName = serverName;
	}
	
	/**
	 * set parameters
	 * @param productId
	 * @param productHistoryAction
	 * @param actionParamters
	 * @param serverName
	 */
	public void setDetails(long productId, ProductHistoryAction productHistoryAction, String actionParamters,
			String serverName) {
		this.productId = productId;
		this.productHistoryAction = productHistoryAction;
		this.actionParamters = actionParamters;
		this.serverName = serverName;
	}

	/**
	 * @return the productId
	 */
	public long getProductId() {
		return productId;
	}
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(long productId) {
		this.productId = productId;
	}
	/**
	 * @return the productHistoryAction
	 */
	public ProductHistoryAction getProductHistoryAction() {
		return productHistoryAction;
	}
	/**
	 * @param productHistoryAction the productHistoryAction to set
	 */
	public void setProductHistoryAction(ProductHistoryAction productHistoryAction) {
		this.productHistoryAction = productHistoryAction;
	}
	/**
	 * @return the actionParamters
	 */
	public String getActionParamters() {
		return actionParamters;
	}
	/**
	 * @param actionParamters the actionParamters to set
	 */
	public void setActionParamters(String actionParamters) {
		this.actionParamters = actionParamters;
	}
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	
	/**
	 * @return the actionSource
	 */
	public ActionSource getActionSource() {
		return actionSource;
	}

	/**
	 * @param actionSource the actionSource to set
	 */
	public void setActionSource(ActionSource actionSource) {
		this.actionSource = actionSource;
	}

	/**
	 * @return the writerId
	 */
	public int getWriterId() {
		return writerId;
	}
	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(int writerId) {
		this.writerId = writerId;
	}
	/**
	 * @return the serverName
	 */
	public String getServerName() {
		return serverName;
	}
	/**
	 * @param serverName the serverName to set
	 */
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ProductHistory [productId=" + productId + ", productHistoryAction=" + productHistoryAction
				+ ", actionParamters=" + actionParamters + ", timeCreated=" + timeCreated + ", actionSource="
				+ actionSource + ", writerId=" + writerId + ", serverName=" + serverName + "]";
	}	
}
