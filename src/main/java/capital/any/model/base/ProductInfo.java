//package capital.any.model.base;
//
//import java.io.Serializable;
//
///**
// * @author eranl
// *
// */
//public class ProductInfo implements Serializable {
//	
//	private static final long serialVersionUID = 3713852681901131633L;
//	
//	private long productId;
//	private int languageId;
//	private String title;
//	private String description;
//	
//	/**
//	 * @return the productId
//	 */
//	public long getProductId() {
//		return productId;
//	}
//	/**
//	 * @param productId the productId to set
//	 */
//	public void setProductId(long productId) {
//		this.productId = productId;
//	}
//	/**
//	 * @return the languageId
//	 */
//	public int getLanguageId() {
//		return languageId;
//	}
//	/**
//	 * @param languageId the languageId to set
//	 */
//	public void setLanguageId(int languageId) {
//		this.languageId = languageId;
//	}
//	/**
//	 * @return the title
//	 */
//	public String getTitle() {
//		return title;
//	}
//	/**
//	 * @param title the title to set
//	 */
//	public void setTitle(String title) {
//		this.title = title;
//	}
//	/**
//	 * @return the description
//	 */
//	public String getDescription() {
//		return description;
//	}
//	/**
//	 * @param description the description to set
//	 */
//	public void setDescription(String description) {
//		this.description = description;
//	}
//	/* (non-Javadoc)
//	 * @see java.lang.Object#toString()
//	 */
//	@Override
//	public String toString() {
//		return "ProductInfo [productId=" + productId + ", languageId=" + languageId + ", title=" + title
//				+ ", description=" + description + "]";
//	}
//
//}
