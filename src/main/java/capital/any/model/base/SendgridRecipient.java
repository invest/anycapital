package capital.any.model.base;

/**
 * 
 * @author eyal.ohana
 *
 */
public class SendgridRecipient {
	private String email;
	private long acUserId;
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the acUserId
	 */
	public long getAcUserId() {
		return acUserId;
	}
	/**
	 * @param acUserId the acUserId to set
	 */
	public void setAcUserId(long acUserId) {
		this.acUserId = acUserId;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SendgridRecipient [email=" + email + ", acUserId=" + acUserId + "]";
	}
}
