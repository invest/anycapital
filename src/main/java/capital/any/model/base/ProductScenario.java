package capital.any.model.base;

import java.io.Serializable;

/**
 * @author eranl
 *
 */
public class ProductScenario implements Serializable {
	
	private static final long serialVersionUID = -7197514200230502645L;
	
	private int position;
	private double finalFixingLevel;
	private double finalFixingLevelForTxt;
	private double formulaResult;
	/**
	 * @return the position
	 */
	public int getPosition() {
		return position;
	}
	/**
	 * @param position the position to set
	 */
	public void setPosition(int position) {
		this.position = position;
	}
	/**
	 * @return the finalFixingLevel
	 */
	public double getFinalFixingLevel() {
		return finalFixingLevel;
	}
	/**
	 * @param finalFixingLevel the finalFixingLevel to set
	 */
	public void setFinalFixingLevel(double finalFixingLevel) {
		this.finalFixingLevel = finalFixingLevel;
	}
	/**
	 * @return the formulaResult
	 */
	public double getFormulaResult() {
		return formulaResult;
	}
	/**
	 * @param formulaResult the formulaResult to set
	 */
	public void setFormulaResult(double formulaResult) {
		this.formulaResult = formulaResult;
	}
	
	/**
	 * @return the finalFixingLevelForTxt
	 */
	public double getFinalFixingLevelForTxt() {
		return finalFixingLevelForTxt;
	}
	/**
	 * @param finalFixingLevelForTxt the finalFixingLevelForTxt to set
	 */
	public void setFinalFixingLevelForTxt(double finalFixingLevelForTxt) {
		this.finalFixingLevelForTxt = finalFixingLevelForTxt;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ProductScenario [position=" + position + ", finalFixingLevel=" + finalFixingLevel
				+ ", finalFixingLevelForTxt=" + finalFixingLevelForTxt + ", formulaResult=" + formulaResult + "]";
	}
}
