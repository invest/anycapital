package capital.any.model.base;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author LioR SoLoMoN
 *
 */
public class BalanceHistory implements Serializable {	
	private static final long serialVersionUID = -1569473627748613276L;
	private long userId;
	private long referenceId;
	private long balance;
	private BalanceHistoryCommand balanceHistoryCommand;
	private Date timeCreated;
	
	/**
	 *  empty const'
	 */
	public BalanceHistory() {
		
	}
	
	/**
	 * @param userId
	 * @param referenceId
	 * @param balance
	 * @param balanceHistoryCommandId
	 */
	public BalanceHistory(long userId, long referenceId, long balance,
			BalanceHistoryCommand balanceHistoryCommand) {
		this.userId = userId;
		this.referenceId = referenceId;
		this.balance = balance;
		this.balanceHistoryCommand = balanceHistoryCommand;
	}

	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	/**
	 * @return the referenceId
	 */
	public long getReferenceId() {
		return referenceId;
	}
	
	/**
	 * @param referenceId the referenceId to set
	 */
	public void setReferenceId(long referenceId) {
		this.referenceId = referenceId;
	}
	
	/**
	 * @return the balance
	 */
	public long getBalance() {
		return balance;
	}
	
	/**
	 * @param balance the balance to set
	 */
	public void setBalance(long balance) {
		this.balance = balance;
	}
	
	
	/**
	 * @return the balanceHistoryCommands
	 */
	public BalanceHistoryCommand getBalanceHistoryCommand() {
		return balanceHistoryCommand;
	}
	
	/**
	 * @param balanceHistoryCommands the balanceHistoryCommands to set
	 */
	public void setBalanceHistoryCommand(BalanceHistoryCommand balanceHistoryCommands) {
		this.balanceHistoryCommand = balanceHistoryCommands;
	}
	
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	@Override
	public String toString() {
		return "BalanceHistory [userId=" + userId + ", referenceId=" + referenceId + ", balance=" + balance
				+ ", balanceHistoryCommand=" + balanceHistoryCommand + ", timeCreated=" + timeCreated + "]";
	}
	
	/**
	 * @author LioR SoLoMoN
	 *
	 */
	public enum BalanceHistoryCommand {
		/**
		 * 1
		 */
		INSERT_INVESTMENTS(1),
		
		/**
		 * 2
		 */
		SETTLE_INVESTMENT(2),
		
		/**
		 * 3
		 */
		CANCEL_INVESTMENT(3),
		
		/**
		 * 4
		 */
		RESETTLE_INVESTMENT(4),
		
		/**
		 * 5
		 */
		SUCCESS_TRANSACTION(5),
		
		/**
		 * 6
		 */
		CANCEL_TRANSACTION(6)
			
		;
		
		private static final Map<Integer, BalanceHistoryCommand> ID_BALANCE_HISTORY_COMMAND_MAP = new HashMap<Integer, BalanceHistoryCommand>(BalanceHistoryCommand.values().length);
		static {
			for (BalanceHistoryCommand b : BalanceHistoryCommand.values()) {
				ID_BALANCE_HISTORY_COMMAND_MAP.put(b.getId(), b);
			}
		}
		
		private int id;
		
		public static BalanceHistoryCommand getById(int id) {
			BalanceHistoryCommand b = ID_BALANCE_HISTORY_COMMAND_MAP.get(id);
			if (b == null) {
				throw new IllegalArgumentException("No balance history command with id: " + id);
			} else {
				return b;
			}
		}

		private BalanceHistoryCommand(int id) {
			this.id = id;
		}

		/**
		 * @return the id
		 */
		public int getId() {
			return id;
		}
		/**
		 * @param id the id to set
		 */
		public void setId(int id) {
			this.id = id;
		}
	}
}
