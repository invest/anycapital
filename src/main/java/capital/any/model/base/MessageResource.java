package capital.any.model.base;

import java.io.Serializable;

/**
 * @author eranl
 *
 */
public class MessageResource implements Serializable {
	
	private static final long serialVersionUID = 3551982776981776529L;

	public static final int MSG_RES_EN = 2;
			
	private String key;
	private String value;
	private int languageId;
	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}
	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}
	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	/**
	 * @return the languageId
	 */
	public int getLanguageId() {
		return languageId;
	}
	/**
	 * @param languageId the languageId to set
	 */
	public void setLanguageId(int languageId) {
		this.languageId = languageId;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MessageResource [key=" + key + ", value=" + value + ", languageId=" + languageId + "]";
	}
		
}
