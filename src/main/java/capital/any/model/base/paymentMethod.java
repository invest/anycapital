package capital.any.model.base;

/**
 * @author LioR SoLoMoN
 *
 */
public abstract class paymentMethod {
	protected long fee;

	/**
	 * @return the fee
	 */
	public long getFee() {
		return fee;
	}

	/**
	 * @param fee the fee to set
	 */
	public void setFee(long fee) {
		this.fee = fee;
	}
}
