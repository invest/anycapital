package capital.any.model.base;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author eranl
 *
 */
public class Writer extends AbstractWriter {
	private static final long serialVersionUID = -4349095679189942775L;
	
	/**
	 * 
	 */
	public Writer() {
		
	}
		
	/**
	 * @param id
	 */
	public Writer(Integer id) {
		this.id = id;
	}

	@Override
	public boolean isNull() {
		return false;
	}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public Date getTimeCreated() {
		return timeCreated;
	}

	@Override
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;		
	}

	@Override
	public String getUserName() {
		return userName;
	}

	@Override
	public void setUserName(String userName) {
		this.userName = userName;		
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String getFirstName() {
		return firstName;
	}

	@Override
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Override
	public String getLastName() {
		return lastName;
	}

	@Override
	public void setLastName(String lastName) {
		this.lastName = lastName;		
	}

	@Override
	public String getEmail() {
		return email;
	}

	@Override
	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String getComments() {
		return comments;
	}

	@Override
	public void setComments(String comments) {
		this.comments = comments;		
	}

	@Override
	public boolean isActive() {
		return isActive;
	}

	@Override
	public void setIsActive(boolean isActive) {
		this.isActive = isActive;		
	}
	
	@Override
	public int getUtcOffset() {
		return utcOffset;
	}

	@Override
	public void setUtcOffset(int utcOffset) {
		this.utcOffset = utcOffset;
	}

	public void setNull(String str) {
		// Fake for deserialize json
	}
	
	@Override
	public void setActive(String str) {
		// Fake for deserialize json
	}
	
}
