package capital.any.model.base;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author LioR SoLoMoN
 *
 */
public class UserStep implements Serializable {

	private static final long serialVersionUID = 8517845625643311668L;
	private int id;
	private String name;
	private String displayName;
	
	/**
	 * 
	 */
	public UserStep() {
		
	}
	
	/**
	 * @param id
	 */
	public UserStep(int id) {
		this.id = id;
	}
	
	public UserStep(int id, String displayName) {
		this.id = id;
		this.displayName = displayName;
	}
	
	@Override
	public String toString() {
		return "TransactionClassType [id=" + id + ", displayName=" + displayName + ", name=" + name + "]";
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	@JsonIgnore
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}
	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}	
}
