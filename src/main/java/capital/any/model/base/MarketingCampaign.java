package capital.any.model.base;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author eyal.ohana
 *
 */
public class MarketingCampaign implements Serializable {
	private static final long serialVersionUID = 4968282405082002104L;
	
	private long id;
	private String name;
	private int sourceId;
	private int domainId;
	private int landingPageId;
	private int contentId;
	private int writerId;
	private Date timeCreated;
	private Date timeModified;
	private MarketingDomain marketingDomain;
	private MarketingLandingPage marketingLandingPage;
	private String url;
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the sourceId
	 */
	public int getSourceId() {
		return sourceId;
	}
	/**
	 * @param sourceId the sourceId to set
	 */
	public void setSourceId(int sourceId) {
		this.sourceId = sourceId;
	}
	/**
	 * @return the writerId
	 */
	public int getWriterId() {
		return writerId;
	}
	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(int writerId) {
		this.writerId = writerId;
	}
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	/**
	 * @return the domainId
	 */
	public int getDomainId() {
		return domainId;
	}
	/**
	 * @param domainId the domainId to set
	 */
	public void setDomainId(int domainId) {
		this.domainId = domainId;
	}
	/**
	 * @return the landingPageId
	 */
	public int getLandingPageId() {
		return landingPageId;
	}
	/**
	 * @param landingPageId the landingPageId to set
	 */
	public void setLandingPageId(int landingPageId) {
		this.landingPageId = landingPageId;
	}
	/**
	 * @return the contentId
	 */
	public int getContentId() {
		return contentId;
	}
	/**
	 * @param contentId the contentId to set
	 */
	public void setContentId(int contentId) {
		this.contentId = contentId;
	}
	/**
	 * @return the timeModified
	 */
	public Date getTimeModified() {
		return timeModified;
	}
	/**
	 * @param timeModified the timeModified to set
	 */
	public void setTimeModified(Date timeModified) {
		this.timeModified = timeModified;
	}
	/**
	 * @return the marketingDomain
	 */
	public MarketingDomain getMarketingDomain() {
		return marketingDomain;
	}
	/**
	 * @param marketingDomain the marketingDomain to set
	 */
	public void setMarketingDomain(MarketingDomain marketingDomain) {
		this.marketingDomain = marketingDomain;
	}
	/**
	 * @return the marketingLandingPage
	 */
	public MarketingLandingPage getMarketingLandingPage() {
		return marketingLandingPage;
	}
	/**
	 * @param marketingLandingPage the marketingLandingPage to set
	 */
	public void setMarketingLandingPage(MarketingLandingPage marketingLandingPage) {
		this.marketingLandingPage = marketingLandingPage;
	}
	
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MarketingCampaign [id=" + id + ", name=" + name + ", sourceId=" + sourceId + ", domainId=" + domainId
				+ ", landingPageId=" + landingPageId + ", contentId=" + contentId + ", writerId=" + writerId
				+ ", timeCreated=" + timeCreated + ", timeModified=" + timeModified + ", marketingDomain="
				+ marketingDomain + ", marketingLandingPage=" + marketingLandingPage + ", url=" + url + "]";
	}
	
	
}
