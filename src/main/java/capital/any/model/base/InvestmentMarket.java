package capital.any.model.base;

import java.io.Serializable;

/**
 * @author Eyal G
 *
 */
public class InvestmentMarket implements Serializable {

	private static final long serialVersionUID = -3582095781524655133L;
	
	private int marketId;
	private long investmentId;
	private double price;
	
	/**
	 * @return the marketId
	 */
	public int getMarketId() {
		return marketId;
	}
	/**
	 * @param marketId the marketId to set
	 */
	public void setMarketId(int marketId) {
		this.marketId = marketId;
	}
	/**
	 * @return the investmentId
	 */
	public long getInvestmentId() {
		return investmentId;
	}
	/**
	 * @param investmentId the investmentId to set
	 */
	public void setInvestmentId(long investmentId) {
		this.investmentId = investmentId;
	}
	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "InvestmentMarket [marketId=" + marketId + ", investmentId=" + investmentId + ", price=" + price + "]";
	}
}