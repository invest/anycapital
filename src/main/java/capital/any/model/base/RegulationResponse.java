package capital.any.model.base;

import java.io.Serializable;
import java.util.Map;

/**
 * 
 * @author eyal.ohana
 *
 */
public class RegulationResponse implements Serializable {

	private static final long serialVersionUID = 1850447392605045503L;
	
	private User user;
	private Map<Integer, RegulationFileRule> regulationFileRuleMap;
	
	/**
	 * RegulationResponse
	 */
	public RegulationResponse() {
		
	}
	
	/**
	 * RegulationResponse
	 * @param user
	 * @param RegulationFileRuleMap
	 */
	public RegulationResponse(User user, Map<Integer, RegulationFileRule> regulationFileRuleMap) {
		this.user = user;
		this.regulationFileRuleMap = regulationFileRuleMap;
	}
	
	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the regulationFileRuleMap
	 */
	public Map<Integer, RegulationFileRule> getRegulationFileRuleMap() {
		return regulationFileRuleMap;
	}

	/**
	 * @param regulationFileRuleMap the regulationFileRuleMap to set
	 */
	public void setRegulationFileRuleMap(Map<Integer, RegulationFileRule> regulationFileRuleMap) {
		this.regulationFileRuleMap = regulationFileRuleMap;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RegulationResponse [user=" + user + ", regulationFileRuleMap=" + regulationFileRuleMap + "]";
	}

	
}
