package capital.any.model.base;

import java.io.Serializable;
import java.util.Date;

/**
 * @author LioR SoLoMoN
 *
 */
public class TransactionFee implements Serializable {

	private static final long serialVersionUID = -444390250727392534L;
	private long transactionId;
	private long amount;
	private Date timeCreated;
	
	/**
	 * 
	 */
	public TransactionFee() {
		
	}

	/**
	 * @param transactionId
	 * @param amount
	 * @param timeCreated
	 */
	public TransactionFee(long transactionId, long amount, Date timeCreated) {
		this.transactionId = transactionId;
		this.amount = amount;
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the transactionId
	 */
	public long getTransactionId() {
		return transactionId;
	}
	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
	/**
	 * @return the amount
	 */
	public long getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(long amount) {
		this.amount = amount;
	}
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
}
