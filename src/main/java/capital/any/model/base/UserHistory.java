package capital.any.model.base;

import java.io.Serializable;
import java.util.Date;

import capital.any.base.enums.ActionSource;

/**
 * 
 * @author eyal.ohana
 *
 */
public class UserHistory implements Serializable {
	private static final long serialVersionUID = -5308854268314704433L;
	
	private long id;
	private long userId;
	private UserHistoryAction userHistoryAction;
	private String actionParamters;
	private ActionSource actionSource;
	private AbstractWriter writer;
	private String serverName;
	private Date timeCreated;
	
	/**
	 * UserHistory
	 */
	public UserHistory() {
		super();
	}
	
	/**
	 * UserHistory
	 * @param actionSource
	 * @param writerId
	 */
	public UserHistory(ActionSource actionSource, AbstractWriter writer) {
		super();
		this.actionSource = actionSource;
		this.writer = writer;
	}
	
	public UserHistory(ActionSource actionSource, AbstractWriter writer, long userId, UserHistoryAction userHistoryAction, String actionParamters, String serverName) {
		this(actionSource, writer);
		this.userId = userId;
		this.userHistoryAction = userHistoryAction;
		this.actionParamters = actionParamters;
		this.serverName = serverName;
	}
	
	/**
	 * Set details
	 * @param userId
	 * @param userHistoryAction
	 * @param actionParamters
	 * @param serverName
	 */
	public void setDetails(long userId, UserHistoryAction userHistoryAction, String actionParamters, String serverName) {
		this.userId = userId;
		this.userHistoryAction = userHistoryAction;
		this.actionParamters = actionParamters;
		this.serverName = serverName;
	}
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the userHistoryAction
	 */
	public UserHistoryAction getUserHistoryAction() {
		return userHistoryAction;
	}
	/**
	 * @param userHistoryAction the userHistoryAction to set
	 */
	public void setUserHistoryAction(UserHistoryAction userHistoryAction) {
		this.userHistoryAction = userHistoryAction;
	}
	/**
	 * @return the actionParamters
	 */
	public String getActionParamters() {
		return actionParamters;
	}
	/**
	 * @param actionParamters the actionParamters to set
	 */
	public void setActionParamters(String actionParamters) {
		this.actionParamters = actionParamters;
	}
	/**
	 * @return the actionSource
	 */
	public ActionSource getActionSource() {
		return actionSource;
	}
	/**
	 * @param actionSource the actionSource to set
	 */
	public void setActionSource(ActionSource actionSource) {
		this.actionSource = actionSource;
	}
	/**
	 * @return the serverName
	 */
	public String getServerName() {
		return serverName;
	}
	/**
	 * @param serverName the serverName to set
	 */
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UserHistory [id=" + id + ", userId=" + userId + ", userHistoryAction=" + userHistoryAction
				+ ", actionParamters=" + actionParamters + ", actionSource=" + actionSource + ", writer=" + writer
				+ ", serverName=" + serverName + ", timeCreated=" + timeCreated + "]";
	}

	/**
	 * @return the writer
	 */
	public AbstractWriter getWriter() {
		return writer;
	}

	/**
	 * @param writer the writer to set
	 */
	public void setWriter(AbstractWriter writer) {
		this.writer = writer;
	}
	
}
