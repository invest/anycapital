package capital.any.model.base;

import java.io.Serializable;

import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * @author Eyal.o
 *
 */
public class MsgResFile implements Serializable {
	private static final long serialVersionUID = 1817657987581380441L;
	
	private int actionSourceId;
	private int typeId;
	private int languageId;
	private boolean addKey;
	private int writerId;
	private MultipartFile fileUpload;
	
	/**
	 * @return the actionSourceId
	 */
	public int getActionSourceId() {
		return actionSourceId;
	}
	/**
	 * @param actionSourceId the actionSourceId to set
	 */
	public void setActionSourceId(int actionSourceId) {
		this.actionSourceId = actionSourceId;
	}
	/**
	 * @return the typeId
	 */
	public int getTypeId() {
		return typeId;
	}
	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	/**
	 * @return the languageId
	 */
	public int getLanguageId() {
		return languageId;
	}
	/**
	 * @param languageId the languageId to set
	 */
	public void setLanguageId(int languageId) {
		this.languageId = languageId;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MsgResFile [actionSourceId=" + actionSourceId + ", typeId=" + typeId + ", languageId=" + languageId
				+ ", addKey=" + addKey + ", writerId=" + writerId + ", fileUpload=" + fileUpload + "]";
	}
	/**
	 * @return the addKey
	 */
	public boolean isAddKey() {
		return addKey;
	}
	/**
	 * @param addKey the addKey to set
	 */
	public void setAddKey(boolean addKey) {
		this.addKey = addKey;
	}
	/**
	 * @return the writerId
	 */
	public int getWriterId() {
		return writerId;
	}
	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(int writerId) {
		this.writerId = writerId;
	}
	/**
	 * @return the fileUpload
	 */
	public MultipartFile getFileUpload() {
		return fileUpload;
	}
	/**
	 * @param fileUpload the fileUpload to set
	 */
	public void setFileUpload(MultipartFile fileUpload) {
		this.fileUpload = fileUpload;
	}
	

}
