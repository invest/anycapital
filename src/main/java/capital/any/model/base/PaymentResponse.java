package capital.any.model.base;

/**
 * @author LioR SoLoMoN
 *
 */
public class PaymentResponse {
	private Transaction transaction;
	private long minimumFeeAmount;
	private long minimumAmount;

	/**
	 * @return the transaction
	 */
	public Transaction getTransaction() {
		return transaction;
	}

	/**
	 * @param transaction the transaction to set
	 */
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	/**
	 * @return the minimumFeeAmount
	 */
	public long getMinimumFeeAmount() {
		return minimumFeeAmount;
	}

	/**
	 * @param minimumFeeAmount the minimumFeeAmount to set
	 */
	public void setMinimumFeeAmount(long minimumFeeAmount) {
		this.minimumFeeAmount = minimumFeeAmount;
	}

	/**
	 * @return the minimumAmount
	 */
	public long getMinimumAmount() {
		return minimumAmount;
	}

	/**
	 * @param minimumAmount the minimumAmount to set
	 */
	public void setMinimumAmount(long minimumAmount) {
		this.minimumAmount = minimumAmount;
	}
}
