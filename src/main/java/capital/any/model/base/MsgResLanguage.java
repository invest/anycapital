package capital.any.model.base;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author eranl
 *
 */
public class MsgResLanguage implements Serializable {
	
	private static final long serialVersionUID = -2825387019741104729L;
	
	private MsgRes msgRes;
	private int msgResLanguageId;
	private String value;
	private int writerId;
	private Date timeUpdated;
	private String largeValue;
		
	/**
	 * @return the msgRes
	 */
	public MsgRes getMsgRes() {
		return msgRes;
	}
	/**
	 * @param msgRes the msgRes to set
	 */
	public void setMsgRes(MsgRes msgRes) {
		this.msgRes = msgRes;
	}
	/**
	 * @return the msgResLanguageId
	 */
	public int getMsgResLanguageId() {
		return msgResLanguageId;
	}
	/**
	 * @param msgResLanguageId the msgResLanguageId to set
	 */
	public void setMsgResLanguageId(int msgResLanguageId) {
		this.msgResLanguageId = msgResLanguageId;
	}
	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
		
	/**
	 * DONT USE IT! only in use for insert to db
	 * @return 
	 */
	@JsonIgnore
	public int getMsgResId() {
		return msgRes.getId();
	}
	/**
	 * @return the writerId
	 */
	public int getWriterId() {
		return writerId;
	}
	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(int writerId) {
		this.writerId = writerId;
	}
	/**
	 * @return the timeUpdated
	 */
	public Date getTimeUpdated() {
		return timeUpdated;
	}
	/**
	 * @param timeUpdated the timeUpdated to set
	 */
	public void setTimeUpdated(Date timeUpdated) {
		this.timeUpdated = timeUpdated;
	}
	/**
	 * @return the largeValue
	 */
	public String getLargeValue() {
		return largeValue;
	}
	/**
	 * @param largeValue the largeValue to set
	 */
	public void setLargeValue(String largeValue) {
		this.largeValue = largeValue;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MsgResLanguage [msgRes=" + msgRes + ", msgResLanguageId=" + msgResLanguageId + ", value=" + value
				+ ", writerId=" + writerId + ", timeUpdated=" + timeUpdated + ", largeValue=" + largeValue + "]";
	}
	
}
