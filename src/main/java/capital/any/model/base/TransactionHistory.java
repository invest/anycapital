package capital.any.model.base;

import java.io.Serializable;
import java.util.Date;
import capital.any.base.enums.ActionSource;

/**
 * @author LioR SoLoMoN
 *
 */
public class TransactionHistory implements Serializable {	
	private static final long serialVersionUID = 1506462766866021699L;
	private long transactionId;
	private TransactionStatus status;
	private AbstractWriter writer;
	private String serverName;
	private ActionSource actionSource;
	private Date timeCreated;
	private long loginId;
	
	/**
	 * 
	 */
	public TransactionHistory() {
		
	}
	
	/**
	 * @param transactionId
	 * @param status
	 * @param writer
	 * @param serverName
	 * @param actionSource
	 * @param timeCreated
	 * @param loginId
	 */
	public TransactionHistory(long transactionId, TransactionStatus status, AbstractWriter writer, String serverName,
			ActionSource actionSource, Date timeCreated, long loginId) {
		this.transactionId = transactionId;
		this.status = status;
		this.writer = writer;
		this.serverName = serverName;
		this.actionSource = actionSource;
		this.timeCreated = timeCreated;
		this.loginId = loginId;
	}
	
	/**
	 * @param transactionId
	 * @param status
	 * @param writer
	 * @param serverName
	 * @param actionSource
	 * @param timeCreated
	 */
	public TransactionHistory(long transactionId, TransactionStatus status, AbstractWriter writer, String serverName,
			ActionSource actionSource, Date timeCreated) {
		this.transactionId = transactionId;
		this.status = status;
		this.writer = writer;
		this.serverName = serverName;
		this.actionSource = actionSource;
		this.timeCreated = timeCreated;
	}
	
	/**
	 * @param writer
	 * @param actionSource
	 */
	public TransactionHistory(AbstractWriter writer, ActionSource actionSource) {
		this.writer = writer;
		this.actionSource = actionSource;
	}

	/**
	 * @param transactionId
	 * @param status
	 * @param writer
	 * @param serverName
	 * @param actionSource
	 */
	public TransactionHistory(long transactionId, TransactionStatus status, 
			AbstractWriter writer, String serverName, ActionSource actionSource) {
		this.transactionId = transactionId;
		this.status = status;
		this.writer = writer;
		this.serverName = serverName;
		this.actionSource = actionSource;
	}

	@Override
	public String toString() {
		return "TransactionHistory [transactionId=" + transactionId + ", status=" + status + ", writer=" + writer
				+ ", serverName=" + serverName + ", actionSource=" + actionSource + ", timeCreated=" + timeCreated
				+ ", loginId=" + loginId + "]";
	}
	/**
	 * @return the transactionId
	 */
	public long getTransactionId() {
		return transactionId;
	}
	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
	/**
	 * @return the status
	 */
	public TransactionStatus getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(TransactionStatus status) {
		this.status = status;
	}
	/**
	 * @return the writer
	 */
	public AbstractWriter getWriter() {
		return writer;
	}
	/**
	 * @param writer the writer to set
	 */
	public void setWriter(AbstractWriter writer) {
		this.writer = writer;
	}		
	/**
	 * @return the serverName
	 */
	public String getServerName() {
		return serverName;
	}
	/**
	 * @param serverName the serverName to set
	 */
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}
	/**
	 * @return the actionSource
	 */
	public ActionSource getActionSource() {
		return actionSource;
	}
	/**
	 * @param actionSource the actionSource to set
	 */
	public void setActionSource(ActionSource actionSource) {
		this.actionSource = actionSource;
	}
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the loginId
	 */
	public long getLoginId() {
		return loginId;
	}

	/**
	 * @param loginId the loginId to set
	 */
	public void setLoginId(long loginId) {
		this.loginId = loginId;
	}
}
