package capital.any.model.base;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import capital.any.service.base.emailAction.handler.EmailActionHandler;

/**
 * 
 * @author eyal.ohana
 *
 */
public class EmailAction implements Serializable {
	private static final long serialVersionUID = 8813367476136719483L;
	
	private int id;
	private String name;
	private String displayName;
	private String classPath;
	private boolean isDisplay;
	
	/**
	 * EmailAction
	 */
	public EmailAction() {
		
	}
	
	/**
	 * EmailAction
	 * @param id
	 */
	public EmailAction(int id) {
		this.id = id;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}
	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	/**
	 * @return the isDisplay
	 */
	public boolean isDisplay() {
		return isDisplay;
	}
	/**
	 * @param isDisplay the isDisplay to set
	 */
	public void setDisplay(boolean isDisplay) {
		this.isDisplay = isDisplay;
	}

	/**
	 * @return the classPath
	 */
	public String getClassPath() {
		return classPath;
	}

	/**
	 * @param classPath the classPath to set
	 */
	public void setClassPath(String classPath) {
		this.classPath = classPath;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EmailAction [id=" + id + ", name=" + name + ", displayName=" + displayName + ", classPath=" + classPath
				+ ", isDisplay=" + isDisplay + "]";
	}
}
