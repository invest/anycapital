package capital.any.model.base;

import java.io.Serializable;
import java.util.Date;

/**
 * @author eranl
 *
 */
public class Wire extends paymentMethod implements Serializable {

	private static final long serialVersionUID = -5762236218012196784L;
	private long id;
	private Date timeCreated;
	private long transactionId;
	private String branchAddress;
	private String bankName;
	private String iban;
	private String swift;
	private String accountInfo;
	private long accountNum;
	private String beneficiaryName;
	private long bankFeeAmount;
	private int branchNumber;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	/**
	 * @return the transactionId
	 */
	public long getTransactionId() {
		return transactionId;
	}
	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
	/**
	 * @return the branchAddress
	 */
	public String getBranchAddress() {
		return branchAddress;
	}
	/**
	 * @param branchAddress the branchAddress to set
	 */
	public void setBranchAddress(String branchAddress) {
		this.branchAddress = branchAddress;
	}
	/**
	 * @return the bankName
	 */
	public String getBankName() {
		return bankName;
	}
	/**
	 * @param bankName the bankName to set
	 */
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	/**
	 * @return the iban
	 */
	public String getIban() {
		return iban;
	}
	/**
	 * @param iban the iban to set
	 */
	public void setIban(String iban) {
		this.iban = iban;
	}
	/**
	 * @return the swift
	 */
	public String getSwift() {
		return swift;
	}
	/**
	 * @param swift the swift to set
	 */
	public void setSwift(String swift) {
		this.swift = swift;
	}
	/**
	 * @return the accountInfo
	 */
	public String getAccountInfo() {
		return accountInfo;
	}
	/**
	 * @param accountInfo the accountInfo to set
	 */
	public void setAccountInfo(String accountInfo) {
		this.accountInfo = accountInfo;
	}
	/**
	 * @return the accountNum
	 */
	public long getAccountNum() {
		return accountNum;
	}
	/**
	 * @param accountNum the accountNum to set
	 */
	public void setAccountNum(long accountNum) {
		this.accountNum = accountNum;
	}
	/**
	 * @return the beneficiaryName
	 */
	public String getBeneficiaryName() {
		return beneficiaryName;
	}
	/**
	 * @param beneficiaryName the beneficiaryName to set
	 */
	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}
	/**
	 * @return the bankFeeAmount
	 */
	public long getBankFeeAmount() {
		return bankFeeAmount;
	}
	/**
	 * @param bankFeeAmount the bankFeeAmount to set
	 */
	public void setBankFeeAmount(long bankFeeAmount) {
		this.bankFeeAmount = bankFeeAmount;
	}
	
	/**
	 * @return the branchNumber
	 */
	public int getBranchNumber() {
		return branchNumber;
	}
	/**
	 * @param branchNumber the branchNumber to set
	 */
	public void setBranchNumber(int branchNumber) {
		this.branchNumber = branchNumber;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Wire [id=" + id + ", timeCreated=" + timeCreated + ", transactionId=" + transactionId
				+ ", branchAddress=" + branchAddress + ", bankName=" + bankName + ", iban=" + iban + ", swift=" + swift
				+ ", accountInfo=" + accountInfo + ", accountNum=" + accountNum + ", beneficiaryName=" + beneficiaryName
				+ ", bankFeeAmount=" + bankFeeAmount + ", bankFeeAmount=" + bankFeeAmount + "]";
	}
			
}
