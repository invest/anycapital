package capital.any.model.base;

import java.io.Serializable;

/**
 * @author LioR SoLoMoN
 *
 */
//TODO may delete
public class LoginInputs implements Serializable {
	private static final long serialVersionUID = -7442215457892358188L;
	public String email;
	public String userId;
	
	@Override
	public String toString() {
		return "LoginInputs [email=" + email + ", userId=" + userId + "]";
	}
}
