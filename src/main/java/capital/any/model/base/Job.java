package capital.any.model.base;

import java.io.Serializable;
import java.util.Date;

/**
 * @author eranl
 *
 */
public class Job implements Serializable {

	private static final long serialVersionUID = -8537012971559439544L;
	
	private int id;
	private Date timeCreated;
	private boolean running;
	private Date lastRunTime;
	private String cronExpression;
	private String description;
	private String jobClass;
	private String config;
	private boolean isActive;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	/**
	 * @return the running
	 */
	public boolean isRunning() {
		return running;
	}
	/**
	 * @param running the running to set
	 */
	public void setRunning(boolean running) {
		this.running = running;
	}
	/**
	 * @return the lastRunTime
	 */
	public Date getLastRunTime() {
		return lastRunTime;
	}
	/**
	 * @param lastRunTime the lastRunTime to set
	 */
	public void setLastRunTime(Date lastRunTime) {
		this.lastRunTime = lastRunTime;
	}
	/**
	 * @return the cronExpression
	 */
	public String getCronExpression() {
		return cronExpression;
	}
	/**
	 * @param cronExpression the cronExpression to set
	 */
	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the jobClass
	 */
	public String getJobClass() {
		return jobClass;
	}
	/**
	 * @param jobClass the jobClass to set
	 */
	public void setJobClass(String jobClass) {
		this.jobClass = jobClass;
	}
	/**
	 * @return the config
	 */
	public String getConfig() {
		return config;
	}
	/**
	 * @param config the config to set
	 */
	public void setConfig(String config) {
		this.config = config;
	}
	/**
	 * @return the isActive
	 */
	public boolean isActive() {
		return isActive;
	}
	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Job [id=" + id + ", timeCreated=" + timeCreated + ", running=" + running + ", lastRunTime="
				+ lastRunTime + ", cronExpression=" + cronExpression + ", description=" + description + ", jobClass="
				+ jobClass + ", config=" + config + ", isActive=" + isActive + "]";
	}
	
}
