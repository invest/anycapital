package capital.any.model.base;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonIgnore;
import capital.any.base.enums.ActionSource;
import capital.any.communication.Response;
import capital.any.model.base.Transaction.TransactionStatusEnum;

/**
 * @author LioR SoLoMoN
 * @param <T>
 *
 */
public class Payment<T> implements Serializable {
	private static final long serialVersionUID = 3918645572339899204L;
	private PaymentRequest<T> paymentRequest;
	private PaymentResponse paymentResponse;
	private User user;
	private AbstractWriter writer;
	private Transaction transaction; //TODO here?
	private ActionSource actionSource;
	private String ip;
	private String serverName;
	private Response<?> response; //i know, but it could be other class... instead, i reused :()
	private Login login;

	/**
	 * @param paymentDetails
	 * @param user
	 * @param writer
	 * @param actionSource
	 * @param ip
	 */
	public Payment(PaymentRequest<T> paymentDetails, User user,
			AbstractWriter writer, ActionSource actionSource, String ip) {
		super();
		this.paymentRequest = paymentDetails;
		this.user = user;
		this.writer = writer;
		this.actionSource = actionSource;
		this.ip = ip;
		this.paymentResponse = new PaymentResponse();
	}
	
	/**
	 * @param paymentDetails
	 * @param user
	 * @param writer
	 * @param actionSource
	 * @param ip
	 * @param login
	 */
	public Payment(PaymentRequest<T> paymentDetails, User user,
			AbstractWriter writer, ActionSource actionSource, String ip, Login login) {
		this(paymentDetails, user, writer, actionSource, ip);
		this.login = login;
	}

	@Override
	public String toString() {
		return "Payment [paymentRequest=" + paymentRequest + ", paymentResponse=" + paymentResponse + ", user=" + user
				+ ", writer=" + writer + ", transaction=" + transaction + ", actionSource=" + actionSource + ", ip="
				+ ip + ", serverName=" + serverName + ", response=" + response + ", login=" + login + "]";
	}

	public boolean isOK() {
		return response.getResponseCode().isOK();
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the actionSource
	 */
	public ActionSource getActionSource() {
		return actionSource;
	}

	/**
	 * @param actionSource the actionSource to set
	 */
	public void setActionSource(ActionSource actionSource) {
		this.actionSource = actionSource;
	}

	/**
	 * @return the writer
	 */
	public AbstractWriter getWriter() {
		return writer;
	}

	/**
	 * @param writer the writer to set
	 */
	public void setWriter(AbstractWriter writer) {
		this.writer = writer;
	}

	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * @param ip the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

	/**
	 * @return the paymentDetails
	 */
	public PaymentRequest<T> getPaymentDetails() {
		return paymentRequest;
	}

	/**
	 * @param paymentDetails the paymentDetails to set
	 */
	public void setPaymentDetails(PaymentRequest<T> paymentDetails) {
		this.paymentRequest = paymentDetails;
	}

	/**
	 * @return the transaction
	 */
	public Transaction getTransaction() {
		return transaction;
	}

	/**
	 * @param transaction the transaction to set
	 */
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}


	/**
	 * @return the serverName
	 */
	public String getServerName() {
		return serverName;
	}

	/**
	 * @param serverName the serverName to set
	 */
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	/**
	 * @return the paymentResponse
	 */
	public PaymentResponse getPaymentResponse() {
		return paymentResponse;
	}

	/**
	 * @param paymentResponse the paymentResponse to set
	 */
	public void setPaymentResponse(PaymentResponse paymentResponse) {
		this.paymentResponse = paymentResponse;
	}

	/**
	 * @return the login
	 */
	public Login getLogin() {
		return login;
	}

	/**
	 * @param login the login to set
	 */
	public void setLogin(Login login) {
		this.login = login;
	}
	
	@JsonIgnore
	public long getLoginId() {
		return login != null ? login.getId() : 0;
	}

	/**
	 * @return the response
	 */
	public Response<?> getResponse() {
		return response;
	}

	/**
	 * @param response the response to set
	 */
	public void setResponse(Response<?> response) {
		this.response = response;
	}

	/**
	 * @return true if balance need to change
	 */
	public boolean changeBalance() {
		boolean result = false;
		if (TransactionStatusEnum.SUCCEED.getId() == transaction.getStatus().getId()) {
			result = true;
		}
		return result;
	}

	public boolean marketingFlow() {
		boolean result = false;
		if (TransactionStatusEnum.SUCCEED.getId() == transaction.getStatus().getId() &&
				transaction.getPaymentType().isReal()) {
			result = true;
		}
		return result;
	}
}
