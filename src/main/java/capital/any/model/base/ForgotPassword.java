package capital.any.model.base;

import capital.any.base.enums.CommunicationChannel;

/**
 * @author LioR SoLoMoN
 *
 */
public class ForgotPassword {
	private CommunicationChannel communicationChannel;
	private String email;
	
	/**
	 * 
	 */
	public ForgotPassword() {
	
	}
	
	/**
	 * @param communicationChannel
	 * @param email
	 */
	public ForgotPassword(CommunicationChannel communicationChannel, String email) {
		super();
		this.communicationChannel = communicationChannel;
		this.email = email;
	}
	/**
	 * @return the communicationChannel
	 */
	public CommunicationChannel getCommunicationChannel() {
		return communicationChannel;
	}
	/**
	 * @param communicationChannel the communicationChannel to set
	 */
	public void setCommunicationChannel(CommunicationChannel communicationChannel) {
		this.communicationChannel = communicationChannel;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}	
}
