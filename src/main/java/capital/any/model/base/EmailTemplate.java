package capital.any.model.base;

import java.io.Serializable;

/**
 * 
 * @author eyal.ohana
 *
 */
public class EmailTemplate implements Serializable {
	private static final long serialVersionUID = -5267186905604367258L;
	
	private int id;
	private int actionId;
	private int languageId;
	private String templateId;
	
	/**
	 * 
	 */
	public EmailTemplate() {
		
	}
	
	/**
	 * 
	 * @param actionId
	 * @param languageId
	 */
	public EmailTemplate(int actionId, int languageId) {
		this.actionId = actionId;
		this.languageId = languageId;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the actionId
	 */
	public int getActionId() {
		return actionId;
	}
	/**
	 * @param actionId the actionId to set
	 */
	public void setActionId(int actionId) {
		this.actionId = actionId;
	}
	/**
	 * @return the languageId
	 */
	public int getLanguageId() {
		return languageId;
	}
	/**
	 * @param languageId the languageId to set
	 */
	public void setLanguageId(int languageId) {
		this.languageId = languageId;
	}
	/**
	 * @return the templateId
	 */
	public String getTemplateId() {
		return templateId;
	}
	/**
	 * @param templateId the templateId to set
	 */
	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EmailTemplate [id=" + id + ", actionId=" + actionId + ", languageId=" + languageId + ", templateId="
				+ templateId + "]";
	}
	
	
}
