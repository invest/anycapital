package capital.any.model.base;

import java.util.Date;

/**
 * @author LioR SoLoMoN
 *
 */
public class NullWriter extends AbstractWriter {
	private static final long serialVersionUID = -4349095679189942775L;
	
	public NullWriter() {
		
	}
	@Override
	public boolean isNull() {
		return true;
	}
	
	@Override
	public Integer getId() {
		return null;
	}

	@Override
	public void setId(Integer id) {
				
	}

	@Override
	public Date getTimeCreated() {
		return null;
	}

	@Override
	public void setTimeCreated(Date timeCreated) {
				
	}

	@Override
	public String getUserName() {
		return "Bot";
	}

	@Override
	public void setUserName(String userName) {
		
	}

	@Override
	public String getPassword() {
		return null;
	}

	@Override
	public void setPassword(String password) {
	
	}
	
	@Override
	public String getFirstName() {
		return "Bot";
	}

	@Override
	public void setFirstName(String firstName) {
		
	}

	@Override
	public String getLastName() {
		return null;
	}

	@Override
	public void setLastName(String lastName) {
		
	}

	@Override
	public String getEmail() {
		return null;
	}

	@Override
	public void setEmail(String email) {
		
	}

	@Override
	public String getComments() {
		return null;
	}

	@Override
	public void setComments(String comments) {
		
	}
	
	@Override
	public int getUtcOffset() {
		return 0;
	}

	@Override
	public void setUtcOffset(int utcOffset) {
	
	}

	@Override
	public boolean isActive() {
		return false;
	}

	@Override
	public void setIsActive(boolean isActive) {
		
	}
	@Override
	public void setNull(String str) {
		// Fake for deserialize json	
	}	
	
	public void setActive(String str) {
		// Fake for deserialize json
	}

}
