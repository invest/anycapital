package capital.any.model.base;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Eyal G
 *
 */
public class ProductCouponFrequency implements Serializable {
	
	private static final long serialVersionUID = 8133837101690841238L;
	
	public static final int NO_COUPON 	= 1;
	public static final int MONTHLY 	= 2;
	public static final int QUATERLY 	= 3;
	public static final int SEMI_ANNUAL = 4;
	public static final int ANNUAL 		= 5;
	public static final int IN_FINE 	= 6;

	private int id;
	private String name;
	private String displayName;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;		
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	@JsonIgnore
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}	
	
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	@Override
	public String toString() {
		return "ProductCouponFrequency [id=" + id + ", name=" + name + ", displayName=" + displayName + "]";
	}	
}
