package capital.any.model.base;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author eyal.ohana
 *
 */
public class MarketingLandingPage implements Serializable {
	private static final long serialVersionUID = 6050057126365948969L;
	
	private int id;
	private String name;
	private String path;
	private int writerId;
	private Date timeCreated;
	private Date timeModified;
	private AbstractWriter writer;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}
	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}
	/**
	 * @return the writerId
	 */
	public int getWriterId() {
		return writerId;
	}
	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(int writerId) {
		this.writerId = writerId;
	}
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	/**
	 * @return the timeModified
	 */
	public Date getTimeModified() {
		return timeModified;
	}
	/**
	 * @param timeModified the timeModified to set
	 */
	public void setTimeModified(Date timeModified) {
		this.timeModified = timeModified;
	}
	
	/**
	 * @return the writer
	 */
	public AbstractWriter getWriter() {
		return writer;
	}
	/**
	 * @param writer the writer to set
	 */
	public void setWriter(AbstractWriter writer) {
		this.writer = writer;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MarketingLandingPage [id=" + id + ", name=" + name + ", path=" + path + ", writerId=" + writerId
				+ ", timeCreated=" + timeCreated + ", timeModified=" + timeModified + ", writer=" + writer + "]";
	}
}
