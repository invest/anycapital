package capital.any.model.base;

import java.io.Serializable;

import capital.any.base.enums.ActionSource;

/**
 * 
 * @author eyal.ohana
 * @param <T>
 * @param <T>
 *
 */
public class EmailActionRequest<T> implements Serializable {
	private static final long serialVersionUID = 6624500062572441100L;
	
	private EmailAction emailAction;
	private User user;
	private T data;
	private ActionSource actionSource;
	private int emailTo;
	private AbstractWriter writer;
	
	/**
	 * EmailActionRequest
	 */
	public EmailActionRequest() {
		
	}
	
	/**
	 * EmailActionRequest
	 * @param user
	 * @param emailAction
	 */
	public EmailActionRequest(User user, EmailAction emailAction) {
		super();
		this.user = user;
		this.emailAction = emailAction;
	}
	
	/**
	 * Email action request
	 * @param user
	 * @param emailAction
	 */
	public EmailActionRequest(User user, EmailAction emailAction ,T data) {
		super();
		this.user = user;
		this.emailAction = emailAction;
		this.data = data;
	}
	
	/**
	 * @return the emailAction
	 */
	public EmailAction getEmailAction() {
		return emailAction;
	}

	/**
	 * @param emailAction the emailAction to set
	 */
	public void setEmailAction(EmailAction emailAction) {
		this.emailAction = emailAction;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the data
	 */
	public T getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(T data) {
		this.data = data;
	}

	/**
	 * @return the actionSource
	 */
	public ActionSource getActionSource() {
		return actionSource;
	}

	/**
	 * @param actionSource the actionSource to set
	 */
	public void setActionSource(ActionSource actionSource) {
		this.actionSource = actionSource;
	}

	/**
	 * @return the emailTo
	 */
	public int getEmailTo() {
		return emailTo;
	}

	/**
	 * @param emailTo the emailTo to set
	 */
	public void setEmailTo(int emailTo) {
		this.emailTo = emailTo;
	}

	/**
	 * @return the writer
	 */
	public AbstractWriter getWriter() {
		return writer;
	}

	/**
	 * @param writer the writer to set
	 */
	public void setWriter(AbstractWriter writer) {
		this.writer = writer;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EmailActionRequest [emailAction=" + emailAction + ", user=" + user + ", data=" + data
				+ ", actionSource=" + actionSource + ", emailTo=" + emailTo + ", writer=" + writer + "]";
	}


}
