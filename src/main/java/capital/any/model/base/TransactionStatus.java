package capital.any.model.base;

import java.io.Serializable;

/**
 * @author LioR SoLoMoN
 *
 */
public class TransactionStatus implements Serializable {
	
	private static final long serialVersionUID = -3033417487777360522L;
	private int id;
	private String name;
	private String displayName;
	
	/**
	 * 
	 */
	public TransactionStatus() {
		
	}
	
	/**
	 * @param id
	 */
	public TransactionStatus(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "TransactionStatus [id=" + id + ", name=" + getName() + ", displayName=" + getDisplayName() + "]";
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}
	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
}
