package capital.any.model.base;

import java.io.Serializable;
import java.util.Map;

/**
 * 
 * @author eyal.ohana
 *
 */
public class MailInfo implements Serializable {
	private static final long serialVersionUID = -7923369264072320077L;
	
	private String from;
	private String to;
	private String subject;
	private Map<String, String> map;
	private String templateName;
	private String text;
	private int typeId;
	private int languageId;
	private String templateId;
	private int emailActionId;
	
	/**
	 * 
	 */
	public MailInfo() {
	}
	
	/**
	 * for sending email with template
	 * @param to
	 * @param subject
	 * @param map
	 * @param templateName
	 */
	public MailInfo(String to, String subject, Map<String, String> map, int emailActionId, int typeId, int languageId) {
		this.to = to;
		this.subject = subject;
		this.map = map;
		this.emailActionId = emailActionId;
		this.typeId = typeId;
		this.languageId = languageId;
	}
	
	/**
	 * for sending email without template
	 * @param to
	 * @param subject
	 * @param map
	 * @param templateName
	 * @param text
	 */
	public MailInfo(String to, String subject, String text, int typeId, int emailActionId) {
		this(to, subject, null, emailActionId, typeId, 0);
		this.text = text;
	}
	
	/**
	 * @return the from
	 */
	public String getFrom() {
		return from;
	}
	/**
	 * @param from the from to set
	 */
	public void setFrom(String from) {
		this.from = from;
	}
	/**
	 * @return the to
	 */
	public String getTo() {
		return to;
	}
	/**
	 * @param to the to to set
	 */
	public void setTo(String to) {
		this.to = to;
	}
	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}
	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	/**
	 * @return the map
	 */
	public Map<String, String> getMap() {
		return map;
	}
	/**
	 * @param map the map to set
	 */
	public void setMap(Map<String, String> map) {
		this.map = map;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return the typeId
	 */
	public int getTypeId() {
		return typeId;
	}

	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	/**
	 * @return the languageId
	 */
	public int getLanguageId() {
		return languageId;
	}

	/**
	 * @param languageId the languageId to set
	 */
	public void setLanguageId(int languageId) {
		this.languageId = languageId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MailInfo [from=" + from + ", to=" + to + ", subject=" + subject + ", map=" + map + ", templateName="
				+ templateName + ", text=" + text + ", typeId=" + typeId + ", languageId=" + languageId
				+ ", templateId=" + templateId + ", emailActionId=" + emailActionId + "]";
	}

	/**
	 * @return the templateId
	 */
	public String getTemplateId() {
		return templateId;
	}

	/**
	 * @param templateId the templateId to set
	 */
	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	/**
	 * @return the templateName
	 */
	public String getTemplateName() {
		return templateName;
	}

	/**
	 * @param templateName the templateName to set
	 */
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	/**
	 * @return the emailActionId
	 */
	public int getEmailActionId() {
		return emailActionId;
	}

	/**
	 * @param emailActionId the emailActionId to set
	 */
	public void setEmailActionId(int emailActionId) {
		this.emailActionId = emailActionId;
	}

}
