package capital.any.model.base;

import java.util.List;

import com.sendgrid.Mail;

/**
 * 
 * @author eyal.ohana
 *
 */
public class Sendgrid {
	private Mail mail;
	private String campaignId;
	private List<SendgridRecipient> sendgridRecipientList;
	private String endpoint;
	private String body;
	
	/**
	 * @return the mail
	 */
	public Mail getMail() {
		return mail;
	}
	/**
	 * @param mail the mail to set
	 */
	public void setMail(Mail mail) {
		this.mail = mail;
	}
	/**
	 * @return the campaignId
	 */
	public String getCampaignId() {
		return campaignId;
	}
	/**
	 * @param campaignId the campaignId to set
	 */
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}
	/**
	 * @return the sendgridRecipientList
	 */
	public List<SendgridRecipient> getSendgridRecipientList() {
		return sendgridRecipientList;
	}
	/**
	 * @param sendgridRecipientList the sendgridRecipientList to set
	 */
	public void setSendgridRecipientList(List<SendgridRecipient> sendgridRecipientList) {
		this.sendgridRecipientList = sendgridRecipientList;
	}
	
	/**
	 * @return the endpoint
	 */
	public String getEndpoint() {
		return endpoint;
	}
	/**
	 * @param endpoint the endpoint to set
	 */
	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}
	/**
	 * @return the body
	 */
	public String getBody() {
		return body;
	}
	/**
	 * @param body the body to set
	 */
	public void setBody(String body) {
		this.body = body;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Sendgrid [mail=" + mail + ", campaignId=" + campaignId + ", sendgridRecipientList="
				+ sendgridRecipientList + ", endpoint=" + endpoint + ", body=" + body + "]";
	}
	
}
