package capital.any.model.base;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author LioR SoLoMoN
 *
 */
public class Transaction implements Serializable {
	private static final long serialVersionUID = 1443211390295599372L;
	private long id;
	private User user;
	private long amount;
	private double rate;
	private String ip;
	private String comments;
	private TransactionOperation operation;
	private TransactionPaymentType paymentType;
	private TransactionStatus status;
	private Date timeCreated;
	private Date timeSettled;
	private TransactionCoupon coupon;
	private TransactionReference reference;
	
	/**
	 * 
	 */
	public Transaction() {
	
	}
	
	public Transaction(long id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return "Transaction [id=" + id + ", user=" + user + ", amount=" + amount + ", rate=" + rate + ", ip=" + ip
				+ ", comments=" + comments + ", operation=" + operation + ", paymentType=" + paymentType + ", status="
				+ status + ", timeCreated=" + timeCreated + ", timeSettled=" + timeSettled + ", coupon=" + coupon
				+ ", reference=" + reference + "]";
	}
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the status
	 */
	public TransactionStatus getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(TransactionStatus status) {
		this.status = status;
	}		
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	/**
	 * @return the timeSettled
	 */
	public Date getTimeSettled() {
		return timeSettled;
	}
	/**
	 * @param timeSettled the timeSettled to set
	 */
	public void setTimeSettled(Date timeSettled) {
		this.timeSettled = timeSettled;
	}
	/**
	 * @return the amount
	 */
	public long getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(long amount) {
		this.amount = amount;
	}
	
	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}
	/**
	 * @param ip the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	/**
	 * @return the rate
	 */
	public double getRate() {
		return rate;
	}
	/**
	 * @param rate the rate to set
	 */
	public void setRate(double rate) {
		this.rate = rate;
	}
	
	/**
	 * @return the operation
	 */
	public TransactionOperation getOperation() {
		return operation;
	}
	/**
	 * @param operation the operation to set
	 */
	public void setOperation(TransactionOperation operation) {
		this.operation = operation;
	}

	/**
	 * @return the paymentType
	 */
	public TransactionPaymentType getPaymentType() {
		return paymentType;
	}
	/**
	 * @param paymentType the paymentType to set
	 */
	public void setPaymentType(TransactionPaymentType paymentType) {
		this.paymentType = paymentType;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	public enum TransactionPaymentTypeEnum {
		TEST(0),
		WIRE(1),
		INVESTMENT_COUPON(2),
		ADMIN(3);
		
		private TransactionPaymentTypeEnum(int id) {
			this.id = id;
		}

		private int id;
		
		private static final Map<Integer, TransactionPaymentTypeEnum> ID_PAYMENT_TYPE_MAP = 
				new HashMap<Integer, TransactionPaymentTypeEnum>(TransactionPaymentTypeEnum.values().length);
		static {
			for (TransactionPaymentTypeEnum pt : TransactionPaymentTypeEnum.values()) {
				ID_PAYMENT_TYPE_MAP.put(pt.getId(), pt);
			}
		}
		
		public static TransactionPaymentTypeEnum getById(int id) {
			TransactionPaymentTypeEnum pt = ID_PAYMENT_TYPE_MAP.get(id);
			if (pt == null) {
				throw new IllegalArgumentException("No TransactionPaymentTypeEnum Type with id: " + id);
			} else {
				return pt;
			}
		}
		
		/**
		 * @return the id
		 */
		public int getId() {
			return id;
		}
		/**
		 * @param id the id to set
		 */
		public void setId(int id) {
			this.id = id;
		}
	}
	
	public enum TransactionOperationEnum {
		/**
		 * 1 
		 */
		CREDIT(1),
		/**
		 * 2
		 */
		DEBIT(2);
		
		private int id;
		
		private TransactionOperationEnum(int id) {
			this.id = id;
		}
		
		private static final Map<Integer, TransactionOperationEnum> ID_OPERATION_MAP = new HashMap<Integer, TransactionOperationEnum>(TransactionOperationEnum.values().length);
		static {
			for (TransactionOperationEnum o : TransactionOperationEnum.values()) {
				ID_OPERATION_MAP.put(o.getId(), o);
			}
		}
		
		public static TransactionOperationEnum getById(int id) {
			TransactionOperationEnum o = ID_OPERATION_MAP.get(id);
			if (o == null) {
				throw new IllegalArgumentException("No TransactionOperationEnum with id: " + id);
			} else {
				return o;
			}
		}

		/**
		 * @return the id
		 */
		public int getId() {
			return id;
		}
	}

	public enum TransactionStatusEnum {	
		/**
		 * 1
		 */
		STARTED(1),
		/**
		 * 2
		 */
		SUCCEED(2),		
		/**
		 * 3
		 */
		FAILED(3),
		/**
		 * 4
		 */
		PENDING(4),
		/**
		 * 10
		 */
		WAITING_FOR_FIRST_APPROVE(10),		
		/**
		 * 11
		 */
		WAITING_FOR_SECOND_APPROVE(11),		
		/**
		 * 12
		 */
		CANCEL(12)
		
		;		
		
		private int id;

		private TransactionStatusEnum(int id) {
			this.id = id;
		}
		
		private static final Map<Integer, TransactionStatusEnum> ID_TRANSACTION_STATUS_MAP = 
				new HashMap<Integer, TransactionStatusEnum>(TransactionStatusEnum.values().length);
		static {
			for (TransactionStatusEnum ts : TransactionStatusEnum.values()) {
				ID_TRANSACTION_STATUS_MAP.put(ts.getId(), ts);
			}
		}
		
		/**
		 * @param id
		 * @return
		 */
		public static TransactionStatusEnum get(int id) {
			TransactionStatusEnum ts = ID_TRANSACTION_STATUS_MAP.get(id);
			if (ts == null) {
				throw new IllegalArgumentException("No TransactionStatusEnum with id: " + id);
			} else {
				return ts;
			}
		}
		
		/**
		 * @return the id
		 */
		public int getId() {
			return id;
		}
	}

	/**
	 * @return the coupon
	 */
	public TransactionCoupon getCoupon() {
		return coupon;
	}
	/**
	 * @param coupon the coupon to set
	 */
	public void setCoupon(TransactionCoupon coupon) {
		this.coupon = coupon;
	}
	/**
	 * @return the reference
	 */
	public TransactionReference getReference() {
		return reference;
	}
	/**
	 * @param reference the reference to set
	 */
	public void setReference(TransactionReference reference) {
		this.reference = reference;
	}
}
