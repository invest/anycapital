package capital.any.model.base;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;

import capital.any.model.base.Product.insertOrUpdateProductValidatorGroup;
import capital.any.service.base.product.FirstDateBeforeSecond;

@FirstDateBeforeSecond.List({
	@FirstDateBeforeSecond(first = "barrierStart", second = "barrierEnd", message = "barrier-start-error", groups = Product.insertOrUpdateProductValidatorGroup.class)  
})


public class ProductBarrier implements Serializable {
	
	private static final long serialVersionUID = 5565567996605425963L;
	@NotNull(groups = insertOrUpdateProductValidatorGroup.class)
	@Future(groups = insertOrUpdateProductValidatorGroup.class, message="barrier-start-future")
	private Date barrierStart;
	@NotNull(groups = insertOrUpdateProductValidatorGroup.class)
	private Date barrierEnd;
	private double barrierLevel;
	private boolean isBarrierOccur;
	private ProductBarrierType productBarrierType;
	private long productId;
	private double distanceToBarrier;
	
	public void calculateDistanceToBarrier(BigDecimal marketPrice, BigDecimal initialFixingLevel) {
		BigDecimal hundred = new BigDecimal("100");
		BigDecimal DTB = null;
		if (barrierLevel > 100) {
			DTB = new BigDecimal(String.valueOf(barrierLevel)).divide(hundred).multiply(initialFixingLevel).divide(
					marketPrice, 20, RoundingMode.HALF_UP).subtract(
							new BigDecimal(1));
		} else {
			DTB = new BigDecimal(1).subtract(new BigDecimal(String.valueOf(barrierLevel)).divide(hundred).multiply(initialFixingLevel)
					.divide(marketPrice, 20, RoundingMode.HALF_UP));
		}
		DTB = DTB.multiply(hundred);
		DTB = DTB.setScale(2, RoundingMode.HALF_UP);
		distanceToBarrier = DTB.doubleValue();
	}
	
	/**
	 * @return the barrierStart
	 */
	public Date getBarrierStart() {
		return barrierStart;
	}
	/**
	 * @param barrierStart the barrierStart to set
	 */
	public void setBarrierStart(Date barrierStart) {
		this.barrierStart = barrierStart;
	}
	/**
	 * @return the barrierEnd
	 */
	public Date getBarrierEnd() {
		return barrierEnd;
	}
	/**
	 * @param barrierEnd the barrierEnd to set
	 */
	public void setBarrierEnd(Date barrierEnd) {
		this.barrierEnd = barrierEnd;
	}

	/**
	 * @return the isBarrierOccur
	 */
	public boolean isBarrierOccur() {
		return isBarrierOccur;
	}
	/**
	 * @param isBarrierOccur the isBarrierOccur to set
	 */
	public void setBarrierOccur(boolean isBarrierOccur) {
		this.isBarrierOccur = isBarrierOccur;
	}
	/**
	 * @return the productBarrierType
	 */
	public ProductBarrierType getProductBarrierType() {
		return productBarrierType;
	}
	/**
	 * @param productBarrierType the productBarrierType to set
	 */
	public void setProductBarrierType(ProductBarrierType productBarrierType) {
		this.productBarrierType = productBarrierType;
	}
	/**
	 * @return the productId
	 */
	public long getProductId() {
		return productId;
	}
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(long productId) {
		this.productId = productId;
	}
	/**
	 * @return the barrierLevel
	 */
	public double getBarrierLevel() {
		return barrierLevel;
	}
	/**
	 * @param barrierLevel the barrierLevel to set
	 */
	public void setBarrierLevel(double barrierLevel) {
		this.barrierLevel = barrierLevel;
	}
	
	/**
	 * @return the distanceToBarrier
	 */
	public double getDistanceToBarrier() {
		return distanceToBarrier;
	}
	
	/**
	 * @param distanceToBarrier the distanceToBarrier to set
	 */
	public void setDistanceToBarrier(double distanceToBarrier) {
		this.distanceToBarrier = distanceToBarrier;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ProductBarrier [barrierStart=" + barrierStart + ", barrierEnd=" + barrierEnd + ", barrierLevel="
				+ barrierLevel + ", isBarrierOccur=" + isBarrierOccur + ", productBarrierType=" + productBarrierType
				+ ", productId=" + productId + ", DistanceToBarrier=" + distanceToBarrier + "]";
	}
}
