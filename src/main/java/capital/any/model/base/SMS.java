package capital.any.model.base;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author eran.levy
 *
 */
public class SMS implements Serializable {
	
	private static final long serialVersionUID = -136439520241621153L;
	
	private long id;
	private String sender;
	private String phone;
	private String message;
	private int providerId;
	private int retries;
	private Date timeCreated;
	private Date timeScheduled;
	private Date timeSent;
	private long referenceId;
	private String response;
	private String senderNumber;
	private Type type;
	private Status status;
	
	/**
	 * 
	 */
	public SMS() {
		
	}
	
	/**
	 * @param phone
	 * @param message
	 */
	public SMS(String phone, String message) {
		this.phone = phone;
		this.message = message;
	}
	
	public enum Type {
		/**
		 * 
		 */
		FREE_TEXT(1);

		
		private static final Map<Integer, Type> TOKEN_TYPE_MAP = new HashMap<Integer, Type>(Type.values().length);
		static {
			for (Type g : Type.values()) {
				TOKEN_TYPE_MAP.put(g.getToken(), g);
			}
		}
		
		private int token;
		
		/**
		 * @param token
		 * @return
		 */
		public static Type getByToken(int token) {
			Type v = TOKEN_TYPE_MAP.get(token);
			if (v == null) {
				throw new IllegalArgumentException("No type with token: " + token);
			} else {
				return v;
			}
		}
		
		/**
		 * @param token
		 */
		private Type(int token) {
			this.token = token;
		}
		
		/**
		 * @return
		 */
		public int getToken() {
			return this.token;
		}	
	}
	
	public enum Status {
		/**
		 * 
		 */		
	    QUEUED(1)
	    /**
		 * 
		 */
		,SUBMITTED(2)
	    /**
		 * 
		 */
		,FAILED_TO_SUBMIT(3)
	    /**
		 * 
		 */
		,DELIVERED(4)
	    /**
		 * 
		 */
		,FAILED(5)
	    /**
		 * 
		 */
		,UNROUTABLE(6)
	    /**
		 * 
		 */
		,NOT_VERIFIED(7)
	    /**
		 * 
		 */
		,INVALID(8)
	    /**
		 * 
		 */
		,WAITING_HLR(9);
				
		private static final Map<Integer, Status> TOKEN_STATUS_MAP = new HashMap<Integer, Status>(Status.values().length);
		static {
			for (Status g : Status.values()) {
				TOKEN_STATUS_MAP.put(g.getToken(), g);
			}
		}
		
		private int token;
		
		/**
		 * @param token
		 * @return
		 */
		public static Status getByToken(int token) {
			Status v = TOKEN_STATUS_MAP.get(token);
			if (v == null) {
				throw new IllegalArgumentException("No status with token: " + token);
			} else {
				return v;
			}
		}
		
		/**
		 * @param token
		 */
		private Status(int token) {
			this.token = token;
		}
		
		/**
		 * @return
		 */
		public int getToken() {
			return this.token;
		}	
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the sender
	 */
	public String getSender() {
		return sender;
	}

	/**
	 * @param sender the sender to set
	 */
	public void setSender(String sender) {
		this.sender = sender;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the providerId
	 */
	public int getProviderId() {
		return providerId;
	}

	/**
	 * @param providerId the providerId to set
	 */
	public void setProviderId(int providerId) {
		this.providerId = providerId;
	}

	/**
	 * @return the retries
	 */
	public int getRetries() {
		return retries;
	}

	/**
	 * @param retries the retries to set
	 */
	public void setRetries(int retries) {
		this.retries = retries;
	}

	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the timeScheduled
	 */
	public Date getTimeScheduled() {
		return timeScheduled;
	}

	/**
	 * @param timeScheduled the timeScheduled to set
	 */
	public void setTimeScheduled(Date timeScheduled) {
		this.timeScheduled = timeScheduled;
	}

	/**
	 * @return the timeSent
	 */
	public Date getTimeSent() {
		return timeSent;
	}

	/**
	 * @param timeSent the timeSent to set
	 */
	public void setTimeSent(Date timeSent) {
		this.timeSent = timeSent;
	}

	/**
	 * @return the referenceId
	 */
	public long getReferenceId() {
		return referenceId;
	}

	/**
	 * @param referenceId the referenceId to set
	 */
	public void setReferenceId(long referenceId) {
		this.referenceId = referenceId;
	}

	/**
	 * @return the response
	 */
	public String getResponse() {
		return response;
	}

	/**
	 * @param response the response to set
	 */
	public void setResponse(String response) {
		this.response = response;
	}

	/**
	 * @return the senderNumber
	 */
	public String getSenderNumber() {
		return senderNumber;
	}

	/**
	 * @param senderNumber the senderNumber to set
	 */
	public void setSenderNumber(String senderNumber) {
		this.senderNumber = senderNumber;
	}

	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(Type type) {
		this.type = type;
	}

	/**
	 * @return the status
	 */
	public Status getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Status status) {
		this.status = status;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SMS [id=" + id + ", sender=" + sender + ", phone=" + phone + ", message=" + message + ", providerId=" +
				providerId + ", retries=" + retries + ", timeCreated=" + timeCreated + ", timeScheduled=" +
				timeScheduled + ", timeSent=" + timeSent + ", referenceId=" + referenceId + ", response=" + response +
				", senderNumber=" + senderNumber + ", type=" + type + ", status=" + status + "]";
	}
	
}
