package capital.any.model.base;

import java.io.Serializable;
import java.util.Date;

import capital.any.service.base.product.FirstDateBeforeSecond;

@FirstDateBeforeSecond.List({
	@FirstDateBeforeSecond(first = "from", second = "to", message = "date-before-error", groups = SqlFilters.searchClientSpaceValidatorGroup.class)
})

public class SqlFilters implements Serializable{
	
	private static final long serialVersionUID = 987839675856979563L;
	
	public interface searchClientSpaceValidatorGroup{};
	
	private Long userId;
	private Long countryId;
	private String name;
	private String email;
	private Date from;
	private Date to;
	private Date subscriptionStartDate;
	private Integer transactionStatusId;
	private Date settledAtFrom;
	private Date settledAtTo;
	private Integer userClassId;
	private String firstName;
	private String lastName;
	private String mobilePhone;
	private Integer transactionPaymentTypeId;
	
	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * @return the countryId
	 */
	public Long getCountryId() {
		return countryId;
	}
	/**
	 * @param countryId the countryId to set
	 */
	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the from
	 */
	public Date getFrom() {
		return from;
	}
	/**
	 * @param from the from to set
	 */
	public void setFrom(Date from) {
		this.from = from;
	}
	/**
	 * @return the to
	 */
	public Date getTo() {
		return to;
	}
	/**
	 * @param to the to to set
	 */
	public void setTo(Date to) {
		this.to = to;
	}
	/**
	 * @return the subscriptionStartDate
	 */
	public Date getSubscriptionStartDate() {
		return subscriptionStartDate;
	}
	/**
	 * @param subscriptionStartDate the subscriptionStartDate to set
	 */
	public void setSubscriptionStartDate(Date subscriptionStartDate) {
		this.subscriptionStartDate = subscriptionStartDate;
	}	
	/**
	 * @return the transactionStatusId
	 */
	public Integer getTransactionStatusId() {
		return transactionStatusId;
	}
	/**
	 * @param transactionStatusId the transactionStatusId to set
	 */
	public void setTransactionStatusId(Integer transactionStatusId) {
		this.transactionStatusId = transactionStatusId;
	}
	/**
	 * @return the settledAtFrom
	 */
	public Date getSettledAtFrom() {
		return settledAtFrom;
	}
	/**
	 * @param settledAtFrom the settledAtFrom to set
	 */
	public void setSettledAtFrom(Date settledAtFrom) {
		this.settledAtFrom = settledAtFrom;
	}
	/**
	 * @return the settledAtTo
	 */
	public Date getSettledAtTo() {
		return settledAtTo;
	}
	/**
	 * @param settledAtTo the settledAtTo to set
	 */
	public void setSettledAtTo(Date settledAtTo) {
		this.settledAtTo = settledAtTo;
	}
	
	/**
	 * @return the userClassId
	 */
	public Integer getUserClassId() {
		return userClassId;
	}
	/**
	 * @param userClassId the userClassId to set
	 */
	public void setUserClassId(Integer userClassId) {
		this.userClassId = userClassId;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the mobilePhone
	 */
	public String getMobilePhone() {
		return mobilePhone;
	}
	/**
	 * @param mobilePhone the mobilePhone to set
	 */
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SqlFilters [userId=" + userId + ", countryId=" + countryId + ", name=" + name + ", email=" + email
				+ ", from=" + from + ", to=" + to + ", subscriptionStartDate=" + subscriptionStartDate
				+ ", transactionStatusId=" + transactionStatusId + ", settledAtFrom=" + settledAtFrom + ", settledAtTo="
				+ settledAtTo + ", userClassId=" + userClassId + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", mobilePhone=" + mobilePhone + ", transactionPaymentTypeId=" + transactionPaymentTypeId + "]";
	}
	/**
	 * @return the transactionPaymentTypeId
	 */
	public Integer getTransactionPaymentTypeId() {
		return transactionPaymentTypeId;
	}
	/**
	 * @param transactionPaymentTypeId the transactionPaymentTypeId to set
	 */
	public void setTransactionPaymentTypeId(Integer transactionPaymentTypeId) {
		this.transactionPaymentTypeId = transactionPaymentTypeId;
	}
	
}
