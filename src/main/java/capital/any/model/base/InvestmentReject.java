package capital.any.model.base;

import java.io.Serializable;
import java.util.Date;
import capital.any.base.enums.ActionSource;
import capital.any.base.enums.InvestmentRejectType;
import capital.any.base.enums.InvestmentTypeEnum;

/**
 * @author eyal.goren
 *
 */
public class InvestmentReject implements Serializable {

	private static final long serialVersionUID = 1L;
	private long id;
	private long userId;
	private long productId;
	private InvestmentRejectType InvestmentRejectType;
	private String sessionId;
	private double ask;
	private double bid;
	private long amount;
	private String rejectAdditionalInfo;
	private Date timeCreated;
	private ActionSource actionSource;
	private long writerId;
	private double rate;
	private InvestmentTypeEnum investmentType;
	private String serverName;
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the productId
	 */
	public long getProductId() {
		return productId;
	}
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(long productId) {
		this.productId = productId;
	}
	/**
	 * @return the investmentRejectType
	 */
	public InvestmentRejectType getInvestmentRejectType() {
		return InvestmentRejectType;
	}
	/**
	 * @param investmentRejectType the investmentRejectType to set
	 */
	public void setInvestmentRejectType(InvestmentRejectType investmentRejectType) {
		InvestmentRejectType = investmentRejectType;
	}
	/**
	 * @return the sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}
	/**
	 * @param sessionId the sessionId to set
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	/**
	 * @return the ask
	 */
	public double getAsk() {
		return ask;
	}
	/**
	 * @param ask the ask to set
	 */
	public void setAsk(double ask) {
		this.ask = ask;
	}
	/**
	 * @return the bid
	 */
	public double getBid() {
		return bid;
	}
	/**
	 * @param bid the bid to set
	 */
	public void setBid(double bid) {
		this.bid = bid;
	}
	/**
	 * @return the amount
	 */
	public long getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(long amount) {
		this.amount = amount;
	}
	/**
	 * @return the rejectAdditionalInfo
	 */
	public String getRejectAdditionalInfo() {
		return rejectAdditionalInfo;
	}
	/**
	 * @param rejectAdditionalInfo the rejectAdditionalInfo to set
	 */
	public void setRejectAdditionalInfo(String rejectAdditionalInfo) {
		this.rejectAdditionalInfo = rejectAdditionalInfo;
	}
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	/**
	 * @return the actionSource
	 */
	public ActionSource getActionSource() {
		return actionSource;
	}
	/**
	 * @param actionSource the actionSource to set
	 */
	public void setActionSource(ActionSource actionSource) {
		this.actionSource = actionSource;
	}
	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}
	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}
	/**
	 * @return the rate
	 */
	public double getRate() {
		return rate;
	}
	/**
	 * @param rate the rate to set
	 */
	public void setRate(double rate) {
		this.rate = rate;
	}
	/**
	 * @return the investmentType
	 */
	public InvestmentTypeEnum getInvestmentType() {
		return investmentType;
	}
	/**
	 * @param investmentType the investmentType to set
	 */
	public void setInvestmentType(InvestmentTypeEnum investmentType) {
		this.investmentType = investmentType;
	}

	/**
	 * @return the serverName
	 */
	public String getServerName() {
		return serverName;
	}
	/**
	 * @param serverName the serverName to set
	 */
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "InvestmentReject [id=" + id + ", userId=" + userId + ", productId=" + productId
				+ ", InvestmentRejectType=" + InvestmentRejectType + ", sessionId=" + sessionId + ", ask=" + ask
				+ ", bid=" + bid + ", amount=" + amount + ", rejectAdditionalInfo=" + rejectAdditionalInfo
				+ ", timeCreated=" + timeCreated + ", actionSource=" + actionSource + ", writerId=" + writerId
				+ ", rate=" + rate + ", investmentType=" + investmentType + ", serverName=" + serverName + "]";
	}
}