package capital.any.model.base;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author eran.levy
 *
 */
public class Device implements Serializable {

	private static final long serialVersionUID = -1190042947155018148L;
	
	private Type type;
	private OSType osType;	
	
	public enum Type {
		/**
		 * 
		 */
		DESKTOP(1)		
		/**
		 * 
		 */
		,MOBILE(2)
		/**
		 * 
		 */
		,TABLET(3);

		
		private static final Map<Integer, Type> TOKEN_TYPE_MAP = new HashMap<Integer, Type>(Type.values().length);
		static {
			for (Type g : Type.values()) {
				TOKEN_TYPE_MAP.put(g.getToken(), g);
			}
		}
		
		private int token;
		
		/**
		 * @param token
		 * @return
		 */
		public static Type getByToken(int token) {
			Type v = TOKEN_TYPE_MAP.get(token);
			if (v == null) {
				throw new IllegalArgumentException("No type with token: " + token);
			} else {
				return v;
			}
		}
		
		/**
		 * @param token
		 */
		private Type(int token) {
			this.token = token;
		}
		
		/**
		 * @return
		 */
		public int getToken() {
			return this.token;
		}	
	}
	
	public enum OSType {
		/**
		 * 
		 */
		ANDROID(1)		
		/**
		 * 
		 */
		,IOS(2);

		
		private static final Map<Integer, OSType> TOKEN_TYPE_MAP = new HashMap<Integer, OSType>(Type.values().length);
		static {
			for (OSType g : OSType.values()) {
				TOKEN_TYPE_MAP.put(g.getToken(), g);
			}
		}
		
		private int token;
		
		/**
		 * @param token
		 * @return
		 */
		public static OSType getByToken(int token) {
			OSType v = TOKEN_TYPE_MAP.get(token);
			if (v == null) {
				throw new IllegalArgumentException("No type with token: " + token);
			} else {
				return v;
			}
		}
		
		/**
		 * @param token
		 */
		private OSType(int token) {
			this.token = token;
		}
		
		/**
		 * @return
		 */
		public int getToken() {
			return this.token;
		}	
	}

	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(Type type) {
		this.type = type;
	}

	/**
	 * @return the osType
	 */
	public OSType getOsType() {
		return osType;
	}

	/**
	 * @param osType the osType to set
	 */
	public void setOsType(OSType osType) {
		this.osType = osType;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Device [type=" + type + ", osType=" + osType + "]";
	}
	
}
