package capital.any.model.base;

import java.io.Serializable;
import java.util.Date;

import capital.any.base.enums.ActionSource;

/**
 * @author Eyal G
 *
 */
public class File implements Serializable {
	
	private static final long serialVersionUID = -1130279432776127097L;
	public static String NAME_SEPARATOR = "_-_";
	private int id;
	private String name;
	private FileType fileType;
	private long userId;
	private Date timeCreated;
	private ActionSource actionSource;
	private AbstractWriter writer;
	private boolean isPrimary;
	private FileStatus fileStatus;
	
	public File(){		
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the fileType
	 */
	public FileType getFileType() {
		return fileType;
	}

	/**
	 * @param fileType the fileType to set
	 */
	public void setFileType(FileType fileType) {
		this.fileType = fileType;
	}
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the actionSource
	 */
	public ActionSource getActionSource() {
		return actionSource;
	}
	/**
	 * @param actionSource the actionSource to set
	 */
	public void setActionSource(ActionSource actionSource) {
		this.actionSource = actionSource;
	}
	/**
	 * @return the writer
	 */
	public AbstractWriter getWriter() {
		return writer;
	}
	/**
	 * @param writer the writer to set
	 */
	public void setWriter(AbstractWriter writer) {
		this.writer = writer;
	}

	/**
	 * @return the isPrimary
	 */
	public boolean isPrimary() {
		return isPrimary;
	}

	/**
	 * @param isPrimary the isPrimary to set
	 */
	public void setPrimary(boolean isPrimary) {
		this.isPrimary = isPrimary;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "File [id=" + id + ", name=" + name + ", fileType=" + fileType + ", userId=" + userId + ", timeCreated="
				+ timeCreated + ", actionSource=" + actionSource + ", writer=" + writer + ", isPrimary=" + isPrimary
				+ ", fileStatus=" + fileStatus + "]";
	}

	/**
	 * @return the fileStatus
	 */
	public FileStatus getFileStatus() {
		return fileStatus;
	}

	/**
	 * @param fileStatus the fileStatus to set
	 */
	public void setFileStatus(FileStatus fileStatus) {
		this.fileStatus = fileStatus;
	}
}
