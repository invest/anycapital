package capital.any.model.base;

import java.io.Serializable;
import java.util.List;
import capital.any.base.enums.ActionSource;

/**
 * @author Eyal Goren
 *
 */
public class SellNowRequest implements Serializable {

	private static final long serialVersionUID = -8637976736188253104L;

	private List<Long> investmentsId;
	private Product product;
	private User user;
	private ActionSource actionSource;
	private String ip;
	private long loginId;
	/**
	 * @return the investmentsId
	 */
	public List<Long> getInvestmentsId() {
		return investmentsId;
	}
	/**
	 * @param investmentsId the investmentsId to set
	 */
	public void setInvestmentsId(List<Long> investmentsId) {
		this.investmentsId = investmentsId;
	}
	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}
	/**
	 * @param product the product to set
	 */
	public void setProduct(Product product) {
		this.product = product;
	}
	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}
	/**
	 * @return the actionSource
	 */
	public ActionSource getActionSource() {
		return actionSource;
	}
	/**
	 * @param actionSource the actionSource to set
	 */
	public void setActionSource(ActionSource actionSource) {
		this.actionSource = actionSource;
	}
	
	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}
	/**
	 * @param ip the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}
	/**
	 * @return the loginId
	 */
	public long getLoginId() {
		return loginId;
	}
	/**
	 * @param loginId the loginId to set
	 */
	public void setLoginId(long loginId) {
		this.loginId = loginId;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SellNowRequest [investmentsId=" + investmentsId + ", product=" + product + ", user=" + user
				+ ", actionSource=" + actionSource + ", ip=" + ip + ", loginId=" + loginId + "]";
	}
}