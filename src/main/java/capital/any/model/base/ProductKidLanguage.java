package capital.any.model.base;

import java.io.Serializable;
import java.util.Date;

import capital.any.model.base.Language;
import capital.any.model.base.Product;

/**
 * 
 * @author Eyal.o
 *
 */
public class ProductKidLanguage implements Serializable {
	private static final long serialVersionUID = 6723696375297012472L;

	private long id;
	private Product product;
	private Language language;
	private Date timeCreated;
	private String url;
	
	/**
	 * ProductLanguage
	 */
	public ProductKidLanguage() {
		
	}
	
	/**
	 * ProductLanguage
	 * @param product
	 * @param language
	 */
	public ProductKidLanguage(Product product, Language language) {
		this.product = product;
		this.language = language;
	}
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the language
	 */
	public Language getLanguage() {
		return language;
	}
	/**
	 * @param language the language to set
	 */
	public void setLanguage(Language language) {
		this.language = language;
	}
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}
	/**
	 * @param product the product to set
	 */
	public void setProduct(Product product) {
		this.product = product;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ProductKidLanguage [id=" + id + ", product=" + product + ", language=" + language + ", timeCreated="
				+ timeCreated + ", url=" + url + "]";
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	
}
