package capital.any.model.base;

import java.io.Serializable;

/**
 * 
 * @author Eyal.o
 *
 */
public class FileRequiredDoc implements Serializable {
	private static final long serialVersionUID = -7819904950473233179L;

	private FileType fileType;
	private FileStatus fileStatus;
	private File file;
	
	/**
	 * FileRequiredDoc
	 */
	public FileRequiredDoc() {
		
	}
	
	/**
	 * FileRequiredDoc
	 * @param fileType
	 * @param fileStatus
	 */
	public FileRequiredDoc(FileType fileType, FileStatus fileStatus, File file) {
		super();
		this.fileType = fileType;
		this.fileStatus = fileStatus;
		this.file = file;
	}
	
	/**
	 * @return the fileType
	 */
	public FileType getFileType() {
		return fileType;
	}
	/**
	 * @param fileType the fileType to set
	 */
	public void setFileType(FileType fileType) {
		this.fileType = fileType;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FileRequiredDoc [fileType=" + fileType + ", fileStatus=" + fileStatus + ", file=" + file + "]";
	}

	/**
	 * @return the fileStatus
	 */
	public FileStatus getFileStatus() {
		return fileStatus;
	}

	/**
	 * @param fileStatus the fileStatus to set
	 */
	public void setFileStatus(FileStatus fileStatus) {
		this.fileStatus = fileStatus;
	}

	/**
	 * @return the file
	 */
	public File getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(File file) {
		this.file = file;
	}
}
