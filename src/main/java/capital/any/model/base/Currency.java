package capital.any.model.base;

import java.io.Serializable;

/**
 * @author eranl
 *
 */
public class Currency implements Serializable {
	
	private static final long serialVersionUID = -6596142437782140482L;
	private int id;
	private String code;
	private String displayName;
	private String symbol;
	private int marketId;
	
	public Currency() {
		
	}
	
	/**
	 * @param id
	 * @param code
	 * @param displayName
	 * @param symbol
	 * @param marketId
	 */
	public Currency(int id, String code, String displayName, String symbol, int marketId) {
		this.id = id;
		this.code = code;
		this.displayName = displayName;
		this.symbol = symbol;
		this.marketId = marketId;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * @return the symbol
	 */
	public String getSymbol() {
		return symbol;
	}

	/**
	 * @param symbol the symbol to set
	 */
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	/**
	 * @return the marketId
	 */
	public int getMarketId() {
		return marketId;
	}

	/**
	 * @param marketId the marketId to set
	 */
	public void setMarketId(int marketId) {
		this.marketId = marketId;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Currency [id=" + id + ", code=" + code + ", displayName=" + displayName + ", symbol=" + symbol
				+ ", marketId=" + marketId + "]";
	}
}
