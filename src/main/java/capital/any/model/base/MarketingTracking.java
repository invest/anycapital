package capital.any.model.base;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author eyal.ohana
 *
 */
public class MarketingTracking implements Serializable {
	private static final long serialVersionUID = 9105117210666013384L;
	
	private long id;
	private long campaignId;
	private String queryString;
	private Date timeCreated;
	private String abName;
	private String abValue;
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the campaignId
	 */
	public long getCampaignId() {
		return campaignId;
	}
	/**
	 * @param campaignId the campaignId to set
	 */
	public void setCampaignId(long campaignId) {
		this.campaignId = campaignId;
	}
	/**
	 * @return the queryString
	 */
	public String getQueryString() {
		return queryString;
	}
	/**
	 * @param queryString the queryString to set
	 */
	public void setQueryString(String queryString) {
		this.queryString = queryString;
	}
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MarketingTracking [id=" + id + ", campaignId=" + campaignId + ", queryString=" + queryString
				+ ", timeCreated=" + timeCreated + ", abName=" + abName + ", abValue=" + abValue + "]";
	}
	/**
	 * @return the abName
	 */
	public String getAbName() {
		return abName;
	}
	/**
	 * @param abName the abName to set
	 */
	public void setAbName(String abName) {
		this.abName = abName;
	}
	/**
	 * @return the abValue
	 */
	public String getAbValue() {
		return abValue;
	}
	/**
	 * @param abValue the abValue to set
	 */
	public void setAbValue(String abValue) {
		this.abValue = abValue;
	}
	
	
	
}
