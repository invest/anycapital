package capital.any.model.base;

import java.io.Serializable;
import java.util.Date;

import capital.any.base.enums.ActionSource;

/**
 * @author Eyal G
 *
 */
public class Email implements Serializable {

	private static final long serialVersionUID = -282245972051760881L;

	private int id;
	private int statusId;
	private String emailInfo;
	private Date timeCreated;
	private ActionSource actionSource;
	private int retries;
	private Date timeSent;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the statusId
	 */
	public int getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId the statusId to set
	 */
	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	/**
	 * @return the emailInfo
	 */
	public String getEmailInfo() {
		return emailInfo;
	}

	/**
	 * @param emailInfo the emailInfo to set
	 */
	public void setEmailInfo(String emailInfo) {
		this.emailInfo = emailInfo;
	}

	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the actionSource
	 */
	public ActionSource getActionSource() {
		return actionSource;
	}

	/**
	 * @param actionSource the actionSource to set
	 */
	public void setActionSource(ActionSource actionSource) {
		this.actionSource = actionSource;
	}

	/**
	 * @return the retries
	 */
	public int getRetries() {
		return retries;
	}

	/**
	 * @param retries the retries to set
	 */
	public void setRetries(int retries) {
		this.retries = retries;
	}

	/**
	 * @return the timeSent
	 */
	public Date getTimeSent() {
		return timeSent;
	}

	/**
	 * @param timeSent the timeSent to set
	 */
	public void setTimeSent(Date timeSent) {
		this.timeSent = timeSent;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Email [id=" + id + ", statusId=" + statusId + ", emailInfo=" + emailInfo + ", timeCreated="
				+ timeCreated + ", actionSource=" + actionSource + ", retries=" + retries + ", timeSent=" + timeSent
				+ "]";
	}
}
