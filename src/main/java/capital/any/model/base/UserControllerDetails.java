package capital.any.model.base;

import java.io.Serializable;

import capital.any.base.enums.ActionSource;

/**
 * @author LioR SoLoMoN
 *
 */
public class UserControllerDetails implements Serializable {
	private static final long serialVersionUID = 1905339740835974087L;
	
	private String userName;
	private ActionSource actionSource;
	private String ip;
	private long loginId;
	private Integer writerId;
	private MarketingTracking marketingTracking;
	private int utcOffset;
	
	public UserControllerDetails() {
		super();
	}

	/**
	 * 
	 * @param actionSource
	 */	
	public UserControllerDetails(ActionSource actionSource) {
		super();
		this.actionSource = actionSource;
	}

	/**
	 * 
	 * @param actionSource
	 * @param writerId
	 */
	public UserControllerDetails(ActionSource actionSource, Integer writerId) {
		this(actionSource);
		this.writerId = writerId;
	}

	/**
	 * @param userName
	 * @param actionSource
	 * @param ip
	 */
	public UserControllerDetails(String userName, ActionSource actionSource, String ip, MarketingTracking marketingTracking) {
		this.userName = userName;
		this.actionSource = actionSource;
		this.ip = ip;
		this.marketingTracking = marketingTracking;
	}
	
	/**
	 * 
	 * @param actionSource
	 * @param ip
	 * @param loginId
	 */
	public UserControllerDetails(ActionSource actionSource, String ip, long loginId) {
		this(null, actionSource, ip, null);
		this.loginId = loginId;
	}
	
	/**
	 * 
	 * @param actionSource
	 * @param ip
	 * @param loginId
	 * @param writerId
	 */
	public UserControllerDetails(ActionSource actionSource, String ip, long loginId, Integer writerId) {
		this(actionSource, ip, loginId);
		this.writerId = writerId;
	}

	/**
	 * @return the writerId
	 */
	public Integer getWriterId() {
		return writerId;
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(Integer writerId) {
		this.writerId = writerId;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the actionSource
	 */
	public ActionSource getActionSource() {
		return actionSource;
	}
	/**
	 * @param actionSource the actionSource to set
	 */
	public void setActionSource(ActionSource actionSource) {
		this.actionSource = actionSource;
	}
	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}
	/**
	 * @param ip the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

	/**
	 * @return the loginId
	 */
	public long getLoginId() {
		return loginId;
	}

	/**
	 * @param loginId the loginId to set
	 */
	public void setLoginId(long loginId) {
		this.loginId = loginId;
	}

	/**
	 * @return the marketingTracking
	 */
	public MarketingTracking getMarketingTracking() {
		return marketingTracking;
	}

	/**
	 * @param marketingTracking the marketingTracking to set
	 */
	public void setMarketingTracking(MarketingTracking marketingTracking) {
		this.marketingTracking = marketingTracking;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UserControllerDetails [userName=" + userName + ", actionSource=" + actionSource + ", ip=" + ip
				+ ", loginId=" + loginId + ", writerId=" + writerId + ", marketingTracking=" + marketingTracking
				+ ", utcOffset=" + utcOffset + "]";
	}

	/**
	 * @return the utcOffset
	 */
	public int getUtcOffset() {
		return utcOffset;
	}

	/**
	 * @param utcOffset the utcOffset to set
	 */
	public void setUtcOffset(int utcOffset) {
		this.utcOffset = utcOffset;
	}
}
