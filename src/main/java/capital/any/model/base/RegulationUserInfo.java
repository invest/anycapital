package capital.any.model.base;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author eyal.ohana
 *
 */
public class RegulationUserInfo implements Serializable {
	private static final long serialVersionUID = 1171753233091966053L;
	
	private long id;
	private long userId;
	private RegulationQuestionnaireStatus regulationQuestionnaireStatus;
	private CountryRisk countryRisk;
	private Date timeCreated;
	private Date timeModified;
	private AbstractWriter writer;
	
	/**
	 * RegulationUserInfo
	 */
	public RegulationUserInfo() {
		
	}
	
	/**
	 * RegulationUserInfo
	 * @param userId
	 */
	public RegulationUserInfo(long userId) {
		super();
		this.userId = userId;
	}
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	/**
	 * @return the timeModified
	 */
	public Date getTimeModified() {
		return timeModified;
	}
	/**
	 * @param timeModified the timeModified to set
	 */
	public void setTimeModified(Date timeModified) {
		this.timeModified = timeModified;
	}
	/**
	 * @return the regulationQuestionnaireStatus
	 */
	public RegulationQuestionnaireStatus getRegulationQuestionnaireStatus() {
		return regulationQuestionnaireStatus;
	}
	/**
	 * @param regulationQuestionnaireStatus the regulationQuestionnaireStatus to set
	 */
	public void setRegulationQuestionnaireStatus(RegulationQuestionnaireStatus regulationQuestionnaireStatus) {
		this.regulationQuestionnaireStatus = regulationQuestionnaireStatus;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RegulationUserInfo [id=" + id + ", userId=" + userId + ", regulationQuestionnaireStatus="
				+ regulationQuestionnaireStatus + ", countryRisk=" + countryRisk + ", timeCreated=" + timeCreated
				+ ", timeModified=" + timeModified + ", writer=" + writer + "]";
	}
	

	/**
	 * @return the writer
	 */
	public AbstractWriter getWriter() {
		return writer;
	}

	/**
	 * @param writer the writer to set
	 */
	public void setWriter(AbstractWriter writer) {
		this.writer = writer;
	}

	/**
	 * @return the countryRisk
	 */
	public CountryRisk getCountryRisk() {
		return countryRisk;
	}

	/**
	 * @param countryRisk the countryRisk to set
	 */
	public void setCountryRisk(CountryRisk countryRisk) {
		this.countryRisk = countryRisk;
	}
}
