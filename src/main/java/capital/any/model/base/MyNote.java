package capital.any.model.base;

import java.io.Serializable;

/**
 * class to represent investment in my notes screen
 * investment and coupon data
 * @author eyal.goren
 *
 */
public class MyNote implements Serializable{

	private static final long serialVersionUID = 3364300124061787857L;

	private Investment investment;
	private long couponAmonut;
	private long couponOriginalAmonut;
	
	public MyNote(Investment investment, long couponAmonut, long couponOriginalAmonut) {
		super();
		this.investment = investment;
		this.couponAmonut = couponAmonut;
		this.couponOriginalAmonut = couponOriginalAmonut;
	}

	/**
	 * @return the investment
	 */
	public Investment getInvestment() {
		return investment;
	}

	/**
	 * @param investment the investment to set
	 */
	public void setInvestment(Investment investment) {
		this.investment = investment;
	}

	/**
	 * @return the couponAmonut
	 */
	public long getCouponAmonut() {
		return couponAmonut;
	}

	/**
	 * @param couponAmonut the couponAmonut to set
	 */
	public void setCouponAmonut(long couponAmonut) {
		this.couponAmonut = couponAmonut;
	}

	/**
	 * @return the couponOriginalAmonut
	 */
	public long getCouponOriginalAmonut() {
		return couponOriginalAmonut;
	}

	/**
	 * @param couponOriginalAmonut the couponOriginalAmonut in product currency to set
	 */
	public void setCouponOriginalAmonut(long couponOriginalAmonut) {
		this.couponOriginalAmonut = couponOriginalAmonut;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MyNotes [investment=" + investment + ", couponAmonut=" + couponAmonut + ", couponOriginalAmonut="
				+ couponOriginalAmonut + "]";
	}
}
