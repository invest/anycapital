package capital.any.model.base;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author Eyal.o
 *
 */
public class FileRequiredDocGroup implements Serializable {
	private static final long serialVersionUID = -4405292074760922227L;
	
	private FileGroup fileGroup;
	private Map<Integer, FileRequiredDoc> mapFileRequiredDocs;
	/**
	 * @return the fileGroup
	 */
	public FileGroup getFileGroup() {
		return fileGroup;
	}
	/**
	 * @param fileGroup the fileGroup to set
	 */
	public void setFileGroup(FileGroup fileGroup) {
		this.fileGroup = fileGroup;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FileRequiredDocGroup [fileGroup=" + fileGroup + ", mapFileRequiredDocs=" + mapFileRequiredDocs + "]";
	}
	/**
	 * @return the mapFileRequiredDocs
	 */
	public Map<Integer, FileRequiredDoc> getMapFileRequiredDocs() {
		return mapFileRequiredDocs;
	}
	/**
	 * @param mapFileRequiredDocs the mapFileRequiredDocs to set
	 */
	public void setMapFileRequiredDocs(Map<Integer, FileRequiredDoc> mapFileRequiredDocs) {
		this.mapFileRequiredDocs = mapFileRequiredDocs;
	}
	
}
