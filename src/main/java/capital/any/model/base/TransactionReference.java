package capital.any.model.base;

import java.io.Serializable;

/**
 * @author Eyal Goren
 *
 */
public class TransactionReference implements Serializable {	
	private static final long serialVersionUID = -4819369093376282830L;
	private long transactionId;
	private long referenceId;
	private int tableId;
	
	/**
	 * @return the transactionId
	 */
	public long getTransactionId() {
		return transactionId;
	}
	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
	/**
	 * @return the referenceId
	 */
	public long getReferenceId() {
		return referenceId;
	}
	/**
	 * @param referenceId the referenceId to set
	 */
	public void setReferenceId(long referenceId) {
		this.referenceId = referenceId;
	}
	
	/**
	 * @return the tableId
	 */
	public int getTableId() {
		return tableId;
	}
	/**
	 * @param tableId the tableId to set
	 */
	public void setTableId(int tableId) {
		this.tableId = tableId;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TransactionReference [transactionId=" + transactionId + ", referenceId=" + referenceId + ", tableId="
				+ tableId + "]";
	}	
}
