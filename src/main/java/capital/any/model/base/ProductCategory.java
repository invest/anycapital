package capital.any.model.base;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author eranl
 *
 */
public class ProductCategory implements Serializable {
	
	private static final long serialVersionUID = 6936096172535845747L;
	private int id;
	private String name;
	private String displayName;
	private boolean isCapitalProtection;
	private String riskFactorsKey;
	
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	@JsonIgnore
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}
	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	/**
	 * @return the isCapitalProtection
	 */
	public boolean isCapitalProtection() {
		return isCapitalProtection;
	}
	/**
	 * @param isCapitalProtection the isCapitalProtection to set
	 */
	public void setCapitalProtection(boolean isCapitalProtection) {
		this.isCapitalProtection = isCapitalProtection;
	}

	/**
	 * @return the riskFactorsKey
	 */
	public String getRiskFactorsKey() {
		return riskFactorsKey;
	}
	/**
	 * @param riskFactorsKey the riskFactorsKey to set
	 */
	public void setRiskFactorsKey(String riskFactorsKey) {
		this.riskFactorsKey = riskFactorsKey;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ProductCategory [id=" + id + ", name=" + name + ", displayName=" + displayName
				+ ", isCapitalProtection=" + isCapitalProtection + ", riskFactorsKey=" + riskFactorsKey + "]";
	}
	
}
