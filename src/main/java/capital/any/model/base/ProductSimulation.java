package capital.any.model.base;

import java.io.Serializable;

/**
 * @author eranl
 *
 */
public class ProductSimulation implements Serializable {
	
	private static final long serialVersionUID = 8039571442568631479L;
	
	private int id;
	private double finalFixingLevel;
	private long cashSettlement;
	private long cashSettlementBarrier;
	private int position;
	private long productId;
	
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the finalFixingLevel
	 */
	public double getFinalFixingLevel() {
		return finalFixingLevel;
	}
	/**
	 * @param finalFixingLevel the finalFixingLevel to set
	 */
	public void setFinalFixingLevel(double finalFixingLevel) {
		this.finalFixingLevel = finalFixingLevel;
	}
	/**
	 * @return the cashSettlement
	 */
	public long getCashSettlement() {
		return cashSettlement;
	}
	/**
	 * @param cashSettlement the cashSettlement to set
	 */
	public void setCashSettlement(long cashSettlement) {
		this.cashSettlement = cashSettlement;
	}
	/**
	 * @return the position
	 */
	public int getPosition() {
		return position;
	}
	/**
	 * @param position the position to set
	 */
	public void setPosition(int position) {
		this.position = position;
	}
	/**
	 * @return the productId
	 */
	public long getProductId() {
		return productId;
	}
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(long productId) {
		this.productId = productId;
	}
	
	/**
	 * @return the cashSettlementBarrier
	 */
	public long getCashSettlementBarrier() {
		return cashSettlementBarrier;
	}
	/**
	 * @param cashSettlementBarrier the cashSettlementBarrier to set
	 */
	public void setCashSettlementBarrier(long cashSettlementBarrier) {
		this.cashSettlementBarrier = cashSettlementBarrier;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ProductSimulation [id=" + id + ", finalFixingLevel=" + finalFixingLevel + ", cashSettlement="
				+ cashSettlement + ", cashSettlementBarrier=" + cashSettlementBarrier + ", position=" + position
				+ ", productId=" + productId + "]";
	}
			
}
