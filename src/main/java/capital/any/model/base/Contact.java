package capital.any.model.base;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.validation.constraints.NotNull;
import capital.any.base.enums.ActionSource;

/**
 * @author eran.levy
 *
 */
public class Contact implements Serializable { 
	
	// empty interface for validation
	public interface InsertContactUsValidatorGroup{};
	public interface insertContactEmailFormValidatorGroup{};
	public interface insertContactShortFormValidatorGroup{};

	private static final long serialVersionUID = 5181022625928377071L;

	private long id;
	@NotNull(groups = {InsertContactUsValidatorGroup.class, insertContactShortFormValidatorGroup.class}, message ="error.empty")
	private String firstName;
	@NotNull(groups = {InsertContactUsValidatorGroup.class, insertContactShortFormValidatorGroup.class}, message ="error.empty")
	private String lastName;
	private Type type;
	@NotNull(groups = {InsertContactUsValidatorGroup.class, insertContactShortFormValidatorGroup.class},  message ="error.empty")
	private String mobilePhone;
	@NotNull(groups = {InsertContactUsValidatorGroup.class, insertContactShortFormValidatorGroup.class, insertContactEmailFormValidatorGroup.class}, message ="error.empty")
	private String email;
	@NotNull(groups = InsertContactUsValidatorGroup.class, message ="error.empty")
	private int countryId;
	@NotNull(groups = {InsertContactUsValidatorGroup.class, insertContactShortFormValidatorGroup.class, insertContactEmailFormValidatorGroup.class}, message ="error.empty")
	private int languageId;
	private Date timeCreated;
	private Date timeModified;	
	private int writerId;	
	private int utcOffset;
	private String ip;	
	private String userAgent;
	private int isContactByEmail;
	private int isContactBySms;			
	private Clazz clazz;
	private ActionSource actionSource;
	private String httpReferer;
	@NotNull(groups = InsertContactUsValidatorGroup.class, message ="error.empty")
	private Issue issue; 
	@NotNull(groups = InsertContactUsValidatorGroup.class, message ="error.empty")
	private String comments;
	@NotNull(groups = {insertContactShortFormValidatorGroup.class, insertContactEmailFormValidatorGroup.class}, message ="error.empty")
	private long campaignId;
	private MarketingTracking marketingTracking;
	
	
	public enum Type {
		/**
		 * 
		 */
		CALL_ME(1)		
		/**
		 * 
		 */
		,CONTACT_US(2)
		/**
		 * 
		 */
		,EMAIL_FORM(3)
		/**
		 * 
		 */
		,SHORT_FORM(4)
		/**
		 * 
		 */
		,IMPORTED(5);

		
		private static final Map<Integer, Type> TOKEN_TYPE_MAP = new HashMap<Integer, Type>(Type.values().length);
		static {
			for (Type g : Type.values()) {
				TOKEN_TYPE_MAP.put(g.getToken(), g);
			}
		}
		
		private int token;
		
		/**
		 * @param token
		 * @return
		 */
		public static Type getByToken(int token) {
			Type v = TOKEN_TYPE_MAP.get(token);
			if (v == null) {
				throw new IllegalArgumentException("No type with token: " + token);
			} else {
				return v;
			}
		}
		
		/**
		 * @param token
		 */
		private Type(int token) {
			this.token = token;
		}
		
		/**
		 * @return
		 */
		public int getToken() {
			return this.token;
		}	
	}
	
	public enum Issue {			
		/**
		 * 
		 */
		GENERAL(1)		
		/**
		 * 
		 */
		,INVESTMENTS(2)		
		/**
		 * 
		 */
		,TECHNICAL_ISSUES(3)
		/**
		 * 
		 */
		,REGISTRATION(4)
		/**
		 * 
		 */
		,DEPOSITS_WITHDRAWALS(5);

		
		private static final Map<Integer, Issue> TOKEN_ISSUE_MAP = new HashMap<Integer, Issue>(Issue.values().length);
		static {
			for (Issue g : Issue.values()) {
				TOKEN_ISSUE_MAP.put(g.getToken(), g);
			}
		}
		
		private int token;
		
		/**
		 * @param token
		 * @return
		 */
		public static Issue getByToken(int token) {
			Issue v = TOKEN_ISSUE_MAP.get(token);
			if (v == null) {
				throw new IllegalArgumentException("No Issue with token: " + token);
			} else {
				return v;
			}
		}
		
		/**
		 * @param token
		 */
		private Issue(int token) {
			this.token = token;
		}
		
		/**
		 * @return
		 */
		public int getToken() {
			return this.token;
		}		
	}
	
	
	/**
	 * 
	 *
	 */
	public enum Clazz {
		TESTER(0), 		
		REAL(1);
		
		private static final Map<Integer, Clazz> TOKEN_CLAZZ_MAP = new HashMap<Integer, Clazz>(Clazz.values().length);
		static {
			for (Clazz g : Clazz.values()) {
				TOKEN_CLAZZ_MAP.put(g.getToken(), g);
			}
		}
		
		private int token;
		
		/**
		 * @param token
		 * @return
		 */
		public static Clazz get(int token) {
			Clazz c = TOKEN_CLAZZ_MAP.get(token);
			if (c == null) {
				throw new IllegalArgumentException("No Clazz with token: " + token);
			} else {
				return c;
			}
		}
		
		/**
		 * @return
		 */
		public static Map<Integer, Clazz> get() {
			return TOKEN_CLAZZ_MAP;
		}
		
		/**
		 * @param token
		 */
		private Clazz(int token) {
			this.token = token;
		}
		
		/**
		 * @return
		 */
		public int getToken() {
			return this.token;
		}		
	}


	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}


	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}


	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}


	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}


	/**
	 * @param type the type to set
	 */
	public void setType(Type type) {
		this.type = type;
	}


	/**
	 * @return the mobilePhone
	 */
	public String getMobilePhone() {
		return mobilePhone;
	}


	/**
	 * @param mobilePhone the mobilePhone to set
	 */
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}


	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}


	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}


	/**
	 * @return the countryId
	 */
	public int getCountryId() {
		return countryId;
	}


	/**
	 * @param countryId the countryId to set
	 */
	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}


	/**
	 * @return the languageId
	 */
	public int getLanguageId() {
		return languageId;
	}


	/**
	 * @param languageId the languageId to set
	 */
	public void setLanguageId(int languageId) {
		this.languageId = languageId;
	}


	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}


	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}


	/**
	 * @return the timeModified
	 */
	public Date getTimeModified() {
		return timeModified;
	}


	/**
	 * @param timeModified the timeModified to set
	 */
	public void setTimeModified(Date timeModified) {
		this.timeModified = timeModified;
	}


	/**
	 * @return the writerId
	 */
	public int getWriterId() {
		return writerId;
	}


	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(int writerId) {
		this.writerId = writerId;
	}

	/**
	 * @return the utcOffset
	 */
	public int getUtcOffset() {
		return utcOffset;
	}


	/**
	 * @param utcOffset the utcOffset to set
	 */
	public void setUtcOffset(int utcOffset) {
		this.utcOffset = utcOffset;
	}

	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}


	/**
	 * @param ip the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}


	/**
	 * @return the userAgent
	 */
	public String getUserAgent() {
		return userAgent;
	}


	/**
	 * @param userAgent the userAgent to set
	 */
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}


	/**
	 * @return the isContactByEmail
	 */
	public int getIsContactByEmail() {
		return isContactByEmail;
	}


	/**
	 * @param isContactByEmail the isContactByEmail to set
	 */
	public void setIsContactByEmail(int isContactByEmail) {
		this.isContactByEmail = isContactByEmail;
	}


	/**
	 * @return the isContactBySms
	 */
	public int getIsContactBySms() {
		return isContactBySms;
	}


	/**
	 * @param isContactBySms the isContactBySms to set
	 */
	public void setIsContactBySms(int isContactBySms) {
		this.isContactBySms = isContactBySms;
	}


	/**
	 * @return the clazz
	 */
	public Clazz getClazz() {
		return clazz;
	}


	/**
	 * @param clazz the clazz to set
	 */
	public void setClazz(Clazz clazz) {
		this.clazz = clazz;
	}


	/**
	 * @return the actionSource
	 */
	public ActionSource getActionSource() {
		return actionSource;
	}


	/**
	 * @param actionSource the actionSource to set
	 */
	public void setActionSource(ActionSource actionSource) {
		this.actionSource = actionSource;
	}


	/**
	 * @return the httpReferer
	 */
	public String getHttpReferer() {
		return httpReferer;
	}


	/**
	 * @param httpReferer the httpReferer to set
	 */
	public void setHttpReferer(String httpReferer) {
		this.httpReferer = httpReferer;
	}


	/**
	 * @return the issue
	 */
	public Issue getIssue() {
		return issue;
	}


	/**
	 * @param issue the issue to set
	 */
	public void setIssue(Issue issue) {
		this.issue = issue;
	}


	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}


	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Contact [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", type=" + type
				+ ", mobilePhone=" + mobilePhone + ", email=" + email + ", countryId=" + countryId + ", languageId="
				+ languageId + ", timeCreated=" + timeCreated + ", timeModified=" + timeModified + ", writerId="
				+ writerId + ", utcOffset=" + utcOffset + ", ip=" + ip + ", userAgent=" + userAgent
				+ ", isContactByEmail=" + isContactByEmail + ", isContactBySms=" + isContactBySms + ", clazz=" + clazz
				+ ", actionSource=" + actionSource + ", httpReferer=" + httpReferer + ", issue=" + issue + ", comments="
				+ comments + ", campaignId=" + campaignId + ", marketingTracking=" + marketingTracking + "]";
	}


	/**
	 * @return the campaignId
	 */
	public long getCampaignId() {
		return campaignId;
	}


	/**
	 * @param campaignId the campaignId to set
	 */
	public void setCampaignId(long campaignId) {
		this.campaignId = campaignId;
	}


	/**
	 * @return the marketingTracking
	 */
	public MarketingTracking getMarketingTracking() {
		return marketingTracking;
	}


	/**
	 * @param marketingTracking the marketingTracking to set
	 */
	public void setMarketingTracking(MarketingTracking marketingTracking) {
		this.marketingTracking = marketingTracking;
	}

}
