package capital.any.model.base;

import java.io.Serializable;

/**
 * 
 * @author eyal.ohana
 *
 */
public class RegulationFileRule implements Serializable {
	private static final long serialVersionUID = -4472882366786161063L;
	
	private int id;
	private int countryRiskId;
	private int gbgStatusId;
	private int fileTypeId;
	private FileType fileType;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the countryRiskId
	 */
	public int getCountryRiskId() {
		return countryRiskId;
	}
	/**
	 * @param countryRiskId the countryRiskId to set
	 */
	public void setCountryRiskId(int countryRiskId) {
		this.countryRiskId = countryRiskId;
	}
	/**
	 * @return the gbgStatusId
	 */
	public int getGbgStatusId() {
		return gbgStatusId;
	}
	/**
	 * @param gbgStatusId the gbgStatusId to set
	 */
	public void setGbgStatusId(int gbgStatusId) {
		this.gbgStatusId = gbgStatusId;
	}
	/**
	 * @return the fileTypeId
	 */
	public int getFileTypeId() {
		return fileTypeId;
	}
	/**
	 * @param fileTypeId the fileTypeId to set
	 */
	public void setFileTypeId(int fileTypeId) {
		this.fileTypeId = fileTypeId;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RegulationFileRule [id=" + id + ", countryRiskId=" + countryRiskId + ", gbgStatusId=" + gbgStatusId
				+ ", fileTypeId=" + fileTypeId + ", fileType=" + fileType + "]";
	}
	/**
	 * @return the fileType
	 */
	public FileType getFileType() {
		return fileType;
	}
	/**
	 * @param fileType the fileType to set
	 */
	public void setFileType(FileType fileType) {
		this.fileType = fileType;
	}


}
