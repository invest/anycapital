package capital.any.model.base;

import java.io.Serializable;

/**
 * @author LioR SoLoMoN
 * @param <T>
 *
 */
public class PaymentRequest<T> implements Serializable {
	private static final long serialVersionUID = -8863805308031880688L;
	private T details;
	private TransactionPaymentType paymentType; //TODO inherited?
	private long amount; //TODO inherited?
	private String comments;
	
	
	/**
	 * @return the paymentType
	 */
	public TransactionPaymentType getPaymentType() {
		return paymentType;
	}
	/**
	 * @param TransactionPaymentType the paymentType to set
	 */
	public void setPaymentType(TransactionPaymentType paymentType) {
		this.paymentType = paymentType;
	}
	/**
	 * @return the amount
	 */
	public long getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(long amount) {
		this.amount = amount;
	}
	/**
	 * @return the details
	 */
	public T getDetails() {
		return details;
	}
	/**
	 * @param details the details to set
	 */
	public void setDetails(T details) {
		this.details = details;
	}
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
}
