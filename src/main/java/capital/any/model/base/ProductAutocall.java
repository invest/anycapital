package capital.any.model.base;

import java.io.Serializable;
import java.util.Date;

/**
 * @author eranl
 *
 */
public class ProductAutocall implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3168423591796850312L;
	
	private int id;
	private long productId;
	private Date examinationDate;
	private double examinationValue;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	public long getProductId() {
		return productId;
	}
	public void setProductId(long productId) {
		this.productId = productId;
	}
	public Date getExaminationDate() {
		return examinationDate;
	}
	public void setExaminationDate(Date examinationDate) {
		this.examinationDate = examinationDate;
	}
	/**
	 * @return the examinationValue
	 */
	public double getExaminationValue() {
		return examinationValue;
	}
	/**
	 * @param examinationValue the examinationValue to set
	 */
	public void setExaminationValue(double examinationValue) {
		this.examinationValue = examinationValue;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ProductAutocall [id=" + id + ", productId=" + productId + ", examinationDate=" + examinationDate
				+ ", examinationValue=" + examinationValue + "]";
	}	
}
