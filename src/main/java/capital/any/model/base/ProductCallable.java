package capital.any.model.base;

import java.io.Serializable;
import java.util.Date;

/**
 * @author eranl
 *
 */
public class ProductCallable implements Serializable {
	
	private static final long serialVersionUID = 2768747662592496405L;
	
	private int id;
	private long productId;
	private Date examinationDate;
	
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the productId
	 */
	public long getProductId() {
		return productId;
	}
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(long productId) {
		this.productId = productId;
	}
	/**
	 * @return the examinationDate
	 */
	public Date getExaminationDate() {
		return examinationDate;
	}
	/**
	 * @param examinationDate the examinationDate to set
	 */
	public void setExaminationDate(Date examinationDate) {
		this.examinationDate = examinationDate;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ProductCallable [productId=" + productId + ", examinationDate=" + examinationDate + "]";
	}
	
}
