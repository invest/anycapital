package capital.any.model.base;

import java.util.Date;

/**
 * @author LioR SoLoMoN
 *
 */
public class GBGUser {
	private long userId;
	private int score;
	private String request;
	private String response;
	private Date timeCreated;
	private Date timeUpdated;

	/**
	 * 
	 */
	public GBGUser() {
		
	}

	/**
	 * @param userId
	 * @param score
	 * @param request
	 * @param response
	 * @param timeCreated
	 * @param timeUpdated
	 */
	public GBGUser(long userId, int score, String request, String response, Date timeCreated,
			Date timeUpdated) {
		super();
		this.userId = userId;
		this.score = score;
		this.request = request;
		this.response = response;
		this.timeCreated = timeCreated;
		this.timeUpdated = timeUpdated;
	}
	
	/**
	 * @param score
	 * @param response
	 * @param timeUpdated
	 */
	public GBGUser(int score, String response, Date timeUpdated) {
		super();
		this.score = score;
		this.response = response;
		this.timeUpdated = timeUpdated;
	}
	
	@Override
	public String toString() {
		return "GBGUser [userId=" + userId + ", score=" + score + ", request=" + request + ", response="
				+ response + ", timeCreated=" + timeCreated + ", time_updated=" + timeUpdated + "]";
	}
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the score
	 */
	public int getScore() {
		return score;
	}
	/**
	 * @param score the score to set
	 */
	public void setScore(int score) {
		this.score = score;
	}
	/**
	 * @return the request
	 */
	public String getRequest() {
		return request;
	}
	/**
	 * @param request the request to set
	 */
	public void setRequest(String request) {
		this.request = request;
	}
	/**
	 * @return the response
	 */
	public String getResponse() {
		return response;
	}
	/**
	 * @param response the response to set
	 */
	public void setResponse(String response) {
		this.response = response;
	}
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	/**
	 * @return the timeUpdated
	 */
	public Date getTimeUpdated() {
		return timeUpdated;
	}

	/**
	 * @param timeUpdated the timeUpdated to set
	 */
	public void setTimeUpdated(Date timeUpdated) {
		this.timeUpdated = timeUpdated;
	}
}
