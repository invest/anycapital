package capital.any.model.base;

import java.io.Serializable;
import java.util.Date;

/**
 * @author eranl
 *
 */
public class Login implements Serializable {

	private static final long serialVersionUID = 3871499399734361438L;
	
	private long id;
	private long userId;
	private int actionSourceId;
	private Date timeCreated;
	private String userAgent;
	private MarketingTracking marketingTracking;
	private String ip;
	
	public Login() {
		super();
	}
	
	public Login(long id) {
		super();
		this.id = id;
	}
	
	public Login(long userId, int actionSourceId, String userAgent,
			MarketingTracking marketingTracking, String ip) {
		super();
		this.userId = userId;
		this.actionSourceId = actionSourceId;
		this.userAgent = userAgent;
		this.marketingTracking = marketingTracking;
		this.ip = ip;
	}
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the actionSourceId
	 */
	public int getActionSourceId() {
		return actionSourceId;
	}
	/**
	 * @param actionSourceId the actionSourceId to set
	 */
	public void setActionSourceId(int actionSourceId) {
		this.actionSourceId = actionSourceId;
	}
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	
	/**
	 * @return the userAgent
	 */
	public String getUserAgent() {
		return userAgent;
	}
	/**
	 * @param userAgent the userAgent to set
	 */
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	/**
	 * @return the marketingTracking
	 */
	public MarketingTracking getMarketingTracking() {
		return marketingTracking;
	}

	/**
	 * @param marketingTracking the marketingTracking to set
	 */
	public void setMarketingTracking(MarketingTracking marketingTracking) {
		this.marketingTracking = marketingTracking;
	}

	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * @param ip the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Login [id=" + id + ", userId=" + userId + ", actionSourceId=" + actionSourceId + ", timeCreated="
				+ timeCreated + ", userAgent=" + userAgent + ", marketingTracking=" + marketingTracking + ", ip=" + ip
				+ "]";
	}
	
}
