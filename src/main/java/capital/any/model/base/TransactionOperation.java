package capital.any.model.base;

import java.io.Serializable;

/**
 * @author LioR SoLoMoN
 *
 */
public class TransactionOperation implements Serializable {

	private static final long serialVersionUID = -4357009411437535822L;
	private int id;
	private String name;
	private String displayName;

	/**
	 * 
	 */
	public TransactionOperation() {
	
	}
	
	public TransactionOperation(int id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return "TransactionOperation [id=" + id + ", name=" + name + ", displayName=" + displayName + "]";
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}
	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
}
