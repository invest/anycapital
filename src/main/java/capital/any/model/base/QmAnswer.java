package capital.any.model.base;

import java.io.Serializable;

/**
 * 
 * @author eyal.ohana
 *
 */
public class QmAnswer implements Serializable {
	private static final long serialVersionUID = -6017377807068422366L;
	
	private int id;
	private String name;
	private String displayName;
	private int questionId;
	private int orderId;
	private int selectedAnswer;
	private String textAnswer;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the questionId
	 */
	public int getQuestionId() {
		return questionId;
	}
	/**
	 * @param questionId the questionId to set
	 */
	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}
	/**
	 * @return the orderId
	 */
	public int getOrderId() {
		return orderId;
	}
	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "QmAnswer [id=" + id + ", name=" + name + ", displayName=" + displayName + ", questionId=" + questionId
				+ ", orderId=" + orderId + ", selectedAnswer=" + selectedAnswer + ", textAnswer=" + textAnswer + "]";
	}
	/**
	 * @return the selectedAnswer
	 */
	public int getSelectedAnswer() {
		return selectedAnswer;
	}
	/**
	 * @param selectedAnswer the selectedAnswer to set
	 */
	public void setSelectedAnswer(int selectedAnswer) {
		this.selectedAnswer = selectedAnswer;
	}
	/**
	 * @return the textAnswer
	 */
	public String getTextAnswer() {
		return textAnswer;
	}
	/**
	 * @param textAnswer the textAnswer to set
	 */
	public void setTextAnswer(String textAnswer) {
		this.textAnswer = textAnswer;
	}
	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}
	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
}
