package capital.any.model.base;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import capital.any.base.enums.ProductBarrierTypeEnum;
import capital.any.base.enums.ProductStatusEnum;
import capital.any.base.enums.ProductTypeEnum;
import capital.any.product.type.formula.Parameters;
import capital.any.service.base.product.FirstDateBeforeOrEqualsToSecond;
import capital.any.service.base.product.FirstDateBeforeSecond;
import capital.any.service.base.product.ProductTypeBarrier;

/**
 * @author eranl
 *
 */

@FirstDateBeforeSecond.List({
	@FirstDateBeforeSecond(first = "subscriptionStartDate", second = "subscriptionEndDate", message = "subscription-start-error", groups = Product.insertOrUpdateProductValidatorGroup.class),
    @FirstDateBeforeSecond(first = "firstExchangeTradingDate", second = "finalFixingDate", message = "first-exchange-error", groups = Product.insertOrUpdateProductValidatorGroup.class),
	@FirstDateBeforeSecond(first = "lastTradingDate", second = "redemptionDate", message = "last-trading-error", groups = Product.insertOrUpdateProductValidatorGroup.class)
})


@FirstDateBeforeOrEqualsToSecond.List({
	@FirstDateBeforeOrEqualsToSecond(first = "subscriptionEndDate", second = "initialFixingDate", message = "subscription-end-error", groups = Product.insertOrUpdateProductValidatorGroup.class),
	@FirstDateBeforeOrEqualsToSecond(first = "initialFixingDate", second = "issueDate", message = "initial-fixing-error", groups = Product.insertOrUpdateProductValidatorGroup.class),
	@FirstDateBeforeOrEqualsToSecond(first = "issueDate", second = "firstExchangeTradingDate", message = "issue-date-error", groups = Product.insertOrUpdateProductValidatorGroup.class),
	@FirstDateBeforeOrEqualsToSecond(first = "finalFixingDate", second = "lastTradingDate", message = "final-fixing-error", groups = Product.insertOrUpdateProductValidatorGroup.class)
})

@ProductTypeBarrier.List({
	@ProductTypeBarrier(first = "productType", second = "productBarrier", message = "barrier-invalid-error", groups = Product.insertOrUpdateProductValidatorGroup.class),
})

public class Product implements Serializable {
	
	public interface updateAskBidValidatorGroup{};
	public interface insertOrUpdateProductValidatorGroup{};
	
	
	private static final Logger logger = LoggerFactory.getLogger(Product.class);
	private static final long serialVersionUID = 8719690677515289462L;
	
	private static final BigDecimal simulationAmonut = new BigDecimal("1000000");
	
	private long id;
	private Date timeCreated;
	private ProductStatus productStatus;
	private ProductType productType;
	private double issuePrice;
	private int currencyId;
	private long denomination;
	private ProductCouponFrequency productCouponFrequency;
	private DayCountConvention dayCountConvention;
	private double maxYield;
	private double maxYieldPA;
	private double levelOfProtection;
	private double levelOfParticipation;
	private double strikeLevel;
	private double bonusLevel;
	@NotNull(groups = insertOrUpdateProductValidatorGroup.class)
	private Date subscriptionStartDate;
	@NotNull(groups = insertOrUpdateProductValidatorGroup.class)
	private Date subscriptionEndDate;
	@NotNull(groups = insertOrUpdateProductValidatorGroup.class)
	private Date initialFixingDate;
	@NotNull(groups = insertOrUpdateProductValidatorGroup.class)
	private Date issueDate;
	@NotNull(groups = insertOrUpdateProductValidatorGroup.class)
	private Date firstExchangeTradingDate;
	@NotNull(groups = insertOrUpdateProductValidatorGroup.class)
	private Date lastTradingDate;
	@NotNull(groups = insertOrUpdateProductValidatorGroup.class)
	private Date finalFixingDate;
	@NotNull(groups = insertOrUpdateProductValidatorGroup.class)
	private Date redemptionDate;
	private boolean isSuspend;
	private boolean isQuanto;
	private int productInsuranceTypeId;
	@Min(groups = updateAskBidValidatorGroup.class , message="" , value=0)
	private double bid;
	@Min(groups = updateAskBidValidatorGroup.class , message="" , value=0)
	private double ask;
	private BigDecimal formulaResult;
	private double bondFloorAtissuance;
	private long issueSize;
	private int priority;
	private double capLevel;
	private ProductMaturity productMaturity;
	private String kidName;
	@Valid
	private ProductBarrier productBarrier;
	private List<ProductMarket> productMarkets;
	private List<ProductScenario> productScenarios;
	private List<ProductCoupon> productCoupons;
	private List<ProductAutocall> productAutocalls;
	private List<ProductCallable> productCallables;
	private List<ProductSimulation> productSimulation;
	private List<ProductSlider> productSliders;
	private List<ProductKidLanguage> ProductKidLanguages; 
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Date getTimeCreated() {
		return timeCreated;
	}
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	
	/**
	 * @return the productStatus
	 */
	public ProductStatus getProductStatus() {
		return productStatus;
	}
	/**
	 * @param productStatus the productStatus to set
	 */
	public void setProductStatus(ProductStatus productStatus) {
		this.productStatus = productStatus;
	}
	public ProductType getProductType() {
		return productType;
	}
	public void setProductType(ProductType productType) {
		this.productType = productType;
	}
	public double getIssuePrice() {
		return issuePrice;
	}
	public void setIssuePrice(double issuePrice) {
		this.issuePrice = issuePrice;
	}
	public int getCurrencyId() {
		return currencyId;
	}
	public void setCurrencyId(int currencyId) {
		this.currencyId = currencyId;
	}
	public long getDenomination() {
		return denomination;
	}
	public void setDenomination(long denomination) {
		this.denomination = denomination;
	}
	
	/**
	 * @return the dayCountConvention
	 */
	public DayCountConvention getDayCountConvention() {
		return dayCountConvention;
	}
	/**
	 * @param dayCountConvention the dayCountConvention to set
	 */
	public void setDayCountConvention(DayCountConvention dayCountConvention) {
		this.dayCountConvention = dayCountConvention;
	}
	public ProductCouponFrequency getProductCouponFrequency() {
		return productCouponFrequency;
	}
	public void setProductCouponFrequency(ProductCouponFrequency productCouponFrequency) {
		this.productCouponFrequency = productCouponFrequency;
	}
	public double getMaxYield() {
		return maxYield;
	}
	public void setMaxYield(double maxYield) {
		this.maxYield = maxYield;
	}
	public double getMaxYieldPA() {
		return maxYieldPA;
	}
	public void setMaxYieldPA(double maxYieldPA) {
		this.maxYieldPA = maxYieldPA;
	}
	public double getLevelOfProtection() {
		return levelOfProtection;
	}
	public void setLevelOfProtection(double levelOfProtection) {
		this.levelOfProtection = levelOfProtection;
	}
	public double getLevelOfParticipation() {
		return levelOfParticipation;
	}
	public void setLevelOfParticipation(double levelOfParticipation) {
		this.levelOfParticipation = levelOfParticipation;
	}
	
	public double getStrikeLevel() {
		return strikeLevel;
	}
	public void setStrikeLevel(double strikeLevel) {
		this.strikeLevel = strikeLevel;
	}
	
	/**
	 * @return the bonusLevel
	 */
	public double getBonusLevel() {
		return bonusLevel;
	}
	/**
	 * @param bonusLevel the bonusLevel to set
	 */
	public void setBonusLevel(double bonusLevel) {
		this.bonusLevel = bonusLevel;
	}
	
	public Date getSubscriptionStartDate() {
		return subscriptionStartDate;
	}
	public void setSubscriptionStartDate(Date subscriptionStartDate) {
		this.subscriptionStartDate = subscriptionStartDate;
	}
	public Date getSubscriptionEndDate() {
		return subscriptionEndDate;
	}
	public void setSubscriptionEndDate(Date subscriptionEndDate) {
		this.subscriptionEndDate = subscriptionEndDate;
	}
	public Date getInitialFixingDate() {
		return initialFixingDate;
	}
	public void setInitialFixingDate(Date initialFixingDate) {
		this.initialFixingDate = initialFixingDate;
	}
	public Date getIssueDate() {
		return issueDate;
	}
	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}
	public Date getFirstExchangeTradingDate() {
		return firstExchangeTradingDate;
	}
	public void setFirstExchangeTradingDate(Date firstExchangeTradingDate) {
		this.firstExchangeTradingDate = firstExchangeTradingDate;
	}
	public Date getLastTradingDate() {
		return lastTradingDate;
	}
	public void setLastTradingDate(Date lastTradingDate) {
		this.lastTradingDate = lastTradingDate;
	}
	public Date getFinalFixingDate() {
		return finalFixingDate;
	}
	public void setFinalFixingDate(Date finalFixingDate) {
		this.finalFixingDate = finalFixingDate;
	}
	public Date getRedemptionDate() {
		return redemptionDate;
	}
	public void setRedemptionDate(Date redemptionDate) {
		this.redemptionDate = redemptionDate;
	}
	public boolean isSuspend() {
		return isSuspend;
	}
	public void setSuspend(boolean isSuspend) {
		this.isSuspend = isSuspend;
	}
	public boolean isQuanto() {
		return isQuanto;
	}
	public void setQuanto(boolean isQuanto) {
		this.isQuanto = isQuanto;
	}
	public int getProductInsuranceTypeId() {
		return productInsuranceTypeId;
	}
	public void setProductInsuranceTypeId(int productInsuranceTypeId) {
		this.productInsuranceTypeId = productInsuranceTypeId;
	}
	public double getBid() {
		return bid;
	}
	public void setBid(double bid) {
		this.bid = bid;
	}
	public double getAsk() {
		return ask;
	}
	public void setAsk(double ask) {
		this.ask = ask;
	}
	/**
	 * @return the formulaResult
	 */
	public BigDecimal getFormulaResult() {
		return formulaResult;
	}
	/**
	 * @param formulaResult the formulaResult to set
	 */
	public void setFormulaResult(BigDecimal formulaResult) {
		this.formulaResult = formulaResult;
	}
	public double getBondFloorAtissuance() {
		return bondFloorAtissuance;
	}
	public void setBondFloorAtissuance(double bondFloorAtissuance) {
		this.bondFloorAtissuance = bondFloorAtissuance;
	}

	
	/**
	 * @return the issueSize
	 */
	public long getIssueSize() {
		return issueSize;
	}
	/**
	 * @param issueSize the issueSize to set
	 */
	public void setIssueSize(long issueSize) {
		this.issueSize = issueSize;
	}
	
	public List<ProductMarket> getProductMarkets() {
		return productMarkets;
	}
	public void setProductMarkets(List<ProductMarket> productMarkets) {
		this.productMarkets = productMarkets;
	}
	public List<ProductScenario> getProductScenarios() {
		return productScenarios;
	}
	public void setProductScenarios(List<ProductScenario> productScenarios) {
		this.productScenarios = productScenarios;
	}
	public List<ProductCoupon> getProductCoupons() {
		return productCoupons;
	}
	public void setProductCoupons(List<ProductCoupon> productCoupons) {
		this.productCoupons = productCoupons;
	}
	public List<ProductAutocall> getProductAutocalls() {
		return productAutocalls;
	}
	public void setProductAutocalls(List<ProductAutocall> productAutocalls) {
		this.productAutocalls = productAutocalls;
	}
	public List<ProductCallable> getProductCallables() {
		return productCallables;
	}
	public void setProductCallables(List<ProductCallable> productCallables) {
		this.productCallables = productCallables;
	}
//	public List<ProductInfo> getProductInfos() {
//		return productInfos;
//	}
//	public void setProductInfos(List<ProductInfo> productInfos) {
//		this.productInfos = productInfos;
//	}
	
	public List<ProductSimulation> getProductSimulation() {
		return productSimulation;
	}
	public void setProductSimulation(List<ProductSimulation> productSimulation) {
		this.productSimulation = productSimulation;
	}
	
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	
	/**
	 * @return the capLevel
	 */
	public double getCapLevel() {
		return capLevel;
	}
	/**
	 * @param capLevel the capLevel to set
	 */
	public void setCapLevel(double capLevel) {
		this.capLevel = capLevel;
	}
	
	/**
	 * @return the productMaturity
	 */
	public ProductMaturity getProductMaturity() {
		return productMaturity;
	}
	/**
	 * @param productMaturity the productMaturity to set
	 */
	public void setProductMaturity(ProductMaturity productMaturity) {
		this.productMaturity = productMaturity;
	}	
	
	/**
	 * @return the productBarrier
	 */
	public ProductBarrier getProductBarrier() {
		return productBarrier;
	}
	/**
	 * @param productBarrier the productBarrier to set
	 */
	public void setProductBarrier(ProductBarrier productBarrier) {
		this.productBarrier = productBarrier;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Product [id=" + id + ", timeCreated=" + timeCreated + ", productStatus=" + productStatus
				+ ", productType=" + productType + ", issuePrice=" + issuePrice + ", currencyId=" + currencyId
				+ ", denomination=" + denomination + ", productCouponFrequency=" + productCouponFrequency
				+ ", dayCountConvention=" + dayCountConvention + ", maxYield=" + maxYield + ", maxYieldPA=" + maxYieldPA
				+ ", levelOfProtection=" + levelOfProtection + ", levelOfParticipation=" + levelOfParticipation
				+ ", strikeLevel=" + strikeLevel + ", bonusLevel=" + bonusLevel + ", subscriptionStartDate="
				+ subscriptionStartDate + ", subscriptionEndDate=" + subscriptionEndDate + ", initialFixingDate="
				+ initialFixingDate + ", issueDate=" + issueDate + ", firstExchangeTradingDate="
				+ firstExchangeTradingDate + ", lastTradingDate=" + lastTradingDate + ", finalFixingDate="
				+ finalFixingDate + ", redemptionDate=" + redemptionDate + ", isSuspend=" + isSuspend + ", isQuanto="
				+ isQuanto + ", productInsuranceTypeId=" + productInsuranceTypeId + ", bid=" + bid + ", ask=" + ask
				+ ", formulaResult=" + formulaResult + ", bondFloorAtissuance=" + bondFloorAtissuance + ", issueSize="
				+ issueSize + ", priority=" + priority + ", capLevel=" + capLevel + ", productMaturity="
				+ productMaturity + ", kidName=" + kidName + ", productBarrier=" + productBarrier + ", productMarkets="
				+ productMarkets + ", productScenarios=" + productScenarios + ", productCoupons=" + productCoupons
				+ ", productAutocalls=" + productAutocalls + ", productCallables=" + productCallables
				+ ", productSimulation=" + productSimulation + ", productSliders=" + productSliders
				+ ", ProductKidLanguages=" + ProductKidLanguages + "]";
	}
	
	public void calculateScenarios() {
		logger.info("calculating scenarios for product id = " + id);
		
		Parameters parameters = new Parameters(this, new BigDecimal("1"));
		logger.info(parameters.toString());
		productScenarios = productType.getProductScenarios(parameters);
		for (ProductScenario productScenario : productScenarios) {
			double addToFinalFixingLevel = 0;
			if ((productType.getId() == ProductTypeEnum.EXPRESS_CERTIFICATE.getId() &&
					productScenario.getPosition() == 1) ||
					(productType.getId() == ProductTypeEnum.CAPITAL_PROTECTION_CERTIFICATE_WITH_COUPON.getId() &&
					productScenario.getPosition() == 1)) {
				addToFinalFixingLevel = 1.1;
			}		
			double formularesult = productScenario.getFormulaResult() + 
					getAllCouponsValue(productScenario.getFinalFixingLevel() + addToFinalFixingLevel, parameters.getInitialFixingLevel());
			productScenario.setFormulaResult(formularesult > 0 ? formularesult : 0);			
		}
	}
	
	public void calculateSimulation() {
		BigDecimal hundred = new BigDecimal("100");
		Parameters parameters = new Parameters(this, new BigDecimal("100"));
		for (ProductSimulation productSimulation : getProductSimulation()) {
			productSimulation.setProductId(id);
			parameters.setFinalFixingLevel(parameters.getInitialFixingLevel().multiply((new BigDecimal(String.valueOf(productSimulation.getFinalFixingLevel())).divide(hundred))));
			parameters.setBarrierOccur(false);
			productSimulation.setCashSettlement(getSimulationCashSettlement(parameters));
			if (productBarrier != null && productBarrier.getProductBarrierType().getId() == ProductBarrierTypeEnum.American.getId()) { 
				parameters.setBarrierOccur(true);
				productSimulation.setCashSettlementBarrier(getSimulationCashSettlement(parameters));
			}
		}
	}
	
	public long getSimulationCashSettlement(Parameters parameters) {
		BigDecimal result = getProductType().getFormula().getSimulationCashSettlement(parameters);
		double couponsValue = getAllCouponsValue(parameters.getFinalFixingLevel().doubleValue(), parameters.getInitialFixingLevel());
		BigDecimal amount = simulationAmonut;
		if (productStatus.getId() == ProductStatusEnum.SECONDARY.getId()) {
			try {
				amount = simulationAmonut.divide(new BigDecimal(String.valueOf(ask)).divide(new BigDecimal("100"), 20, RoundingMode.HALF_UP), 20, RoundingMode.HALF_UP);
			} catch (ArithmeticException e) {
				logger.error("no ask cant calculate simulation for product id " + id);
				amount = new BigDecimal("0");
			}
		}
		result = result.add(new BigDecimal(String.valueOf(couponsValue))).multiply(amount);
		result = result.setScale(2, RoundingMode.HALF_UP);
		return result.longValue();
	}
	
	public void calculateSlider() {
		BigDecimal hundred = new BigDecimal("100");
		Parameters parameters = new Parameters(this, hundred);
		BigDecimal formulaResultSlider;
		productSliders = new ArrayList<ProductSlider>();
		for (int i = (100 + productType.getSliderMin()); i <= (100 + productType.getSliderMax()); i++) {
			ProductSlider productSlider = new ProductSlider(i);
			parameters.setFinalFixingLevel(new BigDecimal(String.valueOf(i)));
			parameters.setBarrierOccur(false);
			double allCouponsValue = getAllCouponsValue(parameters.getFinalFixingLevel().doubleValue(), parameters.getInitialFixingLevel());
			formulaResultSlider = getProductType().getFormula().getFormulaResult(parameters).add(
					(new BigDecimal(String.valueOf(allCouponsValue))));			
			productSlider.setFormulaResult(formulaResultSlider.doubleValue());
			if (productBarrier != null) { 
				parameters.setBarrierOccur(true);
				formulaResultSlider = getProductType().getFormula().getFormulaResult(parameters).add(
						(new BigDecimal(String.valueOf(allCouponsValue))));			
				productSlider.setFormulaResultBarrier(formulaResultSlider.doubleValue());
			}
			productSliders.add(productSlider);
		}
	}
	
	public double getAllCouponsValue(double finalFixingLevel, BigDecimal initialFixingLevel) {
//		logger.info("get coupons value finalFixingLevel " + finalFixingLevel + " initialFixingLevel " + initialFixingLevel);
		Date issueDate = getIssueDate();
		double couponsValue = 0;
		if (null != productCoupons) {
			for (ProductCoupon productCoupon : productCoupons) {
				double coupon = productCoupon.getCouponValue(finalFixingLevel, 
						initialFixingLevel, issueDate,
						dayCountConvention, productType.getId());
				logger.info("add coupon value " + coupon);
				issueDate = productCoupon.getPaymentDate();
				couponsValue += coupon;
			}
		}
		return couponsValue;
	}
	
	public double getUpside() {
		if (productBarrier != null) {
			BigDecimal hundred = new BigDecimal(100);
			BigDecimal upside = new BigDecimal(String.valueOf(levelOfParticipation))
					.divide(hundred, 20, RoundingMode.HALF_UP).multiply((new BigDecimal(String.valueOf(productBarrier.getBarrierLevel()))
					.divide(hundred, 20, RoundingMode.HALF_UP)
					.subtract(new BigDecimal(1))))
					.subtract((new BigDecimal(1).subtract(new BigDecimal(String.valueOf(levelOfProtection))
					.divide(hundred, 20, RoundingMode.HALF_UP))))
					.multiply(hundred);
			upside.setScale(2, RoundingMode.HALF_UP);
			return upside.doubleValue();
		}
		return 0;		
	}
	
	/**
	 * only for junit!
	 * @param upside
	 */
	public void setUpside(double upside) {
		//do nothing we need it for junit
	}
	
	/**
	 * calculate and get formula result if it wasn't already calculate
	 * @return formulaResult
	 */
	public BigDecimal calculateAndGetFormulaResult() {
		if (formulaResult == null || formulaResult.doubleValue() == 0) {
			double endTradeLevel = productMarkets.get(0).getEndTradeLevel() != null ? productMarkets.get(0).getEndTradeLevel() : 0;
			Parameters parameters = new Parameters(this, new BigDecimal(String.valueOf(productMarkets.get(0).getStartTradeLevel())), new BigDecimal(String.valueOf(endTradeLevel)));	
			formulaResult = productType.getFormula().getFormulaResult(parameters);
		}
		return formulaResult;
	}
	
	/**
	 * @return the productSliders
	 */
	public List<ProductSlider> getProductSliders() {
		return productSliders;
	}
	
	/**
	 * @param productSliders the productSliders to set
	 */
	public void setProductSliders(List<ProductSlider> productSliders) {
		this.productSliders = productSliders;
	}
	
	public double getCouponDays() {
		double days = 0;
		if (productCoupons != null && !productCoupons.isEmpty() && dayCountConvention != null && dayCountConvention.getName() != null) {
			days = dayCountConvention.getDayCountConvintionDaycount(getIssueDate(), productCoupons.get(0).getPaymentDate());
		}
		return days;
	}
	
	public void setCouponDays(double couponsDays) {
		//for jackson
	}
	/**
	 * @return the kidName
	 */
	public String getKidName() {
		return kidName;
	}
	/**
	 * @param kidName the kidName to set
	 */
	public void setKidName(String kidName) {
		this.kidName = kidName;
	}
	/**
	 * @return the productKidLanguages
	 */
	public List<ProductKidLanguage> getProductKidLanguages() {
		return ProductKidLanguages;
	}
	/**
	 * @param productKidLanguages the productKidLanguages to set
	 */
	public void setProductKidLanguages(List<ProductKidLanguage> productKidLanguages) {
		ProductKidLanguages = productKidLanguages;
	}
}
