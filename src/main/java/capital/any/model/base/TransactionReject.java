package capital.any.model.base;

import java.io.Serializable;
import java.util.Date;
import capital.any.base.enums.ActionSource;
import capital.any.base.enums.TransactionRejectType;
import capital.any.model.base.Transaction.TransactionOperationEnum;
import capital.any.model.base.Transaction.TransactionPaymentTypeEnum;

/**
 * @author LioR SoLoMoN
 *
 */
public class TransactionReject implements Serializable {

	private static final long serialVersionUID = -5596864734948226465L;
	private long id;
	private long userId;
	private TransactionPaymentTypeEnum paymentType;
	private TransactionRejectType rejectType;
	private TransactionOperationEnum operation;
	private long amount;
	private String sessionId;
	private Date timeCreated;
	private ActionSource actionSource;
	private Integer writerId;
	private String info;
	private String serverName;

	/**
	 * @param userId
	 * @param paymentType
	 * @param rejectType
	 * @param operation - TransactionOperation
	 * @param amount
	 * @param sessionId
	 * @param actionSource
	 * @param writerId
	 * @param info
	 * @param serverName
	 */
	public TransactionReject(long userId, TransactionPaymentTypeEnum paymentType,
			TransactionRejectType rejectType, TransactionOperationEnum operation, long amount, String sessionId,
			ActionSource actionSource, Integer writerId, String info, String serverName) {
		this.userId = userId;
		this.paymentType = paymentType;
		this.rejectType = rejectType;
		this.operation = operation;
		this.amount = amount;
		this.sessionId = sessionId;
		this.actionSource = actionSource;
		this.writerId = writerId;
		this.info = info;
		this.serverName = serverName;
	}




	@Override
	public String toString() {
		return "TransactionReject [id=" + id + ", userId=" + userId + ", paymentType=" + paymentType + ", rejectTypeId="
				+ rejectType + ", operation=" + operation + ", amount=" + amount + ", sessionId=" + sessionId
				+ ", timeCreated=" + timeCreated + ", actionSource=" + actionSource + ", writerId=" + writerId
				+ ", info=" + info + ", serverName=" + serverName + "]";
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the paymentType
	 */
	public TransactionPaymentTypeEnum getPaymentType() {
		return paymentType;
	}
	/**
	 * @param paymentType the paymentType to set
	 */
	public void setPaymentType(TransactionPaymentTypeEnum paymentType) {
		this.paymentType = paymentType;
	}
	/**
	 * @return the rejectType
	 */
	public TransactionRejectType getRejectType() {
		return rejectType;
	}
	/**
	 * @param rejectType the rejectType to set
	 */
	public void setRejectType(TransactionRejectType rejectType) {
		this.rejectType = rejectType;
	}
	/**
	 * @return the operation
	 */
	public TransactionOperationEnum getOperation() {
		return operation;
	}
	/**
	 * @param operation the operation to set
	 */
	public void setOperation(TransactionOperationEnum operation) {
		this.operation = operation;
	}
	/**
	 * @return the amount
	 */
	public long getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(long amount) {
		this.amount = amount;
	}
	/**
	 * @return the sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}
	/**
	 * @param sessionId the sessionId to set
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	/**
	 * @return the actionSource
	 */
	public ActionSource getActionSource() {
		return actionSource;
	}
	/**
	 * @param actionSource the actionSource to set
	 */
	public void setActionSource(ActionSource actionSource) {
		this.actionSource = actionSource;
	}
	/**
	 * @return the writerId
	 */
	public Integer getWriterId() {
		return writerId;
	}
	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(Integer writerId) {
		this.writerId = writerId;
	}
	/**
	 * @return the info
	 */
	public String getInfo() {
		return info;
	}
	/**
	 * @param info the info to set
	 */
	public void setInfo(String info) {
		this.info = info;
	}
	/**
	 * @return the serverName
	 */
	public String getServerName() {
		return serverName;
	}
	/**
	 * @param serverName the serverName to set
	 */
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}
}