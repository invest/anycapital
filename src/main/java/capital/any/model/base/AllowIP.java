package capital.any.model.base;

import java.io.Serializable;

/**
 * @author eranl
 *
 */
public class AllowIP implements Serializable {
		
	private static final long serialVersionUID = 3825024845463202759L;
	private String ip;
	private String comment;
	
	public AllowIP() {		
	}

	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * @param ip the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AllowIP [ip=" + ip + ", comment=" + comment + "]";
	}
	
}
