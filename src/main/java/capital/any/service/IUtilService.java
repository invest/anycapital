package capital.any.service;

import java.util.Date;
import java.util.Locale;

/**
 * @author LioR SoLoMoN
 *
 */
public interface IUtilService {
	public static final String EMPTY_STRING = "";
	public final static String LOCAL_HOST_IPV4 = "127.0.0.1";
	public final static String LOCAL_HOST_IPV6 = "0:0:0:0:0:0:0:1";
	
	/** 
 	 * @param parameter
 	 * @return true if the argument is null or length = 0. false otherwise
 	 */
	boolean isArgumentEmptyOrNull(String parameter);

	/**
	 * do unix command
	 * 
	 * @param command
	 * @return
	 */
	String doUnixCommand(String command);
	
	/**
	 * init the date time to end of day (23:59:59)
	 * @param date
	 * @return date
	 */
	Date setEndTime(Date date);
	
	/**
	 * init the date time to start of day (00:00:00)
	 * @param date
	 * @return date
	 */
	Date setStartTime(Date date);
	
	 /**
     * Get an object reference variable and return the member class
     * generate, concatenate and return query string format 
     * 
     * Examples:
     * 	field1=value1&field2=value2&field3=value3
     * 	field1:value1;field2:value2;field3:value3
     * 
     * use reflection on the member class fields. members should be public.
     * @param objRef
     * @param interToken
     * @param endToken
     * @param includeNullData
     * @return
     */
	String generateObjectToQueryString(Object objRef, String interToken , String endToken, boolean includeNullData);
	
	/**
	 * display amount with currency symbol default €
	 * @param amount to format
	 * @param locale null for default or locale 
	 * @return format amount with symbol
	 */
	String displayAmount(long amount, Locale locale, String currencyCode);
	
}
