package capital.any.service.base;

import capital.any.model.base.BalanceRequest;

/**
 * @author LioR SoLoMoN
 *
 */
public interface IBalanceService {
	
	/**
	 * update user.balance
	 * insert record to balance history
	 * 
	 * @param balanceRequest
	 * @throws Exception 
	 */
	void changeBalance(BalanceRequest balanceRequest) throws Exception;
	
	/**
	 * get user current balance in db
	 * @param user id
	 * @return amount in cents
	 */
	long getBalance(long id);
	
}
