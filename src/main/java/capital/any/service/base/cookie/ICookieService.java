package capital.any.service.base.cookie;

import java.util.Map;import javax.servlet.http.HttpServletRequest;
import capital.any.model.base.CookieInfo;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface ICookieService {
	public static final int Year = 60 * 60 * 24 * 365;
	public static final String GTM_AB = "gtm_ab";
	public static final String COLON = ":";
	public static final String LANGUAGE = "lang";
	
	void addAttribute(CookieInfo cookieInfo);

	Map<String, String> readCookieMap(HttpServletRequest request);
}
