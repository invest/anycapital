package capital.any.service.base.cookie;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import capital.any.model.base.CookieInfo;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service
public class CookieService implements ICookieService {
	private static final Logger logger = LoggerFactory.getLogger(CookieService.class);

	@Override
	public void addAttribute(CookieInfo cookieInfo) {
		logger.info("add attribute to cookie, name: " + cookieInfo.getAttributeName() + 
				", value: " + cookieInfo.getAttributeValue());
		Cookie cookie = new Cookie(cookieInfo.getAttributeName(), cookieInfo.getAttributeValue());
		cookie.setPath("/");
		cookie.setMaxAge(cookieInfo.getExpiry());
		cookieInfo.getResponse().addCookie(cookie);
	}
	
	@Override
	public Map<String, String> readCookieMap(HttpServletRequest request) {
		Map<String, String> cookieMap = new HashMap<String, String>();
	    Cookie[] cookies = request.getCookies();
	    if(null != cookies){
	        for(Cookie cookie : cookies) {
	            cookieMap.put(cookie.getName(), cookie.getValue());
	        }
	    }
	    return cookieMap;
	} 

}
