package capital.any.service.base.issueReactions;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import capital.any.dao.base.issueReaction.IIssueReactionDao;
import capital.any.model.base.IssueReaction;

/**
 * 
 * @author eyal.ohana
 *
 */
@Primary
@Service
public class IssueReactionService implements IIssueReactionService {
	private static final Logger logger = LoggerFactory.getLogger(IssueReactionService.class);
			
	@Autowired
	private IIssueReactionDao issueReactionDao;
	@Autowired
	@Qualifier("IssueReactionHCDao")
	private IIssueReactionDao issueReactionHCDao;
	
	@Override
	public Map<Integer, IssueReaction> get() {
		Map<Integer, IssueReaction> issueReactions = issueReactionHCDao.get();
		if (issueReactions == null || issueReactions.size() <= 0) {
			logger.info("issueReactions empty / null in hazelcast, Let's check RDBMS");
			issueReactions = issueReactionDao.get();
		}
		return issueReactions;
	}

}
