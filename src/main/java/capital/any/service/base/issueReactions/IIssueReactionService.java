package capital.any.service.base.issueReactions;

import java.util.Map;

import capital.any.model.base.IssueReaction;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IIssueReactionService {

	Map<Integer, IssueReaction> get();

}
