package capital.any.service.base.transactionReject;

import capital.any.model.base.TransactionReject;

/**
 * @author LioR SoLoMoN
 *
 */
public interface ITransactionRejectService {

	/**
	 * @param transactionReject
	 */
	void insert(TransactionReject transactionReject);
}
