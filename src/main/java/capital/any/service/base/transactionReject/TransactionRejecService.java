package capital.any.service.base.transactionReject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import capital.any.dao.base.transactionReject.ITransactionRejectDao;
import capital.any.model.base.TransactionReject;

/**
 * @author LioR SoLoMoN
 *
 */
@Service
public class TransactionRejecService implements ITransactionRejectService {
	private static final Logger logger = LoggerFactory.getLogger(TransactionRejecService.class);
	
	@Autowired
	private ITransactionRejectDao transactionRejectDao;
	
	@Override
	public void insert(TransactionReject transactionReject) {
		try {
			transactionRejectDao.insert(transactionReject);
		} catch (Exception e) {
			logger.error("Can't insert transaction reject", e);
		}		
	}
}
