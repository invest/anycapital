package capital.any.service.base.aws;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.util.IOUtils;

import capital.any.model.base.File;
import capital.any.model.base.Language;
import capital.any.model.base.Product;

/**
 * @author Eyal G
 *
 */
@Primary
@Service
public class AwsService implements IAwsService {
	private static final Logger logger = LoggerFactory.getLogger(AwsService.class);
	
	private AmazonS3 s3client;
	
	@Autowired
	private Environment environment;
	private String bucketName;
	private String bucketNameCdn;
	private String envPrefix;
	private String filesPrefix;
	private String templatesPrefix;
	private String marketingContentHTMLPrefix;
	private String productKidPrefix;
	
	private void init() {
		AWSCredentials credentials = new BasicAWSCredentials(environment.getProperty("aws.accessKey"), environment.getProperty("aws.secretKey"));
		s3client = AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.EU_WEST_1).build();
		bucketName = environment.getProperty("aws.bucketName");
		bucketNameCdn = environment.getProperty("aws.bucketName.cdn");
		envPrefix = environment.getProperty("aws.env.prefix");
		filesPrefix = environment.getProperty("aws.files.prefix");
		templatesPrefix = environment.getProperty("aws.templates.prefix");
		marketingContentHTMLPrefix = environment.getProperty("aws.marketing.content.html.prefix");
		productKidPrefix = environment.getProperty("aws.product.kid.prefix");
	}
	
//	@Override
//	public void getBuckets() {
//		try {
//			logger.debug("Listing buckets:");
//			for (Bucket bucket : s3client.listBuckets()) {
//				logger.debug("--> " + bucket.getName());
//			}
//		} catch (Exception e) {
//			logger.error("cant get buckets", e);
//		}
//	}
//	
//	@Override
//	public void getFilesInBucket() {
//		try {
//			logger.debug("Listing get Files In Bucket:");
//			ListObjectsRequest listObjectsRequest = new ListObjectsRequest()
//				    .withBucketName("anycapital")
////				    .withPrefix("10002/") //start with prefix
//				    .withDelimiter("/");
//				ObjectListing objectListing;
//
//				do {
//				        objectListing = s3client.listObjects(listObjectsRequest);
//				        for (S3ObjectSummary objectSummary : 
//				            objectListing.getObjectSummaries()) {
//				        	logger.info( " - " + objectSummary.getKey() + "  " +
//				                    "(size = " + objectSummary.getSize() + 
//				                    ")  (owner = " + objectSummary.getOwner() +
//				                    ")  (etag = " + objectSummary.getETag() + 
//				                    ")  (url = " + s3client.getUrl(objectSummary.getBucketName(), objectSummary.getKey()).toString() +
//				                    ")");
//				        }
//				        listObjectsRequest.setMarker(objectListing.getNextMarker());
//				} while (objectListing.isTruncated());
//		} catch (Exception e) {
//			logger.error("cant get buckets", e);
//		}
//	}
	
	@Override
	public InputStream getUserFile(File file) {
		if (s3client == null) {
			init();
		}
		String keyName = filesPrefix + "/" + file.getUserId() + "/" + file.getName();
		return getFile(keyName, bucketName);
	}
	
	@Override
	public InputStream getTemplate(String templateName) {
		if (s3client == null) {
			init();
		}
		String keyName = templatesPrefix + "/" + templateName;
		return getFile(keyName, bucketName);
	}
	
	@Override
	public InputStream getMarketingContentHTML(String name) {
		if (s3client == null) {
			init();
		}
		String keyName = marketingContentHTMLPrefix + "/" + name;
		return getFile(keyName, bucketName);
	}

	private InputStream getFile(String keyName, String bucket) {
		if (s3client == null) {
			init();
		}
		keyName = envPrefix + "/" + keyName;
		S3Object object = s3client.getObject(
		                  new GetObjectRequest(bucket, keyName));
		InputStream objectData = object.getObjectContent();
		return objectData;
	}
	@Override
	public void uploadUserFile(MultipartFile fileUpload, File file, String contentType) throws Exception {
		if (s3client == null) {
			init();
		}	
		String keyName = envPrefix + "/" + filesPrefix + "/" + file.getUserId() + "/" + file.getName();
		uploadFile(fileUpload, keyName, contentType, bucketName);
	}

	@Override
	public void uploadMarketingContentHTMLFile(MultipartFile fileUpload, String fileName, String contentType) throws Exception {
		if (s3client == null) {
			init();
		}	
		String keyName = envPrefix + "/" + marketingContentHTMLPrefix + "/" + fileName;
		uploadFile(fileUpload, keyName, contentType, bucketName);
	}
	
	@Override
	public void uploadProductKidFile(MultipartFile fileUpload, Product product, Language language, String contentType) throws Exception {
		if (s3client == null) {
			init();
		}
		String sourceKey = getSourceKeyProductKid(product, language);
		String destinationKey = envPrefix + "/" + productKidPrefix + "/" + product.getId() + "/" + language.getCode() + "/" + new Date().getTime() + "/" + product.getKidName();
		moveFile(bucketNameCdn, sourceKey, destinationKey);		
		uploadFile(fileUpload, sourceKey, contentType, bucketNameCdn);
	}
	
	@Override
	public String getSourceKeyProductKid(Product product, Language language) {
		if (s3client == null) {
			init();
		}
		return envPrefix + "/" + productKidPrefix + "/" + product.getId() + "/" + language.getCode() + "/" + product.getKidName();
	}
	
	public void uploadFile(MultipartFile fileUpload, String keyName, String contentType, String bucket) throws Exception {
		try {
			byte[] contentBytes = null;
			try {
			    InputStream is = fileUpload.getInputStream();
			    contentBytes = IOUtils.toByteArray(is);
			} catch (IOException e) {
				logger.error("Failed while reading bytes from: " + fileUpload.getOriginalFilename(), e);
			    throw e;
			}
			Long contentLength = Long.valueOf(contentBytes.length);
			ObjectMetadata objectMetadata = new ObjectMetadata();
			objectMetadata.setContentLength(contentLength);			
			if (contentType != null) {    
				objectMetadata.setContentType(contentType);
            }
			s3client.putObject(new PutObjectRequest(
	    		                 bucket, keyName, fileUpload.getInputStream(), objectMetadata));
		} catch (AmazonServiceException ase) {
			logger.error("Caught an AmazonServiceException, which " +
            		"means your request made it " +
                    "to Amazon S3, but was rejected with an error response" +
                    " for some reason.");
			logger.error("HTTP Status Code: " + ase.getStatusCode());
			logger.error("AWS Error Code:   " + ase.getErrorCode());
			logger.error("Error Type:       " + ase.getErrorType());
			logger.error("Request ID:       " + ase.getRequestId());
			logger.error("Error Message:    " + ase.getMessage(), ase);
			throw ase;
        } catch (AmazonClientException ace) {
        	logger.error("Caught an AmazonClientException, which " +
            		"means the client encountered " +
                    "an internal error while trying to " +
                    "communicate with S3, " +
                    "such as not being able to access the network.");
        	logger.error("Error Message: " + ace.getMessage(), ace);
        	throw ace;
        } catch (IOException e) {
        	logger.error("cant upload file" , e);
        	throw e;
        }
	}
	
	/**
	 * Move file.
	 * @param bucket
	 * @param sourceKey
	 * @param destinationKey
	 */
	public void moveFile(String bucket, String sourceKey, String destinationKey) {
		try {
			s3client.copyObject(bucket, sourceKey, bucket, destinationKey);
		} catch (AmazonServiceException ase) {
			logger.error("Caught an AmazonServiceException, which " +
	        		"means your request made it " +
	                "to Amazon S3, but was rejected with an error response" +
	                " for some reason.");
			logger.error("HTTP Status Code: " + ase.getStatusCode());
			logger.error("AWS Error Code:   " + ase.getErrorCode());
			logger.error("Error Type:       " + ase.getErrorType());
			logger.error("Request ID:       " + ase.getRequestId());
			logger.error("Error Message:    " + ase.getMessage(), ase);
	    } catch (AmazonClientException ace) {
	    	logger.error("Caught an AmazonClientException, which " +
	        		"means the client encountered " +
	                "an internal error while trying to " +
	                "communicate with S3, " +
	                "such as not being able to access the network.");
	    	logger.error("Error Message: " + ace.getMessage(), ace);
	    } catch (Exception e) {
	    	logger.error("cant move file" , e);
	    }
	}
}
