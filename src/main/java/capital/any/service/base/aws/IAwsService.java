package capital.any.service.base.aws;

import java.io.InputStream;

import org.springframework.web.multipart.MultipartFile;

import capital.any.model.base.File;
import capital.any.model.base.Language;
import capital.any.model.base.Product;

/**
 * @author Eyal g
 *
 */
public interface IAwsService {	

//	void getBuckets();
//
//	void getFilesInBucket();

//	InputStream getFile(File file);

	void uploadUserFile(MultipartFile fileUpload, File file, String contentType) throws Exception;

	void uploadMarketingContentHTMLFile(MultipartFile fileUpload, String fileName, String contentType) throws Exception;
	
	/**
	 * get template for sending email
	 * @param templateName
	 * @return InputStream template
	 */
	InputStream getTemplate(String templateName);
	

	/**
	 * Get marketing content HTML
	 * @param name
	 * @return
	 */
	InputStream getMarketingContentHTML(String name);

	/**
	 * get user file
	 * @param file
	 * @return InputStream of the file
	 */
	InputStream getUserFile(File file);

	/**
	 * Upload product KID file
	 * @param fileUpload
	 * @param product
	 * @param contentType
	 * @throws Exception
	 */
	void uploadProductKidFile(MultipartFile fileUpload, Product product, Language language, String contentType) throws Exception;

	/**
	 * Get source key product kid
	 * @param product
	 * @param language
	 * @return
	 */
	String getSourceKeyProductKid(Product product, Language language);
}
