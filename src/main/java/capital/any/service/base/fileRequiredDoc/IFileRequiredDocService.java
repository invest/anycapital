package capital.any.service.base.fileRequiredDoc;

import java.util.List;

import capital.any.model.base.FileRequiredDocGroup;

/**
 * 
 * @author Eyal.o
 *
 */
public interface IFileRequiredDocService {

	List<FileRequiredDocGroup> getRequiredDocs(long userId);

}
