package capital.any.service.base.fileRequiredDoc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.fileRequiredDoc.IFileRequiredDocDao;
import capital.any.model.base.FileRequiredDocGroup;

/**
 * 
 * @author Eyal.o
 *
 */
@Service
public class FileRequiredDocService implements IFileRequiredDocService {
	
	@Autowired
	private IFileRequiredDocDao fileRequiredDocDao;
	
	@Override
	public List<FileRequiredDocGroup> getRequiredDocs(long userId) {
		return fileRequiredDocDao.getRequiredDocs(userId);
	}

}
