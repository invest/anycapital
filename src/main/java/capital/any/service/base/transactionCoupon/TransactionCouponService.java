package capital.any.service.base.transactionCoupon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.transactionCoupon.ITransactionCouponDao;
import capital.any.model.base.TransactionCoupon;

/**
 * @author Eyal Goren
 *
 */
@Service
public class TransactionCouponService implements ITransactionCouponService {
	private static final Logger logger = LoggerFactory.getLogger(TransactionCouponService.class);
	
	@Autowired
	private ITransactionCouponDao transactionCouponDao;
	
	@Override
	public void insert(TransactionCoupon transactionCoupon) {
		transactionCouponDao.insert(transactionCoupon);
	}
}
