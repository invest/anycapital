package capital.any.service.base.transactionCoupon;

import capital.any.model.base.TransactionCoupon;

/**
 * @author Eyal Goren
 *
 */
public interface ITransactionCouponService {	
	
	/**
	 * insert {@link TransactionCoupon} into db
	 * @param transactionCoupon
	 */
	void insert(TransactionCoupon transactionCoupon);
}
