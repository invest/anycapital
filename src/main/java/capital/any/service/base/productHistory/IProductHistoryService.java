package capital.any.service.base.productHistory;

import capital.any.model.base.ProductHistory;

/**
 * @author Eyal G
 *
 */
public interface IProductHistoryService {	
	
	/**
	 * insert into productHistory
	 * @param productHistory
	 */
	void insert(ProductHistory productHistory);
}
