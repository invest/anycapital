package capital.any.service.base.productHistory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import capital.any.dao.base.productHistory.IProductHistoryDao;
import capital.any.model.base.ProductHistory;

/**
 * @author Eyal G
 *
 */
@Primary
@Service
public class ProductHistoryService implements IProductHistoryService {
	private static final Logger logger = LoggerFactory.getLogger(ProductHistoryService.class);
	
	@Autowired
	private IProductHistoryDao productHistoryDao;

	@Override
	public void insert(ProductHistory productHistory) {
		try {
			productHistoryDao.insert(productHistory);
		} catch (Exception e) {
			logger.error("Can't insert to product history product id " + productHistory.getProductId(), e);
		}
	}	
}
