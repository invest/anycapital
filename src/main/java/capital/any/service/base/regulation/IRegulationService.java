package capital.any.service.base.regulation;

import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.RegulationDetails;
import capital.any.model.base.RegulationResponse;
import capital.any.model.base.User;
import capital.any.model.base.UserHistory;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IRegulationService {
	
	RegulationResponse doRegulation(RegulationDetails regulationDetails, UserHistory userHistory) throws Exception;

	ResponseCode approveUser(User user, UserHistory userHistory);

	ResponseCode questionnaireFittingAfterTraining(RegulationDetails regulationDetails, UserHistory userHistory) throws Exception;

	RegulationDetails getRegulationDetails(long userId);

	User doRegulationODT(RegulationDetails regulationDetails, UserHistory userHistory) throws Exception;

	ResponseCode questionnaireFittingAfterTrainingODT(RegulationDetails regulationDetails, UserHistory userHistory) throws Exception;
	
}
