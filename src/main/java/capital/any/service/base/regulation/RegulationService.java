package capital.any.service.base.regulation;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import capital.any.base.enums.GBGStatus;
import capital.any.base.enums.RegulationQuestionnaireStatusEnum;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.Country;
import capital.any.model.base.EmailAction;
import capital.any.model.base.EmailActionRequest;
import capital.any.model.base.GBGCountry;
import capital.any.model.base.GBGRequest;
import capital.any.model.base.GBGResponse;
import capital.any.model.base.RegulationDetails;
import capital.any.model.base.RegulationFileRule;
import capital.any.model.base.RegulationQuestionnaireStatus;
import capital.any.model.base.RegulationResponse;
import capital.any.model.base.User;
import capital.any.model.base.UserHistory;
import capital.any.model.base.User.Step;
import capital.any.model.base.UserStep;
import capital.any.service.base.country.ICountryService;
import capital.any.service.base.emailAction.IEmailActionService;
import capital.any.service.base.gbg.IGBGService;
import capital.any.service.base.gbg.country.IGBGCountryService;
import capital.any.service.base.gbg.score.IGBGScoreService;
import capital.any.service.base.qmUserAnswer.IQmUserAnswerService;
import capital.any.service.base.regulationFileRule.IRegulationFileRuleService;
import capital.any.service.base.regulationQuestionnaireStatus.IRegulationQuestionnaireStatusService;
import capital.any.service.base.regulationUserInfo.IRegulationUserInfoService;
import capital.any.service.base.user.IUserService;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service
public class RegulationService implements IRegulationService {
	private static final Logger logger = LoggerFactory.getLogger(RegulationService.class);
	
	@Autowired
	private IUserService userService;
	@Autowired
	private IRegulationUserInfoService regulationUserInfoService;
	@Autowired
	private IQmUserAnswerService qmUserAnswerService;
	@Autowired
	private ICountryService countryService;
	@Autowired
	private IGBGService gbgService;
	@Autowired
	private IGBGCountryService gbgCountryService;
	@Autowired
	private IGBGScoreService gbgScoreService;
	@Autowired
	private IRegulationFileRuleService regulationFileRuleService;
	@Autowired
	private IRegulationQuestionnaireStatusService regulationQuestionnaireStatusService;
	@Autowired
	private IEmailActionService emailActionService;

	public void questionnaireCheck(RegulationDetails regulationDetails) throws Exception {
		logger.debug("RegulationService.questionnaireCheck - Start.");
		long score = qmUserAnswerService.getSumScore(regulationDetails.getQmUserAnswerList());
		Map<Long, Integer> map = regulationQuestionnaireStatusService.getScoreStatus();		
		int statusId = RegulationQuestionnaireStatusEnum.FITTING.getId();
		if (null != map.get(score)) {
			statusId = map.get(score);
		}
		regulationDetails.getRegulationUserInfo().setRegulationQuestionnaireStatus(new RegulationQuestionnaireStatus(statusId));
		regulationUserInfoService.insert(regulationDetails.getRegulationUserInfo());
		if (questionnaireStatusCheck(regulationDetails, RegulationQuestionnaireStatusEnum.UNFITTING.getId())
				|| questionnaireStatusCheck(regulationDetails, RegulationQuestionnaireStatusEnum.UNFITTING_RESTRICTED.getId())) {
			EmailActionRequest<Object> emailActionRequest = new EmailActionRequest<Object>(regulationDetails.getUser(), new EmailAction(capital.any.base.enums.EmailAction.HIGH_RISK_PRODUCTS.getId()));
			emailActionRequest.setActionSource(regulationDetails.getUser().getActionSource());
			emailActionService.insertEmailByAction(emailActionRequest);
		}
		logger.debug("RegulationService.questionnaireCheck - End.");
	}

	public RegulationResponse userCheck(RegulationDetails regulationDetails, UserHistory userHistory) {
		logger.debug("RegulationService.userCheck - Start. " + regulationDetails.toString());
		UserStep userStep = new UserStep(Step.MISSING.getId());
		Map<Integer, RegulationFileRule> mapRequiredDocs = null;
		//TODO ADD enum for country risk
		int countryRiskId = 1;
		int gbgStatusId = GBGStatus.FAIL.getId();
		if (questionnaireStatusCheck(regulationDetails, RegulationQuestionnaireStatusEnum.FITTING.getId())
				|| questionnaireStatusCheck(regulationDetails, RegulationQuestionnaireStatusEnum.FITTING_AFTER_TRAINING.getId())) {
			userStep = new UserStep(Step.PENDING.getId());
			gbgStatusId = gbgCheck(regulationDetails.getUser());
			countryRiskId = countryRiskCheck(regulationDetails.getUser());
			//TODO ADD enum for country risk
			if (gbgStatusId == GBGStatus.PASS.getId() && countryRiskId == 1) {
				userStep = new UserStep(Step.APPROVE.getId());
			} else {
				RegulationFileRule regulationFileRule = new RegulationFileRule();
				regulationFileRule.setCountryRiskId(countryRiskId);
				regulationFileRule.setGbgStatusId(gbgStatusId);
				mapRequiredDocs = regulationFileRuleService.getFilesByRule(regulationFileRule);
			}
		}
		regulationDetails.getUser().setUserStep(userStep);
		userService.updateStep(regulationDetails.getUser(), userHistory);
		RegulationResponse regulationResponse = new RegulationResponse(regulationDetails.getUser(), mapRequiredDocs);
		logger.debug("RegulationService.userCheck - End.");
		return regulationResponse;
	}

	public int gbgCheck(User user) {
		logger.debug("RegulationService.gbgCheck - Start.");
		int gbgStatusId = GBGStatus.FAIL.getId();
		/* Time Birth Date */
		Date timeBirthDate = user.getTimeBirthDate();
		Calendar cal = Calendar.getInstance();
		cal.setTime(timeBirthDate);
		String day = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
		String month = String.valueOf(cal.get(Calendar.MONTH) + 1);
		String year = String.valueOf(cal.get(Calendar.YEAR));
		/* GBG Integration */
		GBGCountry gbgCountry = gbgCountryService.get(user.getCountryByUser());
		if (null != gbgCountry) {
			GBGRequest gbgRequest = 
					new GBGRequest(user.getId(), 
							gbgCountry.getProfileId(),
							user.getFirstName(),
							user.getLastName(), 
							user.getGender().name(),
							day,
							month, 
							year,
							countryService.get().get(user.getCountryByUser()).getCountryName(),
							user.getStreet() + " " + user.getStreetNo(),
							user.getCityName(), 
							user.getZipCode(), 
							user.getStreetNo());
			Response<GBGResponse> response = gbgService.authenticateSP(gbgRequest);
			GBGResponse gbgResponse = ((GBGResponse)response.getData());
			int score = gbgResponse.getScore();
			gbgStatusId = gbgScoreService.getStatus(score).getId();
		} else {
			logger.info("The country id " + user.getCountryByUser() + " has not profile id by GBG.");
		}
		logger.debug("RegulationService.gbgCheck - End.");
		return gbgStatusId;
	}

	public int countryRiskCheck(User user) {
		logger.debug("RegulationService.countryRiskCheck - START.");
		Map<Integer, Country> countries = countryService.get();
		int countryRiskIdByUser = countries.get(user.getCountryByUser()).getCountryRisk().getId();
		int countryRiskIdByIP = countries.get(user.getCountryByIP()).getCountryRisk().getId();
		int countryRiskIdByPrefix = countries.get(user.getCountryByPrefix()).getCountryRisk().getId();
		int crId = countryRiskIdByUser;
		if (crId < countryRiskIdByIP) {
			crId = countryRiskIdByIP;
		}
		if (crId < countryRiskIdByPrefix) {
			crId = countryRiskIdByPrefix;
		}
		logger.debug("RegulationService.countryRiskCheck - END.");
		return crId;
	}
	
	/**
	 * questionnaireStatusCheck
	 * @return true if questionnaire status is equals to questionnaireStatusId
	 */
	public boolean questionnaireStatusCheck(RegulationDetails regulationDetails, int questionnaireStatusId) {
		boolean result = false;
		if (questionnaireStatusId == regulationDetails.getRegulationUserInfo().getRegulationQuestionnaireStatus().getId()) {
			result = true;
		}
		return result;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public RegulationResponse doRegulation(RegulationDetails regulationDetails, UserHistory userHistory) throws Exception {
		logger.info("Regulation process - Start. " + regulationDetails.toString());
		RegulationResponse regulationResponse = null;
		try {
			qmUserAnswerService.insertBatch(regulationDetails.getQmUserAnswerList());
			questionnaireCheck(regulationDetails);
			regulationResponse = userCheck(regulationDetails, userHistory);
		} catch (Exception e) {
			logger.error("Problem with regulation process. ", e);
			throw e;
		}
		logger.info("Regulation process - End.");
		return regulationResponse;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseCode approveUser(User user, UserHistory userHistory) {
		ResponseCode responseCode = ResponseCode.OPERATION_DENIED;
		if (user.getUserStep().getId() == Step.PENDING.getId()) {
			user.setUserStep(new UserStep(Step.APPROVE.getId()));
			userService.updateStep(user, userHistory);
			responseCode = ResponseCode.OK;
		}
		return responseCode;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseCode questionnaireFittingAfterTraining(RegulationDetails regulationDetails, UserHistory userHistory) throws Exception {
		logger.debug("RegulationService.questionnaireFittingAfterTraining - Start.");
		ResponseCode responseCode = ResponseCode.OPERATION_DENIED;
		if (regulationDetails.getRegulationUserInfo().getRegulationQuestionnaireStatus().getId() == RegulationQuestionnaireStatusEnum.UNFITTING.getId()) {
			regulationDetails.getRegulationUserInfo().getRegulationQuestionnaireStatus().setId(RegulationQuestionnaireStatusEnum.FITTING_AFTER_TRAINING.getId());
			regulationUserInfoService.update(regulationDetails.getRegulationUserInfo());
			userCheck(regulationDetails, userHistory);
			responseCode = ResponseCode.OK;
		}
		logger.debug("RegulationService.questionnaireFittingAfterTraining - End.");
		return responseCode;
	}
	
	@Override
	public RegulationDetails getRegulationDetails(long userId) {
		logger.debug("RegulationService.getRegulationDetails - Start.");
		RegulationDetails regulationDetails = new RegulationDetails(null, 
				userService.getUserById(userId), 
				regulationUserInfoService.get(userId));
		logger.debug("RegulationService.getRegulationDetails - End.");
		return regulationDetails;
	}
	
	/******** ODT **********/
	@Override
	@Transactional(rollbackFor = Exception.class)
	public User doRegulationODT(RegulationDetails regulationDetails, UserHistory userHistory) throws Exception {
		logger.info("Regulation process - Start. " + regulationDetails.toString());
		User user = null;
		try {
			qmUserAnswerService.insertBatch(regulationDetails.getQmUserAnswerList());
			questionnaireCheck(regulationDetails);
			user = userCheckODT(regulationDetails, userHistory);
		} catch (Exception e) {
			logger.error("Problem with regulation process. ", e);
			throw e;
		}
		logger.info("Regulation process - End.");
		return user;
	}
	
	/******** ODT **********/
	public User userCheckODT(RegulationDetails regulationDetails, UserHistory userHistory) {
		logger.debug("RegulationService.userCheck - Start. " + regulationDetails.toString());
		UserStep userStep = new UserStep(Step.MISSING.getId());
		if (questionnaireStatusCheck(regulationDetails, RegulationQuestionnaireStatusEnum.FITTING.getId())
				|| questionnaireStatusCheck(regulationDetails, RegulationQuestionnaireStatusEnum.FITTING_AFTER_TRAINING.getId())) {
			userStep = new UserStep(Step.PENDING.getId());
		}
		regulationDetails.getUser().setUserStep(userStep);
		userService.updateStep(regulationDetails.getUser(), userHistory);
		logger.debug("RegulationService.userCheck - End.");
		return regulationDetails.getUser();
	}
	
	/******** ODT **********/
	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseCode questionnaireFittingAfterTrainingODT(RegulationDetails regulationDetails, UserHistory userHistory) throws Exception {
		logger.debug("RegulationService.questionnaireFittingAfterTrainingODT - Start.");
		ResponseCode responseCode = ResponseCode.OPERATION_DENIED;
		if (questionnaireStatusCheck(regulationDetails, RegulationQuestionnaireStatusEnum.UNFITTING.getId()) ||
				questionnaireStatusCheck(regulationDetails, RegulationQuestionnaireStatusEnum.UNFITTING_RESTRICTED.getId())) {
			regulationDetails.getRegulationUserInfo().getRegulationQuestionnaireStatus().setId(RegulationQuestionnaireStatusEnum.FITTING_AFTER_TRAINING.getId());
			regulationUserInfoService.update(regulationDetails.getRegulationUserInfo());
			userCheckODT(regulationDetails, userHistory);
			responseCode = ResponseCode.OK;
		}
		logger.debug("RegulationService.questionnaireFittingAfterTrainingODT - End.");
		return responseCode;
	}
}
