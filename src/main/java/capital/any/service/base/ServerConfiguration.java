package capital.any.service.base;

import javax.annotation.PostConstruct;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import capital.any.service.IUtilService;

/**
 * @author LioR SoLoMoN
 *
 */
@Service
public class ServerConfiguration implements IServerConfiguration {
	private static final Logger logger = LoggerFactory.getLogger(ServerConfiguration.class);
	private static final String SERVER_NAME_COMMAND = "hostname";
	private static String GEO_IP_LOOKUP_COMMAND = "geoiplookup ";
	private static String CHECK_MX_COMMAND = "checkmx ";
	private static final String SERVER_NAME_NOT_APPLICABLE = "N/A";
	private static final String SERVER_NAME_PREFIX = "SNID";

	@Autowired
	private IUtilService utilService;
	private String serverName;
	
	@PostConstruct
	private void init() {
		serverName = utilService.doUnixCommand(SERVER_NAME_COMMAND);
		if (utilService.isArgumentEmptyOrNull(serverName)) {
			serverName = SERVER_NAME_NOT_APPLICABLE;
		}
		logger.info(SERVER_NAME_PREFIX + ": " + serverName);
	}

	@Override
	public String getServerName() {
		return this.serverName;
	}

	@Override
	public String[] getGeoLocation(String ip) {
		String[] geoLocationFields = null;
		try {
			String geographicLocation = utilService
					.doUnixCommand((new StringBuilder(GEO_IP_LOOKUP_COMMAND)).append(ip).toString());
			logger.info("after unix geo city ip " + ip + " lookup command, res: " + geographicLocation);			
			
			if (!utilService.isArgumentEmptyOrNull(geographicLocation)
					&& !geographicLocation.toLowerCase().contains("ip address not found") 
					&& geographicLocation.contains(": ")) {
				String[] locationSplit = geographicLocation.split(": ");
				if (locationSplit.length > 1 && locationSplit[1].contains(",")) {
					geoLocationFields = locationSplit[1].split(", ");	
				}				
			}		
		} catch (Exception e) {
			logger.error("ERROR! getGeoLocationFields " , e);
		}
		
		return geoLocationFields;	
	}

	@Override
	public boolean isValidEmail(String email) {
		boolean isValidEmail = false;
		InternetAddress emailAddr;
		if (!utilService.isArgumentEmptyOrNull(email)) {
			try {
				emailAddr = new InternetAddress(email);
				emailAddr.validate();
				String domain = email.split("@")[1];
				String response = utilService.doUnixCommand((new StringBuilder(CHECK_MX_COMMAND)).append(domain).toString());
				logger.info("isValidEmail; email: " + email + " ; domain: " + domain + " ; response: " + response
						+ " response.equals(\"1\") = "  + response.equals("1\n"));
				if (!utilService.isArgumentEmptyOrNull(response)
						&& response.equals("1\n")) {
					isValidEmail = true;
				}
			} catch (AddressException e) {
				logger.info("isValidEmail; " + email + " not valid email by javax.mail.internet.InternetAddress"); 
			}
		} 
		return isValidEmail;
	}
}
