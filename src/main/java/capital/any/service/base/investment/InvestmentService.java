package capital.any.service.base.investment;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import capital.any.base.enums.ActionSource;
import capital.any.base.enums.DBParameter;
import capital.any.base.enums.EmailAction;
import capital.any.base.enums.InvestmentRejectType;
import capital.any.base.enums.InvestmentStatusEnum;
import capital.any.base.enums.InvestmentTypeEnum;
import capital.any.base.enums.ProductStatusEnum;
import capital.any.communication.Error;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.dao.base.investment.IInvestmentDao;
import capital.any.model.base.BalanceHistory.BalanceHistoryCommand;
import capital.any.model.base.BalanceRequest;
import capital.any.model.base.BalanceRequest.OperationType;
import capital.any.model.base.EmailActionRequest;
import capital.any.model.base.Investment;
import capital.any.model.base.InvestmentEmailAction;
import capital.any.model.base.InvestmentHistory;
import capital.any.model.base.InvestmentMarket;
import capital.any.model.base.InvestmentParamters;
import capital.any.model.base.InvestmentReject;
import capital.any.model.base.InvestmentStatus;
import capital.any.model.base.MyNote;
import capital.any.model.base.Product;
import capital.any.model.base.ProductMarket;
import capital.any.model.base.SellNowRequest;
import capital.any.model.base.SqlFilters;
import capital.any.model.base.User;
import capital.any.model.base.UserControllerDetails;
import capital.any.service.base.IBalanceService;
import capital.any.service.base.IServerConfiguration;
import capital.any.service.base.currency.ICurrencyService;
import capital.any.service.base.dbParameter.IDBParameterService;
import capital.any.service.base.emailAction.IEmailActionService;
import capital.any.service.base.investmentHistory.IInvestmentHistoryService;
import capital.any.service.base.investmentMarkets.IInvestmentMarketService;
import capital.any.service.base.investmentReject.IInvestmentRejectService;
import capital.any.service.base.product.IProductService;
import capital.any.service.base.user.IUserService;

/**
 * @author Eyal G
 *
 */
@Service
public class InvestmentService implements IInvestmentService {
	
	private static final Logger logger = LoggerFactory.getLogger(InvestmentService.class);
	
	@Autowired
	private IInvestmentDao investmentDao;
	
	@Autowired
	private IInvestmentMarketService investmentMarketService;
	
	@Autowired
	private IInvestmentHistoryService investmentHistoryService;
	
	@Autowired
	protected IServerConfiguration serverConfiguration;
	
	@Autowired
	protected IProductService productService;
	
	@Autowired
	protected IBalanceService balanceService;
	
	@Autowired
	private IDBParameterService dbParameterService;
	
	@Autowired
	private IInvestmentRejectService investmentRejectService;
	
	@Autowired
	private ICurrencyService currencyService;
	
	@Autowired
	private IUserService userService;
	
	@Autowired
	private IEmailActionService emailActionService;

	private void insert(Investment investment) {
		investmentDao.insert(investment);		
	}

	@Override
	public List<Investment> get(SqlFilters filters) {
		return investmentDao.get(filters);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Response<Investment> insertWithValidation(InvestmentParamters investmentParamters) throws Exception {
		Investment investment = investmentParamters.getInvestment();
		investment.setUser(userService.getUserById(investment.getUser().getId())); //load user from db
		investment.setRate(currencyService.getRate(investment.getUser().getCurrencyId()));
		investment.setTimeCreated(new Date());
		investment.setInvestmentStatus(new InvestmentStatus(InvestmentStatusEnum.OPEN.getId()));
		logger.debug("try to insert investment " + investment);
		Product product = productService.getProduct(investment.getProductId());
		//in euro
		BigDecimal baseAmount = new BigDecimal(String.valueOf(investment.getOriginalAmount()))
				.divide(new BigDecimal(String.valueOf(currencyService.getRate(product.getCurrencyId()))), 0, RoundingMode.HALF_UP);
		//TODO convert to user currency Currently we have only euro
		investment.setAmount(baseAmount.longValue());		
		Response<Investment> response = validateInsertInvestment(investmentParamters, product);
		if (ResponseCode.OK == response.getResponseCode()) {
			insert(investment);
			investment.setInvestmentMarkets(new ArrayList<InvestmentMarket>());
			InvestmentMarket investmentMarket = null;
			for (ProductMarket productMarket : product.getProductMarkets()) {
				investmentMarket = new InvestmentMarket();
				investmentMarket.setMarketId(productMarket.getMarket().getId());
				investmentMarket.setPrice(productMarket.getMarket().getLastPrice().getPrice());
				investmentMarket.setInvestmentId(investment.getId());
				investment.getInvestmentMarkets().add(investmentMarket);
			}
			investmentMarketService.insert(investment.getInvestmentMarkets());
			int emailActionId = EmailAction.INSERT_INVESTMENT_PENDING.getId();
			if (investment.getInvestmentStatus().getId() == InvestmentStatusEnum.OPEN.getId()) { //on pending not changing balance
				emailActionId = EmailAction.INSERT_INVESTMENT_OPEN.getId();
				balanceService.changeBalance(
						new BalanceRequest(investment.getUser(), 
								investment.getAmount(),
								BalanceHistoryCommand.INSERT_INVESTMENTS, 
								investment.getId(), 
								OperationType.DEBIT, new UserControllerDetails(
										investment.getInvestmentHistory().getActionSource(),
										investment.getInvestmentHistory().getIp(),
										investment.getInvestmentHistory().getLoginId(),
										investment.getInvestmentHistory().getWriterId())));
			}
			investment.getInvestmentHistory().setInvestmentStatus(investment.getInvestmentStatus());
			investment.getInvestmentHistory().setTimeCreated(investment.getTimeCreated());
			investment.getInvestmentHistory().setInvestmntId(investment.getId());
			investment.getInvestmentHistory().setServerName(serverConfiguration.getServerName());
			investmentHistoryService.insert(investment.getInvestmentHistory());
			try {
				InvestmentEmailAction investmentEmailAction = new InvestmentEmailAction(investment, product);
				EmailActionRequest<Object> emailActionRequest = new EmailActionRequest<Object>(investment.getUser(), new capital.any.model.base.EmailAction(emailActionId), investmentEmailAction);
				emailActionRequest.setActionSource(investment.getInvestmentHistory().getActionSource());
				emailActionService.insertEmailByAction(emailActionRequest);
			} catch (Exception e) {
				logger.error("Problem with insert email.", e);
			}
		}
		return response;
	}

	@Override
	public List<Investment> getByUserId(SqlFilters filters) {
		List<Investment> investments = investmentDao.getByUserId(filters);
		for (Investment investment : investments) {			
			investment.setInvestmentMarkets(investmentMarketService.get(investment.getId()));
		}
		return investments;
	}
	
	@Override
	public List<Map<String, Object>> getByUserId(long userId, boolean isOpenInv, List<Integer> statuses) {
		Map<Long, List<MyNote>> productInvestments = new LinkedHashMap<Long, List<MyNote>>(); 
		List<MyNote> MyNotes = investmentDao.getByUserId(userId, isOpenInv, statuses);
		Product product = null;
		for (MyNote myNote : MyNotes) {
			if (isOpenInv) {
				product = productService.getProduct(myNote.getInvestment().getProductId());
				if (product.getProductStatus().getId() > ProductStatusEnum.SUBSCRIPTION.getId()) {
					myNote.setInvestment(calculateReturn(myNote.getInvestment(), product));
				}
			}
			myNote.getInvestment().setInvestmentMarkets(investmentMarketService.get(myNote.getInvestment().getId()));
			List<MyNote> list = productInvestments.get(myNote.getInvestment().getProductId());
			if (list == null) {
				list = new ArrayList<MyNote>();
			} 
			list.add(myNote);
			productInvestments.put(myNote.getInvestment().getProductId(), list);
		}
		
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		productInvestments.forEach((k,v) -> {
			Map<String, Object> prodInv = new HashMap<String, Object>();
			prodInv.put("product", productService.getProduct(k));
			prodInv.put("myNotes", v);
			result.add(prodInv);
		});		
		return result;
	}
	
	private Response<Investment> validateInsertInvestment(InvestmentParamters investmentParamters, Product product) {
		Investment investment = investmentParamters.getInvestment();
		InvestmentReject investmentReject = new InvestmentReject(); 
		investmentReject.setActionSource(investment.getInvestmentHistory().getActionSource());
		investmentReject.setAmount(investment.getAmount());
		investmentReject.setAsk(investment.getAsk());
		investmentReject.setBid(investment.getBid());		
		investmentReject.setInvestmentType(InvestmentTypeEnum.get((investment.getInvestmentType().getId())));
		investmentReject.setProductId(investment.getProductId());
		investmentReject.setRate(investment.getRate());
		investmentReject.setSessionId(investmentParamters.getSessionId());
		investmentReject.setUserId(investment.getUser().getId());
		investmentReject.setWriterId(investment.getInvestmentHistory().getWriterId());
		
		long minInvAmount = dbParameterService.get(DBParameter.MIN_INV.getId()).getNumValue();
		Response<Investment> response = new Response<Investment>(investment, ResponseCode.OK);
		
		//validate user regulated
		if (investment.getUser().getUserStep().getId() != User.Step.APPROVE.getId()) {
			investmentReject.setRejectAdditionalInfo("user step id:" + investment.getUser().getUserStep().getId());
			investmentReject.setInvestmentRejectType(InvestmentRejectType.USER_NOT_REGULATED);
			response = new Response<Investment>(investment, ResponseCode.INVESTMENT_ERROR_USER_NOT_REGULATED, null, new Error("investment-purchase-message-user-not-regulated", null));
			investmentRejectService.insert(investmentReject);
			return response;
		}
		
		//check user balance
		response = validateUserBalance(investment, investmentReject, response, minInvAmount);
		if (response.getResponseCode() != ResponseCode.OK) {
			return response;			
		}
		
		if (minInvAmount > investment.getAmount()) {
			investmentReject.setRejectAdditionalInfo("min inv amount:" + minInvAmount);
			investmentReject.setInvestmentRejectType(InvestmentRejectType.MIN_INV_LIMIT);
			response = new Response<Investment>(investment, ResponseCode.INVESTMENT_ERROR_MIN_INV_LIMIT, null, new Error("investment-purchase-message-below-minimum", null));
			investmentRejectService.insert(investmentReject);
			return response;
		}
		
		long maxInvAmount = dbParameterService.get(DBParameter.MAX_INV.getId()).getNumValue();
		if (maxInvAmount < investment.getAmount()) {
			investmentReject.setRejectAdditionalInfo("max inv amount:" + maxInvAmount);
			investmentReject.setInvestmentRejectType(InvestmentRejectType.MAX_INV_LIMIT);
			response = new Response<Investment>(investment, ResponseCode.INVESTMENT_ERROR_MAX_INV_LIMIT, null, new Error("investment-purchase-message-above-maximum", null));
			investmentRejectService.insert(investmentReject);
			return response;
		}
		
		if (product.getProductStatus().getId() != ProductStatusEnum.SUBSCRIPTION.getId() && product.getProductStatus().getId() != ProductStatusEnum.SECONDARY.getId()) {
			investmentReject.setInvestmentRejectType(InvestmentRejectType.PRODUCT_IS_CURRENTLY_UNAVAILABLE);
			response = new Response<Investment>(investment, ResponseCode.INVESTMENT_ERROR_PRODUCT_IS_CURRENTLY_UNAVAILABLE, null, new Error("investment-purchase-message-unavailable", null));
			investmentRejectService.insert(investmentReject);
		}
		
		//check according to investment type that we are in the right time and status (secondary must have bid/ask)
		if (investment.getInvestmentType().getId() == InvestmentTypeEnum.SUBSCRIPTION.getId() &&
				!(product.getSubscriptionStartDate().before(investment.getTimeCreated()) &&
						product.getSubscriptionEndDate().after(investment.getTimeCreated()) &&
							product.getProductStatus().getId() == ProductStatusEnum.SUBSCRIPTION.getId())) {
			investmentReject.setInvestmentRejectType(InvestmentRejectType.INVESTMENT_PRAIMERY_PRODUCT_SECONDARY);
			response = new Response<Investment>(investment, ResponseCode.INVESTMENT_ERROR_INVESTMENT_PRAIMERY_PRODUCT_SECONDARY, null, new Error("investment-purchase-message-already-issued", null));
			investmentRejectService.insert(investmentReject);
			return response;
		}
		
		if (investment.getInvestmentType().getId() == InvestmentTypeEnum.SECONDARY.getId() &&
				!(product.getSubscriptionEndDate().before(investment.getTimeCreated()) &&
						product.getLastTradingDate().after(investment.getTimeCreated()) &&
							product.getProductStatus().getId() == ProductStatusEnum.SECONDARY.getId())) {
			investmentReject.setInvestmentRejectType(InvestmentRejectType.INVESTMENT_SECONDARY_PRODUCT_PRAIMERY);
			response = new Response<Investment>(investment, ResponseCode.INVESTMENT_ERROR_INVESTMENT_SECONDARY_PRODUCT_PRAIMERY, null, new Error("investment-purchase-message-deviation", null));
			investmentRejectService.insert(investmentReject);
			return response;
		}
		
		//check if product is suspended
		if (product.isSuspend()) {
			investmentReject.setInvestmentRejectType(InvestmentRejectType.PRODUCT_SUSPENDED);
			response = new Response<Investment>(investment, ResponseCode.INVESTMENT_ERROR_PRODUCT_SUSPENDED, null, new Error("investment-purchase-message-suspended", null));
			investmentRejectService.insert(investmentReject);
			return response;
		}		
		
		if (investment.getInvestmentType().getId() == InvestmentTypeEnum.SECONDARY.getId() && (investment.getAsk() != product.getAsk() || product.getAsk() == 0)) {
			investmentReject.setRejectAdditionalInfo("product ask:" + product.getAsk());
			investmentReject.setInvestmentRejectType(InvestmentRejectType.DEVIATION_ASK);
			response = new Response<Investment>(investment, ResponseCode.INVESTMENT_ERROR_DEVIATION_ASK, null, new Error("investment-purchase-message-deviation", null));
			investmentRejectService.insert(investmentReject);
			return response;
		}
		
		//check user balance again must be last validation
		response = validateUserBalance(investment, investmentReject, response, minInvAmount);
		
		return response;
	}
	
	private Response<Investment> validateUserBalance(Investment investment, InvestmentReject investmentReject, Response<Investment> response, long minInvAmount) {
		long userCurrentBalance = balanceService.getBalance(investment.getUser().getId());
		if (userCurrentBalance < investment.getAmount()) {
			if (userCurrentBalance >= minInvAmount) {
				investmentReject.setRejectAdditionalInfo("min amount:" + minInvAmount + ", user balance:" + userCurrentBalance);
				investmentReject.setInvestmentRejectType(InvestmentRejectType.INSUFFICIENT_BALANCE_MORE_THEN_MIN_INV);
				response = new Response<Investment>(investment, ResponseCode.INVESTMENT_ERROR_INSUFFICIENT_BALANCE_MORE_THEN_MIN_INV, null, new Error("investment.error.insufficient.balance.more.then.min.inv", null)); 
				investmentRejectService.insert(investmentReject);
				return response;
			}
			if (investment.getInvestmentType().getId() == InvestmentTypeEnum.SECONDARY.getId()) {
				investmentReject.setRejectAdditionalInfo("min amount:" + minInvAmount + ", user balance:" + userCurrentBalance);
				investmentReject.setInvestmentRejectType(InvestmentRejectType.INSUFFICIENT_BALANCE_LESS_THEN_MIN_INV);
				response = new Response<Investment>(investment, ResponseCode.INVESTMENT_ERROR_INSUFFICIENT_BALANCE_LESS_THEN_MIN_INV, null, new Error("investment-purchase-message-below-minimum-secondary", null));
				investmentRejectService.insert(investmentReject);
				return response;
			} else {
				if (investmentDao.isExistPendingInvestment(investment)) {
					investmentReject.setRejectAdditionalInfo("min amount:" + minInvAmount + ", user balance:" + userCurrentBalance);
					investmentReject.setInvestmentRejectType(InvestmentRejectType.LIMIT_OF_ONE_PENDING_INVESTMENT_PER_PRODUCT_IN_PRIMARY);
					response = new Response<Investment>(investment, ResponseCode.INVESTMENT_ERROR_LIMIT_OF_ONE_PENDING_INVESTMENT_PER_PRODUCT_IN_PRIMARY, null, new Error("investment-purchase-message-already-secured", null));
					investmentRejectService.insert(investmentReject);
					return response;
				} else {
					investment.setInvestmentStatus(new InvestmentStatus(InvestmentStatusEnum.PENDING.getId()));
				}
			}
		}
		return new Response<Investment>(investment, ResponseCode.OK);
	}

	@Override
	public List<Investment> getByStatusesAndProductId(long productId, List<Integer> statuses) {
		return investmentDao.getByStatusesAndProductId(productId, statuses);
	}

	@Override
	public void settle(Investment investment, Product product, ActionSource actionSource) throws Exception {
		investment = calculateReturn(investment, product);
		investment.setInvestmentStatus(new InvestmentStatus(InvestmentStatusEnum.SETTLED.getId()));
		InvestmentHistory investmentHistory = investment.getInvestmentHistory();
		if (investmentHistory == null) {
			investmentHistory = new InvestmentHistory();
		}
		investmentHistory.setActionSource(actionSource);
		investmentHistory.setInvestmentStatus(new InvestmentStatus(investment.getInvestmentStatus().getId()));
		investmentHistory.setTimeCreated(new Date());
		investmentHistory.setInvestmntId(investment.getId());
		investmentHistory.setServerName(serverConfiguration.getServerName());
		investment.setInvestmentHistory(investmentHistory);
		balanceService.changeBalance(
				new BalanceRequest(investment.getUser(), 
						investment.getReturnAmount(),
						BalanceHistoryCommand.SETTLE_INVESTMENT, 
						investment.getId(), 
						OperationType.CREDIT, new UserControllerDetails( 
								investment.getInvestmentHistory().getActionSource(),
								investment.getInvestmentHistory().getIp(),
								investment.getInvestmentHistory().getLoginId(),
								investment.getInvestmentHistory().getWriterId())));		
		updateSettle(investment);
	}
	
	protected Investment calculateReturn(Investment investment, Product product) {
		BigDecimal hunderd = new BigDecimal("100");
		BigDecimal amount = investment.getDenomination();
		BigDecimal result;
		if (product.getProductStatus().getId() == ProductStatusEnum.SETTLED.getId()) {
			result = amount.multiply(product.calculateAndGetFormulaResult());
		} else {
			BigDecimal bid = new BigDecimal(String.valueOf(product.getBid())).divide(hunderd);
			result = amount.multiply(bid);
		}
		result = result.setScale(0, RoundingMode.HALF_UP);
		BigDecimal baseresult = result.divide(new BigDecimal(String.valueOf(currencyService.getRate(product.getCurrencyId()))), 0, RoundingMode.HALF_UP);
		investment.setReturnAmount(baseresult.longValue());
		investment.setOriginalReturnAmount(result.longValue());
		return investment;
	}

	@Override
	public boolean updateStatus(Investment investment) {
		logger.debug("update Status for investment id: " + investment.getId() + " new status " + investment.getInvestmentStatus().getId());
		boolean isUpdate = investmentDao.updateStatus(investment);
		if (isUpdate) {
			investmentHistoryService.insert(investment.getInvestmentHistory());
		}
		return isUpdate;
	}

	@Override
	public boolean updateSettle(Investment investment) {
		boolean isUpdate = investmentDao.updateSettle(investment);
		if (isUpdate) {
			investmentHistoryService.insert(investment.getInvestmentHistory());
		}
		return isUpdate;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public Response<List<Investment>> sellNow(SellNowRequest sellNowRequest) throws Exception {
		List<Investment> investments = getInvestmentsToSell(sellNowRequest);
		if (investments == null || investments.size() != sellNowRequest.getInvestmentsId().size()) {
			logger.warn("can't sell now investment/s not open for selling investments id " + sellNowRequest.getInvestmentsId());
			return new Response<List<Investment>>(null, ResponseCode.INVALID_INPUT);
		}
		Product product = productService.getProduct(sellNowRequest.getProduct().getId());
		Response<List<Investment>> response = validateSellNow(product, sellNowRequest.getProduct().getBid());
		if (response == null) {
			for (Investment investment : investments) {
				InvestmentHistory investmentHistory = new InvestmentHistory();
				investmentHistory.setIp(sellNowRequest.getIp());
				investmentHistory.setLoginId(sellNowRequest.getLoginId());
				investment.setInvestmentHistory(investmentHistory);
				settle(investment, product, sellNowRequest.getActionSource());
			}
			response = new Response<List<Investment>>(investments, ResponseCode.OK);
		}
		return response;
	}
	
	private List<Investment> getInvestmentsToSell(SellNowRequest sellNowRequest) {
		return investmentDao.getInvestmentToSellNow(sellNowRequest);
	}

	public Response<List<Investment>> validateSellNow(Product product, double bid) {
		if (!(product.getFirstExchangeTradingDate().before(new Date()) && product.getFinalFixingDate().after(new Date()))) {
			logger.warn("can't sell now product not open for selling " + product.getId());
			return new Response<List<Investment>>(null, ResponseCode.INVALID_INPUT);
		}
		if (product.isSuspend()) {
			logger.warn("can't sell now product suspended ");
			return new Response<List<Investment>>(null, ResponseCode.INVALID_INPUT);
		}
		if (product.getBid() != bid || product.getBid() == 0) {
			logger.warn("can't sell now product bid " + product.getBid() + " !=  page bid " + bid);
			return new Response<List<Investment>>(null, ResponseCode.INVESTMENT_ERROR_SELL_NOW_BID);
		}
		return null;
	}

	@Override
	public List<Investment> getInvestmentsToBuy(long userId) {
		return investmentDao.getInvestmentsToBuy(userId);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void buyPendingInvestments(BalanceRequest balanceRequest) throws Exception {
		logger.debug("try to find pending investments to buy for user id " + balanceRequest.getUser().getId());
		List<Investment> investmentsToBuy = getInvestmentsToBuy(balanceRequest.getUser().getId());
		logger.debug("found " + investmentsToBuy.size() + " investment to buy ");
		for (Investment investment : investmentsToBuy) {
			logger.debug("try to buy investment id " + investment.getId() + " with amonut " + investment.getAmount() + " user balance " + balanceRequest.getUser().getBalance());
			if (investment.getAmount() <= balanceRequest.getUser().getBalance()) {
				investment.setInvestmentStatus(new InvestmentStatus(InvestmentStatusEnum.OPEN.getId()));
				InvestmentHistory investmentHistory = new InvestmentHistory();
				investmentHistory.setActionSource(balanceRequest.getUserControllerDetails().getActionSource());
				investmentHistory.setIp(balanceRequest.getUserControllerDetails().getIp());
				investmentHistory.setLoginId(balanceRequest.getUserControllerDetails().getLoginId());
				investmentHistory.setInvestmentStatus(investment.getInvestmentStatus());
				investmentHistory.setTimeCreated(new Date());
				investmentHistory.setInvestmntId(investment.getId());
				investmentHistory.setServerName(serverConfiguration.getServerName());
				investmentHistory.setWriterId(balanceRequest.getUserControllerDetails().getWriterId());
				investment.setInvestmentHistory(investmentHistory);
				updateStatus(investment);
				balanceService.changeBalance(
						new BalanceRequest(balanceRequest.getUser(), 
								investment.getAmount(),
								BalanceHistoryCommand.INSERT_INVESTMENTS, 
								investment.getId(),
								OperationType.DEBIT));
			}
		};
	}
	
	@Override
	public Investment getInvestmentById(long id) {
		return investmentDao.getInvestmentById(id);
	}
}
