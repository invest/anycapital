package capital.any.service.base.investment;

import java.util.List;
import java.util.Map;
import capital.any.base.enums.ActionSource;
import capital.any.communication.Response;
import capital.any.model.base.BalanceRequest;
import capital.any.model.base.Investment;
import capital.any.model.base.InvestmentParamters;
import capital.any.model.base.Product;
import capital.any.model.base.SellNowRequest;
import capital.any.model.base.SqlFilters;

/**
 * @author @author Eyal G
 *
 */
public interface IInvestmentService {
	

	/**
	 * select all investments
	 * @return List<Investment>
	 */
	List<Investment> get(SqlFilters filters);
	
	/**
	 * insert investment after validation
	 * @param investment Paramters
	 * @return error or null if there is no error
	 * @throws Exception 
	 */
	Response<Investment> insertWithValidation(InvestmentParamters investmentParamters) throws Exception;
	
	/**
	 * select all investments for user
	 * @param userId the user id
	 * @return List<Investment>
	 */
	List<Investment> getByUserId(SqlFilters filters);

	/**
	 * get open or close investments by user id with coupon
	 * @param userId
	 * @param isOpenInv true for open inv else false
	 * @return List<Map<String, Object>>
	 */
	List<Map<String, Object>> getByUserId(long userId, boolean isOpenInv, List<Integer> statuses);
	
	/**
	 * get all investments by statuses and product id
	 * @param productId
	 * @param statuses
	 * @return List<Investment>
	 */
	List<Investment> getByStatusesAndProductId(long productId, List<Integer> statuses);

	/**
	 * settle investment
	 * @param investment
	 * @param product
	 * @throws Exception 
	 */
	void settle(Investment investment, Product product, ActionSource actionSource) throws Exception;
	
	/**
	 * update the investment status and history
	 * @param Investment
	 * @return true if update success else false
	 */
	boolean updateStatus(Investment investment);
	
	/**
	 * update investment to settle and history
	 * @param investment
	 * @return true if update success else false
	 */
	boolean updateSettle(Investment investment);
	
	/**
	 * validate and sell investment now
	 * @param sellNowRequest
	 * @return 
	 * @throws Exception
	 */
	Response<List<Investment>> sellNow(SellNowRequest sellNowRequest) throws Exception;
	
	/**
	 * get pending investments to buy after balance increase
	 * @param userId
	 * @return investment to buy
	 */
	List<Investment> getInvestmentsToBuy(long userId);
	
	/**
	 * buy pending investments for this user
	 * we use this method after balance increase
	 * @param userId
	 * @throws Exception 
	 */
	void buyPendingInvestments(BalanceRequest balanceRequest) throws Exception;

	/**
	 * Get investment by id
	 * @param id
	 * @return
	 */
	Investment getInvestmentById(long id);
}
