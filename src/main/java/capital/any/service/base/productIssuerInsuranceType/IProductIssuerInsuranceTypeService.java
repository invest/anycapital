package capital.any.service.base.productIssuerInsuranceType;

import java.util.HashMap;

import capital.any.model.base.ProductIssuerInsuranceType;

/**
 * @author eranl
 *
 */
public interface IProductIssuerInsuranceTypeService {	
	
	/**
	 * Get Product Issuer Insurance Type hashmap
	 * @return
	 */
	HashMap<Integer, ProductIssuerInsuranceType> getProductIssuerInsuranceType();
}
