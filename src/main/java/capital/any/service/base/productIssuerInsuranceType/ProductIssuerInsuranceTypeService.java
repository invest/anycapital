package capital.any.service.base.productIssuerInsuranceType;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.productIssuerInsuranceType.IProductIssuerInsuranceTypeDao;
import capital.any.model.base.ProductIssuerInsuranceType;

/**
 * @author eranl
 *
 */
@Service
public class ProductIssuerInsuranceTypeService implements IProductIssuerInsuranceTypeService {
	
	@Autowired
	private IProductIssuerInsuranceTypeDao productIssuerInsuranceTypeDao;

	@Override
	public HashMap<Integer, ProductIssuerInsuranceType> getProductIssuerInsuranceType() {
		return productIssuerInsuranceTypeDao.getProductIssuerInsuranceType();
	}

}
