package capital.any.service.base.fileType;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import capital.any.dao.base.fileType.IFileTypeDao;
import capital.any.model.base.FileType;

/**
 * @author Eyal G
 *
 */
@Primary
@Service
public class FileTypeService implements IFileTypeService {
	private static final Logger logger = LoggerFactory.getLogger(FileTypeService.class);
	@Autowired
	private IFileTypeDao fileTypeDao;
	@Autowired
	@Qualifier("FileTypeHCDao")
	private IFileTypeDao fileTypeHCDao;
	
	@Override
	public Map<Integer, FileType> get() {
		Map<Integer, FileType> fileType = fileTypeHCDao.get();
		if (fileType == null || fileType.size() <= 0) {
			logger.info("fileType empty / null in hazelcast, Let's check RDBMS");
			fileType = fileTypeDao.get(); 
		}
		return fileType;
	}	
}
