package capital.any.service.base.investmentHistory;

import java.util.List;

import capital.any.model.base.InvestmentHistory;

/**
 * @author Eyal G
 *
 */
public interface IInvestmentHistoryService {
	
	/**
	 * insert investment history
	 * @param investmentHistory
	 */
	void insert(InvestmentHistory investmentHistory);
	
	/**
	 * select all InvestmentHistory
	 * @return List<InvestmentHistory>
	 */
	List<InvestmentHistory> get();
}
