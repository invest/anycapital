package capital.any.service.base.investmentHistory;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.investmentHistory.IInvestmentHistoryDao;
import capital.any.model.base.InvestmentHistory;

/**
 * @author Eyal G
 *
 */
@Service
public class InvestmentHistoryService implements IInvestmentHistoryService {
	
	@Autowired
	private IInvestmentHistoryDao investmentHistoryDao;

	@Override
	public void insert(InvestmentHistory investmentHistory) {
		investmentHistoryDao.insert(investmentHistory);		
	}

	@Override
	public List<InvestmentHistory> get() {
		return investmentHistoryDao.get();
	}

}
