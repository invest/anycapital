package capital.any.service.base.transactionOperation;

import java.util.Map;
import capital.any.model.base.TransactionOperation;

/**
 * @author LioR SoLoMoN
 *
 */
public interface ITransactionOperationService {	
	
	/**
	 * Get transaction status map
	 * @return
	 */
	Map<Integer, TransactionOperation> get();
}
