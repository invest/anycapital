package capital.any.service.base.transactionOperation;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import capital.any.dao.base.transactionOperation.ITransactionOperationDao;
import capital.any.model.base.TransactionOperation;

/**
 * @author LioR SoLoMoN
 *
 */
@Primary
@Service
public class TransactionOperationService implements ITransactionOperationService {
	private static final Logger logger = LoggerFactory.getLogger(TransactionOperationService.class);
	@Autowired
	private ITransactionOperationDao transactionOperationDao;
	@Autowired
	@Qualifier("TransactionOperationHCDao")
	private ITransactionOperationDao transactionOperationHCDao;
	
	@Override
	public Map<Integer, TransactionOperation> get() {
		Map<Integer, TransactionOperation> transactionOperation = transactionOperationHCDao.get();
		if (transactionOperation == null || transactionOperation.size() <= 0) {
			logger.info("TransactionOperation empty / null in hazelcast, Let's check RDBMS");
			transactionOperation = transactionOperationDao.get(); 
		}
		return transactionOperation;
	}
}
