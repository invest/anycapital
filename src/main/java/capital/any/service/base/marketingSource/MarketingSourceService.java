package capital.any.service.base.marketingSource;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.marketingSource.IMarketingSourceDao;
import capital.any.model.base.MarketingSource;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service
public class MarketingSourceService implements IMarketingSourceService {

	@Autowired
	private IMarketingSourceDao marketingSourceDao;
	
	@Override
	public void insert(MarketingSource marketingSource) {
		marketingSourceDao.insert(marketingSource);
	}

	@Override
	public void update(MarketingSource marketingSource) {
		marketingSourceDao.update(marketingSource);
	}

	@Override
	public Map<Integer, MarketingSource> getMarketingSources() {
		Map<Integer, MarketingSource> marketingSources = marketingSourceDao.getMarketingSources();
		return marketingSources;
	}

	@Override
	public MarketingSource getMarketingSource(int id) {
		MarketingSource marketingSource = marketingSourceDao.getMarketingSource(id);
		return marketingSource;
		
	}

}
