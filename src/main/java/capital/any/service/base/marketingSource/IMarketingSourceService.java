package capital.any.service.base.marketingSource;

import java.util.Map;

import capital.any.model.base.MarketingSource;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IMarketingSourceService {

	/**
	 * Insert marketing source 
	 * @param marketingSource
	 */
	void insert(MarketingSource marketingSource);
	
	/**
	 * Update marketing source
	 * @param marketingSource
	 */
	void update(MarketingSource marketingSource);
	
	/**
	 * Get marketing sources
	 * @return
	 */
	Map<Integer, MarketingSource> getMarketingSources();
	
	/**
	 * Get marketing source
	 * @param id
	 * @return MarketingSource
	 */
	MarketingSource getMarketingSource(int id);
}
