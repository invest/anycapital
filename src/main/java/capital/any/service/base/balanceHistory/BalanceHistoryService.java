package capital.any.service.base.balanceHistory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import capital.any.dao.base.balanceHistory.IBalanceHistoryDao;

/**
 * @author LioR SoLoMoN
 *
 */
@Service
public class BalanceHistoryService implements IBalanceHistoryService {
	private static final Logger logger = LoggerFactory.getLogger(BalanceHistoryService.class);
	@Autowired
	private IBalanceHistoryDao balanceHistoryDao;


}