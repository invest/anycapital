package capital.any.service.base.transactionPaymentType;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import capital.any.dao.base.transactionPaymentTypes.ITransactionPaymentTypeDao;
import capital.any.model.base.TransactionPaymentType;

/**
 * @author LioR SoLoMoN
 *
 */
@Primary
@Service
public class TransactionPaymentTypeService implements ITransactionPaymentTypeService {
	private static final Logger logger = LoggerFactory.getLogger(TransactionPaymentTypeService.class);
	@Autowired
	private ITransactionPaymentTypeDao transactionPaymentTypeDao;
	@Autowired
	@Qualifier("TransactionPaymentTypeHCDao")
	private ITransactionPaymentTypeDao transactionPaymentTypeHCDao;
	
	@Override
	public Map<Integer, TransactionPaymentType> get() {
		Map<Integer, TransactionPaymentType> transactionPaymentTypes = transactionPaymentTypeHCDao.get();
		if (transactionPaymentTypes == null || transactionPaymentTypes.size() <= 0) {
			logger.info("transactionPaymentTypes empty / null in hazelcast, Let's check RDBMS");
			transactionPaymentTypes = transactionPaymentTypeDao.get(); 
		}
		return transactionPaymentTypes;
	}
}
