package capital.any.service.base.transactionPaymentType;

import java.util.Map;
import capital.any.model.base.TransactionPaymentType;

/**
 * @author LioR SoLoMoN
 *
 */
public interface ITransactionPaymentTypeService {	
	
	/**
	 * Get transaction type map
	 * @return
	 */
	Map<Integer, TransactionPaymentType> get();
}
