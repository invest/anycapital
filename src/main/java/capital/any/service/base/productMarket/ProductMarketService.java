package capital.any.service.base.productMarket;

import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import capital.any.dao.base.productMarket.IProductMarketDao;
import capital.any.model.base.ProductMarket;

/**
 * @author eranl
 *
 */
@Primary
@Service
public class ProductMarketService implements IProductMarketService {
	private static final Logger logger = LoggerFactory.getLogger(ProductMarketService.class);
	@Autowired
	private IProductMarketDao productMarketDao;
	@Autowired
	@Qualifier("ProductMarketHCDao")
	private IProductMarketDao productMarketHCDao;
	
	@Override
	public Map<Long, ProductMarket> get() {
		Map<Long, ProductMarket> productMarkets = productMarketHCDao.get();
		if (productMarkets == null || productMarkets.size() <= 0) {
			logger.info("productMarkets empty / null in hazelcast, Let's check RDBMS");
			productMarkets = productMarketDao.get(); 
		}
		return productMarkets;
	}

	@Override
	public List<ProductMarket> getProductMarkets(long productId) {
		return productMarketDao.getProductMarkets(productId);
	}
}
