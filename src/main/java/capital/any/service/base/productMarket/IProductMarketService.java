package capital.any.service.base.productMarket;

import java.util.List;
import java.util.Map;
import capital.any.model.base.ProductMarket;

/**
 * @author eranl
 *
 */
public interface IProductMarketService {	
	
	/**
	 * Get Product Markets hashmap
	 * @return
	 */
	Map<Long, ProductMarket> get();
	
	List<ProductMarket> getProductMarkets(long productId);
}
