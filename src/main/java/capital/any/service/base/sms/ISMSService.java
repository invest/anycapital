package capital.any.service.base.sms;

import java.util.Map;

import capital.any.model.base.SMS;
import capital.any.model.base.SMSProvider;

/**
 * @author eran.levy
 *
 */
public interface ISMSService {
	
	/**
	 * Insert SMS
	 * @param Sms
	 */
	void insert(SMS sms);
	
	/**
	 * Update SMS
	 * @param Sms
	 */
	void update(SMS sms);
	
	/**
	 * Get SMS providers
	 * @return
	 */
	Map<Integer, SMSProvider> getSMSProviders();
	
	/**
	 * Get SMS by ID
	 * @param id
	 * @return
	 */
	SMS get(long id);
	
}
