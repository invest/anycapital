package capital.any.service.base.sms;

import java.util.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.sms.ISMSDao;
import capital.any.model.base.SMS;
import capital.any.model.base.SMS.Status;
import capital.any.model.base.SMS.Type;
import capital.any.model.base.SMSProvider;

/**
 * @author eran.levy
 *
 */
@Service
public class SMSService implements ISMSService {
	private static final Logger logger = LoggerFactory.getLogger(SMSService.class);
	
	@Autowired
	private ISMSDao SMSDao;


	@Override
	public void insert(SMS sms) {
		sms.setType(Type.FREE_TEXT);
		sms.setSender("anycapital");
		sms.setStatus(Status.NOT_VERIFIED);
		sms.setSenderNumber("");		
		if (sms.getTimeScheduled() == null) {
			sms.setTimeScheduled(new Date());
		}		
		logger.info("Going to insert to SMS table:" + sms);
		SMSDao.insert(sms);		
	}
	
	@Override
	public void update(SMS sms) {
		logger.info("Going to update to SMS table:" + sms);
		SMSDao.update(sms);	
	}
	
	@Override
	public Map<Integer, SMSProvider> getSMSProviders() { 
		return SMSDao.getSMSProviders(); 
	}

	@Override
	public SMS get(long id) {
		return SMSDao.get(id);
	}
	
}
