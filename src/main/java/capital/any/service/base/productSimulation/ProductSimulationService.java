package capital.any.service.base.productSimulation;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.productSimulation.IProductSimulationDao;
import capital.any.model.base.ProductSimulation;

/**
 * @author eranl
 *
 */
@Service
public class ProductSimulationService implements IProductSimulationService {
	
	@Autowired
	private IProductSimulationDao productSimulationDao;

	@Override
	public HashMap<Long, List<ProductSimulation>> getProductSimulations() {
		return productSimulationDao.getProductSimulations();
	}

	@Override
	public List<ProductSimulation> getProductSimulations(long productId) {
		return productSimulationDao.getProductSimulations(productId);
	}

}
