package capital.any.service.base.productSimulation;

import java.util.HashMap;
import java.util.List;

import capital.any.model.base.ProductSimulation;

/**
 * @author eranl
 *
 */
public interface IProductSimulationService {	
	
	/**
	 * Get Product Simulations hashmap
	 * @return
	 */
	HashMap<Long, List<ProductSimulation>> getProductSimulations();
	
	List<ProductSimulation> getProductSimulations(long productId);
}
