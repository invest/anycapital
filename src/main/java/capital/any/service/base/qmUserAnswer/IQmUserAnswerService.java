package capital.any.service.base.qmUserAnswer;

import java.util.List;
import java.util.Map;

import capital.any.model.base.QmQuestion;
import capital.any.model.base.QmUserAnswer;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IQmUserAnswerService {

	/**
	 * Insert QmUserAnswer
	 * @param qmUserAnswer
	 */
	void insertBatch(List<QmUserAnswer> qmUserAnswerList);

	Map<Integer, QmQuestion> get(long userId);

	long getSumScore(List<QmUserAnswer> qmUserAnswerList);

}
