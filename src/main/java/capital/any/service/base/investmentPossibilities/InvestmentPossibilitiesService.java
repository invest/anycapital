package capital.any.service.base.investmentPossibilities;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import capital.any.dao.base.investmentPossibilities.IInvestmentPossibilitiesDao;
import capital.any.model.base.InvestmentPossibility;

/**
 * @author Eyal G
 *
 */
@Service
public class InvestmentPossibilitiesService implements IInvestmentPossibilitiesService {
	
	private static final Logger logger = LoggerFactory.getLogger(InvestmentPossibilitiesService.class);
	
	@Autowired
	private IInvestmentPossibilitiesDao investmentPossibilitiesDao;
	
	@Autowired
	@Qualifier("InvestmentPossibilitiesDaoHCDao")
	private IInvestmentPossibilitiesDao investmentPossibilitiesDaoHCDao;

	
	@Override
	public Map<Integer, InvestmentPossibility> get() {
		Map<Integer, InvestmentPossibility> investmentPossibilities = investmentPossibilitiesDaoHCDao.get();
		if (investmentPossibilities == null || investmentPossibilities.size() <= 0) {
			logger.info("investmentPossibilities empty / null in hazelcast, Let's check RDBMS");
			investmentPossibilities = investmentPossibilitiesDao.get();
		}
		return investmentPossibilities;
	}


	@Override
	public InvestmentPossibility get(int currencyId) {
		return get().get(currencyId);
	}
}
