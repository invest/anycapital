package capital.any.service.base.investmentPossibilities;

import java.util.Map;

import capital.any.model.base.InvestmentPossibility;

/**
 * @author @author Eyal G
 *
 */
public interface IInvestmentPossibilitiesService {
	
	/**
	 * get investment Possibilities with investment Possibility tabs
	 * @return Map<Integer, InvestmentPossibility> currency and his investment Possibilities
	 */
	Map<Integer, InvestmentPossibility> get();
	
	/**
	 * get investment Possibility with investment Possibility tabs
	 * @param currencyId
	 * @return {@link InvestmentPossibility}
	 */
	InvestmentPossibility get(int currencyId);
}
