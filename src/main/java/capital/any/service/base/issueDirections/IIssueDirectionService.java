package capital.any.service.base.issueDirections;

import java.util.Map;

import capital.any.model.base.IssueDirection;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IIssueDirectionService {

	Map<Integer, IssueDirection> get();

}
