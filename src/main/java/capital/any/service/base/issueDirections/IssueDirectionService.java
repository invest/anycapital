package capital.any.service.base.issueDirections;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import capital.any.dao.base.issueDirection.IIssueDirectionDao;
import capital.any.model.base.IssueDirection;

/**
 * 
 * @author eyal.ohana
 *
 */
@Primary
@Service
public class IssueDirectionService implements IIssueDirectionService {
	private static final Logger logger = LoggerFactory.getLogger(IssueDirectionService.class);
			
	@Autowired
	private IIssueDirectionDao issueDirectionDao;
	@Autowired
	@Qualifier("IssueDirectionHCDao")
	private IIssueDirectionDao issueDirectionHCDao;
	
	@Override
	public Map<Integer, IssueDirection> get() {
		Map<Integer, IssueDirection> issueDirections = issueDirectionHCDao.get();
		if (issueDirections == null || issueDirections.size() <= 0) {
			logger.info("issueDirections empty / null in hazelcast, Let's check RDBMS");
			issueDirections = issueDirectionDao.get();
		}
		return issueDirections;
	}

}
