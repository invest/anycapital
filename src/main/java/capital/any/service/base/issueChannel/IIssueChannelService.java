package capital.any.service.base.issueChannel;

import java.util.Map;

import capital.any.model.base.IssueChannel;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IIssueChannelService {

	Map<Integer, IssueChannel> get();

}
