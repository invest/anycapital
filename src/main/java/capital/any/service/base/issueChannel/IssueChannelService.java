package capital.any.service.base.issueChannel;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import capital.any.dao.base.issueChannel.IIssueChannelDao;
import capital.any.model.base.IssueChannel;

/**
 * 
 * @author eyal.ohana
 *
 */
@Primary
@Service
public class IssueChannelService implements IIssueChannelService {
	private static final Logger logger = LoggerFactory.getLogger(IssueChannelService.class);
			
	@Autowired
	private IIssueChannelDao issueChannelDao;
	@Autowired
	@Qualifier("IssueChannelHCDao")
	private IIssueChannelDao issueChannelHCDao;
	
	@Override
	public Map<Integer, IssueChannel> get() {
		Map<Integer, IssueChannel> issueChannels = issueChannelHCDao.get();
		if (issueChannels == null || issueChannels.size() <= 0) {
			logger.info("issueChannels empty / null in hazelcast, Let's check RDBMS");
			issueChannels = issueChannelDao.get();
		}
		return issueChannels;
	}

}
