package capital.any.service.base.marketingTracking;

import java.util.Map;

import capital.any.model.base.MarketingTracking;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IMarketingTrackingService {

	/**
	 * Insert marketing tracking 
	 * @param marketingTracking
	 */
	void insert(MarketingTracking marketingTracking);
	
	/**
	 * Parse map into marketing tracking
	 * @param hm
	 */
	MarketingTracking getMarketingTrackingByMap(Map<String, String> mapRequestParams, Map<String, String> mapCookies);
}
