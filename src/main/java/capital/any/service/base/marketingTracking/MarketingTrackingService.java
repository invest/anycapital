package capital.any.service.base.marketingTracking;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import capital.any.dao.base.marketingTracking.IMarketingTrackingDao;
import capital.any.model.base.MarketingTracking;
import capital.any.service.IUtilService;
import capital.any.service.base.cookie.ICookieService;
import capital.any.service.base.marketingCampaign.IMarketingCampaignService;

/**
 * 
 * @author eyal.ohana
 *
 */
@Primary
@Service
public class MarketingTrackingService implements IMarketingTrackingService {
	private static final Logger logger = LoggerFactory.getLogger(MarketingTrackingService.class);

	@Autowired
	private IMarketingTrackingDao marketingTrackingDao;
	@Autowired
	private IUtilService utilService;
	
	@Override
	public void insert(MarketingTracking marketingTracking) {
		marketingTrackingDao.insert(marketingTracking);
	}

	@Override
	public MarketingTracking getMarketingTrackingByMap(Map<String, String> mapRequestParams, Map<String, String> mapCookies) {
		//TODO create default campaign !!!
		//Session Info
		long campaignId = 1;
		String queryString = IUtilService.EMPTY_STRING;
		if (null != mapRequestParams) {
			String campaign = mapRequestParams.get(IMarketingCampaignService.CAMPAIGN_ID);
			try {
				campaignId = Long.valueOf(campaign);
			} catch (Exception e) {
				logger.warn("Can't parse CAMPAIGN_ID: " + campaign);
			}
			queryString = mapRequestParams.toString();
		}
		//Cookie Info
		String abName = IUtilService.EMPTY_STRING;
		String abValue = IUtilService.EMPTY_STRING;
		if (null != mapCookies) {
			String cookieValue = mapCookies.get(ICookieService.GTM_AB);
			if (!utilService.isArgumentEmptyOrNull(cookieValue) && cookieValue.contains(ICookieService.COLON)) {
				String[] gtmAb = cookieValue.split(ICookieService.COLON);
				if (null != gtmAb && gtmAb.length >= 2) {
					abName = gtmAb[0];
					abValue = gtmAb[1];
				}
			}
		}
		// Marketing Tracking
		MarketingTracking marketingTracking = new MarketingTracking();
		marketingTracking.setCampaignId(campaignId);
		marketingTracking.setQueryString(queryString);
		marketingTracking.setAbName(abName);
		marketingTracking.setAbValue(abValue);
		return marketingTracking;
	}
}
