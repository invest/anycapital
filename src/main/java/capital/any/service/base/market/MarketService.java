package capital.any.service.base.market;

import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import capital.any.dao.base.market.IMarketDao;
import capital.any.model.base.Market;

/**
 * @author eranl
 *
 */
@Primary
@Service
public class MarketService implements IMarketService {
	private static final Logger logger = LoggerFactory.getLogger(MarketService.class);
	@Autowired
	private IMarketDao marketDao;
	@Autowired
	@Qualifier("MarketHCDao")
	private IMarketDao marketHCDao;

	@Override
	public Map<Integer, Market> getMarketsWithLastPrice() {
		Map<Integer, Market> markets = marketHCDao.getMarketsWithLastPrice();
		if (markets == null || markets.size() <= 0) {
			logger.info("markets empty / null in hazelcast, Let's check RDBMS");
			markets = marketDao.getMarketsWithLastPrice(); 
		}
		return markets;
	}
	
	@Override
	public HashMap<String, Market> getMarketsHMByFeedName() {
		return marketDao.getMarketsHMByFeedName();
	}

	@Override
	public Map<Integer, Market> getThin() {
		Map<Integer, Market> markets = marketHCDao.getThin();
		if (markets == null || markets.size() <= 0) {
			logger.info("markets thin empty / null in hazelcast, Let's check RDBMS");
			markets = marketDao.getThin(); 
		}
		return markets;
	}
	
	/*@Override
	public Map<Integer, Market> getMarketsWithLastPrice() {
		return marketDao.getMarketsWithLastPrice();
	}*/
	
	@Override
	public Map<Integer, Market> getUnderlyingAssets() {
		Map<Integer, Market> markets = marketHCDao.getUnderlyingAssets();
		if (markets == null || markets.size() <= 0) {
			logger.info("markets thin empty / null in hazelcast, Let's check RDBMS");
			markets = marketDao.getUnderlyingAssets();
		}
		return markets;
	}
}
