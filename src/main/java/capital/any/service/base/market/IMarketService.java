package capital.any.service.base.market;

import java.util.HashMap;
import java.util.Map;
import capital.any.model.base.Market;

/**
 * @author eranl
 *
 */
public interface IMarketService {	
	
	/**
	 * Get Markets with price hashmap
	 * @return
	 */
	Map<Integer, Market> getMarketsWithLastPrice();
	
	/**
	 * @return thin markets
	 */
	Map<Integer, Market> getThin();

	/**
	 * get all markets that have price
	 * @return hashmap<marketId, market>
	 */
	//Map<Integer, Market> getMarketsWithLastPrice();
	
	/**
	 * Get Markets hashmap by feedname
	 * @return
	 */
	HashMap<String, Market> getMarketsHMByFeedName();
	
	/**
	 * Get underlying assets
	 * @return Map<Integer, Market>
	 */
	Map<Integer, Market> getUnderlyingAssets();
}
