//package capital.any.service.base.productInfo;
//
//import java.util.HashMap;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import capital.any.dao.base.productInfo.IProductInfoDao;
//import capital.any.model.base.ProductInfo;
//
///**
// * @author eranl
// *
// */
//@Service
//public class ProductInfoService implements IProductInfoService {
//	
//	@Autowired
//	private IProductInfoDao productInfoDao;
//
//	@Override
//	public HashMap<Long, HashMap<Integer, ProductInfo>> getProductInfo() {
//		return productInfoDao.getProductInfo();
//	}
//
//	@Override
//	public List<ProductInfo> getProductInfo(long productId) {
//		return productInfoDao.getProductInfo(productId);
//	}
//
//}
