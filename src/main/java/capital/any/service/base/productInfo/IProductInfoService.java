//package capital.any.service.base.productInfo;
//
//import java.util.HashMap;
//import java.util.List;
//
//import capital.any.model.base.ProductInfo;
//
///**
// * @author eranl
// *
// */
//public interface IProductInfoService {	
//	
//	/**
//	 * Get Product Info hashmap
//	 * @return
//	 */
//	HashMap<Long, HashMap<Integer, ProductInfo>> getProductInfo();
//	
//	List<ProductInfo> getProductInfo(long productId);
//}
