package capital.any.service.base.anyoption.api;

import java.util.ArrayList;
import java.util.concurrent.Future;
import capital.any.model.base.User;
import capital.any.model.base.UserHistory;
import capital.any.model.base.anyoption.AnyoptionAPIRequest;
import capital.any.model.base.anyoption.AnyoptionAPIResponse;
import capital.any.model.base.anyoption.LastCurrencyRate;
import capital.any.model.base.anyoption.ResponseLogin;
import capital.any.model.base.anyoption.UserExtraFields;

/**
 * @author LioR SoLoMoN
 *
 */
public interface IAnyoptionAPIService {
	
	/**
	 * @param loginInput
	 * @return
	 * @throws Exception
	 */
	public ResponseLogin getAnyoptionUser(AnyoptionAPIRequest data) throws Exception;
	
	/**
	 * @param user
	 * @return
	 */
	public Future<AnyoptionAPIResponse<UserExtraFields>> getAnyoptionUserExtraFields(User user, UserHistory userHistory);
	
	/**
	 * @return
	 * @throws Exception
	 */
	public AnyoptionAPIResponse<ArrayList<LastCurrencyRate>> getCurrencyRate() throws Exception;
	
	/**
	 * @param loginInput
	 * @return
	 * @throws Exception
	 */
	//TODO
	//public AnyoptionAPIResponse<User> getAnyoptionUser(AnyoptionAPIRequest data) throws Exception;
	
}
