package capital.any.service.base.marketPrice;

import java.util.List;
import capital.any.model.base.MarketPrice;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IMarketPriceService {
		
	/**
	 * @param marketPrice
	 * @return
	 * @throws Exception 
	 */
	boolean updateMarketPrice(MarketPrice marketPrice) throws Exception;
	
	/**
	 * @param marketPrice
	 */
	void insertMarketPrice(MarketPrice marketPrice);
	
	/**
	 * @param marketPriceList
	 */
	void insertMarketPriceBatch(List<MarketPrice> marketPriceList);

	/**
	 * @param marketPrice
	 * @return 
	 * @throws Exception 
	 */
	boolean updateProductsMarketPrice(MarketPrice marketPrice) throws Exception;

	
	/**
	 * update market price and the market in every product that have this market
	 * @param marketPrice
	 * @return 
	 * @throws Exception 
	 */
	boolean updateMarketPriceAndProducts(MarketPrice marketPrice) throws Exception;

	void insertMarketHistoryPrice(List<MarketPrice> marketPriceList) throws Exception;
		
}
