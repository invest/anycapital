package capital.any.service.base.marketPrice;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import capital.any.dao.base.marketPrice.IMarketPriceDao;
import capital.any.model.base.MarketPrice;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service
public class MarketPriceService implements IMarketPriceService {

	@Autowired
	private IMarketPriceDao marketPriceDao;
	@Autowired
	@Qualifier("MarketPriceHCDao")
	private IMarketPriceDao marketPriceHCDao;
	
	public boolean updateMarketPrice(MarketPrice marketPrice) throws Exception {
		boolean result = marketPriceDao.updateMarketPrice(marketPrice);
		if (result) {
			result = marketPriceHCDao.updateMarketPrice(marketPrice);
		}
		return result;
	}
	
	@Override
	public boolean updateProductsMarketPrice(MarketPrice marketPrice) throws Exception {
		return marketPriceHCDao.updateProductsMarketPrice(marketPrice);
	}
	
	@Override
	public void	insertMarketPrice(MarketPrice marketPrice) {
		marketPriceDao.insertMarketPrice(marketPrice);
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void	insertMarketHistoryPrice(List<MarketPrice> marketPriceList) throws Exception {
		for (MarketPrice marketPrice : marketPriceList) {
			marketPriceDao.insertMarketPrice(marketPrice);
			marketPriceHCDao.updateProductsMarketHistoryPrice(marketPrice);
		}		
	}
	
	@Override
	public void insertMarketPriceBatch(List<MarketPrice> marketPriceList) {
		marketPriceDao.insertMarketPriceBatch(marketPriceList);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean updateMarketPriceAndProducts(MarketPrice marketPrice) throws Exception {
		boolean result = false; 
		if (updateMarketPrice(marketPrice)) {
			result = updateProductsMarketPrice(marketPrice);
		}
		return result;
	}
}
