package capital.any.service.base.productBarrierType;

import java.util.Map;
import capital.any.model.base.ProductBarrierType;

/**
 * @author Eyal G
 *
 */
public interface IProductBarrierTypeService {	
	
	Map<Integer, ProductBarrierType> get();
}
