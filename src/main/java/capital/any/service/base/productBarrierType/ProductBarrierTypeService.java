package capital.any.service.base.productBarrierType;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import capital.any.dao.base.productBarrierType.IProductBarrierTypeDao;
import capital.any.model.base.ProductBarrierType;

/**
 * @author Eyal G
 *
 */
@Primary
@Service
public class ProductBarrierTypeService implements IProductBarrierTypeService {
	private static final Logger logger = LoggerFactory.getLogger(ProductBarrierTypeService.class);
	@Autowired
	private IProductBarrierTypeDao productBarrierTypeDao;
	@Autowired
	@Qualifier("ProductBarrierTypeHCDao")
	private IProductBarrierTypeDao productBarrierTypeHCDao;
	
	@Override
	public Map<Integer, ProductBarrierType> get() {
		Map<Integer, ProductBarrierType> productBarrierTypes = productBarrierTypeHCDao.get();
		if (productBarrierTypes == null || productBarrierTypes.size() <= 0) {
			logger.info("productBarrierTypes empty / null in hazelcast, Let's check RDBMS");
			productBarrierTypes =  productBarrierTypeDao.get(); 
		}
		return productBarrierTypes;
	}	
}
