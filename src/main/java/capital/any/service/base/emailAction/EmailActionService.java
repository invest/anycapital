package capital.any.service.base.emailAction;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import capital.any.dao.base.emailAction.IEmailActionDao;
import capital.any.model.base.EmailAction;
import capital.any.model.base.EmailActionDetails;
import capital.any.model.base.EmailActionRequest;
import capital.any.model.base.EmailActionResponse;
import capital.any.service.base.emailAction.handler.EmailActionHandler;

/**
 * 
 * @author eyal.ohana
 *
 */
@Primary
@Service
public class EmailActionService implements IEmailActionService {
	private static final Logger logger = LoggerFactory.getLogger(EmailActionService.class);

	@Autowired
	private IEmailActionDao emailActionDao;
	@Autowired
	@Qualifier("EmailActionHCDao")
	private IEmailActionDao emailActionHCDao;
	@Autowired
	private ApplicationContext applicationContext;
	
	@Override
	public Map<Integer, EmailAction> get() {
		Map<Integer, EmailAction> map = emailActionHCDao.get();
		if (null == map || map.size() <= 0) {
			logger.info("map empty / null in hazelcast, Let's check RDBMS");
			map = emailActionDao.get();
		}
		return map;
	}
	
	@Override
	public EmailAction getEmailAction(int id) {
		EmailAction emailAction = emailActionHCDao.getEmailAction(id);
		if (emailAction == null) {
			logger.info("emailAction empty / null in hazelcast, Let's check RDBMS");
			emailAction = emailActionDao.getEmailAction(id);
		}
		return emailAction;
	}
	
	@Override
	public void insertEmailByAction(EmailActionRequest<?> emailActionRequest) throws Exception {
		int emailActionId = emailActionRequest.getEmailAction().getId();
		EmailAction emailAction = getEmailAction(emailActionId);
		EmailActionDetails<?> emailActionDetails = new EmailActionDetails<>(emailAction, emailActionRequest);
		getEmailActionHandler(emailAction).insert(emailActionDetails);
	}
	
	@Override
	public EmailActionResponse<?> getListByAction(EmailActionRequest<?> emailActionRequest) throws Exception {
		int emailActionId = emailActionRequest.getEmailAction().getId();
		EmailAction emailAction = getEmailAction(emailActionId);
		EmailActionDetails<?> emailActionDetails = new EmailActionDetails<>(emailAction, emailActionRequest);
		return getEmailActionHandler(emailAction).getListByAction(emailActionDetails);
	}
	
	public EmailActionHandler getEmailActionHandler(EmailAction emailAction) throws Exception {
		EmailActionHandler emailActionHandler = null;
		Class<?> clazz = Class.forName(emailAction.getClassPath());
		emailActionHandler = (EmailActionHandler) clazz.newInstance();
		applicationContext.getAutowireCapableBeanFactory().autowireBean(emailActionHandler);
		return emailActionHandler;
	}

}
