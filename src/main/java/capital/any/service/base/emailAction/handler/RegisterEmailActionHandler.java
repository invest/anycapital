package capital.any.service.base.emailAction.handler;

import java.io.Serializable;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import capital.any.base.enums.EmailParameters;
import capital.any.model.base.Country;
import capital.any.model.base.EmailActionDetails;
import capital.any.model.base.User;
import capital.any.service.base.country.ICountryService;
import capital.any.service.base.messageResource.IMessageResourceService;

/**
 * 
 * @author eyal.ohana
 *
 */
public class RegisterEmailActionHandler extends EmailActionHandler implements Serializable {
	private static final long serialVersionUID = 588219190860190380L;
	
	@Autowired
	private ICountryService countryService;
	@Autowired
	private IMessageResourceService messageResourceService;
	
	/**
	 * Get map parameters
	 */
	public Map<String, String> getMapParameters(EmailActionDetails<?> emailActionDetails) throws Exception {
		Map<String, String> map = super.getMapParameters(emailActionDetails);
		/* Get Info */
		User user = emailActionDetails.getEmailActionRequest().getUser();
		Country country = countryService.get().get(user.getCountryByPrefix());
		String countryName = messageResourceService.translateKey(user.getActionSource().getId(), user.getLanguageId(), country.getDisplayName());
		/* Add parameters for forget password */
		map.put(EmailParameters.USER_COUNTRY_BY_PREFIX.getParam(), countryName);
		return map;
	}
}
