package capital.any.service.base.emailAction.handler;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.base.enums.DBParameter;
import capital.any.base.enums.EmailParameters;
import capital.any.base.enums.EmailTo;
import capital.any.base.enums.EmailType;
import capital.any.model.base.Email;
import capital.any.model.base.EmailAction;
import capital.any.model.base.EmailActionDetails;
import capital.any.model.base.EmailActionResponse;
import capital.any.model.base.MailInfo;
import capital.any.model.base.User;
import capital.any.service.base.dbParameter.IDBParameterService;
import capital.any.service.base.email.IEmailService;

/**
 * 
 * @author eyal.ohana
 *
 */
public abstract class EmailActionHandler implements Serializable {
	private static final long serialVersionUID = -7877561296956004388L;
	
	@Autowired
	private ObjectMapper mapper;
	@Autowired
	private IEmailService emailService;
	@Autowired
	private IDBParameterService dbParameterService;
	
	public void insert(EmailActionDetails<?> emailActionDetails) throws Exception {
		Map<String, String> map = getMapParameters(emailActionDetails);
		User user = (User) emailActionDetails.getEmailActionRequest().getUser();
		EmailAction emailAction = emailActionDetails.getEmailAction();
		MailInfo mailInfo = new MailInfo(
				user.getEmail(),
				null,
				map,
				emailAction.getId(),
				EmailType.TEMPLATE.getId(),
				user.getLanguageId());
		insert(emailActionDetails, mailInfo);
	}
	
	public void insert(EmailActionDetails<?> emailActionDetails, MailInfo mailInfo) throws Exception {
		int emailTo = emailActionDetails.getEmailActionRequest().getEmailTo();
		if (emailTo != 0 && emailTo != EmailTo.USER.getId()) {
			if (emailTo == EmailTo.BOTH.getId()) {
				insertEmail(emailActionDetails, mailInfo);
			}
			mailInfo.setTo(emailActionDetails.getEmailActionRequest().getWriter().getEmail());
		}
		insertEmail(emailActionDetails, mailInfo);
	}
	
	public void insertEmail(EmailActionDetails<?> emailActionDetails, MailInfo mailInfo) throws Exception {
		Email email = new Email();		
		email.setActionSource(emailActionDetails.getEmailActionRequest().getActionSource());
		email.setEmailInfo(mapper.writeValueAsString(mailInfo));
		emailService.insert(email);
	}
	
	public Map<String, String> getMapParameters(EmailActionDetails<?> emailActionDetails) throws Exception {
		User user = (User) emailActionDetails.getEmailActionRequest().getUser();
		Map<String, String> map = new HashMap<String, String>();
	    /* Add default parameter for all email templates */
		map.put(EmailParameters.SUPPORT_EMAIL.getParam(), dbParameterService.get(DBParameter.SUPPORT_EMAIL.getId()).getStringValue());
		map.put(EmailParameters.SUPPORT_PHONE.getParam(), dbParameterService.get(DBParameter.SUPPORT_PHONE.getId()).getStringValue());
		map.put(EmailParameters.DOCUMENTS_EMAIL.getParam(), dbParameterService.get(DBParameter.DOCUMENTS_EMAIL.getId()).getStringValue());
		/* add parameters by user */
		map.put(EmailParameters.PARAM_FIRST_NAME.getParam(), user.getFirstName());
		map.put(EmailParameters.USER_NAME.getParam(), user.getEmail());
		return map;
	}
	
	public EmailActionResponse<?> getListByAction(EmailActionDetails<?> emailActionDetails) {
		EmailActionResponse<Object> emailActionResponse = new EmailActionResponse<Object>(null);
		return emailActionResponse;
	}
}
