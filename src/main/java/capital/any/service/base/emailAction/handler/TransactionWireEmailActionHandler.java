package capital.any.service.base.emailAction.handler;

import java.io.Serializable;
import java.util.List;

import capital.any.base.enums.TransactionPaymentType;
import capital.any.model.base.EmailActionDetails;
import capital.any.model.base.EmailActionResponse;
import capital.any.model.base.SqlFilters;
import capital.any.model.base.Transaction;
import capital.any.model.base.User;
import capital.any.model.base.Transaction.TransactionStatusEnum;

/**
 * 
 * @author Eyal.o
 *
 */
public class TransactionWireEmailActionHandler extends TransactionEmailActionHandler implements Serializable {
	private static final long serialVersionUID = 4931315679621307590L;

	public EmailActionResponse<?> getListByAction(EmailActionDetails<?> emailActionDetails) {
		User user = emailActionDetails.getEmailActionRequest().getUser();
		SqlFilters filters = new SqlFilters();
		filters.setUserId(user.getId());
		filters.setTransactionPaymentTypeId(TransactionPaymentType.BANK_WIRE.getId());
		filters.setTransactionStatusId(TransactionStatusEnum.PENDING.getId());
		List<Transaction> transactions = transactionService.getTransactions(filters);
		EmailActionResponse<List<Transaction>> emailActionResponse = new EmailActionResponse<List<Transaction>>(transactions);
		return emailActionResponse;
	}
}
