package capital.any.service.base.emailAction.handler;

import java.io.Serializable;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.base.enums.EmailParameters;
import capital.any.base.security.ISecureAlgorithmsService;
import capital.any.model.base.EmailActionDetails;
import capital.any.model.base.EmailActionRequest;
import capital.any.model.base.ForgetPasswordEmailAction;
import capital.any.model.base.GoogleShortURLResponse;
import capital.any.service.IUtilService;

/**
 * 
 * @author eyal.ohana
 *
 */
public class ForgetPasswordEmailActionHandler extends EmailActionHandler implements Serializable {
	private static final long serialVersionUID = -6121986577053870212L;

	private static final Logger logger = LoggerFactory.getLogger(ForgetPasswordEmailActionHandler.class);
	
	@Autowired
	private ObjectMapper mapper;
	@Autowired
	private ISecureAlgorithmsService secureAlgorithmsService;
	@Autowired
	private IUtilService utilService;
	
	@SuppressWarnings("unchecked")
	public Map<String, String> getMapParameters(EmailActionDetails<?> emailActionDetails) throws Exception {
		Map<String, String> map = super.getMapParameters(emailActionDetails);
		/* Get Info */
		EmailActionRequest<ForgetPasswordEmailAction> request = null;
		try {
			EmailActionRequest<?> emailActionRequest = emailActionDetails.getEmailActionRequest();
			String json = mapper.writeValueAsString(emailActionRequest);
			request = (EmailActionRequest<ForgetPasswordEmailAction>) mapper.readValue(json, new TypeReference<EmailActionRequest<ForgetPasswordEmailAction>>() {});
			ForgetPasswordEmailAction forgetPasswordEmailAction = (ForgetPasswordEmailAction) request.getData();
			String googleShortUrl = null;
			if (null != forgetPasswordEmailAction && !utilService.isArgumentEmptyOrNull(forgetPasswordEmailAction.getGoogleShortUrlId())) {
				googleShortUrl = forgetPasswordEmailAction.getGoogleShortUrlId();
			} else {
				GoogleShortURLResponse generateGoogleShortURL = secureAlgorithmsService.generateTokenAndLink(emailActionRequest.getUser());
				googleShortUrl = generateGoogleShortURL.getId();
			}
			map.put(EmailParameters.RESET_LINK.getParam(), googleShortUrl);
		} catch (Exception e) {
			logger.error("Problem with ForgetPasswordEmailActionHandler.", e);
			throw e;
		}
		return map;
	}
}
