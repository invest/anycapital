package capital.any.service.base.emailAction.handler;

import java.io.Serializable;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.base.enums.EmailParameters;
import capital.any.model.base.Currency;
import capital.any.model.base.EmailActionDetails;
import capital.any.model.base.EmailActionRequest;
import capital.any.model.base.Transaction;
import capital.any.service.IUtilService;
import capital.any.service.base.country.ICountryService;
import capital.any.service.base.currency.ICurrencyService;
import capital.any.service.base.locale.ILocaleService;
import capital.any.service.base.transaction.ITransactionService;

/**
 *
 * @author Eyal.o
 *
 */
public class TransactionEmailActionHandler extends EmailActionHandler implements Serializable {
	private static final long serialVersionUID = -1976449891476955980L;

	@Autowired
	private ObjectMapper mapper;
	@Autowired
	protected ITransactionService transactionService;
	@Autowired
	private IUtilService utilService;
	@Autowired
	private ICountryService countryService;
	@Autowired
	private ILocaleService localeService;
	@Autowired
	private ICurrencyService currencyService;
	
	/**
	 * Get map parameters
	 */
	@SuppressWarnings("unchecked")
	public Map<String, String> getMapParameters(EmailActionDetails<?> emailActionDetails) throws Exception {
		Map<String, String> map = super.getMapParameters(emailActionDetails);
		/* Get Info */
		EmailActionRequest<Transaction> request = null;
		EmailActionRequest<?> emailActionRequest = emailActionDetails.getEmailActionRequest();
		String json = mapper.writeValueAsString(emailActionRequest);
		request = (EmailActionRequest<Transaction>) mapper.readValue(json, new TypeReference<EmailActionRequest<Transaction>>() {});
		Transaction transaction = (Transaction) request.getData();
		if (transaction.getAmount() == 0) {
			transaction = transactionService.getTransactionById(transaction.getId());
		}
		Locale locale = localeService.getAvailableLocales().get(countryService.get().get(emailActionRequest.getUser().getCountryByUser()));
		Currency currency = currencyService.get().get(emailActionRequest.getUser().getCurrencyId());
		/* Add parameters for investment mail */
		map.put(EmailParameters.AMOUNT.getParam(), utilService.displayAmount(transaction.getAmount(), locale, currency.getCode()));
		return map;
	}
}
