package capital.any.service.base.emailAction.handler;

import java.io.Serializable;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.base.enums.EmailParameters;
import capital.any.model.base.Currency;
import capital.any.model.base.EmailActionDetails;
import capital.any.model.base.EmailActionRequest;
import capital.any.model.base.Investment;
import capital.any.model.base.InvestmentEmailAction;
import capital.any.model.base.Product;
import capital.any.service.IUtilService;
import capital.any.service.base.country.ICountryService;
import capital.any.service.base.currency.ICurrencyService;
import capital.any.service.base.investment.IInvestmentService;
import capital.any.service.base.locale.ILocaleService;
import capital.any.service.base.messageResource.IMessageResourceService;
import capital.any.service.base.product.IProductService;

/**
 * 
 * @author eyal.ohana
 *
 */
public class InvestmentEmailActionHandler extends EmailActionHandler implements Serializable {
	private static final long serialVersionUID = -8932116320540120494L;

	private static final Logger logger = LoggerFactory.getLogger(InvestmentEmailActionHandler.class);

	@Autowired
	private IMessageResourceService messageResourceService;
	@Autowired
	private IUtilService utilService;
	@Autowired
	protected IInvestmentService investmentService;
	@Autowired
	private IProductService productService;
	@Autowired
	private ObjectMapper mapper;
	@Autowired
	private ICountryService countryService;
	@Autowired
	private ILocaleService localeService;
	@Autowired
	private ICurrencyService currencyService;
	
	/**
	 * Get map parameters
	 */
	@SuppressWarnings("unchecked")
	public Map<String, String> getMapParameters(EmailActionDetails<?> emailActionDetails) throws Exception {
		Map<String, String> map = super.getMapParameters(emailActionDetails);
		/* Get Info */
		EmailActionRequest<InvestmentEmailAction> request = null;
		try {
			EmailActionRequest<?> emailActionRequest = emailActionDetails.getEmailActionRequest();
			String json = mapper.writeValueAsString(emailActionRequest);
			request = (EmailActionRequest<InvestmentEmailAction>) mapper.readValue(json, new TypeReference<EmailActionRequest<InvestmentEmailAction>>() {});
			InvestmentEmailAction investmentEmailAction = (InvestmentEmailAction) request.getData();
			Investment investment = investmentEmailAction.getInvestment();
			Product product = investmentEmailAction.getProduct();
			if (null == product) {
				investment = investmentService.getInvestmentById(investment.getId());
				product = productService.getProduct(investment.getProductId());
			}
			String productName = messageResourceService.translateKey(emailActionRequest.getActionSource().getId(), emailActionRequest.getUser().getLanguageId(), product.getProductType().getDisplayName());
			String marketName = messageResourceService.translateKey(emailActionRequest.getActionSource().getId(), emailActionRequest.getUser().getLanguageId(), product.getProductMarkets().get(0).getMarket().getDisplayName());
			Locale locale = localeService.getAvailableLocales().get(countryService.get().get(emailActionRequest.getUser().getCountryByUser()));
			Currency currency = currencyService.get().get(emailActionRequest.getUser().getCurrencyId());
			/* Add parameters for investment mail */
			map.put(EmailParameters.PRODUCT_NAME.getParam(), productName);
			map.put(EmailParameters.ASSET.getParam(), marketName);
			map.put(EmailParameters.PRODUCT_ID.getParam(), String.valueOf(product.getId()));
			map.put(EmailParameters.INV_AMOUNT.getParam(), utilService.displayAmount(investment.getAmount(), locale, currency.getCode()));
			map.put(EmailParameters.PARAM_FIRST_NAME.getParam(), investment.getUser().getFirstName());
			map.put(EmailParameters.INV_ID.getParam(), String.valueOf(investment.getId()));
			map.put(EmailParameters.SUBSCRIPTION_ENDDATE.getParam(), product.getSubscriptionEndDate().toString());
		} catch (Exception e) {
			logger.error("Problem with InvestmentEmailActionHandler.", e);
			throw e;
		}
		return map;
	}
}
