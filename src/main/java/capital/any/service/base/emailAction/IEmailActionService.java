package capital.any.service.base.emailAction;

import java.util.Map;

import capital.any.model.base.EmailAction;
import capital.any.model.base.EmailActionRequest;
import capital.any.model.base.EmailActionResponse;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IEmailActionService {
	
	Map<Integer, EmailAction> get();

	EmailAction getEmailAction(int id);

	void insertEmailByAction(EmailActionRequest<?> emailActionRequest) throws Exception;

	EmailActionResponse<?> getListByAction(EmailActionRequest<?> emailActionRequest) throws Exception;
	
}
