package capital.any.service.base.emailAction.handler;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import capital.any.base.enums.InvestmentStatusEnum;
import capital.any.model.base.EmailActionDetails;
import capital.any.model.base.EmailActionResponse;
import capital.any.model.base.User;

/**
 * 
 * @author eyal.ohana
 *
 */
public class InvestmentPendingEmailActionHandler extends InvestmentEmailActionHandler implements Serializable {
	private static final long serialVersionUID = 1867835653775472171L;

	public EmailActionResponse<?> getListByAction(EmailActionDetails<?> emailActionDetails) {
		User user = emailActionDetails.getEmailActionRequest().getUser();
		List<Integer> statuses = new ArrayList<Integer>();
		statuses.add(InvestmentStatusEnum.PENDING.getId());
		List<Map<String, Object>> investments = investmentService.getByUserId(user.getId(), true, statuses);
		EmailActionResponse<List<Map<String, Object>>> emailActionResponse = new EmailActionResponse<List<Map<String, Object>>>(investments);
		return emailActionResponse;
	}
}
