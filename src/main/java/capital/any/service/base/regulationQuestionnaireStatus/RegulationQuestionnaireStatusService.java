package capital.any.service.base.regulationQuestionnaireStatus;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import capital.any.dao.base.regulationQuestionnaireStatus.IRegulationQuestionnaireStatusDao;

/**
 * 
 * @author eyal.ohana
 *
 */
@Primary
@Service
public class RegulationQuestionnaireStatusService implements IRegulationQuestionnaireStatusService {
	private static final Logger logger = LoggerFactory.getLogger(RegulationQuestionnaireStatusService.class);
	
	@Autowired
	private IRegulationQuestionnaireStatusDao regulationQuestionnaireStatusDao;
	@Autowired
	@Qualifier("RegulationQuestionnaireStatusHCDao")
	private IRegulationQuestionnaireStatusDao regulationQuestionnaireStatusHCDao;
	
	@Override
	public Map<Long, Integer> getScoreStatus() {
		Map<Long, Integer> mapScoreStatus = regulationQuestionnaireStatusHCDao.getScoreStatus();
		if (mapScoreStatus == null || mapScoreStatus.size() <= 0) {
			logger.info("mapScoreStatus empty / null in hazelcast, Let's check RDBMS");
			mapScoreStatus = regulationQuestionnaireStatusDao.getScoreStatus();
		}
		return mapScoreStatus;
	}
}
