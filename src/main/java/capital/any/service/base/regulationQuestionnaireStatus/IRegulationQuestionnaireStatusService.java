package capital.any.service.base.regulationQuestionnaireStatus;

import java.util.Map;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IRegulationQuestionnaireStatusService {

	Map<Long, Integer> getScoreStatus();

}
