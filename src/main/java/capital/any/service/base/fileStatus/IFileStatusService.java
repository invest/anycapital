package capital.any.service.base.fileStatus;

import java.util.Map;

import capital.any.model.base.FileStatus;

/**
 * 
 * @author Eyal.o
 *
 */
public interface IFileStatusService {
	
	Map<Integer, FileStatus> get();

}
