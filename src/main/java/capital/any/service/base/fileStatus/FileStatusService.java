package capital.any.service.base.fileStatus;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import capital.any.dao.base.fileStatus.IFileStatusDao;
import capital.any.model.base.FileStatus;

/**
 * 
 * @author Eyal.o
 *
 */
@Primary
@Service
public class FileStatusService implements IFileStatusService {
	private static final Logger logger = LoggerFactory.getLogger(FileStatusService.class);
	
	@Autowired
	private IFileStatusDao fileStatusDao;
	@Autowired
	@Qualifier("FileStatusHCDao")
	private IFileStatusDao fileStatusHCDao;

	@Override
	public Map<Integer, FileStatus> get() {
		Map<Integer, FileStatus> map = fileStatusHCDao.get();
		if (null == map || map.size() <= 0) {
			logger.info("map empty / null in hazelcast, Let's check RDBMS");
			map = fileStatusDao.get();
		}
		return map;
	}
	
}
