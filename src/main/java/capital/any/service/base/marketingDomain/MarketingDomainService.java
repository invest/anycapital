package capital.any.service.base.marketingDomain;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.marketingDomains.IMarketingDomainDao;
import capital.any.model.base.MarketingDomain;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service
public class MarketingDomainService implements IMarketingDomainService {

	@Autowired
	private IMarketingDomainDao marketingDomainDao;

	@Override
	public Map<Integer, MarketingDomain> getMarketingDomains() {
		Map<Integer, MarketingDomain> marketingDomains = marketingDomainDao.getMarketingDomains();
		return marketingDomains;
	}
}
