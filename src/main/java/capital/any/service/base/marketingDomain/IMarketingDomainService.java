package capital.any.service.base.marketingDomain;

import java.util.Map;

import capital.any.model.base.MarketingDomain;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IMarketingDomainService {
	
	/**
	 * Get marketing Domains
	 * @return
	 */
	Map<Integer, MarketingDomain> getMarketingDomains();
}
