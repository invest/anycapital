package capital.any.service.base.productKidLanguage;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import capital.any.dao.base.productKidLanguage.IProductKidLanguageDao;
import capital.any.model.base.Product;
import capital.any.model.base.ProductKidLanguage;

/**
 * 
 * @author Eyal.o
 *
 */
@Service
public class ProductKidLanguageService implements IProductKidLanguageService {
	
	@Autowired
	private IProductKidLanguageDao productKidLanguageDao;
	@Autowired
	@Qualifier("ProductKidLanguageHCDao")
	private IProductKidLanguageDao productKidLanguageHCDao;

	@Override
	public boolean insert(ProductKidLanguage ProductKidLanguage) throws Exception {
		boolean result = productKidLanguageDao.insert(ProductKidLanguage);
		if (result) {
			result = productKidLanguageHCDao.insert(ProductKidLanguage);
		}
		return result;
	}
	
	@Override
	public List<ProductKidLanguage> get(Product product) {
		return productKidLanguageDao.get(product);
	}
	
}
