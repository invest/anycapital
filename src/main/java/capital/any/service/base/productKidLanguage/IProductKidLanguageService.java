package capital.any.service.base.productKidLanguage;

import java.util.List;

import capital.any.model.base.Product;
import capital.any.model.base.ProductKidLanguage;

/**
 * 
 * @author Eyal.o
 *
 */
public interface IProductKidLanguageService {

	boolean insert(ProductKidLanguage ProductKidLanguage) throws Exception;

	List<ProductKidLanguage> get(Product product);
	
}
