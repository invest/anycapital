package capital.any.service.base.productCoupon;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import capital.any.dao.base.productCoupon.IProductCouponDao;
import capital.any.model.base.ProductCoupon;

/**
 * @author eranl
 *
 */
@Primary
@Service
public class ProductCouponService implements IProductCouponService {
	private static final Logger logger = LoggerFactory.getLogger(ProductCouponService.class);
	@Autowired
	private IProductCouponDao productCouponDao;
	@Autowired
	@Qualifier("ProductCouponHCDao")
	private IProductCouponDao productCouponHCDao;
	
	@Override
	public Map<Long, List<ProductCoupon>> get() {	
		Map<Long, List<ProductCoupon>> productCoupons = productCouponHCDao.get();
		if (productCoupons == null || productCoupons.size() <= 0) {
			logger.info("productCoupons empty / null in hazelcast, Let's check RDBMS");
			productCoupons = productCouponDao.get(); 
		}
		return productCoupons;
	}

	@Override
	public List<ProductCoupon> getProductCoupons(long productId) {
		return productCouponDao.getProductCoupons(productId);
	}
}
