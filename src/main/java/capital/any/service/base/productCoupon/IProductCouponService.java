package capital.any.service.base.productCoupon;

import java.util.List;
import java.util.Map;
import capital.any.model.base.ProductCoupon;

/**
 * @author eranl
 *
 */
public interface IProductCouponService {	
	
	/**
	 * Get Product Coupons hashmap
	 * @return
	 */
	Map<Long, List<ProductCoupon>> get();
	
	List<ProductCoupon> getProductCoupons(long productId);
}
