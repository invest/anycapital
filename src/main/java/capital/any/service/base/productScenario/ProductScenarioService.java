//package capital.any.service.base.productScenario;
//
//import java.util.HashMap;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import capital.any.dao.base.productScenario.IProductScenarioDao;
//import capital.any.model.base.ProductScenario;
//
///**
// * @author eranl
// *
// */
//@Service
//public class ProductScenarioService implements IProductScenarioService {
//	
//	@Autowired
//	private IProductScenarioDao productScenarioDao;
//
//	@Override
//	public HashMap<Long, List<ProductScenario>> getProductScenarios() { 
//		return productScenarioDao.getProductScenarios();
//	}
//
//	@Override
//	public List<ProductScenario> getProductScenarios(long productId) {
//		return productScenarioDao.getProductScenarios(productId);
//	}
//
//}
