//package capital.any.service.base.productScenario;
//
//import java.util.HashMap;
//import java.util.List;
//
//import capital.any.model.base.ProductScenario;
//
///**
// * @author eranl
// *
// */
//public interface IProductScenarioService {	
//	
//	/**
//	 * Get Product Scenarios hashmap
//	 * @return
//	 */
//	HashMap<Long, List<ProductScenario>> getProductScenarios();
//	
//	List<ProductScenario> getProductScenarios(long productId);
//}
