package capital.any.service.base.allowIP;

import java.util.Map;
import capital.any.model.base.AllowIP;

/**
 * @author eran.levy
 *
 */
public interface IAllowIPService {
	
	/**
	 * get allowIP map
	 */
	Map<String, AllowIP> get();
	
}
