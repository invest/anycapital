package capital.any.service.base.allowIP;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import capital.any.dao.base.allowIP.IAllowIPDao;
import capital.any.model.base.AllowIP;

/**
 * @author eran.levy
 *
 */
@Service
public class AllowIPService implements IAllowIPService {
	
	private static final Logger logger = LoggerFactory.getLogger(AllowIPService.class);
	
	@Autowired
	private IAllowIPDao allowIPDao;
	@Autowired
	@Qualifier("AllowIPHCDao")
	private IAllowIPDao allowIPHCDao;
	
	@Override
	public Map<String, AllowIP> get() {
		Map<String, AllowIP> list = allowIPHCDao.get();
		if (list == null || list.size() <= 0) {
			logger.info("allowip list  empty / null in hazelcast, Let's check RDBMS");
			list = allowIPDao.get(); 
		}
		return list;
	}
}