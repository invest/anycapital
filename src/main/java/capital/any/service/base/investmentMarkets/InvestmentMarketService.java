package capital.any.service.base.investmentMarkets;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.investmentMarket.IInvestmentMarketDao;
import capital.any.model.base.InvestmentMarket;

/**
 * @author Eyal G
 *
 */
@Service
public class InvestmentMarketService implements IInvestmentMarketService {
	
	@Autowired
	private IInvestmentMarketDao investmentMarketDao;

	@Override
	public void insert(InvestmentMarket investmentMarket) {
		investmentMarketDao.insert(investmentMarket);
	}

	@Override
	public void insert(List<InvestmentMarket> investmentMarkets) {
		investmentMarketDao.insert(investmentMarkets);		
	}

	@Override
	public List<InvestmentMarket> get(long investmentId) {
		return investmentMarketDao.get(investmentId);
	}
}
