package capital.any.service.base.investmentStatus;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import capital.any.dao.base.investmentStatus.IInvestmentStatusDao;
import capital.any.model.base.InvestmentStatus;


/**
 * @author Eyal Goren
 *
 */
@Primary
@Service
public class InvestmentStatusService implements IInvestmentStatusService {
	private static final Logger logger = LoggerFactory.getLogger(InvestmentStatusService.class);
	@Autowired
	private IInvestmentStatusDao investmentStatusDao;
	@Autowired
	@Qualifier("InvestmentStatusHCDao")
	private IInvestmentStatusDao investmentStatusHCDao;

	@Override
	public Map<Integer, InvestmentStatus> get() {
		Map<Integer, InvestmentStatus> investmentStatus = investmentStatusHCDao.get();
		if (investmentStatus == null || investmentStatus.size() <= 0) {
			logger.info("investmentStatus empty / null in hazelcast, Let's check RDBMS");
			investmentStatus = investmentStatusDao.get(); 
		}
		return investmentStatus;
	}
}
