package capital.any.service.base.investmentStatus;

import java.util.Map;

import capital.any.model.base.InvestmentStatus;

/**
 * @author Eyal Goren
 *
 */
public interface IInvestmentStatusService {	
	
	/**
	 * Get Investment Status hashmap
	 * @return
	 */
	Map<Integer, InvestmentStatus> get();
}
