//package capital.any.service.base.mail;
//
//import javax.annotation.PostConstruct;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.thymeleaf.context.Context;
//import org.thymeleaf.spring4.SpringTemplateEngine;
//import org.thymeleaf.templateresolver.TemplateResolver;
//
//import capital.any.model.base.MailInfo;
//
///**
// * 
// * @author eyal.ohana
// *
// */
//@Service
//public class MailContentBuilder implements IMailContentBuilder {
//	
//	@Autowired
//	private SpringTemplateEngine templateEngine;
//	
//	@PostConstruct
//	private void init() {
//		TemplateResolver resolver = new TemplateResolver();
//        resolver.setResourceResolver(new AwsTemplateResolver());
//        resolver.setSuffix(".html");
//        resolver.setTemplateMode(TemplateResolver.DEFAULT_TEMPLATE_MODE);
//        resolver.setCharacterEncoding("UTF-8");
//        resolver.setOrder(1);
//        templateEngine.setTemplateResolver(resolver);
//	}
//
//	@Override
//	public String build(MailInfo mailInfo) {
//		Context context = new Context();
//		context.setVariables(mailInfo.getMap());
//        return templateEngine.process(mailInfo.getTemplateName(), context);
//	}
//}
