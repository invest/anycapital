//package capital.any.service.base.mail;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.mail.MailException;
//import org.springframework.mail.javamail.JavaMailSender;
//import org.springframework.mail.javamail.MimeMessageHelper;
//import org.springframework.mail.javamail.MimeMessagePreparator;
//import org.springframework.stereotype.Service;
//
//import capital.any.base.enums.EmailType;
//import capital.any.model.base.MailInfo;
//
///**
// * 
// * @author eyal.ohana
// *
// */
//@Service
//public class MailService implements IMailService {
//	private static final Logger logger = LoggerFactory.getLogger(MailService.class);
//	
//	@Autowired
//	private JavaMailSender mailSender;
//	@Autowired
//    private IMailContentBuilder mailContentBuilder;
//	@Value("${spring.mail.username}")
//	private String from;
//	
//	@Override
//	public void prepareAndSend(MailInfo mailInfo) throws MailException {
//		MimeMessagePreparator messagePreparator = mimeMessage -> {
//            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
//            messageHelper.setFrom(from);
//            messageHelper.setTo(mailInfo.getTo());
//            messageHelper.setSubject(mailInfo.getSubject());
//            String content = mailInfo.getText();
//            boolean isHTML = true;
//            if (mailInfo.getTypeId() == EmailType.TEMPLATE.getId()) {
//            	content = mailContentBuilder.build(mailInfo);
//            } else if (mailInfo.getTypeId() == EmailType.NO_TEMPLATE_TEXT.getId()) {
//            	isHTML = false;
//            }
//            messageHelper.setText(content, isHTML);
//        };
//        mailSender.send(messagePreparator);
//	}
//	
////	@Override
////	public void prepareAndSendNoTemplate(MailInfo mailInfo, boolean isHTML) {
////		logger.debug("going to send email " + mailInfo);
////		MimeMessagePreparator messagePreparator = mimeMessage -> {
////            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
////            messageHelper.setFrom(from);
////            messageHelper.setTo(mailInfo.getTo());
////            messageHelper.setSubject(mailInfo.getSubject());
////            messageHelper.setText(mailInfo.getText(), isHTML);
////        };
////        try {
////            mailSender.send(messagePreparator);
////        } catch (MailException e) {
////        	logger.error("Problem with sending mail.", e);
////        }
////	}
//
//}
