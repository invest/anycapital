//package capital.any.service.base.mail;
//
//import capital.any.model.base.MailInfo;
//
///**
// * 
// * @author eyal.ohana
// *
// */
//public interface IMailContentBuilder {
//	public static final String PARAM_FIRST_NAME = "first_name";
//	public static final String RESET_LINK = "resetLink";
//	public static final String USER_NAME = "userName";
//	public static final String PARAM_CONTENT = "content";
//	
//	String build(MailInfo mailInfo);
//	
//}
