package capital.any.service.base.emailTemplate;

import java.util.Map;

import capital.any.model.base.EmailTemplate;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IEmailTemplateService {

	EmailTemplate getTemplate(EmailTemplate et) throws Exception;

	Map<Integer, EmailTemplate> get();

}
