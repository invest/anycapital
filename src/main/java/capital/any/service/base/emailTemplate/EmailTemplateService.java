package capital.any.service.base.emailTemplate;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import capital.any.dao.base.emailTemplate.IEmailTemplateDao;
import capital.any.model.base.EmailTemplate;

/**
 * 
 * @author eyal.ohana
 *
 */
@Primary
@Service
public class EmailTemplateService implements IEmailTemplateService {
	private static final Logger logger = LoggerFactory.getLogger(EmailTemplateService.class);
	
	@Autowired
	private IEmailTemplateDao emailTemplateDao;
	@Autowired
	@Qualifier("EmailTemplateHCDao")
	private IEmailTemplateDao emailTemplateHCDao;
	
	@Override
	public EmailTemplate getTemplate(EmailTemplate et) throws Exception {
		EmailTemplate emailTemplate = emailTemplateHCDao.getTemplate(et);
		if (emailTemplate == null) {
			logger.info("emailTemplate empty / null in hazelcast, Let's check RDBMS");
			emailTemplate = emailTemplateDao.getTemplate(et);
		}
		return emailTemplate;
	}
	
	@Override
	public Map<Integer, EmailTemplate> get() {
		Map<Integer, EmailTemplate> map = emailTemplateHCDao.get();
		if (null == map || map.size() <= 0) {
			logger.info("map empty / null in hazelcast, Let's check RDBMS");
			map = emailTemplateDao.get();
		}
		return map;
	}
}
