package capital.any.service.base.productBarrier;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import capital.any.dao.base.productBarrier.IProductBarrierDao;
import capital.any.model.base.ProductBarrier;

/**
 * @author Eyal G
 *
 */
@Service
public class ProductBarrierService implements IProductBarrierService {
	
	@Autowired
	protected IProductBarrierDao productBarrierDao;

	@Override
	public Map<Long, ProductBarrier> get() {
		//XXX without cache
		return productBarrierDao.get();
	}

	@Override
	public ProductBarrier get(long productId) {
		//XXX without cache
		return productBarrierDao.get(productId);
	}	
}
