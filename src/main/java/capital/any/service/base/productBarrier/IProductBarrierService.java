package capital.any.service.base.productBarrier;

import java.util.Map;
import capital.any.model.base.ProductBarrier;

/**
 * @author Eyal G
 *
 */
public interface IProductBarrierService {	
	Map<Long, ProductBarrier> get();
	
	ProductBarrier get(long productId);
}
