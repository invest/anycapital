package capital.any.service.base.locale;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import capital.any.dao.base.locale.ILocaleDao;

/**
 * 
 * @author eyal.ohana
 *
 */
@Primary
@Service
public class LocaleService implements ILocaleService {
	private static final Logger logger = LoggerFactory.getLogger(LocaleService.class);
	
	@Autowired
	@Qualifier("LocaleHCDao")
	private ILocaleDao localeHCDao;
	
	@Override
	public Map<String, Locale> getAvailableLocales() {
		Map<String, Locale> map = localeHCDao.get();
		if (null == map || map.size() <= 0) {
			logger.info("map empty / null in hazelcast, Let's get available locales");
			map = get();
		}
		return map;
	}
	
	@Override
	public Map<String, Locale> get() {
		Map<String, Locale> map = new HashMap<String, Locale>();
		Locale[] locales = Locale.getAvailableLocales();
		for(Locale l : locales) {
			map.put(l.getCountry(), l);
		}
		return map;
	}
}
