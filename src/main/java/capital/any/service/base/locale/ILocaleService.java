package capital.any.service.base.locale;

import java.util.Locale;
import java.util.Map;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface ILocaleService {
	
	public static final String DEFAULT_LANGUAGE = "en";
	public static final String DEFAULT_COUNTRY = "IE";
	public static final String DEFAULT_CURRENCY = "EUR";

	Map<String, Locale> getAvailableLocales();

	Map<String, Locale> get();

}
