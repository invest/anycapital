package capital.any.service.base.gbg.users;

import java.util.Map;
import capital.any.model.base.GBGUser;

/**
 * @author LioR SoLoMoN
 *
 */
public interface IGBGUsersService {	
	/**
	 * @param gbgUser - userId, score, request, response, timeCreated, timeUpdated
	 * @return
	 */
	boolean insert(GBGUser gbgUser);
	
	/**
	 * @param gbgUser - id, score, response, timeUpdated
	 * @return
	 */
	boolean update(GBGUser gbgUser);
	
	/**
	 * @return
	 */
	Map<Long, GBGUser> get();
	
	/**
	 * @param userId
	 * @return
	 */
	GBGUser get(long userId);
}
