package capital.any.service.base.gbg.users;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import capital.any.dao.base.gbg.users.IGBGUsersDao;
import capital.any.model.base.GBGUser;

/**
 * @author LioR SoLoMoN
 *
 */
@Primary
@Service
public class GBGUsersService implements IGBGUsersService {
	private static final Logger logger = LoggerFactory.getLogger(GBGUsersService.class);
	@Autowired
	private IGBGUsersDao gbgUsersDao;
	
	
	@Override
	public boolean insert(GBGUser gbgUser) {
		return gbgUsersDao.insert(gbgUser);
	}
	
	@Override
	public boolean update(GBGUser gbgUser) {
		return gbgUsersDao.update(gbgUser);
	}
	
	@Override
	public Map<Long, GBGUser> get() {
		return gbgUsersDao.get();
	}
	
	@Override
	public GBGUser get(long userId) {
		return gbgUsersDao.get(userId);
	} 
}
