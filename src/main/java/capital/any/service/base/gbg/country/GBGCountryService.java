package capital.any.service.base.gbg.country;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import capital.any.dao.base.gbg.country.IGBGCountryDao;
import capital.any.model.base.GBGCountry;

/**
 * @author LioR SoLoMoN
 *
 */
@Primary
@Service
public class GBGCountryService implements IGBGCountryService {
	private static final Logger logger = LoggerFactory.getLogger(GBGCountryService.class);
	@Autowired
	private IGBGCountryDao gbgCountryDao; 
	
	@Override
	public Map<Integer, GBGCountry> get() {
		return gbgCountryDao.get();
	}
	
	@Override
	public GBGCountry get(int countryId) {
		return gbgCountryDao.get(countryId);
	}
}
