package capital.any.service.base.gbg;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPHeaderElement;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import capital.any.communication.Error;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.GBGRequest;
import capital.any.model.base.GBGResponse;
import capital.any.model.base.GBGUser;
import capital.any.service.IUtilService;
import capital.any.service.IXMLService;
import capital.any.service.base.gbg.users.IGBGUsersService;
import capital.any.service.base.soap.ISOAPService;

/**
 * @author LioR SoLoMoN
 *
 */
@Service
public class GBGService implements IGBGService {
	private static final Logger logger = LoggerFactory.getLogger(GBGService.class);
	@Value("${gbg.user.name}")
	private String GBG_USER_NAME;
	@Value("${gbg.password}")
	private String GBG_PASSWORD;
	@Value("${gbg.url}")
	private String GBG_URL;
	@Autowired
	private IUtilService utilService;
	@Autowired
	private IXMLService xmlService;
	@Autowired
	private ISOAPService soapService;
	@Autowired
	private IGBGUsersService gbgUsersService;

	@Override
	public Response<GBGResponse> authenticateSP(GBGRequest gbgRequest) {
		ResponseCode responseCode = ResponseCode.OK;
		List<String> messages = null;
		Error error = null;
		GBGResponse gbgResponse = null;
		SOAPConnection soapConnection = null;
		if (gbgRequest != null && !utilService.isArgumentEmptyOrNull(gbgRequest.getProfileId())) {
			try {
				gbgResponse = new GBGResponse();
				soapConnection = soapService.createSOAPConnection();
				SOAPMessage soapRequest = prepareRequest(gbgRequest);
				SOAPMessage soapResponse = soapConnection.call(soapRequest, GBG_URL);
				gbgResponse.setXmlRequest(soapToXML(soapRequest));
				getGBGResponse(soapResponse, gbgResponse);
				GBGUser gbgUser = new GBGUser(
						gbgRequest.getUserId(), 
						gbgResponse.getScore(), 
						gbgResponse.getXmlRequest(), 
						gbgResponse.getXmlResponse(), 
						new Date(), 
						new Date());
				gbgUsersService.insert(gbgUser);
			} catch (Exception e) {
				logger.error("ERROR! gbg authenticateSP", e);
				responseCode = ResponseCode.UNKNOWN_ERROR;
			} finally {
				soapService.closeConnection(soapConnection);
			}
		}
		return new Response<GBGResponse>(gbgResponse, responseCode, messages, error);
	}

	private SOAPMessage prepareRequest(GBGRequest gbgRequest) throws SOAPException, IOException {
		SOAPMessage message = MessageFactory.newInstance().createMessage();
		SOAPPart part = message.getSOAPPart();
		SOAPEnvelope envelope = part.getEnvelope();
		envelope.addNamespaceDeclaration("ns", "http://www.id3global.com/ID3gWS/2013/04");

		SOAPHeader header = envelope.getHeader();
		header.addNamespaceDeclaration("wsa", "http://www.w3.org/2005/08/addressing");
		QName security = new QName("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd",
				"Security", "wsse");
		SOAPHeaderElement securityHeader = header.addHeaderElement(security);
		securityHeader.addNamespaceDeclaration("wsu",
				"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
		securityHeader.setMustUnderstand(true);
		SOAPElement usernameToken = securityHeader.addChildElement("UsernameToken", "wsse");
		SOAPElement username = usernameToken.addChildElement("Username", "wsse");
		username.addTextNode(GBG_USER_NAME);
		SOAPElement password = usernameToken.addChildElement("Password", "wsse");
		password.addTextNode(GBG_PASSWORD);

		SOAPBody body = envelope.getBody();
		SOAPElement authenticateSP = body.addChildElement("AuthenticateSP", "ns");
		SOAPElement profileIDVersion = authenticateSP.addChildElement("ProfileIDVersion", "ns");
		SOAPElement id = profileIDVersion.addChildElement("ID", "ns");
		id.addTextNode(gbgRequest.getProfileId());
		SOAPElement version = profileIDVersion.addChildElement("Version", "ns");
		version.addTextNode(String.valueOf(gbgRequest.getVersion()));

		SOAPElement inputData = authenticateSP.addChildElement("InputData", "ns");
		SOAPElement personal = inputData.addChildElement("Personal", "ns");
		SOAPElement personalDetails = personal.addChildElement("PersonalDetails", "ns");

		addTag(gbgRequest.getFirstName(), personalDetails, "Forename");
		addTag(gbgRequest.getLastName(), personalDetails, "Surname");
		addTag(gbgRequest.getGender(), personalDetails, "Gender");
		addTag(gbgRequest.getDOBDay(), personalDetails, "DOBDay");
		addTag(gbgRequest.getDOBMonth(), personalDetails, "DOBMonth");
		addTag(gbgRequest.getDOBYear(), personalDetails, "DOBYear");

		SOAPElement addresses = inputData.addChildElement("Addresses", "ns");
		SOAPElement currentAddress = addresses.addChildElement("CurrentAddress", "ns");
		addTag(gbgRequest.getCountryName(), currentAddress, "Country");
		addTag(gbgRequest.getStreet(), currentAddress, "Street");
		addTag(gbgRequest.getAddressLine1(), currentAddress, "AddressLine1");
		addTag(gbgRequest.getCity(), currentAddress, "City");
		addTag(gbgRequest.getZipCode(), currentAddress, "ZipPostcode");
		addTag(gbgRequest.getStreetNo(), currentAddress, "Building");

		MimeHeaders headers = message.getMimeHeaders();
		headers.addHeader("SOAPAction", "http://www.id3global.com/ID3gWS/2013/04/IGlobalAuthenticate/AuthenticateSP");

		message.saveChanges();

		return message;
	}

	private void getGBGResponse(SOAPMessage soapResponse, GBGResponse response) throws Exception {
		String xml = soapToXML(soapResponse);
		response.setXmlResponse(xml);

		Document doc = xmlService.xmlToDocument(xml);
		XPath xPath = XPathFactory.newInstance().newXPath();

		String errorCode = xPath.compile("//Fault//faultcode").evaluate(doc);
		String errorReason = xPath.compile("//Fault//faultstring").evaluate(doc);
		if (errorCode == null || errorCode.equals("")) {
			int score = Integer.valueOf(xPath.compile("//AuthenticateSPResponse//AuthenticateSPResult//Score").evaluate(doc));
			response.setScore(score );
		} else {
			response.setErrorCode(errorCode);
			response.setErrorReason(errorReason);
		}
	}

	private static String soapToXML(SOAPMessage message) throws Exception {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		message.writeTo(out);
		return new String(out.toByteArray());
	}

	private static void addTag(String requestValue, SOAPElement el, String tagName) throws SOAPException {
		if (requestValue != null && !requestValue.equals("")) {
			SOAPElement e = el.addChildElement(tagName, "ns");
			e.addTextNode(requestValue);
		}
	}

}
