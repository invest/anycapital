package capital.any.service.base.gbg.score;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import capital.any.base.enums.GBGStatus;
import capital.any.dao.base.gbg.score.IGBGScoreDao;
import capital.any.model.base.GBGUser;

/**
 * @author LioR SoLoMoN
 *
 */
@Primary
@Service
public class GBGScoreService implements IGBGScoreService {
	@Autowired
	private IGBGScoreDao gbgScoreDao;
	
	@Override
	public Map<Long, GBGUser> get() {
		return gbgScoreDao.get();
	}
	
	@Override
	public GBGStatus getStatus(int score) {
		int status = gbgScoreDao.getStatus(score);
		return GBGStatus.get(status);
	}
}
