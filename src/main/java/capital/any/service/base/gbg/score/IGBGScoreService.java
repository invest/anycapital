package capital.any.service.base.gbg.score;

import java.util.Map;
import capital.any.base.enums.GBGStatus;
import capital.any.model.base.GBGUser;

/**
 * @author LioR SoLoMoN
 *
 */
public interface IGBGScoreService {	

	/**
	 * @return
	 */
	Map<Long, GBGUser> get();
	
	/**
	 * @return
	 */
	GBGStatus getStatus(int score);
	
}
