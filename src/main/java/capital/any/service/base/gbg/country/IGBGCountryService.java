package capital.any.service.base.gbg.country;

import java.util.Map;
import capital.any.model.base.GBGCountry;

/**
 * @author LioR SoLoMoN
 *
 */
public interface IGBGCountryService {	
	
	/**
	 * Get countries map
	 * @return
	 */
	Map<Integer, GBGCountry> get();
	/**
	 * @param A2
	 * @return
	 */
	GBGCountry get(int countryId);
}
