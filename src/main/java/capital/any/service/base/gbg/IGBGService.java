package capital.any.service.base.gbg;

import capital.any.communication.Response;
import capital.any.model.base.GBGRequest;
import capital.any.model.base.GBGResponse;

/**
 * @author LioR SoLoMoN
 *
 */
public interface IGBGService {
	/**
	 * @param gbgRequest
	 * @return
	 */
	Response<GBGResponse> authenticateSP(GBGRequest gbgRequest);
	
}
