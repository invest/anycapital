package capital.any.service.base.landing;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface ILandingService {
	
	/**
	 * Initial
	 * @param allRequestParams
	 * @param response
	 * @return String
	 */
	String init(Map<String,String> allRequestParams, HttpServletResponse response, HttpServletRequest request);
}
