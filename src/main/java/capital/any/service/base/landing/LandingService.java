package capital.any.service.base.landing;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.base.security.ISecureAlgorithmsService;
import capital.any.model.base.CookieInfo;
import capital.any.model.base.Country;
import capital.any.model.base.MarketingLandingPage;
import capital.any.service.HttpClientService;
import capital.any.service.IUtilService;
import capital.any.service.base.cookie.ICookieService;
import capital.any.service.base.country.ICountryService;
import capital.any.service.base.language.ILanguageService;
import capital.any.service.base.locale.ILocaleService;
import capital.any.service.base.marketingCampaign.IMarketingCampaignService;
import capital.any.service.base.marketingLandingPage.IMarketingLandingPageService;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service
public class LandingService implements ILandingService {
	private static final Logger logger = LoggerFactory.getLogger(LandingService.class);
	
	@Autowired
	private ICookieService cookieService;
	@Autowired
	private IUtilService utilService;
	@Autowired
	private ISecureAlgorithmsService secureAlgorithmsService;
	@Autowired
	private IMarketingLandingPageService marketingLandingPageService;
	@Autowired
	private HttpClientService httpClientService;
	@Autowired
	private ICountryService countryService;
	@Autowired
	private ILanguageService languageService;
	
	@Override
	public String init(Map<String,String> allRequestParams, HttpServletResponse response, HttpServletRequest request) {
		/* Get request */
		String email = allRequestParams.get(IMarketingCampaignService.EMAIL);
		String pageR = allRequestParams.get(IMarketingCampaignService.PAGE_REDIRECT);
		/* page redirect by language */
		String languageCode = ILocaleService.DEFAULT_LANGUAGE;
		Map<String,String> mapCookies = cookieService.readCookieMap(request);
		if (null != mapCookies && !utilService.isArgumentEmptyOrNull(mapCookies.get(ICookieService.LANGUAGE))) {
			String languageId = mapCookies.get(ICookieService.LANGUAGE);
			Integer langId = Integer.parseInt(languageId);
			languageCode = languageService.get().get(langId).getCode();
		} else {
			String ip = httpClientService.getIP(request);
			if (!utilService.isArgumentEmptyOrNull(ip)) {
				Country country = countryService.getByIp(ip);
				if (country != null) {
					languageCode = languageService.get().get(country.getDefaultLanguageId()).getCode();
				}
			}
		}
		String pageRedirect = IMarketingLandingPageService.INDEX_PAGE_1 + languageCode + IMarketingLandingPageService.INDEX_PAGE_2;
		/* Redirect by pageR */
		if (!utilService.isArgumentEmptyOrNull(pageR)) {
			MarketingLandingPage marketingLandingPage = marketingLandingPageService.getByName(pageR);
			if (null != marketingLandingPage) {
				pageRedirect += marketingLandingPage.getPath();
			}
		}
		/* email */
		if (!utilService.isArgumentEmptyOrNull(email)) {
			try {
				/* Decrypt */
				email = secureAlgorithmsService.decryptAES(email);
				/* Add cookies */
				cookieService.addAttribute(
						new CookieInfo(response, IMarketingCampaignService.EMAIL, email, ICookieService.Year));
			} catch (Exception e) {
				logger.error("Problem to decrypt info. ", e);
			}
		}
		return pageRedirect;
	}

}
