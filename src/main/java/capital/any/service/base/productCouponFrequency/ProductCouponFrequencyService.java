package capital.any.service.base.productCouponFrequency;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import capital.any.dao.base.productCouponFrequency.IProductCouponFrequencyDao;
import capital.any.model.base.ProductCouponFrequency;

/**
 * @author Eyal G
 *
 */
@Primary
@Service
public class ProductCouponFrequencyService implements IProductCouponFrequencyService {
	private static final Logger logger = LoggerFactory.getLogger(ProductCouponFrequencyService.class);
	@Autowired
	private IProductCouponFrequencyDao productCouponFrequencyDao;
	@Autowired
	@Qualifier("ProductCouponFrequencyHCDao")
	private IProductCouponFrequencyDao productCouponFrequencyHCDao;
	
	@Override
	public Map<Integer, ProductCouponFrequency> get() {
		Map<Integer, ProductCouponFrequency> productCouponFrequencies = productCouponFrequencyHCDao.get();
		if (productCouponFrequencies == null || productCouponFrequencies.size() <= 0) {
			logger.info("productCouponFrequencies empty / null in hazelcast, Let's check RDBMS");
			productCouponFrequencies = productCouponFrequencyDao.get(); 
		}
		return productCouponFrequencies;
	}
}
