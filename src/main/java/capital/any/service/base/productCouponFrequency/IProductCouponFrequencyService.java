package capital.any.service.base.productCouponFrequency;

import java.util.Map;
import capital.any.model.base.ProductCouponFrequency;

/**
 * @author Eyal G
 *
 */
public interface IProductCouponFrequencyService {	
	
	/**
	 * Get Product Statuses hashmap
	 * @return
	 */
	Map<Integer, ProductCouponFrequency> get();
}
