package capital.any.service.base.issueReachedStatuses;

import java.util.Map;

import capital.any.model.base.IssueReachedStatus;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IIssueReachedStatusService {

	Map<Integer, IssueReachedStatus> get();

}
