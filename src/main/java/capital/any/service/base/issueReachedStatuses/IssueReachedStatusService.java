package capital.any.service.base.issueReachedStatuses;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import capital.any.dao.base.issueReachedStatus.IIssueReachedStatusDao;
import capital.any.model.base.IssueReachedStatus;

/**
 * 
 * @author eyal.ohana
 *
 */
@Primary
@Service
public class IssueReachedStatusService implements IIssueReachedStatusService {
	private static final Logger logger = LoggerFactory.getLogger(IssueReachedStatusService.class);
			
	@Autowired
	private IIssueReachedStatusDao issueReachedStatusDao;
	@Autowired
	@Qualifier("IssueReachedStatusHCDao")
	private IIssueReachedStatusDao issueReachedStatusHCDao;
	
	@Override
	public Map<Integer, IssueReachedStatus> get() {
		Map<Integer, IssueReachedStatus> issueReachedStatuss = issueReachedStatusHCDao.get();
		if (issueReachedStatuss == null || issueReachedStatuss.size() <= 0) {
			logger.info("issueReachedStatuss empty / null in hazelcast, Let's check RDBMS");
			issueReachedStatuss = issueReachedStatusDao.get();
		}
		return issueReachedStatuss;
	}

}
