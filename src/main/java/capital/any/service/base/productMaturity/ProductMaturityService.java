package capital.any.service.base.productMaturity;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import capital.any.dao.base.productMaturity.IProductMaturityDao;
import capital.any.model.base.ProductMaturity;

/**
 * @author Eyal G
 *
 */
@Primary
@Service
public class ProductMaturityService implements IProductMaturityService {
	private static final Logger logger = LoggerFactory.getLogger(ProductMaturityService.class);
	@Autowired
	private IProductMaturityDao productMaturityDao;
	@Autowired
	@Qualifier("ProductMaturityHCDao")
	private IProductMaturityDao productMaturityHCDao;

	@Override
	public Map<Integer, ProductMaturity> get() {
		Map<Integer, ProductMaturity> productMaturities = productMaturityHCDao.get();
		if (productMaturities == null || productMaturities.size() <= 0) {
			logger.info("productMaturities empty / null in hazelcast, Let's check RDBMS");
			productMaturities = productMaturityDao.get();
		}
		return productMaturities;
	}
}
