package capital.any.service.base.productMaturity;

import java.util.Map;
import capital.any.model.base.ProductMaturity;

/**
 * @author Eyal G
 *
 */
public interface IProductMaturityService {	
	
	/**
	 * Get Product Maturity hashmap
	 * @return
	 */
	Map<Integer, ProductMaturity> get();
}
