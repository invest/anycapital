package capital.any.service.base.marketingLandingPage;

import java.util.Map;

import capital.any.model.base.MarketingLandingPage;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IMarketingLandingPageService {
	
	public static final String INDEX_PAGE_1 = "#/";
	public static final String INDEX_PAGE_2 = "/";
	
	/**
	 * Insert marketing landing page 
	 * @param marketingLandingPage
	 */
	boolean insert(MarketingLandingPage marketingLandingPage) throws Exception;
	
	/**
	 * Update marketing landing page
	 * @param marketingLandingPage
	 */
	boolean update(MarketingLandingPage marketingLandingPage) throws Exception;
	
	/**
	 * Get all marketing landing page
	 * @return Map<Integer, MarketingLandingPage>
	 */
	Map<Integer, MarketingLandingPage> getAll();
	
	/**
	 * Get marketing landing page
	 * @param id
	 * @return MarketingLandingPage
	 */
	MarketingLandingPage getMarketingLandingPage(int id);

	/**
	 * Get by name
	 * @param name
	 * @return MarketingLandingPage
	 */
	MarketingLandingPage getByName(String name);
}
