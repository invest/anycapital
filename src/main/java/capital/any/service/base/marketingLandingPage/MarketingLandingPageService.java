package capital.any.service.base.marketingLandingPage;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import capital.any.dao.base.marketingLandingPage.IMarketingLandingPageDao;
import capital.any.model.base.MarketingLandingPage;

/**
 * 
 * @author eyal.ohana
 *
 */
@Primary
@Service
public class MarketingLandingPageService implements IMarketingLandingPageService {
	private static final Logger logger = LoggerFactory.getLogger(MarketingLandingPageService.class);

	@Autowired
	private IMarketingLandingPageDao marketingLandingPageDao;
	@Autowired
	@Qualifier("MarketingLandingPageHCDao")
	private IMarketingLandingPageDao marketingLandingPageHCDao;
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean insert(MarketingLandingPage marketingLandingPage) throws Exception {
		boolean result = marketingLandingPageDao.insert(marketingLandingPage);
		if (result) {
			result = marketingLandingPageHCDao.insert(marketingLandingPage);
		}
		return result;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean update(MarketingLandingPage marketingLandingPage) throws Exception {
		boolean result = marketingLandingPageDao.update(marketingLandingPage);
		if (result) {
			result = marketingLandingPageHCDao.update(marketingLandingPage);
		}
		return result;
	}

	@Override
	public Map<Integer, MarketingLandingPage> getAll() {
		Map<Integer, MarketingLandingPage> mapMarketingLandingPage = marketingLandingPageHCDao.getAll();
		if (null == mapMarketingLandingPage || mapMarketingLandingPage.size() <= 0) {
			logger.info("mapMarketingLandingPage empty / null in hazelcast, Let's check RDBMS");
			mapMarketingLandingPage = marketingLandingPageDao.getAll();
		}
		return mapMarketingLandingPage;
	}

	@Override
	public MarketingLandingPage getMarketingLandingPage(int id) {
		MarketingLandingPage marketingLandingPage = marketingLandingPageHCDao.getMarketingLandingPage(id);
		if (marketingLandingPage == null) {
			logger.info("marketingLandingPage empty / null in hazelcast, Let's check RDBMS");
			marketingLandingPage = marketingLandingPageDao.getMarketingLandingPage(id);
		}
		return marketingLandingPage;
	}
	
	@Override
	public MarketingLandingPage getByName(String name) {
		MarketingLandingPage marketingLandingPage = marketingLandingPageHCDao.getByName(name);
		if (marketingLandingPage == null) {
			marketingLandingPage = marketingLandingPageDao.getByName(name);
		}
		return marketingLandingPage;
	}

}
