package capital.any.service.base.xlsx;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * @author Eyal.o
 *
 */
@Service
public class XlsxService implements IXlsxService {
	private static final Logger logger = LoggerFactory.getLogger(XlsxService.class);
	
	@SuppressWarnings({ "resource", "deprecation" })
	@Override
	public List<List<String>> readDocument(MultipartFile fileUpload) {
		List<List<String>> docInfo = new ArrayList<List<String>>();
		try {
			DataFormatter formatter = new DataFormatter();
			InputStream is = fileUpload.getInputStream();
            Workbook workbook = new XSSFWorkbook(is);
            Sheet datatypeSheet = workbook.getSheetAt(0);
            Iterator<Row> iterator = datatypeSheet.iterator();
            while (iterator.hasNext()) {
                Row currentRow = iterator.next();
                Iterator<Cell> cellIterator = currentRow.iterator();
                List<String> lineList = new ArrayList<String>();
                while (cellIterator.hasNext()) {
                    Cell currentCell = cellIterator.next();                    
                    lineList.add(formatter.formatCellValue(currentCell));
                }
                docInfo.add(lineList);
            }
        } catch (Exception e) {
            logger.error("Problem with read xlsx document.", e);
        }
		return docInfo;
	}
}
