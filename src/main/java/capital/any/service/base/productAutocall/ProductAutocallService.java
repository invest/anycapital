package capital.any.service.base.productAutocall;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.productAutocall.IProductAutocallDao;
import capital.any.model.base.ProductAutocall;

/**
 * @author eranl
 *
 */
@Service
public class ProductAutocallService implements IProductAutocallService {
	
	@Autowired
	private IProductAutocallDao productAutocallDao;
	
	@Override
	public HashMap<Long, List<ProductAutocall>> getProductAutocalls() {
		return productAutocallDao.getProductAutocalls(); 
	}

	@Override
	public List<ProductAutocall> getProductAutocalls(long productId) {
		return productAutocallDao.getProductAutocalls(productId);
	}

}
