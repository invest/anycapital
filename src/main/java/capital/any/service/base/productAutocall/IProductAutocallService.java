package capital.any.service.base.productAutocall;

import java.util.HashMap;
import java.util.List;

import capital.any.model.base.ProductAutocall;

/**
 * @author eranl
 *
 */
public interface IProductAutocallService {	
	
	/**
	 * Get Product Autocalls hashmap
	 * @return
	 */
	HashMap<Long, List<ProductAutocall>> getProductAutocalls();
	
	List<ProductAutocall> getProductAutocalls(long productId);
}
