package capital.any.service.base.dayCountConvention;

import java.util.Map;
import capital.any.model.base.DayCountConvention;

/**
 * @author eranl
 *
 */
public interface IDayCountConventionService {	
	
	/**
	 * Get Day Count Conventions hashmap
	 * @return
	 */
	Map<Integer, DayCountConvention> get();
}
