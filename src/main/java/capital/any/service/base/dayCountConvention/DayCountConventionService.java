package capital.any.service.base.dayCountConvention;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import capital.any.dao.base.dayCountConvention.IDayCountConventionDao;
import capital.any.model.base.DayCountConvention;

/**
 * @author eranl
 *
 */
@Primary
@Service
public class DayCountConventionService implements IDayCountConventionService {
	private static final Logger logger = LoggerFactory.getLogger(DayCountConventionService.class);
	@Autowired
	private IDayCountConventionDao dayCountConventionDao;
	@Autowired
	@Qualifier("DayCountConventionHCDao")
	private IDayCountConventionDao dayCountConventionHCDao;
	@Override
	public Map<Integer, DayCountConvention> get() {
		Map<Integer, DayCountConvention> dayCountConventions = dayCountConventionHCDao.get();
		if (dayCountConventions == null || dayCountConventions.size() <= 0) {
			logger.info("dayCountConventions empty / null in hazelcast, Let's check RDBMS");
			dayCountConventions = dayCountConventionDao.get(); 
		}
		return dayCountConventions;
	}
}
