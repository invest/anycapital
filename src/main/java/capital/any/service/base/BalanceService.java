package capital.any.service.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import capital.any.model.base.BalanceHistory;
import capital.any.model.base.BalanceRequest;
import capital.any.service.base.investment.IInvestmentService;

/**
 * @author LioR SoLoMoN
 *
 */
@Service
public class BalanceService implements IBalanceService {
	private static final Logger logger = LoggerFactory.getLogger(BalanceService.class);	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private NamedParameterJdbcTemplate npjt;
	@Autowired
	private IInvestmentService investmentService;

	public static final String GET_BALANCE = 
			"SELECT "
			+ "		balance "
			+ "FROM "
			+ "		users "
			+ "WHERE "
			+ "		id = :userId";
	
	public static final String UPDATE_BALANCE =
			" UPDATE "
			+ "		users "
			+ "SET "
			+ "		balance = balance + :amount "
			+ "		,time_modified = now() "
			+ "WHERE "
			+ "		id = :userId";	

	public static final String INSERT_BALANCE_HISTORY = 
			" INSERT INTO balance_history "
			+ "	( "
			+ "		user_id "
			+ "		,reference_id "
			+ " 	,balance "
			+ "		,balance_history_command_id "
			+ "	)" +
			" VALUES "
			+ "	( "
			+ "		:userId "
			+ "		,:referenceId " 
			+ "		,:balance " 
			+ "		,:balanceHistoryCommandId " 
			+ "	) ";

	@Override
	@Transactional
	public void changeBalance(BalanceRequest balanceRequest) throws Exception {
		switch (balanceRequest.getOperationType()) {
		case CREDIT:
			credit(balanceRequest);
			investmentService.buyPendingInvestments(balanceRequest);
			break;
		case DEBIT:
			debit(balanceRequest);
			break;
		default: 
			throw new Exception();
		}
	}

	/**
	 * Add balance
	 * 
	 * @param balanceHistory
	 * @throws Exception 
	 */
	private void credit(BalanceRequest balanceRequest) {
		logger.info("before; " + balanceRequest.toString());		
		long balanceBefore = getBalance(balanceRequest.getUser().getId());
		long balanceAfter = Math.addExact(balanceBefore, balanceRequest.getAmount());
		
		databaseChanges(balanceRequest, balanceAfter, balanceRequest.getAmount());
		balanceRequest.getUser().setBalance(balanceAfter);
		logger.info("after; " + balanceRequest.toString());
	}

	/**
	 * Subtract balance, if applicable.
	 * 
	 * @param balanceHistory
	 */
	private void debit(BalanceRequest balanceRequest) {
		logger.info("before; " + balanceRequest.toString());
		long balanceBefore = getBalance(balanceRequest.getUser().getId());
		long balanceAfter = Math.subtractExact(balanceBefore, balanceRequest.getAmount());
		// amount withdrawn exceeds the current balance
		if (balanceAfter >= 0) {
			databaseChanges(balanceRequest, balanceAfter, -balanceRequest.getAmount());			
			balanceRequest.getUser().setBalance(balanceAfter);
		} else {
			logger.warn("amount withdrawn exceeds the current balance! \n"
					+ "balanceBefore - amount = balanceAfter: " + balanceBefore + " - " + balanceRequest.getAmount() + " = "
					+ balanceAfter);
			// TODO something. fix balance / validation / exception
		}
		logger.info("after; " + balanceRequest.toString());
	}

	/**
	 * @param balanceRequest
	 * @param balance - the new balance after change
	 * @throws Exception 
	 */
	private void databaseChanges(BalanceRequest balanceRequest, long balanceAfter, long amount)  {	
		updateBalance(balanceRequest.getUser().getId(), amount);
		BalanceHistory balanceHistory = new BalanceHistory(balanceRequest.getUser().getId(), 
				balanceRequest.getReferenceId(), balanceAfter, balanceRequest.getCommandHistory());
		insertBalanceHistory(balanceHistory);
	}
	
	/**
	 * @param userId
	 * @return
	 */
	public long getBalance(long userId) {
		SqlParameterSource namedParameters = new MapSqlParameterSource("userId", userId);
		return jdbcTemplate.queryForObject(GET_BALANCE, namedParameters, Long.class);
	}

	/**
	 * @param userId
	 * @param amount
	 */
	private void updateBalance(long userId, long amount) {
		logger.info("updateBalance; userId: " + userId + "; amount:" + amount + "; ");
		SqlParameterSource namedParameters = new MapSqlParameterSource("userId", userId).addValue("amount", amount);
		jdbcTemplate.update(UPDATE_BALANCE, namedParameters);
	}

	/**
	 * @param balanceHistory
	 */
	private void insertBalanceHistory(BalanceHistory balanceHistory) {
		SqlParameterSource namedParameters = new MapSqlParameterSource("balance", balanceHistory.getBalance())
				.addValue("balanceHistoryCommandId", balanceHistory.getBalanceHistoryCommand().getId())
				.addValue("referenceId", balanceHistory.getReferenceId())
				.addValue("userId", balanceHistory.getUserId());

		npjt.update(INSERT_BALANCE_HISTORY, namedParameters);
	}
}
