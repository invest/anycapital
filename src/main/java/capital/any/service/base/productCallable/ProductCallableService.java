package capital.any.service.base.productCallable;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.productCallable.IProductCallableDao;
import capital.any.model.base.ProductCallable;

/**
 * @author eranl
 *
 */
@Service
public class ProductCallableService implements IProductCallableService {
	
	@Autowired
	private IProductCallableDao productCallableDao;
	
	@Override
	public HashMap<Long, List<ProductCallable>> getProductCallables() {
		return productCallableDao.getProductCallables(); 
	}

	@Override
	public List<ProductCallable> getProductCallables(long productId) {
		return productCallableDao.getProductCallables(productId);
	}

}
