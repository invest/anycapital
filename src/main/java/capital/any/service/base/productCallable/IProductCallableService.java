package capital.any.service.base.productCallable;

import java.util.HashMap;
import java.util.List;

import capital.any.model.base.ProductCallable;

/**
 * @author eranl
 *
 */
public interface IProductCallableService {	
	
	/**
	 * Get Product Callables hashmap
	 * @return
	 */
	HashMap<Long, List<ProductCallable>> getProductCallables();
	
	List<ProductCallable> getProductCallables(long productId);
}
