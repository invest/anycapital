package capital.any.service.base.role;

import java.util.List;

import capital.any.model.base.Role;

public interface IRoleService {
	
	List<Role> getUserRoles(long userId);

}
