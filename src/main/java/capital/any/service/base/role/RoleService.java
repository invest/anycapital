package capital.any.service.base.role;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import capital.any.dao.base.role.IRoleDao;
import capital.any.model.base.Role;

/**
 * @author eranl
 *
 */
@Service
public class RoleService implements IRoleService {
	
	@Autowired
	private IRoleDao roleDao;

	@Override
	public List<Role> getUserRoles(long userId) {
		return roleDao.getRoleByUserId(userId); 
	} 

}
