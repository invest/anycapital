package capital.any.service.base.investmentReject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.investmentReject.IInvestmentRejectDao;
import capital.any.model.base.InvestmentReject;
import capital.any.service.base.IServerConfiguration;

/**
 * @author Eyal G
 *
 */
@Service
public class InvestmentRejectService implements IInvestmentRejectService {
	private static final Logger logger = LoggerFactory.getLogger(InvestmentRejectService.class);
	
	@Autowired
	private IInvestmentRejectDao investmentRejectDao;
	
	@Autowired
	private IServerConfiguration serverConfiguration;

	@Override
	public void insert(InvestmentReject investmentReject) {
		try {
			investmentReject.setServerName(serverConfiguration.getServerName());
			investmentRejectDao.insert(investmentReject);
		} catch (Exception e) {
			logger.error("Can't insert investment reject", e);
		}		
	}
}
