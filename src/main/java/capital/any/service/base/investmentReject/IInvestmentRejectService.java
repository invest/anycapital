package capital.any.service.base.investmentReject;

import capital.any.model.base.InvestmentReject;

/**
 * 
 * @author eyal.goren
 *
 */
public interface IInvestmentRejectService {
	/**
	 * insert investment Reject
	 * @param investment Reject
	 */
	void insert(InvestmentReject investmentReject);
}
