package capital.any.service.base.language;

import java.util.Map;
import capital.any.model.base.Language;

/**
 * @author eranl
 *
 */
public interface ILanguageService {
	
	/**
	 * Get languages hashmap 
	 * @return
	 */
	Map<Integer, Language> get();
	
	/**
	 * Get language object by code
	 * @param code
	 * @return
	 */
	Language getByCode(String code);
}
