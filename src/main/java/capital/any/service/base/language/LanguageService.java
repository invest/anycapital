package capital.any.service.base.language;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import capital.any.dao.base.language.ILanguageDao;
import capital.any.model.base.Language;

/**
 * @author eranl
 *
 */
@Service
public class LanguageService implements ILanguageService {
	private static final Logger logger = LoggerFactory.getLogger(LanguageService.class);
	@Autowired
	private ILanguageDao languageDao;
	@Autowired
	@Qualifier("LanguageHCDao")
	private ILanguageDao languageHCDao;

	@Override
	public Map<Integer, Language> get() {
		Map<Integer, Language> languages = languageHCDao.get();
		if (languages == null || languages.size() <= 0) {
			logger.info("languages empty / null in hazelcast, Let's check RDBMS");
			languages = languageDao.get(); 
		}
		return languages;
	}
	
	@Override
	public Language getByCode(String code) {
		Language language = languageHCDao.getByCode(code);
		if (language == null) {
			logger.info("language empty / null in hazelcast, Let's check RDBMS");
			language = languageDao.getByCode(code);
		}
		return language;
	}
}
