package capital.any.service.base.marketingContent;

import java.util.Map;

import capital.any.model.base.MarketingContent;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IMarketingContentService {
	
	public static String NAME_SEPARATOR = "_-_";
	
	/**
	 * Insert marketing content 
	 * @param marketingContent
	 */
	void insert(MarketingContent marketingContent) throws Exception;
	
	/**
	 * Update marketing content
	 * @param marketingContent
	 */
	void update(MarketingContent marketingContent) throws Exception;
	
	/**
	 * Get all marketing content
	 * @return Map<Integer, MarketingContent>
	 */
	Map<Integer, MarketingContent> getAll();
	
	/**
	 * Get marketing content
	 * @param id
	 * @return MarketingContent
	 */
	MarketingContent getMarketingContent(int id);
}
