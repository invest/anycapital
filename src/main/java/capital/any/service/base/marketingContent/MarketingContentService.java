package capital.any.service.base.marketingContent;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import capital.any.dao.base.marketingContent.IMarketingContentDao;
import capital.any.model.base.MarketingContent;
import capital.any.service.base.aws.IAwsService;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service
public class MarketingContentService implements IMarketingContentService {

	@Autowired
	private IMarketingContentDao marketingContentDao;
	
	@Autowired
	private IAwsService awsService;
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void insert(MarketingContent marketingContent) throws Exception {
		// insert to DB
		marketingContentDao.insert(marketingContent);
		// insert to aws
		if (marketingContent.getFileUpload() != null) {
			String fileName = marketingContent.getId() + IMarketingContentService.NAME_SEPARATOR + marketingContent.getFileUpload().getOriginalFilename();  
			awsService.uploadMarketingContentHTMLFile(marketingContent.getFileUpload(), fileName, "text/html");
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void update(MarketingContent marketingContent) throws Exception {				
		String fileName = "";
		if (marketingContent.getFileUpload() != null) {
			fileName = marketingContent.getId() + IMarketingContentService.NAME_SEPARATOR + marketingContent.getFileUpload().getOriginalFilename();
			marketingContent.setHTMLFilePath(fileName);
		}		
		// update DB
		marketingContentDao.update(marketingContent);
		// update aws
		if (marketingContent.getFileUpload() != null) {			  
			awsService.uploadMarketingContentHTMLFile(marketingContent.getFileUpload(), fileName, "text/html");
		}		
	}

	@Override
	public Map<Integer, MarketingContent> getAll() {
		Map<Integer, MarketingContent> marketingContents = marketingContentDao.getAll();
		return marketingContents;
	}

	@Override
	public MarketingContent getMarketingContent(int id) {
		return marketingContentDao.getMarketingContent(id);		
	}

}
