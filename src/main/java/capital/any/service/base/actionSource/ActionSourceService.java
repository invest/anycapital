package capital.any.service.base.actionSource;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.actionSource.IActionSourceDao;
import capital.any.model.base.ActionSource;

/**
 * 
 * @author Eyal.o
 *
 */
@Service
public class ActionSourceService implements IActionSourceService {

	@Autowired
	private IActionSourceDao actionSourceDao;
	
	@Override
	public Map<Integer, ActionSource> get() {
		return actionSourceDao.get();
	}

}
