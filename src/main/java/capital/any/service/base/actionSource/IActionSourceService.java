package capital.any.service.base.actionSource;

import java.util.Map;

import capital.any.model.base.ActionSource;

/**
 * 
 * @author Eyal.o
 *
 */
public interface IActionSourceService {
	
	Map<Integer, ActionSource> get();

}
