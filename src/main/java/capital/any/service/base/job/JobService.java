package capital.any.service.base.job;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.job.IJobDao;
import capital.any.model.base.Job;

/**
 * @author eranl
 *
 */
@Service
public class JobService implements IJobService {
	
	@Autowired
	private IJobDao jobDao;

	@Override
	public HashMap<Integer, Job> getJobs() {
		return jobDao.getJobs();
	}
	
	@Override
	public void updateJobById(int id) {
		jobDao.updateJobById(id);
	}


}
