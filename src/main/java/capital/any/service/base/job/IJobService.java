package capital.any.service.base.job;

import java.util.HashMap;

import capital.any.model.base.Job;

/**
 * @author eranl
 *
 */
public interface IJobService {

	HashMap<Integer, Job> getJobs();
	
	void updateJobById(int id);
	
}
