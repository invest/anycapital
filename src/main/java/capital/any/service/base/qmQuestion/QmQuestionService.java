package capital.any.service.base.qmQuestion;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import capital.any.dao.base.qmQuestion.IQmQuestionDao;
import capital.any.model.base.QmQuestion;

/**
 * 
 * @author eyal.ohana
 *
 */
@Primary
@Service
public class QmQuestionService implements IQmQuestionService {
	private static final Logger logger = LoggerFactory.getLogger(QmQuestionService.class);
	
	@Autowired
	private IQmQuestionDao qmQuestionDao;
	@Autowired
	@Qualifier("QmQuestionHCDao")
	private IQmQuestionDao qmQuestionHCDao;
	
	@Override
	public Map<Integer, QmQuestion> getAll() {
		Map<Integer, QmQuestion> mapQuestions = qmQuestionHCDao.getAll();
		if (mapQuestions == null || mapQuestions.size() <= 0) {
		logger.info("mapQuestions empty / null in hazelcast, Let's check RDBMS");
			mapQuestions = qmQuestionDao.getAll();
		}
		return mapQuestions;
	}
}
