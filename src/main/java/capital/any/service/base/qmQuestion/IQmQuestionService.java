package capital.any.service.base.qmQuestion;

import java.util.Map;

import capital.any.model.base.QmQuestion;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IQmQuestionService {

	Map<Integer, QmQuestion> getAll();

}
