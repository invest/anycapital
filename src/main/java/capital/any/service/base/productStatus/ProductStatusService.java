package capital.any.service.base.productStatus;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import capital.any.dao.base.productStatus.IProductStatusDao;
import capital.any.model.base.ProductStatus;


/**
 * @author eranl
 *
 */
@Primary
@Service
public class ProductStatusService implements IProductStatusService {
	private static final Logger logger = LoggerFactory.getLogger(ProductStatusService.class);
	@Autowired
	private IProductStatusDao productStatusDao;
	@Autowired
	@Qualifier("ProductStatusHCDao")
	private IProductStatusDao productStatusHCDao;

	@Override
	public Map<Integer, ProductStatus> get() {
		Map<Integer, ProductStatus> productStatuses = productStatusHCDao.get();
		if (productStatuses == null || productStatuses.size() <= 0) {
			logger.info("productStatuses empty / null in hazelcast, Let's check RDBMS");
			productStatuses = productStatusDao.get(); 
		}
		return productStatuses;
	}
}
