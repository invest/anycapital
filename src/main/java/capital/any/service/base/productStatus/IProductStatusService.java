package capital.any.service.base.productStatus;

import java.util.Map;
import capital.any.model.base.ProductStatus;

/**
 * @author eranl
 *
 */
public interface IProductStatusService {	
	
	/**
	 * Get Product Statuses hashmap
	 * @return
	 */
	Map<Integer, ProductStatus> get();
}
