package capital.any.service.base.issueSubjects;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import capital.any.dao.base.issueSubject.IIssueSubjectDao;
import capital.any.model.base.IssueSubject;

/**
 * 
 * @author eyal.ohana
 *
 */
@Primary
@Service
public class IssueSubjectService implements IIssueSubjectService {
	private static final Logger logger = LoggerFactory.getLogger(IssueSubjectService.class);
			
	@Autowired
	private IIssueSubjectDao issueSubjectDao;
	@Autowired
	@Qualifier("IssueSubjectHCDao")
	private IIssueSubjectDao issueSubjectHCDao;
	
	@Override
	public Map<Integer, IssueSubject> get() {
		Map<Integer, IssueSubject> issueSubjects = issueSubjectHCDao.get();
		if (issueSubjects == null || issueSubjects.size() <= 0) {
			logger.info("issueSubjects empty / null in hazelcast, Let's check RDBMS");
			issueSubjects = issueSubjectDao.get();
		}
		return issueSubjects;
	}

}
