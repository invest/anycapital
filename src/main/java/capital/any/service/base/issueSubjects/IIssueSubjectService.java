package capital.any.service.base.issueSubjects;

import java.util.Map;

import capital.any.model.base.IssueSubject;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IIssueSubjectService {

	Map<Integer, IssueSubject> get();

}
