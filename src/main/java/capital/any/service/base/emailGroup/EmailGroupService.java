package capital.any.service.base.emailGroup;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.emailGroups.IEmailGroupsDao;
import capital.any.model.base.EmailGroup;

/**
 * @author Eyal G
 *
 */
@Service
public class EmailGroupService implements IEmailGroupService {
	
	private static final Logger logger = LoggerFactory.getLogger(EmailGroupService.class);

	@Autowired
	private IEmailGroupsDao emailGroupsDao;
	
	private Map<Integer, EmailGroup> emailGroups;
	
	@Override
	public String get(Integer id) {
		if (emailGroups == null) {
			emailGroups = emailGroupsDao.get();
		}
		EmailGroup emailGroup = emailGroups.get(id);
		String email = null;
		if (emailGroup != null) {
			email = emailGroup.getEmail();
		}
		return email;
	}	
}
