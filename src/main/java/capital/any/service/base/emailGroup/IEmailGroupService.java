package capital.any.service.base.emailGroup;

/**
 * @author @author Eyal G
 *
 */
public interface IEmailGroupService {
	

	/**
	 * get email for this group
	 * @return email of the group
	 */
	String get(Integer id);
}
