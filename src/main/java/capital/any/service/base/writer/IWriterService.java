package capital.any.service.base.writer;

import java.util.Map;
import capital.any.model.base.Writer;

/**
 * @author Eran
 *
 */
public interface IWriterService {
	
	/**
	 * Get writer by Username
	 * @param userName
	 * @return
	 */
	Writer getWriterByUsername(String userName);
	
	/**
	 * Get writer by id
	 * @param id
	 * @return
	 */
	Writer getWriterById(Integer id);
	
	/**
	 * Get writer by email
	 * @param email
	 * @return
	 */
	Writer getWriterByEmail(String email);
//	Map<Writer, List<Role>> getWritersWithRoles();
	
	/**
	 * @return all writers
	 */
	Map<Integer, Writer> get();

	/**
	 * @param emailOrUserName
	 * @return
	 */
	Writer getWriterByEmailOrUserName(String emailOrUserName);
	
	/**
	 * Insert writer
	 * @param writer
	 */
	boolean insert(Writer writer) throws Exception;
	
	/**
	 * Update writer
	 * @param writer
	 */
	boolean update(Writer writer) throws Exception;

}
