package capital.any.service.base.writer;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import capital.any.dao.base.writer.IWriterDao;
import capital.any.model.base.Writer;

/**
 * @author Eran
 *
 */
@Primary
@Service
public class WriterService implements IWriterService {
	private static final Logger logger = LoggerFactory.getLogger(WriterService.class);
	@Autowired
	private IWriterDao writerDao;
	@Autowired
	@Qualifier("WriterHCDao")
	private IWriterDao writerHCDao;

	@Override
	public Writer getWriterByUsername(String userName) {
		return writerDao.getWriterByUsername(userName);
	}
	
	@Override
	public Writer getWriterById(Integer id) {
		return writerDao.getWriterById(id);
	}
	
	@Override
	public Writer getWriterByEmail(String email) {
		return writerDao.getWriterByEmail(email);
	}
	
	@Override
	public Writer getWriterByEmailOrUserName(String emailOrUserName) {
		return writerDao.getWriterByEmailOrUserName(emailOrUserName);
	}

	@Override
	public Map<Integer, Writer> get() {
		Map<Integer, Writer> writers = writerHCDao.get();
		if (writers == null || writers.size() <= 0) {
			logger.info("writers empty / null in hazelcast, Let's check RDBMS");
			writers = writerDao.get(); 
		}
		return writers;
	}

	@Override
	public boolean insert(Writer writer) throws Exception {
		boolean result = writerDao.insert(writer); 
		if (result) {
			result = writerHCDao.insert(writer);
		}
		return result; 		
	}

	@Override
	public boolean update(Writer writer) throws Exception {
		boolean result = writerDao.update(writer);
		if (result) {
			result = writerHCDao.update(writer);
		}
		return result;		
	}
	
	
	

}
