package capital.any.service.base.transactionReference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.transactionReference.ITransactionReferenceDao;
import capital.any.model.base.TransactionReference;

/**
 * @author Eyal Goren
 *
 */
@Service
public class TransactionReferenceService implements ITransactionReferenceService {
	private static final Logger logger = LoggerFactory.getLogger(TransactionReferenceService.class);
	
	@Autowired
	private ITransactionReferenceDao transactionReferenceDao;
	
	@Override
	public void insert(TransactionReference transactionReference) {
		transactionReferenceDao.insert(transactionReference);
	}
}
