package capital.any.service.base.transactionReference;

import capital.any.model.base.TransactionReference;

/**
 * @author Eyal Goren
 *
 */
public interface ITransactionReferenceService {	
	
	/**
	 * insert {@link TransactionReference} into db
	 * @param transactionReference
	 */
	void insert(TransactionReference transactionReference);
}
