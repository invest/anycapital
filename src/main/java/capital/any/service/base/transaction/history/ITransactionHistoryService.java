package capital.any.service.base.transaction.history;

import capital.any.model.base.TransactionHistory;

/**
 * @author LioR SoLoMoN
 *
 */
public interface ITransactionHistoryService {

	/**
	 * @param transactionId
	 * @param status
	 * @return
	 */
	TransactionHistory get(TransactionHistory transactionHistory);

	/**
	 * @param transactionHistory
	 * @return 
	 */
	boolean insert(TransactionHistory transactionHistory);

}
