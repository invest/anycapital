package capital.any.service.base.transaction.history;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import capital.any.dao.base.transaction.history.ITransactionHistoryDao;
import capital.any.model.base.TransactionHistory;

/**
 * @author LioR SoLoMoN
 *
 */
@Service
public class TransactionHistoryService implements ITransactionHistoryService {
	@Autowired
	private ITransactionHistoryDao transactionHistoryDao;

	@Override
	public TransactionHistory get(TransactionHistory transactionHistory) {
		return transactionHistoryDao.get(transactionHistory);
	}

	@Override
	public boolean insert(TransactionHistory transactionHistory) {
		return transactionHistoryDao.insert(transactionHistory);		
	}
}