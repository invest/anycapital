package capital.any.service.base.transaction;

import java.util.List;
import java.util.Map;

import capital.any.model.base.SqlFilters;
import capital.any.model.base.Transaction;
import capital.any.model.base.TransactionHistory;

/**
 * @author LioR SoLoMoN
 *
 */
public interface ITransactionService {
	
	/**
	 * Get transaction by id
	 * @param id
	 * @return
	 */
	Transaction getTransactionById(long id);
	
	/**
	 * Get transactions list by filters
	 * @param {@link SqlFilters}
	 * @return {@link List<Transaction>}
	 */
	public List<Transaction> getTransactions(SqlFilters filters);
			
	/**
	 * Insert transaction
	 * @param transaction
	 */
	void insertTransaction(Transaction transaction);
	
	/**
	 * insert transaction with history
	 * @param transaction
	 * @param history
	 */
	void insertTransaction(Transaction transaction, TransactionHistory history);
	
	/**
	 * Update transaction
	 * @param transaction
	 */
	void updateTransaction(Transaction transaction);
	
	/**
	 * @param transaction
	 * @param history
	 * @return 
	 */
	boolean updateTransaction(Transaction transaction, TransactionHistory history);

	/**
	 * @return
	 */
	Map<Long, Transaction> getTransactions();
	
	/**
	 * @param userId
	 * @return
	 */
	boolean isUserHaveOpenWithdraw(long userId);

	

}
