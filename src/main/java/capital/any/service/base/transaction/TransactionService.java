package capital.any.service.base.transaction;

import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import capital.any.dao.base.transaction.ITransactionDao;
import capital.any.model.base.SqlFilters;
import capital.any.model.base.Transaction;
import capital.any.model.base.TransactionHistory;
import capital.any.service.base.transaction.history.ITransactionHistoryService;
import capital.any.service.base.transactionCoupon.ITransactionCouponService;
import capital.any.service.base.transactionReference.ITransactionReferenceService;

/**
 * @author LioR SoLoMoN
 *
 */
@Service
public class TransactionService implements ITransactionService {
	private static final Logger logger = LoggerFactory.getLogger(TransactionService.class);
	@Autowired
	private ITransactionDao transactionDao;
	@Autowired
	private ITransactionHistoryService transactionHistorySerivce;
	@Autowired
	private ITransactionReferenceService transactionReferenceService;
	@Autowired
	private ITransactionCouponService transactionCouponService;
	
	/**
	 * Get transactions list by user id
	 * @param userId
	 * @return
	 */
	public List<Transaction> getTransactions(SqlFilters filters) {
		return transactionDao.getTransactions(filters);
	}
		
	public Transaction getTransactionById(long id) {
		return transactionDao.getTransaction(id); 
	}

	/**
	 * Insert transaction
	 * @param transaction
	 */
	public void insertTransaction(Transaction transaction) {
		transactionDao.insertTransaction(transaction);
		if (transaction.getReference() != null) {
			transaction.getReference().setTransactionId(transaction.getId());
			transactionReferenceService.insert(transaction.getReference());
		}
		if (transaction.getCoupon() != null) {
			transaction.getCoupon().setTransactionId(transaction.getId());
			transactionCouponService.insert(transaction.getCoupon());
		}		
	}
	
	/**
	 * Update transaction
	 * @param transaction
	 */
	public void updateTransaction(Transaction transaction) {
		transactionDao.updateTransaction(transaction);
	}

	@Transactional(rollbackFor = Exception.class)
	public boolean updateTransaction(Transaction transaction, TransactionHistory history) {
		boolean result = transactionDao.updateTransaction(transaction);
		if (result) {
			result = transactionHistorySerivce.insert(history);	
		}
		return result;
	}

	@Override
	public Map<Long, Transaction> getTransactions() {
		return transactionDao.getTransactions();
	}

	@Override
	public void insertTransaction(Transaction transaction, TransactionHistory history) {
		insertTransaction(transaction);
		history.setTransactionId(transaction.getId());
		history.setTimeCreated(transaction.getTimeCreated());
		transactionHistorySerivce.insert(history);	
	}

	@Override
	public boolean isUserHaveOpenWithdraw(long userId) {	
		return transactionDao.getUserWaitingWithdrawalTransaction(userId).size() > 0;
	}
}