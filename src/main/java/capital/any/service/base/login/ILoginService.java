package capital.any.service.base.login;

import java.util.ArrayList;

import capital.any.model.base.Login;

/**
 * @author eranl
 *
 */
public interface ILoginService {

	ArrayList<Login> getLogins();
	
}
