package capital.any.service.base.login;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.login.ILoginDao;
import capital.any.model.base.Login;

/**
 * @author eranl
 *
 */
@Service
public class LoginService implements ILoginService {
	
	@Autowired
	private ILoginDao loginDao;

	@Override
	public ArrayList<Login> getLogins() {
		return loginDao.getLogins();
	}

}
