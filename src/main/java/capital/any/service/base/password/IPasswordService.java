package capital.any.service.base.password;

import capital.any.model.base.ChangePassword;

/**
 * @author LioR SoLoMoN
 *
 */
public interface IPasswordService {
	
	
	void changePassword(ChangePassword changePassword);
}
