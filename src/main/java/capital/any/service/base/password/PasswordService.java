package capital.any.service.base.password;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import capital.any.model.base.ChangePassword;

/**
 * @author LioR SoLoMoN
 *
 */
@Service
public class PasswordService implements IPasswordService {
	private static final Logger logger = LoggerFactory.getLogger(PasswordService.class);
	
	@Override
	public void changePassword(ChangePassword changePassword) {
		logger.info("about to change password ");
		
	} 
}
