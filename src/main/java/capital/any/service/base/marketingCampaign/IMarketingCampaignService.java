package capital.any.service.base.marketingCampaign;

import java.util.Map;

import capital.any.model.base.MarketingCampaign;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IMarketingCampaignService {
	/* Link's Parameters */
	public static final String EMAIL = "e";
	public static final String CAMPAIGN_ID = "camp_id";
	public static final String PAGE_REDIRECT = "pageR";
	public static final String UTM_CAMPAIGN = "utm_campaign";
	public static final String UTM_SOURCE = "utm_source";
	public static final String UTM_MEDIUM = "utm_medium";
	public static final String UTM_CONTENT = "utm_content";
	
	/* SendGrid */
	public static final String SENDGRID_FIELD_HASH_EMAIL = "[%Hash_E%]";
	/* Link */
	public static final String PATH = "/landing/init?";
	
	/**
	 * insert marketing campaign
	 * @param marketingCampaign
	 */
	void insert(MarketingCampaign marketingCampaign);
	
	/**
	 * update marketing campaign
	 * @param marketingCampaign
	 */
	void update(MarketingCampaign marketingCampaign);
	
	/**
	 * Get marketing campaigns
	 * @return Map<Long, MarketingCampaign>
	 */
	Map<Long, MarketingCampaign> getMarketingCampaigns();
	
	/**
	 * Get marketing campaign
	 * @param id
	 * @return MarketingCampaign
	 */
	MarketingCampaign getMarketingCampaign(long id);
}
