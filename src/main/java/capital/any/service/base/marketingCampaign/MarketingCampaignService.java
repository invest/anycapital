package capital.any.service.base.marketingCampaign;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.marketingCampaign.MarketingCampaignDao;
import capital.any.model.base.MarketingCampaign;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service
public class MarketingCampaignService implements IMarketingCampaignService {

	@Autowired
	private MarketingCampaignDao marketingCampaignDao;
	
	@Override
	public void insert(MarketingCampaign marketingCampaign) {
		marketingCampaignDao.insert(marketingCampaign);
	}
	
	@Override
	public void update(MarketingCampaign marketingCampaign) {
		marketingCampaignDao.update(marketingCampaign);
	}

	@Override
	public Map<Long, MarketingCampaign> getMarketingCampaigns() {
		Map<Long, MarketingCampaign> marketingCampaigns = marketingCampaignDao.getMarketingCampaigns();
		return marketingCampaigns;
	}

	@Override
	public MarketingCampaign getMarketingCampaign(long id) {
		MarketingCampaign marketingCampaign = marketingCampaignDao.getMarketingCampaign(id);
		return marketingCampaign;
	}
}
