package capital.any.service.base.user;

import capital.any.model.base.User;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;

public class EmailExistInDBValidator implements ConstraintValidator<EmailExistInDB, Object> {

	private static final Logger logger = LoggerFactory.getLogger(EmailExistInDBValidator.class);
	private String firstFieldName;
	@Autowired
	private IUserService userService;
	
	@Override
	public void initialize(final EmailExistInDB constraintAnnotation) {
		firstFieldName = constraintAnnotation.first();
	}
	
	@Override
	public boolean isValid(final Object value, final ConstraintValidatorContext context) {
		boolean isValidated = true;
		try {
			BeanWrapperImpl wrapper = new BeanWrapperImpl(value);
			final Object firstObj = wrapper.getPropertyValue(firstFieldName);
			String email = (String) firstObj;
			User user = userService.getUserByEmail(email);
			if (user != null) {
				isValidated = false;
			}
		} catch (final Exception ignore) {
			logger.error("cannot validate email", ignore);
		}
		return isValidated;
	}
}
