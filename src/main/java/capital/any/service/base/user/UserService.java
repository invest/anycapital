package capital.any.service.base.user;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import capital.any.base.enums.Currency;
import capital.any.base.enums.Table;
import capital.any.base.enums.UserHistoryAction;
import capital.any.dao.base.user.IUserDao;
import capital.any.dao.base.writer.BaseWriterFactory;
import capital.any.model.base.AbstractWriter;
import capital.any.model.base.ChangePassword;
import capital.any.model.base.EmailAction;
import capital.any.model.base.EmailActionRequest;
import capital.any.model.base.MarketingAttribution;
import capital.any.model.base.User;
import capital.any.model.base.User.Step;
import capital.any.model.base.UserFilters;
import capital.any.model.base.UserHistory;
import capital.any.model.base.UserStep;
import capital.any.service.base.IServerConfiguration;
import capital.any.service.base.emailAction.IEmailActionService;
import capital.any.service.base.marketingAttribution.IMarketingAttributionService;
import capital.any.service.base.marketingTracking.IMarketingTrackingService;
import capital.any.service.base.userHistory.IUserHistoryService;

/**
 * @author eranl
 *
 */
@Service
public class UserService implements IUserService {
	private static final Logger logger = LoggerFactory.getLogger(UserService.class);
	
	@Autowired
	private IMarketingTrackingService marketingTrackingService;
	@Autowired
	private IMarketingAttributionService marketingAttributionService;
	@Autowired
	private IUserDao userDao;
	@Autowired
	private IUserHistoryService UserHistoryService;
	@Autowired
	private ObjectMapper mapper;
	@Autowired
	private IServerConfiguration serverConfiguration;
	@Autowired
	private BaseWriterFactory writerFactory;
	@Autowired
	private IEmailActionService emailActionService;

	@Override
	public User getUserByEmail(String email) {
		return userDao.getUserByEmail(email);
	}

	@Override
	public User getUserById(long id) {
		return userDao.getUserById(id);
	}

	@Override
	public void insertUser(User user) throws Exception {		
		String password = user.getPassword();
		user.setPassword(new ShaPasswordEncoder().encodePassword(password, null));
		user.setUserStep(new UserStep(Step.NEW.getId()));
		//*****************************************
		//TODO change according to spec.
		user.setCurrencyId(Currency.EURO.getId());
		//*****************************************
		userDao.insertUser(user);
		afterInsertUser(user);
	}

	@Override 
	public void update(User user, UserHistory userHistory) throws Exception { 
		userDao.update(user);
		afterUpdateUser(user, userHistory, mapper.writeValueAsString(user), UserHistoryAction.UPDATE_USER.getId());
	}
	
	@Override
	public boolean updateStepB(User user, UserHistory userHistory) throws Exception {
		user.setUserStep(new UserStep(Step.UPDATE.getId()));
		boolean result = userDao.updateStepB(user);
		if (result) {
			afterUpdateUser(user, userHistory, mapper.writeValueAsString(user), UserHistoryAction.UPDATE_ADDITIONAL_INFO.getId());
		}
		return result;
	}

	@Override
	public void changePassword(ChangePassword changePassword, UserHistory userHistory) {
		logger.info("about to change password");
		if (changePassword.getUser() != null) {
			if (changePassword.getCurrentPassword().equals(changePassword.getUser().getPassword()) &&
					changePassword.getNewPassword().equals(changePassword.getRetypePassword())) {
				changePassword.getUser().setPassword(new ShaPasswordEncoder().encodePassword(changePassword.getNewPassword(), null));
				changePassword.setChanged(userDao.changePassword(changePassword.getUser()));
				ObjectNode json = mapper.createObjectNode();
				json.put("password", changePassword.getUser().getPassword());
				afterUpdateUser(changePassword.getUser(), userHistory, json.toString(), UserHistoryAction.CHANGE_PASSWORD.getId());
			}
		}
	}
	
	public void afterInsertUser(User user) throws Exception {
		/* marketing Tracking */
		marketingTrackingService.insert(user.getMarketingTracking());
		/* Marketing Attribution */
		MarketingAttribution marketingAttribution = new MarketingAttribution(Table.USERS.getId(), user.getId(), user.getMarketingTracking().getId(), user.getId());
		marketingAttributionService.insert(marketingAttribution);
		/* User History */
		AbstractWriter writer = writerFactory.createWriter(user.getWriterId());
		writer.setId(user.getWriterId());
		UserHistory userHistory = new UserHistory(user.getActionSource(), writer, user.getId(), 
				new capital.any.model.base.UserHistoryAction(UserHistoryAction.INSERT_USER.getId()), 
				mapper.writeValueAsString(user), serverConfiguration.getServerName());
		UserHistoryService.insert(userHistory);
		try {
			EmailActionRequest<Object> emailActionRequest = new EmailActionRequest<Object>(user, new EmailAction(capital.any.base.enums.EmailAction.REGISTER.getId()));
			emailActionRequest.setActionSource(user.getActionSource());
			emailActionService.insertEmailByAction(emailActionRequest);
		} catch (Exception e) {
			logger.error("Problem with insert email.", e);
		}
	}
	
	@Override
	public List<User> getUsersByDates(UserFilters userFilters) {
		return userDao.getUsersByDates(userFilters);
	}
	
	@Override
	public boolean updateStep(User user, UserHistory userHistory) {
		boolean result = userDao.updateStep(user);
		if (result) {
			ObjectNode json = mapper.createObjectNode();
			json.put("step", user.getUserStep().getId());
			afterUpdateUser(user, userHistory, json.toString(), UserHistoryAction.UPDATE_STEP.getId());
		}
		return result;
	}
	
	public void afterUpdateUser(User user, UserHistory userHistory, String actionParamters, int userHistoryActionId) {
		/* User History */
		userHistory.setDetails(user.getId(), 
				new capital.any.model.base.UserHistoryAction(userHistoryActionId), 
				actionParamters, serverConfiguration.getServerName());
		UserHistoryService.insert(userHistory);
	}

	@Override
	public User getUserForResetPassword(String email) {
		User userTmp = userDao.getUserByEmail(email);		
		User user = null;			
		if (userTmp != null) {
			user = new User();
			user.setEmail(userTmp.getEmail());
			if (userTmp.getMobilePhone() != null) {		
				user.setMobilePhone("****" + userTmp.getMobilePhone().substring(userTmp.getMobilePhone().length() / 2));
			}
		}
		return user;
	}
	
	@Override
	public boolean acceptTerms(User user, UserHistory userHistory) throws Exception {		
		boolean result = userDao.acceptTerms(user);
		if (result) {
			user = getUserById(user.getId());
			afterUpdateUser(user, userHistory, mapper.writeValueAsString(user), UserHistoryAction.ACCEPT_TERMS.getId());
		}
		return result;
	}
}
