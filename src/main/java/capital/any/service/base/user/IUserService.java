package capital.any.service.base.user;

import java.util.List;

import capital.any.model.base.ChangePassword;
import capital.any.model.base.User;
import capital.any.model.base.UserFilters;
import capital.any.model.base.UserHistory;

/**
 * @author eranl
 *
 */
public interface IUserService {
	
	/**
	 * Get user by email
	 * @param id
	 * @return
	 */
	User getUserByEmail(String email);
	
	/**
	 * @param email
	 * @return
	 */
	User getUserForResetPassword(String email);
		
	/**
	 * Get user by id
	 * @param id
	 * @return
	 */
	User getUserById(long id);
	
	/**
	 * Insert user
	 * @param user
	 */
	void insertUser(User user) throws Exception;
	
	/**
	 * Update user
	 * @param user
	 */
	void update(User user, UserHistory userHistory) throws Exception;
	
	/**
	 * @param user
	 * @return 
	 */
	boolean updateStepB(User user, UserHistory userHistory) throws Exception;

	/**
	 * @param changePassword
	 */
	void changePassword(ChangePassword changePassword, UserHistory userHistory);

	/**
	 * Get users by dates
	 * @param userFilters
	 * @return List<User>
	 */
	List<User> getUsersByDates(UserFilters userFilters);

	/**
	 * Update step
	 * @param user
	 * @return boolean
	 */
	boolean updateStep(User user, UserHistory userHistory);
	
	/**
	 * @param user
	 * @return 
	 */
	boolean acceptTerms(User user, UserHistory userHistory) throws Exception;

}
