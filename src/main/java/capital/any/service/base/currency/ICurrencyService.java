package capital.any.service.base.currency;

import java.util.Map;
import capital.any.model.base.Currency;

/**
 * @author eranl
 *
 */
public interface ICurrencyService {
	
	/**
	 * Get currencies hashmap
	 * @return
	 */
	Map<Integer, Currency> get();

	/**
	 * get currency rate from market last price
	 * if its Euro return 1
	 * @param currencyId 
	 * @return return the rate of the currency
	 */
	double getRate(int currencyId);
}
