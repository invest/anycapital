package capital.any.service.base.currency;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import capital.any.dao.base.currency.ICurrencyDao;
import capital.any.model.base.Currency;
import capital.any.service.base.market.IMarketService;

/**
 * @author eranl
 *
 */
@Primary
@Service
public class CurrencyService implements ICurrencyService {
	private static final Logger logger = LoggerFactory.getLogger(CurrencyService.class);
	@Autowired
	private ICurrencyDao currencyDao;
	@Autowired
	@Qualifier("CurrencyHCDao")
	private ICurrencyDao currencyHCDao;
	
	@Autowired
	private IMarketService marketService;

	@Override
	public Map<Integer, Currency> get() {
		Map<Integer, Currency> currencies = currencyHCDao.get();
		if (currencies == null || currencies.size() <= 0) {
			logger.info("currencies empty / null in hazelcast, Let's check RDBMS");
			currencies = currencyDao.get(); 
		}
		return currencies;
	}

	@Override
	public double getRate(int currencyId) {
		if (currencyId == capital.any.base.enums.Currency.EURO.getId()) {
			return 1;
		}
		int MarketId = get().get(currencyId).getMarketId();
		return marketService.getMarketsWithLastPrice().get(MarketId).getLastPrice().getPrice();
	}
}
