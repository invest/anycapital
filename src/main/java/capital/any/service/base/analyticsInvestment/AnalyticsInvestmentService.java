package capital.any.service.base.analyticsInvestment;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import capital.any.dao.base.analyticsInvestment.IAnalyticsInvestmentDao;
import capital.any.model.base.analytics.AnalyticsInvestment;
import capital.any.model.base.analytics.AnalyticsInvestmentProduct;

/**
 * 
 * @author eyal.ohana
 *
 */
@Primary
@Service
public class AnalyticsInvestmentService implements IAnalyticsInvestmentService {

	@Autowired
	private IAnalyticsInvestmentDao analyticsInvestmentDao;

	@Override
	public AnalyticsInvestment get() {
		return analyticsInvestmentDao.get();
	}
	
	@Override
	public List<AnalyticsInvestmentProduct> getByProduct() {
		return analyticsInvestmentDao.getByProduct();
	}
}
