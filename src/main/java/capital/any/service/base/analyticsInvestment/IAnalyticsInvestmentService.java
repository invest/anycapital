package capital.any.service.base.analyticsInvestment;

import java.util.List;

import capital.any.model.base.analytics.AnalyticsInvestment;
import capital.any.model.base.analytics.AnalyticsInvestmentProduct;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IAnalyticsInvestmentService {

	AnalyticsInvestment get();

	List<AnalyticsInvestmentProduct> getByProduct();

}
