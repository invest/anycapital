package capital.any.service.base.csv;

import java.util.List;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface ICSVService {
	public static final String DEFAULT_SEPARATOR = ",";
	public static final String END_OF_LINE = "\n";
	public static final String EMPTY_STRING = "";
	
	List<List<String>> readDocument(String fileName);
	
	void writeDocument(String fileName, List<List<String>> twoDimensionalList);
}
