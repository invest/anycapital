package capital.any.service.base.csv;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service
public class CSVService implements ICSVService {
	private static final Logger logger = LoggerFactory.getLogger(CSVService .class);
	
	@Override
	public void writeDocument(String fileName, List<List<String>> twoDimensionalList) {
		try {
			FileWriter fileWriter = new FileWriter(fileName);
			for (List<String> list : twoDimensionalList) {
				writeLine(fileWriter, list);
			}
			fileWriter.flush();
			fileWriter.close();
		} catch (IOException e) {
			logger.error("Problem with writeDocument. ", e);
		}
		
	}
	
	public void writeLine(Writer w, List<String> values) throws IOException {
		boolean first = true;
        StringBuilder sb = new StringBuilder();
        for (String value : values) {
            if (!first) {
                sb.append(DEFAULT_SEPARATOR);
            }
            sb.append(followCVSformat(value));
            first = false;
        }
        sb.append(END_OF_LINE);
        w.append(sb.toString());
    }

    //https://tools.ietf.org/html/rfc4180
    private String followCVSformat(String value) {
        String result = value;
        if (result.contains("\"")) {
            result = result.replace("\"", "\"\"");
        }
        return result;
    }
    
    @Override
    public List<List<String>> readDocument(String fileName) {
        String line = EMPTY_STRING;
        List<List<String>> docInfo = new ArrayList<List<String>>();
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            while ((line = br.readLine()) != null) {
                String[] info = line.split(DEFAULT_SEPARATOR);
                List<String> lineList = Arrays.asList(info);
                docInfo.add(lineList);
            }
        } catch (Exception e) {
            logger.error("Problem with readDocument. ", e);
        }
        return docInfo;
    }
}
