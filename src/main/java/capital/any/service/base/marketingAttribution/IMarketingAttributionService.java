package capital.any.service.base.marketingAttribution;

import capital.any.model.base.MarketingAttribution;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IMarketingAttributionService {
	
	/**
	 * Insert marketing attribution
	 * @param marketingAttribution
	 */
	void insert(MarketingAttribution marketingAttribution);
}
