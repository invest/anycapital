package capital.any.service.base.marketingAttribution;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.marketingAttribution.IMarketingAttributionDao;
import capital.any.model.base.MarketingAttribution;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service
public class MarketingAttributionService implements IMarketingAttributionService {

	@Autowired
	private IMarketingAttributionDao marketingAttributionDao;
	
	@Override
	public void insert(MarketingAttribution marketingAttribution) {
		marketingAttributionDao.insert(marketingAttribution);
	}
}
