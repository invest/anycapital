package capital.any.service.base.userToken;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import capital.any.dao.base.userToken.IUserTokenDao;
import capital.any.model.base.User;
import capital.any.model.base.UserToken;

/**
 * @author LioR SoLoMoN
 *
 */
@Primary
@Service
public class UserTokenService implements IUserTokenService {
	private static final Logger logger = LoggerFactory.getLogger(UserTokenService.class);
	@Autowired
	private IUserTokenDao userTokenDao;

	
	@Override
	public UserToken get(UserToken userToken) {
		return userTokenDao.get(userToken);
	}
	
	@Override
	public boolean insert(UserToken userToken) {
		return userTokenDao.insert(userToken);		
	}

	@Override
	public String generateToken(User user) throws Exception {
		StringBuilder token = new StringBuilder(
				String.valueOf(System.currentTimeMillis())).append(user.getId());
		
		return token.toString();
	}
}
