package capital.any.service.base.file;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import capital.any.model.base.File;

/**
 * @author Eyal G
 *
 */
public interface IFileService {	
	
	/**
	 * get all files for this user id
	 * @param userId
	 * @return List<file>
	 */
	List<File> getByUserId(long userId);
	
	/**
	 * insert file to user
	 * @param file
	 */
	void insert(File file);
	
	/**
	 * get file by user id and file id
	 * @param file
	 * @return file
	 */
	File getByIdAndUserId(File file);

	/**
	 * insert to db and upload to aws
	 * @param file db file
	 * @param fileUpload file to upload to aws
	 * @throws Exception 
	 */
	void insertAndUpload(File file, MultipartFile fileUpload) throws Exception;
	
	/**
	 * update file
	 * @param file
	 * @return true if update else false
	 */
	boolean updateFile(File file) throws Exception;
	
	/**
	 * update is primary to false by user id and file type
	 * @param file
	 * @return true if update else false
	 */
	boolean updateIsPrimary(File file);
}
