package capital.any.service.base.file;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import capital.any.dao.base.file.IFileDao;
import capital.any.model.base.File;
import capital.any.service.base.aws.IAwsService;

/**
 * @author Eyal G
 *
 */
@Primary
@Service
public class FileService implements IFileService {
	private static final Logger logger = LoggerFactory.getLogger(FileService.class);
	
	@Autowired
	private IFileDao fileDao;
	
	@Autowired
	private IAwsService awsService;
	
	@Override
	public List<File> getByUserId(long userId) {
		return fileDao.getByUserId(userId);
	}
	
	@Override
	public void insert(File file) {
		fileDao.insert(file);		
	}
	
	@Override
	public File getByIdAndUserId(File file) {
		return fileDao.getByIdAndUserId(file);
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void insertAndUpload(File file, MultipartFile fileUpload) throws Exception {
		if (file.isPrimary()) {
			updateIsPrimary(file);
		}
		file.setName(new Date().getTime() + File.NAME_SEPARATOR + file.getName());
		insert(file); //insert to db
		awsService.uploadUserFile(fileUpload, file, null); //upload to aws		
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean updateFile(File file) throws Exception {
		if (file.isPrimary()) {
			updateIsPrimary(file);
		}
		return fileDao.updateFile(file);
	}

	@Override
	public boolean updateIsPrimary(File file) {
		return fileDao.updateIsPrimary(file);
	}
}
