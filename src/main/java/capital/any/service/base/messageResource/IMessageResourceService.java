package capital.any.service.base.messageResource;

import java.util.List;
import java.util.Map;
import capital.any.model.base.MsgRes;
import capital.any.model.base.MsgResLanguage;

/**
 * @author eranl
 *
 */
public interface IMessageResourceService {
	
	/**
	 * @return
	 */
	Map<Integer, Map<Integer, Map<String, String>>> get();
	/**
	 * Insert MessageResource
	 * @throws Exception 
	 */
	void insertMessageResources(MsgRes msgRes, List<MsgResLanguage> msgResLanguages) throws Exception;
				
	/**
	 * Get Msg res
	 * @param actionSourceId
	 * @return
	 */
	Map<Integer, Map<Integer, Map<String, String>>> get(MsgRes msgRes);
	
	/**
	 * get translate to key from message resource
	 * @param actionSourceId
	 * @param languageId
	 * @param key key to translate
	 * @return translate of the key
	 */
	String translateKey(int actionSourceId, int languageId, String key);
	
}
