package capital.any.service.base.messageResource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import capital.any.base.enums.MsgResTypeEnum;
import capital.any.dao.base.messageResource.IMessageResourceDao;
import capital.any.model.base.MsgRes;
import capital.any.model.base.MsgResLanguage;

/**
 * @author eranl
 *
 */
@Service
public class MessageResourceService implements IMessageResourceService {
	private static final Logger logger = LoggerFactory.getLogger(MessageResourceService.class);
	@Autowired
	private IMessageResourceDao messageResourceDao;
	@Autowired
	@Qualifier("MessageResourceHCDao")
	private IMessageResourceDao messageResourceHCDao;

	@Override
	@Transactional(rollbackFor=Exception.class)
	public void insertMessageResources(MsgRes msgRes, List<MsgResLanguage> msgResLanguages) throws Exception {
		messageResourceDao.insertMsgRes(msgRes);
		for (MsgResLanguage obj: msgResLanguages) {
			obj.setMsgRes(msgRes); 
		}
		messageResourceDao.insertMsgResLanguage(msgResLanguages);
		messageResourceHCDao.insertMsgResLanguage(msgResLanguages);
	}
	
	@Override
	public Map<Integer, Map<Integer, Map<String, String>>> get() {	
		Map<Integer, Map<Integer, Map<String, String>>> messageResources = messageResourceHCDao.get();
		if (messageResources == null || messageResources.size() <= 0) {
			logger.info("messageResources empty / null in hazelcast, Let's check RDBMS");
			messageResources = messageResourceDao.get();
		}		
		return messageResources;
	}
	
	@Override
	public Map<Integer, Map<Integer, Map<String, String>>> get(MsgRes msgRes) {
		Map<Integer, Map<Integer, Map<String, String>>> messageResources = messageResourceHCDao.get(msgRes);
		if (messageResources == null || messageResources.size() <= 0) {
			logger.info("messageResources empty / null in hazelcast, Let's check RDBMS");
			messageResources = messageResourceDao.get(msgRes);
		}		
		return messageResources;
	}
	
	public String translateKey(int actionSourceId, int languageId, String key) {
		Map<Integer, Map<Integer, Map<String, String>>> msgResource = get();
		return msgResource.get(actionSourceId).get(languageId).get(key);
	}
}
