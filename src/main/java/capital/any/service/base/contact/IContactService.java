package capital.any.service.base.contact;

import capital.any.model.base.Contact;

/**
 * @author eran.levy
 *
 */
public interface IContactService {
	
	/**
	 * insert contact
	 * @param contact
	 */
	void insert(Contact contact);
}
