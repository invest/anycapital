package capital.any.service.base.contact;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.contact.IContactDao;
import capital.any.model.base.Contact;
import capital.any.service.base.marketingTracking.IMarketingTrackingService;

/**
 * @author eran.levy
 *
 */
@Service
public class ContactService implements IContactService {
	
	@Autowired
	protected IContactDao contactDao;
	@Autowired
	private IMarketingTrackingService marketingTrackingService;
	
	@Override
	public void insert(Contact contact) {
		beforeInsertContact(contact);
		contactDao.insert(contact);		
	}
	
	private void beforeInsertContact(Contact contact) {
		/* marketing Tracking */
		marketingTrackingService.insert(contact.getMarketingTracking());
	}
}