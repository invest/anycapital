package capital.any.service.base.issue;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.issue.IIssueDao;
import capital.any.model.base.Issue;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service
public class IssueService implements IIssueService {
	
	@Autowired
	private IIssueDao issueDao;
	
	@Override
	public void insert(Issue issue) {
		issueDao.insert(issue);
	}
	
	@Override
	public Map<Long, Issue> getIssuesByUserId(long userId) {
		return issueDao.getIssuesByUserId(userId);
	}

}
