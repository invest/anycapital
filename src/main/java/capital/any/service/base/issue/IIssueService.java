package capital.any.service.base.issue;

import java.util.Map;

import capital.any.model.base.Issue;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IIssueService {

	void insert(Issue issue);

	Map<Long, Issue> getIssuesByUserId(long userId);

}
