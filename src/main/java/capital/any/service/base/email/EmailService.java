package capital.any.service.base.email;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import capital.any.dao.base.email.IEmailDao;
import capital.any.model.base.Email;

/**
 * @author Eyal G
 *
 */
@Primary
@Service
public class EmailService implements IEmailService {
	private static final Logger logger = LoggerFactory.getLogger(EmailService.class);
	
	@Autowired
	private IEmailDao emailDao;
	
	@Override
	public void insert(Email email) {
		emailDao.insert(email);		
	}
}
