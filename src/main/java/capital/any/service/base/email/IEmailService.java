package capital.any.service.base.email;

import capital.any.model.base.Email;

/**
 * @author Eyal G
 *
 */
public interface IEmailService {	
	
	/**
	 * insert email to send
	 * @param {@link}Email
	 */
	void insert(Email email);
}
