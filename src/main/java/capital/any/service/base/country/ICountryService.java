package capital.any.service.base.country;

import java.util.Map;
import capital.any.model.base.Country;

/**
 * @author eranl
 *
 */
public interface ICountryService {	
	
	/**
	 * Get countries map
	 * @return
	 */
	Map<Integer, Country> get();
	
	/**
	 * @param ip
	 * @return
	 */
	Country getByIp(String ip);
	
	/**
	 * @param A2
	 * @return
	 */
	Country getByA2(String A2);
	
	/**
	 * Get regulated countries map
	 * @return
	 */
	Map<Integer, Country> getRegulatedCountries();
	
}
