package capital.any.service.base.country;

import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import capital.any.dao.base.country.ICountryDao;
import capital.any.model.base.Country;
import capital.any.service.IUtilService;
import capital.any.service.base.IServerConfiguration;

/**
 * @author eranl
 *
 */
@Primary
@Service
public class CountryService implements ICountryService {
	private static final Logger logger = LoggerFactory.getLogger(CountryService.class);
	@Autowired
	private ICountryDao countryDao;
	@Autowired
	@Qualifier("CountryHCDao")
	private ICountryDao countryHCDao;
	@Autowired
	private IServerConfiguration serverConfiguration;
	private static final int UNITED_KINGDOM_ID = 226;
	
	@Override
	public Map<Integer, Country> get() {
		Map<Integer, Country> countries = countryHCDao.get();
		if (countries == null || countries.size() <= 0) {
			logger.info("countries empty / null in hazelcast, Let's check RDBMS");
			countries = countryDao.get(); 
		}
		return countries;
	}
	
	@Override
	public Map<Integer, Country> getRegulatedCountries() {
		Map<Integer, Country> countries = countryHCDao.get();
		if (countries == null || countries.size() <= 0) {
			logger.info("countries empty / null in hazelcast, Let's check RDBMS");
			countries = countryDao.get(); 
		}
		Map<Integer, Country> regulatedCountries = new HashMap<Integer, Country>();
		countries.forEach((key, value) -> {
			if (value.getIsBlocked() == 0) {
				regulatedCountries.put(key, value);
			}
		}
		);
										
		return regulatedCountries;
	}

	
	@Override
	public Country getByA2(String A2) {
		Country country = countryHCDao.getByA2(A2);
		if (country == null) {
			country = countryDao.getByA2(A2);
		}
		return country;
	}

	@Override
	public Country getByIp(String ip) {
		Country country = get().get(UNITED_KINGDOM_ID);
		if (!ip.equals(IUtilService.LOCAL_HOST_IPV4) && !ip.equals(IUtilService.LOCAL_HOST_IPV6)) {
			String[] geoLocation = serverConfiguration.getGeoLocation(ip);
			if (geoLocation != null &&
					geoLocation.length > 0 && 
					geoLocation[0].length() == 2) {
				country = getByA2(geoLocation[0]);
			}
		} 
		return country;
	}
}
