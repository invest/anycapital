package capital.any.service.base.transactionFee;

import capital.any.model.base.TransactionFee;

/**
 * @author LioR SoLoMoN
 *
 */
public interface ITransactionFeeService {	
	
	/**
	 * @return transaction fee
	 */
	TransactionFee get(long transactionId);
	
	/**
	 * @param transactionFee
	 * @return
	 */
	public boolean insert(TransactionFee transactionFee);
	
}
