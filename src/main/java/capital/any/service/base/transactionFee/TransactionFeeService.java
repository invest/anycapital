package capital.any.service.base.transactionFee;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import capital.any.dao.base.transactionFee.ITransactionFeeDao;
import capital.any.model.base.TransactionFee;

/**
 * @author LioR SoLoMoN
 *
 */
@Primary
@Service
public class TransactionFeeService implements ITransactionFeeService {
	private static final Logger logger = LoggerFactory.getLogger(TransactionFeeService.class);
	@Autowired
	private ITransactionFeeDao transactionFeeDao;

	@Override
	public TransactionFee get(long transactionId) {
		return transactionFeeDao.get(transactionId);
	}

	@Override
	public boolean insert(TransactionFee transactionFee) {
		return transactionFeeDao.insert(transactionFee);
	}
}
