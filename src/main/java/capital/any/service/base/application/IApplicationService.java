package capital.any.service.base.application;

/**
 * @author LioR SoLoMoN
 *
 */
public interface IApplicationService {	
	
	/**
	 * @return
	 */
	String getDomainURL();

}
