package capital.any.service.base.application;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @author LioR SoLoMoN
 *
 */
@Service
public class ApplicationService implements IApplicationService {
	@Value("${domain.url}")
	private String DOMAIN_URL;
	
	/**
	 * @return the dOMAIN_URL
	 */
	public String getDomainURL() {
		return DOMAIN_URL;
	}
}
