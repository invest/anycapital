package capital.any.service.base.session;

import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import capital.any.model.base.Login;

/**
 * @author LioR SoLoMoN
 *
 */
@Service
public class SessionService implements ISessionService {
	private static final Logger logger = LoggerFactory.getLogger(SessionService.class);
	
	@Override
	public Object getAttribute(HttpSession session, String attributeName) {
		logger.info("get attribute from session, name: " + attributeName);
		return session.getAttribute(attributeName);
	}

	@Override
	public void addAttribute(HttpSession session, Object object, String attributeName) {
		logger.info("add attribute to session, name: " + attributeName);
		session.setAttribute(attributeName, object);
	}

	@Override
	public long getLoginId(HttpSession session) {
		Login login = (Login) getAttribute(session, ISessionService.LOGIN);
		if (login != null) {
			return login.getId();
		}
		return 0;
	}
}
