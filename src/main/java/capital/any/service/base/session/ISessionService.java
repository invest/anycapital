package capital.any.service.base.session;

import javax.servlet.http.HttpSession;

/**
 * @author LioR SoLoMoN
 *
 */
public interface ISessionService {
	public static final String USER = "user";
	public static final String MAP_REQUEST_PARAMS = "map_request_params";
	public static final String COMBINATION_ID = "comb_id";
	public static final String LOGIN = "login";
	public static final String MAP_FORGOT_PASSWORD_PARAMS = "map_forgot_password_params";
	
	Object getAttribute(HttpSession session, String attributeName);
	
	void addAttribute(HttpSession session, Object object, String attributeName);
	
	long getLoginId(HttpSession session);

}
