package capital.any.service.base.wire;

import capital.any.model.base.Wire;

/**
 * @author Eran
 *
 */
public interface IWireService {
				
	/**
	 * Insert wire
	 * @param wire
	 */
	void insertWire(Wire wire);
	

}
