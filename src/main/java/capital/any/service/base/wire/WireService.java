package capital.any.service.base.wire;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.wire.IWireDao;
import capital.any.model.base.Wire;

/**
 * @author eranl
 *
 */
@Service
public class WireService implements IWireService {
	
	@Autowired
	private IWireDao wireDao;
	
	/**
	 * Insert Wire
	 * @param wire
	 */
	public void insertWire(Wire wire) {
		wireDao.insertWire(wire);
	}
		
}