package capital.any.service.base.providerRequestResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.providerRequestResponse.IProviderRequestResponseDao;
import capital.any.model.base.ProviderRequestResponse;

/**
 * @author eran.levy
 *
 */
@Service
public class ProviderRequestResponseService implements IProviderRequestResponseService {
	
	@Autowired
	private IProviderRequestResponseDao providerRequestResponseDao;
	
	@Override
	public void insert(ProviderRequestResponse providerRequestResponse) {
		providerRequestResponseDao.insert(providerRequestResponse);		
	}
}