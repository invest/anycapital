package capital.any.service.base.providerRequestResponse;

import capital.any.model.base.ProviderRequestResponse;

/**
 * @author eran.levy
 *
 */
public interface IProviderRequestResponseService {
	
	/**
	 * Insert
	 * @param providerRequestResponse
	 */
	void insert(ProviderRequestResponse providerRequestResponse);
}
