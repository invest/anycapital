package capital.any.service.base.regulationFileRule;

import java.util.Map;

import capital.any.model.base.RegulationFileRule;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IRegulationFileRuleService {
	
	Map<Integer, RegulationFileRule> getFilesByRule(RegulationFileRule regulationFileRule);

	/******** Finotec **********/
	/*Map<Integer, RegulationFileRule> getRequiredDocs(long userId);*/
}
