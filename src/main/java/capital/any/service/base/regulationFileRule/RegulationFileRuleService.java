package capital.any.service.base.regulationFileRule;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.base.enums.GBGStatus;
import capital.any.dao.base.regulationFileRule.IRegulationFileRuleDao;
import capital.any.model.base.GBGUser;
import capital.any.model.base.RegulationFileRule;
import capital.any.model.base.RegulationUserInfo;
import capital.any.service.base.gbg.score.IGBGScoreService;
import capital.any.service.base.gbg.users.IGBGUsersService;
import capital.any.service.base.regulationUserInfo.IRegulationUserInfoService;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service
public class RegulationFileRuleService implements IRegulationFileRuleService {
	private static final Logger logger = LoggerFactory.getLogger(RegulationFileRuleService.class);
	
	@Autowired
	private IRegulationFileRuleDao regulationFileRuleDao;
	@Autowired
	private IGBGUsersService gbgUsersService;
	@Autowired
	private IGBGScoreService gbgScoreService;
	@Autowired
	private IRegulationUserInfoService regulationUserInfoService;

	@Override
	public Map<Integer, RegulationFileRule> getFilesByRule(RegulationFileRule regulationFileRule) {
		return regulationFileRuleDao.getFilesByRule(regulationFileRule);
	}
	
	/******** Finotec **********/
	/*@Override
	public Map<Integer, RegulationFileRule> getRequiredDocs(long userId) {
		logger.debug("RegulationService.getRequiredDocs - Start.");
		Map<Integer, RegulationFileRule> map = null;
		GBGUser gbgUser = gbgUsersService.get(userId);
		RegulationUserInfo regulationUserInfo = regulationUserInfoService.get(userId);
		if (null != gbgUser && null != regulationUserInfo.getCountryRisk()) {
			int score = gbgUser.getScore();
			GBGStatus gbgStatus = gbgScoreService.getStatus(score);
			RegulationFileRule regulationFileRule = new RegulationFileRule();
			regulationFileRule.setCountryRiskId(regulationUserInfo.getCountryRisk().getId());
			regulationFileRule.setGbgStatusId(gbgStatus.getId());
			map = getFilesByRule(regulationFileRule);
		}
		logger.debug("RegulationService.getRequiredDocs - End.");
		return map;
	}*/

}
