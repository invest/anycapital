package capital.any.service.base.soap;

import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPException;

/**
 * @author LioR SoLoMoN
 *
 */
public interface ISOAPService {
	
	/**
	 * @return
	 * @throws SOAPException 
	 */
	SOAPConnection createSOAPConnection() throws SOAPException;
	/**
	 * @param soapConnection
	 */
	void closeConnection(SOAPConnection soapConnection);
}
