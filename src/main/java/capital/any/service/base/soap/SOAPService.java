package capital.any.service.base.soap;

import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author LioR SoLoMoN
 *
 */
@Service
public class SOAPService implements ISOAPService {
	private static final Logger logger = LoggerFactory.getLogger(SOAPService.class);
	
	@Override
	public SOAPConnection createSOAPConnection() throws SOAPException {
		SOAPConnection soapConnection = null;
		try {
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			soapConnection = soapConnectionFactory.createConnection();
			logger.info("***** SOAP connection created *****");
		} catch (SOAPException e) {
			logger.error("ERROR! can't create SOAP connection ", e);
			throw e;
		}
		return soapConnection;
	}
	
	@Override
	public void closeConnection(SOAPConnection connection) {
		if (connection != null) {
			try {
				connection.close();
				logger.info("***** SOAP connection closed *****");
			} catch (SOAPException e) {
				logger.error("ERROR! can't close SOAP connection", e);
			}
		}
	}
}
