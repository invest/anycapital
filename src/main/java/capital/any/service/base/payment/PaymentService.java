package capital.any.service.base.payment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import capital.any.model.base.Payment;
import capital.any.service.base.IServerConfiguration;
import capital.any.service.base.dbParameter.IDBParameterService;
import capital.any.service.base.payment.deposit.IDepositService;
import capital.any.service.base.payment.withdraw.IWithdrawService;
import capital.any.base.enums.DBParameter;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;;

/**
 * @author LioR SoLoMoN
 *
 */
@Service
public class PaymentService implements IPaymentService {
	private static final Logger logger = LoggerFactory.getLogger(PaymentService.class);
	@Autowired
	private IDepositService depositService;
	@Autowired
	private IWithdrawService withdrawService;
	@Autowired
	private IServerConfiguration serverConfiguration;
	@Autowired
	private IDBParameterService dbParameterService;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void doDeposit(Payment<?> payment) throws Exception {
		logger.info("doDeposit; " + payment.toString());
		payment.setServerName(serverConfiguration.getServerName());
		payment.setResponse(new Response<Object>(null, ResponseCode.OK));
		
		depositService.validationsCheck(payment);

		if (payment.isOK()) {
			depositService.limitationsCheck(payment);
		}
		
		if (payment.isOK()) {
			depositService.executeTransaction(payment);
		}

		if (payment.isOK()) {
			depositService.doClearing(payment);
		}
		
		if (payment.isOK() && payment.changeBalance()) {
			depositService.changeBalance(payment);
		}

		if (payment.isOK() && payment.marketingFlow()) {
			depositService.doMarketingFlow(payment);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void doWithdraw(Payment<?> payment) throws Exception {
		logger.info("doWithdraw; " + payment.toString());
		
		payment.setServerName(serverConfiguration.getServerName());
		payment.getPaymentResponse().setMinimumFeeAmount(
				dbParameterService.get(DBParameter.WITHDRAW_FEE_AMOUNT.getId()).getNumValue());

		withdrawService.validationsCheck(payment);
		
		if (payment.isOK()) {
			withdrawService.limitationsCheck(payment);
		}
		
		
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void confirmWithdraw(Payment<?> payment) throws Exception {	
		//XXX quick fix for withdraw confirm
		//TODO check transaction status validation?
		payment.setServerName(serverConfiguration.getServerName());
		withdrawService.executeTransaction(payment);
		payment.setResponse(new Response<Object>(null, ResponseCode.OK));
		withdrawService.changeBalance(payment);
		withdrawService.doClearing(payment);
	}
}
