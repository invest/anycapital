package capital.any.service.base.payment.deposit;

import capital.any.model.base.Payment;

/**
 * @author LioR SoLoMoN
 *
 */
public interface IDepositService {
	
	/**
	 * @param payment
	 */
	void validationsCheck(Payment<?> payment);
	
	/**
	 * @param payment
	 */
	void limitationsCheck(Payment<?> payment);
	
	/**
	 * @param payment
	 * @throws Exception 
	 */
	void executeTransaction(Payment<?> payment) throws Exception;
	
	/**
	 * @param payment
	 * @throws Exception
	 */
	void changeBalance(Payment<?> payment) throws Exception;
	
	/**
	 * @param payment
	 */
	void doClearing(Payment<?> payment) throws Exception;
	
	/**
	 * @param payment
	 */
	void doMarketingFlow(Payment<?> payment);
	
	/**
	 * @param payment
	 * @throws Exception
	 */
	void failTransaction(Payment<?> payment) throws Exception;
	
}
