package capital.any.service.base.payment;

import capital.any.model.base.Payment;

/**
 * @author LioR SoLoMoN
 *
 */
public interface IPaymentService {

	/**
	 * @param deposit
	 * @return 
	 */
	void doDeposit(Payment<?> deposit) throws Exception;
	
	/**
	 * @param withdraw
	 */
	void doWithdraw(Payment<?> withdraw) throws Exception;

	/**
	 * @param payment
	 * @throws Exception 
	 */
	void confirmWithdraw(Payment<?> payment) throws Exception;

}
