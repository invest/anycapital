package capital.any.service.base.payment.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import capital.any.model.base.Payment;

/**
 * @author LioR SoLoMoN
 *
 */
public class TestPaymentHandler implements PaymentHandler {
	private static final Logger logger = LoggerFactory.getLogger(TestPaymentHandler.class);
	
	@Override
	public void check() {
		logger.info("TestPaymentHandler; check; do nothing");
	}

	@Override
	public boolean doDepositAction(Payment<?> payment) {
		logger.info("TestPaymentHandler; doDepositAction; do nothing");
		return false;
	}

	@Override
	public boolean doWithdrawAction(Payment<?> payment) {
		logger.info("TestPaymentHandler; doWithdrawAction; do nothing");
		return false;
	}	
}
