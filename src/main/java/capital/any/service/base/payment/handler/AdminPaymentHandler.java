package capital.any.service.base.payment.handler;

import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import capital.any.model.base.Payment;
import capital.any.model.base.TransactionHistory;
import capital.any.model.base.Transaction.TransactionStatusEnum;
import capital.any.service.base.transaction.ITransactionService;
import capital.any.service.base.transactionStatus.ITransactionStatusService;

/**
 * @author LioR SoLoMoN
 *
 */
public class AdminPaymentHandler implements PaymentHandler {
	private static final Logger logger = LoggerFactory.getLogger(WirePaymentHandler.class);
	@Autowired
	private ITransactionService transactionService;
	@Autowired
	private ITransactionStatusService transactionStatusService;
	
	@Override
	public void check() {
		logger.info("AdminPaymentHandler check");
	}

	@Override
	public boolean doDepositAction(Payment<?> payment) {

		payment.getTransaction().setStatus(transactionStatusService.get().get(TransactionStatusEnum.SUCCEED.getId()));
		TransactionHistory transactionHistory = new TransactionHistory();
		transactionHistory.setActionSource(payment.getActionSource());
		transactionHistory.setServerName(payment.getServerName());
		transactionHistory.setStatus(payment.getTransaction().getStatus());
		transactionHistory.setTimeCreated(new Date());
		transactionHistory.setTransactionId(payment.getTransaction().getId());
		transactionHistory.setWriter(payment.getWriter());		
		payment.getTransaction().setTimeSettled(new Date());
		transactionService.updateTransaction(payment.getTransaction(), transactionHistory);
	
		//TODO
		return true;
	}

	@Override
	public boolean doWithdrawAction(Payment<?> payment) {

		payment.getTransaction().setStatus(transactionStatusService.get().get(TransactionStatusEnum.SUCCEED.getId()));
		TransactionHistory transactionHistory = new TransactionHistory();
		transactionHistory.setActionSource(payment.getActionSource());
		transactionHistory.setServerName(payment.getServerName());
		transactionHistory.setStatus(payment.getTransaction().getStatus());
		transactionHistory.setTimeCreated(new Date());
		transactionHistory.setTransactionId(payment.getTransaction().getId());
		transactionHistory.setWriter(payment.getWriter());		
		transactionHistory.setLoginId(payment.getLoginId());
		payment.getTransaction().setTimeSettled(new Date());
		transactionService.updateTransaction(payment.getTransaction(), transactionHistory);
				
		//TODO
		return true;
	}
}
