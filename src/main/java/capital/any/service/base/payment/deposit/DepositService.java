package capital.any.service.base.payment.deposit;

import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import capital.any.base.enums.Table;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.BalanceHistory.BalanceHistoryCommand;
import capital.any.model.base.BalanceRequest;
import capital.any.model.base.MarketingAttribution;
import capital.any.model.base.Transaction;
import capital.any.model.base.Transaction.TransactionOperationEnum;
import capital.any.model.base.Transaction.TransactionStatusEnum;
import capital.any.model.base.TransactionHistory;
import capital.any.model.base.TransactionStatus;
import capital.any.model.base.UserControllerDetails;
import capital.any.model.base.BalanceRequest.OperationType;
import capital.any.model.base.Payment;
import capital.any.service.base.IBalanceService;
import capital.any.service.base.IServerConfiguration;
import capital.any.service.base.currency.ICurrencyService;
import capital.any.service.base.marketingAttribution.IMarketingAttributionService;
import capital.any.service.base.payment.handler.PaymentHandler;
import capital.any.service.base.transaction.ITransactionService;
import capital.any.service.base.transactionOperation.ITransactionOperationService;
import capital.any.service.base.transactionStatus.ITransactionStatusService;

/**
 * @author LioR SoLoMoN
 *
 */
@Service
public class DepositService implements IDepositService {
	private static final Logger logger = LoggerFactory.getLogger(DepositService.class);
	@Autowired
	private ITransactionService transactionService;
	@Autowired
	private IBalanceService balanceService;
	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private ICurrencyService currencyService;
	@Autowired
	private ITransactionOperationService transactionOperationService;
	@Autowired
	private ITransactionStatusService transactionStatusService;
	@Autowired
	private IMarketingAttributionService marketingAttributionService;

	@Override
	public void validationsCheck(Payment<?> payment) {
		payment.getResponse().setResponseCode(ResponseCode.OK);
		// TODO Auto-generated method stub
		logger.info("TODO, implement validationsCheck - general check");
		logger.info("TODO, implement validationsCheck - provider check");

	}

	@Override
	public void limitationsCheck(Payment<?> payment) {
		payment.getResponse().setResponseCode(ResponseCode.OK);
		// TODO Auto-generated method stub
		logger.info("TODO, implement limitationsCheck");
	}

	@Override
	public void executeTransaction(Payment<?> payment) throws Exception {
		
		Transaction transaction = new Transaction();
		transaction.setUser(payment.getUser());
		transaction.setAmount(payment.getPaymentDetails().getAmount());
		transaction.setStatus(transactionStatusService.get().get(TransactionStatusEnum.STARTED.getId()));
		transaction.setIp(payment.getIp());
		transaction.setOperation(transactionOperationService.get().get(TransactionOperationEnum.CREDIT.getId()));
		transaction.setPaymentType(payment.getPaymentDetails().getPaymentType());
		transaction.setRate(currencyService.getRate(payment.getUser().getCurrencyId()));
		transaction.setTimeCreated(new Date());
		transaction.setComments(payment.getPaymentDetails().getComments()); // TODO		
		TransactionHistory transactionHistory = 
				new TransactionHistory(transaction.getId(),
						transaction.getStatus(),
						payment.getWriter(),
						payment.getServerName(),
						payment.getActionSource());
		transactionService.insertTransaction(transaction, transactionHistory);
		//deposit.getPaymentDetails().getDetails()
		payment.setTransaction(transaction);

		payment.getResponse().setResponseCode(ResponseCode.OK);
	}
	
	@Override
	public void changeBalance(Payment<?> payment) throws Exception {
		BalanceRequest br = new BalanceRequest();
		br.setAmount(payment.getTransaction().getAmount());
		br.setCommandHistory(BalanceHistoryCommand.SUCCESS_TRANSACTION);
		br.setOperationType(OperationType.CREDIT);
		br.setReferenceId(payment.getTransaction().getId());
		br.setUser(payment.getUser());
		br.setUserControllerDetails(new UserControllerDetails(payment.getActionSource()
				,payment.getTransaction().getIp()
				,payment.getLoginId(), payment.getWriter().getId()));
		logger.info("balance before credit: " + br.getUser().getBalance());
		balanceService.changeBalance(br);
		logger.info("balance after credit: " + br.getUser().getBalance());	
	}
	
	@Override
	public void doClearing(Payment<?> payment) throws Exception {;
		logger.info("doClearing");
		payment.getResponse().setResponseCode(ResponseCode.OK);
		Class<?> clazz = Class.forName(payment.getPaymentDetails().getPaymentType().getHandlerClass());
		PaymentHandler paymentHandler = (PaymentHandler) clazz.newInstance();
		applicationContext.getAutowireCapableBeanFactory().autowireBean(paymentHandler);
		paymentHandler.doDepositAction(payment);
		payment.getPaymentResponse().setTransaction(payment.getTransaction());
	}

	@Override
	public void doMarketingFlow(Payment<?> payment) {
		/* Marketing Attribution */
		if (payment.getUser().getFtdId() == 0) {
			long trackingId = 1;
			if (null != payment.getLogin() && null != payment.getLogin().getMarketingTracking()) {
				trackingId = payment.getLogin().getMarketingTracking().getId();
			}
			MarketingAttribution marketingAttribution = new MarketingAttribution(Table.TRANSACTIONS.getId(), 
					payment.getTransaction().getId(), trackingId, payment.getUser().getId());
			marketingAttributionService.insert(marketingAttribution);
		}
		payment.getResponse().setResponseCode(ResponseCode.OK);
	}

	@Override
	public void failTransaction(Payment<?> payment) throws Exception {
		payment.getTransaction().setStatus(new TransactionStatus(TransactionStatusEnum.FAILED.getId()));
		payment.getTransaction().setTimeSettled(new Date());
		payment.getTransaction().setComments(payment.getResponse().getResponseCode().getName()); //TODO change
		TransactionHistory transactionHistory = 
				new TransactionHistory(payment.getTransaction().getId(),
						payment.getTransaction().getStatus(),
						payment.getWriter(),
						payment.getServerName(),
						payment.getActionSource(),
						new Date(),
						payment.getLoginId());
		transactionService.updateTransaction(payment.getTransaction(), transactionHistory);		
	}
}
