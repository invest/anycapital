package capital.any.service.base.payment.handler;

import capital.any.model.base.Payment;

/**
 * @author LioR SoLoMoN
 *
 */
public interface PaymentHandler {
	
	/**
	 * 
	 */
	void check();
	
	/**
	 * @param payment
	 * @return
	 */
	boolean doDepositAction(Payment<?> payment);
	
	/**
	 * @param payment
	 * @return
	 */
	boolean doWithdrawAction(Payment<?> payment);

}
