package capital.any.service.base.payment.handler;

import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import capital.any.model.base.Payment;
import capital.any.model.base.Wire;
import capital.any.model.base.Transaction.TransactionStatusEnum;
import capital.any.model.base.TransactionHistory;
import capital.any.service.base.transaction.ITransactionService;
import capital.any.service.base.transactionStatus.ITransactionStatusService;
import capital.any.service.base.wire.IWireService;

/**
 * @author LioR SoLoMoN
 *
 */
@Service
public class WirePaymentHandler implements PaymentHandler {
	private static final Logger logger = LoggerFactory.getLogger(WirePaymentHandler.class);
	@Autowired
	private IWireService wireService;
	@Autowired
	private ITransactionService transactionService;
	@Autowired
	private ITransactionStatusService transactionStatusService;
	
	@Override
	public void check() {
		logger.info("WirePaymentHandler check");
	}

	@Override
	public boolean doDepositAction(Payment<?> payment) {		
		Wire wire = (Wire)payment.getPaymentDetails().getDetails();
		wire.setTransactionId(payment.getTransaction().getId());
		wireService.insertWire(wire);	
		payment.getTransaction().setStatus(transactionStatusService.get().get(TransactionStatusEnum.PENDING.getId()));
		TransactionHistory transactionHistory = new TransactionHistory();
		transactionHistory.setActionSource(payment.getActionSource());
		transactionHistory.setServerName(payment.getServerName());
		transactionHistory.setStatus(payment.getTransaction().getStatus());
		transactionHistory.setTimeCreated(new Date());
		transactionHistory.setTransactionId(payment.getTransaction().getId());
		transactionHistory.setWriter(payment.getWriter());		
		payment.getTransaction().setTimeSettled(new Date());
		transactionService.updateTransaction(payment.getTransaction(), transactionHistory);
	
		//TODO
		return true;
	}

	@Override
	public boolean doWithdrawAction(Payment<?> payment) {
		Wire wire = (Wire)payment.getPaymentDetails().getDetails();
		wire.setTransactionId(payment.getTransaction().getId());
		wireService.insertWire(wire);	
		payment.getTransaction().setStatus(transactionStatusService.get().get(TransactionStatusEnum.WAITING_FOR_FIRST_APPROVE.getId()));
		TransactionHistory transactionHistory = new TransactionHistory();
		transactionHistory.setActionSource(payment.getActionSource());
		transactionHistory.setServerName(payment.getServerName());
		transactionHistory.setStatus(payment.getTransaction().getStatus());
		transactionHistory.setTimeCreated(new Date());
		transactionHistory.setTransactionId(payment.getTransaction().getId());
		transactionHistory.setWriter(payment.getWriter());		
		transactionHistory.setLoginId(payment.getLoginId());
		payment.getTransaction().setTimeSettled(new Date());
		transactionService.updateTransaction(payment.getTransaction(), transactionHistory);
				
		//TODO
		return true;
	}
}
