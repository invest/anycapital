package capital.any.service.base.payment.withdraw;

import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import capital.any.base.limitation.withdraw.IWithdrawLimitationService;
import capital.any.base.rule.WithdrawRule;
import capital.any.base.validation.withdraw.IWithdrawValidationService;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.BalanceRequest;
import capital.any.model.base.Payment;
import capital.any.model.base.Transaction;
import capital.any.model.base.TransactionHistory;
import capital.any.model.base.TransactionReject;
import capital.any.model.base.TransactionStatus;
import capital.any.model.base.UserControllerDetails;
import capital.any.model.base.BalanceHistory.BalanceHistoryCommand;
import capital.any.model.base.BalanceRequest.OperationType;
import capital.any.model.base.Transaction.TransactionOperationEnum;
import capital.any.model.base.Transaction.TransactionPaymentTypeEnum;
import capital.any.model.base.Transaction.TransactionStatusEnum;
import capital.any.service.base.IBalanceService;
import capital.any.service.base.currency.ICurrencyService;
import capital.any.service.base.payment.handler.PaymentHandler;
import capital.any.service.base.transaction.ITransactionService;
import capital.any.service.base.transactionOperation.ITransactionOperationService;
import capital.any.service.base.transactionReject.ITransactionRejectService;
import capital.any.service.base.transactionStatus.ITransactionStatusService;

/**
 * @author LioR SoLoMoN
 *
 */
@Service
public class WithdrawService implements IWithdrawService {
	private static final Logger logger = LoggerFactory.getLogger(WithdrawService.class);
	@Autowired
	private ITransactionService transactionService;
	@Autowired
	private IBalanceService balanceService;
	@Autowired
	private ICurrencyService currencyService;
	@Autowired
	private ITransactionOperationService transactionOperationService;
	@Autowired
	private ITransactionStatusService transactionStatusService;
	@Autowired
	private IWithdrawValidationService withdrawValidatationService;
	@Autowired
	private IWithdrawLimitationService withdrawLimitationService;
	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private ITransactionRejectService transactionRejectService;
	
	@Override
	public void validationsCheck(Payment<?> payment) {
		logger.debug("START withdraw; validationsCheck ");
		payment.setResponse(new Response<Object>(null, ResponseCode.OK));

		for (WithdrawRule rule : withdrawValidatationService.getRules()) {
			Response<?> inspect = rule.inspect(payment);
			if (!inspect.getResponseCode().isOK()) {
				payment.setResponse(inspect);
				TransactionReject transactionReject =
						new TransactionReject(payment.getUser().getId(),
								TransactionPaymentTypeEnum.getById(payment.getPaymentDetails().getPaymentType().getId()), 
								rule.getRejectType(), 
								TransactionOperationEnum.DEBIT, 
								payment.getPaymentDetails().getAmount(), 
								"session",  //TODO
								payment.getActionSource(), 
								payment.getWriter().getId(), 
								"info", //TODO
								payment.getServerName());
				transactionRejectService.insert(transactionReject );
				break;
			}
		}
		
		logger.debug("END withdraw; validationsCheck " + payment);
	}

	@Override
	public void limitationsCheck(Payment<?> payment) {
		logger.debug("START withdraw; limitationsCheck");
		payment.setResponse(new Response<Object>(null, ResponseCode.OK));		
		for (WithdrawRule rule : withdrawLimitationService.getRules()) {
			Response<?> inspect = rule.inspect(payment);
			if (!inspect.getResponseCode().isOK()) {
				payment.setResponse(inspect);
				TransactionReject transactionReject =
						new TransactionReject(payment.getUser().getId(),
								TransactionPaymentTypeEnum.getById(payment.getPaymentDetails().getPaymentType().getId()), 
								rule.getRejectType(), 
								TransactionOperationEnum.DEBIT, 
								payment.getPaymentDetails().getAmount(), 
								"session", //TODO
								payment.getActionSource(), 
								payment.getWriter().getId(), 
								"info", //TODO
								payment.getServerName());
				transactionRejectService.insert(transactionReject );
				break;
			}
		}
		logger.debug("END withdraw; limitationsCheck; " + payment);
	}

	@Override
	public void executeTransaction(Payment<?> payment) throws Exception {
		logger.debug("withdraw; executeChange");

		Transaction transaction = new Transaction();
		transaction.setUser(payment.getUser());
		transaction.setAmount(payment.getPaymentDetails().getAmount());
		transaction.setStatus(transactionStatusService.get().get(TransactionStatusEnum.STARTED.getId()));
		transaction.setIp(payment.getIp());
		transaction.setOperation(transactionOperationService.get().get(TransactionOperationEnum.DEBIT.getId()));
		transaction.setPaymentType(payment.getPaymentDetails().getPaymentType());
		transaction.setRate(currencyService.getRate(payment.getUser().getCurrencyId()));
		transaction.setTimeCreated(new Date());
		transaction.setComments(payment.getPaymentDetails().getComments()); // TODO

		TransactionHistory transactionHistory = new TransactionHistory();
		transactionHistory.setActionSource(payment.getActionSource());
		transactionHistory.setServerName(payment.getServerName());
		transactionHistory.setStatus(transaction.getStatus());
		transactionHistory.setTransactionId(transaction.getId());
		transactionHistory.setWriter(payment.getWriter());	
		transactionHistory.setLoginId(payment.getLoginId());
		transactionService.insertTransaction(transaction, transactionHistory);

		payment.setTransaction(transaction);
		payment.getPaymentResponse().setTransaction(payment.getTransaction());
	}
	
	@Override
	public void changeBalance(Payment<?> payment) throws Exception {
		payment.setTransaction(transactionService.getTransactionById(payment.getTransaction().getId()));
		BalanceRequest br = new BalanceRequest();
		br.setAmount(payment.getTransaction().getAmount());
		br.setCommandHistory(BalanceHistoryCommand.SUCCESS_TRANSACTION);
		br.setOperationType(OperationType.DEBIT);
		br.setReferenceId(payment.getTransaction().getId());
		br.setUser(payment.getUser());
		br.setUserControllerDetails(new UserControllerDetails(payment.getActionSource()
				,payment.getTransaction().getIp()
				,payment.getLoginId(), payment.getWriter().getId()));
		balanceService.changeBalance(br);

		logger.info("balance after debit: " + br.getUser().getBalance());		
	}

	@Override
	public void doClearing(Payment<?> payment) throws Exception {
		logger.debug("START withdraw; doClearing");
		
		Class<?> clazz = Class.forName(payment.getPaymentDetails().getPaymentType().getHandlerClass());
		PaymentHandler paymentHandler = (PaymentHandler) clazz.newInstance();
		applicationContext.getAutowireCapableBeanFactory().autowireBean(paymentHandler);
		paymentHandler.doWithdrawAction(payment);
		payment.getPaymentResponse().setTransaction(payment.getTransaction());
		
		logger.debug("END withdraw; doClearing");
		//XXX can fail?
	}

	@Override
	public void failTransaction(Payment<?> payment) throws Exception {
		payment.getTransaction().setStatus(new TransactionStatus(TransactionStatusEnum.FAILED.getId()));
		payment.getTransaction().setTimeSettled(new Date());
		payment.getTransaction().setComments(payment.getResponse().getResponseCode().getName()); //TODO change
		
		TransactionHistory transactionHistory = new TransactionHistory();
		transactionHistory.setActionSource(payment.getActionSource());
		transactionHistory.setServerName(payment.getServerName());
		transactionHistory.setStatus(payment.getTransaction().getStatus());
		transactionHistory.setTimeCreated(new Date());
		transactionHistory.setTransactionId(payment.getTransaction().getId());
		transactionHistory.setWriter(payment.getWriter());
		transactionHistory.setLoginId(payment.getLoginId());
		
		transactionService.updateTransaction(payment.getTransaction(), transactionHistory);
		payment.getPaymentResponse().setTransaction(payment.getTransaction());
	}
}
