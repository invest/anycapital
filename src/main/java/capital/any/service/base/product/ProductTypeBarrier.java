package capital.any.service.base.product;

import capital.any.service.base.product.ProductTypeBarrierValidator;
import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;

@Target({TYPE, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = ProductTypeBarrierValidator.class)
@Documented
public @interface ProductTypeBarrier {
	
	String message() default "{}";
	
	Class<?>[] groups() default {};
	
	Class<? extends Payload>[] payload() default {};

	
	String first();

	String second();
    
    @Target({TYPE, ANNOTATION_TYPE})
    @Retention(RUNTIME)
    @Documented
            @interface List
    {
    	ProductTypeBarrier[] value();
    }
}
