package capital.any.service.base.product;

import java.util.List;
import java.util.Map;

import capital.any.base.enums.ProductStatusEnum;
import capital.any.model.base.Product;
import capital.any.model.base.ProductHistory;

/**
 * @author eranl
 *
 */
public interface IProductService {	
	
	/**
	 * Get Products hashmap
	 * @return
	 */
	Map<Long, Product> getProducts();
	
	/**
	 * get product
	 * @param product id
	 * @return product
	 */
	Product getProduct(long id);	
	
	//TODO: what this function do
	List<Product> getHomePageProducts();

	/**
	 * update the product status
	 * @param productId the id of the product to update
	 * @param productStatus the new status for the product
	 * @throws Exception 
	 */
	boolean updateStatus(long productId, ProductStatusEnum productStatus, ProductHistory productHistory) throws Exception;
	
	/**
	 * settle product
	 * @param productId
	 * @param productHistory - with writerId and actionsourceId
	 * @throws Exception 
	 */
	void settle(long productId, ProductHistory productHistory) throws Exception;
}
