package capital.any.service.base.product;

import capital.any.base.enums.ProductTypeEnum;
import capital.any.model.base.Product;
import capital.any.model.base.ProductBarrier;
import capital.any.model.base.ProductType;
import capital.any.service.base.product.ProductTypeBarrier;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanWrapperImpl;


public class ProductTypeBarrierValidator implements ConstraintValidator<ProductTypeBarrier, Object> {

	private static final Logger logger = LoggerFactory.getLogger(ProductTypeBarrierValidator.class);
	private String firstFieldName;
	private String secondFieldName;
	
	@Override
	public void initialize(final ProductTypeBarrier constraintAnnotation) {
		firstFieldName = constraintAnnotation.first();
		secondFieldName = constraintAnnotation.second();
	}

	@Override
	public boolean isValid(final Object value, final ConstraintValidatorContext context) {
		boolean isValidated = true;
		try {
			BeanWrapperImpl wrapper = new BeanWrapperImpl(value);
			final Object firstObj = wrapper.getPropertyValue(firstFieldName);
			final Object secondObj = wrapper.getPropertyValue(secondFieldName);
			ProductType productType = (ProductType) firstObj;
			ProductBarrier productBarrier = (ProductBarrier) secondObj;
			String errorMsg = "";
			if (productBarrier != null) {
				if ((productType.getId() == ProductTypeEnum.BARRIER_CAPITAL_PROTECTION_CERTIFICATE.getId() || 
					 productType.getId() == ProductTypeEnum.INVERSE_BARRIER_REVERSE_CONVERTIBLE.getId()) &&  
					 productBarrier.getBarrierLevel() <= 100) {
					 errorMsg = "barrier-more-error";
					 isValidated = false;
				} else if ((productType.getId() == ProductTypeEnum.BARRIER_REVERSE_CONVERTIBLE.getId() || 
						   productType.getId() == ProductTypeEnum.EXPRESS_CERTIFICATE.getId() ||
						   productType.getId() == ProductTypeEnum.BONUS_CERTIFICATE.getId()) &&  
						   productBarrier.getBarrierLevel() >= 100){
					  errorMsg = "barrier-less-error";
					  isValidated = false;
				}
			}
			if (!isValidated) {
				context.disableDefaultConstraintViolation();
				context.buildConstraintViolationWithTemplate(errorMsg).addConstraintViolation(); 
			}
		} catch (final Exception ignore) {
			logger.error("cannot validate dates", ignore);
		}
		return isValidated;
	}
}
