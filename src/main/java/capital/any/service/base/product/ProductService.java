package capital.any.service.base.product;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import capital.any.base.enums.EmailAction;
import capital.any.base.enums.EmailGroup;
import capital.any.base.enums.EmailParameters;
import capital.any.base.enums.EmailType;
import capital.any.base.enums.InvestmentStatusEnum;
import capital.any.base.enums.LanguageEnum;
import capital.any.base.enums.ProductHistoryAction;
import capital.any.base.enums.ProductStatusEnum;
import capital.any.dao.base.dayCountConvention.IDayCountConventionDao;
import capital.any.dao.base.market.IMarketDao;
import capital.any.dao.base.product.IProductDao;
import capital.any.dao.base.productAutocall.IProductAutocallDao;
import capital.any.dao.base.productBarrier.IProductBarrierDao;
import capital.any.dao.base.productBarrierType.IProductBarrierTypeDao;
import capital.any.dao.base.productCallable.IProductCallableDao;
import capital.any.dao.base.productCategory.IProductCategoryDao;
import capital.any.dao.base.productCoupon.IProductCouponDao;
import capital.any.dao.base.productCouponFrequency.IProductCouponFrequencyDao;
import capital.any.dao.base.productMarket.IProductMarketDao;
import capital.any.dao.base.productMaturity.IProductMaturityDao;
import capital.any.dao.base.productSimulation.IProductSimulationDao;
import capital.any.dao.base.productStatus.IProductStatusDao;
import capital.any.dao.base.productType.IProductTypeDao;
import capital.any.model.base.DayCountConvention;
import capital.any.model.base.Email;
import capital.any.model.base.Investment;
import capital.any.model.base.MailInfo;
import capital.any.model.base.Market;
import capital.any.model.base.Product;
import capital.any.model.base.ProductBarrierType;
import capital.any.model.base.ProductCategory;
import capital.any.model.base.ProductCouponFrequency;
import capital.any.model.base.ProductHistory;
import capital.any.model.base.ProductMarket;
import capital.any.model.base.ProductMaturity;
import capital.any.model.base.ProductStatus;
import capital.any.model.base.ProductType;
import capital.any.service.base.IServerConfiguration;
import capital.any.service.base.email.IEmailService;
import capital.any.service.base.emailGroup.IEmailGroupService;
import capital.any.service.base.investment.IInvestmentService;
import capital.any.service.base.productAutocall.IProductAutocallService;
import capital.any.service.base.productBarrier.IProductBarrierService;
import capital.any.service.base.productCallable.IProductCallableService;
import capital.any.service.base.productCoupon.IProductCouponService;
import capital.any.service.base.productHistory.IProductHistoryService;
import capital.any.service.base.productKidLanguage.IProductKidLanguageService;
import capital.any.service.base.productMarket.IProductMarketService;
import capital.any.service.base.productSimulation.IProductSimulationService;

/**
 * @author eranl
 *
 */
@Service
public class ProductService implements IProductService {
	private static final Logger logger = LoggerFactory.getLogger(ProductService.class);
	@Autowired
	protected IProductDao productDao;
	@Autowired
	@Qualifier("ProductHCDao")
	private IProductDao productHCDao;
	@Autowired
	private IMarketDao marketDao;
	@Autowired
	private IProductTypeDao productTypeDao;
	@Autowired
	private IProductStatusDao productStatusDao;
	@Autowired
	private IProductCategoryDao productCategoryDao;
	@Autowired
	private IProductCouponFrequencyDao productCouponFrequencyDao;
	@Autowired
	private IDayCountConventionDao dayCountConventionDao;
	@Autowired
	private IProductMaturityDao productMaturityDao;
	@Autowired
	protected IProductBarrierTypeDao productBarrierTypeDao;
	@Autowired
	private IProductAutocallDao productAutocallDao;
	@Autowired
	private IProductCallableDao productCallableDao;
	@Autowired
	private IProductCouponDao productCouponDao;
	@Autowired
	private IProductMarketDao productMarketDao;
	@Autowired
	private IProductSimulationDao productSimulationDao;
	@Autowired
	protected IProductBarrierDao productBarrierDao;
	@Autowired
	protected IProductAutocallService productAutocallService;
	@Autowired
	protected IProductCallableService productCallableService;
	@Autowired
	protected IProductCouponService productCouponService;
	@Autowired
	protected IProductMarketService productMarketService;
	@Autowired
	protected IProductSimulationService productSimulationService;
	@Autowired
	protected IProductBarrierService productBarrierService;
	
	@Autowired
	private IInvestmentService investmentService;
	
	@Autowired
	private IServerConfiguration serverConfiguration;
	
	@Autowired
	private ObjectMapper mapper;
	
	@Autowired
	private IProductHistoryService productHistoryService;
	
	@Autowired
	private IEmailGroupService emailGroupService;
	
	@Autowired
	private IEmailService emailService;
	
	@Autowired
	private IProductKidLanguageService productKidLanguageService;

	@Override
	public Map<Long, Product> getProducts() {
		Map<Long, Product> products = productHCDao.getProducts();
		if (products == null || products.size() <= 0) {
			logger.info("products empty / null in hazelcast, Let's check RDBMS");
			products = productDao.getProducts();
			setProductsFull(products.values());
		}
		return products;
	}

	@Override
	public Product getProduct(long id) {
		Product product = productHCDao.getProduct(id);
		if (product == null) {
			logger.info("product empty / null in hazelcast, Let's check RDBMS");	
			product = productDao.getProduct(id);
			if (product != null) {
				Map<Integer, Market> markets = marketDao.getMarketsWithLastPrice();
				Map<Integer, ProductBarrierType> productBarrierTypes = productBarrierTypeDao.get();
				Map<Integer, ProductType> productTypes = productTypeDao.get();
				Map<Integer, ProductStatus> productStatus = productStatusDao.get();
				Map<Integer, ProductCategory> productCategory = productCategoryDao.get();
				Map<Integer, ProductCouponFrequency> productCouponFrequency = productCouponFrequencyDao.get();
				Map<Integer, DayCountConvention> dayCountConvention = dayCountConventionDao.get();
				Map<Integer, ProductMaturity> productMaturities = productMaturityDao.get();
				
				setProductsFull(
						product, markets, 
						productBarrierTypes, productTypes, productStatus,
						productCategory, productCouponFrequency, 
						dayCountConvention, productMaturities);
			}
		} 
		return product;
	}
	
	public void setProductsFull(Product product, Map<Integer, Market> markets,
			Map<Integer, ProductBarrierType> productBarrierTypes,
			Map<Integer, ProductType> productTypes,
			Map<Integer, ProductStatus> productStatus,
			Map<Integer, ProductCategory> productCategory,
			Map<Integer, ProductCouponFrequency> productCouponFrequency,
			Map<Integer, DayCountConvention> dayCountConvention,
			Map<Integer, ProductMaturity> productMaturities) {

		product.setProductAutocalls(productAutocallDao.getProductAutocalls(product.getId()));
		product.setProductCallables(productCallableDao.getProductCallables(product.getId()));
		product.setProductCoupons(productCouponDao.getProductCoupons(product.getId()));
		product.setProductMarkets(productMarketDao.getProductMarkets(product.getId()));
		for (ProductMarket productMarket : product.getProductMarkets()) {
			productMarket.setMarket(markets.get(productMarket.getMarket().getId()));
		}
		product.setProductSimulation(productSimulationDao.getProductSimulations(product.getId()));
		product.setProductBarrier(productBarrierDao.get(product.getId()));
		if (product.getProductBarrier() != null) {
			product.getProductBarrier().setProductBarrierType((productBarrierTypes.get(product.getProductBarrier().getProductBarrierType().getId())));
			if (ProductStatusEnum.SUBSCRIPTION.getId() < product.getProductStatus().getId()) {
				product.getProductBarrier().calculateDistanceToBarrier(new BigDecimal(String.valueOf(product.getProductMarkets().get(0).getMarket().getLastPrice().getPrice())), 
																				new BigDecimal(String.valueOf(product.getProductMarkets().get(0).getStartTradeLevel())));
			}
		}
		product.setProductStatus(productStatus.get(product.getProductStatus().getId()));
		product.setProductType(productTypes.get(product.getProductType().getId()));
		product.getProductType().setProductCategory(productCategory.get(product.getProductType().getProductCategory().getId()));
		if (product.getProductCouponFrequency() != null) {
			product.setProductCouponFrequency(productCouponFrequency.get(product.getProductCouponFrequency().getId()));
		}
		if (product.getDayCountConvention() != null) {
			product.setDayCountConvention(dayCountConvention.get(product.getDayCountConvention().getId()));
		}
		product.setProductMaturity(productMaturities.get(product.getProductMaturity().getId()));
		product.calculateScenarios();
		product.calculateSimulation();
		product.calculateSlider();
		product.setProductKidLanguages(productKidLanguageService.get(product));
	}
	
	public Collection<Product> setProductsFull(Collection<Product> products) {
		Map<Integer, Market> markets = marketDao.getMarketsWithLastPrice();
		Map<Integer, ProductBarrierType> productBarrierTypes = productBarrierTypeDao.get();
		Map<Integer, ProductType> productTypes = productTypeDao.get();
		Map<Integer, ProductStatus> productStatus = productStatusDao.get();
		Map<Integer, ProductCategory> productCategory = productCategoryDao.get();
		Map<Integer, ProductCouponFrequency> productCouponFrequency = productCouponFrequencyDao.get();
		Map<Integer, DayCountConvention> dayCountConvention = dayCountConventionDao.get();
		Map<Integer, ProductMaturity> productMaturities = productMaturityDao.get();
		
		for (Product product : products) {
			setProductsFull(
					product, markets, 
					productBarrierTypes, productTypes, productStatus,
					productCategory, productCouponFrequency, 
					dayCountConvention, productMaturities);
		}
		
		return products;
	}

	@Override
	public List<Product> getHomePageProducts() {
		List<Product> products = productDao.getHomePageProducts();
		/*Map<Integer, ProductType> productTypes = (Map<Integer, ProductType>) HazelCastClientFactory.getClient().getMap(Constants.MAP_PRODUCT_TYPES);
		Map<Integer, ProductCategory> productCategory = (Map<Integer, ProductCategory>) HazelCastClientFactory.getClient().getMap(Constants.MAP_PRODUCT_CATEGORIES);
		Map<Integer, ProductCouponFrequency> productCouponFrequency = (Map<Integer, ProductCouponFrequency>) HazelCastClientFactory.getClient().getMap(Constants.MAP_PRODUCT_COUPON_FREQUENCIES);
		Map<Integer, DayCountConvention> dayCountConvention = (Map<Integer, DayCountConvention>) HazelCastClientFactory.getClient().getMap(Constants.MAP_DAY_COUNT_CONVINTION);
		Map<Integer, Market> markets = (Map<Integer, Market>) HazelCastClientFactory.getClient().getMap(Constants.MAP_MARKETS);
		for (Product product : products) {
			product.setProductMarkets(productMarketService.getProductMarkets(product.getId()));
			for (ProductMarket productMarket : product.getProductMarkets()) {
				productMarket.setMarket(markets.get(productMarket.getMarket().getId()));
			}
			product.setProductType(productTypes.get(product.getProductType().getId()));
			product.getProductType().setProductCategory(productCategory.get(product.getProductType().getProductCategory().getId()));
			product.setProductCouponFrequency(productCouponFrequency.get(product.getProductCouponFrequency().getId()));
			product.setDayCountConvention(dayCountConvention.get(product.getDayCountConvention().getId()));
			product.setProductBarrier(productBarrierService.get(product.getId())); //no need the type name
			product.calculateScenarios();
		}
		return products;*/
		return products;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean updateStatus(long productId, ProductStatusEnum productStatus, ProductHistory productHistory) throws Exception {
		boolean result = productDao.updateStatus(productId, productStatus);
		if (result) {
			ObjectNode json = mapper.createObjectNode();
			json.put("status_id", productStatus.getId());
			productHistory.setDetails(productId,
					new capital.any.model.base.ProductHistoryAction(ProductHistoryAction.UPDATE_STATUS.getId()),
					json.toString(), serverConfiguration.getServerName());
			productHistoryService.insert(productHistory);
		}
		if (result) {
			result = productHCDao.updateStatus(productId, productStatus);
		}
		return result;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void settle(long productId, ProductHistory productHistory) throws Exception {
		logger.info("Start settle Products " + productId);
		Product product = getProduct(productId);
		if (product.getProductMarkets().get(0).getEndTradeLevel() != null) {
			List<Integer> statuses = new ArrayList<Integer>();
			statuses.add(InvestmentStatusEnum.OPEN.getId());
			List<Investment> investments = investmentService.getByStatusesAndProductId(productId, statuses);
			if (!investments.isEmpty()) {				
				product.setProductStatus(new ProductStatus(ProductStatusEnum.SETTLED.getId()));
				for (Investment investment : investments) {
					investmentService.settle(investment, product, productHistory.getActionSource());
				}
			}
			updateStatus(productId, ProductStatusEnum.SETTLED, productHistory);
		} else {
			Map<String, String> map = new HashMap<String, String>();
			map.put(EmailParameters.PARAM_CONTENT.getParam(), "Can't settle product id " + productId + " no end trade level");
			MailInfo mailInfo = new MailInfo(emailGroupService.get(EmailGroup.TRADERS.getId()), "Can't settle product", map, EmailAction.PRODUCT_SETTLE.getId(), EmailType.TEMPLATE.getId(), LanguageEnum.EN.getId());
			try {
				Email email = new Email();		
				email.setActionSource(productHistory.getActionSource());
				email.setEmailInfo(mapper.writeValueAsString(mailInfo));
				emailService.insert(email);
			} catch (JsonProcessingException e) {
				logger.error("cant send email", e);
			}
			logger.error("can't settle product id " + productId + " no end trade level");
		}
	}
}
