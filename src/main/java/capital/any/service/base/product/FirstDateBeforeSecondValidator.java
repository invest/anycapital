package capital.any.service.base.product;

import capital.any.service.IUtilService;
import capital.any.service.base.product.FirstDateBeforeSecond;
import java.util.Date;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;

public class FirstDateBeforeSecondValidator implements ConstraintValidator<FirstDateBeforeSecond, Object> {
	
	@Autowired
	private IUtilService utilService;
	private static final Logger logger = LoggerFactory.getLogger(FirstDateBeforeSecondValidator.class);
	private String firstFieldName;
	private String secondFieldName;

	@Override
	public void initialize(final FirstDateBeforeSecond constraintAnnotation) {
		firstFieldName = constraintAnnotation.first();
		secondFieldName = constraintAnnotation.second();
	}

	@Override
	public boolean isValid(final Object value, final ConstraintValidatorContext context) {
		try {
			
			BeanWrapperImpl wrapper = new BeanWrapperImpl(value);
			final Object firstObj = wrapper.getPropertyValue(firstFieldName);
			
			BeanWrapperImpl wrapper2 = new BeanWrapperImpl(value);
			final Object secondObj = wrapper2.getPropertyValue(secondFieldName);

		
			Date firstObjDate = (Date) firstObj;
			Date secondObjDate = (Date) secondObj;
			utilService.setStartTime(firstObjDate);
			utilService.setStartTime(secondObjDate);

			return firstObjDate != null && secondObjDate != null && firstObjDate.before(secondObjDate);

			
		} catch (final Exception ignore) {
			logger.error("cannot validate dates", ignore);
		}
		return true;
	}
}
