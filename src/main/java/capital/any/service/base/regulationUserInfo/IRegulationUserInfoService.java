package capital.any.service.base.regulationUserInfo;

import capital.any.model.base.RegulationUserInfo;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IRegulationUserInfoService {

	/**
	 * 
	 * @param regulationUserInfo
	 */
	void insert(RegulationUserInfo regulationUserInfo) throws Exception;

	/**
	 * 
	 * @param regulationUserInfo
	 * @throws Exception
	 */
	void update(RegulationUserInfo regulationUserInfo) throws Exception;

	/**
	 * Get RegulationUserInfo
	 * @param userId
	 * @return
	 * @throws Exception 
	 */
	RegulationUserInfo get(long userId);

}
