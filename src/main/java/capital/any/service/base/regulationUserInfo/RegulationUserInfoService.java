package capital.any.service.base.regulationUserInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.regulationUserInfo.IRegulationUserInfoDao;
import capital.any.model.base.RegulationUserInfo;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service
public class RegulationUserInfoService implements IRegulationUserInfoService {
	private static final Logger logger = LoggerFactory.getLogger(RegulationUserInfoService.class);
	
	@Autowired
	private IRegulationUserInfoDao regulationUserInfoDao;
	
	@Override
	public void insert(RegulationUserInfo regulationUserInfo) throws Exception {
		logger.info("About to insert into RegulationUserInfo. " + regulationUserInfo.toString());
		regulationUserInfoDao.insert(regulationUserInfo);
	}
	
	@Override
	public void update(RegulationUserInfo regulationUserInfo) throws Exception {
		logger.info("About to update RegulationUserInfo. " + regulationUserInfo.toString());
		regulationUserInfoDao.update(regulationUserInfo);
	}
	
	@Override
	public RegulationUserInfo get(long userId) {
		logger.info("About to get from RegulationUserInfo. userId = " + userId);
		return regulationUserInfoDao.get(userId);
	}
}
