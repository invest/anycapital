package capital.any.service.base.dbParameter;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import capital.any.dao.base.dbParameter.IDBParameterDao;
import capital.any.model.base.DBParameter;

/**
 * @author Eyal Goren
 *
 */
@Service
@Primary
public class DBParameterService implements IDBParameterService {
	
	private static final Logger logger = LoggerFactory.getLogger(DBParameterService.class);
	
	@Autowired
	private IDBParameterDao dbParameterDao;
	
	@Autowired
	@Qualifier("DBParameterHCDao")
	private IDBParameterDao dbParameterHCDao;
	
	@Override
	public Map<Integer, DBParameter> get() {
		Map<Integer, DBParameter> DBParameters = dbParameterHCDao.get();
		if (DBParameters == null || DBParameters.size() <= 0) {
			logger.info("DBParameters empty / null in hazelcast, Let's check RDBMS");
			DBParameters = dbParameterDao.get();
		}
		return DBParameters;
	}
	
	@Override
	public DBParameter get(Integer id) {
		return get().get(id);
	}
}
