package capital.any.service.base.dbParameter;

import java.util.Map;

import capital.any.model.base.DBParameter;

/**
 * @author eranl
 *
 */
public interface IDBParameterService {	
	
	Map<Integer, DBParameter> get();

	/**
	 * get db parameter by id
	 * @param id
	 * @return {@link DBParameter}
	 */
	DBParameter get(Integer id);

}
