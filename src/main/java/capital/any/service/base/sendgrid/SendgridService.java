package capital.any.service.base.sendgrid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;

import capital.any.base.enums.DBParameter;
import capital.any.model.base.Sendgrid;
import capital.any.service.base.dbParameter.IDBParameterService;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service
public class SendgridService implements ISendgridService {
	private static final Logger logger = LoggerFactory.getLogger(SendgridService.class);
	
	@Autowired
	private IDBParameterService dbParameterService;
	
	/**
	 * patch
	 * @param sendgrid
	 */
	public void patch(Sendgrid sendgrid) {
		logger.info("patch start");
		SendGrid sg = new SendGrid(dbParameterService.get(DBParameter.SENDGRID_API_KEY.getId()).getStringValue());
	    Request request = new Request();
	    try {
	    	request.method = Method.PATCH;
	    	request.endpoint = sendgrid.getEndpoint();
	    	request.body = sendgrid.getBody();
	    	Response response = sg.api(request);
	    	logger.info("response.statusCode: " + response.statusCode);
	    	logger.info("response.body: " + response.body);
	    	logger.info("response.headers: " + response.headers);
	    } catch (Exception e) {
	    	logger.error("can't patch via Sendgrid Service.", e);
	    }
	    logger.info("patch end");
	}
	
	/**
	 * post
	 * @param sendgrid
	 */
	public void post(Sendgrid sendgrid) {
		logger.info("post start");
	    SendGrid sg = new SendGrid(dbParameterService.get(DBParameter.SENDGRID_API_KEY.getId()).getStringValue());
	    Request request = new Request();
	    try {
	    	request.method = Method.POST;
	    	request.endpoint = sendgrid.getEndpoint();
	    	request.body = sendgrid.getBody();
	    	Response response = sg.api(request);
	    	logger.info("response.statusCode: " + response.statusCode);
	    	logger.info("response.body: " + response.body);
	    	logger.info("response.headers: " + response.headers);
	    } catch (Exception e) {
	    	logger.error("can't post via Sendgrid Service.", e);
	    }
	    logger.info("post end");
	}
}
