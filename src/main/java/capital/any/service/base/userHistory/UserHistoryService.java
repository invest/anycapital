package capital.any.service.base.userHistory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.userHistory.IUserHistoryDao;
import capital.any.model.base.UserHistory;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service
public class UserHistoryService implements IUserHistoryService {
	private static final Logger logger = LoggerFactory.getLogger(UserHistoryService.class);
	
	@Autowired
	private IUserHistoryDao userHistoryDao;
	
	@Override
	public void insert(UserHistory userHistory) {
		try {
			userHistoryDao.insert(userHistory);
		} catch (Exception e) {
			logger.error("Can't insert to user history. " + userHistory.toString(), e);
		}
		
	}
}
