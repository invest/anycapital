package capital.any.service.base.userHistory;

import capital.any.model.base.UserHistory;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IUserHistoryService {

	void insert(UserHistory userHistory);

}
