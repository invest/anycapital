package capital.any.service.base.marketGroup;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import capital.any.dao.base.marketGroup.IMarketGroupDao;
import capital.any.model.base.MarketGroup;

/**
 * @author eranl
 *
 */
@Primary
@Service
public class MarketGroupService implements IMarketGroupService {
	private static final Logger logger = LoggerFactory.getLogger(MarketGroupService.class);
	@Autowired
	private IMarketGroupDao marketGroupDao;
	@Autowired
	@Qualifier("MarketGroupHCDao")
	private IMarketGroupDao marketGroupHCDao;

	@Override
	public Map<Integer, MarketGroup> get() {
		Map<Integer, MarketGroup> marketGroups = marketGroupHCDao.get();
		if (marketGroups == null || marketGroups.size() <= 0) {
			logger.info("marketGroups empty / null in hazelcast, Let's check RDBMS");
			marketGroups = marketGroupDao.get(); 
		}
		return marketGroups;
	}
}
