package capital.any.service.base.marketGroup;

import java.util.Map;
import capital.any.model.base.MarketGroup;

/**
 * @author eranl
 *
 */
public interface IMarketGroupService {	
	
	/**
	 * Get Market Groups hashmap
	 * @return
	 */
	Map<Integer, MarketGroup> get();
}
