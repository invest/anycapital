package capital.any.service.base.transactionStatus;

import java.util.Map;
import capital.any.model.base.TransactionStatus;

/**
 * @author LioR SoLoMoN
 *
 */
public interface ITransactionStatusService {	
	
	/**
	 * Get transaction status map
	 * @return
	 */
	Map<Integer, TransactionStatus> get();
}
