package capital.any.service.base.transactionStatus;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import capital.any.dao.base.transactionStatus.ITransactionStatusDao;
import capital.any.model.base.TransactionStatus;

/**
 * @author LioR SoLoMoN
 *
 */
@Primary
@Service
public class TransactionStatusService implements ITransactionStatusService {
	private static final Logger logger = LoggerFactory.getLogger(TransactionStatusService.class);
	@Autowired
	private ITransactionStatusDao transactionStatusDao;
	@Autowired
	@Qualifier("TransactionStatusHCDao")
	private ITransactionStatusDao transactionStatusHCDao;
	
	@Override
	public Map<Integer, TransactionStatus> get() {
		Map<Integer, TransactionStatus> transactionStatuses = transactionStatusHCDao.get();
		if (transactionStatuses == null || transactionStatuses.size() <= 0) {
			logger.info("transactionStatus empty / null in hazelcast, Let's check RDBMS");
			transactionStatuses = transactionStatusDao.get(); 
		}
		return transactionStatuses;
	}
}
