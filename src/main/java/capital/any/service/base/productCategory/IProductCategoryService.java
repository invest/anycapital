package capital.any.service.base.productCategory;

import java.util.Map;
import capital.any.model.base.ProductCategory;

/**
 * @author eranl
 *
 */
public interface IProductCategoryService {	
	
	/**
	 * Get Product Categories hashmap
	 * @return
	 */
	Map<Integer, ProductCategory> get();
}
