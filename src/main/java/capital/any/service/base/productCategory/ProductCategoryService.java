package capital.any.service.base.productCategory;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import capital.any.dao.base.productCategory.IProductCategoryDao;
import capital.any.model.base.ProductCategory;

/**
 * @author eranl
 *
 */
@Primary
@Service
public class ProductCategoryService implements IProductCategoryService {
	private static final Logger logger = LoggerFactory.getLogger(ProductCategoryService.class);
	@Autowired
	private IProductCategoryDao productCategoryDao;
	@Autowired
	@Qualifier("ProductCategoryHCDao")
	private IProductCategoryDao productCategoryHCDao;
	
	@Override
	public Map<Integer, ProductCategory> get() {
		Map<Integer, ProductCategory> productCategories = productCategoryHCDao.get();
		if (productCategories == null || productCategories.size() <= 0) {
			logger.info("productCategory empty / null in hazelcast, Let's check RDBMS");
			productCategories = productCategoryDao.get(); 
		}
		return productCategories;
	}
}
