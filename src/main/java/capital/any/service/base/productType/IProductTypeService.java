package capital.any.service.base.productType;

import java.util.Map;
import capital.any.model.base.ProductType;

/**
 * @author eranl
 *
 */
public interface IProductTypeService {	
	
	/**
	 * Get Product Types hashmap
	 * @return
	 */
	Map<Integer, ProductType> get();
}
