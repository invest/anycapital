package capital.any.service.base.productType;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import capital.any.dao.base.productType.IProductTypeDao;
import capital.any.model.base.ProductType;

/**
 * @author eranl
 *
 */
@Primary
@Service
public class ProductTypeService implements IProductTypeService {
	private static final Logger logger = LoggerFactory.getLogger(ProductTypeService.class);
	@Autowired
	private IProductTypeDao productTypeDao;
	@Autowired
	@Qualifier("ProductTypeHCDao")
	private IProductTypeDao productTypeHCDao;
	
	@Override
	public Map<Integer, ProductType> get() {
		Map<Integer, ProductType> productTypes = productTypeHCDao.get();
		if (productTypes == null || productTypes.size() <= 0) {
			logger.info("productTypes empty / null in hazelcast, Let's check RDBMS");
			productTypes = productTypeDao.get(); 
		}
		return productTypes;
	}
}
