package capital.any.service.base;

/**
 * @author LioR SoLoMoN
 *
 */
public interface IServerConfiguration {
	
	/**
	 * @return server hostname
	 */
	String getServerName();
	
	/**
	 * geographic location by ip
	 * 
	 * assumptions: The string contain ", " after ": " 
	 * @param ip
	 * @return fields <b>if</b> found = [country, region, city, postalCode, latitude, longitude, metroCode, areaCode]
	 * <br/><b>else</b> return null
	 */
	String[] getGeoLocation(String ip);
	
	/**
	 * assumption: email with @
	 * @param email, example: lior@anycapital.com
	 * @return true iff checkmx command return 1 on the received domain.
	 */
	boolean isValidEmail(String email);
}
