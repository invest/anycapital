package capital.any.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * @author LioR SoLoMoN
 *
 */
@Service
public class XMLService implements IXMLService {
	private static final Logger logger = LoggerFactory.getLogger(XMLService.class); 
	
	//FIXME
	public Document xmlToDocument(String xml) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        byte[] xmldata = xml.getBytes();
        ByteArrayInputStream bais = new ByteArrayInputStream(xmldata);
        Document resp = docBuilder.parse(bais);
        try {
        	bais.close();
        } catch (Exception e) {
        	logger.error("ERROR! ", e);
        }
        return resp;
    }
}
