package capital.any.service;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * @author LioR SoLoMoN
 *
 */
public interface IXMLService {

	/**
	 * 
	 *  
	 * @param xml
	 * @return
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	Document xmlToDocument(String xml) throws ParserConfigurationException, SAXException, IOException;
}
