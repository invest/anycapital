package capital.any.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import capital.any.service.base.locale.ILocaleService;

/**
 * @author LioR SoLoMoN
 *
 */
@Service
public class UtilService implements IUtilService {
	private static final Logger logger = LoggerFactory.getLogger(UtilService.class);
	private static String UNIX_ERROR_STREAM = "runError: ";

	public boolean isArgumentEmptyOrNull(String parameter) {
		boolean isEmpty = false;
		if (parameter == null || parameter.length() == 0) {
			isEmpty = true;
		}
		return isEmpty;
	}
	
	/**
	 * Run unix command
	 *
	 * @param command
	 *            unix command to run
	 * @return
	 */
	@Override
	public String doUnixCommand(String command) {
		String line = "";
		StringBuffer result = new StringBuffer();
		Process process = null;
		BufferedReader stdOutput = null;
		BufferedReader stdError = null;
		try {
			logger.info("about to run unix command: " + command);
			process = Runtime.getRuntime().exec(command);
			stdOutput = new BufferedReader(new InputStreamReader(
					process.getInputStream()));
			stdError = new BufferedReader(new InputStreamReader(
					process.getErrorStream()));

			while ((line = stdOutput.readLine()) != null) {
				logger.info(line);
				result.append(line + "\n");
			}

			while ((line = stdError.readLine()) != null) {
				result.append(UNIX_ERROR_STREAM).append(line);
				logger.info(line);
				break;
			}
		} catch (IOException e) {
			logger.info("Exception running unix command: " + e);
			result.append(UNIX_ERROR_STREAM);
		} finally {
			try {
				stdError.close();
				stdOutput.close();
			} catch (Exception e) {
			}
		}

		return result.toString();
	}
	
	@Override
    public String generateObjectToQueryString(Object objRef, String interToken , String endToken, boolean includeNullData) {
    	StringBuilder params = new StringBuilder();
    	String endKeyValue = "";    	
    	try {
	    	Field[] fields = objRef.getClass().getFields();
	    	for (Field field : fields) {
	    		if (field.get(objRef) != null || includeNullData) {
	    			params.append(endKeyValue).append(field.getName()).append(interToken).append(field.get(objRef));
					endKeyValue = endToken;
	    		}
	    	}
	    	logger.debug("generate Object To Query String: " + params);    	
    	} catch (IllegalArgumentException | IllegalAccessException e) {
    		logger.error("ERROR! generateObjectToQueryString, Illegal ", e);
    	} catch (Exception e) {
    		logger.error("ERROR! Can't construct Query String from the retrieved object", e);
    	}
		return params.toString();
	}
	
	@SuppressWarnings("deprecation")
	public Date setEndTime(Date date) {
		date.setHours(23);
		date.setMinutes(59);
		date.setSeconds(59);
		return date;
	}
	
	@SuppressWarnings("deprecation")
	public Date setStartTime(Date date) {
		date.setHours(0);
		date.setMinutes(0);
		date.setSeconds(0);
		return date;
	}
	
	/**
	 * display amount with currency symbol default Locale.GERMANY €
	 * @param amount to format
	 * @param locale null for default or locale 
	 * @return format amount with symbol
	 */
	@Override
	public String displayAmount(long amount, Locale locale, String currencyCode) {
		amount /= 100;
		if (locale == null) {
			locale = new Locale(ILocaleService.DEFAULT_LANGUAGE, ILocaleService.DEFAULT_COUNTRY);
		}
		if (isArgumentEmptyOrNull(currencyCode)) {
			currencyCode = ILocaleService.DEFAULT_CURRENCY;
		}
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);
		numberFormat.setCurrency(Currency.getInstance(currencyCode));
		return numberFormat.format(amount);
	}
	
}
