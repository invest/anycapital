package capital.any.service;

import javax.servlet.http.HttpServletRequest;

import capital.any.model.base.HttpRequest;

/**
 * @author LioR SoLoMoN
 *
 */
public interface IHttpClientService {
	

	/**
	 * Do Post request  
	 * @param request - Headers is not mandatory
	 * @return
	 * @throws Exception
	 */
	<T> T doPost(HttpRequest<T> request) throws Exception;
	
	/**
	 * Do get request
	 * @param request - Headers is not mandatory
	 * @return
	 * @throws Exception
	 */
	<T> T doGet(HttpRequest<T> request) throws Exception;
	
	/**
	 * @param request
	 * @return
	 */
	public String getIP(HttpServletRequest request);
	
	/**
	 * @param request
	 * @return
	 */
	public String getUserAgent(HttpServletRequest request);
	
}	
