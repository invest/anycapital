package capital.any.service;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Hashtable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import capital.any.base.enums.Table;
import capital.any.model.base.HttpRequest;
import capital.any.model.base.ProviderRequestResponse;
import capital.any.model.base.SMS;
import capital.any.model.base.SMSProvider;
import capital.any.service.base.providerRequestResponse.IProviderRequestResponseService;

/**
 * @author eran.levy
 *
 */
@Service
public class MobivateSMSProvider extends SMSProvider {			
	
	private static final Logger logger = LoggerFactory.getLogger(MobivateSMSProvider.class);
		
	private Hashtable<String, String> ccToRoutingID;
	
	private final String USER_ROUTE_PRICING_URL = "entity/user.UserRoutePricing/ALL/visible"; 
	private final String SEND_SINGLE_URL		= "send/sms/single?";
	
	@Autowired
	private IHttpClientService httpClientService;
	@Autowired
	private IXMLService xmlService;
	
	@Autowired
	private IProviderRequestResponseService providerRequestResponseService;
	
	/** 
	 * Init Mobivate SMS provide in order to build routing according to county of the SMS 
	 */
	public void init() {
		try {
			HttpRequest httpRequest = new HttpRequest<>();
			// "http://app.mobivatebulksms.com/bulksms/xmlapi/username:password/entity/user.UserRoutePricing/ALL/visible");
			httpRequest.setUrl(url + userName + ":" + password + "/" + USER_ROUTE_PRICING_URL);
			Object getAllCCandRouteIDs = httpClientService.doGet(httpRequest);
			Document doc = xmlService.xmlToDocument(getAllCCandRouteIDs.toString());
			ccToRoutingID = parseUserRouting(doc, "userroutepricing", "countryCode", "userRouteId");
		} catch (Exception e) {
			throw new RuntimeException("Mobivate error" + e.getMessage());
		}
	}

	public void send(SMS sms) throws Exception {
		String request;		
		request = "xml=" + URLEncoder.encode(composeTextRequest(sms), "UTF-8");
		HashMap<String, String> headers;
		headers = new HashMap<String, String>();
		// To send Unicode messages, make sure the following header is set:
		headers.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		HttpRequest httpRequest = new HttpRequest<>();
		httpRequest.setHeaders(headers);
		httpRequest.setData(request);
		//http://app.mobivatebulksms.com/bulksms/xmlapi/username:password/send/sms/single?			
		httpRequest.setUrl(url + userName + ":" + password + "/" + SEND_SINGLE_URL);
		Object response = httpClientService.doPost(httpRequest);
		ProviderRequestResponse providerRequestResponse = new ProviderRequestResponse();
		providerRequestResponse.setTableId(Table.SMS.getId());
		providerRequestResponse.setReferenceId(sms.getId());
		providerRequestResponse.setRequest(request);
		providerRequestResponse.setResponse(response.toString());
		providerRequestResponseService.insert(providerRequestResponse);
	}
	
    private Hashtable<String, String> parseUserRouting(Document rootEl, String userRoutePricingTag,
    	    String countryCodeTag, String userRouteIdTag) {
    		Hashtable<String, String> routing = new Hashtable<String, String>();
    	
    		NodeList allChildRootEldocument = rootEl.getElementsByTagName(userRoutePricingTag);
    	
    		for (int i = 0; i < allChildRootEldocument.getLength(); i++) {
    		    Node node = allChildRootEldocument.item(i);
    		    if (node.getNodeType() == Node.ELEMENT_NODE) {
    				Element eElement = (Element) node;
    				String countryCode = eElement.getElementsByTagName(countryCodeTag).item(0).getTextContent();
    				String userRouteId = eElement.getElementsByTagName(userRouteIdTag).item(0).getTextContent();
    				//in case the given first tag has more than one item in it separated by a comma
    				if (countryCode.contains(",")) {
    				    String[] ccSinOneEl = countryCode.split(",");
    				    for (String xtraCC : ccSinOneEl) {
    			    		if(routing.get(xtraCC) == null) {
    			    			routing.put(xtraCC, userRouteId);
    			    		} else {
    			    			logger.info("duplicate routing for:"+xtraCC+ " value:" + userRouteId);
    			    		}
    			    	}
    				} else {
    		    		if(routing.get(countryCode) == null) {
    		    			routing.put(countryCode, userRouteId);
    		    		} else {
    		    			logger.info("duplicate routing for["+countryCode+ "] value [" + userRouteId + "]");
    		    		}
    				}
    		    }
    		}
    		return routing;
        }
    
    private String getRouteID(String msgPhoneNumber) {
		int getRouteIDtries = 1;
		String routeID = null;
		do {
		    routeID = ccToRoutingID.get(msgPhoneNumber.substring(0, getRouteIDtries));
		    getRouteIDtries++;
		} while (routeID == null && getRouteIDtries < 4);//max Country codes are 3(4) digits
	
		return routeID;
    }
    
    private String composeTextRequest(SMS sms) {
		String request =
	        "<message>\n" + 
	        "    <originator>"	+ sms.getSender()	+ "</originator>\n" + 
	        "    <recipient>"	+ sms.getPhone()	+ "</recipient>\n" + 
	        "    <body>"		+ sms.getMessage() 	+ "</body>\n" +
	        //REFERENCE -> Used to correlate message with client reference
	        "    <reference>"	+	sms.getId()		+ "</reference>\n" ;		
	        if(sms.getStatus() == SMS.Status.NOT_VERIFIED) {
	        	//HLR route ID
	        	logger.info("SENDING HLR MESSAGE:" + sms.getId());
	        	request += "    <routeId>mhlrglobal</routeId>\n";
	        } else {
	        	//ROUTE -> UUID which identifies the route used for delivering the message
	        	String routeId = getRouteID(sms.getPhone());
	        	request += "    <routeId>"		+ routeId +"</routeId>\n" ;	        	
	        }
	        request += "</message>" ;	        
	        return request;
    }
			
}
