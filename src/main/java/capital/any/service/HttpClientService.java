package capital.any.service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.model.base.HttpRequest;


/**
 * @author LioR SoLoMoN
 * 
 */
@Service
public class HttpClientService implements IHttpClientService {	
	private static final Logger logger = LoggerFactory.getLogger(HttpClientService.class);	
	private static final String IP_NOT_FOUND = "IP_NOT_FOUND";
	
	@Autowired
	private ObjectMapper mapper;
	@Autowired
	private UtilService utilService;
	
	/**
	 * 
	 * @param serviceUrl
	 * @param clazz
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public <T> T doPost(HttpRequest<T> request) throws Exception {
		HttpURLConnection httpConnection = null;
		try {		
			URL url = new URL(request.getUrl());
			httpConnection = (HttpURLConnection) url.openConnection();
			httpConnection.setRequestMethod("POST");
            if (request.getHeaders() != null) {    
            	for (String key : request.getHeaders().keySet()) {
            		httpConnection.setRequestProperty(key, request.getHeaders().get(key));
            	}
            }
            httpConnection.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(httpConnection.getOutputStream());
			String data = request.getData().toString();
			if (request.isJson()) {
				data = mapper.writeValueAsString(request.getData());
			}						
			wr.writeBytes(data);
			wr.flush();
			wr.close();	
			
			int responseCode = httpConnection.getResponseCode();
			logger.info("\nSending POST request to URL: " + url +
					"\n POST parameters: " + data +
					"\n Response Code : " + responseCode);
			BufferedReader in = new BufferedReader(
					new InputStreamReader(httpConnection.getInputStream(), "UTF-8"));
			String inputLine;
			StringBuffer response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();			
			logger.info("Response: " + response.toString());
			Object returnValue = response.toString();
			if (request.isJson()) {
				returnValue = mapper.readValue(response.toString(), request.getClazz());
			}
			return (T)returnValue;
		} finally {
			httpConnection.disconnect();
		}
	}
	
	/**
	 * @param serviceUrl
	 * @param clazz
	 * @return
	 * @throws Exception
	 */
	public <T> T doGet(HttpRequest<T> request) throws Exception {
		HttpURLConnection httpConnection = null;
		try {
			URL url = new URL(request.getUrl());
			httpConnection = (HttpURLConnection) url.openConnection();
			httpConnection.setRequestMethod("GET");
			int responseCode = httpConnection.getResponseCode();
			logger.info("Sending GET request to " + request.getUrl() +
						"Response Code : " + responseCode);
			BufferedReader in = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			logger.info("Response: " + response.toString());
			return (T) response;
		} finally {
			httpConnection.disconnect();
		}				
	}
	
	/**
	 * @return ip from header / RemoteAddr. return IP_NOT_FOUND if not found or error.
	 */
	public String getIP(HttpServletRequest request) {
		String ip = IP_NOT_FOUND;
		String name = "";
		try {	
			name = "X-Forwarded-For";
			ip = request.getHeader(name);  
	        if (ip == null || ip.length() == 0) {  
	        	name = "Proxy-Client-IP";
	            ip = request.getHeader(name);  
	        }  
	        if (ip == null || ip.length() == 0) {
	        	name = "WL-Proxy-Client-IP";
	            ip = request.getHeader(name);  
	        }  
	        if (ip == null || ip.length() == 0) {
	        	name = "HTTP_CLIENT_IP";
	            ip = request.getHeader(name);
	        }  
	        if (ip == null || ip.length() == 0) {
	        	name = "HTTP_X_FORWARDED_FOR";
	            ip = request.getHeader(name);  
	        }  
	        if (ip == null || ip.length() == 0) { 
	        	name = "getRemoteAddr()";
	            ip = request.getRemoteAddr();  
	        }  	
			if (!utilService.isArgumentEmptyOrNull(ip)) {
				logger.info("IP from header name: " + name + "; ip: " + ip);
				int indexOfLastComma = ip.indexOf(','); // for proxy users(list of ip's) we need only the first IP in the list
				if (indexOfLastComma != -1) {
					ip = ip.substring(0, indexOfLastComma).trim();
					logger.info("First ip only = " + ip);
				}
			}
		} catch (Exception e) {
			ip = IP_NOT_FOUND;
		}
		logger.info("Final IP for user " + ip);
		return ip;
	}
	
	/**
	 * @return user agent from header.
	 */
	public String getUserAgent(HttpServletRequest request) {
		String userAgent = "";
		try {
			userAgent = request.getHeader("User-Agent");
		} catch (Exception e) {
			userAgent = "";
		}
		return userAgent;
	}
}

