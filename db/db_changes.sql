-- db_changes												|
--  	                                   _ _        _ 	|
--		                                  (_) |      | |	|
--		  __ _ _ __  _   _  ___ __ _ _ __  _| |_ __ _| |	|
--		 / _` | '_ \| | | |/ __/ _` | '_ \| | __/ _` | |	|
--		| (_| | | | | |_| | (_| (_| | |_) | | || (_| | |	|
--		 \__,_|_| |_|\__, |\___\__,_| .__/|_|\__\__,_|_|	|
--		              __/ |         | |                 	|
--		             |___/          |_|						|
-- 															|
-------------------------------------------------------------

-------------------------------------------------------------------------
--------------------------- db version -----------------------------------
-------------------------------------------------------------------------

-- use anycapital;
create schema anycapital;

alter database `anycapital` character set utf8 collate utf8_unicode_ci; 

-- ------------------------------------------------------
--  DDL for Table ACTION_SOURCE
-- ------------------------------------------------------
CREATE TABLE anycapital.ACTION_SOURCE (
    `ID` TINYINT NOT NULL AUTO_INCREMENT,
    `NAME` VARCHAR(30) NOT NULL,
    PRIMARY KEY (ID)
);
-- INSERTING into ACTION_SOURCE
Insert into anycapital.ACTION_SOURCE (NAME) values ('WEB');
Insert into anycapital.ACTION_SOURCE (NAME) values ('CRM');
Insert into anycapital.ACTION_SOURCE (NAME) values ('JOB');
-- ------------------------------------------------------
--  DDL for Table BE_SPACES
-- ------------------------------------------------------
CREATE TABLE anycapital.BE_SPACES (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT,
    `NAME` VARCHAR(20) NOT NULL,
    PRIMARY KEY (ID)
);
-- INSERTING into BE_SPACES
Insert into anycapital.BE_SPACES (NAME) values ('users');
Insert into anycapital.BE_SPACES (NAME) values ('content');
Insert into anycapital.BE_SPACES (NAME) values ('products');
Insert into anycapital.BE_SPACES (NAME) values ('marketing');
-- ------------------------------------------------------
--  DDL for Table BE_PERMISSIONS
-- ------------------------------------------------------
CREATE TABLE anycapital.BE_PERMISSIONS
   (`ID` SMALLINT NOT NULL AUTO_INCREMENT, 
	`NAME` VARCHAR(20) NOT NULL,
    PRIMARY KEY (ID)
   );
-- INSERTING into BE_PERMISSIONS
Insert into anycapital.BE_PERMISSIONS (NAME) values ('view');
Insert into anycapital.BE_PERMISSIONS (NAME) values ('edit');
Insert into anycapital.BE_PERMISSIONS (NAME) values ('add');
-- ------------------------------------------------------
--  DDL for Table LANGUAGES
-- ------------------------------------------------------
CREATE TABLE anycapital.LANGUAGES (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT,
    `CODE` VARCHAR(2) NOT NULL,
    `DISPLAY_NAME` VARCHAR(20) NOT NULL,
    PRIMARY KEY (ID)
);
-- INSERTING into LANGUAGES
Insert into anycapital.LANGUAGES (id,CODE,DISPLAY_NAME) values (2,'en','languages.eng');
Insert into anycapital.LANGUAGES (id,CODE,DISPLAY_NAME) values (8,'de','languages.de');
-- ------------------------------------------------------
--  DDL for Table BE_SCREENS
-- ------------------------------------------------------
CREATE TABLE anycapital.BE_SCREENS
   (`ID` SMALLINT NOT NULL AUTO_INCREMENT,
	`NAME` VARCHAR(20) NOT NULL, 
	`SPACE_ID` SMALLINT NOT NULL,
	FOREIGN KEY (SPACE_ID)
        REFERENCES anycapital.BE_SPACES(id)
        ON DELETE CASCADE,
	PRIMARY KEY (ID)
   );
-- INSERTING into BE_SCREENS
Insert into anycapital.BE_SCREENS (NAME,SPACE_ID) values ('product',3);
Insert into anycapital.BE_SCREENS (NAME,SPACE_ID) values ('translations',2);
Insert into anycapital.BE_SCREENS (NAME,SPACE_ID) values ('user_transactions',1);
Insert into anycapital.BE_SCREENS (NAME,SPACE_ID) values ('user_change_password',1);
Insert into anycapital.BE_SCREENS (NAME,SPACE_ID) values ('marketing_sources',4);
Insert into anycapital.BE_SCREENS (NAME,SPACE_ID) values ('marketing_lp',4);
Insert into anycapital.BE_SCREENS (NAME,SPACE_ID) values ('marketing_contents',4);
Insert into anycapital.BE_SCREENS (NAME,SPACE_ID) values ('marketing_campaigns',4);
Insert into anycapital.BE_SCREENS (NAME,SPACE_ID) values ('user',1);
Insert into anycapital.BE_SCREENS (NAME,SPACE_ID) values ('user_search',1);
Insert into anycapital.BE_SCREENS (NAME,SPACE_ID) values ('user_wire_deposit',1);
Insert into anycapital.BE_SCREENS (NAME,SPACE_ID) values ('user_wire_withdraw',1);
-- ------------------------------------------------------
--  DDL for Table COUNTRIES
-- ------------------------------------------------------
CREATE TABLE anycapital.COUNTRIES (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT,
    `COUNTRY_NAME` VARCHAR(40) NOT NULL,
    `A2` VARCHAR(2) NOT NULL,
    `A3` VARCHAR(3) NOT NULL,
    `PHONE_CODE` SMALLINT NOT NULL,
    `DISPLAY_NAME` VARCHAR(20) NOT NULL,
    `SUPPORT_PHONE` VARCHAR(4000) DEFAULT 0 NOT NULL,
    `SUPPORT_FAX` VARCHAR(4000) DEFAULT 0 NOT NULL,
    `GMT_OFFSET` VARCHAR(9) NOT NULL,
    PRIMARY KEY (ID)
);
-- ------------------------------------------------------
--  DDL for Table DB_PARAMETERS
-- ------------------------------------------------------
CREATE TABLE anycapital.DB_PARAMETERS
   (`ID` SMALLINT NOT NULL AUTO_INCREMENT,
	`NAME` VARCHAR(30) NOT NULL, 
	`NUM_VALUE` DOUBLE, 
	`STRING_VALUE` VARCHAR(100), 
	`DATE_VALUE` DATETIME, 
	`COMMENTS` VARCHAR(100),
    PRIMARY KEY (ID),
	UNIQUE (NAME)
);
-- INSERTING into DB_PARAMETERS
Insert into anycapital.DB_PARAMETERS (NAME,NUM_VALUE,STRING_VALUE,DATE_VALUE,COMMENTS) values ('max_inv',99999900,null,null,'Max investment');
Insert into anycapital.DB_PARAMETERS (NAME,NUM_VALUE,STRING_VALUE,DATE_VALUE,COMMENTS) values ('minimum_withdraw',100,null,null,'minimum withdraw amount');
Insert into anycapital.DB_PARAMETERS (NAME,NUM_VALUE,STRING_VALUE,DATE_VALUE,COMMENTS) values ('withdraw_fee',3000,null,null,'withdraw fee amount');
Insert into anycapital.DB_PARAMETERS (NAME,NUM_VALUE,STRING_VALUE,DATE_VALUE,COMMENTS) values ('user_tokens_time_expired',24,null,null,'time to expired user token. sysdate - this / 24');
Insert into anycapital.DB_PARAMETERS (NAME,NUM_VALUE,STRING_VALUE,DATE_VALUE,COMMENTS) values ('google_short_URL_API',null,'https://www.googleapis.com/urlshortener/v1/url?key=AIzaSyBGV0vc0s64bGCE5gXK0g4CpyqGSvciCNI',null,'google short URL API ');
Insert into anycapital.DB_PARAMETERS (NAME,NUM_VALUE,STRING_VALUE,DATE_VALUE,COMMENTS) values ('min_inv',300000,null,null,'Min investment');
Insert into anycapital.DB_PARAMETERS (NAME,NUM_VALUE,STRING_VALUE,DATE_VALUE,COMMENTS) values ('sendgrid_api_key',null,'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',null,'Key for sendgrid api');
Insert into anycapital.DB_PARAMETERS (NAME,NUM_VALUE,STRING_VALUE,DATE_VALUE,COMMENTS) values ('sendgrid_service_url',null,'https://api.sendgrid.com/v3/',null,'service url of sendgrid');
Insert into anycapital.DB_PARAMETERS (NAME,NUM_VALUE,STRING_VALUE,DATE_VALUE,COMMENTS) values ('key_secure_aes',null,'TheBestKeyEver#1',null,'Key for encrypt and decrypt by AES algorithm');
INSERT INTO `anycapital`.`db_parameters` (`NAME`, `NUM_VALUE`, `COMMENTS`) VALUES ('email_sender_consumers', '5', 'number of consumers in the email sender job');
INSERT INTO `anycapital`.`db_parameters` (`NAME`, `NUM_VALUE`, `COMMENTS`) VALUES ('email_sender_queue_size', '500', 'queue size for email sender job');
INSERT INTO `anycapital`.`db_parameters` (`NAME`, `STRING_VALUE`, `COMMENTS`) VALUES ('support_phone', '+44-2080682960', 'support phone to call');
INSERT INTO `anycapital`.`db_parameters` (`NAME`, `STRING_VALUE`, `COMMENTS`) VALUES ('support_email', 'support@anycapital.com', 'support email');
INSERT INTO `anycapital`.`db_parameters` (`NAME`, `STRING_VALUE`, `COMMENTS`) VALUES ('Company_Address', '24, 28th October Street, 2nd Floor, 2414 Engomi, Nicosia, Cyprus', 'Company Address');

-- ------------------------------------------------------
--  DDL for Table JOBS
-- ------------------------------------------------------
CREATE TABLE anycapital.JOBS (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT,
    `TIME_CREATED` TIMESTAMP(3) DEFAULT NOW(3),
    `RUNNING` BINARY DEFAULT 0 COMMENT 'if the job is currently running',
    `LAST_RUN_TIME` DATETIME COMMENT 'time the last run started',
    `CRON_EXPRESSION` VARCHAR(100) COMMENT 'cron expression execution time',
    `DESCRIPTION` VARCHAR(256),
    `JOB_CLASS` VARCHAR(100),
    `CONFIG` VARCHAR(2000) COMMENT 'Comma separated list of key=value pairs. To set job specific configs.',
    `IS_ACTIVE` BINARY DEFAULT 1,
	PRIMARY KEY (ID)
);
-- INSERTING into JOBS
Insert into anycapital.JOBS (RUNNING,CRON_EXPRESSION,DESCRIPTION,JOB_CLASS,CONFIG,IS_ACTIVE) values (0,'05 0 0 1/1 * *','Products settle job','capital.any.job.ProductsSettlesJob',null,1);
Insert into anycapital.JOBS (RUNNING,CRON_EXPRESSION,DESCRIPTION,JOB_CLASS,CONFIG,IS_ACTIVE) values (0,'0 0,20,40 * * * *','Market job','capital.any.job.MarketJob',null,1);
Insert into anycapital.JOBS (RUNNING,CRON_EXPRESSION,DESCRIPTION,JOB_CLASS,CONFIG,IS_ACTIVE) values (0,'05 0 0 1/1 * *','Products Statuses Job','capital.any.job.ProductsStatusesJob',null,1);
Insert into anycapital.JOBS (RUNNING,CRON_EXPRESSION,DESCRIPTION,JOB_CLASS,CONFIG,IS_ACTIVE) values (0,'0 0 * * * *','Sendgrid Job','capital.any.job.SendgridJob',null,1);
Insert into anycapital.JOBS (RUNNING,CRON_EXPRESSION,DESCRIPTION,JOB_CLASS,CONFIG,IS_ACTIVE) values (0,'05 0 0 1/1 * *','Products Coupon job','capital.any.job.PayProductCouponsJob',null,1);
Insert into anycapital.JOBS (RUNNING,CRON_EXPRESSION,DESCRIPTION,JOB_CLASS,CONFIG,IS_ACTIVE) values (0,'0/30 * 0 ? * *','SMS job','capital.any.job.SMSJob',null,1);
Insert into anycapital.JOBS (RUNNING,CRON_EXPRESSION,DESCRIPTION,JOB_CLASS,CONFIG,IS_ACTIVE) values (0,'0 0 1 1/1 * *','Markets history daily Job','capital.any.job.MarketDailyHistoryJob',null,1);
Insert into anycapital.JOBS (RUNNING,CRON_EXPRESSION,DESCRIPTION,JOB_CLASS,CONFIG,IS_ACTIVE) values (0,'* * * * * *','send emails job','capital.any.job.EmailSenderJob',null,1);
Insert into anycapital.JOBS (RUNNING,CRON_EXPRESSION,DESCRIPTION,JOB_CLASS,CONFIG,IS_ACTIVE) values (0,'0 0 2 1/1 * *','Daily report Job','capital.any.job.DailyReportJob',null,1);
-- ------------------------------------------------------
--  DDL for Table MARKET_GROUPS
-- ------------------------------------------------------ 
 CREATE TABLE anycapital.MARKET_GROUPS (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT,
    `NAME` VARCHAR(20) NOT NULL,
    `DISPLAY_NAME` VARCHAR(20),
    PRIMARY KEY (ID)
);
-- INSERTING into MARKET_GROUPS
Insert into anycapital.MARKET_GROUPS (NAME,DISPLAY_NAME) values ('Indices','market.group.1');
Insert into anycapital.MARKET_GROUPS (NAME,DISPLAY_NAME) values ('Shares','market.group.2');
Insert into anycapital.MARKET_GROUPS (NAME,DISPLAY_NAME) values ('Commodities','market.group.3');
Insert into anycapital.MARKET_GROUPS (NAME,DISPLAY_NAME) values ('Rates','market.group.4');
Insert into anycapital.MARKET_GROUPS (NAME,DISPLAY_NAME) values ('Currencies','market.group.5');
-- ------------------------------------------------------
--  DDL for Table MARKETS
-- ------------------------------------------------------ 
CREATE TABLE anycapital.MARKETS (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT,
    `NAME` VARCHAR(40) NOT NULL,
    `DISPLAY_NAME` VARCHAR(20) NOT NULL,
    `MARKET_GROUP_ID` SMALLINT NOT NULL,
    `TICKER` VARCHAR(30) NOT NULL,
    `CURRENCY` VARCHAR(20) COMMENT 'market exchange currency' NOT NULL,
    `FEED_NAME` VARCHAR(20) NOT NULL,
    `IS_ACTIVE` TINYINT DEFAULT 1,
    `DECIMAL_POINT` SMALLINT DEFAULT 2,
    FOREIGN KEY (MARKET_GROUP_ID)
        REFERENCES anycapital.MARKET_GROUPS (id)
        ON DELETE CASCADE,
    PRIMARY KEY (ID)
); 

ALTER TABLE `anycapital`.`markets` 
ADD COLUMN `MARKET_ID_AO` SMALLINT(6) NULL COMMENT 'the market id in ao db' AFTER `DECIMAL_POINT`;


-- ------------------------------------------------------
--  DDL for Table USER_CLASSES
-- ------------------------------------------------------ 
CREATE TABLE anycapital.USER_CLASSES (
    `ID` TINYINT NOT NULL AUTO_INCREMENT,
    `NAME` VARCHAR(30) NOT NULL,
    PRIMARY KEY (ID)
);
-- INSERTING into USER_CLASSES
Insert into anycapital.USER_CLASSES (NAME) values ('user.class.test');
Insert into anycapital.USER_CLASSES (NAME) values ('user.class.private');
Insert into anycapital.USER_CLASSES (NAME) values ('user.class.company');
-- ------------------------------------------------------
--  DDL for Table URLS
-- ------------------------------------------------------ 
CREATE TABLE anycapital.URLS (
    `ID` TINYINT NOT NULL AUTO_INCREMENT,
    `URL` VARCHAR(200) NOT NULL,
    `URL_NAME` VARCHAR(50) NOT NULL,
    PRIMARY KEY (ID)
);
-- INSERTING into URLS
Insert into anycapital.URLS (URL,URL_NAME) values ('www.testenv.anycapital.com','anycapital');
-- ------------------------------------------------------
--  DDL for Table TABLES
-- ------------------------------------------------------ 
CREATE TABLE anycapital.TABLES (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT COMMENT 'Table\'s id',
    `NAME` VARCHAR(100) COMMENT 'Table\'s name',
    PRIMARY KEY (ID)
);
-- INSERTING into TABLES
Insert into anycapital.TABLES (NAME) values ('INVESTMENTS');
Insert into anycapital.TABLES (NAME) values ('TRANSACTIONS');
Insert into anycapital.TABLES (NAME) values ('USERS');
Insert into anycapital.TABLES (NAME) values ('SMS');
-- ------------------------------------------------------
--  DDL for Table CURRENCIES
-- ------------------------------------------------------ 
CREATE TABLE anycapital.CURRENCIES (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT,
    `CODE` VARCHAR(3) NOT NULL COMMENT 'A common 3 characters name code for that currency',
    `DISPLAY_NAME` VARCHAR(20) NOT NULL,
    `SYMBOL` VARCHAR(20) NOT NULL COMMENT 'The symbol of the currency',
    `MARKET_ID` SMALLINT DEFAULT 0 COMMENT 'The market id to take the rate from. 0 if no need to take rate',
    PRIMARY KEY (ID)
);
-- INSERTING into CURRENCIES
Insert into anycapital.CURRENCIES (id,CODE,DISPLAY_NAME,SYMBOL,MARKET_ID) values (3,'EUR','currencies.eur','€',0);
Insert into anycapital.CURRENCIES (id,CODE,DISPLAY_NAME,SYMBOL,MARKET_ID) values (2,'USD','currencies.usd','$',48);
-- ------------------------------------------------------
--  DDL for Table WRITER_GROUPS
-- ------------------------------------------------------
CREATE TABLE anycapital.WRITER_GROUPS (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT,
    `NAME` VARCHAR(30) NOT NULL,
    PRIMARY KEY (ID)
);
-- Insert into WRITER_GROUPS
Insert into anycapital.WRITER_GROUPS (NAME) values ('admin');
Insert into anycapital.WRITER_GROUPS (NAME) values ('support');
-- ------------------------------------------------------
--  DDL for Table WRITERS
-- ------------------------------------------------------ 
CREATE TABLE anycapital.WRITERS (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT,
    `time_created` TIMESTAMP(3) DEFAULT NOW(3),
    `USER_NAME` VARCHAR(40) NOT NULL,
    `PASSWORD` VARCHAR(15) NOT NULL,
    `FIRST_NAME` NVARCHAR(20) NOT NULL,
    `LAST_NAME` NVARCHAR(20) NOT NULL,
    `EMAIL` NVARCHAR(30) NOT NULL,
    `COMMENTS` NVARCHAR(100),
    `IS_ACTIVE` BINARY DEFAULT 1,
    `UTC_OFFSET` SMALLINT(6) NOT NULL DEFAULT -180,
    `GROUP_ID` SMALLINT DEFAULT 1,
    FOREIGN KEY (GROUP_ID)
        REFERENCES anycapital.WRITER_GROUPS (id),    
    UNIQUE (`EMAIL`),
    UNIQUE (`USER_NAME`),
    PRIMARY KEY (ID)
);

Insert into anycapital.WRITERS (ID,TIME_CREATED,USER_NAME,PASSWORD,FIRST_NAME,LAST_NAME,EMAIL,COMMENTS,IS_ACTIVE,GROUP_ID) values (4,str_to_date('01/08/2016 12:53.06','%d/%m/%Y %H:%i.%s'),'lior','1','LioR','SoLoMoN','liors@etrader.co.il',null,1,1);
Insert into anycapital.WRITERS (ID,TIME_CREATED,USER_NAME,PASSWORD,FIRST_NAME,LAST_NAME,EMAIL,COMMENTS,IS_ACTIVE,GROUP_ID) values (5,str_to_date('01/08/2016 12:53.06','%d/%m/%Y %H:%i.%s'),'eyalg','123456','Eyal','Goren','eyal.goren@anyoption.com',null,1,1);
Insert into anycapital.WRITERS (ID,TIME_CREATED,USER_NAME,PASSWORD,FIRST_NAME,LAST_NAME,EMAIL,COMMENTS,IS_ACTIVE,GROUP_ID) values (6,str_to_date('01/08/2016 12:53.06','%d/%m/%Y %H:%i.%s'),'eyalo','123456','Eyal','Ohana','eyal.ohana@anyoption.com',null,1,1);
Insert into anycapital.WRITERS (ID,TIME_CREATED,USER_NAME,PASSWORD,FIRST_NAME,LAST_NAME,EMAIL,COMMENTS,IS_ACTIVE,GROUP_ID) values (9,str_to_date('11/08/2016 12:55.47','%d/%m/%Y %H:%i.%s'),'support','123456','support','support','support@any.capital',null,1,2);
Insert into anycapital.WRITERS (ID,TIME_CREATED,USER_NAME,PASSWORD,FIRST_NAME,LAST_NAME,EMAIL,COMMENTS,IS_ACTIVE,GROUP_ID) values (-1,str_to_date('01/01/2017 12:33.54','%d/%m/%Y %H:%i.%s'),'junit','123456','j','unit','junit@anyoption.com',null,1,1);
Insert into anycapital.WRITERS (ID,TIME_CREATED,USER_NAME,PASSWORD,FIRST_NAME,LAST_NAME,EMAIL,COMMENTS,IS_ACTIVE,GROUP_ID) values (41,str_to_date('02/01/2017 08:27.47','%d/%m/%Y %H:%i.%s'),'alexander','123456','alexander','Z','alexander.z@anyoption.com',null,1,1);
Insert into anycapital.WRITERS (ID,TIME_CREATED,USER_NAME,PASSWORD,FIRST_NAME,LAST_NAME,EMAIL,COMMENTS,IS_ACTIVE,GROUP_ID) values (8,str_to_date('11/08/2016 12:55.47','%d/%m/%Y %H:%i.%s'),'petar','123456','Petar','Tchorbadjiyski','petart@anyoption.com',null,1,1);
Insert into anycapital.WRITERS (ID,TIME_CREATED,USER_NAME,PASSWORD,FIRST_NAME,LAST_NAME,EMAIL,COMMENTS,IS_ACTIVE,GROUP_ID) values (2,str_to_date('11/08/2016 12:55.47','%d/%m/%Y %H:%i.%s'),'miki','123456','Miki','F','miki@anyoption.com',null,1,1);
Insert into anycapital.WRITERS (ID,TIME_CREATED,USER_NAME,PASSWORD,FIRST_NAME,LAST_NAME,EMAIL,COMMENTS,IS_ACTIVE,GROUP_ID) values (21,str_to_date('26/10/2016 13:09.47','%d/%m/%Y %H:%i.%s'),'mira','123456','mira','Kofman','mira.kofman@anyoption.com',null,1,1);
Insert into anycapital.WRITERS (ID,TIME_CREATED,USER_NAME,PASSWORD,FIRST_NAME,LAST_NAME,EMAIL,COMMENTS,IS_ACTIVE,GROUP_ID) values (3,str_to_date('22/07/2016 11:41.41','%d/%m/%Y %H:%i.%s'),'eran','123456','Eran','Levy','eran.levy@anyoption.com',null,1,1);
Insert into anycapital.WRITERS (ID,TIME_CREATED,USER_NAME,PASSWORD,FIRST_NAME,LAST_NAME,EMAIL,COMMENTS,IS_ACTIVE,GROUP_ID) values (22,now(),'kosta','123456','Kosta','Romanov','kosta.romanov@anyoption.com',null,1,1);
-- ------------------------------------------------------
--  DDL for Table USERS
-- ------------------------------------------------------
CREATE TABLE anycapital.USERS (
    `ID` BIGINT NOT NULL AUTO_INCREMENT,
    `BALANCE` BIGINT NOT NULL DEFAULT 0,
    `PASSWORD` VARCHAR(110) NOT NULL,
    `CURRENCY_ID` SMALLINT NOT NULL,
    `FIRST_NAME` NVARCHAR(30) NOT NULL,
    `LAST_NAME` NVARCHAR(30) NOT NULL,
    `STREET` NVARCHAR(50),
    `ZIP_CODE` VARCHAR(10),
    `TIME_CREATED` TIMESTAMP(3) DEFAULT NOW(3) NOT NULL,
    `TIME_LAST_LOGIN` DATETIME,
    `IS_ACTIVE` BINARY DEFAULT 1,
    `EMAIL` NVARCHAR(50),
    `COMMENTS` NVARCHAR(100),
    `IP` VARCHAR(30),
    `TIME_BIRTH_DATE` DATETIME,
    `IS_CONTACT_BY_EMAIL` BINARY DEFAULT 1,
    `IS_CONTACT_BY_SMS` BINARY DEFAULT 1,
    `IS_CONTACT_BY_PHONE` BINARY DEFAULT 1,
    `IS_ACCEPTED_TERMS` BINARY DEFAULT 1,
    `MOBILE_PHONE` VARCHAR(30),
    `LAND_LINE_PHONE` VARCHAR(30),
    `GENDER` VARCHAR(1),
    `TIME_MODIFIED` TIMESTAMP NULL,
    `CLASS_ID` TINYINT DEFAULT 1,
    `STREET_NO` NVARCHAR(6),
    `LANGUAGE_ID` SMALLINT DEFAULT 1,
    `UTC_OFFSET` SMALLINT  DEFAULT 0,
    `COUNTRY_ID` SMALLINT,
    `CITY_NAME` VARCHAR(120),
    `UTC_OFFSET_CREATED` VARCHAR(9) DEFAULT 'GMT+02:00',
    `WRITER_ID` SMALLINT,
    `AO_USER_ID` BIGINT,
    `ACTION_SOURCE_ID` TINYINT NOT NULL,
    `USER_AGENT` VARCHAR(2000),
    `HTTP_REFERER` VARCHAR(4000),
    `IS_REGULATED` BINARY DEFAULT 0,
    `VIP_STATUS_ID` TINYINT,
    FOREIGN KEY (CURRENCY_ID)
        REFERENCES anycapital.CURRENCIES (id)
        ON DELETE CASCADE,
    FOREIGN KEY (COUNTRY_ID)
        REFERENCES anycapital.COUNTRIES (id)
        ON DELETE CASCADE,
    FOREIGN KEY (WRITER_ID)
        REFERENCES anycapital.WRITERS (id)
        ON DELETE CASCADE,
    FOREIGN KEY (ACTION_SOURCE_ID)
        REFERENCES anycapital.ACTION_SOURCE (id)
        ON DELETE CASCADE,
    FOREIGN KEY (LANGUAGE_ID)
        REFERENCES anycapital.LANGUAGES (id)
        ON DELETE CASCADE,
	FOREIGN KEY (CLASS_ID)
        REFERENCES anycapital.USER_CLASSES (id)
        ON DELETE CASCADE,
    UNIQUE (`EMAIL`),
    PRIMARY KEY (ID)
);
-- ------------------------------------------------------
--  DDL for Table USER_TOKENS
-- ------------------------------------------------------
CREATE TABLE anycapital.USER_TOKENS (
    `USER_ID` BIGINT NOT NULL,
    `TOKEN` VARCHAR(256) NOT NULL,
    `TIME_CREATED` TIMESTAMP(3) DEFAULT NOW(3) NOT NULL,
    FOREIGN KEY (USER_ID)
        REFERENCES anycapital.USERS (id)
        ON DELETE CASCADE
);
ALTER TABLE anycapital.USER_TOKENS COMMENT 'user tokens. first stage - using for change password. second stage - may add types to use also other user token types.';
-- ------------------------------------------------------
--  DDL for Table WRITER_GROUP_PERMISSIONS
-- ------------------------------------------------------
CREATE TABLE anycapital.WRITER_GROUP_PERMISSIONS (
    `GROUP_ID` SMALLINT NOT NULL,
    `BE_SCREEN_ID` SMALLINT NOT NULL,
    `BE_PERMISSION_ID` SMALLINT NOT NULL,
    `IS_ACTIVE` BINARY DEFAULT 1,
    FOREIGN KEY (GROUP_ID)
        REFERENCES anycapital.WRITER_GROUPS (id)
        ON DELETE CASCADE,
    FOREIGN KEY (BE_SCREEN_ID)
        REFERENCES anycapital.BE_SCREENS (id)
        ON DELETE CASCADE,
    FOREIGN KEY (BE_PERMISSION_ID)
        REFERENCES anycapital.BE_PERMISSIONS (id)
        ON DELETE CASCADE
);
-- INSERTING into WRITER_GROUP_PERMISSIONS
Insert into anycapital.WRITER_GROUP_PERMISSIONS (GROUP_ID,BE_SCREEN_ID,BE_PERMISSION_ID,IS_ACTIVE) values (1,4,1,1);
Insert into anycapital.WRITER_GROUP_PERMISSIONS (GROUP_ID,BE_SCREEN_ID,BE_PERMISSION_ID,IS_ACTIVE) values (1,1,1,1);
Insert into anycapital.WRITER_GROUP_PERMISSIONS (GROUP_ID,BE_SCREEN_ID,BE_PERMISSION_ID,IS_ACTIVE) values (1,1,2,1);
Insert into anycapital.WRITER_GROUP_PERMISSIONS (GROUP_ID,BE_SCREEN_ID,BE_PERMISSION_ID,IS_ACTIVE) values (1,1,3,1);
Insert into anycapital.WRITER_GROUP_PERMISSIONS (GROUP_ID,BE_SCREEN_ID,BE_PERMISSION_ID,IS_ACTIVE) values (1,3,1,1);
Insert into anycapital.WRITER_GROUP_PERMISSIONS (GROUP_ID,BE_SCREEN_ID,BE_PERMISSION_ID,IS_ACTIVE) values (1,3,2,1);
Insert into anycapital.WRITER_GROUP_PERMISSIONS (GROUP_ID,BE_SCREEN_ID,BE_PERMISSION_ID,IS_ACTIVE) values (1,3,3,1);
Insert into anycapital.WRITER_GROUP_PERMISSIONS (GROUP_ID,BE_SCREEN_ID,BE_PERMISSION_ID,IS_ACTIVE) values (1,9,1,1);
Insert into anycapital.WRITER_GROUP_PERMISSIONS (GROUP_ID,BE_SCREEN_ID,BE_PERMISSION_ID,IS_ACTIVE) values (1,10,1,1);
Insert into anycapital.WRITER_GROUP_PERMISSIONS (GROUP_ID,BE_SCREEN_ID,BE_PERMISSION_ID,IS_ACTIVE) values (1,11,1,1);
Insert into anycapital.WRITER_GROUP_PERMISSIONS (GROUP_ID,BE_SCREEN_ID,BE_PERMISSION_ID,IS_ACTIVE) values (1,12,1,1);
Insert into anycapital.WRITER_GROUP_PERMISSIONS (GROUP_ID,BE_SCREEN_ID,BE_PERMISSION_ID,IS_ACTIVE) values (1,2,1,1);
Insert into anycapital.WRITER_GROUP_PERMISSIONS (GROUP_ID,BE_SCREEN_ID,BE_PERMISSION_ID,IS_ACTIVE) values (1,2,2,1);
Insert into anycapital.WRITER_GROUP_PERMISSIONS (GROUP_ID,BE_SCREEN_ID,BE_PERMISSION_ID,IS_ACTIVE) values (1,2,3,1);
Insert into anycapital.WRITER_GROUP_PERMISSIONS (GROUP_ID,BE_SCREEN_ID,BE_PERMISSION_ID,IS_ACTIVE) values (2,1,1,1);
Insert into anycapital.WRITER_GROUP_PERMISSIONS (GROUP_ID,BE_SCREEN_ID,BE_PERMISSION_ID,IS_ACTIVE) values (1,5,1,1);
Insert into anycapital.WRITER_GROUP_PERMISSIONS (GROUP_ID,BE_SCREEN_ID,BE_PERMISSION_ID,IS_ACTIVE) values (1,6,1,1);
Insert into anycapital.WRITER_GROUP_PERMISSIONS (GROUP_ID,BE_SCREEN_ID,BE_PERMISSION_ID,IS_ACTIVE) values (1,7,1,1);
Insert into anycapital.WRITER_GROUP_PERMISSIONS (GROUP_ID,BE_SCREEN_ID,BE_PERMISSION_ID,IS_ACTIVE) values (1,8,1,1);
-- ------------------------------------------------------
--  DDL for Table TRANSACTION_CLASSES
-- ------------------------------------------------------
CREATE TABLE anycapital.TRANSACTION_CLASSES (
    `ID` TINYINT NOT NULL AUTO_INCREMENT,
    `NAME` VARCHAR(20) NOT NULL,
    `DISPLAY_NAME` VARCHAR(30) NOT NULL,
    PRIMARY KEY (ID)
);
-- INSERTING into TRANSACTION_CLASSES
Insert into anycapital.TRANSACTION_CLASSES (NAME,DISPLAY_NAME) values ('Real','transaction.class.1');
Insert into anycapital.TRANSACTION_CLASSES (NAME,DISPLAY_NAME) values ('Not Real','transaction.class.2');
-- ------------------------------------------------------
--  DDL for Table TRANSACTION_OPERATIONS
-- ------------------------------------------------------
CREATE TABLE anycapital.TRANSACTION_OPERATIONS (
    `ID` TINYINT NOT NULL AUTO_INCREMENT,
    `NAME` VARCHAR(10) NOT NULL,
    `DISPLAY_NAME` VARCHAR(30) NOT NULL,
    PRIMARY KEY (ID)
);
-- INSERTING into anycapital.TRANSACTION_OPERATIONS
Insert into anycapital.TRANSACTION_OPERATIONS (NAME,DISPLAY_NAME) values ('Credit','transaction.operation.1');
Insert into anycapital.TRANSACTION_OPERATIONS (NAME,DISPLAY_NAME) values ('Debit','transaction.operation.2');
-- ------------------------------------------------------
--  DDL for Table TRANSACTION_PAYMENT_TYPES
-- ------------------------------------------------------
CREATE TABLE anycapital.TRANSACTION_PAYMENT_TYPES (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT,
    `NAME` VARCHAR(20) NOT NULL COMMENT 'name of the payment type',
    `TRANSACTION_CLASS_ID` TINYINT NOT NULL COMMENT 'reference',
    `DISPLAY_NAME` VARCHAR(30),
    `OBJECT_CLASS` VARCHAR(50) DEFAULT 'java.lang.Object' COMMENT 'blueprint class path - represent the payment class type',
    `HANDLER_CLASS` VARCHAR(100) DEFAULT 'java.lang.Object' COMMENT 'class path of the class who responsible to handle the payment type',
    FOREIGN KEY (TRANSACTION_CLASS_ID)
        REFERENCES anycapital.TRANSACTION_CLASSES (id)
        ON DELETE CASCADE,
    PRIMARY KEY (ID)
); 
-- INSERTING into TRANSACTION_PAYMENT_TYPES
Insert into anycapital.TRANSACTION_PAYMENT_TYPES (NAME,TRANSACTION_CLASS_ID,DISPLAY_NAME,OBJECT_CLASS,HANDLER_CLASS) values ('Bank Wire',1,'transaction.payment.type.1','capital.any.model.base.Wire','capital.any.service.base.payment.handler.WirePaymentHandler');
Insert into anycapital.TRANSACTION_PAYMENT_TYPES (NAME,TRANSACTION_CLASS_ID,DISPLAY_NAME,OBJECT_CLASS,HANDLER_CLASS) values ('investment coupon',2,'transaction.payment.type.2','java.lang.Object','java.lang.Object');

Insert into anycapital.TRANSACTION_PAYMENT_TYPES 
(NAME,TRANSACTION_CLASS_ID,DISPLAY_NAME,OBJECT_CLASS,HANDLER_CLASS) 
values 
('Admin',2,'transaction.payment.type.3','java.lang.Object','capital.any.service.base.payment.handler.AdminPaymentHandler');
-- ------------------------------------------------------
--  DDL for Table TRANSACTION_STATUSES
-- ------------------------------------------------------
CREATE TABLE anycapital.TRANSACTION_STATUSES (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT,
    `NAME` VARCHAR(40) NOT NULL,
    `DISPLAY_NAME` VARCHAR(30) NOT NULL,
    PRIMARY KEY (ID)
);
-- INSERTING into TRANSACTION_STATUSES
Insert into anycapital.TRANSACTION_STATUSES (ID,NAME,DISPLAY_NAME) values (1,'Started','transaction.status.1');
Insert into anycapital.TRANSACTION_STATUSES (ID,NAME,DISPLAY_NAME) values (2,'Succeed','transaction.status.2');
Insert into anycapital.TRANSACTION_STATUSES (ID,NAME,DISPLAY_NAME) values (3,'Failed','transaction.status.3');
Insert into anycapital.TRANSACTION_STATUSES (ID,NAME,DISPLAY_NAME) values (4,'Pending','transaction.status.4');
Insert into anycapital.TRANSACTION_STATUSES (ID,NAME,DISPLAY_NAME) values (12,'Cancel','transaction.status.12');
Insert into anycapital.TRANSACTION_STATUSES (ID,NAME,DISPLAY_NAME) values (10,'Waiting for First Approve','transaction.status.10');
Insert into anycapital.TRANSACTION_STATUSES (ID,NAME,DISPLAY_NAME) values (11,'Waiting for Second Approve','transaction.status.11');
-- ------------------------------------------------------
--  DDL for Table TRANSACTIONS
-- ------------------------------------------------------
CREATE TABLE anycapital.TRANSACTIONS (
    `ID` BIGINT NOT NULL AUTO_INCREMENT,
    `USER_ID` BIGINT NOT NULL,
    `AMOUNT` INT NOT NULL,
    `RATE` FLOAT NOT NULL,
    `IP` VARCHAR(30),
    `COMMENTS` VARCHAR(400),
    `TRANSACTION_OPERATION_ID` TINYINT NOT NULL,
    `TRANSACTION_PAYMENT_TYPE_ID` SMALLINT NOT NULL,
    `TRANSACTION_STATUS_ID` SMALLINT NOT NULL,
    `TIME_CREATED` TIMESTAMP(3) DEFAULT NOW(3) NOT NULL,
    `TIME_SETTLED` TIMESTAMP(3) NULL,
    FOREIGN KEY (USER_ID)
        REFERENCES anycapital.USERS (id)
        ON DELETE CASCADE,
    FOREIGN KEY (TRANSACTION_OPERATION_ID)
        REFERENCES anycapital.TRANSACTION_OPERATIONS (id)
        ON DELETE CASCADE,
    FOREIGN KEY (TRANSACTION_PAYMENT_TYPE_ID)
        REFERENCES anycapital.TRANSACTION_PAYMENT_TYPES (id)
        ON DELETE CASCADE,
    FOREIGN KEY (TRANSACTION_STATUS_ID)
        REFERENCES anycapital.TRANSACTION_STATUSES (id)
        ON DELETE CASCADE,
    PRIMARY KEY (ID)
);
-- ------------------------------------------------------
--  DDL for Table WIRES
-- ------------------------------------------------------
CREATE TABLE anycapital.WIRES (
    `ID` INT NOT NULL AUTO_INCREMENT,
    `TIME_CREATED` TIMESTAMP(3) DEFAULT NOW(3) NOT NULL,
    `TRANSACTION_ID` BIGINT NOT NULL,
    `BRANCH_ADDRESS` VARCHAR(200),
    `BANK_NAME` VARCHAR(160),
    `IBAN` VARCHAR(160),
    `SWIFT` VARCHAR(160),
    `ACCOUNT_INFO` VARCHAR(500),
    `ACCOUNT_NUM` DOUBLE,
    `BENEFICIARY_NAME` VARCHAR(160),
    `BANK_FEE_AMOUNT` DOUBLE,
    FOREIGN KEY (TRANSACTION_ID)
        REFERENCES anycapital.TRANSACTIONS (id)
        ON DELETE CASCADE,
    PRIMARY KEY (ID)
);
-- ------------------------------------------------------
--  DDL for Table TRANSACTIONS_FEE
-- ------------------------------------------------------
CREATE TABLE anycapital.TRANSACTIONS_FEE (
    `TRANSACTION_ID` BIGINT NOT NULL COMMENT 'reference',
    `AMOUNT` INT,
    `TIME_CREATED` TIMESTAMP(3) DEFAULT NOW(3) NOT NULL,
    FOREIGN KEY (TRANSACTION_ID)
        REFERENCES anycapital.TRANSACTIONS (id)
        ON DELETE CASCADE
); 
ALTER TABLE anycapital.TRANSACTIONS_FEE COMMENT 'One fee per transaction';
-- ------------------------------------------------------
--  DDL for Table TRANSACTION_REFERENCES
-- ------------------------------------------------------
CREATE TABLE anycapital.TRANSACTION_REFERENCES (
	`TRANSACTION_ID` BIGINT NOT NULL,
    `REFERENCE_ID` BIGINT NOT NULL COMMENT 'REFERENCE to other table id according to table_id',
    `TABLE_ID` SMALLINT NOT NULL COMMENT 'table id of the REFERENCE',
    FOREIGN KEY (TRANSACTION_ID)
        REFERENCES anycapital.TRANSACTIONS (id)
        ON DELETE CASCADE,
    FOREIGN KEY (TABLE_ID)
        REFERENCES anycapital.TABLES (id)
        ON DELETE CASCADE,
	UNIQUE (`TRANSACTION_ID`, `REFERENCE_ID`)
);
-- ------------------------------------------------------
--  DDL for Table CONTACT_ISSUES
-- ------------------------------------------------------
CREATE TABLE anycapital.CONTACT_ISSUES (
    `ID` TINYINT NOT NULL AUTO_INCREMENT,
    `NAME` NVARCHAR(30) NOT NULL,
    `DISPLAY_NAME` NVARCHAR(50) NOT NULL,
    PRIMARY KEY (ID)
);
-- INSERTING into CONTACT_ISSUES
Insert into anycapital.CONTACT_ISSUES (ID,NAME,DISPLAY_NAME) values (1,'General','contact.issues1');
Insert into anycapital.CONTACT_ISSUES (ID,NAME,DISPLAY_NAME) values (2,'Investments','contact.issues2');
Insert into anycapital.CONTACT_ISSUES (ID,NAME,DISPLAY_NAME) values (3,'Technical issues','contact.issues3');
Insert into anycapital.CONTACT_ISSUES (ID,NAME,DISPLAY_NAME) values (4,'Registration','contact.issues4');
Insert into anycapital.CONTACT_ISSUES (ID,NAME,DISPLAY_NAME) values (5,'Deposits/Withdrawals','contact.issues5');
-- ------------------------------------------------------
--  DDL for Table CONTACT_TYPES
-- ------------------------------------------------------
  CREATE TABLE anycapital.CONTACT_TYPES
   (`ID` TINYINT NOT NULL AUTO_INCREMENT, 
	`NAME` NVARCHAR(30) NOT NULL,
 PRIMARY KEY (ID)
   );
-- INSERTING into CONTACT_TYPES
Insert into anycapital.CONTACT_TYPES (ID,NAME) values (1,'Call Me');
Insert into anycapital.CONTACT_TYPES (ID,NAME) values (2,'Contact Us');
-- ------------------------------------------------------
--  DDL for Table CONTACTS
-- ------------------------------------------------------
  CREATE TABLE anycapital.CONTACTS (
    `ID` BIGINT NOT NULL AUTO_INCREMENT,
    `FIRST_NAME` VARCHAR(100),
    `LAST_NAME` VARCHAR(100),
    `TYPE_ID` TINYINT NOT NULL,
    `MOBILE_PHONE` VARCHAR(30),
    `EMAIL` VARCHAR(50),
    `COUNTRY_ID` SMALLINT NOT NULL,
    `LANGUAGE_ID` SMALLINT NOT NULL,
    `TIME_CREATED` TIMESTAMP(3) DEFAULT NOW(3) NOT NULL,
    `TIME_MODIFIED` TIMESTAMP NULL,
    `WRITER_ID` SMALLINT,
    `UTC_OFFSET` SMALLINT,
    `IP` VARCHAR(20),
    `USER_AGENT` VARCHAR(2000),
    `IS_CONTACT_BY_EMAIL` BINARY DEFAULT 1,
    `IS_CONTACT_BY_SMS` BINARY DEFAULT 1,
    `CLASS_ID` TINYINT DEFAULT 1,
    `ACTION_SOURCE_ID` TINYINT NOT NULL,
    `HTTP_REFERER` VARCHAR(4000),
    `ISSUE_ID` TINYINT,
    `COMMENTS` VARCHAR(4000),
    FOREIGN KEY (ISSUE_ID)
        REFERENCES anycapital.CONTACT_ISSUES (id)
        ON DELETE CASCADE,
    FOREIGN KEY (COUNTRY_ID)
        REFERENCES anycapital.COUNTRIES (id)
        ON DELETE CASCADE,
    FOREIGN KEY (TYPE_ID)
        REFERENCES anycapital.CONTACT_TYPES (id)
        ON DELETE CASCADE,
    FOREIGN KEY (CLASS_ID)
        REFERENCES anycapital.USER_CLASSES (id)
        ON DELETE CASCADE,
    FOREIGN KEY (WRITER_ID)
        REFERENCES anycapital.WRITERS (id)
        ON DELETE CASCADE,
    FOREIGN KEY (ACTION_SOURCE_ID)
        REFERENCES anycapital.ACTION_SOURCE (id)
        ON DELETE CASCADE,        
    PRIMARY KEY (ID)
); 
-- ------------------------------------------------------
--  DDL for Table BALANCE_HISTORY_TYPES
-- ------------------------------------------------------
  CREATE TABLE anycapital.BALANCE_HISTORY_TYPES
   (	 `ID` TINYINT NOT NULL AUTO_INCREMENT,
		`TABLE_NAME` VARCHAR(20) NOT NULL,
		PRIMARY KEY (ID)
   );
-- INSERTING into BALANCE_HISTORY_TYPES
Insert into anycapital.BALANCE_HISTORY_TYPES (ID,TABLE_NAME) values (1,'INVESTMENTS');
Insert into anycapital.BALANCE_HISTORY_TYPES (ID,TABLE_NAME) values (2,'TRANSACTIONS');
-- ------------------------------------------------------
--  DDL for Table BALANCE_HISTORY_COMMANDS
-- ------------------------------------------------------
  CREATE TABLE anycapital.BALANCE_HISTORY_COMMANDS
   (`ID` SMALLINT NOT NULL AUTO_INCREMENT,
	`NAME` VARCHAR(30) NOT NULL COMMENT 'command name', 
	`BALANCE_HISTORY_TYPE_ID` TINYINT NOT NULL,
    FOREIGN KEY (BALANCE_HISTORY_TYPE_ID)
        REFERENCES anycapital.BALANCE_HISTORY_TYPES (id)
        ON DELETE CASCADE,
	PRIMARY KEY (ID)
   );
-- INSERTING into anycapital.BALANCE_HISTORY_COMMANDS
Insert into anycapital.BALANCE_HISTORY_COMMANDS (ID,NAME,BALANCE_HISTORY_TYPE_ID) values (6,'CANCEL TRANSACTION',2);
Insert into anycapital.BALANCE_HISTORY_COMMANDS (ID,NAME,BALANCE_HISTORY_TYPE_ID) values (1,'INSERT INVESTMENTS',1);
Insert into anycapital.BALANCE_HISTORY_COMMANDS (ID,NAME,BALANCE_HISTORY_TYPE_ID) values (2,'SETTLE INVESTMENT',1);
Insert into anycapital.BALANCE_HISTORY_COMMANDS (ID,NAME,BALANCE_HISTORY_TYPE_ID) values (3,'CANCEL INVESTMENT',1);
Insert into anycapital.BALANCE_HISTORY_COMMANDS (ID,NAME,BALANCE_HISTORY_TYPE_ID) values (4,'RESETTLE INVESTMENT',1);
Insert into anycapital.BALANCE_HISTORY_COMMANDS (ID,NAME,BALANCE_HISTORY_TYPE_ID) values (5,'SUCCESS TRANSACTION',2);
-- ------------------------------------------------------
--  DDL for Table BALANCE_HISTORY
-- ------------------------------------------------------
CREATE TABLE anycapital.BALANCE_HISTORY (
    `USER_ID` BIGINT NOT NULL COMMENT 'see constraints',
    `REFERENCE_ID` BIGINT NOT NULL COMMENT 'reference unique identifier to a record which change the user balance',
    `BALANCE` INT NOT NULL COMMENT 'User balance on the time this record inserted. (new balance)',
    `BALANCE_HISTORY_COMMAND_ID` SMALLINT NOT NULL COMMENT 'see constraints',
    `TIME_CREATED` TIMESTAMP(3) DEFAULT NOW(3) NOT NULL comment 'Date - record created',
    FOREIGN KEY (USER_ID)
        REFERENCES anycapital.USERS (id)
        ON DELETE CASCADE,
    FOREIGN KEY (BALANCE_HISTORY_COMMAND_ID)
        REFERENCES anycapital.BALANCE_HISTORY_COMMANDS (id)
        ON DELETE CASCADE
);
-- ------------------------------------------------------
--  DDL for Table DAY_COUNT_CONVENTIONS
-- ------------------------------------------------------
CREATE TABLE anycapital.DAY_COUNT_CONVENTIONS (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT,
    `NAME` VARCHAR(20) NOT NULL COMMENT 'we use this name as convention string for the formula',
    `DISPLAY_NAME` VARCHAR(40) NOT NULL COMMENT 'messages properties key name',
    PRIMARY KEY (ID)
); 
ALTER TABLE anycapital.DAY_COUNT_CONVENTIONS  COMMENT 'In finance, a day count convention determines how interest accrues over time for a variety of investments, including bonds, notes, loans, mortgages, medium-term notes, swaps, and forward rate agreements (FRAs).
https://en.wikipedia.org/wiki/Day_count_convention';
-- INSERTING into DAY_COUNT_CONVENTIONS
Insert into anycapital.DAY_COUNT_CONVENTIONS (ID,NAME,DISPLAY_NAME) values (4,'30U/360','day.count.conventions.4');
Insert into anycapital.DAY_COUNT_CONVENTIONS (ID,NAME,DISPLAY_NAME) values (1,'act/act isda','DAY.COUNT.CONVENTIONS.1');
Insert into anycapital.DAY_COUNT_CONVENTIONS (ID,NAME,DISPLAY_NAME) values (2,'act/365','DAY.COUNT.CONVENTIONS.2');
Insert into anycapital.DAY_COUNT_CONVENTIONS (ID,NAME,DISPLAY_NAME) values (3,'act/360','DAY.COUNT.CONVENTIONS.3');
-- ------------------------------------------------------
--  DDL for Table MARKETING_DOMAINS
-- ------------------------------------------------------
CREATE TABLE anycapital.MARKETING_DOMAINS (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT COMMENT 'Domain\'s id',
    `DOMAIN` VARCHAR(100) NOT NULL COMMENT 'domain\'s address',
    PRIMARY KEY (ID)
); 
-- ------------------------------------------------------
--  DDL for Table MARKETING_SOURCES
-- ------------------------------------------------------
CREATE TABLE anycapital.MARKETING_SOURCES (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT COMMENT 'Source\'s id',
    `NAME` VARCHAR(100) NOT NULL COMMENT 'Source\'s name',
    `WRITER_ID` SMALLINT NOT NULL COMMENT 'Foreign key to WRITERS',
    `TIME_CREATED` TIMESTAMP(3) DEFAULT NOW(3) NOT NULL COMMENT 'Record\'s time created',
    `TIME_MODIFIED` TIMESTAMP NULL COMMENT 'Record\'s time updated',
    FOREIGN KEY (WRITER_ID)
        REFERENCES anycapital.WRITERS (id)
        ON DELETE CASCADE,
    PRIMARY KEY (ID)
); 
-- ------------------------------------------------------
--  DDL for Table MARKETING_CONTENTS
-- ------------------------------------------------------
CREATE TABLE anycapital.MARKETING_CONTENTS (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT COMMENT 'Content\'s id',
    `NAME` VARCHAR(50) NOT NULL COMMENT 'Content\'s name',
    `COMMENTS` VARCHAR(4000) COMMENT 'Any comments',
    `PAGE_CONTENT` LONGTEXT COMMENT 'Page content can include html file etc.',
    `WRITER_ID` SMALLINT NOT NULL COMMENT 'Foreign key to WRITERS',
    `TIME_CREATED` TIMESTAMP(3) DEFAULT NOW(3) COMMENT 'Content\'s time created',
    `TIME_MODIFIED` TIMESTAMP NULL COMMENT 'Content\'s time updated',
    FOREIGN KEY (WRITER_ID)
        REFERENCES anycapital.WRITERS (id)
        ON DELETE CASCADE,
    PRIMARY KEY (ID)
);
-- ------------------------------------------------------
--  DDL for Table MARKETING_LANDING_PAGES
-- ------------------------------------------------------
CREATE TABLE anycapital.MARKETING_LANDING_PAGES (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT COMMENT 'Landing page\'s id',
    `NAME` VARCHAR(50) NOT NULL COMMENT 'Landing page\'s name',
    `PATH` VARCHAR(4000) NOT NULL COMMENT 'Landing page\'s path',
    `WRITER_ID` SMALLINT NOT NULL COMMENT 'Foreign key to WRITERS',
    `TIME_CREATED` TIMESTAMP(3) DEFAULT NOW(3) COMMENT 'Landing page\'s time created',
    `TIME_MODIFIED` TIMESTAMP NULL COMMENT 'Landing page\'s time modified',
    FOREIGN KEY (WRITER_ID)
        REFERENCES anycapital.WRITERS (id)
        ON DELETE CASCADE,
    PRIMARY KEY (ID)
);
-- ------------------------------------------------------
--  DDL for Table MARKETING_CAMPAIGNS
-- ------------------------------------------------------
CREATE TABLE anycapital.MARKETING_CAMPAIGNS (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT COMMENT 'Campaign\'s id',
    `NAME` VARCHAR(100) NOT NULL COMMENT 'Campaign\'s name',
    `SOURCE_ID` SMALLINT COMMENT 'Foreign key to MARKETING_SOURCES',
    `WRITER_ID` SMALLINT NOT NULL COMMENT 'Foreign key to WRITERS',
    `TIME_CREATED` TIMESTAMP(3) DEFAULT NOW(3) COMMENT 'Campaign\'s time created',
    `TIME_MODIFIED` TIMESTAMP NULL COMMENT 'Campaign\'s time updated',
    `DOMAIN_ID` SMALLINT COMMENT 'Foreign key to MARKETING_DOMAINS',
    `LANDING_PAGE_ID` SMALLINT COMMENT 'Foreign key to MARKETING_LANDING_PAGES',
    `CONTENT_ID` SMALLINT COMMENT 'Foreign key to MARKETING_CONTENTS',
    FOREIGN KEY (WRITER_ID)
        REFERENCES anycapital.WRITERS (id)
        ON DELETE CASCADE,
    FOREIGN KEY (SOURCE_ID)
        REFERENCES anycapital.MARKETING_SOURCES (id)
        ON DELETE CASCADE,
    FOREIGN KEY (DOMAIN_ID)
        REFERENCES anycapital.MARKETING_DOMAINS (id)
        ON DELETE CASCADE,
    FOREIGN KEY (LANDING_PAGE_ID)
        REFERENCES anycapital.MARKETING_LANDING_PAGES (id)
        ON DELETE CASCADE,
    FOREIGN KEY (CONTENT_ID)
        REFERENCES anycapital.MARKETING_CONTENTS (id)
        ON DELETE CASCADE,
    PRIMARY KEY (ID)
); 
-- ------------------------------------------------------
--  DDL for Table MARKETING_TRACKING
-- ------------------------------------------------------
CREATE TABLE anycapital.MARKETING_TRACKING (
    `ID` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'Marketing tracking\'s id',
    `CAMPAIGN_ID` SMALLINT COMMENT 'Foreign key to MARKETING_CAMPAIGNS',
    `TIME_CREATED` TIMESTAMP(3) DEFAULT NOW(3) COMMENT 'Marketing tracking\'s time created',
    `QUERY_STRING` VARCHAR(4000) COMMENT 'Full tracking\'s query string',
    FOREIGN KEY (CAMPAIGN_ID)
        REFERENCES anycapital.MARKETING_CAMPAIGNS (id)
        ON DELETE CASCADE,
    PRIMARY KEY (ID)
);
-- ------------------------------------------------------
--  DDL for Table MARKETING_ATTRIBUTION
-- ------------------------------------------------------
CREATE TABLE anycapital.MARKETING_ATTRIBUTION (
    `ID` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'Marketing attribution\'s id',
    `USER_ID` BIGINT NOT NULL COMMENT 'Foreign key to USERS',
    `TABLE_ID` SMALLINT NOT NULL COMMENT 'Foreign key to TABLES',
    `REFERENCE_ID` BIGINT NOT NULL COMMENT 'reference id to table, according to the column TABLE_ID',
    `TRACKING_ID` BIGINT COMMENT 'Foreign key to MARKETING_TRACKING',
    `TIME_CREATED` TIMESTAMP(3) DEFAULT NOW(3) COMMENT 'Marketing attribution\'s time_created',
    FOREIGN KEY (USER_ID)
        REFERENCES anycapital.USERS (id)
        ON DELETE CASCADE,
    FOREIGN KEY (TABLE_ID)
        REFERENCES anycapital.TABLES (id)
        ON DELETE CASCADE,
    FOREIGN KEY (TRACKING_ID)
        REFERENCES anycapital.MARKETING_TRACKING (id)
        ON DELETE CASCADE,
    PRIMARY KEY (ID)
); 
-- ------------------------------------------------------
--  DDL for Table LOGINS
-- ------------------------------------------------------
  CREATE TABLE anycapital.LOGINS (
    `ID` BIGINT NOT NULL AUTO_INCREMENT,
    `USER_ID` BIGINT,
    `ACTION_SOURCE_ID` TINYINT NOT NULL,
    `TIME_CREATED` TIMESTAMP(3) DEFAULT NOW(3),
    `USER_AGENT` VARCHAR(2000),
    `MARKETING_TRACKING_ID` BIGINT COMMENT 'Foreign key to MARKETING_TRACKING',
    FOREIGN KEY (USER_ID)
        REFERENCES anycapital.USERS (id)
        ON DELETE CASCADE,
    FOREIGN KEY (MARKETING_TRACKING_ID)
        REFERENCES anycapital.MARKETING_TRACKING (id)
        ON DELETE CASCADE,
    PRIMARY KEY (ID)
); 
-- ------------------------------------------------------
--  DDL for Table MARKET_PRICES
-- ------------------------------------------------------
  CREATE TABLE anycapital.MARKET_PRICES
   (`MARKET_ID` SMALLINT, 
	`TIME_MODIFIED` TIMESTAMP DEFAULT NOW(), 
	`PRICE` DOUBLE, 
	`TYPE_ID` TINYINT NOT NULL DEFAULT 1, 
	`PRICE_DATE` TIMESTAMP NULL,
    FOREIGN KEY (MARKET_ID)
		REFERENCES anycapital.MARKETS (id)
     ON DELETE CASCADE
   );
-- ------------------------------------------------------
--  DDL for Table MSG_RES_TYPE
-- ------------------------------------------------------
CREATE TABLE anycapital.MSG_RES_TYPE (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT,
    `NAME` VARCHAR(20) NOT NULL,
    PRIMARY KEY (ID)
);
Insert into anycapital.MSG_RES_TYPE (NAME) values ('General');
Insert into anycapital.MSG_RES_TYPE (NAME) values ('Product-type');
-- ------------------------------------------------------
--  DDL for Table MSG_RES
-- ------------------------------------------------------
CREATE TABLE anycapital.MSG_RES (
    `ID` INT NOT NULL AUTO_INCREMENT,
    `KEY` VARCHAR(200) NOT NULL,
    `ACTION_SOURCE_ID` TINYINT NOT NULL,
    `TYPE_ID` SMALLINT DEFAULT 1,
    `ENTITY_ID` SMALLINT,
    FOREIGN KEY (ACTION_SOURCE_ID)
        REFERENCES anycapital.ACTION_SOURCE (id)
        ON DELETE CASCADE,
    FOREIGN KEY (TYPE_ID)
        REFERENCES anycapital.MSG_RES_TYPE (id)
        ON DELETE CASCADE,
	UNIQUE (`KEY`, `ACTION_SOURCE_ID`),
    PRIMARY KEY (ID)
);

-- ------------------------------------------------------
--  DDL for Table MSG_RES_LANGUAGE
-- ------------------------------------------------------
CREATE TABLE anycapital.MSG_RES_LANGUAGE (
    `MSG_RES_ID` INT NOT NULL,
    `MSG_RES_LANGUAGE_ID` SMALLINT NOT NULL,
    `VALUE` VARCHAR(2000),
    `TIME_UPDATED` TIMESTAMP DEFAULT NOW() NOT NULL,
    `WRITER_ID` SMALLINT,
    `LARGE_VALUE` LONGTEXT,
    FOREIGN KEY (MSG_RES_ID)
        REFERENCES anycapital.MSG_RES (id)
        ON DELETE CASCADE,
    FOREIGN KEY (WRITER_ID)
        REFERENCES anycapital.WRITERS (id)
        ON DELETE CASCADE,
    FOREIGN KEY (MSG_RES_LANGUAGE_ID)
        REFERENCES anycapital.LANGUAGES (id)
        ON DELETE CASCADE,
    UNIQUE (`MSG_RES_ID` , `MSG_RES_LANGUAGE_ID`)
);

-- ------------------------------------------------------
--  DDL for Table TRANSACTIONS_HISTORY
-- ------------------------------------------------------
CREATE TABLE anycapital.TRANSACTIONS_HISTORY (
    `TRANSACTION_ID` BIGINT NOT NULL COMMENT 'reference',
    `TRANSACTION_STATUS_ID` SMALLINT NOT NULL COMMENT 'reference',
    `WRITER_ID` SMALLINT COMMENT 'reference',
    `SERVER_NAME` VARCHAR(50) NOT NULL COMMENT 'The server from which the operation was carried out',
    `ACTION_SOURCE_ID` TINYINT NOT NULL COMMENT 'reference',
    `TIME_CREATED` TIMESTAMP(3) DEFAULT NOW(3) NOT NULL,
    `LOGIN_ID` BIGINT COMMENT 'the login id of the user that made this action 0 if its job',
    FOREIGN KEY (TRANSACTION_STATUS_ID)
        REFERENCES anycapital.TRANSACTION_STATUSES (id)
        ON DELETE CASCADE,
    FOREIGN KEY (TRANSACTION_ID)
        REFERENCES anycapital.TRANSACTIONS (id)
        ON DELETE CASCADE,
    FOREIGN KEY (WRITER_ID)
        REFERENCES anycapital.WRITERS (id)
        ON DELETE CASCADE,
    FOREIGN KEY (ACTION_SOURCE_ID)
        REFERENCES anycapital.ACTION_SOURCE (id)
        ON DELETE CASCADE,
    FOREIGN KEY (LOGIN_ID)
        REFERENCES anycapital.LOGINS (id)
        ON DELETE CASCADE
); 
-- ------------------------------------------------------
--  DDL for Table SMS_TYPES
-- ------------------------------------------------------
  CREATE TABLE anycapital.SMS_TYPES 
   (`ID` SMALLINT NOT NULL AUTO_INCREMENT, 
	`NAME` VARCHAR(30) NOT NULL,
    PRIMARY KEY (ID)
   );
-- INSERTING into anycapital.SMS_TYPES
Insert into anycapital.SMS_TYPES (NAME) values ('free_text');
-- ------------------------------------------------------
--  DDL for Table SMS_STATUSES
-- ------------------------------------------------------
CREATE TABLE anycapital.SMS_STATUSES (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT,
    `NAME` VARCHAR(30) NOT NULL,
    PRIMARY KEY (ID)
);
-- INSERTING into anycapital.SMS_STATUSES
Insert into anycapital.SMS_STATUSES (NAME) values ('Queued');
Insert into anycapital.SMS_STATUSES (NAME) values ('Submitted');
Insert into anycapital.SMS_STATUSES (NAME) values ('Failed to submit');
Insert into anycapital.SMS_STATUSES (NAME) values ('Delivered');
Insert into anycapital.SMS_STATUSES (NAME) values ('Failed');
Insert into anycapital.SMS_STATUSES (NAME) values ('Unroutable');
Insert into anycapital.SMS_STATUSES (NAME) values ('Not verified');
Insert into anycapital.SMS_STATUSES (NAME) values ('Invalid');
Insert into anycapital.SMS_STATUSES (NAME) values ('Waiting HLR');
-- ------------------------------------------------------
--  DDL for Table SMS_PROVIDERS
-- ------------------------------------------------------
CREATE TABLE anycapital.SMS_PROVIDERS (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT,
    `NAME` VARCHAR(20) NOT NULL,
    `PROVIDER_CLASS` VARCHAR(200) NOT NULL,
    `URL` VARCHAR(200) NOT NULL,
    `USER_NAME` VARCHAR(30) NOT NULL,
    `PASSWORD` VARCHAR(20) NOT NULL,
    `PROPS` VARCHAR(2000),
    `DLR_URL` VARCHAR(200),
    PRIMARY KEY (ID)
);
-- INSERTING into anycapital.SMS_PROVIDERS
Insert into anycapital.SMS_PROVIDERS (NAME,PROVIDER_CLASS,URL,USER_NAME,PASSWORD,PROPS,DLR_URL) values ('mobivate','capital.any.sms.MobivateSMSProvider','http://app.mobivatebulksms.com/bulksms/xmlapi/','eran.levy@anyoption.com','anyopt123',null,null);
-- ------------------------------------------------------
--  DDL for Table SMS
-- ------------------------------------------------------
  CREATE TABLE anycapital.SMS
   (`ID` BIGINT NOT NULL AUTO_INCREMENT,
	`TYPE_ID` SMALLINT NOT NULL, 
	`SENDER` VARCHAR(20) NOT NULL, 
	`PHONE` VARCHAR(20) NOT NULL, 
	`MESSAGE` NVARCHAR(1000) NOT NULL, 
	`PROVIDER_ID` SMALLINT, 
	`RETRIES` TINYINT DEFAULT 0, 
	`TIME_CREATED` TIMESTAMP(3) DEFAULT NOW(3), 
	`TIME_SCHEDULED` TIMESTAMP NULL, 
	`TIME_SENT` TIMESTAMP NULL, 
	`STATUS_ID` SMALLINT NOT NULL, 
	`REFERENCE_ID` VARCHAR(100), 
	`RESPONSE` VARCHAR(2000), 
	`SENDER_NUMBER` VARCHAR(20),
    FOREIGN KEY (TYPE_ID)
        REFERENCES anycapital.SMS_TYPES (id)
        ON DELETE CASCADE,
    FOREIGN KEY (PROVIDER_ID)
        REFERENCES anycapital.SMS_PROVIDERS (id)
        ON DELETE CASCADE,
    FOREIGN KEY (STATUS_ID)
        REFERENCES anycapital.SMS_STATUSES (id)
        ON DELETE CASCADE,        
    PRIMARY KEY (ID)
   );
-- ------------------------------------------------------
--  DDL for Table PRODUCT_CATEGORIES
-- ------------------------------------------------------
CREATE TABLE anycapital.PRODUCT_CATEGORIES (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT,
    `NAME` VARCHAR(50) NOT NULL COMMENT 'the category name',
    `DISPLAY_NAME` VARCHAR(20) NOT NULL COMMENT 'messages properties key name',
    `IS_CAPITAL_PROTECTION` BINARY NOT NULL DEFAULT 0 COMMENT '1 if this category should have Capital Protection option',
    PRIMARY KEY (ID)
);

ALTER TABLE `anycapital`.`product_categories` 
ADD COLUMN `RISK_FACTORS_KEY` VARCHAR(45) NOT NULL COMMENT 'the msg resource key for the text' AFTER `IS_CAPITAL_PROTECTION`;


-- INSERTING into anycapital.PRODUCT_CATEGORIES
Insert into anycapital.PRODUCT_CATEGORIES (NAME,DISPLAY_NAME,IS_CAPITAL_PROTECTION, RISK_FACTORS_KEY) values ('Capital Protection','product.categroy.1',1, 'risk-factors-1');
Insert into anycapital.PRODUCT_CATEGORIES (NAME,DISPLAY_NAME,IS_CAPITAL_PROTECTION, RISK_FACTORS_KEY) values ('Yield Enhancement','product.categroy.2',0, 'risk-factors-2');
Insert into anycapital.PRODUCT_CATEGORIES (NAME,DISPLAY_NAME,IS_CAPITAL_PROTECTION, RISK_FACTORS_KEY) values ('Participation','product.categroy.3',0, 'risk-factors-3');
-- ------------------------------------------------------
--  DDL for Table PRODUCT_MATURITIES
-- ------------------------------------------------------
CREATE TABLE anycapital.PRODUCT_MATURITIES (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT,
    `NAME` VARCHAR(20) NOT NULL,
    `DISPLAY_NAME` VARCHAR(50) NOT NULL COMMENT 'key for translaiton',
    PRIMARY KEY (ID)
); 
ALTER TABLE anycapital.PRODUCT_MATURITIES COMMENT 'This table is only used to display information';
-- INSERTING into anycapital.PRODUCT_MATURITIES
Insert into anycapital.PRODUCT_MATURITIES (ID,NAME,DISPLAY_NAME) values (1,'3M','product.maturities.1');
Insert into anycapital.PRODUCT_MATURITIES (ID,NAME,DISPLAY_NAME) values (2,'6M','product.maturities.2');
Insert into anycapital.PRODUCT_MATURITIES (ID,NAME,DISPLAY_NAME) values (3,'9M','product.maturities.3');
Insert into anycapital.PRODUCT_MATURITIES (ID,NAME,DISPLAY_NAME) values (4,'1Y','product.maturities.4');
Insert into anycapital.PRODUCT_MATURITIES (ID,NAME,DISPLAY_NAME) values (5,'18M','product.maturities.5');
Insert into anycapital.PRODUCT_MATURITIES (ID,NAME,DISPLAY_NAME) values (6,'2Y','product.maturities.6');
Insert into anycapital.PRODUCT_MATURITIES (ID,NAME,DISPLAY_NAME) values (7,'3Y','product.maturities.7');
Insert into anycapital.PRODUCT_MATURITIES (ID,NAME,DISPLAY_NAME) values (8,'5Y','product.maturities.8');
-- ------------------------------------------------------
--  DDL for Table PRODUCT_COUPON_FREQUENCIES
-- ------------------------------------------------------
CREATE TABLE anycapital.PRODUCT_COUPON_FREQUENCIES (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT,
    `NAME` VARCHAR(20) NOT NULL,
    `DISPLAY_NAME` VARCHAR(40) NOT NULL,
    PRIMARY KEY (ID)
);
-- INSERTING into anycapital.PRODUCT_COUPON_FREQUENCIES
Insert into anycapital.PRODUCT_COUPON_FREQUENCIES (ID,NAME,DISPLAY_NAME) values (1,'no coupon','PRODUCT.COUPON.FREQUENCIES.1');
Insert into anycapital.PRODUCT_COUPON_FREQUENCIES (ID,NAME,DISPLAY_NAME) values (2,'Monthly','PRODUCT.COUPON.FREQUENCIES.2');
Insert into anycapital.PRODUCT_COUPON_FREQUENCIES (ID,NAME,DISPLAY_NAME) values (3,'Quaterly','PRODUCT.COUPON.FREQUENCIES.3');
Insert into anycapital.PRODUCT_COUPON_FREQUENCIES (ID,NAME,DISPLAY_NAME) values (4,'Semi-Annual','PRODUCT.COUPON.FREQUENCIES.4');
Insert into anycapital.PRODUCT_COUPON_FREQUENCIES (ID,NAME,DISPLAY_NAME) values (5,'Annual','PRODUCT.COUPON.FREQUENCIES.5');
Insert into anycapital.PRODUCT_COUPON_FREQUENCIES (ID,NAME,DISPLAY_NAME) values (6,'In Fine','PRODUCT.COUPON.FREQUENCIES.6');
-- ------------------------------------------------------
--  DDL for Table PRODUCT_ISSUER_INSURANCE_TYPES
-- ------------------------------------------------------
CREATE TABLE anycapital.PRODUCT_ISSUER_INSURANCE_TYPES (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT,
    `NAME` VARCHAR(10) NOT NULL,
    `DISPLAY_NAME` VARCHAR(50) NOT NULL COMMENT 'messages properties key name',
    PRIMARY KEY (ID)
); 
-- INSERTING into anycapital.PRODUCT_ISSUER_INSURANCE_TYPES
Insert into anycapital.PRODUCT_ISSUER_INSURANCE_TYPES (ID,NAME,DISPLAY_NAME) values (1,'Cosi','PRODUCT.ISSUER.INSURANCE.TYPE.1');
Insert into anycapital.PRODUCT_ISSUER_INSURANCE_TYPES (ID,NAME,DISPLAY_NAME) values (2,'TCM','PRODUCT.ISSUER.INSURANCE.TYPE.2');
-- ------------------------------------------------------
--  DDL for Table PRODUCT_STATUSES
-- ------------------------------------------------------
CREATE TABLE anycapital.PRODUCT_STATUSES (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT,
    `NAME` VARCHAR(20) NOT NULL,
    `DISPLAY_NAME` VARCHAR(40) NOT NULL,
    PRIMARY KEY (ID)
);
-- INSERTING into anycapital.PRODUCT_STATUSES
Insert into anycapital.PRODUCT_STATUSES (ID,NAME,DISPLAY_NAME) values (2,'subscription','product.statuses.2');
Insert into anycapital.PRODUCT_STATUSES (ID,NAME,DISPLAY_NAME) values (3,'secondary','product.statuses.3');
Insert into anycapital.PRODUCT_STATUSES (ID,NAME,DISPLAY_NAME) values (4,'waiting for settle','product.statuses.4');
Insert into anycapital.PRODUCT_STATUSES (ID,NAME,DISPLAY_NAME) values (5,'settled','product.statuses.5');
Insert into anycapital.PRODUCT_STATUSES (ID,NAME,DISPLAY_NAME) values (6,'canceled','product.statuses.6');
Insert into anycapital.PRODUCT_STATUSES (ID,NAME,DISPLAY_NAME) values (1,'unpublished','product.statuses.1');
-- ------------------------------------------------------
--  DDL for Table PRODUCT_TYPE_PROTECTION_LEVEL
-- ------------------------------------------------------
CREATE TABLE anycapital.PRODUCT_TYPE_PROTECTION_LEVEL (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT,
    `NAME` VARCHAR(20) NOT NULL,
    `DISPLAY_NAME` VARCHAR(40) NOT NULL,
    PRIMARY KEY (ID)
);
-- INSERTING into anycapital.PRODUCT_TYPE_PROTECTION_LEVEL
Insert into anycapital.PRODUCT_TYPE_PROTECTION_LEVEL (ID,NAME,DISPLAY_NAME) values (1,'Protected','product_type_protection_level.1');
Insert into anycapital.PRODUCT_TYPE_PROTECTION_LEVEL (ID,NAME,DISPLAY_NAME) values (2,'Conditional','product_type_protection_level.2');
Insert into anycapital.PRODUCT_TYPE_PROTECTION_LEVEL (ID,NAME,DISPLAY_NAME) values (3,'Unprotected','product_type_protection_level.3');
-- ------------------------------------------------------
--  DDL for Table PRODUCT_BARRIER_TYPES
-- ------------------------------------------------------
CREATE TABLE anycapital.PRODUCT_BARRIER_TYPES (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT,
    `NAME` VARCHAR(20) NOT NULL,
    `DISPLAY_NAME` VARCHAR(50) NOT NULL,
    PRIMARY KEY (ID)
);
-- INSERTING into anycapital.PRODUCT_BARRIER_TYPES
Insert into anycapital.PRODUCT_BARRIER_TYPES (ID,NAME,DISPLAY_NAME) values (1,'American','product.barrier.types.1');
Insert into anycapital.PRODUCT_BARRIER_TYPES (ID,NAME,DISPLAY_NAME) values (2,'European','product.barrier.types.2');
-- ------------------------------------------------------
--  DDL for Table PRODUCTS_HISTORY_ACTIONS
-- ------------------------------------------------------
CREATE TABLE anycapital.PRODUCTS_HISTORY_ACTIONS (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT,
    `NAME` VARCHAR(40) NOT NULL,
    PRIMARY KEY (ID)
);
-- INSERTING into anycapital.PRODUCTS_HISTORY_ACTIONS
Insert into anycapital.PRODUCTS_HISTORY_ACTIONS (ID,NAME) values (1,'Insert product');
Insert into anycapital.PRODUCTS_HISTORY_ACTIONS (ID,NAME) values (2,'Update Ask Bid');
Insert into anycapital.PRODUCTS_HISTORY_ACTIONS (ID,NAME) values (3,'Update Suspend');
Insert into anycapital.PRODUCTS_HISTORY_ACTIONS (ID,NAME) values (4,'Update Status');
Insert into anycapital.PRODUCTS_HISTORY_ACTIONS (ID,NAME) values (5,'Update Product');
Insert into anycapital.PRODUCTS_HISTORY_ACTIONS (ID,NAME) values (6,'update priorty');
Insert into anycapital.PRODUCTS_HISTORY_ACTIONS (ID,NAME) values (7,'update product market start level');
Insert into anycapital.PRODUCTS_HISTORY_ACTIONS (ID,NAME) values (8,'update product market end level');
Insert into anycapital.PRODUCTS_HISTORY_ACTIONS (ID,NAME) values (9,'update product barrier');
Insert into anycapital.PRODUCTS_HISTORY_ACTIONS (ID,NAME) values (10,'update product observation level');
Insert into anycapital.PRODUCTS_HISTORY_ACTIONS (ID,NAME) values (11,'update product coupon paid');
-- ------------------------------------------------------
--  DDL for Table PRODUCT_TYPES
-- ------------------------------------------------------
CREATE TABLE anycapital.PRODUCT_TYPES (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT,
    `NAME` VARCHAR(50) NOT NULL,
    `DISPLAY_NAME` VARCHAR(20) NOT NULL COMMENT 'messages properties key name',
    `PRODUCT_CATEGORY_ID` SMALLINT NOT NULL,
    `IS_COUPON` BINARY DEFAULT 0 COMMENT '1 if this category should have coupon option',
    `IS_DAY_COUNT_CONVENTION` BINARY DEFAULT 0 COMMENT '1 if this category should have DAY COUNT CONVENTION option',
    `IS_MAX_YIELD` BINARY COMMENT '1 if this category should have Max Yield option',
    `IS_MAX_YIELD_P_A` BINARY DEFAULT 0 COMMENT '1 if this category should have Max Yield p.a option',
    `IS_PARTICIPATION` BINARY DEFAULT 0 COMMENT '1 if this category should have PARTICIPATION option',
    `IS_STRIKE_LEVEL` BINARY DEFAULT 0 COMMENT '1 if this category should have STRIKE LEVEL option',
    `IS_BONUS_PRECENTAGE` BINARY DEFAULT 0 COMMENT '1 if this category should have BONUS PRECENTAGE option',
    `RISK_LEVEL` TINYINT NOT NULL COMMENT 'the risk level of this type',
    `FORMULA_CLASS_PATH` VARCHAR(100) NOT NULL COMMENT 'the class path to the formula in our application',
    `FORMULA_PARAMETERS` VARCHAR(100) NOT NULL COMMENT 'json string with parameters for the formula class',
    `IS_DISTANCE_TO_BARRIER` BINARY NOT NULL DEFAULT 0 COMMENT '1 if this type can have DISTANCE TO BARRIER else 0',
    `IS_CAP_LEVEL` BINARY NOT NULL DEFAULT 0 COMMENT '1 if this type can have cap level else 0',
    `PRODUCT_TYPE_P_L_ID` SMALLINT NOT NULL DEFAULT 1 COMMENT 'PRODUCT_TYPE_PROTECTION_LEVEL id',
    FOREIGN KEY (PRODUCT_CATEGORY_ID)
        REFERENCES anycapital.PRODUCT_CATEGORIES (id)
        ON DELETE CASCADE,
    FOREIGN KEY (PRODUCT_TYPE_P_L_ID)
        REFERENCES anycapital.PRODUCT_TYPE_PROTECTION_LEVEL (id)
        ON DELETE CASCADE,
    PRIMARY KEY (ID)
);

ALTER TABLE anycapital.PRODUCT_TYPES COMMENT 'all product types';

ALTER TABLE `anycapital`.`product_types` 
ADD COLUMN `SLIDER_MIN` INT NOT NULL DEFAULT -100 COMMENT 'min value for the slider' AFTER `PRODUCT_TYPE_P_L_ID`,
ADD COLUMN `SLIDER_MAX` INT NOT NULL DEFAULT 100 COMMENT 'max value for the slider' AFTER `SLIDER_MIN`;

ALTER TABLE `anycapital`.`product_types` 
ADD COLUMN `is_Active` BINARY(1) NOT NULL DEFAULT 1 AFTER `SLIDER_MAX`;


-- INSERTING into anycapital.PRODUCT_TYPES
Insert into anycapital.PRODUCT_TYPES (ID,NAME,DISPLAY_NAME,PRODUCT_CATEGORY_ID,IS_COUPON,IS_DAY_COUNT_CONVENTION,IS_MAX_YIELD,IS_MAX_YIELD_P_A,IS_PARTICIPATION,IS_STRIKE_LEVEL,IS_BONUS_PRECENTAGE,RISK_LEVEL,FORMULA_CLASS_PATH,FORMULA_PARAMETERS,IS_DISTANCE_TO_BARRIER,IS_CAP_LEVEL,PRODUCT_TYPE_P_L_ID) values (1,'Capital Protection Cartificate with Participation','product.type.1',1,0,1,1,1,1,0,0,1,'capital.any.product.type.formula.CPCP','{"scenarioPercentages": [180, 120, 100]}',0,0,1);
Insert into anycapital.PRODUCT_TYPES (ID,NAME,DISPLAY_NAME,PRODUCT_CATEGORY_ID,IS_COUPON,IS_DAY_COUNT_CONVENTION,IS_MAX_YIELD,IS_MAX_YIELD_P_A,IS_PARTICIPATION,IS_STRIKE_LEVEL,IS_BONUS_PRECENTAGE,RISK_LEVEL,FORMULA_CLASS_PATH,FORMULA_PARAMETERS,IS_DISTANCE_TO_BARRIER,IS_CAP_LEVEL,PRODUCT_TYPE_P_L_ID) values (2,'Barrier Capital Protection Certificate','product.type.2',1,0,1,1,1,1,0,0,1,'capital.any.product.type.formula.BCPC','{"scenarioPercentages": [80, 100, 100]}',1,0,1);
Insert into anycapital.PRODUCT_TYPES (ID,NAME,DISPLAY_NAME,PRODUCT_CATEGORY_ID,IS_COUPON,IS_DAY_COUNT_CONVENTION,IS_MAX_YIELD,IS_MAX_YIELD_P_A,IS_PARTICIPATION,IS_STRIKE_LEVEL,IS_BONUS_PRECENTAGE,RISK_LEVEL,FORMULA_CLASS_PATH,FORMULA_PARAMETERS,IS_DISTANCE_TO_BARRIER,IS_CAP_LEVEL,PRODUCT_TYPE_P_L_ID) values (3,'Capital Protection Certificate with Coupon','product.type.3',1,1,1,0,0,0,0,0,1,'capital.any.product.type.formula.CPCC','{"scenarioPercentages": [100, 100]}',0,0,1);
Insert into anycapital.PRODUCT_TYPES (ID,NAME,DISPLAY_NAME,PRODUCT_CATEGORY_ID,IS_COUPON,IS_DAY_COUNT_CONVENTION,IS_MAX_YIELD,IS_MAX_YIELD_P_A,IS_PARTICIPATION,IS_STRIKE_LEVEL,IS_BONUS_PRECENTAGE,RISK_LEVEL,FORMULA_CLASS_PATH,FORMULA_PARAMETERS,IS_DISTANCE_TO_BARRIER,IS_CAP_LEVEL,PRODUCT_TYPE_P_L_ID) values (4,'Reverse Convertible','product.type.4',2,1,1,0,0,0,1,0,3,'capital.any.product.type.formula.RC','{"scenarioPercentages": [100, 90, 50]}',0,0,2);
Insert into anycapital.PRODUCT_TYPES (ID,NAME,DISPLAY_NAME,PRODUCT_CATEGORY_ID,IS_COUPON,IS_DAY_COUNT_CONVENTION,IS_MAX_YIELD,IS_MAX_YIELD_P_A,IS_PARTICIPATION,IS_STRIKE_LEVEL,IS_BONUS_PRECENTAGE,RISK_LEVEL,FORMULA_CLASS_PATH,FORMULA_PARAMETERS,IS_DISTANCE_TO_BARRIER,IS_CAP_LEVEL,PRODUCT_TYPE_P_L_ID) values (5,'Barrier Reverse Convertible','product.type.5',2,1,1,0,0,0,0,0,2,'capital.any.product.type.formula.BRC','{"scenarioPercentages": [110, 80]}',1,0,2);
Insert into anycapital.PRODUCT_TYPES (ID,NAME,DISPLAY_NAME,PRODUCT_CATEGORY_ID,IS_COUPON,IS_DAY_COUNT_CONVENTION,IS_MAX_YIELD,IS_MAX_YIELD_P_A,IS_PARTICIPATION,IS_STRIKE_LEVEL,IS_BONUS_PRECENTAGE,RISK_LEVEL,FORMULA_CLASS_PATH,FORMULA_PARAMETERS,IS_DISTANCE_TO_BARRIER,IS_CAP_LEVEL,PRODUCT_TYPE_P_L_ID,SLIDER_MAX) values (6,'Inverse Barrier Reverse Convertible','product.type.6',2,1,1,0,0,0,0,0,2,'capital.any.product.type.formula.IBRC','{"scenarioPercentages": [90, 120]}',1,0,2,140);
Insert into anycapital.PRODUCT_TYPES (ID,NAME,DISPLAY_NAME,PRODUCT_CATEGORY_ID,IS_COUPON,IS_DAY_COUNT_CONVENTION,IS_MAX_YIELD,IS_MAX_YIELD_P_A,IS_PARTICIPATION,IS_STRIKE_LEVEL,IS_BONUS_PRECENTAGE,RISK_LEVEL,FORMULA_CLASS_PATH,FORMULA_PARAMETERS,IS_DISTANCE_TO_BARRIER,IS_CAP_LEVEL,PRODUCT_TYPE_P_L_ID) values (7,'Express Certificate','product.type.7',2,1,1,0,0,0,1,0,2,'capital.any.product.type.formula.EC','{"scenarioPercentages": [110, 90, 80]}',1,0,2);
Insert into anycapital.PRODUCT_TYPES (ID,NAME,DISPLAY_NAME,PRODUCT_CATEGORY_ID,IS_COUPON,IS_DAY_COUNT_CONVENTION,IS_MAX_YIELD,IS_MAX_YIELD_P_A,IS_PARTICIPATION,IS_STRIKE_LEVEL,IS_BONUS_PRECENTAGE,RISK_LEVEL,FORMULA_CLASS_PATH,FORMULA_PARAMETERS,IS_DISTANCE_TO_BARRIER,IS_CAP_LEVEL,PRODUCT_TYPE_P_L_ID) values (8,'Outperformance Certificate','product.type.8',3,0,0,0,0,1,0,0,3,'capital.any.product.type.formula.OC','{"scenarioPercentages": [160, 110, 80, 120, 50, 80]}',0,1,3);
Insert into anycapital.PRODUCT_TYPES (ID,NAME,DISPLAY_NAME,PRODUCT_CATEGORY_ID,IS_COUPON,IS_DAY_COUNT_CONVENTION,IS_MAX_YIELD,IS_MAX_YIELD_P_A,IS_PARTICIPATION,IS_STRIKE_LEVEL,IS_BONUS_PRECENTAGE,RISK_LEVEL,FORMULA_CLASS_PATH,FORMULA_PARAMETERS,IS_DISTANCE_TO_BARRIER,IS_CAP_LEVEL,PRODUCT_TYPE_P_L_ID) values (9,'Bonus Certificate','product.type.9',3,0,0,1,1,1,1,1,2,'capital.any.product.type.formula.BC','{"scenarioPercentages": [140, 90, 80]}',1,0,2);
Insert into anycapital.PRODUCT_TYPES (ID,NAME,DISPLAY_NAME,PRODUCT_CATEGORY_ID,IS_COUPON,IS_DAY_COUNT_CONVENTION,IS_MAX_YIELD,IS_MAX_YIELD_P_A,IS_PARTICIPATION,IS_STRIKE_LEVEL,IS_BONUS_PRECENTAGE,RISK_LEVEL,FORMULA_CLASS_PATH,FORMULA_PARAMETERS,IS_DISTANCE_TO_BARRIER,IS_CAP_LEVEL,PRODUCT_TYPE_P_L_ID) values (10,'Bonus Outperformance Certificate','product.type.10',3,0,0,1,1,1,1,1,2,'capital.any.product.type.formula.BOC','{"scenarioPercentages": [150, 80, 80]}',1,0,2);

UPDATE `anycapital`.`product_types` SET `is_Active`=0 WHERE `ID`='10';

-- ------------------------------------------------------
--  DDL for Table PRODUCTS
-- ------------------------------------------------------
  CREATE TABLE anycapital.PRODUCTS (
    `ID` BIGINT NOT NULL AUTO_INCREMENT,
    `TIME_CREATED` TIMESTAMP(3) DEFAULT NOW(3) NOT NULL,
    `PRODUCT_STATUS_ID` SMALLINT NOT NULL,
    `PRODUCT_TYPE_ID` SMALLINT NOT NULL,
    `ISSUE_PRICE` SMALLINT NOT NULL COMMENT 'percentage of the Denomination',
    `CURRENCY_ID` SMALLINT NOT NULL COMMENT 'currency of Denomination',
    `DENOMINATION` INT NOT NULL COMMENT 'amount of one denomination (minimum investment)',
    `PRODUCT_COUPON_FREQUENCY_ID` SMALLINT NOT NULL COMMENT 'when to give user coupon only for user information',
    `DAY_COUNT_CONVENTION_ID` SMALLINT,
    `MAX_YIELD` SMALLINT COMMENT 'maximum profit effective profit',
    `MAX_YIELD_P_A` SMALLINT COMMENT 'maximum profit yearly',
    `LEVEL_OF_PROTECTION` SMALLINT COMMENT 'how much percentage of user investment amount is protected user cant get less than this amount. (maximum u  can lose 1-level of protection)',
    `LEVEL_OF_PARTICIPATION` SMALLINT COMMENT 'percentage of the level change',
    `STRIKE_LEVEL` SMALLINT COMMENT 'percentage of the market price at the start of the product for formula result',
    `BONUS_LEVEL` SMALLINT COMMENT 'percentage of the Denomination at the start of the product for formula result',
    `SUBSCRIPTION_START_DATE` TIMESTAMP NOT NULL COMMENT 'start date to subscribe',
    `SUBSCRIPTION_END_DATE` TIMESTAMP NOT NULL COMMENT 'end date to subscribe',
    `INITIAL_FIXING_DATE` TIMESTAMP NOT NULL COMMENT 'Date where we fix the initial level of the underlying',
    `ISSUE_DATE` TIMESTAMP NULL COMMENT 'Date of the issuance of the product usually at the Exchange or Clearance house in case there is an issuance otherwise this date is similar to the initial fixing date',
    `FIRST_EXCHANGE_TRADING_DATE` TIMESTAMP NOT NULL COMMENT 'first day where we take into account the underlying movement',
    `LAST_TRADING_DATE` TIMESTAMP NOT NULL COMMENT 'last day of trading of the underlying that account for the product',
    `FINAL_FIXING_DATE` TIMESTAMP NOT NULL COMMENT 'Date where we fix the final fixing level for settlement calculation',
    `REDEMPTION_DATE` TIMESTAMP NOT NULL COMMENT 'when we need to give money to customers',
    `IS_SUSPEND` BINARY NOT NULL DEFAULT 0 COMMENT '1 if this market is currently suspend',
    `IS_QUANTO` BINARY NOT NULL DEFAULT 0 COMMENT '1 if the markets have different currency from the denomination',
    `PRODUCT_INSURANCE_TYPE_ID` SMALLINT COMMENT 'type of the Issuer Insurance from product_Issuer_Insurance_types  can be null',
    `BID` DOUBLE COMMENT 'when product is in Secondary state user can sell his investment with this bid percentage of the domination',
    `ASK` DOUBLE COMMENT 'when product is in Secondary state user can buy investment with this ask percentage of the domination',
    `FORMULA_RESULT` DOUBLE COMMENT 'when settle we take the formula result to calculate the investment result',
    `BOND_FLOOR_AT_ISSUANCE` DOUBLE COMMENT 'structured product include a zero coupon bond + an option strategy
    this is the zero coupon face amount',
    `ISSUE_SIZE` BIGINT COMMENT 'max amount that we accept subscription',
    `PRIORITY` INT COMMENT 'the priority of the product in home page and products page',
    `CAP_LEVEL` DOUBLE COMMENT '0 if no cap level , percentage that represent from where the client stop participating to the upside',
    `PRODUCT_MATURITY_ID` SMALLINT NOT NULL COMMENT 'in product pages we need to display maturity in term of Tenor',
    FOREIGN KEY (PRODUCT_TYPE_ID)
        REFERENCES anycapital.PRODUCT_TYPES (id)
        ON DELETE CASCADE,
    FOREIGN KEY (PRODUCT_STATUS_ID)
        REFERENCES anycapital.PRODUCT_STATUSES (id)
        ON DELETE CASCADE,
    FOREIGN KEY (DAY_COUNT_CONVENTION_ID)
        REFERENCES anycapital.DAY_COUNT_CONVENTIONS (id)
        ON DELETE CASCADE,
    FOREIGN KEY (PRODUCT_COUPON_FREQUENCY_ID)
        REFERENCES anycapital.PRODUCT_COUPON_FREQUENCIES (id)
        ON DELETE CASCADE,
    FOREIGN KEY (PRODUCT_MATURITY_ID)
        REFERENCES anycapital.PRODUCT_MATURITIES (id)
        ON DELETE CASCADE,
    FOREIGN KEY (CURRENCY_ID)
        REFERENCES anycapital.CURRENCIES (id)
        ON DELETE CASCADE,
    PRIMARY KEY (ID)
); 
-- ------------------------------------------------------
--  DDL for Table PRODUCT_BARRIERS
-- ------------------------------------------------------
CREATE TABLE anycapital.PRODUCT_BARRIERS (
    `PRODUCT_ID` BIGINT NOT NULL,
    `IS_BARRIER_OCCUR` BINARY NOT NULL DEFAULT 0 COMMENT '1 if a barrier event has  occur else 0',
    `BARRIER_LEVEL` DOUBLE COMMENT 'percentage of the market price at the start of the product to touch the barrier',
    `PRODUCT_BARRIER_TYPE_ID` SMALLINT NOT NULL,
    `BARRIER_END` TIMESTAMP NULL COMMENT 'end date to check barrier',
    `BARRIER_START` TIMESTAMP NULL COMMENT 'start date to check barrier',
    FOREIGN KEY (PRODUCT_ID)
        REFERENCES anycapital.PRODUCTS (id)
        ON DELETE CASCADE,
    FOREIGN KEY (PRODUCT_BARRIER_TYPE_ID)
        REFERENCES anycapital.PRODUCT_BARRIER_TYPES (id)
        ON DELETE CASCADE,
    PRIMARY KEY (PRODUCT_ID)
); 
-- ------------------------------------------------------
--  DDL for Table PRODUCT_COUPONS
-- ------------------------------------------------------
CREATE TABLE anycapital.PRODUCT_COUPONS (
    `ID` BIGINT NOT NULL AUTO_INCREMENT,
    `PRODUCT_ID` BIGINT NOT NULL,
    `OBSERVATION_DATE` TIMESTAMP NOT NULL COMMENT 'on which date to check the coupon trigger level',
    `PAYMENT_DATE` TIMESTAMP NOT NULL COMMENT 'on which date to pay coupon',
    `TRIGGER_LEVEL` DOUBLE NOT NULL COMMENT 'percentage to check from start price of market that done worst performance',
    `PAY_RATE` DOUBLE NOT NULL COMMENT 'percentage from denomination to pay customer',
    `IS_PAID` BINARY NOT NULL DEFAULT 0 COMMENT '1 if we paid this coupon',
    `OBSERVATION_LEVEL` DOUBLE COMMENT 'the level to use in the observation date',
    FOREIGN KEY (PRODUCT_ID)
        REFERENCES anycapital.PRODUCTS (id)
        ON DELETE CASCADE,
	UNIQUE (`PRODUCT_ID`, `OBSERVATION_DATE`),         
    PRIMARY KEY (ID)
);   
ALTER TABLE anycapital.PRODUCT_COUPONS COMMENT 'pay coupon at payment date if in the OBSERVATION DATE the condition was true';
-- ------------------------------------------------------
--  DDL for Table PRODUCT_MARKETS
-- ------------------------------------------------------
CREATE TABLE anycapital.PRODUCT_MARKETS (
    `ID` BIGINT NOT NULL AUTO_INCREMENT,
    `PRODUCT_ID` BIGINT NOT NULL,
    `MARKET_ID` SMALLINT NOT NULL,
    `START_TRADE_LEVEL` DOUBLE,
    `END_TRADE_LEVEL` DOUBLE,
    FOREIGN KEY (PRODUCT_ID)
        REFERENCES anycapital.PRODUCTS (id)
        ON DELETE CASCADE,
    FOREIGN KEY (MARKET_ID)
        REFERENCES anycapital.MARKETS (id)
        ON DELETE CASCADE,
	UNIQUE (`PRODUCT_ID`, `MARKET_ID`),              
    PRIMARY KEY (ID)
);
-- ------------------------------------------------------
--  DDL for Table PRODUCT_SIMULATION
-- ------------------------------------------------------
CREATE TABLE anycapital.PRODUCT_SIMULATION (
    `ID` BIGINT NOT NULL AUTO_INCREMENT,
    `FINAL_FIXING_LEVEL` DOUBLE NOT NULL,
    `POSITION` SMALLINT NOT NULL,
    `PRODUCT_ID` BIGINT NOT NULL,
    FOREIGN KEY (PRODUCT_ID)
        REFERENCES anycapital.PRODUCTS (id)
        ON DELETE CASCADE,
    UNIQUE (`FINAL_FIXING_LEVEL` , `PRODUCT_ID`),
    PRIMARY KEY (ID)
);
-- ------------------------------------------------------
--  DDL for Table PRODUCTS_HISTORY
-- ------------------------------------------------------
CREATE TABLE anycapital.PRODUCTS_HISTORY (
    `PRODUCT_ID` BIGINT NOT NULL,
    `PRODUCTS_HISTORY_ACTION_ID` SMALLINT NOT NULL,
    `ACTION_PARAMTERS` LONGTEXT NOT NULL COMMENT 'the action new paramters we set to the product json object',
    `TIME_CREATED` TIMESTAMP(3) DEFAULT NOW(3) NOT NULL,
    `ACTION_SOURCE_ID` TINYINT NOT NULL,
    `WRITER_ID` SMALLINT,
    `SERVER_NAME` VARCHAR(50) NOT NULL,
    FOREIGN KEY (PRODUCT_ID)
        REFERENCES anycapital.PRODUCTS (id)
        ON DELETE CASCADE,
    FOREIGN KEY (PRODUCTS_HISTORY_ACTION_ID)
        REFERENCES anycapital.PRODUCTS_HISTORY_ACTIONS (id)
        ON DELETE CASCADE,
    FOREIGN KEY (ACTION_SOURCE_ID)
        REFERENCES anycapital.ACTION_SOURCE (id)
        ON DELETE CASCADE,
    FOREIGN KEY (WRITER_ID)
        REFERENCES anycapital.WRITERS (id)
        ON DELETE CASCADE,
    PRIMARY KEY (`PRODUCT_ID` , `TIME_CREATED`)
); 
-- ------------------------------------------------------
--  DDL for Table INVESTMENT_TYPES
-- ------------------------------------------------------
CREATE TABLE anycapital.INVESTMENT_TYPES (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT,
    `NAME` VARCHAR(20) NOT NULL,
    `DISPLAY_NAME` VARCHAR(50) NOT NULL,
    PRIMARY KEY (ID)
);
-- INSERTING into anycapital.INVESTMENT_TYPES
Insert into anycapital.INVESTMENT_TYPES (ID,NAME,DISPLAY_NAME) values (1,'subscription','investment.types.1');
Insert into anycapital.INVESTMENT_TYPES (ID,NAME,DISPLAY_NAME) values (2,'secondary','investment.types.2');
-- ------------------------------------------------------
--  DDL for Table INVESTMENT_STATUSES
-- ------------------------------------------------------
CREATE TABLE anycapital.INVESTMENT_STATUSES (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT,
    `NAME` VARCHAR(40) NOT NULL,
    `DISPLAY_NAME` VARCHAR(50) NOT NULL,
    PRIMARY KEY (ID)
);
-- INSERTING into anycapital.INVESTMENT_STATUSES
Insert into anycapital.INVESTMENT_STATUSES (ID,NAME,DISPLAY_NAME) values (1,'pending','investment.statuses.1');
Insert into anycapital.INVESTMENT_STATUSES (ID,NAME,DISPLAY_NAME) values (2,'open','investment.statuses.2');
Insert into anycapital.INVESTMENT_STATUSES (ID,NAME,DISPLAY_NAME) values (3,'settled','investment.statuses.3');
Insert into anycapital.INVESTMENT_STATUSES (ID,NAME,DISPLAY_NAME) values (4,'canceled','investment.statuses.4');
Insert into anycapital.INVESTMENT_STATUSES (ID,NAME,DISPLAY_NAME) values (5,'canceled insufficient funds','investment.statuses.5');
-- ------------------------------------------------------
--  DDL for Table INVESTMENT_REJECT_TYPES
-- ------------------------------------------------------
CREATE TABLE anycapital.INVESTMENT_REJECT_TYPES (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT,
    `DESCRIPTION` VARCHAR(70) NOT NULL,
    PRIMARY KEY (ID)
);
-- INSERTING into anycapital.INVESTMENT_REJECT_TYPES
Insert into anycapital.INVESTMENT_REJECT_TYPES (ID,DESCRIPTION) values (6,'Insufficient balance more then min inv');
Insert into anycapital.INVESTMENT_REJECT_TYPES (ID,DESCRIPTION) values (7,'Insufficient balance less then min inv');
Insert into anycapital.INVESTMENT_REJECT_TYPES (ID,DESCRIPTION) values (8,'investment praimery product secondary');
Insert into anycapital.INVESTMENT_REJECT_TYPES (ID,DESCRIPTION) values (9,'Limit of one pending investment per product in primary');
Insert into anycapital.INVESTMENT_REJECT_TYPES (ID,DESCRIPTION) values (1,'Product Suspended');
Insert into anycapital.INVESTMENT_REJECT_TYPES (ID,DESCRIPTION) values (2,'Deviation ask');
Insert into anycapital.INVESTMENT_REJECT_TYPES (ID,DESCRIPTION) values (3,'MIN Inv Limit');
Insert into anycapital.INVESTMENT_REJECT_TYPES (ID,DESCRIPTION) values (4,'MAX Inv Limit');
Insert into anycapital.INVESTMENT_REJECT_TYPES (ID,DESCRIPTION) values (5,'Exposure');
Insert into anycapital.INVESTMENT_REJECT_TYPES (ID,DESCRIPTION) values (11,'investment secondary product praimery');
Insert into anycapital.INVESTMENT_REJECT_TYPES (ID,DESCRIPTION) values (10,'Product is currently unavailable');
Insert into anycapital.INVESTMENT_REJECT_TYPES (ID,DESCRIPTION) values (12,'User not regulated');
-- ------------------------------------------------------
--  DDL for Table INVESTMENT_POSSIBILITIES
-- ------------------------------------------------------
CREATE TABLE anycapital.INVESTMENT_POSSIBILITIES (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT,
    `CURRENCY_ID` SMALLINT NOT NULL,
    `MINIMUM_AMOUNT` BIGINT NOT NULL COMMENT 'the minimum amount to invest in one investment',
    `MAXIMUM_AMOUNT` BIGINT NOT NULL COMMENT 'the maximum amount to invest in one investment',
    `DEFAULT_AMOUNT` BIGINT NOT NULL COMMENT 'the defualt amount to show in the amount input',
    FOREIGN KEY (CURRENCY_ID)
        REFERENCES anycapital.CURRENCIES (id)
        ON DELETE CASCADE,
    PRIMARY KEY (ID)
);
-- INSERTING into anycapital.INVESTMENT_POSSIBILITIES
Insert into anycapital.INVESTMENT_POSSIBILITIES (ID,CURRENCY_ID,MINIMUM_AMOUNT,MAXIMUM_AMOUNT,DEFAULT_AMOUNT) values (1,2,500000,99999900,1000000);
Insert into anycapital.INVESTMENT_POSSIBILITIES (ID,CURRENCY_ID,MINIMUM_AMOUNT,MAXIMUM_AMOUNT,DEFAULT_AMOUNT) values (2,3,300000,99999900,1000000);
-- ------------------------------------------------------
--  DDL for Table INVESTMENT_POSSIBILITY_TABS
-- ------------------------------------------------------
CREATE TABLE anycapital.INVESTMENT_POSSIBILITY_TABS (
    `INVESTMENT_POSSIBILITIES_ID` SMALLINT NOT NULL,
    `AMOUNT` BIGINT NOT NULL COMMENT 'the amount to show on the button',
    `POSITION` SMALLINT NOT NULL COMMENT 'the position of the button',
    FOREIGN KEY (INVESTMENT_POSSIBILITIES_ID)
        REFERENCES anycapital.INVESTMENT_POSSIBILITIES (id)
        ON DELETE CASCADE,
    PRIMARY KEY (`INVESTMENT_POSSIBILITIES_ID` , `AMOUNT`)
); 
-- INSERTING into anycapital.INVESTMENT_POSSIBILITY_TABS
Insert into anycapital.INVESTMENT_POSSIBILITY_TABS (INVESTMENT_POSSIBILITIES_ID,AMOUNT,POSITION) values (1,500000,1);
Insert into anycapital.INVESTMENT_POSSIBILITY_TABS (INVESTMENT_POSSIBILITIES_ID,AMOUNT,POSITION) values (1,2000000,2);
Insert into anycapital.INVESTMENT_POSSIBILITY_TABS (INVESTMENT_POSSIBILITIES_ID,AMOUNT,POSITION) values (1,5000000,3);
Insert into anycapital.INVESTMENT_POSSIBILITY_TABS (INVESTMENT_POSSIBILITIES_ID,AMOUNT,POSITION) values (2,300000,1);
Insert into anycapital.INVESTMENT_POSSIBILITY_TABS (INVESTMENT_POSSIBILITIES_ID,AMOUNT,POSITION) values (2,1500000,2);
Insert into anycapital.INVESTMENT_POSSIBILITY_TABS (INVESTMENT_POSSIBILITIES_ID,AMOUNT,POSITION) values (2,3000000,3);
-- ------------------------------------------------------
--  DDL for Table INVESTMENTS
-- ------------------------------------------------------
CREATE TABLE anycapital.INVESTMENTS (
    `ID` BIGINT NOT NULL AUTO_INCREMENT,
    `USER_ID` BIGINT NOT NULL,
    `PRODUCT_ID` BIGINT NOT NULL,
    `INVESTMENT_STATUS_ID` SMALLINT NOT NULL,
    `AMOUNT` BIGINT,
    `RETURN_AMOUNT` BIGINT COMMENT 'the amount that will retun to the user balance after settlement',
    `INVESTMENT_TYPE_ID` SMALLINT NOT NULL,
    `ASK` DOUBLE,
    `BID` DOUBLE,
    `RATE` FLOAT NOT NULL COMMENT 'rate to convert amount to usd',
    `TIME_CREATED` TIMESTAMP(3) DEFAULT NOW(3) NOT NULL,
    `ORIGINAL_AMOUNT` BIGINT NOT NULL COMMENT 'The amount in product currency',
    `ORIGINAL_RETURN_AMOUNT` BIGINT COMMENT 'The return amount in product currency',
    FOREIGN KEY (USER_ID)
        REFERENCES anycapital.USERS (id)
        ON DELETE CASCADE,
    FOREIGN KEY (PRODUCT_ID)
        REFERENCES anycapital.PRODUCTS (id)
        ON DELETE CASCADE,
    FOREIGN KEY (INVESTMENT_STATUS_ID)
        REFERENCES anycapital.INVESTMENT_STATUSES (id)
        ON DELETE CASCADE,
    FOREIGN KEY (INVESTMENT_TYPE_ID)
        REFERENCES anycapital.INVESTMENT_TYPES (id)
        ON DELETE CASCADE,
    PRIMARY KEY (ID)
); 

ALTER TABLE `anycapital`.`investments` 
CHANGE COLUMN `AMOUNT` `AMOUNT` BIGINT(20) NULL DEFAULT NULL COMMENT 'amonut in EUR' ;

-- ------------------------------------------------------
--  DDL for Table INVESTMENTS_HISTORY
-- ------------------------------------------------------
CREATE TABLE anycapital.INVESTMENTS_HISTORY (
    `INVESTMNT_ID` BIGINT NOT NULL,
    `INVESTMENT_STATUS_ID` SMALLINT NOT NULL,
    `TIME_CREATED` TIMESTAMP(3) DEFAULT NOW(3) NOT NULL,
    `WRITER_ID` SMALLINT,
    `ACTION_SOURCE_ID` TINYINT NOT NULL,
    `SERVER_NAME` VARCHAR(50) NOT NULL,
    `LOGIN_ID` BIGINT COMMENT 'the login id of the user that done that action - null if its job/backend',
    `IP` VARCHAR(20) COMMENT 'the  ip of the user that done that action - null if its job',
    FOREIGN KEY (INVESTMNT_ID)
        REFERENCES anycapital.INVESTMENTS (id)
        ON DELETE CASCADE,
    FOREIGN KEY (INVESTMENT_STATUS_ID)
        REFERENCES anycapital.INVESTMENT_STATUSES (id)
        ON DELETE CASCADE,
    FOREIGN KEY (WRITER_ID)
        REFERENCES anycapital.WRITERS (id)
        ON DELETE CASCADE,
    FOREIGN KEY (ACTION_SOURCE_ID)
        REFERENCES anycapital.ACTION_SOURCE (id)
        ON DELETE CASCADE,
    FOREIGN KEY (LOGIN_ID)
        REFERENCES anycapital.LOGINS (id)
        ON DELETE CASCADE,
    PRIMARY KEY (`INVESTMNT_ID` , `INVESTMENT_STATUS_ID`)
); 
-- ------------------------------------------------------
--  DDL for Table INVESTMENT_MARKETS
-- ------------------------------------------------------
CREATE TABLE anycapital.INVESTMENT_MARKETS (
    `INVESTMENT_ID` BIGINT NOT NULL,
    `MARKET_ID` SMALLINT NOT NULL,
    `PRICE` DOUBLE NOT NULL COMMENT 'the price of the market when user insert investment',
    FOREIGN KEY (MARKET_ID)
        REFERENCES anycapital.MARKETS (id)
        ON DELETE CASCADE,
    FOREIGN KEY (INVESTMENT_ID)
        REFERENCES anycapital.INVESTMENTS (id)
        ON DELETE CASCADE,        
    PRIMARY KEY (`INVESTMENT_ID` , `MARKET_ID`)
);
-- ------------------------------------------------------
--  DDL for Table INVESTMENT_REJECTS
-- ------------------------------------------------------
CREATE TABLE anycapital.INVESTMENT_REJECTS (
    `ID` BIGINT NOT NULL AUTO_INCREMENT,
    `USER_ID` BIGINT NOT NULL,
    `PRODUCT_ID` BIGINT NOT NULL,
    `INVESTMENT_REJECT_TYPE_ID` SMALLINT NOT NULL,
    `SESSION_ID` VARCHAR(50),
    `ASK` DOUBLE,
    `BID` DOUBLE,
    `AMOUNT` BIGINT NOT NULL,
    `REJECT_ADDITIONAL_INFO` VARCHAR(200) COMMENT 'This column should be populated with different info for different rejects.',
    `TIME_CREATED` TIMESTAMP(3) DEFAULT NOW(3) NOT NULL,
    `ACTION_SOURCE_ID` TINYINT NOT NULL,
    `WRITER_ID` SMALLINT COMMENT 'we can have investments from backend writer',
    `RATE` FLOAT NOT NULL COMMENT 'rate to convert investment amount to dollar',
    `INVESTMENT_TYPE_ID` SMALLINT NOT NULL,
    `SERVER_NAME` VARCHAR(50) NOT NULL,
    FOREIGN KEY (USER_ID)
        REFERENCES anycapital.USERS (id)
        ON DELETE CASCADE,
    FOREIGN KEY (PRODUCT_ID)
        REFERENCES anycapital.PRODUCTS (id)
        ON DELETE CASCADE,
    FOREIGN KEY (INVESTMENT_REJECT_TYPE_ID)
        REFERENCES anycapital.INVESTMENT_REJECT_TYPES (id)
        ON DELETE CASCADE,
    FOREIGN KEY (INVESTMENT_TYPE_ID)
        REFERENCES anycapital.INVESTMENT_TYPES (id)
        ON DELETE CASCADE,
    FOREIGN KEY (ACTION_SOURCE_ID)
        REFERENCES anycapital.ACTION_SOURCE (id)
        ON DELETE CASCADE,
    FOREIGN KEY (WRITER_ID)
        REFERENCES anycapital.WRITERS (id)
        ON DELETE CASCADE,
    PRIMARY KEY (ID)
); 
-- ------------------------------------------------------
--  DDL for Table PRODUCT_AUTOCALLS
-- ------------------------------------------------------
CREATE TABLE anycapital.PRODUCT_AUTOCALLS (
    `ID` BIGINT NOT NULL AUTO_INCREMENT,
    `PRODUCT_ID` BIGINT NOT NULL,
    `EXAMINATION_DATE` TIMESTAMP NOT NULL,
    `EXAMINATION_VALUE` DOUBLE NOT NULL COMMENT 'if this trigger is true we settle the product',
    FOREIGN KEY (PRODUCT_ID)
        REFERENCES anycapital.PRODUCTS (id)
        ON DELETE CASCADE,
    PRIMARY KEY (ID)
);
 ALTER TABLE anycapital.PRODUCT_AUTOCALLS  COMMENT 'in this dates if the condition is true we settle the product';
-- ------------------------------------------------------
--  DDL for Table PRODUCT_CALLABLE
-- ------------------------------------------------------
CREATE TABLE anycapital.PRODUCT_CALLABLE (
    `ID` BIGINT NOT NULL AUTO_INCREMENT,
    `PRODUCT_ID` BIGINT NOT NULL,
    `EXAMINATION_DATE` TIMESTAMP NOT NULL,
    FOREIGN KEY (PRODUCT_ID)
        REFERENCES anycapital.PRODUCTS (id)
        ON DELETE CASCADE,
	UNIQUE (`PRODUCT_ID`, `EXAMINATION_DATE`),         
    PRIMARY KEY (ID)
);
ALTER TABLE anycapital.PRODUCT_CALLABLE  COMMENT 'in this dates issuer can settle the product';
-- ------------------------------------------------------
--  DDL for Table TRANSACTION_COUPONS
-- ------------------------------------------------------
CREATE TABLE ANYCAPITAL.TRANSACTION_COUPONS (
    `TRANSACTION_ID` BIGINT NOT NULL,
    `AMOUNT` BIGINT NOT NULL COMMENT 'amount of the transaction in product currency',
    `PRODUCT_COUPON_ID` BIGINT NOT NULL COMMENT 'the product coupon id that create this transaction',
    FOREIGN KEY (TRANSACTION_ID)
        REFERENCES anycapital.TRANSACTIONS (id)
        ON DELETE CASCADE,
    FOREIGN KEY (PRODUCT_COUPON_ID)
        REFERENCES anycapital.PRODUCT_COUPONS (id)
        ON DELETE CASCADE,
    PRIMARY KEY (TRANSACTION_ID)
);
-- ------------------------------------------------------
--  DDL for Table PROVIDER_REQUEST_RESPONSE
-- ------------------------------------------------------
CREATE TABLE anycapital.PROVIDER_REQUEST_RESPONSE (   
    `TABLE_ID` SMALLINT NOT NULL COMMENT 'Reference to `tables` table',
    `REFERENCE_ID` BIGINT COMMENT 'the id on the table',
    `REQUEST` LONGTEXT COMMENT 'request to provider',
    `RESPONSE` LONGTEXT COMMENT 'response to provider',    
    `TIME_CREATED` TIMESTAMP(3) DEFAULT NOW(3),
    `TIME_UPDATED` TIMESTAMP NULL COMMENT '',
    FOREIGN KEY (TABLE_ID)
        REFERENCES anycapital.TABLES (id)
        ON DELETE CASCADE,
    PRIMARY KEY (TIME_CREATED)
);

---------------------------------------------------------
-- SP-12 [Technical][General] - A/B testing
-- Eyal O
---------------------------------------------------------
ALTER TABLE `anycapital`.`marketing_tracking` 
ADD COLUMN `AB_NAME` VARCHAR(45) NULL COMMENT 'The name of AB tesing.' AFTER `QUERY_STRING`,
ADD COLUMN `AB_VALUE` VARCHAR(45) NULL COMMENT 'The value of AB testing.' AFTER `AB_NAME`;
---------------------------------------------------------
-- END
-- SP-12 [Technical][General] - A/B testing
-- Eyal O
---------------------------------------------------------

-----------------------------------------------------------
--email groups
-----------------------------------------------------------
CREATE TABLE `anycapital`.`email_groups` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `NAME` VARCHAR(45) NOT NULL,
  `EMAIL` VARCHAR(60) NOT NULL,
  `DESCRIPTION` VARCHAR(45) NULL,
  PRIMARY KEY (`ID`),
  UNIQUE INDEX `NAME_UNIQUE` (`NAME` ASC),
  UNIQUE INDEX `EMAIL_UNIQUE` (`EMAIL` ASC))
COMMENT = 'groups for sending emails';

INSERT INTO `anycapital`.`email_groups`
(
`NAME`,
`EMAIL`,
`DESCRIPTION`)
VALUES(
'traders',
'traders@anycapital.com',
'email for trade related emails');

INSERT INTO `anycapital`.`email_groups`
(
`NAME`,
`EMAIL`,
`DESCRIPTION`)
VALUES(
'daily report',
'Daily@anycapital.com',
'daily report group');


---------------------------------------------------------
-- questionnaire
-- Eyal O
---------------------------------------------------------

CREATE TABLE `anycapital`.`qm_question_types` (
  `id` SMALLINT NOT NULL AUTO_INCREMENT COMMENT 'The question\'s type id.',
  `description` VARCHAR(45) NOT NULL COMMENT 'The description\'s question type.',
  PRIMARY KEY (`id`));
  
INSERT INTO `anycapital`.`qm_question_types` (`description`) VALUES ('Drop down');
INSERT INTO `anycapital`.`qm_question_types` (`description`) VALUES ('Checkbox');
INSERT INTO `anycapital`.`qm_question_types` (`description`) VALUES ('Input');
  
-- ???
CREATE TABLE `anycapital`.`qm_answer_types` (
  `id` SMALLINT NOT NULL AUTO_INCREMENT COMMENT 'The answer\'s type id.',
  `description` VARCHAR(45) NOT NULL COMMENT 'The description\'s answer type.',
  PRIMARY KEY (`id`));
  
CREATE TABLE `anycapital`.`qm_questions` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT 'The question\'s id.',
  `name` VARCHAR(100) NOT NULL COMMENT 'The question\'s key will be replaced by text\'s languages.',
  `is_active` BINARY NOT NULL DEFAULT 1 COMMENT 'Represent if the record is active or not.',
  `order_id` INT NOT NULL COMMENT 'The orders of the questions.',
  `question_type_id` SMALLINT NOT NULL COMMENT 'Represent the question\'s type.',
  PRIMARY KEY (`id`),
  INDEX `id_idx` (`question_type_id` ASC),
  CONSTRAINT ``
    FOREIGN KEY (`question_type_id`)
    REFERENCES `anycapital`.`qm_question_types` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION);
    
CREATE TABLE `anycapital`.`qm_answers` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT 'The answer\'s id.',
  `name` VARCHAR(100) NOT NULL COMMENT 'The question\'s key will be replaced by text\'s languages.',
  `question_id` INT NOT NULL COMMENT 'Represent the question\'s id.',
  `order_id` INT NOT NULL COMMENT 'The orders of the answers.',
  PRIMARY KEY (`id`),
  INDEX `qm_answers_fk_1_idx` (`question_id` ASC),
  CONSTRAINT `qm_answers_fk_1`
    FOREIGN KEY (`question_id`)
    REFERENCES `anycapital`.`qm_questions` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION);

CREATE TABLE `anycapital`.`qm_user_answers` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT 'User\'s answer id.',
  `user_id` INT NOT NULL COMMENT 'Represent the user id which did action.',
  `question_id` INT NOT NULL COMMENT 'Represent the question id.',
  `answer_id` INT NULL COMMENT 'Represent the answer id.',
  `text_answer` INT NULL COMMENT 'Used for answer by free input text.',
  `action_source_id` TINYINT NOT NULL COMMENT 'Reference to action source id which action made of.',
  `time_created` TIMESTAMP NOT NULL COMMENT 'The time which user answered.',
  PRIMARY KEY (`id`),
  INDEX `qm_user_answers_fk_2_idx` (`question_id` ASC),
  INDEX `qm_user_answers_fk_3_idx` (`answer_id` ASC),
  INDEX `qm_user_answers_fk_4_idx` (`action_source_id` ASC),
  CONSTRAINT `qm_user_answers_fk_2`
    FOREIGN KEY (`question_id`)
    REFERENCES `anycapital`.`qm_questions` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `qm_user_answers_fk_3`
    FOREIGN KEY (`answer_id`)
    REFERENCES `anycapital`.`qm_answers` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `qm_user_answers_fk_4`
    FOREIGN KEY (`action_source_id`)
    REFERENCES `anycapital`.`action_source` (`ID`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION);

ALTER TABLE `anycapital`.`qm_user_answers` 
CHANGE COLUMN `id` `id` BIGINT(20) NOT NULL COMMENT 'User\'s answer id.' ;

ALTER TABLE `anycapital`.`qm_user_answers` 
CHANGE COLUMN `user_id` `user_id` BIGINT(20) NOT NULL COMMENT 'Represent the user id which did action.' ;

ALTER TABLE `anycapital`.`qm_user_answers` 
CHANGE COLUMN `text_answer` `text_answer` VARCHAR(200) NULL DEFAULT NULL COMMENT 'Used for answer by free input text.' ;

ALTER TABLE `anycapital`.`qm_user_answers` 
CHANGE COLUMN `id` `id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT 'User\'s answer id.' ;


-- foreign key to users ???
    
---------------------------------------------------------
-- questionnaire
-- Eyal O
---------------------------------------------------------

---------------------------------------------------------
-- SP-81 [Technical][General] - Issues handling to user
-- Eyal O
---------------------------------------------------------

CREATE TABLE `anycapital`.`issue_channels` (
  `id` SMALLINT NOT NULL COMMENT 'The channel\'s id.',
  `name` VARCHAR(45) NOT NULL COMMENT 'The channel\'s key will replaced by text\'s languages.',
  PRIMARY KEY (`id`));
  
ALTER TABLE `anycapital`.`issue_channels` 
CHANGE COLUMN `id` `id` SMALLINT(6) NOT NULL AUTO_INCREMENT COMMENT 'The channel\'s id.' ;
  
CREATE TABLE `anycapital`.`issue_subjects` (
  `id` SMALLINT NOT NULL COMMENT 'The subject\'s id.',
  `name` VARCHAR(45) NOT NULL COMMENT 'The subject\'s key.',
  PRIMARY KEY (`id`));

ALTER TABLE `anycapital`.`issue_subjects` 
CHANGE COLUMN `id` `id` SMALLINT(6) NOT NULL AUTO_INCREMENT COMMENT 'The subject\'s id.' ;
  
CREATE TABLE `anycapital`.`issue_directions` (
  `id` SMALLINT NOT NULL COMMENT 'The direction\'s id.',
  `name` VARCHAR(45) NOT NULL COMMENT 'The direction\'s key.',
  PRIMARY KEY (`id`));
  
ALTER TABLE `anycapital`.`issue_directions` 
CHANGE COLUMN `id` `id` SMALLINT(6) NOT NULL AUTO_INCREMENT COMMENT 'The direction\'s id.' ;

CREATE TABLE `anycapital`.`issue_reached_statuses` (
  `id` SMALLINT NOT NULL AUTO_INCREMENT COMMENT 'The reached status\'s id.',
  `name` VARCHAR(45) NOT NULL COMMENT 'The reached status\'s key.',
  PRIMARY KEY (`id`));

CREATE TABLE `anycapital`.`issue_reactions` (
  `id` SMALLINT NOT NULL AUTO_INCREMENT COMMENT 'The reaction\'s id.',
  `name` VARCHAR(45) NOT NULL COMMENT 'The reaction\'s key.',
  `reached_status_id` SMALLINT NOT NULL COMMENT 'Foreign key to issue_reached_statuses',
  PRIMARY KEY (`id`),
  INDEX `issue_reactions_fk_1_idx` (`reached_status_id` ASC),
  CONSTRAINT `issue_reactions_fk_1`
    FOREIGN KEY (`reached_status_id`)
    REFERENCES `anycapital`.`issue_reached_statuses` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION);

CREATE TABLE `anycapital`.`issues` (
  `id` SMALLINT NOT NULL AUTO_INCREMENT COMMENT 'The issue\'s id.',
  `channel_id` SMALLINT NOT NULL COMMENT 'Foreign key to issue_channels',
  `subject_id` SMALLINT NOT NULL COMMENT 'Foreign key to issue_subjects',
  `direction_id` SMALLINT NULL COMMENT 'Foreign key to issue_directions',
  `reached_status_id` SMALLINT NULL COMMENT 'Foreign key to issue_reached_statuses',
  `reaction_id` SMALLINT NULL COMMENT 'Foreign key to issue_reactions',
  `comments` VARCHAR(100) NULL COMMENT 'Any comment regarding to the issue.',
  `significant_note` BINARY NOT NULL COMMENT 'Represent if it\'s significant note or not.',
  `time_created` TIMESTAMP NOT NULL COMMENT 'The record\'s time created.',
  PRIMARY KEY (`id`),
  INDEX `issues_fk_1_idx` (`channel_id` ASC),
  INDEX `issues_fk_2_idx` (`subject_id` ASC),
  INDEX `issues_fk_3_idx` (`direction_id` ASC),
  INDEX `issues_fk_4_idx` (`reached_status_id` ASC),
  INDEX `issues_fk_5_idx` (`reaction_id` ASC),
  CONSTRAINT `issues_fk_1`
    FOREIGN KEY (`channel_id`)
    REFERENCES `anycapital`.`issue_channels` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `issues_fk_2`
    FOREIGN KEY (`subject_id`)
    REFERENCES `anycapital`.`issue_subjects` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `issues_fk_3`
    FOREIGN KEY (`direction_id`)
    REFERENCES `anycapital`.`issue_directions` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `issues_fk_4`
    FOREIGN KEY (`reached_status_id`)
    REFERENCES `anycapital`.`issue_reached_statuses` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `issues_fk_5`
    FOREIGN KEY (`reaction_id`)
    REFERENCES `anycapital`.`issue_reactions` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION);
    
ALTER TABLE `anycapital`.`issues` 
ADD COLUMN `user_id` INT NOT NULL AFTER `time_created`,
ADD COLUMN `writer_id` INT NOT NULL AFTER `user_id`;

-- user id & writer id should be a foreign key ???
  
---------------------------------------------------------
-- End
-- SP-81 [Technical][General] - Issues handling to user
-- Eyal O
---------------------------------------------------------


------
--LioR SoLoMoN
ALTER TABLE `anycapital`.`wires` 
ADD COLUMN `BRANCH_NUMBER` INT(15) NULL DEFAULT NULL AFTER `BANK_FEE_AMOUNT`;

---------------------------------------------------------
-- SP-391 - Registration + Questionnaire
-- Eyal O
---------------------------------------------------------

CREATE TABLE `anycapital`.`regulation_questionnaire_statuses` (
  `id` SMALLINT NOT NULL AUTO_INCREMENT COMMENT 'The questionnaire\'s status id',
  `name` VARCHAR(45) NOT NULL COMMENT 'The questionnaire\'s status name',
  `display_name` VARCHAR(45) NOT NULL COMMENT 'The questionnaire\'s status key',
  PRIMARY KEY (`id`))
COMMENT = 'Represent the regulation status of the questionnaire (for example if user passed the questionnaire according to regulation process)';

CREATE TABLE `anycapital`.`regulation_user_info` (
  `id` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'The user info\'s id.',
  `user_id` BIGINT NOT NULL COMMENT 'Related to the table USERS',
  `regulation_questionnaire_status_id` SMALLINT NOT NULL COMMENT 'Foreign key to regulation_questionnaire_statuses',
  `country_id` SMALLINT(6) NULL COMMENT 'Foreign key to countries',
  `time_created` TIMESTAMP(3) NOT NULL DEFAULT NOW(3) COMMENT 'The time which user get into regulation process (after user filled questionnaire).',
  `time_modified` TIMESTAMP NULL COMMENT 'The record\'s time modified.',
  PRIMARY KEY (`id`),
  INDEX `regulation_user_info_fk_1_idx` (`user_id` ASC),
  INDEX `regulation_user_info_fk_2_idx` (`regulation_questionnaire_status_id` ASC),
  INDEX `regulation_user_info_fk_3_idx` (`country_id` ASC),
  CONSTRAINT `regulation_user_info_fk_1`
    FOREIGN KEY (`user_id`)
    REFERENCES `anycapital`.`users` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `regulation_user_info_fk_2`
    FOREIGN KEY (`regulation_questionnaire_status_id`)
    REFERENCES `anycapital`.`regulation_questionnaire_statuses` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `regulation_user_info_fk_3`
    FOREIGN KEY (`country_id`)
    REFERENCES `anycapital`.`countries` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
COMMENT = 'Represent user\' s additional information regarding to regulation process.';

ALTER TABLE `anycapital`.`regulation_user_info` 
ADD COLUMN `writer_id` SMALLINT(6) NULL COMMENT 'Foreign key to writers.' AFTER `time_modified`,
ADD INDEX `regulation_user_info_fk_4_idx` (`writer_id` ASC);
ALTER TABLE `anycapital`.`regulation_user_info` 
ADD CONSTRAINT `regulation_user_info_fk_4`
  FOREIGN KEY (`writer_id`)
  REFERENCES `anycapital`.`writers` (`ID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

---------------------------------------------------------
-- END
-- SP-391 - Registration + Questionnaire
-- Eyal O
---------------------------------------------------------


---------------------------------------------------------
-- start
-- SP-434 - [Server] - Files (Documents)
-- eyal G
---------------------------------------------------------

CREATE TABLE `anycapital`.`file_types` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `display_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  UNIQUE INDEX `display_name_UNIQUE` (`display_name` ASC))
COMMENT = 'files type user can have';

CREATE TABLE `anycapital`.`files` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(200) NOT NULL,
  `file_type_id` INT NOT NULL,
  `user_id` BIGINT(20) NOT NULL,
  `time_created` TIMESTAMP(3) NOT NULL DEFAULT NOW(3),
  `action_source_id` TINYINT(4) NOT NULL,
  `writer_id` SMALLINT(4) NULL,
  PRIMARY KEY (`id`),
  INDEX `users_fk_idx` (`user_id` ASC),
  INDEX `file_type_fk_idx` (`file_type_id` ASC),
  INDEX `action_source_fk_idx` (`action_source_id` ASC),
  INDEX `writer_fk_idx` (`writer_id` ASC),
  CONSTRAINT `users_files_fk`
    FOREIGN KEY (`user_id`)
    REFERENCES `anycapital`.`users` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `file_type_files_fk`
    FOREIGN KEY (`file_type_id`)
    REFERENCES `anycapital`.`file_types` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `action_source_files_fk`
    FOREIGN KEY (`action_source_id`)
    REFERENCES `anycapital`.`action_source` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `writer_files_fk`
    FOREIGN KEY (`writer_id`)
    REFERENCES `anycapital`.`writers` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

ALTER TABLE `anycapital`.`files` 
ADD COLUMN `is_primary` BINARY(1) NULL DEFAULT 0 COMMENT '1 if this file is the most update file from this type to this user' AFTER `writer_id`;


INSERT INTO `anycapital`.`file_types` (`name`, `display_name`) VALUES ('ID', 'file_type_1');
INSERT INTO `anycapital`.`file_types` (`name`, `display_name`) VALUES ('Driver\'s License', 'file_type_2');
INSERT INTO `anycapital`.`file_types` (`name`, `display_name`) VALUES ('Passport', 'file_type_3');
INSERT INTO `anycapital`.`file_types` (`name`, `display_name`) VALUES ('Utility Bill', 'file_type_4');
INSERT INTO `anycapital`.`file_types` (`name`, `display_name`) VALUES ('Other', 'file_type_5');

    
    
---------------------------------------------------------
-- end
-- SP-434 - [Server] - Files (Documents)
-- eyal G
----------------------------------------------------------------------------------------------------
--LioR SoLoMoN
--05.04.2017
--SP-447 [Technical][General] - user step

CREATE TABLE anycapital.USER_STEPS (
    `ID` SMALLINT NOT NULL AUTO_INCREMENT,
    `DISPLAY_NAME` VARCHAR(20) NOT NULL,
    `NAME` VARCHAR(20) NOT NULL,
    PRIMARY KEY (ID)
);

Insert into anycapital.USER_STEPS (id,DISPLAY_NAME,NAME) values (1,'user.step.1','new');
Insert into anycapital.USER_STEPS (id,DISPLAY_NAME,NAME) values (2,'user.step.2','update');
Insert into anycapital.USER_STEPS (id,DISPLAY_NAME,NAME) values (3,'user.step.3','questionnaire');


ALTER TABLE `anycapital`.`users` 
ADD COLUMN `STEP` SMALLINT(6) NOT NULL DEFAULT 1 AFTER `VIP_STATUS_ID`,
ADD INDEX `users_step_fk_idx` (`STEP` ASC);
ALTER TABLE `anycapital`.`users` 
ADD CONSTRAINT `users_step_fk`
  FOREIGN KEY (`STEP`)
  REFERENCES `anycapital`.`user_steps` (`ID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
  
ALTER TABLE `anycapital`.`users` 
DROP FOREIGN KEY `users_step_fk`;
ALTER TABLE `anycapital`.`users` 
CHANGE COLUMN `STEP` `STEP` SMALLINT(6) NOT NULL DEFAULT '1' COMMENT 'The step the user finish' ;
ALTER TABLE `anycapital`.`users` 
ADD CONSTRAINT `users_step_fk`
  FOREIGN KEY (`STEP`)
  REFERENCES `anycapital`.`user_steps` (`ID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
--LioR SoLoMoN
--09.04.2017
--SP-445 [Technical][General] - countries handling

ALTER TABLE `anycapital`.`languages` 
ADD COLUMN `DEFAULT_COUNTRY_ID` SMALLINT(6) NULL AFTER `DISPLAY_NAME`,
ADD INDEX `languages_fk_country_idx` (`DEFAULT_COUNTRY_ID` ASC);
ALTER TABLE `anycapital`.`languages` 
ADD CONSTRAINT `languages_fk_country`
  FOREIGN KEY (`DEFAULT_COUNTRY_ID`)
  REFERENCES `anycapital`.`countries` (`ID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


UPDATE `anycapital`.`languages` SET `DEFAULT_COUNTRY_ID`='226' WHERE `ID`='2';
UPDATE `anycapital`.`languages` SET `DEFAULT_COUNTRY_ID`='86' WHERE `ID`='8';


ALTER TABLE `anycapital`.`countries` 
ADD COLUMN `DEFAULT_LANGUAGE_ID` SMALLINT(6) NULL AFTER `GMT_OFFSET`,
ADD INDEX `country_fk_language_idx` (`DEFAULT_LANGUAGE_ID` ASC);
ALTER TABLE `anycapital`.`countries` 
ADD CONSTRAINT `country_fk_language`
  FOREIGN KEY (`DEFAULT_LANGUAGE_ID`)
  REFERENCES `anycapital`.`languages` (`ID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;



UPDATE anycapital.countries c 
SET 
    c.DEFAULT_LANGUAGE_ID = 2
WHERE
    c.A2 != 'DE'; 
    
    
    
UPDATE anycapital.countries c 
SET 
    c.DEFAULT_LANGUAGE_ID = 8
WHERE
    c.A2 = 'DE'; 


ALTER TABLE `anycapital`.`users` 
ADD COLUMN `COUNTRY_BY_USER` SMALLINT(6) NULL AFTER `STEP`,
ADD COLUMN `COUNTRY_BY_IP` SMALLINT(6) NULL AFTER `COUNTRY_BY_USER`,
ADD COLUMN `COUNTRY_BY_PREFIX` SMALLINT(6) NULL AFTER `COUNTRY_BY_IP`,
ADD INDEX `users_fk_country_user_idx` (`COUNTRY_BY_USER` ASC),
ADD INDEX `users_fk_country_ip_idx` (`COUNTRY_BY_IP` ASC),
ADD INDEX `users_fk_country_prefix_idx` (`COUNTRY_BY_PREFIX` ASC);
ALTER TABLE `anycapital`.`users` 
ADD CONSTRAINT `users_fk_country_user`
  FOREIGN KEY (`COUNTRY_BY_USER`)
  REFERENCES `anycapital`.`countries` (`ID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `users_fk_country_ip`
  FOREIGN KEY (`COUNTRY_BY_IP`)
  REFERENCES `anycapital`.`countries` (`ID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `users_fk_country_prefix`
  FOREIGN KEY (`COUNTRY_BY_PREFIX`)
  REFERENCES `anycapital`.`countries` (`ID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

----------------------------------------------------------------------------------------------------

---------------------------------------------------------
-- SP-454 [Technical][Regulation] - update user step to approve
-- Eyal O
---------------------------------------------------------
    
update
	user_steps us
set
	us.NAME = 'missing'
where
	us.id = 3;
	
---------------------------------------------------------
-- END
-- SP-454 [Technical][Regulation] - update user step to approve
-- Eyal O
---------------------------------------------------------
	
---------------------------------------------------------
-- SP-452 [Technical][Regulation] - Add country regulation risk identification
-- Eyal O
---------------------------------------------------------

CREATE TABLE `anycapital`.`country_risk` (
  `ID` SMALLINT NOT NULL AUTO_INCREMENT,
  `DISPLAY_NAME` VARCHAR(20) NOT NULL,
  `NAME` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`ID`));

ALTER TABLE `anycapital`.`countries` 
ADD COLUMN `risk_id` SMALLINT NULL COMMENT 'Foreign key to country_risk' AFTER `DEFAULT_LANGUAGE_ID`,
ADD INDEX `country_fk_risk_idx` (`risk_id` ASC);
ALTER TABLE `anycapital`.`countries` 
ADD CONSTRAINT `country_fk_risk`
  FOREIGN KEY (`risk_id`)
  REFERENCES `anycapital`.`country_risk` (`ID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

---------------------------------------------------------
-- END
-- SP-452 [Technical][Regulation] - Add country regulation risk identification
-- Eyal O
---------------------------------------------------------

---------------------------------------------------------
-- SP-459 [Technical][Regulation] - Logic for calculation files by GBG and Country - require docs
-- Eyal O
---------------------------------------------------------

CREATE TABLE `anycapital`.`regulation_file_rules` (
  `id` SMALLINT NOT NULL AUTO_INCREMENT,
  `country_risk_id` SMALLINT NULL COMMENT 'Foreign key to country_risk',
  `gbg_status_id` SMALLINT NULL,
  `file_type_id` INT NOT NULL COMMENT 'Foreign key to file_types',
  PRIMARY KEY (`id`),
  INDEX `regulation_file_rules_fk_country_risk_idx` (`country_risk_id` ASC),
  INDEX `regulation_file_rules_fk_file_type_idx` (`file_type_id` ASC),
  CONSTRAINT `regulation_file_rules_fk_country_risk`
    FOREIGN KEY (`country_risk_id`)
    REFERENCES `anycapital`.`country_risk` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `regulation_file_rules_fk_file_type`
    FOREIGN KEY (`file_type_id`)
    REFERENCES `anycapital`.`file_types` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

---------------------------------------------------------
-- END
-- SP-459 [Technical][Regulation] - Logic for calculation files by GBG and Country - require docs
-- Eyal O
---------------------------------------------------------
  
---------------------------------------------------------------------------------------------------
-- LioR SoLoMoN  
-- SP-451
-- [Technical][Regulation] - GBG integration

CREATE TABLE `anycapital`.`gbg_statuses` (
  `id` SMALLINT(6) NOT NULL,
  `DISPLAY_NAME` VARCHAR(30) NOT NULL,
  `NAME` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`id`));

INSERT INTO `anycapital`.`gbg_statuses` (`id`, `DISPLAY_NAME`, `NAME`) VALUES ('1', 'gbg.statuses.1', 'PASS');
INSERT INTO `anycapital`.`gbg_statuses` (`id`, `DISPLAY_NAME`, `NAME`) VALUES ('2', 'gbg.statuses.2', 'FAIL');
INSERT INTO `anycapital`.`gbg_statuses` (`id`, `DISPLAY_NAME`, `NAME`) VALUES ('3', 'gbg.statuses.3', 'REFER');


ALTER TABLE `anycapital`.`regulation_file_rules` 
ADD INDEX `regulation_file_rules_fk_gbg_status_idx` (`gbg_status_id` ASC);
ALTER TABLE `anycapital`.`regulation_file_rules` 
ADD CONSTRAINT `regulation_file_rules_fk_gbg_status`
  FOREIGN KEY (`gbg_status_id`)
  REFERENCES `anycapital`.`gbg_statuses` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


CREATE TABLE `anycapital`.`gbg_countries` (
  `COUNTRY_ID` SMALLINT(6) NOT NULL,
  `PROFILE_ID` VARCHAR(300) NOT NULL,
  PRIMARY KEY (`COUNTRY_ID`),
  CONSTRAINT `FK_GBG_COUNTRIES`
    FOREIGN KEY (`COUNTRY_ID`)
    REFERENCES `anycapital`.`countries` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


Insert into anycapital.GBG_COUNTRIES (COUNTRY_ID,PROFILE_ID) values (15,'56fb7d60-3baa-446e-831c-af635b03b134');
Insert into anycapital.GBG_COUNTRIES (COUNTRY_ID,PROFILE_ID) values (22,'34b61a57-bf7b-41a6-8cfd-e257bdd1f28c');
Insert into anycapital.GBG_COUNTRIES (COUNTRY_ID,PROFILE_ID) values (57,'541e5563-3160-4381-be7c-f2b91a375644');
Insert into anycapital.GBG_COUNTRIES (COUNTRY_ID,PROFILE_ID) values (72,'a3f5cc56-b425-4ee4-8958-d6ed9fc44fdb');
Insert into anycapital.GBG_COUNTRIES (COUNTRY_ID,PROFILE_ID) values (73,'00a3ac16-0d72-440a-b90a-994c3ede5009');
Insert into anycapital.GBG_COUNTRIES (COUNTRY_ID,PROFILE_ID) values (80,'89d8add5-53eb-4695-816a-9a657e60ff8e');
Insert into anycapital.GBG_COUNTRIES (COUNTRY_ID,PROFILE_ID) values (101,'f4675047-ac7a-4cc3-8bcd-724c63dcbdc8');
Insert into anycapital.GBG_COUNTRIES (COUNTRY_ID,PROFILE_ID) values (102,'dabaf23b-8c05-4283-8b90-a18fc9d4ae6c');
Insert into anycapital.GBG_COUNTRIES (COUNTRY_ID,PROFILE_ID) values (119,'5479baca-1278-491c-945c-f713dc4a86ff');
Insert into anycapital.GBG_COUNTRIES (COUNTRY_ID,PROFILE_ID) values (145,'d5da40f0-399c-48c2-8a97-b84c55ddf9da');
Insert into anycapital.GBG_COUNTRIES (COUNTRY_ID,PROFILE_ID) values (148,'39813132-3fcd-40aa-8d7f-24545b0db714');
Insert into anycapital.GBG_COUNTRIES (COUNTRY_ID,PROFILE_ID) values (156,'f452598e-40b0-4ae8-8999-2208b627f496');
Insert into anycapital.GBG_COUNTRIES (COUNTRY_ID,PROFILE_ID) values (166,'f11e3b50-11b0-4a9f-a923-aa8241c518ef');
Insert into anycapital.GBG_COUNTRIES (COUNTRY_ID,PROFILE_ID) values (189,'6ec682ee-271c-48c1-a89c-ea57882d2982');
Insert into anycapital.GBG_COUNTRIES (COUNTRY_ID,PROFILE_ID) values (192,'2663938d-9a3a-4a54-9c64-3d471ca47f9c');
Insert into anycapital.GBG_COUNTRIES (COUNTRY_ID,PROFILE_ID) values (200,'00c926c8-d46d-456a-92bc-7aa39630559f');
Insert into anycapital.GBG_COUNTRIES (COUNTRY_ID,PROFILE_ID) values (201,'c33a81b9-e00b-4e0b-9a9c-587e67b0d731');
Insert into anycapital.GBG_COUNTRIES (COUNTRY_ID,PROFILE_ID) values (219,'091dd41c-4b8d-43f3-8885-5f0fa285d275');


CREATE TABLE `anycapital`.`gbg_users` (
  `user_id` BIGINT(20) NOT NULL,
  `score` INT NOT NULL,
  `request` LONGTEXT NULL,
  `response` LONGTEXT NULL,
  `time_created` TIMESTAMP(3) NOT NULL,
  `time_updated` TIMESTAMP(3) NULL,
  PRIMARY KEY (`user_id`),
  INDEX `fk_gbg_users_users_idx` (`user_id` ASC),
  CONSTRAINT `fk_gbg_users_users`
    FOREIGN KEY (`user_id`)
    REFERENCES `anycapital`.`users` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;



CREATE TABLE `anycapital`.`gbg_score` (
  `id` SMALLINT(6) NOT NULL,
  `gbg_status_id` SMALLINT(6) NOT NULL,
  `from` VARCHAR(45) NOT NULL,
  `to` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_gbg_score_status_idx` (`gbg_status_id` ASC),
  CONSTRAINT `fk_gbg_score_status`
    FOREIGN KEY (`gbg_status_id`)
    REFERENCES `anycapital`.`gbg_statuses` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

ALTER TABLE `anycapital`.`gbg_score` 
CHANGE COLUMN `from` `from` INT NOT NULL ,
CHANGE COLUMN `to` `to` INT NOT NULL ;


INSERT INTO `anycapital`.`gbg_score` (`id`, `gbg_status_id`, `from`, `to`) VALUES ('1', '1', '1111', '999999 ');
INSERT INTO `anycapital`.`gbg_score` (`id`, `gbg_status_id`, `from`, `to`) VALUES ('2', '3', '0', '1110 ');



---------------------------------------------------------------------------------------------------
  
---------------------------------------------------------------------------------------------------
-- Eyal O
-- SP-474 [Technical][Regulation][CRM][Server] - Display require docs for each user
---------------------------------------------------------------------------------------------------

ALTER TABLE `anycapital`.`regulation_user_info` 
DROP FOREIGN KEY `regulation_user_info_fk_3`;
ALTER TABLE `anycapital`.`regulation_user_info` 
CHANGE COLUMN `country_id` `country_risk_id` SMALLINT(6) NULL DEFAULT NULL COMMENT 'Foreign key to countries' ,
ADD INDEX `regulation_user_info_fk_3_idx` (`country_risk_id` ASC),
DROP INDEX `regulation_user_info_fk_3_idx` ;
ALTER TABLE `anycapital`.`regulation_user_info` 
ADD CONSTRAINT `regulation_user_info_fk_3`
  FOREIGN KEY (`country_risk_id`)
  REFERENCES `anycapital`.`country_risk` (`ID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `anycapital`.`regulation_user_info` 
ADD UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC);

---------------------------------------------------------------------------------------------------
-- Eyal O
-- SP-474 [Technical][Regulation][CRM][Server] - Display require docs for each user
---------------------------------------------------------------------------------------------------

--------------------------------------------
-- Eyal Goren
-- SP-496 - [Technical][General]- email mechanism
-- start
--------------------------------------------

CREATE TABLE `anycapital`.`email_statuses` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
COMMENT = 'all the email statuses we have';

INSERT INTO `anycapital`.`email_statuses`
(`id`,
`name`)
VALUES
(1,
'Queued');

INSERT INTO `anycapital`.`email_statuses`
(`id`,
`name`)
VALUES
(2,
'Processing');

INSERT INTO `anycapital`.`email_statuses`
(`id`,
`name`)
VALUES
(3,
'Sent');

INSERT INTO `anycapital`.`email_statuses`
(`id`,
`name`)
VALUES
(4,
'Failed');

CREATE TABLE `anycapital`.`emails` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `status_id` INT NOT NULL DEFAULT 1 COMMENT 'email status id',
  `email_info` VARCHAR(2000) NOT NULL COMMENT 'information about the email that need to send in josn format. (from, to , subject, template, text....)',
  `time_created` TIMESTAMP(3) NOT NULL DEFAULT now(3),
  `action_source_id` TINYINT(4) NOT NULL,
  `retries` INT NOT NULL DEFAULT 0 COMMENT 'number of retries',
  `time_sent` TIMESTAMP(3) NULL,
  PRIMARY KEY (`id`),
  INDEX `status_id_fk_idx` (`status_id` ASC),
  CONSTRAINT `status_id_fk`
    FOREIGN KEY (`status_id`)
    REFERENCES `anycapital`.`email_statuses` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
COMMENT = 'emails need to be send';

--------------------------------------------
-- Eyal Goren
-- SP-496 - [Technical][General]- email mechanism
-- end
--------------------------------------------

--------------------------------------------
-- Eyal O
-- SP-498 ODT Questionnaire + Logic
--------------------------------------------

ALTER TABLE `anycapital`.`qm_questions` 
CHANGE COLUMN `name` `display_name` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT 'The question\'s key will be replaced by text\'s languages.' ;

ALTER TABLE `anycapital`.`qm_questions` 
ADD COLUMN `name` VARCHAR(200) NOT NULL AFTER `id`;

ALTER TABLE `anycapital`.`qm_answers` 
CHANGE COLUMN `name` `display_name` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT 'The question\'s key will be replaced by text\'s languages.' ;

ALTER TABLE `anycapital`.`qm_answers` 
ADD COLUMN `name` VARCHAR(200) NOT NULL AFTER `id`;

--------------------------------------------
-- End
-- Eyal O
-- SP-498 ODT Questionnaire + Logic
--------------------------------------------

--------------------------------------------
-- Eyal O
-- SP-450 [Technical][Regulation] - Logic for questionnaire pass or failed
--------------------------------------------

ALTER TABLE `anycapital`.`qm_answers` 
ADD COLUMN `score` BIGINT(20) NOT NULL DEFAULT 0 AFTER `order_id`;

CREATE TABLE `anycapital`.`regulation_questionnaire_status_score` (
  `id` SMALLINT NOT NULL AUTO_INCREMENT,
  `score` BIGINT(20) NOT NULL,
  `status_id` SMALLINT(6) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_regulation_questionnaire_status_id_idx` (`status_id` ASC),
  CONSTRAINT `fk_regulation_questionnaire_status_id`
    FOREIGN KEY (`status_id`)
    REFERENCES `anycapital`.`regulation_questionnaire_statuses` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
COMMENT = 'The table represent the regulation_questionnaire_status by sum of answer\'s score.';

--------------------------------------------
-- END
-- Eyal O
-- SP-450 [Technical][Regulation] - Logic for questionnaire pass or failed
--------------------------------------------
--------------------------------------------
-- Eran Levy - add ip to logins START
--------------------------------------------
ALTER TABLE `anycapital`.`logins` 
ADD COLUMN `IP` VARCHAR(30) NULL DEFAULT NULL AFTER `MARKETING_TRACKING_ID`;
--------------------------------------------
-- Eran Levy - add ip to logins END
--------------------------------------------
--------------------------------------------
-- Eran Levy - add blocked START
--------------------------------------------
ALTER TABLE `anycapital`.`countries` 
ADD COLUMN `is_blocked` SMALLINT(6) NOT NULL DEFAULT 0 AFTER `risk_id`;
--------------------------------------------
-- Eran Levy - add blocked to logins END
--------------------------------------------

--------------------------------------------
-- Eyal O
-- Users history
--------------------------------------------

CREATE TABLE `anycapital`.`users_history_actions` (
  `id` SMALLINT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));
  
CREATE TABLE `anycapital`.`users_history` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `user_id` BIGINT(20) NOT NULL,
  `users_history_action_id` SMALLINT(6) NOT NULL,
  `action_paramters` LONGTEXT NOT NULL COMMENT 'the action new paramters we set to the user json object',
  `action_source_id` TINYINT NOT NULL,
  `writer_id` SMALLINT NULL,
  `server_name` VARCHAR(50) NOT NULL,
  `time_created` TIMESTAMP(3) NOT NULL DEFAULT NOW(3),
  PRIMARY KEY (`id`),
  INDEX `user_id_idx` (`user_id` ASC),
  INDEX `fk_uh_user_history_action_id_idx` (`users_history_action_id` ASC),
  INDEX `fk_uh_action_source_id_idx` (`action_source_id` ASC),
  INDEX `fk_uh_writer_id_idx` (`writer_id` ASC),
  CONSTRAINT `fk_uh_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `anycapital`.`users` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_uh_user_history_action_id`
    FOREIGN KEY (`users_history_action_id`)
    REFERENCES `anycapital`.`users_history_actions` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_uh_action_source_id`
    FOREIGN KEY (`action_source_id`)
    REFERENCES `anycapital`.`action_source` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_uh_writer_id`
    FOREIGN KEY (`writer_id`)
    REFERENCES `anycapital`.`writers` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
  
--------------------------------------------
-- END
-- Eyal O
-- Users history
--------------------------------------------
--LioR    
ALTER TABLE `anycapital`.`users` 
DROP COLUMN `VIP_STATUS_ID`;

--------------------------------------------
-- Sendgrid
-- Eyal O
--------------------------------------------

CREATE TABLE `anycapital`.`email_actions` (
  `id` SMALLINT NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`));
  
CREATE TABLE `anycapital`.`email_templates` (
  `id` SMALLINT NOT NULL,
  `action_id` SMALLINT NOT NULL COMMENT 'Foreign key to email_actions.',
  `language_id` SMALLINT(6) NOT NULL COMMENT 'Foreign key to languages.',
  `template_id` VARCHAR(100) NOT NULL COMMENT 'The template id which defined in Sendgrid system.',
  PRIMARY KEY (`id`),
  INDEX `fk_et_action_id_idx` (`action_id` ASC),
  INDEX `fk_et_language_id_idx` (`language_id` ASC),
  CONSTRAINT `fk_et_action_id`
    FOREIGN KEY (`action_id`)
    REFERENCES `anycapital`.`email_actions` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_et_language_id`
    FOREIGN KEY (`language_id`)
    REFERENCES `anycapital`.`languages` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
ALTER TABLE `anycapital`.`email_templates` 
CHANGE COLUMN `id` `id` SMALLINT(6) NOT NULL AUTO_INCREMENT ;

ALTER TABLE `anycapital`.`email_templates` 
ADD COLUMN `is_active` BINARY NOT NULL DEFAULT 1 COMMENT 'Represent if the record is active or not.' AFTER `template_id`;

--------------------------------------------
-- End
-- Sendgrid
-- Eyal O
--------------------------------------------
--------------------------------------------------------------------------------
--LioR SoLoMoN
CREATE TABLE `anycapital`.`transaction_reject_types` (
  `id` SMALLINT(6) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `display_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));
  
  
  
CREATE TABLE `anycapital`.`transaction_reject` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` BIGINT(20) NOT NULL,
  `transaction_payment_type_id` SMALLINT(6) NOT NULL,
  `transaction_reject_type_id` SMALLINT(6) NOT NULL,
  `Amount` INT NOT NULL,
  `action_source_id` TINYINT(4) NOT NULL,
  `writer_id` SMALLINT(6) NULL,
  `info` VARCHAR(250) NULL,
  `SESSION_ID` VARCHAR(50) NULL,
  `server_name` VARCHAR(50) NOT NULL,
  `time_created` TIMESTAMP(3) NOT NULL DEFAULT NOW(3),
  PRIMARY KEY (`id`),
  INDEX `fk_user_id_idx` (`user_id` ASC),
  INDEX `fk_transaction_payment_type_id_idx` (`transaction_payment_type_id` ASC),
  INDEX `fk_action_source_id_idx` (`action_source_id` ASC),
  INDEX `fk_writer_id_idx` (`writer_id` ASC),
  INDEX `fk_transaction_reject_type_id_idx` (`transaction_reject_type_id` ASC),
  CONSTRAINT `fk_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `anycapital`.`users` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_transaction_payment_type_id`
    FOREIGN KEY (`transaction_payment_type_id`)
    REFERENCES `anycapital`.`transaction_payment_types` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_action_source_id`
    FOREIGN KEY (`action_source_id`)
    REFERENCES `anycapital`.`action_source` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_writer_id`
    FOREIGN KEY (`writer_id`)
    REFERENCES `anycapital`.`writers` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_transaction_reject_type_id`
    FOREIGN KEY (`transaction_reject_type_id`)
    REFERENCES `anycapital`.`transaction_reject_types` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

    
ALTER TABLE `anycapital`.`transaction_reject` 
ADD COLUMN `transaction_operation_id` TINYINT(4) NOT NULL AFTER `transaction_reject_type_id`;

ALTER TABLE `anycapital`.`transaction_reject` 
ADD INDEX `fk_transaction_operation_id_idx` (`transaction_operation_id` ASC);
ALTER TABLE `anycapital`.`transaction_reject` 
ADD CONSTRAINT `fk_transaction_operation_id`
  FOREIGN KEY (`transaction_operation_id`)
  REFERENCES `anycapital`.`transaction_operations` (`ID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

INSERT INTO `anycapital`.`transaction_reject_types` (`id`, `name`, `display_name`) VALUES ('1', 'minimum fee', 'minimum.fee');
INSERT INTO `anycapital`.`transaction_reject_types` (`id`, `name`, `display_name`) VALUES ('2', 'minimum amount', 'minimum.amount');
INSERT INTO `anycapital`.`transaction_reject_types` (`id`, `name`, `display_name`) VALUES ('3', 'open withdraw', 'open.withdraw');
INSERT INTO `anycapital`.`transaction_reject_types` (`id`, `name`, `display_name`) VALUES ('4', 'balance', 'balance');

--------------------------------------------------------------------------------=======
--------------------------------------------

--------------------------------------------
-- marketing landing pages
-- Eyal O
--------------------------------------------

ALTER TABLE `anycapital`.`marketing_landing_pages` 
ADD UNIQUE INDEX `NAME_UNIQUE` (`NAME` ASC);

--------------------------------------------
-- END
-- marketing landing pages
-- Eyal O
-------------------------------------------

---------------------------------------------
-- start
-- SP-513
-- [Daily Report][server]
-- Eyal G
--------------------------------------------

CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `anycapital`@`%` 
    SQL SECURITY DEFINER
VIEW `daily_report` AS
    SELECT 
        `users_reg`.`users_registerd` AS `Reg`,
        `users_history`.`complete_quest` AS `Completed Quest`,
        `regulation_user_info`.`quest_blocked` AS `Blocked by Quest`,
        `files`.`files` AS `Uploaded files`,
        `regulation_user_info`.`quest_after_training` AS `Customers found Fitting after Training`,
        `users_history`.`Regulation_Approved` AS `Regulation Approved`,
        `transactions`.`FTD` AS `FTD`,
        `pending_deposits`.`pending_deposits` AS `Deposits still Pending`,
        `transactions`.`desposites_count` AS `Deposits`,
        `transactions`.`desposites_sum` AS `Deposits €`,
        `transactions`.`withdraw_count` AS `Withdrawals`,
        `transactions`.`withdraw_sum` AS `Withdrawals €`,
        `investments`.`investment_pending_count` AS `Investments secured`,
        `investments`.`investment_pending_amonut` AS `Investments secured €`,
        `investments`.`investment_open_count` AS `Investments purchased`,
        `investments`.`investment_open_amonut` AS `Investments purchased €`,
        `investments`.`investment_canceled_count` AS `Investments Canceled`,
        `investments`.`investment_canceled_insufficient_funds` AS `Investments Canceled due to insufficient funds`,
        `investment_rejects`.`investment_rejects_count` AS `Investments rejected`,
        `products`.`products_count` AS `Products created`,
        `issues`.`call_issue` AS `Phone calls`,
        `pending_investments`.`pending_investments` AS `Investments still Pending 72 hrs to go time`
    FROM
        ((((((((((((SELECT 
            COUNT(1) AS `users_registerd`
        FROM
            `anycapital`.`users` `u`
        WHERE
            ((CAST(`u`.`TIME_CREATED` AS DATE) = CAST((NOW() - INTERVAL 1 DAY) AS DATE))
                AND (`u`.`CLASS_ID` <> 1)))) `users_reg`
        JOIN (SELECT 
            COUNT(1) AS `complete_quest`,
                COUNT((CASE
                    WHEN (UPPER(`uh`.`action_paramters`) = UPPER('{"STEP":5}')) THEN 1
                    ELSE NULL
                END)) AS `Regulation_Approved`
        FROM
            (`anycapital`.`users_history` `uh`
        JOIN `anycapital`.`users` `u`)
        WHERE
            ((CAST(`uh`.`time_created` AS DATE) = CAST((NOW() - INTERVAL 1 DAY) AS DATE))
                AND (`uh`.`users_history_action_id` = 3)
                AND (`u`.`ID` = `uh`.`user_id`)
                AND (`u`.`CLASS_ID` <> 1)
                AND ((UPPER(`uh`.`action_paramters`) = UPPER('{"STEP":3}'))
                OR (UPPER(`uh`.`action_paramters`) = UPPER('{"STEP":4}'))
                OR (UPPER(`uh`.`action_paramters`) = UPPER('{"STEP":5}'))))) `users_history`)
        JOIN (SELECT 
            COUNT((CASE
                    WHEN (`rui`.`regulation_questionnaire_status_id` IN (1 , 4)) THEN 1
                    ELSE NULL
                END)) AS `quest_blocked`,
                COUNT((CASE
                    WHEN (`rui`.`regulation_questionnaire_status_id` = 3) THEN 1
                    ELSE NULL
                END)) AS `quest_after_training`
        FROM
            (`anycapital`.`regulation_user_info` `rui`
        JOIN `anycapital`.`users` `u`)
        WHERE
            ((((CAST(`rui`.`time_created` AS DATE) = CAST((NOW() - INTERVAL 1 DAY) AS DATE))
                AND (`rui`.`regulation_questionnaire_status_id` IN (1 , 4)))
                OR ((CAST(`rui`.`time_modified` AS DATE) = CAST((NOW() - INTERVAL 1 DAY) AS DATE))
                AND (`rui`.`regulation_questionnaire_status_id` = 3)))
                AND (`u`.`ID` = `rui`.`user_id`)
                AND (`u`.`CLASS_ID` <> 1))) `regulation_user_info`)
        JOIN (SELECT 
            COUNT(1) AS `files`
        FROM
            (`anycapital`.`files` `f`
        JOIN `anycapital`.`users` `u`)
        WHERE
            ((CAST(`f`.`time_created` AS DATE) = CAST((NOW() - INTERVAL 1 DAY) AS DATE))
                AND (`u`.`ID` = `f`.`user_id`)
                AND (`u`.`CLASS_ID` <> 1))) `files`)
        JOIN (SELECT 
            COUNT((CASE
                    WHEN
                        ((`t`.`TRANSACTION_OPERATION_ID` = 1)
                            AND (`ma`.`ID` IS NOT NULL))
                    THEN
                        1
                    ELSE NULL
                END)) AS `FTD`,
                COUNT((CASE
                    WHEN (`t`.`TRANSACTION_OPERATION_ID` = 1) THEN 1
                    ELSE NULL
                END)) AS `desposites_count`,
                ROUND((SUM((CASE
                    WHEN (`t`.`TRANSACTION_OPERATION_ID` = 1) THEN `t`.`AMOUNT`
                    ELSE 0
                END)) / 100), 2) AS `desposites_sum`,
                COUNT((CASE
                    WHEN (`t`.`TRANSACTION_OPERATION_ID` = 2) THEN 1
                    ELSE NULL
                END)) AS `withdraw_count`,
                ROUND((SUM((CASE
                    WHEN (`t`.`TRANSACTION_OPERATION_ID` = 2) THEN `t`.`AMOUNT`
                    ELSE 0
                END)) / 100), 2) AS `withdraw_sum`
        FROM
            (`anycapital`.`users` `u`
        JOIN (`anycapital`.`transactions` `t`
        LEFT JOIN `anycapital`.`marketing_attribution` `ma` ON (((`ma`.`REFERENCE_ID` = `t`.`ID`)
            AND (`ma`.`TABLE_ID` = 2)))))
        WHERE
            ((CAST(`t`.`TIME_CREATED` AS DATE) = CAST((NOW() - INTERVAL 1 DAY) AS DATE))
                AND (`t`.`TRANSACTION_PAYMENT_TYPE_ID` = 1)
                AND (`u`.`ID` = `t`.`USER_ID`)
                AND (`u`.`CLASS_ID` <> 1))) `transactions`)
        JOIN (SELECT 
            COUNT(1) AS `pending_deposits`
        FROM
            (`anycapital`.`transactions` `t`
        JOIN `anycapital`.`users` `u`)
        WHERE
            ((CAST(`t`.`TIME_CREATED` AS DATE) <= CAST((NOW() - INTERVAL 1 DAY) AS DATE))
                AND (`t`.`TRANSACTION_PAYMENT_TYPE_ID` = 1)
                AND (`t`.`TRANSACTION_OPERATION_ID` = 1)
                AND (`t`.`TRANSACTION_STATUS_ID` = 4)
                AND (`u`.`ID` = `t`.`USER_ID`)
                AND (`u`.`CLASS_ID` <> 1))) `pending_deposits`)
        JOIN (SELECT 
            COUNT((CASE
                    WHEN (`i`.`INVESTMENT_STATUS_ID` = 1) THEN 1
                    ELSE NULL
                END)) AS `investment_pending_count`,
                ROUND((SUM((CASE
                    WHEN (`i`.`INVESTMENT_STATUS_ID` = 1) THEN `i`.`AMOUNT`
                    ELSE 0
                END)) / 100), 2) AS `investment_pending_amonut`,
                COUNT((CASE
                    WHEN (`i`.`INVESTMENT_STATUS_ID` = 2) THEN 1
                    ELSE NULL
                END)) AS `investment_open_count`,
                ROUND((SUM((CASE
                    WHEN (`i`.`INVESTMENT_STATUS_ID` = 2) THEN `i`.`AMOUNT`
                    ELSE 0
                END)) / 100), 2) AS `investment_open_amonut`,
                COUNT((CASE
                    WHEN (`i`.`INVESTMENT_STATUS_ID` IN (4 , 5)) THEN 1
                    ELSE NULL
                END)) AS `investment_canceled_count`,
                COUNT((CASE
                    WHEN (`i`.`INVESTMENT_STATUS_ID` = 5) THEN 1
                    ELSE NULL
                END)) AS `investment_canceled_insufficient_funds`
        FROM
            (`anycapital`.`investments` `i`
        JOIN `anycapital`.`users` `u`)
        WHERE
            ((CAST(`i`.`TIME_CREATED` AS DATE) = CAST((NOW() - INTERVAL 1 DAY) AS DATE))
                AND (`u`.`ID` = `i`.`USER_ID`)
                AND (`u`.`CLASS_ID` <> 1)
                AND (`i`.`INVESTMENT_STATUS_ID` IN (1 , 2, 4, 5)))) `investments`)
        JOIN (SELECT 
            COUNT(1) AS `investment_rejects_count`
        FROM
            (`anycapital`.`investment_rejects` `ir`
        JOIN `anycapital`.`users` `u`)
        WHERE
            ((CAST(`ir`.`TIME_CREATED` AS DATE) = CAST((NOW() - INTERVAL 1 DAY) AS DATE))
                AND (`u`.`ID` = `ir`.`USER_ID`)
                AND (`u`.`CLASS_ID` <> 1))) `investment_rejects`)
        JOIN (SELECT 
            COUNT(1) AS `products_count`
        FROM
            `anycapital`.`products` `p`
        WHERE
            ((CAST(`p`.`TIME_CREATED` AS DATE) = CAST((NOW() - INTERVAL 1 DAY) AS DATE))
                AND (`p`.`IS_SUSPEND` = 0))) `products`)
        JOIN (SELECT 
            COUNT(1) AS `call_issue`
        FROM
            (`anycapital`.`issues` `iss`
        JOIN `anycapital`.`users` `u`)
        WHERE
            ((CAST(`iss`.`time_created` AS DATE) = CAST((NOW() - INTERVAL 1 DAY) AS DATE))
                AND (`iss`.`channel_id` = 3)
                AND (`u`.`ID` = `iss`.`user_id`)
                AND (`u`.`CLASS_ID` <> 1))) `issues`)
        JOIN (SELECT 
            COUNT(1) AS `pending_investments`
        FROM
            ((`anycapital`.`investments` `i`
        JOIN `anycapital`.`products` `p`)
        JOIN `anycapital`.`users` `u`)
        WHERE
            ((`i`.`INVESTMENT_STATUS_ID` = 1)
                AND (`p`.`ID` = `i`.`PRODUCT_ID`)
                AND (`p`.`SUBSCRIPTION_END_DATE` <= (NOW() + INTERVAL 72 HOUR))
                AND (`u`.`ID` = `i`.`USER_ID`)
                AND (`u`.`CLASS_ID` <> 1))) `pending_investments`)

---------------------------------------------
-- end
-- SP-513
-- [Daily Report][server]
-- Eyal G
--------------------------------------------
                
--------------------------------------------
-- Eyal O
-- market place
--------------------------------------------

ALTER TABLE `anycapital`.`markets` 
ADD COLUMN `MARKET_PLACE` VARCHAR(45) NOT NULL AFTER `MARKET_ID_AO`;

--------------------------------------------
-- END
-- Eyal O
-- market place
--------------------------------------------
--------------------------------------------
-- Allow register
-- Eran
-- Start
--------------------------------------------                
 CREATE TABLE `anycapital`.`allow_ip` (
  `ip` VARCHAR(50) NOT NULL,
  `comment` VARCHAR(50) NULL,
  PRIMARY KEY (`ip`));
--------------------------------------------
-- Allow register
-- Eran
-- End
--------------------------------------------       

--------------------------------------------
-- Eyal O
-- msg_res_language_history
--------------------------------------------

CREATE TABLE msg_res_language_history LIKE msg_res_language;

ALTER TABLE `anycapital`.`msg_res_language_history` 
CHANGE COLUMN `MSG_RES_LANGUAGE_ID` `LANGUAGE_ID` SMALLINT(6) NOT NULL ,
CHANGE COLUMN `TIME_UPDATED` `TIME_CREATED` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ;
ALTER TABLE `anycapital`.`msg_res_language_history` 
ADD CONSTRAINT `msg_res_language_history_fk1`
  FOREIGN KEY (`MSG_RES_ID`)
  REFERENCES `anycapital`.`msg_res` (`ID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `msg_res_language_history_fk2`
  FOREIGN KEY (`WRITER_ID`)
  REFERENCES `anycapital`.`writers` (`ID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `msg_res_language_history_fk3`
  FOREIGN KEY (`LANGUAGE_ID`)
  REFERENCES `anycapital`.`languages` (`ID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
  
ALTER TABLE `anycapital`.`msg_res_language_history` 
ADD COLUMN `id` BIGINT(20) NOT NULL AUTO_INCREMENT FIRST,
ADD PRIMARY KEY (`id`);

ALTER TABLE `anycapital`.`msg_res_language_history` 
DROP FOREIGN KEY `msg_res_language_history_fk1`;
ALTER TABLE `anycapital`.`msg_res_language_history` 
DROP INDEX `MSG_RES_ID` ;

ALTER TABLE `anycapital`.`msg_res_language_history` 
ADD INDEX `msg_res_language_history_fk1_idx` (`MSG_RES_ID` ASC);
ALTER TABLE `anycapital`.`msg_res_language_history` 
ADD CONSTRAINT `msg_res_language_history_fk1`
  FOREIGN KEY (`MSG_RES_ID`)
  REFERENCES `anycapital`.`msg_res` (`ID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

--------------------------------------------
-- END
-- Eyal O
-- msg_res_language_history
--------------------------------------------
  
--------------------------------------------
-- Eyal O
-- Taxpayer Identification Number (TIN)
--------------------------------------------

ALTER TABLE `anycapital`.`users` 
ADD COLUMN `TIN` VARCHAR(45) NULL COMMENT 'Taxpayer Identification Number' AFTER `COUNTRY_BY_PREFIX`;

--------------------------------------------
-- END
-- Eyal O
-- Taxpayer Identification Number (TIN)
--------------------------------------------

-------------------------------------------------------------------------
--------------------------- db insert -----------------------------------
-------------------------------------------------------------------------

-- INSERTING into MARKETS
INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('S&P 500 Index',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
1,
'SPX Index',
'EUR',
'^GSPC',
1,
2,
6);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('FTSE 100 Index',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
1,
'UKX Index',
'GBP',
'^FTSE',
1,
2,
4);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('Nikkei 225',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
1,
'NKY Index',
'JPY',
'^N225',
1,
2,
9);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('DAX Index',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
1,
'DAX Index',
'EUR',
'^GDAXI',
1,
2,
8);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('CAC 40 Index',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
1,
'CAC Index',
'EUR',
'^FCHI',
1,
2,
10);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('Dow Jones Indus Avg',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
1,
'Indu Index',
'EUR',
'^DJI',
1,
2,
5);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('Nasdaq 100 Composite Index',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
1,
'NDX Index',
'EUR',
'^IXIC',
1,
2,
7);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('Apple',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
2,
'AAPL UQ Equity',
'EUR',
'AAPL',
1,
2,
129);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('Alibaba',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
2,
'BABA UN Equity',
'EUR',
'BABA',
1,
2,
679);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('Amazon',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
2,
'AMZN UQ Equity',
'EUR',
'AMZN',
1,
2,
627);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('Twitter',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
2,
'TWTR UN Equity',
'EUR',
'TWTR',
1,
2,
648);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('Facebook',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
2,
'FB UQ Equity',
'EUR',
'FB',
1,
2,
620);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('Deutsche Bank',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
2,
'DBK GY Equity',
'EUR',
'DBK.DE',
1,
2,
394);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('Allianz',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
2,
'ALV GY Equity',
'EUR',
'ALV.DE',
1,
2,
537);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('BMW',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
2,
'BMW GY Equity',
'EUR',
'BMW.DE',
1,
2,
702);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('Microsoft',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
2,
'MSFT UQ Equity',
'EUR',
'MSFT',
1,
2,
130);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('Adidas',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
2,
'ADS GY Equity',
'EUR',
'ADS.DE',
1,
2,
697);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('Coca-Cola',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
2,
'KO US Equity',
'EUR',
'KO',
1,
2,
694);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('Cisco',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
2,
'CSCO US Equity',
'EUR',
'CSCO',
1,
2,
626);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('Paypal',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
2,
'PYPL UQ Equity',
'EUR',
'PYPL',
1,
2,
708);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('Citi Group',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
2,
'C UN Equity',
'EUR',
'C',
1,
2,
128);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('Bank Of America',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
2,
'BAC US Equity',
'EUR',
'BAC',
1,
2,
598);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('Goldman Sachs',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
2,
'GS UN Equity',
'EUR',
'GS',
1,
2,
591);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('Pfizer',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
2,
'PFE UN Equity',
'EUR',
'PFE',
1,
2,
628);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('Procter&gamble',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
2,
'PG UN Equity',
'EUR',
'PG',
1,
2,
713);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('AIG',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
2,
'AIG UN Equity',
'EUR',
'AIG',
1,
2,
629);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('Wal-mart stores',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
2,
'WMT US Equity',
'EUR',
'WMT',
1,
2,
769);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('Tesla',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
2,
'TSLA US Equity',
'EUR',
'TSLA',
1,
2,
767);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('Google',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
2,
'GOOG US Equity',
'EUR',
'GOOG',
1,
2,
132);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('British Petroleum',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
2,
'BP/ LN Equity',
'GBP',
'BP.L',
1,
2,
415);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('Royal Bank of Scotland',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
2,
'RBS LN Equity',
'GBP',
'RBS.L',
1,
2,
433);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('Banco Santander',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
2,
'SAN SM Equity',
'EUR',
'SAN.MC',
1,
2,
543);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('Exxon Mobil',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
2,
'XOM US Equity',
'EUR',
'XOM',
1,
2,
565);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('Societe Generale',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
2,
'GLE FP Equity',
'EUR',
'GLE.PA',
1,
2,
587);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('NIKE',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
2,
'NKE US Equity',
'EUR',
'NKE',
1,
2,
696);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('Ebay',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
2,
'EBAY US Equity',
'EUR',
'EBAY',
1,
2,
770);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('General Motors',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
2,
'GM US Equity',
'EUR',
'GM',
1,
2,
761);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('Delta Airlines',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
2,
'DAL US Equity',
'EUR',
'DAL',
1,
2,
786);

INSERT INTO `anycapital`.`markets` (`NAME`, `DISPLAY_NAME`, `MARKET_GROUP_ID`, `TICKER`, `CURRENCY`, `FEED_NAME`, `IS_ACTIVE`, `DECIMAL_POINT`, `MARKET_ID_AO`)
VALUES
('EUR/USD',
CONCAT('market.', (SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES
WHERE table_name = 'markets'
AND table_schema = 'anycapital')),
5,
'EURUSD CURNCY',
'EUR',
'EURUSD=X',
1,
4,
16);

-- INSERTING into COUNTRIES
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Reunion Island','RE','REU',262,'countries.reu','+44-2080997262','+44-8081890112','GMT-00:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Russia','RU','RUS',7,'countries.rus','788001006472','+44-8081890112','GMT+03:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Rwanda','RW','RWA',250,'countries.rwa','+44-2080997262','+44-8081890112','GMT+02:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Saint Kitts and Nevis','KN','KNA',1869,'countries.kna','+44-2080997262','+44-8081890112','GMT-04:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Saint Lucia','LC','LCA',1758,'countries.lca','+44-2080997262','+44-8081890112','GMT-04:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('San Marino','SM','SMR',378,'countries.smr','+44-2080997262','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Senegal','SN','SEN',221,'countries.sen','+44-2080997262','+44-8081890112','GMT-00:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Seychelles','SC','SYC',248,'countries.syc','+44-2080997262','+44-8081890112','GMT+04:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Sierra Leone','SL','SLE',232,'countries.sle','+44-2080997262','+44-8081890112','GMT-00:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Singapore','SG','SGP',65,'countries.sgp','+44-2080997262','+44-8081890112','GMT+08:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Slovenia','SI','SVN',386,'countries.svn','+44-2080997262','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Solomon Islands','SB','SLB',677,'countries.slb','+44-2080997262','+44-8081890112','GMT+11:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Somalia','SO','SOM',252,'countries.som','+44-2080997262','+44-8081890112','GMT+03:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('South Korea','KR','KOR',82,'countries.kor','00308131770','+44-8081890112','GMT+09:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Spain','ES','ESP',34,'countries.esp','34911237253','900939786','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('ST. Helena','SH','SHN',290,'countries.shn','+44-2080997262','+44-8081890112','GMT-00:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Sudan','SD','SDN',249,'countries.sdn','+973-16199107','+44-8081890112','GMT+02:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Suriname','SR','SUR',597,'countries.sur','+44-2080997262','+44-8081890112','GMT-03:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Swaziland','SZ','SWZ',268,'countries.swz','+44-2080997262','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Sweden','SE','SWE',46,'countries.swe','+46-852507086','+46-852507086','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Syria','SY','SYR',963,'countries.syr','+973-16199107','+44-8081890112','GMT+02:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Taiwan','TW','TWN',886,'countries.twn','4400801147060','+44-8081890112','GMT+08:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Tajikistan','TJ','TJK',992,'countries.tjk','+44-2080997262','+44-8081890112','GMT+06:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Tanzania','TZ','TZA',255,'countries.tza','+44-2080997262','+44-8081890112','GMT+03:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Tokelau','TK','TKL',690,'countries.tkl','+44-2080997262','+44-8081890112','GMT-10:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Tonga Islands','TO','TON',676,'countries.ton','+44-2080997262','+44-8081890112','GMT+13:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Trinidad and Tobago','TT','TTO',1868,'countries.tto','+44-2080997262','+44-8081890112','GMT-04:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Tunisia','TN','TUN',216,'countries.tun','+973-16199107','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Turkmenistan','TM','TKM',993,'countries.tkm','+44-2080997262','+44-8081890112','GMT+05:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Tuvalu','TV','TUV',688,'countries.tuv','+44-2080997262','+44-8081890112','GMT+12:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Ukraine','UA','UKR',380,'countries.ukr','+44-2080997262','+44-8081890112','GMT+02:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('United Arab Emirates','AE','ARE',971,'countries.are','+973-16199107','+44-8081890112','GMT+04:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Uzbekistan','UZ','UZB',998,'countries.uzb','+44-2080997262','+44-8081890112','GMT+05:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Vanuato','VU','VUT',678,'countries.vut','+44-2080997262','+44-8081890112','GMT+11:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Vatican','VA','VAT',39,'countries.vat','+44-2080997262','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Venezuela','VE','VEN',58,'countries.ven','+44-2080997262','+44-8081890112','GMT-04:30');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Vietnam','VN','VNM',84,'countries.vnm','+44-2080997262','+44-8081890112','GMT+07:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Western Sahara','EH','ESH',0,'countries.esh','+44-2080997262','+44-8081890112','GMT-00:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Yemen','YE','YEM',967,'countries.yem','+973-16199107','+44-8081890112','GMT+03:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Zaire','ZR','COD',243,'countries.zar','+44-2080997262','+44-8081890112','GMT+02:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Zambia','ZM','ZMB',260,'countries.zmb','+44-2080997262','+44-8081890112','GMT+02:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Zimbabwe','ZW','ZWE',263,'countries.zwe','+44-2080997262','+44-8081890112','GMT+02:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Israel','IL','ISR',972,'countries.isr','0772282222','03-6444403','GMT+03:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Algeria','DZ','DZA',213,'countries.dza','+973-16199107','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('American Samoa','AS','ASM',684,'countries.asm','+44-2080997262','+44-8081890112','GMT-11:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Andorra','AD','AND',376,'countries.and','+44-2080997262','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Anguilla','AI','AIA',1265,'countries.aia','+44-2080997262','+44-8081890112','GMT-04:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Argentina','AR','ARG',54,'countries.arg','08004449743','08003330731','GMT-03:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Armenia','AM','ARM',374,'countries.arm','+44-2080997262','+44-8081890112','GMT+04:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Aruba','AW','ABW',297,'countries.abw','+44-2080997262','+44-8081890112','GMT-04:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Australia','AU','AUS',61,'countries.aus','61261452244','+44-8081890112','GMT+10:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Azerbaijan','AZ','AZE',994,'countries.aze','+44-2080997262','+44-8081890112','GMT+03:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Belarus','BY','BLR',375,'countries.blr','+44-2080997262','+44-8081890112','GMT+02:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Belgium','BE','BEL',32,'countries.bel','+44-2080997262','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Benin','BJ','BEN',229,'countries.ben','+44-2080997262','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Bermuda','BM','BMU',1441,'countries.bmu','+44-2080997262','+44-8081890112','GMT-04:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Bosnia','BA','BIH',387,'countries.bih','+44-2080997262','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Botswana','BW','BWA',267,'countries.bwa','+44-2080997262','+44-8081890112','GMT+02:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Brunei Darussalam','BN','BRN',673,'countries.brn','+44-2080997262','+44-8081890112','GMT+08:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Bulgaria','BG','BGR',359,'countries.bgr','+44-2080997262','+44-8081890112','GMT+02:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Burundi','BI','BDI',257,'countries.bdi','+44-2080997262','+44-8081890112','GMT-00:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Cambodia','KH','KHM',855,'countries.khm','+44-2080997262','+44-8081890112','GMT+07:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Cameroon','CM','CMR',237,'countries.cmr','+44-2080997262','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Cayman Islands','KY','CYM',1345,'countries.cym','+44-2080997262','+44-8081890112','GMT-05:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Central Africa','CF','CAF',236,'countries.caf','+44-2080997262','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Chad','TD','TCD',235,'countries.tcd','+44-2080997262','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('China','CN','CHN',86,'countries.chn','864001203105','400-120-3044','GMT+08:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Colombia','CO','COL',57,'countries.col','+44-2080997262','+44-8081890112','GMT-05:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Comoros','KM','COM',269,'countries.com','+44-2080997262','+44-8081890112','GMT+03:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Costa Rica','CR','CRI',506,'countries.cri','+44-2080997262','+44-8081890112','GMT-06:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Ivory Coast','CI','CIV',225,'countries.civ','+44-2080997262','+44-8081890112','GMT-00:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Cuba','CU','CUB',53,'countries.cub','+44-2080997262','+44-8081890112','GMT-05:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Czech Republic','CZ','CZE',420,'countries.cze','+44-2080997262','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Dominica','DM','DMA',1767,'countries.dma','+44-2080997262','+44-8081890112','GMT-04:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Dominican Republic','DO','DOM',1809,'countries.dom','+44-2080997262','+44-8081890112','GMT-04:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('East Timor','TP','TMP',670,'countries.tmp','+44-2080997262','+44-8081890112','GMT+09:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Egypt','EG','EGY',20,'countries.egy','+973-16199107','+44-8081890112','GMT+02:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Equatorial Guinea','GQ','GNQ',240,'countries.gnq','+44-2080997262','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Estonia','EE','EST',372,'countries.est','+44-2080997262','+44-8081890112','GMT+02:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Ethiopia','ET','ETH',251,'countries.eth','+44-2080997262','+44-8081890112','GMT+03:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Falkland Islands','FK','FLK',500,'countries.flk','+44-2080997262','+44-8081890112','GMT-04:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Fiji','FJ','FJI',679,'countries.fji','+44-2080997262','+44-8081890112','GMT+12:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('France','FR','FRA',33,'countries.fra','33170726528','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Gabon','GA','GAB',241,'countries.gab','+44-2080997262','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Gambia','GM','GMB',220,'countries.gmb','+44-2080997262','+44-8081890112','GMT-00:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Germany','DE','DEU',49,'countries.deu','49305683700568','+49-0800-184-4978','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Ghana','GH','GHA',233,'countries.gha','+44-2080997262','+44-8081890112','GMT-00:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Greenland','GL','GRL',299,'countries.grl','+44-2080997262','+44-8081890112','GMT-03:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Guadeloupe','GP','GLP',590,'countries.glp','+44-2080997262','+44-8081890112','GMT-04:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Guam','GU','GUM',1671,'countries.gum','+44-2080997262','+44-8081890112','GMT+10:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Guinea','GN','GIN',224,'countries.gin','+44-2080997262','+44-8081890112','GMT-00:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Guyana','GY','GUY',592,'countries.guy','+44-2080997262','+44-8081890112','GMT-03:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Hong Kong','HK','HKG',852,'countries.hkg','44800906106','+44-8081890112','GMT+08:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Hungary','HU','HUN',36,'countries.hun','+44-2080997262','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Iceland','IS','ISL',354,'countries.isl','+44-2080997262','+44-8081890112','GMT-00:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('India','IN','IND',91,'countries.ind','+44-2080997262','+44-8081890112','GMT+05:30');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Indonesia','ID','IDN',62,'countries.idn','+44-2080997262','+44-8081890112','GMT+07:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Iraq','IQ','IRQ',964,'countries.irq','+973-16199107','+44-8081890112','GMT+03:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Ireland','IE','IRL',353,'countries.irl','+44-2080997262','+44-8081890112','GMT-00:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Italy','IT','ITA',39,'countries.ita','390294751384','800-788-728','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Jamaica','JM','JAM',1876,'countries.jam','+44-2080997262','+44-8081890112','GMT-05:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Jordan','JO','JOR',962,'countries.jor','+973-16199107','+44-8081890112','GMT+02:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Kazakhstan','KZ','KAZ',7,'countries.kaz','+44-2080997262','+44-8081890112','GMT+06:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Kiribati','KI','KIR',686,'countries.kir','+44-2080997262','+44-8081890112','GMT+12:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Kuwait','KW','KWT',965,'countries.kwt','+973-16199107','+44-8081890112','GMT+03:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Kyrgyzstan','KG','KGZ',996,'countries.kgz','+44-2080997262','+44-8081890112','GMT+05:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Latvia','LV','LVA',371,'countries.lva','+44-2080997262','+44-8081890112','GMT+02:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Lebanon','LB','LBN',961,'countries.lbn','+973-16199107','+44-8081890112','GMT+02:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Lesotho','LS','LSO',266,'countries.lso','+44-2080997262','+44-8081890112','GMT+02:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Liberia','LR','LBR',231,'countries.lbr','+44-2080997262','+44-8081890112','GMT-00:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Liechtenstein','LI','LIE',423,'countries.lie','+44-2080997262','+44-8081890112','GMT-00:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Lithuania','LT','LTU',370,'countries.ltu','+44-2080997262','+44-8081890112','GMT+02:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Macau','MO','MAC',853,'countries.mac','+44-2080997262','+44-8081890112','GMT-00:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Madagascar','MG','MDG',261,'countries.mdg','+44-2080997262','+44-8081890112','GMT+03:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Malawi','MW','MWI',265,'countries.mwi','+44-2080997262','+44-8081890112','GMT+02:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Malaysia','MY','MYS',60,'countries.mys','+44-2080997262','+44-8081890112','GMT+08:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Maldives','MV','MDV',960,'countries.mdv','+44-2080997262','+44-8081890112','GMT+05:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Mali','ML','MLI',223,'countries.mli','+44-2080997262','+44-8081890112','GMT-00:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Marshal Islands','MH','MHL',692,'countries.mhl','+44-2080997262','+44-8081890112','GMT+12:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Martinique','MQ','MTQ',596,'countries.mtq','+44-2080997262','+44-8081890112','GMT-04:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Mauritania','MR','MRT',222,'countries.mrt','+973-16199107','+44-8081890112','GMT-00:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Mauritius','MU','MUS',230,'countries.mus','+44-2080997262','+44-8081890112','GMT+04:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Mexico','MX','MEX',52,'countries.mex','52-55-12077446','018001233331','GMT-06:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Moldova','MD','MDA',373,'countries.mda','+44-2080997262','+44-8081890112','GMT+02:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Monaco','MC','MCO',377,'countries.mco','+44-2080997262','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Mongolia','MN','MNG',976,'countries.mng','+44-2080997262','+44-8081890112','GMT+08:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Montserrat','MS','MSR',1664,'countries.msr','+44-2080997262','+44-8081890112','GMT-00:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Mozambique','MZ','MOZ',258,'countries.moz','+44-2080997262','+44-8081890112','GMT+02:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Myanmar','MM','MMR',95,'countries.mmr','+44-2080997262','+44-8081890112','GMT+06:30');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Namibia','NA','NAM',264,'countries.nam','+44-2080997262','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Nauru','NR','NRU',674,'countries.nru','+44-2080997262','+44-8081890112','GMT+12:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Nepal','NP','NPL',977,'countries.npl','+44-2080997262','+44-8081890112','GMT+05:30');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Netherlands','NL','NLD',31,'countries.nld','+31-202410310','08004049860','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('New Caledonia','NC','NCL',687,'countries.ncl','+44-2080997262','+44-8081890112','GMT+11:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('New Zealand','NZ','NZL',64,'countries.nzl','+44-2080997262','+44-8081890112','GMT+12:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Nicaragua','NI','NIC',505,'countries.nic','+44-2080997262','+44-8081890112','GMT-06:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Niger','NE','NER',227,'countries.ner','+44-2080997262','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Nigeria','NG','NGA',234,'countries.nga','+44-2080997262','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Norfolk Island','NF','NFK',672,'countries.nfk','+44-2080997262','+44-8081890112','GMT+11:30');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('North Korea','KP','PRK',850,'countries.prk','+44-2080997262','+44-8081890112','GMT+09:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Norway','NO','NOR',47,'countries.nor','+44-2080997262','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Oman','OM','OMN',968,'countries.omn','+973-16199107','+44-8081890112','GMT+04:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Palau','PW','PLW',680,'countries.plw','+44-2080997262','+44-8081890112','GMT+09:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Panama','PA','PAN',507,'countries.pan','+44-2080997262','+44-8081890112','GMT-05:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Papua New Guinea','PG','PNG',675,'countries.png','+44-2080997262','+44-8081890112','GMT+10:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Paraguay','PY','PRY',595,'countries.pry','+44-2080997262','+44-8081890112','GMT-04:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Peru','PE','PER',51,'countries.per','+44-2080997262','+44-8081890112','GMT-05:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Pitcairn','PN','PCN',0,'countries.pcn','+44-2080997262','+44-8081890112','GMT-00:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Poland','PL','POL',48,'countries.pol','+44-2080997262','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Portugal','PT','PRT',351,'countries.prt','+44-2080997262','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Puerto Rico','PR','PRI',1787,'countries.pri','+44-2080997262','+44-8081890112','GMT-04:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Albania','AL','ALB',355,'countries.alb','+44-2080997262','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Angola','AO','AGO',244,'countries.ago','+44-2080997262','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Antigua and Barbuda','AG','ATG',1268,'countries.atg','+44-2080997262','+44-8081890112','GMT-04:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Austria','AT','AUT',43,'countries.aut','43720881671','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Belize','BZ','BLZ',501,'countries.blz','+44-2080997262','+44-8081890112','GMT-06:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Bolivia','BO','BOL',591,'countries.bol','+44-2080997262','+44-8081890112','GMT-04:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Brazil','BR','BRA',55,'countries.bra','+44-2080997262','+44-8081890112','GMT-02:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Burkina Faso','BF','BFA',226,'countries.bfa','+44-2080997262','+44-8081890112','GMT-00:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Chile','CL','CHL',56,'countries.chl','+44-2080997262','+44-8081890112','GMT-03:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Cocos Islands','CC','CCK',61,'countries.cck','+44-2080997262','+44-8081890112','GMT-00:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Cook Islands','CK','COK',682,'countries.cok','+44-2080997262','+44-8081890112','GMT-10:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Cyprus','CY','CYP',357,'countries.cyp','35722030398','357-22030417','GMT+02:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Djibouti','DJ','DJI',253,'countries.dji','+973-16199107','+44-8081890112','GMT+03:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Eritrea','ER','ERI',291,'countries.eri','+44-2080997262','+44-8081890112','GMT+03:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('French Guiana','GF','GUF',594,'countries.guf','+44-2080997262','+44-8081890112','GMT-03:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Georgia','GE','GEO',995,'countries.geo','+44-2080997262','+44-8081890112','GMT+04:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Greece','GR','GRC',30,'countries.grc','+44-2080997262','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Serbia','RS','SRB',381,'countries.srb','08009174606','08004049860','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Montenegro','ME','MNE',381,'countries.mne','08009174606','08004049860','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Uganda','UG','UGA',256,'countries.uga','+44-2080997262','+44-8081890112','GMT+03:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Bahrain','BH','BHR',973,'countries.bhr','+973-16199107','+44-8081890112','GMT+03:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Congo','CD','COG',242,'countries.cog','+44-2080997262','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Guatemala','GT','GTM',502,'countries.gtm','+44-2080997262','+44-8081890112','GMT-06:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Bahamas','BS','BHS',1242,'countries.bhs','+44-2080997262','+44-8081890112','GMT-05:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Bhutan','BT','BTN',975,'countries.btn','+44-2080997262','+44-8081890112','GMT+06:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Croatia','HR','HRV',385,'countries.hrv','+44-2080997262','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Finland','FI','FIN',358,'countries.fin','+44-2080997262','+44-8081890112','GMT+02:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Honduras','HN','HND',504,'countries.hnd','+44-2080997262','+44-8081890112','GMT-06:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Kenya','KE','KEN',254,'countries.ken','+44-2080997262','+44-8081890112','GMT+03:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Luxembourg','LU','LUX',352,'countries.lux','+44-2080997262','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Samoa','WS','WSM',685,'countries.wsm','+44-2080997262','+44-8081890112','GMT-11:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('South Africa','ZA','ZAF',27,'countries.zaf','0800982374','0800982383','GMT+02:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Thailand','TH','THA',66,'countries.tha','+44-2080997262','+44-8081890112','GMT+07:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Uruguay','UY','URY',598,'countries.ury','+44-2080997262','+44-8081890112','GMT-03:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Virgin Islands US','VI','VIR',1340,'countries.vir','+44-2080997262','+44-8081890112','GMT-00:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Gibraltar','GI','GIB',350,'countries.gib','+44-2080997262','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Micronesia','FM','FSM',691,'countries.fsm','+44-2080997262','+44-8081890112','GMT-00:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Faroe Islands','FO','FRO',298,'countries.fro','+44-2080997262','+44-8081890112','GMT-00:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Afghanistan','AF','AFG',93,'countries.afg','+44-2080997262','+44-8081890112','GMT+04:30');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Antarctica','AQ','ATA',672,'countries.ata','+44-2080997262','+44-8081890112','GMT-02:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Bangladesh','BD','BGD',880,'countries.bgd','+44-2080997262','+44-8081890112','GMT+07:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Barbados','BB','BRB',1246,'countries.brb','+44-2080997262','+44-8081890112','GMT-04:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Bouvet Island','BV','BVT',0,'countries.bvt','+44-2080997262','+44-8081890112','GMT-00:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Canada','CA','CAN',1,'countries.can','15148190839','+44-8081890112','GMT-05:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Cape Verde','CV','CPV',238,'countries.cpv','+44-2080997262','+44-8081890112','GMT-01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Christmas Island','CX','CXR',61,'countries.cxr','+44-2080997262','+44-8081890112','GMT-10:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Denmark','DK','DNK',45,'countries.dnk','+44-2080997262','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Ecuador','EC','ECU',593,'countries.ecu','+44-2080997262','+44-8081890112','GMT-05:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('El Salvador','SV','SLV',503,'countries.slv','+44-2080997262','+44-8081890112','GMT-06:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('French Polynesia','PF','PYF',689,'countries.pyf','+44-2080997262','+44-8081890112','GMT-10:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Grenada','GD','GRD',1473,'countries.grd','+44-2080997262','+44-8081890112','GMT-04:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Haiti','HT','HTI',509,'countries.hti','+44-2080997262','+44-8081890112','GMT-05:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Iran','IR','IRN',98,'countries.irn','+44-2080997262','+44-8081890112','GMT+03:30');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Japan','JP','JPN',81,'countries.jpn','+44-2080997262','+44-8081890112','GMT+09:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Laos','LA','LAO',856,'countries.lao','+44-2080997262','+44-8081890112','GMT+07:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Libya','LY','LBY',218,'countries.lby','+973-16199107','+44-8081890112','GMT+02:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Macedonia','MK','MKD',389,'countries.mkd','+44-2080997262','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Malta','MT','MLT',356,'countries.mlt','+44-2080997262','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Mayotte Island','YT','MYT',269,'countries.myt','+44-2080997262','+44-8081890112','GMT+03:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Morocco','MA','MAR',212,'countries.mar','+973-16199107','+44-8081890112','GMT-00:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Netherlands Antilles','AN','ANT',599,'countries.ant','+44-2080997262','+44-8081890112','GMT-04:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Niue','NU','NIU',683,'countries.niu','+44-2080997262','+44-8081890112','GMT-11:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Pakistan','PK','PAK',92,'countries.pak','+44-2080997262','+44-8081890112','GMT+05:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Philippines','PH','PHL',63,'countries.phl','+44-2080997262','+44-8081890112','GMT+08:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Qatar','QA','QAT',974,'countries.qat','+973-16199107','+44-8081890112','GMT+03:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Romania','RO','ROU',40,'countries.rom','+44-2080997262','+44-8081890112','GMT+02:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Saudi Arabia','SA','SAU',966,'countries.sau','+973-16199107','+44-8081890112','GMT+03:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Slovakia','SK','SVK',421,'countries.svk','+44-2080997262','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Sri Lanka','LK','LKA',94,'countries.lka','+44-2080997262','+44-8081890112','GMT+05:30');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Switzerland','CH','CHE',41,'countries.che','41435080960','+44-8081890112','GMT+01:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Togo','TG','TGO',228,'countries.tgo','+44-2080997262','+44-8081890112','GMT-00:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Turkey','TR','TUR',90,'countries.tur','902129880247','00-800-8529-2724','GMT+02:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('United States','US','USA',1,'countries.usa','18668444540','18667343034','GMT-06:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('Virgin Islands UK','VG','VGB',0,'countries.vgb','+44-2080997262','+44-8081890112','GMT-04:00');
Insert into anycapital.COUNTRIES (COUNTRY_NAME,A2,A3,PHONE_CODE,DISPLAY_NAME,SUPPORT_PHONE,SUPPORT_FAX,GMT_OFFSET) values ('United Kingdom','GB','GBR',44,'countries.gbr','44-20-35141216','08004049860','GMT-00:00');

-- insert to PRODUCTS 
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1272,str_to_date('22/11/2016 13:57.25','%d/%m/%Y %H:%i.%s'),3,3,100,3,100000,6,4,0,0,95,0,0,0,str_to_date('21/11/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('29/11/2016 23:59.59','%d/%m/%Y %H:%i.%s'),str_to_date('29/11/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('04/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('04/12/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('04/12/2019 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('04/12/2019 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('08/12/2019 00:00.00','%d/%m/%Y %H:%i.%s'),0,0,0,98.95,99.95,null,98.5,100000000,13,0,7);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1273,str_to_date('23/11/2016 12:22.04','%d/%m/%Y %H:%i.%s'),3,2,100,3,100000,1,4,0,0,95,110,100,0,str_to_date('22/11/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('08/12/2016 23:59.59','%d/%m/%Y %H:%i.%s'),str_to_date('08/12/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('11/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('11/12/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('11/12/2019 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('11/12/2019 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('15/12/2019 00:00.00','%d/%m/%Y %H:%i.%s'),0,1,0,98.99,99.99,null,92,300000000,14,0,7);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1282,str_to_date('24/11/2016 10:29.04','%d/%m/%Y %H:%i.%s'),3,5,100,3,100000,5,4,0,0,0,0,0,0,str_to_date('23/11/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('07/12/2016 23:59.59','%d/%m/%Y %H:%i.%s'),str_to_date('07/12/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('11/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('11/12/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('09/12/2019 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('09/12/2019 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('11/12/2019 00:00.00','%d/%m/%Y %H:%i.%s'),0,0,0,101,102,null,100,100000000,15,0,7);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1283,str_to_date('24/11/2016 10:39.41','%d/%m/%Y %H:%i.%s'),3,3,100,3,100000,6,4,0,0,95,0,0,0,str_to_date('23/11/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('07/12/2016 23:59.59','%d/%m/%Y %H:%i.%s'),str_to_date('07/12/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('11/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('11/12/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('09/12/2018 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('09/12/2018 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('11/12/2018 00:00.00','%d/%m/%Y %H:%i.%s'),0,0,0,99.5,100.5,null,94,100000000,16,0,6);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1284,str_to_date('24/11/2016 10:52.36','%d/%m/%Y %H:%i.%s'),3,5,100,3,100000,5,4,0,0,0,0,0,0,str_to_date('23/11/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('07/12/2016 23:59.59','%d/%m/%Y %H:%i.%s'),str_to_date('07/12/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('11/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('11/12/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('09/12/2019 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('09/12/2019 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('11/12/2019 00:00.00','%d/%m/%Y %H:%i.%s'),0,0,1,100,101,null,100,100000000,17,0,7);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1332,str_to_date('11/12/2016 14:21.30','%d/%m/%Y %H:%i.%s'),3,8,100,2,100000,1,null,0,0,0,150,100,0,str_to_date('11/12/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('25/12/2016 23:59.59','%d/%m/%Y %H:%i.%s'),str_to_date('25/12/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('27/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('27/12/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('25/12/2019 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('25/12/2019 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('29/12/2019 00:00.00','%d/%m/%Y %H:%i.%s'),0,1,0,98.9,99.8,null,95.5,300000000,23,0,7);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1504,str_to_date('12/01/2017 08:48.47','%d/%m/%Y %H:%i.%s'),4,8,100,3,100000,1,null,0,0,0,250,100,0,str_to_date('02/01/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('31/01/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('30/01/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('07/02/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('06/02/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('30/01/2017 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('30/01/2017 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('06/02/2017 22:00.00','%d/%m/%Y %H:%i.%s'),0,1,0,95,96,null,100,100000000,25,122.5,4);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1538,str_to_date('25/01/2017 15:03.02','%d/%m/%Y %H:%i.%s'),2,1,100,3,100000,1,null,0,0,95,80,100,0,str_to_date('24/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('07/02/2017 23:59.59','%d/%m/%Y %H:%i.%s'),str_to_date('07/02/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('09/02/2017 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('09/02/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('07/02/2018 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('07/02/2018 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('11/02/2018 00:00.00','%d/%m/%Y %H:%i.%s'),0,1,0,null,null,null,100,100000000,34,0,4);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1539,str_to_date('25/01/2017 15:06.05','%d/%m/%Y %H:%i.%s'),2,5,100,3,100000,5,4,0,0,0,0,0,0,str_to_date('24/01/2017 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('07/02/2017 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('07/02/2017 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('09/02/2017 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('09/02/2017 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('07/02/2018 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('07/02/2018 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('11/02/2018 22:00.00','%d/%m/%Y %H:%i.%s'),0,0,0,null,null,null,100,100000000,33,0,4);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1271,str_to_date('22/11/2016 11:05.20','%d/%m/%Y %H:%i.%s'),3,5,100,3,100000,5,4,0,0,0,0,0,0,str_to_date('20/11/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('08/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('07/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('14/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('13/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('14/12/2017 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('14/12/2017 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('18/12/2017 22:00.00','%d/%m/%Y %H:%i.%s'),0,0,0,98.99,99.99,null,99.4,100000000,11,0,4);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1248,str_to_date('17/11/2016 09:00.37','%d/%m/%Y %H:%i.%s'),3,3,100,3,100000,6,4,0,0,100,0,0,0,str_to_date('15/11/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('23/11/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('23/11/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('26/11/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('26/11/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('24/11/2021 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('24/11/2021 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('28/11/2021 22:00.00','%d/%m/%Y %H:%i.%s'),0,0,0,2,2,null,92.37,100000000,10,0,8);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1249,str_to_date('17/11/2016 10:02.49','%d/%m/%Y %H:%i.%s'),3,2,100,3,100000,1,4,0,0,95,120,100,0,str_to_date('15/11/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('24/11/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('23/11/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('27/11/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('26/11/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('26/11/2018 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('26/11/2018 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('28/11/2018 22:00.00','%d/%m/%Y %H:%i.%s'),0,1,0,98.92,99.92,null,95,200000000,9,0,6);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1251,str_to_date('17/11/2016 10:10.13','%d/%m/%Y %H:%i.%s'),3,1,100,3,100000,1,4,0,0,90,80,100,0,str_to_date('15/11/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('29/11/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('28/11/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('30/11/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('29/11/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('29/11/2017 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('29/11/2017 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('03/12/2017 22:00.00','%d/%m/%Y %H:%i.%s'),0,1,0,98.82,99.82,null,90,100000000,29,0,4);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1253,str_to_date('17/11/2016 10:34.56','%d/%m/%Y %H:%i.%s'),3,5,100,3,100000,5,4,0,0,0,0,0,0,str_to_date('16/11/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('29/11/2016 23:59.59','%d/%m/%Y %H:%i.%s'),str_to_date('29/11/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('30/11/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('30/11/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('01/12/2019 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('01/12/2019 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('03/12/2019 00:00.00','%d/%m/%Y %H:%i.%s'),0,0,0,99.32,100.32,null,100,200000000,8,0,7);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1254,str_to_date('17/11/2016 11:05.25','%d/%m/%Y %H:%i.%s'),3,7,100,3,100000,5,4,0,0,0,0,100,0,str_to_date('16/11/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('24/11/2016 23:59.59','%d/%m/%Y %H:%i.%s'),str_to_date('24/11/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('27/11/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('27/11/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('25/05/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('25/05/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('29/05/2017 00:00.00','%d/%m/%Y %H:%i.%s'),0,1,0,98.63,99.63,null,100,200000000,6,0,2);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1255,str_to_date('17/11/2016 11:56.05','%d/%m/%Y %H:%i.%s'),3,4,100,3,100000,5,4,0,0,0,0,75,0,str_to_date('16/11/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('14/12/2016 23:59.59','%d/%m/%Y %H:%i.%s'),str_to_date('14/12/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('18/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('18/12/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('17/05/2018 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('17/05/2018 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('21/05/2018 00:00.00','%d/%m/%Y %H:%i.%s'),1,0,0,96,97,null,100,300000000,5,0,5);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1257,str_to_date('17/11/2016 12:10.26','%d/%m/%Y %H:%i.%s'),3,9,100,3,100000,1,null,0,0,0,100,100,100,str_to_date('16/11/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('24/11/2016 23:59.59','%d/%m/%Y %H:%i.%s'),str_to_date('24/11/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('27/11/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('27/11/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('27/11/2019 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('27/11/2019 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('01/12/2019 00:00.00','%d/%m/%Y %H:%i.%s'),1,1,0,98.43,99.43,null,99.2,200000000,4,0,7);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1258,str_to_date('17/11/2016 12:35.31','%d/%m/%Y %H:%i.%s'),3,8,100,3,100000,1,null,0,0,0,115,100,0,str_to_date('16/11/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('14/12/2016 23:59.59','%d/%m/%Y %H:%i.%s'),str_to_date('14/12/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('18/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('18/12/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('18/12/2019 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('18/12/2019 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('22/12/2019 00:00.00','%d/%m/%Y %H:%i.%s'),0,0,0,99.3,100.3,null,96,500000000,3,0,7);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1259,str_to_date('17/11/2016 13:40.55','%d/%m/%Y %H:%i.%s'),3,6,100,3,100000,5,4,0,0,0,0,0,0,str_to_date('15/11/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('24/11/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('23/11/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('29/11/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('28/11/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('28/11/2018 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('28/11/2018 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('02/12/2018 22:00.00','%d/%m/%Y %H:%i.%s'),0,0,0,null,null,null,100,1000000000,2,0,6);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1400,str_to_date('26/12/2016 10:24.17','%d/%m/%Y %H:%i.%s'),5,5,100,3,100000,5,4,0,0,0,0,0,0,str_to_date('25/12/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('03/01/2017 23:59.59','%d/%m/%Y %H:%i.%s'),str_to_date('04/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('04/01/2017 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('04/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('15/01/2019 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('15/01/2019 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('17/01/2019 00:00.00','%d/%m/%Y %H:%i.%s'),0,0,0,101,102,null,99.5,100000000,23,0,6);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1501,str_to_date('02/01/2017 10:33.32','%d/%m/%Y %H:%i.%s'),3,1,100,2,100000,1,4,0,0,90,130,100,0,str_to_date('01/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('19/01/2017 23:59.59','%d/%m/%Y %H:%i.%s'),str_to_date('19/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('22/01/2017 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('22/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('22/01/2020 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('22/01/2020 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('26/01/2020 00:00.00','%d/%m/%Y %H:%i.%s'),0,1,0,101,102,null,98,200000000,24,0,7);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1502,str_to_date('02/01/2017 10:56.17','%d/%m/%Y %H:%i.%s'),3,10,100,3,100000,1,null,0,0,0,100,100,115,str_to_date('01/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('19/01/2017 23:59.59','%d/%m/%Y %H:%i.%s'),str_to_date('19/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('22/01/2017 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('22/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('22/01/2020 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('22/01/2020 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('26/01/2020 00:00.00','%d/%m/%Y %H:%i.%s'),1,0,0,96,97,null,99.5,100000000,28,0,7);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1503,str_to_date('02/01/2017 11:08.56','%d/%m/%Y %H:%i.%s'),3,7,100,2,100000,5,4,0,0,0,0,100,0,str_to_date('01/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('19/01/2017 23:59.59','%d/%m/%Y %H:%i.%s'),str_to_date('19/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('22/01/2017 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('22/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('20/07/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('20/07/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('24/07/2017 00:00.00','%d/%m/%Y %H:%i.%s'),0,0,0,96,97,null,99.5,300000000,12,0,2);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1527,str_to_date('19/01/2017 09:08.09','%d/%m/%Y %H:%i.%s'),5,8,100,3,100000,1,null,0,0,0,160,100,0,str_to_date('17/01/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('24/01/2016 23:59.59','%d/%m/%Y %H:%i.%s'),str_to_date('24/01/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('26/01/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('26/01/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('24/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('24/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('26/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),0,0,0,106,107,null,100,200000000,262,0,4);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1528,str_to_date('19/01/2017 09:27.37','%d/%m/%Y %H:%i.%s'),5,9,100,3,100000,1,null,0,0,0,100,100,110,str_to_date('17/01/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('24/01/2016 23:59.59','%d/%m/%Y %H:%i.%s'),str_to_date('24/01/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('26/01/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('26/01/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('24/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('24/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('26/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),0,0,0,107,108,null,100,200000000,263,0,4);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1529,str_to_date('19/01/2017 09:32.41','%d/%m/%Y %H:%i.%s'),5,2,100,3,100000,1,4,0,0,90,140,100,0,str_to_date('17/01/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('24/01/2016 23:59.59','%d/%m/%Y %H:%i.%s'),str_to_date('24/01/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('26/01/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('26/01/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('24/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('24/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('26/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),0,0,0,90,91,null,100,200000000,264,0,4);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1530,str_to_date('19/01/2017 09:43.46','%d/%m/%Y %H:%i.%s'),2,1,100,3,100000,1,null,0,0,90,80,100,0,str_to_date('17/01/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('24/01/2017 23:59.59','%d/%m/%Y %H:%i.%s'),str_to_date('24/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('26/01/2017 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('26/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('24/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('24/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('26/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),0,0,0,108.35,109.3,null,100,200000000,30,0,4);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1532,str_to_date('19/01/2017 09:54.20','%d/%m/%Y %H:%i.%s'),5,3,100,2,100000,5,4,0,0,90,0,0,0,str_to_date('17/07/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('24/07/2016 23:59.59','%d/%m/%Y %H:%i.%s'),str_to_date('24/07/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('26/07/2016 21:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('26/07/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('24/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('24/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('26/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),0,1,0,111,112,null,100,100000000,267,0,2);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1533,str_to_date('19/01/2017 10:05.42','%d/%m/%Y %H:%i.%s'),5,5,100,3,100000,5,4,0,0,0,0,0,0,str_to_date('18/01/2015 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('25/01/2015 23:59.59','%d/%m/%Y %H:%i.%s'),str_to_date('25/01/2015 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('27/01/2015 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('27/01/2015 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('25/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('25/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('29/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),0,0,0,104,105,null,99.1,200000000,268,0,6);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1534,str_to_date('19/01/2017 16:20.46','%d/%m/%Y %H:%i.%s'),3,2,2,2,200,1,4,2,2,2,2,2,0,str_to_date('04/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('21/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('06/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('07/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('08/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('09/01/2018 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('09/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('11/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),1,1,2,2,2,null,2,200,32,0,6);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1566,str_to_date('01/02/2017 08:56.47','%d/%m/%Y %H:%i.%s'),1,2,100,3,100000,1,4,312,123,22,234,0,0,str_to_date('11/03/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('10/03/2017 23:59.59','%d/%m/%Y %H:%i.%s'),str_to_date('18/02/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('21/02/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('19/02/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('21/02/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('28/02/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('16/02/2017 00:00.00','%d/%m/%Y %H:%i.%s'),0,1,2,null,null,null,423,11100,301,0,6);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1401,str_to_date('29/12/2016 10:05.04','%d/%m/%Y %H:%i.%s'),3,6,100,3,100000,5,4,0,0,0,0,0,0,str_to_date('28/12/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('12/01/2017 23:59.59','%d/%m/%Y %H:%i.%s'),str_to_date('12/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('15/01/2017 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('15/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('15/01/2018 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('15/01/2018 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('17/01/2018 00:00.00','%d/%m/%Y %H:%i.%s'),0,1,0,98,99,null,99,100000000,1,0,4);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1286,str_to_date('28/11/2016 10:12.54','%d/%m/%Y %H:%i.%s'),3,7,100,3,100000,5,4,0,0,0,0,100,0,str_to_date('27/11/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('08/12/2016 23:59.59','%d/%m/%Y %H:%i.%s'),str_to_date('08/12/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('11/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('11/12/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('09/12/2018 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('09/12/2018 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('11/12/2018 00:00.00','%d/%m/%Y %H:%i.%s'),0,0,0,100,101,null,99,100000000,18,0,6);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1287,str_to_date('28/11/2016 13:09.50','%d/%m/%Y %H:%i.%s'),3,7,100,3,100000,5,4,0,0,0,0,100,0,str_to_date('27/11/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('08/12/2016 23:59.59','%d/%m/%Y %H:%i.%s'),str_to_date('08/12/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('11/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('11/12/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('09/12/2018 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('09/12/2018 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('11/12/2018 00:00.00','%d/%m/%Y %H:%i.%s'),0,0,1,98.5,99.5,null,99,100000000,19,0,6);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1288,str_to_date('28/11/2016 13:41.05','%d/%m/%Y %H:%i.%s'),3,4,100,3,100000,5,4,0,0,0,0,80,0,str_to_date('27/11/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('15/12/2016 23:59.59','%d/%m/%Y %H:%i.%s'),str_to_date('15/12/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('18/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('18/12/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('18/09/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('18/09/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('20/09/2017 00:00.00','%d/%m/%Y %H:%i.%s'),0,0,2,101,102,null,99,200000000,20,0,3);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1289,str_to_date('28/11/2016 14:20.04','%d/%m/%Y %H:%i.%s'),3,9,100,3,100000,1,null,0,0,0,100,100,105.5,str_to_date('26/11/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('15/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('14/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('18/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('17/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('17/12/2019 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('17/12/2019 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('21/12/2019 22:00.00','%d/%m/%Y %H:%i.%s'),0,1,0,99.2,100.2,null,98,300000000,21,0,7);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1290,str_to_date('28/11/2016 14:46.53','%d/%m/%Y %H:%i.%s'),3,9,100,3,100000,1,null,0,0,0,100,100,107,str_to_date('27/11/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('15/12/2016 23:59.59','%d/%m/%Y %H:%i.%s'),str_to_date('15/12/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('18/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('18/12/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('18/12/2019 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('18/12/2019 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('22/12/2019 00:00.00','%d/%m/%Y %H:%i.%s'),0,1,0,99.5,100.5,null,98,300000000,31,0,7);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1291,str_to_date('28/11/2016 14:57.03','%d/%m/%Y %H:%i.%s'),3,8,100,3,100000,1,null,0,0,0,180,100,0,str_to_date('26/11/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('15/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('14/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('18/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('17/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('15/12/2021 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('15/12/2021 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('19/12/2021 22:00.00','%d/%m/%Y %H:%i.%s'),0,0,0,98.5,99.5,null,98,300000000,22,190,8);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1292,str_to_date('29/11/2016 09:09.46','%d/%m/%Y %H:%i.%s'),3,2,100,3,100000,1,4,0,0,95,115,100,0,str_to_date('28/11/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('15/12/2016 23:59.59','%d/%m/%Y %H:%i.%s'),str_to_date('15/12/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('18/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('18/12/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('18/06/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('18/06/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('20/06/2017 00:00.00','%d/%m/%Y %H:%i.%s'),0,0,0,100.5,101.5,null,94.5,100000000,7,0,2);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1309,str_to_date('06/12/2016 09:20.22','%d/%m/%Y %H:%i.%s'),3,8,100,2,100000,1,null,0,0,0,160,100,0,str_to_date('05/12/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('22/12/2016 23:59.59','%d/%m/%Y %H:%i.%s'),str_to_date('22/12/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('25/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('25/12/2016 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('25/12/2019 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('25/12/2019 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('29/12/2019 00:00.00','%d/%m/%Y %H:%i.%s'),0,1,0,101,102,null,96.5,300000000,27,0,7);
Insert into anycapital.PRODUCTS (ID,TIME_CREATED,PRODUCT_STATUS_ID,PRODUCT_TYPE_ID,ISSUE_PRICE,CURRENCY_ID,DENOMINATION,PRODUCT_COUPON_FREQUENCY_ID,DAY_COUNT_CONVENTION_ID,MAX_YIELD,MAX_YIELD_P_A,LEVEL_OF_PROTECTION,LEVEL_OF_PARTICIPATION,STRIKE_LEVEL,BONUS_LEVEL,SUBSCRIPTION_START_DATE,SUBSCRIPTION_END_DATE,INITIAL_FIXING_DATE,ISSUE_DATE,FIRST_EXCHANGE_TRADING_DATE,LAST_TRADING_DATE,FINAL_FIXING_DATE,REDEMPTION_DATE,IS_SUSPEND,IS_QUANTO,PRODUCT_INSURANCE_TYPE_ID,BID,ASK,FORMULA_RESULT,BOND_FLOOR_AT_ISSUANCE,ISSUE_SIZE,PRIORITY,CAP_LEVEL,PRODUCT_MATURITY_ID) values (1355,str_to_date('13/12/2016 09:58.11','%d/%m/%Y %H:%i.%s'),3,8,100,3,100000,1,null,0,0,0,170,100,0,str_to_date('11/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('22/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('21/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('25/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('24/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('21/12/2019 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('21/12/2019 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('25/12/2019 22:00.00','%d/%m/%Y %H:%i.%s'),0,1,0,100.5,101.5,null,99,100000000,26,150,7);

-- insert PRODUCT_SIMULATION
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (125,140,107097,1,1271);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (126,120,107097,2,1271);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (127,100,107097,3,1271);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (128,80,107097,4,1271);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (129,60,67097,5,1271);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (130,40,47097,6,1271);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (131,160,131133,1,1272);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (132,140,131133,2,1272);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (133,120,131133,3,1272);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (134,100,95000,4,1272);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (135,80,95000,5,1272);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (136,60,95000,6,1272);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (137,140,95000,1,1273);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (138,130,95000,2,1273);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (139,120,117000,3,1273);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (140,110,106000,4,1273);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (141,100,95000,5,1273);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (142,90,95000,6,1273);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (143,80,95000,7,1273);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (144,70,95000,8,1273);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (177,130,124000,1,1282);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (178,120,124000,2,1282);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (179,110,124000,3,1282);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (180,100,124000,4,1282);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (181,90,124000,5,1282);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (182,80,124000,6,1282);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (183,70,124000,7,1282);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (184,60,124000,8,1282);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (185,50,74000,9,1282);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (186,40,64000,10,1282);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (187,140,125000,1,1283);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (188,130,125000,2,1283);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (189,120,125000,3,1283);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (190,110,95000,4,1283);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (191,100,95000,5,1283);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (192,90,95000,6,1283);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (193,80,95000,7,1283);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (194,70,95000,8,1283);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (195,60,95000,9,1283);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (196,130,121000,1,1284);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (197,120,121000,2,1284);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (198,110,121000,3,1284);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (199,100,121000,4,1284);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (200,90,121000,5,1284);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (201,80,121000,6,1284);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (202,70,91000,7,1284);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (203,60,81000,8,1284);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (204,50,71000,9,1284);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (393,180,220000,1,1332);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (394,160,190000,2,1332);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (395,140,160000,3,1332);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (396,120,130000,4,1332);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (397,100,100000,5,1332);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (398,80,80000,6,1332);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (399,60,60000,7,1332);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (400,40,40000,8,1332);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (879,140,122500,1,1504);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (880,130,122500,2,1504);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (881,120,122500,3,1504);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (882,110,122500,4,1504);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (883,105,112500,5,1504);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (884,100,100000,6,1504);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (885,90,90000,7,1504);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (886,70,70000,8,1504);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (887,50,50000,9,1504);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (1015,180,159000,1,1538);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (1016,160,143000,2,1538);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (1017,140,127000,3,1538);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (1018,120,111000,4,1538);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (1019,100,95000,5,1538);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (1020,80,95000,6,1538);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (1021,60,95000,7,1538);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (1022,40,95000,8,1538);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (1023,140,108022,1,1539);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (1024,120,108022,2,1539);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (1025,100,108022,3,1539);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (1026,80,108022,4,1539);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (1027,60,68022,5,1539);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (1028,40,48022,6,1539);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (13,150,170077,1,1248);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (14,130,170077,2,1248);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (15,120,170077,3,1248);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (16,110,170077,4,1248);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (17,100,100000,5,1248);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (18,90,100000,6,1248);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (19,80,100000,7,1248);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (20,70,100000,8,1248);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (21,150,95000,1,1249);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (22,140,95000,2,1249);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (23,130,95000,3,1249);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (24,120,119000,4,1249);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (25,110,107000,5,1249);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (26,100,95000,6,1249);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (27,90,95000,7,1249);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (28,80,95000,8,1249);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (29,70,95000,9,1249);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (30,60,95000,10,1249);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (38,160,138000,1,1251);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (39,140,122000,2,1251);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (40,120,106000,3,1251);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (41,100,90000,4,1251);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (42,80,90000,5,1251);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (43,60,90000,6,1251);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (50,130,116245,1,1253);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (51,110,116245,2,1253);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (52,90,116245,3,1253);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (53,70,116245,4,1253);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (54,50,66245,5,1253);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (55,130,575238,1,1254);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (56,110,575238,2,1254);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (57,90,555577,3,1254);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (58,80,444461,4,1254);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (59,70,388903,5,1254);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (60,60,333346,6,1254);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (61,130,114250,1,1255);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (62,120,114250,2,1255);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (63,100,114250,3,1255);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (64,90,114250,4,1255);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (65,80,114250,5,1255);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (66,70,107583,6,1255);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (67,60,94250,7,1255);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (68,180,180000,1,1257);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (69,160,160000,2,1257);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (70,140,140000,3,1257);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (71,120,120000,4,1257);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (72,100,100000,5,1257);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (73,80,100000,6,1257);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (74,60,60000,7,1257);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (75,40,40000,8,1257);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (76,180,192000,1,1258);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (77,160,169000,2,1258);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (78,140,146000,3,1258);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (79,120,123000,4,1258);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (80,100,100000,5,1258);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (81,80,80000,6,1258);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (82,60,60000,7,1258);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (83,220,-11955,1,1259);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (84,200,8044,2,1259);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (85,180,28044,3,1259);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (86,160,48044,4,1259);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (87,140,68044,5,1259);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (88,120,88044,6,1259);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (89,100,108044,7,1259);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (90,80,108044,8,1259);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (541,150,122061,1,1400);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (542,120,122061,2,1400);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (543,80,122061,3,1400);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (544,70,122061,4,1400);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (545,60,82061,5,1400);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (546,40,62061,6,1400);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (852,180,194000,1,1501);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (853,160,168000,2,1501);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (854,140,142000,3,1501);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (855,120,116000,4,1501);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (856,100,90000,5,1501);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (857,80,90000,6,1501);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (858,60,90000,7,1501);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (859,40,90000,8,1501);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (860,200,200000,1,1502);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (861,180,180000,2,1502);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (862,160,160000,3,1502);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (863,140,140000,4,1502);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (864,120,120000,5,1502);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (865,100,115000,6,1502);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (866,80,115000,7,1502);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (867,60,60000,8,1502);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (868,40,40000,9,1502);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (869,160,108088,1,1503);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (870,140,108088,2,1503);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (871,120,108088,3,1503);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (872,110,108088,4,1503);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (873,100,100000,5,1503);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (874,90,100000,6,1503);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (875,80,100000,7,1503);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (876,70,100000,8,1503);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (877,60,60000,9,1503);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (878,40,40000,10,1503);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (954,180,228000,1,1527);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (955,160,196000,2,1527);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (956,140,164000,3,1527);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (957,120,132000,4,1527);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (958,100,100000,5,1527);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (959,80,80000,6,1527);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (960,60,60000,7,1527);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (961,40,40000,8,1527);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (962,180,180000,1,1528);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (963,160,160000,2,1528);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (964,140,140000,3,1528);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (965,120,120000,4,1528);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (966,100,110000,5,1528);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (967,80,110000,6,1528);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (968,60,60000,7,1528);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (969,40,40000,8,1528);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (970,180,90000,1,1529);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (971,160,90000,2,1529);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (972,140,90000,3,1529);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (973,120,118000,4,1529);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (974,100,90000,5,1529);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (975,80,90000,6,1529);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (976,60,90000,7,1529);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (977,40,90000,8,1529);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (978,180,154000,1,1530);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (979,160,138000,2,1530);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (980,140,122000,3,1530);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (981,120,106000,4,1530);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (982,100,90000,5,1530);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (983,80,90000,6,1530);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (984,60,90000,7,1530);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (985,40,90000,8,1530);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (993,160,115000,1,1532);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (994,140,115000,2,1532);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (995,120,115000,3,1532);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (996,100,90000,4,1532);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (997,80,90000,5,1532);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (998,60,90000,6,1532);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (999,40,90000,7,1532);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (1000,140,112033,1,1533);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (1001,120,112033,2,1533);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (1002,100,112033,3,1533);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (1003,80,112033,4,1533);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (1004,60,72033,5,1533);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (1005,40,52033,6,1533);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (547,220,-14972,1,1401);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (548,180,25027,2,1401);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (549,160,45027,3,1401);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (550,140,65027,4,1401);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (551,120,105027,5,1401);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (552,100,105027,6,1401);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (553,80,105027,7,1401);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (554,60,105027,8,1401);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (212,140,124000,1,1286);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (213,120,124000,2,1286);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (214,110,124000,3,1286);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (215,100,100000,4,1286);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (216,90,100000,5,1286);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (217,80,100000,6,1286);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (218,70,70000,7,1286);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (219,60,60000,8,1286);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (220,50,50000,9,1286);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (221,140,120000,1,1287);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (222,120,120000,2,1287);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (223,110,120000,3,1287);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (224,100,100000,4,1287);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (225,90,100000,5,1287);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (226,80,100000,6,1287);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (227,70,70000,7,1287);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (228,60,60000,8,1287);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (229,50,50000,9,1287);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (230,140,104533,1,1288);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (231,120,104533,2,1288);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (232,100,104533,3,1288);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (233,80,104533,4,1288);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (234,60,79533,5,1288);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (235,50,67033,6,1288);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (236,180,180000,1,1289);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (237,160,160000,2,1289);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (238,140,140000,3,1289);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (239,120,120000,4,1289);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (240,100,105500,5,1289);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (241,80,105500,6,1289);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (242,60,60000,7,1289);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (243,40,40000,8,1289);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (244,160,160000,1,1290);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (245,140,140000,2,1290);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (246,120,120000,3,1290);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (247,110,110000,4,1290);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (248,100,107000,5,1290);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (249,90,107000,6,1290);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (250,80,80000,7,1290);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (251,60,60000,8,1290);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (252,180,190000,1,1291);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (253,160,190000,2,1291);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (254,140,172000,3,1291);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (255,120,136000,4,1291);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (256,100,100000,5,1291);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (257,80,80000,6,1291);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (258,60,60000,7,1291);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (259,40,40000,8,1291);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (260,160,95000,1,1292);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (261,150,95000,2,1292);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (262,140,95000,3,1292);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (263,130,95000,4,1292);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (264,120,118000,5,1292);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (265,110,106500,6,1292);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (266,100,95000,7,1292);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (267,90,95000,8,1292);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (268,80,95000,9,1292);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (269,70,95000,10,1292);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (318,200,260000,1,1309);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (319,180,228000,2,1309);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (320,160,196000,3,1309);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (321,140,164000,4,1309);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (322,120,132000,5,1309);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (323,100,100000,6,1309);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (324,80,80000,7,1309);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (325,60,60000,8,1309);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (326,40,40000,9,1309);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (401,160,150000,1,1355);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (402,140,150000,2,1355);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (403,130,150000,3,1355);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (404,120,134000,4,1355);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (405,110,117000,5,1355);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (406,100,100000,6,1355);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (407,80,80000,7,1355);
Insert into anycapital.PRODUCT_SIMULATION (ID,FINAL_FIXING_LEVEL,CASH_SETTLEMENT,POSITION,PRODUCT_ID) values (408,60,60000,8,1355);

-- insert PRODUCT_MARKETS
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (28,1271,22,17.34,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (29,1272,31,68.2,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (30,1273,39,23.09,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (39,1282,23,46.35,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (40,1283,8,4764.07,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (41,1284,23,46.35,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (89,1332,3,7700,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (239,1504,24,78.7,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (273,1538,15,null,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (274,1539,15,null,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (5,1248,40,2,40);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (6,1249,38,56.48,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (8,1251,19,120.85,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (10,1253,32,129.85,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (11,1254,14,111.23,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (12,1255,29,13.64,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (13,1257,35,41.1,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (14,1258,1,2268,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (15,1259,48,null,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (135,1400,22,17.545,10);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (236,1501,3,7500,0);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (237,1502,29,13,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (238,1503,18,18,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (262,1527,8,4500,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (263,1528,8,4500,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (264,1529,8,5500,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (265,1530,8,4300,4700);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (267,1532,8,4000,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (268,1533,8,5000,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (136,1401,6,3280,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (269,1534,14,2,2);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (301,1566,30,null,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (43,1286,15,95.53,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (44,1287,15,95.53,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (45,1288,8,4842,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (46,1289,2,8237,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (47,1290,2,8237,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (48,1291,3,7029,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (49,1292,5,11447,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (66,1309,3,7800,null);
Insert into anycapital.PRODUCT_MARKETS (ID,PRODUCT_ID,MARKET_ID,START_TRADE_LEVEL,END_TRADE_LEVEL) values (90,1355,11,79.5,null);

-- insert PRODUCT_COUPONS
Insert into anycapital.PRODUCT_COUPONS (ID,PRODUCT_ID,OBSERVATION_DATE,PAYMENT_DATE,TRIGGER_LEVEL,PAY_RATE,IS_PAID,OBSERVATION_LEVEL) values (13,1271,str_to_date('15/12/2017 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('19/12/2017 22:00.00','%d/%m/%Y %H:%i.%s'),0,7,0,0);
Insert into anycapital.PRODUCT_COUPONS (ID,PRODUCT_ID,OBSERVATION_DATE,PAYMENT_DATE,TRIGGER_LEVEL,PAY_RATE,IS_PAID,OBSERVATION_LEVEL) values (14,1272,str_to_date('04/12/2019 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('08/12/2019 22:00.00','%d/%m/%Y %H:%i.%s'),110,12,0,0);
Insert into anycapital.PRODUCT_COUPONS (ID,PRODUCT_ID,OBSERVATION_DATE,PAYMENT_DATE,TRIGGER_LEVEL,PAY_RATE,IS_PAID,OBSERVATION_LEVEL) values (21,1282,str_to_date('09/12/2019 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('11/12/2019 22:00.00','%d/%m/%Y %H:%i.%s'),0,8,0,0);
Insert into anycapital.PRODUCT_COUPONS (ID,PRODUCT_ID,OBSERVATION_DATE,PAYMENT_DATE,TRIGGER_LEVEL,PAY_RATE,IS_PAID,OBSERVATION_LEVEL) values (22,1283,str_to_date('09/12/2018 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('11/12/2018 22:00.00','%d/%m/%Y %H:%i.%s'),110,15,0,0);
Insert into anycapital.PRODUCT_COUPONS (ID,PRODUCT_ID,OBSERVATION_DATE,PAYMENT_DATE,TRIGGER_LEVEL,PAY_RATE,IS_PAID,OBSERVATION_LEVEL) values (23,1284,str_to_date('09/12/2019 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('11/12/2019 22:00.00','%d/%m/%Y %H:%i.%s'),0,7,0,0);
Insert into anycapital.PRODUCT_COUPONS (ID,PRODUCT_ID,OBSERVATION_DATE,PAYMENT_DATE,TRIGGER_LEVEL,PAY_RATE,IS_PAID,OBSERVATION_LEVEL) values (132,1539,str_to_date('06/02/2018 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('10/02/2018 22:00.00','%d/%m/%Y %H:%i.%s'),0,8,0,0);
Insert into anycapital.PRODUCT_COUPONS (ID,PRODUCT_ID,OBSERVATION_DATE,PAYMENT_DATE,TRIGGER_LEVEL,PAY_RATE,IS_PAID,OBSERVATION_LEVEL) values (2,1248,str_to_date('24/11/2021 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('28/11/2021 22:00.00','%d/%m/%Y %H:%i.%s'),100,14,0,0);
Insert into anycapital.PRODUCT_COUPONS (ID,PRODUCT_ID,OBSERVATION_DATE,PAYMENT_DATE,TRIGGER_LEVEL,PAY_RATE,IS_PAID,OBSERVATION_LEVEL) values (4,1253,str_to_date('01/12/2019 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('03/12/2019 22:00.00','%d/%m/%Y %H:%i.%s'),0,5.4,0,0);
Insert into anycapital.PRODUCT_COUPONS (ID,PRODUCT_ID,OBSERVATION_DATE,PAYMENT_DATE,TRIGGER_LEVEL,PAY_RATE,IS_PAID,OBSERVATION_LEVEL) values (5,1254,str_to_date('25/05/2017 21:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('29/05/2017 21:00.00','%d/%m/%Y %H:%i.%s'),100,7,0,0);
Insert into anycapital.PRODUCT_COUPONS (ID,PRODUCT_ID,OBSERVATION_DATE,PAYMENT_DATE,TRIGGER_LEVEL,PAY_RATE,IS_PAID,OBSERVATION_LEVEL) values (6,1255,str_to_date('17/05/2018 21:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('21/05/2018 21:00.00','%d/%m/%Y %H:%i.%s'),0,10,0,0);
Insert into anycapital.PRODUCT_COUPONS (ID,PRODUCT_ID,OBSERVATION_DATE,PAYMENT_DATE,TRIGGER_LEVEL,PAY_RATE,IS_PAID,OBSERVATION_LEVEL) values (7,1259,str_to_date('29/11/2018 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('03/12/2018 22:00.00','%d/%m/%Y %H:%i.%s'),0,4,0,0);
Insert into anycapital.PRODUCT_COUPONS (ID,PRODUCT_ID,OBSERVATION_DATE,PAYMENT_DATE,TRIGGER_LEVEL,PAY_RATE,IS_PAID,OBSERVATION_LEVEL) values (64,1400,str_to_date('15/01/2016 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('17/01/2019 22:00.00','%d/%m/%Y %H:%i.%s'),0,11,1,4);
Insert into anycapital.PRODUCT_COUPONS (ID,PRODUCT_ID,OBSERVATION_DATE,PAYMENT_DATE,TRIGGER_LEVEL,PAY_RATE,IS_PAID,OBSERVATION_LEVEL) values (118,1503,str_to_date('20/07/2017 21:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('24/07/2017 21:00.00','%d/%m/%Y %H:%i.%s'),100,16,0,0);
Insert into anycapital.PRODUCT_COUPONS (ID,PRODUCT_ID,OBSERVATION_DATE,PAYMENT_DATE,TRIGGER_LEVEL,PAY_RATE,IS_PAID,OBSERVATION_LEVEL) values (130,1532,str_to_date('24/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('26/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),110,25,1,1);
Insert into anycapital.PRODUCT_COUPONS (ID,PRODUCT_ID,OBSERVATION_DATE,PAYMENT_DATE,TRIGGER_LEVEL,PAY_RATE,IS_PAID,OBSERVATION_LEVEL) values (131,1533,str_to_date('25/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('29/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),0,6,1,0);
Insert into anycapital.PRODUCT_COUPONS (ID,PRODUCT_ID,OBSERVATION_DATE,PAYMENT_DATE,TRIGGER_LEVEL,PAY_RATE,IS_PAID,OBSERVATION_LEVEL) values (65,1401,str_to_date('15/01/2018 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('17/01/2018 22:00.00','%d/%m/%Y %H:%i.%s'),0,5,0,0);
Insert into anycapital.PRODUCT_COUPONS (ID,PRODUCT_ID,OBSERVATION_DATE,PAYMENT_DATE,TRIGGER_LEVEL,PAY_RATE,IS_PAID,OBSERVATION_LEVEL) values (25,1286,str_to_date('09/12/2018 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('11/12/2018 22:00.00','%d/%m/%Y %H:%i.%s'),100,12,0,0);
Insert into anycapital.PRODUCT_COUPONS (ID,PRODUCT_ID,OBSERVATION_DATE,PAYMENT_DATE,TRIGGER_LEVEL,PAY_RATE,IS_PAID,OBSERVATION_LEVEL) values (26,1287,str_to_date('09/12/2018 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('11/12/2018 22:00.00','%d/%m/%Y %H:%i.%s'),100,10,0,0);
Insert into anycapital.PRODUCT_COUPONS (ID,PRODUCT_ID,OBSERVATION_DATE,PAYMENT_DATE,TRIGGER_LEVEL,PAY_RATE,IS_PAID,OBSERVATION_LEVEL) values (27,1288,str_to_date('18/09/2017 21:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('20/09/2017 21:00.00','%d/%m/%Y %H:%i.%s'),0,6,0,0);

-- insert PRODUCT_BARRIERS
Insert into anycapital.PRODUCT_BARRIERS (PRODUCT_ID,IS_BARRIER_OCCUR,BARRIER_LEVEL,PRODUCT_BARRIER_TYPE_ID,BARRIER_END,BARRIER_START) values (1273,0,125,2,str_to_date('11/12/2019 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('11/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.PRODUCT_BARRIERS (PRODUCT_ID,IS_BARRIER_OCCUR,BARRIER_LEVEL,PRODUCT_BARRIER_TYPE_ID,BARRIER_END,BARRIER_START) values (1282,0,55,1,str_to_date('09/12/2019 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('23/11/2016 22:00.00','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.PRODUCT_BARRIERS (PRODUCT_ID,IS_BARRIER_OCCUR,BARRIER_LEVEL,PRODUCT_BARRIER_TYPE_ID,BARRIER_END,BARRIER_START) values (1284,0,70,2,str_to_date('09/12/2019 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('11/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.PRODUCT_BARRIERS (PRODUCT_ID,IS_BARRIER_OCCUR,BARRIER_LEVEL,PRODUCT_BARRIER_TYPE_ID,BARRIER_END,BARRIER_START) values (1539,0,70,1,str_to_date('07/02/2018 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('09/02/2017 22:00.00','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.PRODUCT_BARRIERS (PRODUCT_ID,IS_BARRIER_OCCUR,BARRIER_LEVEL,PRODUCT_BARRIER_TYPE_ID,BARRIER_END,BARRIER_START) values (1271,0,70,1,str_to_date('14/12/2017 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('14/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.PRODUCT_BARRIERS (PRODUCT_ID,IS_BARRIER_OCCUR,BARRIER_LEVEL,PRODUCT_BARRIER_TYPE_ID,BARRIER_END,BARRIER_START) values (1253,1,60,1,str_to_date('01/12/2019 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('30/11/2016 22:00.00','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.PRODUCT_BARRIERS (PRODUCT_ID,IS_BARRIER_OCCUR,BARRIER_LEVEL,PRODUCT_BARRIER_TYPE_ID,BARRIER_END,BARRIER_START) values (1254,0,80,1,str_to_date('25/05/2017 21:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('27/11/2016 22:00.00','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.PRODUCT_BARRIERS (PRODUCT_ID,IS_BARRIER_OCCUR,BARRIER_LEVEL,PRODUCT_BARRIER_TYPE_ID,BARRIER_END,BARRIER_START) values (1257,1,60,1,str_to_date('27/11/2019 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('27/11/2016 22:00.00','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.PRODUCT_BARRIERS (PRODUCT_ID,IS_BARRIER_OCCUR,BARRIER_LEVEL,PRODUCT_BARRIER_TYPE_ID,BARRIER_END,BARRIER_START) values (1259,0,120,1,str_to_date('29/11/2018 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('29/11/2016 22:00.00','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.PRODUCT_BARRIERS (PRODUCT_ID,IS_BARRIER_OCCUR,BARRIER_LEVEL,PRODUCT_BARRIER_TYPE_ID,BARRIER_END,BARRIER_START) values (1400,1,60,1,str_to_date('15/01/2019 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('15/01/2017 22:00.00','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.PRODUCT_BARRIERS (PRODUCT_ID,IS_BARRIER_OCCUR,BARRIER_LEVEL,PRODUCT_BARRIER_TYPE_ID,BARRIER_END,BARRIER_START) values (1502,0,60,1,str_to_date('22/01/2020 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('22/01/2017 22:00.00','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.PRODUCT_BARRIERS (PRODUCT_ID,IS_BARRIER_OCCUR,BARRIER_LEVEL,PRODUCT_BARRIER_TYPE_ID,BARRIER_END,BARRIER_START) values (1249,0,130,1,str_to_date('26/11/2018 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('27/11/2016 22:00.00','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.PRODUCT_BARRIERS (PRODUCT_ID,IS_BARRIER_OCCUR,BARRIER_LEVEL,PRODUCT_BARRIER_TYPE_ID,BARRIER_END,BARRIER_START) values (1503,0,60,1,str_to_date('20/07/2017 21:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('22/01/2017 22:00.00','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.PRODUCT_BARRIERS (PRODUCT_ID,IS_BARRIER_OCCUR,BARRIER_LEVEL,PRODUCT_BARRIER_TYPE_ID,BARRIER_END,BARRIER_START) values (1528,0,75,1,str_to_date('24/01/2017 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('24/01/2016 22:00.00','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.PRODUCT_BARRIERS (PRODUCT_ID,IS_BARRIER_OCCUR,BARRIER_LEVEL,PRODUCT_BARRIER_TYPE_ID,BARRIER_END,BARRIER_START) values (1529,1,130,1,str_to_date('24/01/2017 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('24/01/2016 22:00.00','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.PRODUCT_BARRIERS (PRODUCT_ID,IS_BARRIER_OCCUR,BARRIER_LEVEL,PRODUCT_BARRIER_TYPE_ID,BARRIER_END,BARRIER_START) values (1533,1,60,1,str_to_date('24/01/2017 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('25/01/2015 22:00.00','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.PRODUCT_BARRIERS (PRODUCT_ID,IS_BARRIER_OCCUR,BARRIER_LEVEL,PRODUCT_BARRIER_TYPE_ID,BARRIER_END,BARRIER_START) values (1534,0,2,2,str_to_date('03/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('02/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.PRODUCT_BARRIERS (PRODUCT_ID,IS_BARRIER_OCCUR,BARRIER_LEVEL,PRODUCT_BARRIER_TYPE_ID,BARRIER_END,BARRIER_START) values (1566,0,11,2,str_to_date('10/03/2017 00:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('07/02/2017 00:00.00','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.PRODUCT_BARRIERS (PRODUCT_ID,IS_BARRIER_OCCUR,BARRIER_LEVEL,PRODUCT_BARRIER_TYPE_ID,BARRIER_END,BARRIER_START) values (1401,0,125,1,str_to_date('15/01/2018 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('12/01/2017 22:00.00','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.PRODUCT_BARRIERS (PRODUCT_ID,IS_BARRIER_OCCUR,BARRIER_LEVEL,PRODUCT_BARRIER_TYPE_ID,BARRIER_END,BARRIER_START) values (1289,0,70,1,str_to_date('18/12/2019 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('18/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.PRODUCT_BARRIERS (PRODUCT_ID,IS_BARRIER_OCCUR,BARRIER_LEVEL,PRODUCT_BARRIER_TYPE_ID,BARRIER_END,BARRIER_START) values (1290,0,80,2,str_to_date('18/12/2019 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('18/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.PRODUCT_BARRIERS (PRODUCT_ID,IS_BARRIER_OCCUR,BARRIER_LEVEL,PRODUCT_BARRIER_TYPE_ID,BARRIER_END,BARRIER_START) values (1292,0,130,1,str_to_date('18/06/2017 21:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('18/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.PRODUCT_BARRIERS (PRODUCT_ID,IS_BARRIER_OCCUR,BARRIER_LEVEL,PRODUCT_BARRIER_TYPE_ID,BARRIER_END,BARRIER_START) values (1286,0,70,1,str_to_date('09/12/2018 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('11/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.PRODUCT_BARRIERS (PRODUCT_ID,IS_BARRIER_OCCUR,BARRIER_LEVEL,PRODUCT_BARRIER_TYPE_ID,BARRIER_END,BARRIER_START) values (1287,0,70,2,str_to_date('09/12/2018 22:00.00','%d/%m/%Y %H:%i.%s'),str_to_date('11/12/2016 22:00.00','%d/%m/%Y %H:%i.%s'));


Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (1,str_to_date('18/10/2016 14:24.34','%d/%m/%Y %H:%i.%s'),2297.42,1,str_to_date('05/02/2017 10:04.02','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (2,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),8350.84,1,str_to_date('05/02/2017 10:04.01','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (3,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),7188.3,1,str_to_date('05/02/2017 10:04.02','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (4,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),18918.2,1,str_to_date('05/02/2017 10:04.01','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (5,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),11651.49,1,str_to_date('05/02/2017 10:03.01','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (6,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),3273.11,1,str_to_date('05/02/2017 10:04.01','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (7,str_to_date('05/09/2016 11:26.00','%d/%m/%Y %H:%i.%s'),null,1,null);
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (8,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),4825.42,1,str_to_date('05/02/2017 10:04.01','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (9,str_to_date('05/09/2016 11:25.58','%d/%m/%Y %H:%i.%s'),null,1,null);
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (10,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),5666.77,1,str_to_date('05/02/2017 10:04.01','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (11,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),55.53,1,str_to_date('05/02/2017 10:03.01','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (12,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),67.65,1,str_to_date('05/02/2017 10:03.01','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (13,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),195.96,1,str_to_date('31/01/2017 22:56.01','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (14,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),129.08,1,str_to_date('05/02/2017 10:04.00','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (15,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),100.39,1,str_to_date('05/02/2017 10:04.01','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (16,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),140.25,1,str_to_date('05/02/2017 10:04.01','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (17,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),810.2,1,str_to_date('05/02/2017 10:03.01','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (18,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),17.61,1,str_to_date('05/02/2017 10:04.01','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (19,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),130.98,1,str_to_date('05/02/2017 10:03.01','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (20,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),15.08,1,str_to_date('05/02/2017 10:04.01','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (21,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),16,1,str_to_date('05/02/2017 10:04.02','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (22,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),18.84,1,str_to_date('05/02/2017 10:04.01','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (23,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),46.75,1,str_to_date('05/02/2017 10:03.01','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (24,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),72.95,1,str_to_date('05/02/2017 10:04.00','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (25,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),235.8,1,str_to_date('05/02/2017 10:04.01','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (26,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),347.5,1,str_to_date('05/02/2017 10:04.01','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (27,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),94.4,1,str_to_date('05/02/2017 10:04.01','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (28,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),24.48,1,str_to_date('03/01/2017 10:08.01','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (29,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),13.81,1,str_to_date('05/02/2017 10:04.00','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (30,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),158.3,1,str_to_date('05/02/2017 10:04.00','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (31,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),84.03,1,str_to_date('05/02/2017 10:04.00','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (32,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),144.9,1,str_to_date('05/02/2017 10:04.01','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (33,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),63.68,1,str_to_date('05/02/2017 10:04.01','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (34,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),147,1,str_to_date('05/02/2017 10:04.01','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (35,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),41.54,1,str_to_date('05/02/2017 10:04.01','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (36,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),31.32,1,str_to_date('05/02/2017 10:04.00','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (37,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),39.59,1,str_to_date('05/02/2017 10:04.01','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (38,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),57.76,1,str_to_date('05/02/2017 10:04.00','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (39,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),23.29,1,str_to_date('05/02/2017 10:04.01','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (40,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),240.95,1,str_to_date('05/02/2017 10:04.02','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (41,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),32.09,1,str_to_date('05/02/2017 10:04.01','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (42,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),87.41,1,str_to_date('05/02/2017 10:03.01','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (43,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),64.94,1,str_to_date('05/02/2017 10:04.01','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (44,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),1221.6,1,str_to_date('05/02/2017 10:04.01','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (45,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),54.49,1,str_to_date('05/02/2017 10:04.01','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (46,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),15.975,1,str_to_date('03/01/2017 10:10.02','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (47,str_to_date('05/09/2016 11:26.00','%d/%m/%Y %H:%i.%s'),null,1,null);
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (48,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),1.078,1,str_to_date('05/02/2017 10:04.01','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (49,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),1.2297,1,str_to_date('03/01/2017 10:11.00','%d/%m/%Y %H:%i.%s'));
Insert into anycapital.MARKET_PRICES (MARKET_ID,TIME_MODIFIED,PRICE,TYPE_ID,PRICE_DATE) values (50,str_to_date('13/10/2016 19:00.00','%d/%m/%Y %H:%i.%s'),112.584,1,str_to_date('05/02/2017 10:04.02','%d/%m/%Y %H:%i.%s'));

-- insert MSG_RES

Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (165,'distance-to-barrier',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (166,'product.categroy.1',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (45,'test6',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (167,'product.categroy.2',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (168,'product.categroy.3',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (237,'PRODUCT.COUPON.FREQUENCIES.1',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (238,'PRODUCT.COUPON.FREQUENCIES.2',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (239,'PRODUCT.COUPON.FREQUENCIES.3',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (240,'PRODUCT.COUPON.FREQUENCIES.4',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (241,'PRODUCT.COUPON.FREQUENCIES.5',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (242,'PRODUCT.COUPON.FREQUENCIES.6',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (243,'DAY.COUNT.CONVENTIONS.1',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (244,'DAY.COUNT.CONVENTIONS.2',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (245,'DAY.COUNT.CONVENTIONS.3',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (246,'market.1',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (247,'market.2',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (248,'market.3',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (249,'market.4',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (250,'market.5',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (251,'market.6',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (252,'market.7',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (253,'market.8',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (254,'market.9',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (255,'market.10',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (256,'market.11',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (257,'market.12',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (258,'market.13',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (259,'market.14',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (260,'market.15',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (261,'market.16',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (262,'market.17',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (263,'market.18',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (264,'market.19',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (265,'market.20',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (266,'market.21',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (267,'market.22',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (268,'market.23',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (269,'market.24',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (270,'market.25',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (271,'market.26',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (272,'market.27',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (273,'market.28',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (274,'market.29',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (275,'market.30',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (276,'market.31',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (277,'market.32',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (278,'market.33',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (279,'market.34',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (280,'market.35',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (281,'market.36',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (282,'market.37',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (283,'market.38',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (284,'market.39',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (285,'market.40',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (286,'market.41',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (287,'market.42',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (288,'market.43',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (289,'market.44',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (290,'market.45',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (291,'market.46',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (292,'market.47',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (293,'market.48',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (294,'market.49',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (295,'market.50',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (296,'PRODUCT.ISSUER.INSURANCE.TYPE.1',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (297,'PRODUCT.ISSUER.INSURANCE.TYPE.2',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (307,'contact-us',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (652,'month',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (653,'months',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (654,'year',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (725,'risk-appetite',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (726,'capital-guaranty',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (727,'protection',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (728,'participation',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (729,'capital-protection',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (730,'bondfloor-at-issuance',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (731,'upside',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (732,'barrier',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (733,'barrier-level',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (734,'barrier-type',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (735,'barrier-observation-period',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (736,'barrier-event',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (737,'distance-to-barrier',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (738,'initial-fixing-level',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (739,'observation-frequency',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (740,'coupon-trigger-level',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (741,'bonus-level',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (742,'exchange-rate',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (743,'bbg-ticker',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (746,'currency',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (747,'product-investors-note',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (749,'product-documentation-text',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (750,'product-type-1-product-description',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (751,'product-type-2-product-description',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (752,'product-type-3-product-description',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (753,'product-type-4-product-description',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (754,'product-type-5-product-description',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (755,'product-type-6-product-description',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (756,'product-type-7-product-description',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (757,'product-type-8-product-description',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (758,'product-type-9-product-description',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (759,'product-type-10-product-description',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (760,'product-type-1-one-liner',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (761,'product-type-2-one-liner',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (762,'product-type-3-one-liner',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (763,'product-type-4-one-liner',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (764,'product-type-5-one-liner',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (765,'product-type-6-one-liner',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (766,'product-type-7-one-liner',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (767,'product-type-8-one-liner',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (768,'product-type-9-one-liner',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (769,'product-type-10-one-liner',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (900,'redemption-1-scenario-1',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (901,'redemption-1-scenario-2',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (902,'redemption-2-scenario-1',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (903,'redemption-2-scenario-2',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (904,'redemption-2-scenario-3',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (905,'redemption-4-scenario-1',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (906,'redemption-4-scenario-2',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (907,'redemption-5-scenario-1',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (908,'redemption-5-scenario-2',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (909,'redemption-5-scenario-3',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (910,'redemption-6-scenario-1',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (911,'redemption-6-scenario-2',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (912,'redemption-6-scenario-3',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (913,'redemption-7-scenario-1',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (914,'redemption-7-scenario-2',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (915,'redemption-7-scenario-3',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (916,'redemption-8-scenario-1',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (917,'redemption-8-scenario-2',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (918,'redemption-9-scenario-1',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (919,'redemption-9-scenario-2',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (920,'redemption-9-scenario-3',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (921,'redemption-10-scenario-1',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (922,'redemption-10-scenario-2',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (923,'redemption-10-scenario-3',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (924,'redemption-10-scenario-4',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (925,'in-percent',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (940,'original-lifetime',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (950,'my-notes',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (951,'funding',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (952,'open',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (953,'closed',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (954,'deposit',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (955,'withdraw',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (956,'cancel-withdraw',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (957,'banking-history',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (958,'personal-details',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (959,'password',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (960,'username',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (961,'first-name',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (962,'last-name',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (963,'day-of-birth',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (964,'gender',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (965,'email',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (966,'street-address',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (967,'house-flat-number',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (968,'city',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (969,'postal-code-zip',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (970,'country',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (971,'mobile',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (972,'phone',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (973,'personal-details-no-newsletters',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (974,'personal-details-no-text-messages',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (975,'submit',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (976,'search',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (977,'my-account-deposit-page',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1093,'your-note-was-purchased',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1094,'investment-purchase-message-purchased',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1095,'your-note-was-secured',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1096,'investment-purchase-message-secured',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1097,'fund-your-account',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1098,'investment-purchase-message-insufficient-above-minimum',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1099,'ok',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1100,'investment-purchase-message-insufficient-increase-balance',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1103,'glossary-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1471,'faq-my-account-forgotten-password-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1472,'faq-my-account-transaction-history-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1473,'faq-my-account-update-personal-details-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1474,'faq-technical-download-software-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1475,'faq-technical-change-language-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1476,'faq-product-types-capital-guarantee-products-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1477,'faq-product-types-yield-enhancement-products-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1478,'faq-product-types-participation-products-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1479,'faq-product-types-capital-guarantee-products-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1480,'faq-product-types-yield-enhancement-products-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1482,'faq-product-types-participation-products-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1484,'article-1-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1487,'article-1-short-description',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1488,'article-2-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1490,'article-2-short-description',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1491,'article-3-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1492,'article-3-short-description',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1493,'article-4-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1494,'article-4-short-description',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1495,'article-5-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1496,'article-5-subTitle',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1497,'article-5-short-description',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1498,'article-6-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1499,'article-6-short-description',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1500,'article-7-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1501,'article-7-short-description',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1502,'article-8-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1503,'article-8-short-description',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1505,'article-9-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1506,'article-9-short-description',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1507,'article-10-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1508,'article-10-subTitle',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1509,'article-10-short-description',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1510,'article-11-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1511,'article-11-subTitle',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1512,'article-11-short-description',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1513,'article-12-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1514,'article-12-subTitle',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (691,'general-info-depositor-value',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (565,'currencies.eur',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (692,'general-info-lead-manager-value',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (693,'general-info-calculation-agent-value',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (694,'general-info-paying-agent-value',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (695,'general-info-secondary-market-value',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (696,'general-info-quoting-type-value',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (697,'general-info-quotation-type-value',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (698,'general-info-settelment-type-value',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (699,'product-category-1-product-prospect',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (700,'product-category-2-product-prospect',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (701,'product-category-3-product-prospect',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (702,'product-type-1-market-expectation',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (703,'product-type-2-market-expectation',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (704,'product-type-3-market-expectation',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (705,'product-type-4-market-expectation',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (706,'product-type-5-market-expectation',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (707,'product-type-6-market-expectation',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (708,'product-type-7-market-expectation',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (709,'product-type-8-market-expectation',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (710,'product-type-9-market-expectation',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (711,'product-type-10-market-expectation',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1001,'current-password',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1002,'new-password',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1003,'retype-new-password',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1004,'view-all-notes',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1108,'investment-purchase-message-below-minimum',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1109,'investment-purchase-message-above-maximum',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1110,'investment-purchase-message-below-minimum-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1111,'investment-purchase-message-already-secured',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1112,'investment-purchase-message-suspended',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1113,'investment-purchase-message-unavailable',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1114,'investment-purchase-message-already-issued',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1115,'investment-purchase-message-deviation',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1116,'view-available-notes',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1117,'show-me-updated-terms',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1118,'refresh',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1119,'redemption-2-scenario-1-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1120,'redemption-2-scenario-2-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1121,'redemption-2-scenario-3-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1122,'redemption-5-scenario-1-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1123,'redemption-5-scenario-2-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1124,'redemption-5-scenario-3-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1125,'redemption-6-scenario-1-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1126,'redemption-6-scenario-2-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1127,'redemption-6-scenario-3-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1128,'redemption-7-scenario-1-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1129,'redemption-7-scenario-2-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1130,'redemption-7-scenario-3-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1131,'redemption-9-scenario-1-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1132,'redemption-9-scenario-2-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1133,'redemption-9-scenario-3-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1134,'redemption-10-scenario-1-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1135,'redemption-10-scenario-2-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1136,'redemption-10-scenario-3-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1137,'redemption-10-scenario-4-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1139,'barrier-event-description-2-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1140,'barrier-event-description-5-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1141,'barrier-event-description-6-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1142,'barrier-event-description-7-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1143,'barrier-event-description-9-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1144,'barrier-event-description-10-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1145,'product-type-2-one-liner-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1146,'product-type-5-one-liner-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1147,'product-type-6-one-liner-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1148,'product-type-7-one-liner-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1149,'product-type-9-one-liner-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1150,'product-type-10-one-liner-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1151,'product-type-2-product-description-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1152,'product-type-5-product-description-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1153,'product-type-6-product-description-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1154,'product-type-7-product-description-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1155,'product-type-9-product-description-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1156,'product-type-10-product-description-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1157,'error.access_denied',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1158,'transaction.status.1',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1159,'transaction.status.2',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1160,'transaction.status.3',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1161,'Real',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1162,'Not Real',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1163,'transaction.class.1',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1164,'transaction.class.2',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1165,'transaction.payment.type.1',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1166,'transaction.status.1',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1167,'transaction.status.2',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1168,'transaction.status.3',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1169,'transaction.class.1',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1170,'transaction.class.2',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1171,'transaction.operation.1',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1172,'transaction.operation.2',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1173,'transaction.payment.type.1',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1249,'product.statuses.1',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1250,'product.statuses.2',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1251,'product.statuses.3',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1252,'product.statuses.4',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1253,'product.statuses.5',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1254,'product.statuses.6',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1332,'product-type-5-scenario-1-short-barrier-event',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1333,'product-type-6-scenario-1-short-barrier-event',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1334,'you-will-receive',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1335,'product-you-have-a-note',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1336,'currencies.usd',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1337,'unavailable',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1338,'confirmed',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1339,'sell-confirmation',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1340,'sell',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1357,'my-notes-open-message-fund-your-account',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1358,'redemption-amount',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1359,'coupon-amount',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1360,'in-subscription',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1515,'article-12-short-description',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1516,'article-13-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1517,'article-13-short-description',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1520,'open-glossary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1521,'home',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1522,'close',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1523,'available',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1524,'no-protection-on-this-product',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1525,'return',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1526,'last-level',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1527,'in',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1528,'simulation-for-investment',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1529,'effective',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1530,'interactive-calculator',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1531,'the-attraction',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1532,'worst-case',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1533,'product-type-1-header-short-attraction-primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1534,'product-type-2-header-short-attraction-primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1535,'product-type-2-header-short-attraction-primary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1536,'product-type-3-header-short-attraction-primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1537,'product-type-4-header-short-attraction-primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1538,'product-type-5-header-short-attraction-primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1539,'product-type-5-header-short-attraction-primary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1540,'product-type-6-header-short-attraction-primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1541,'product-type-6-header-short-attraction-primary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1542,'product-type-7-header-short-attraction-primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1543,'product-type-7-header-short-attraction-primary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1544,'product-type-8-header-short-attraction-primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1545,'product-type-8-capped-header-short-attraction-primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1546,'product-type-9-header-short-attraction-primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1547,'product-type-9-header-short-attraction-primary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1548,'product-type-10-header-short-attraction-primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1549,'product-type-10-header-short-attraction-primary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1550,'product-type-1-header-short-attraction-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1551,'product-type-2-header-short-attraction-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1552,'product-type-2-header-short-attraction-secondary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1553,'product-type-3-header-short-attraction-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1554,'product-type-4-header-short-attraction-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1555,'product-type-5-header-short-attraction-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1556,'product-type-5-header-short-attraction-secondary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1557,'product-type-6-header-short-attraction-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1558,'product-type-6-header-short-attraction-secondary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1559,'product-type-7-header-short-attraction-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1560,'product-type-7-header-short-attraction-secondary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (561,'currency.eur',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (324,'faq',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (571,'market.1',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (572,'market.2',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (325,'maturity',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (326,'coupon',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (327,'strike-level',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (328,'min',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (329,'pre-order',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (330,'secure-this-note',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (331,'on',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (332,'final-fixing-date',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (333,'coupon-level',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (334,'denomination-currency',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (335,'outcome-scenarios',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (336,'scenario',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (337,'is-at-or-above',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (338,'receive',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (339,'market-expectation',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (340,'underlying',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (341,'product-description',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (342,'product-details',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (343,'issue-price',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (344,'issue-size',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (345,'issue-size-can-increase',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (346,'denomination',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (347,'settlement-currency',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (348,'quanto',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (349,'yes',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (350,'no',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (351,'dates',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (352,'subscription-end-date',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (353,'initial-fixing-date',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (354,'first-exchange-trading-date',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (355,'last-trading-date',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (573,'market.3',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (357,'redemption',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (358,'simulation',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (359,'final-fixing-level',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (360,'cash-settlement',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (361,'general-information',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (362,'depositor',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (363,'lead-manager',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (364,'calculation-agent',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (365,'paying-agent',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (366,'secondary-market',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (367,'quoting-type',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (368,'quotation-type',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (369,'settlement-type',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (370,'minimum-investment',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (371,'product-documentation',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (372,'prospect-for-profit-and-loss',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (373,'significant-risks',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (374,'risk-fact-relating-product',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (375,'additional-risk-factors',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (574,'market.4',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (575,'market.5',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (576,'market.6',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (577,'market.7',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (578,'market.8',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (579,'market.9',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (580,'market.10',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (581,'market.11',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (582,'market.12',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (583,'market.13',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (584,'market.14',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (585,'market.15',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (586,'market.16',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (587,'market.17',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (588,'market.18',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (589,'market.19',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (590,'market.20',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (591,'market.21',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (592,'market.22',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (593,'market.23',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (594,'market.24',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (595,'market.25',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (596,'market.26',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (597,'market.27',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (598,'market.28',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (599,'market.29',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (600,'market.30',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (601,'market.31',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (602,'market.32',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (603,'market.33',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (604,'market.34',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (605,'market.35',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (606,'market.36',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (607,'market.37',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (608,'market.38',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (609,'market.39',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (610,'market.40',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (611,'market.41',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (612,'market.42',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (613,'market.43',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (614,'market.44',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (615,'market.45',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (616,'market.46',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (617,'market.47',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (618,'market.48',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (619,'market.49',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (620,'market.50',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (681,'product.type.1',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (682,'product.type.2',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (683,'product.type.3',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (684,'product.type.4',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (685,'product.type.5',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (686,'product.type.6',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (429,'currency.eur',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (430,'about',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (431,'my-account',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (432,'articles',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (433,'products',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (687,'product.type.7',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (688,'product.type.8',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (689,'product.type.9',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (690,'product.type.10',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (712,'day.count.conventions.4',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (713,'product.maturities.1',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (714,'product.maturities.2',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (715,'product.maturities.3',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (716,'product.maturities.4',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (717,'product.maturities.5',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (718,'product.maturities.6',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (719,'product.maturities.7',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (720,'product.maturities.8',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (721,'product.barrier.types.1',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (722,'product.barrier.types.2',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (817,'maturity',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (818,'barrier-type',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (879,'up-to-cap-level',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (880,'redemption-1-header',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (881,'redemption-2-header',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (882,'redemption-3-header',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (883,'redemption-4-header',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (884,'redemption-5-header',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (885,'redemption-6-header',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (886,'redemption-7-header',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (887,'redemption-8-header',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (888,'redemption-9-header',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (889,'redemption-10-header',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (890,'coupon-trigger-event',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (891,'cap-level',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (892,'initial-fixing-level-description',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (893,'final-fixing-level-description',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (894,'barrier-event-description',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (895,'Underlyings close above their respective Coupon Trigger Level, as reasonably determined by the Calculation Agent.',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (896,'barrier-observation-period-description',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (897,'coupon-trigger-event-description',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (899,'cap-level-description',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (979,'my-profile',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (986,'from',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (987,'to',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (988,'transaction-id',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (989,'time-date',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (990,'description',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (991,'credit',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (992,'debit',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (993,'previous',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (994,'next',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (995,'example',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (996,'positive',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (997,'negative',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1005,'how-to-invest-capital-notes',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1006,'home-page-step-1',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1007,'home-page-step-2',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1008,'home-page-step-3',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1009,'home-page-step-4',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1010,'home-page-step-5',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1011,'about-us',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1012,'more',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1013,'footer-text',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1014,'home-page-slide-hand',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1015,'home-page-slide-boat-lake',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1016,'home-page-slide-boat',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1017,'home-page-slide-kids-lake',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1065,'category',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1066,'type',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1067,'market',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1068,'status',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1069,'subscription-date',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1176,'beneficiary-name',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1178,'swift',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1179,'iban',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1180,'account-number',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1181,'account-info',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1182,'bank-name',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1183,'branch-address',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1184,'amount',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1185,'bank-fee-amount',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1186,'comments',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1187,'payment-type',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1188,'created-at',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1189,'settled-at',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1190,'credit/debit',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1191,'ip',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1192,'email',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1193,'current-password',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1194,'new-password',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1195,'verify-password',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1196,'submit',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1197,'currencies.usd',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1226,'open-account',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1227,'contact-us-top',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1228,'support-email',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1229,'phone-number',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1230,'contact-this-is-a-toll-line',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1231,'fax-number',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1232,'working-hours',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1233,'contact-working-hours',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1234,'address',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1235,'contact-address',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1236,'issue',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1237,'comments',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1238,'send',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1239,'balance',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1240,'use-same-password-as-anyoption',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1241,'load-more',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1242,'you-pay',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1243,'product',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1244,'purchased-at',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1245,'sell-now-for',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1246,'buy-more-at',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1247,'prod-id',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1248,'inv-id',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1297,'all',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1298,'bid-ask',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1299,'observation-level',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1300,'web-page',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1301,'view',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1302,'from',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1303,'to',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1304,'sources',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1305,'landing-pages',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1306,'contents',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1307,'name',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1308,'add-source',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1309,'path',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1310,'add-landing-page',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1311,'add-content',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1312,'deposits',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1313,'deposit',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1314,'edit-user',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1317,'first-name',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1318,'last-name',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1319,'day-of-birth',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1320,'gender',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1321,'wire-withdraw',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1322,'change-password',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1323,'load-user',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1324,'balance',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1325,'mobile-phone',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1326,'utc-offset',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1327,'branch-number',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1329,'add-user',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1350,'campaigns',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1351,'writer-user-name',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1352,'add-campaign',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1353,'source',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (47,'test7',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (43,'test5',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (61,'test.test',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (62,'login',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (63,'logout',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (64,'login-username',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (65,'login-password',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (66,'home',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (67,'administrator',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (68,'products',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (69,'translations',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (70,'search',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (71,'save',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (72,'remove',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (73,'add',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (74,'key',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (75,'edit-product',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (76,'id',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (77,'product-type',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (78,'issue-price',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (79,'currencry-id',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (80,'denomination',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (81,'coupon-frequency',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (82,'day-count-convention-id',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (83,'max-yield',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (84,'max-yield-pa',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (85,'level-of-protection',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (86,'level-of-participation',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (87,'barrier-start',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (88,'barrier-end',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (89,'strike-level',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (90,'bonus-precentage',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (91,'subscription-start-date',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (92,'subscription-end-date',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (93,'initial-fixing-date',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (94,'issue-date',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (95,'first-exchange-trading-date',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (96,'last-trading-date',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (97,'final-fixing-date',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (98,'redemption-date',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (99,'quanto',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (100,'product-insurance-type',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (101,'bond-floor-atissuance',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (102,'issue-size',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (103,'priority',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (104,'markets',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (105,'product-scenarios',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (106,'final-fixing-level',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (107,'text',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (108,'product-coupons',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (109,'observation-date',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (110,'payment-date',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (111,'trigger-level',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (112,'pay-rate',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (113,'product-autocalls',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (114,'examination-date',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (115,'condition',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (116,'product-callables',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (117,'product-simulation',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (118,'cash-settlement',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (629,'login-username',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (627,'login',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (628,'login-header',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (630,'login-password',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (631,'remember-me',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (632,'forgot-password',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (633,'support',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (634,'hello',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (635,'logout',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (636,'other',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (637,'PRODUCT.COUPON.FREQUENCIES.1',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (638,'PRODUCT.COUPON.FREQUENCIES.2',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (639,'PRODUCT.COUPON.FREQUENCIES.3',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (640,'PRODUCT.COUPON.FREQUENCIES.4',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (641,'PRODUCT.COUPON.FREQUENCIES.5',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (642,'PRODUCT.COUPON.FREQUENCIES.6',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (643,'currencies.eur',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (644,'reset',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (645,'back',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (646,'forgot-password-header',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (647,'forgot-password-username',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (841,'cap-level',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (842,'TBA',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (843,'continuous-barrier-monitoring',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (844,'barrier-monitoring-at-final-fixing-date',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (845,'exchange-market-close',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (846,'10-NY-time-close',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (847,'purchase-this-note',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (848,'unlimited',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (849,'cap',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (928,'end-trade-level',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (929,'suspend',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (930,'barrier-occur',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1081,'assets',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1082,'type',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1083,'appetite',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1084,'all',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1085,'primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1086,'secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1087,'months-and-up',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1088,'cancel-withdraw-description',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1089,'amount',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1090,'withdraw-amount',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1091,'select',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1092,'glossary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1354,'landing-page',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1355,'content',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1356,'domain',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1361,'sold-at',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1366,'not-now',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1368,'status',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1369,'return-amount',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1370,'investment-status-1',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1371,'investment-status-2',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1372,'investment-status-3',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1373,'investment-status-4',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1374,'investment-status-5',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1375,'product-type-2-scenario-1-short-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1376,'product-type-2-scenario-2-short-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1377,'product-type-2-scenario-3-short-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1378,'product-type-5-scenario-1-short-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1379,'product-type-5-scenario-2-short-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1380,'product-type-5-scenario-3-short-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1381,'product-type-6-scenario-1-short-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1382,'product-type-6-scenario-2-short-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1383,'product-type-6-scenario-3-short-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1384,'product-type-7-scenario-1-short-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1385,'product-type-7-scenario-2-short-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1386,'product-type-7-scenario-3-short-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1387,'product-type-9-scenario-1-short-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1388,'product-type-9-scenario-2-short-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1389,'product-type-9-scenario-3-short-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1390,'product-type-10-scenario-1-short-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1391,'product-type-10-scenario-2-short-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1392,'product-type-10-scenario-3-short-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1393,'product-type-5-scenario-1-short-european-barrier-event',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1394,'barrier-event-description-2-american',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1395,'barrier-event-description-5-american',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1396,'barrier-event-description-6-american',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1397,'barrier-event-description-7-american',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1398,'barrier-event-description-9-american',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1399,'barrier-event-description-10-american',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1561,'product-type-8-header-short-attraction-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1562,'product-type-8-capped-header-short-attraction-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1563,'product-type-9-header-short-attraction-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1564,'product-type-9-header-short-attraction-secondary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1565,'product-type-10-header-short-attraction-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1566,'product-type-10-header-short-attraction-secondary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1567,'product-type-1-header-short-worstcase-primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1568,'product-type-2-header-short-worstcase-primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1569,'product-type-2-header-short-worstcase-primary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1570,'product-type-3-header-short-worstcase-primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1571,'product-type-4-header-short-worstcase-primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1572,'product-type-5-header-short-worstcase-primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1573,'product-type-5-header-short-worstcase-primary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1574,'product-type-6-header-short-worstcase-primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1575,'product-type-6-header-short-worstcase-primary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1576,'product-type-7-header-short-worstcase-primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1577,'product-type-7-header-short-worstcase-primary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1578,'product-type-8-header-short-worstcase-primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1579,'product-type-8-capped-header-short-worstcase-primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1580,'product-type-9-header-short-worstcase-primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1581,'product-type-9-header-short-worstcase-primary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1582,'product-type-10-header-short-worstcase-primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1583,'product-type-10-header-short-worstcase-primary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1584,'product-type-1-header-short-worstcase-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1585,'product-type-2-header-short-worstcase-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1586,'product-type-2-header-short-worstcase-secondary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1587,'product-type-3-header-short-worstcase-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1588,'product-type-4-header-short-worstcase-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1589,'product-type-5-header-short-worstcase-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1590,'product-type-5-header-short-worstcase-secondary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1591,'product-type-6-header-short-worstcase-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1592,'product-type-6-header-short-worstcase-secondary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1593,'product-type-7-header-short-worstcase-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1594,'product-type-7-header-short-worstcase-secondary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1595,'product-type-8-header-short-worstcase-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1596,'product-type-8-capped-header-short-worstcase-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1597,'product-type-9-header-short-worstcase-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1598,'product-type-9-header-short-worstcase-secondary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1599,'product-type-10-header-short-worstcase-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1600,'product-type-10-header-short-worstcase-secondary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (122,'users',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (123,'transactions',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (124,'investments',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (125,'totals',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (569,'product-type',1,2,1);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (648,'test-large',1,2,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (119,'add-product',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (120,'product-category',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (121,'currency',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (772,'product-significant-risks-text',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (155,'product.type.1',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (156,'product.type.2',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (157,'product.type.3',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (158,'product.type.4',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (159,'product.type.5',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (160,'product.type.6',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (161,'product.type.7',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (162,'product.type.8',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (163,'product.type.9',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (164,'product.type.10',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (300,'test1.test1',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (773,'product.barrier.types.1',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (774,'product.barrier.types.2',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (775,'product-type-1-scenario-1-short',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (776,'product-type-1-scenario-2-short',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (777,'product-type-1-scenario-3-short',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (778,'product-type-2-scenario-1-short',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (779,'product-type-2-scenario-2-short',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (780,'product-type-2-scenario-3-short',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (781,'product-type-3-scenario-1-short',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (782,'product-type-3-scenario-2-short',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (783,'product-type-3-scenario-3-short',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (784,'product-type-4-scenario-1-short',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (785,'product-type-4-scenario-2-short',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (786,'product-type-4-scenario-3-short',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (787,'product-type-5-scenario-1-short',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (788,'product-type-5-scenario-2-short',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (789,'product-type-5-scenario-3-short',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (790,'product-type-6-scenario-1-short',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (791,'product-type-6-scenario-2-short',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (792,'product-type-6-scenario-3-short',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (793,'product-type-7-scenario-1-short',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (794,'product-type-7-scenario-2-short',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (795,'product-type-7-scenario-3-short',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (796,'product-type-8-scenario-1-short',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (797,'product-type-8-scenario-2-short',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (798,'product-type-8-scenario-3-short',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (799,'product-type-9-scenario-1-short',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (800,'product-type-9-scenario-2-short',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (801,'product-type-9-scenario-3-short',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (802,'product-type-10-scenario-1-short',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (803,'product-type-10-scenario-2-short',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (804,'product-type-10-scenario-3-short',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (805,'barrier-level',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (806,'bonus-level',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (850,'user-id',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (851,'beneficiary-name',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (853,'swift-bic-code',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (854,'iban',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (855,'account-number',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (856,'bank-name',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (858,'branch-number',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (859,'branch-address',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (860,'deposit-amount',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (861,'product-id',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (862,'market-name',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (863,'time-created',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (864,'ask-bid',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (865,'start-trade-level',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (866,'update',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (931,'product-investors-note-asterisk',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (932,'all-notes',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (933,'last',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (934,'outcome-scenarios-for',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (935,'continue',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (936,'none',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (937,'products-list-disclaimer',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (938,'discount',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (939,'premium',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (941,'publish',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1028,'forgot-password-reset-failed-header',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1029,'set-up-account',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1030,'signup-header',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1031,'accept-terms',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1032,'signup',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1033,'forgot-password-reset-submit-header',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1034,'send-me-via',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1035,'sms',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1036,'footer-contact',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1037,'footer-privacy-policy',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1038,'footer-terms',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1291,'approve',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1292,'cancel',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1293,'first-approval',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1294,'second-approval',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1295,'ask',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1296,'bid',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1341,'writer-id',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1342,'time-modified',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1343,'pagination-result-count',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1344,'next',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1345,'previous',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1346,'previous-group',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1347,'next-group',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1348,'url',2,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1400,'faq-categories-trading',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1401,'faq-categories-product-types',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1402,'faq-categories-deposit-and-withdraw',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1403,'faq-categories-my-account',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1404,'faq-categories-technical',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1405,'faq-trading-structured-product-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1406,'faq-trading-underlying-asset-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1407,'faq-trading-term-sheet-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1408,'faq-trading-barrier-event-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1409,'faq-trading-capital-protected-product-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1410,'faq-trading-structured-product-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1411,'faq-trading-underlying-asset-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1412,'faq-trading-term-sheet-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1413,'faq-trading-barrier-event-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1415,'faq-trading-capital-protected-product-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1416,'frequently-asked-questions',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1420,'faq-trading-yield-enhancement-product-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1421,'faq-trading-participation-product-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1422,'faq-trading-credit-risk-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1423,'faq-trading-calculate-redemption-amount-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1424,'faq-trading-get-out-before-end-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1425,'faq-trading-events-affecting-product-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1426,'faq-trading-buy-sp-different-currency-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1427,'faq-trading-invest-in-sp-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1428,'faq-trading-issuer-knows-his-sp-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1429,'faq-trading-case-of-credit-event-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1430,'faq-trading-insured-against-credit-event-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1431,'faq-trading-secondary-market-price-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1432,'faq-trading-initial-fixing-level-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1433,'faq-trading-final-fixing-level-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1434,'faq-trading-issue-price-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1435,'faq-trading-Quanto-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1436,'faq-deposit-and-withdraw-deposit-options-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1437,'faq-deposit-and-withdraw-currency-in-my-account-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1438,'faq-deposit-and-withdraw-minimal-withdrawal-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1439,'faq-deposit-and-withdraw-withdraw-profit-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1440,'faq-deposit-and-withdraw-must-deposit-register-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1441,'faq-deposit-and-withdraw-cancel-withdrawal-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1442,'faq-deposit-and-withdraw-request-wire-withdrawal-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1443,'faq-my-account-forgotten-password-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1444,'faq-my-account-transaction-history-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1445,'faq-my-account-update-personal-details-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1446,'faq-technical-download-software-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1447,'faq-technical-change-language-title',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1448,'faq-trading-yield-enhancement-product-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1449,'faq-trading-participation-product-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1450,'faq-trading-credit-risk-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1451,'faq-trading-calculate-redemption-amount-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1452,'faq-trading-get-out-before-end-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1453,'faq-trading-events-affecting-product-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1454,'faq-trading-buy-sp-different-currency-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1455,'faq-trading-invest-in-sp-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1456,'faq-trading-issuer-knows-his-sp-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1457,'faq-trading-case-of-credit-event-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1458,'faq-trading-secondary-market-price-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1459,'faq-trading-insured-against-credit-event-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1460,'faq-trading-initial-fixing-level-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1461,'faq-trading-final-fixing-level-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1462,'faq-trading-issue-price-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1463,'faq-trading-Quanto-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1464,'faq-deposit-and-withdraw-deposit-options-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1465,'faq-deposit-and-withdraw-currency-in-my-account-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1466,'faq-deposit-and-withdraw-minimal-withdrawal-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1467,'faq-deposit-and-withdraw-withdraw-profit-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1468,'faq-deposit-and-withdraw-must-deposit-register-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1469,'faq-deposit-and-withdraw-cancel-withdrawal-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1470,'faq-deposit-and-withdraw-request-wire-withdrawal-content',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1601,'product-type-1-header-long-primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1602,'product-type-2-header-long-primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1603,'product-type-2-header-long-primary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1604,'product-type-3-header-long-primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1605,'product-type-4-header-long-primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1606,'product-type-5-header-long-primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1607,'product-type-5-header-long-primary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1608,'product-type-6-header-long-primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1609,'product-type-6-header-long-primary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1610,'product-type-7-header-long-primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1611,'product-type-7-header-long-primary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1612,'product-type-8-header-long-primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1613,'product-type-8-capped-header-long-primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1614,'product-type-9-header-long-primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1615,'product-type-9-header-long-primary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1616,'product-type-10-header-long-primary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1617,'product-type-10-header-long-primary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1618,'product-type-1-header-long-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1619,'product-type-2-header-long-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1620,'product-type-2-header-long-secondary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1621,'product-type-3-header-long-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1622,'product-type-4-header-long-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1623,'product-type-5-header-long-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1624,'product-type-5-header-long-secondary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1625,'product-type-6-header-long-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1626,'product-type-6-header-long-secondary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1627,'product-type-7-header-long-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1628,'product-type-7-header-long-secondary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1629,'product-type-8-header-long-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1630,'product-type-8-capped-header-long-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1631,'product-type-9-header-long-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1632,'product-type-9-header-long-secondary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1633,'product-type-10-header-long-secondary',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1634,'product-type-10-header-long-secondary-european',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1635,'denomination-description',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1636,'product_type_protection_level.1',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1637,'product_type_protection_level.2',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1638,'product_type_protection_level.3',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1639,'barrier-monitoring',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1640,'continuously',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1641,'at-final-fixing-date',1,1,null);
Insert into anycapital.MSG_RES (ID,`KEY`,ACTION_SOURCE_ID,TYPE_ID,ENTITY_ID) values (1642,'current-price',1,1,null);

-- INSERTING into anycapital.MSG_RES_LANGUAGE
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (165,2,'Distance to barrier',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (166,2,'Capital Protection',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (45,2,'aaaa',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (167,2,'Yield Enhancement',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (168,2,'Participation',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (237,2,'no coupon',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (238,2,'Monthly',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (239,2,'Quaterly',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (240,2,'Semi-Annual',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (241,2,'Annual',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (242,2,'In Fine',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (243,2,'Actual/Actual ISDA',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (244,2,'Actual/365 Fixed',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (245,2,'Actual/360',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (246,2,'S&P 500 Index',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (247,2,'SMI Index',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (248,2,'FTSE 100 Index',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (249,2,'Nikkei 225',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (250,2,'DAX Index',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (251,2,'Euro stoxx 50',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (252,2,'Euro stoxx Bank Index',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (253,2,'CAC 40 Index',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (254,2,'Dow Jones Indus Avg',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (255,2,'Nasdaq 100 Composite Index',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (256,2,'Porsche',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (257,2,'Daimler',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (258,2,'LinkedIn',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (259,2,'Apple',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (260,2,'Alibaba',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (261,2,'Netflix',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (262,2,'Amazon',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (263,2,'Twitter',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (264,2,'Facebook',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (265,2,'Credit Suisse',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (266,2,'UBS',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (267,2,'Deutsche Bank',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (268,2,'Julius Baer',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (269,2,'Novartis ',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (270,2,'Roche',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (271,2,'Swatch',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (272,2,'Swiss Re',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (273,2,'AXA',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (274,2,'ING',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (275,2,'Allianz',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (276,2,'BMW',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (277,2,'Volkswagen',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (278,2,'Microsoft',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (279,2,'Adidas',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (280,2,'Coca-Cola',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (281,2,'Cisco',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (282,2,'Paypal',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (283,2,'Citi Group',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (284,2,'Bank Of America',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (285,2,'Goldman Sachs',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (286,2,'Pfizer',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (287,2,'Procter&gamble',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (288,2,'AIG',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (289,2,'Gold',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (290,2,'WTI Crude Oil',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (291,2,'Silver',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (292,2,'US 10Y IRS',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (293,2,'EUR/USD',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (294,2,'GBP/USD',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (295,2,'USD/JPY',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (296,2,'Cosi',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (297,2,'TCM',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (307,2,'contact us',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (652,2,'Month',str_to_date('14/09/2016 13:06.27','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (653,2,'Months',str_to_date('14/09/2016 13:06.27','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (654,2,'year',str_to_date('14/09/2016 13:06.27','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (725,2,'risk appetite',str_to_date('16/09/2016 08:07.15','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (726,2,'capital guaranty',str_to_date('16/09/2016 08:07.15','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (727,2,'protection',str_to_date('16/09/2016 08:07.15','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (728,2,'participation',str_to_date('16/09/2016 08:07.15','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (729,2,'capital protection',str_to_date('16/09/2016 08:07.15','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (730,2,'bondfloor at issuance',str_to_date('16/09/2016 08:07.15','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (731,2,'upside',str_to_date('16/09/2016 08:07.15','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (732,2,'barrier',str_to_date('16/09/2016 08:07.15','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (733,2,'barrier level',str_to_date('16/09/2016 08:07.15','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (734,2,'barrier type',str_to_date('16/09/2016 08:07.15','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (735,2,'barrier observation period',str_to_date('16/09/2016 08:07.15','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (736,2,'barrier event',str_to_date('16/09/2016 08:07.15','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (737,2,'distance to barrier',str_to_date('16/09/2016 08:07.15','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (738,2,'Initial fixing level',str_to_date('16/09/2016 08:07.15','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (739,2,'observation frequency',str_to_date('16/09/2016 08:07.15','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (740,2,'coupon trigger level',str_to_date('16/09/2016 08:07.15','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (741,2,'bonus level',str_to_date('16/09/2016 08:07.15','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (742,2,'exchange rate',str_to_date('16/09/2016 08:11.46','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (743,2,'BBG ticker',str_to_date('16/09/2016 08:11.46','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (746,2,'currency',str_to_date('16/09/2016 08:11.46','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (565,2,'EUR',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (691,2,'Any.Capital',str_to_date('14/09/2016 20:00.37','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (692,2,'Any.Capital',str_to_date('14/09/2016 20:00.37','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (693,2,'Any.Capital',str_to_date('14/09/2016 20:00.37','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (694,2,'Any.Capital',str_to_date('14/09/2016 20:00.37','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (695,2,'24h',str_to_date('14/09/2016 20:00.37','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (696,2,'Secondary market are quoting dirty, accrued interest is included in the price',str_to_date('14/09/2016 20:00.37','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (697,2,'Secondary market are quoted in percentage',str_to_date('14/09/2016 20:00.37','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (698,2,'Cash settlement ',str_to_date('14/09/2016 20:00.37','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (326,2,'coupon',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (328,2,'min',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (324,2,'faq',str_to_date('10/01/2017 13:32.04','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (324,8,'de faq',str_to_date('10/01/2017 13:32.04','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (325,2,'maturity',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (329,2,'pre-order',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (330,2,'secure this note',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (331,2,'on',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (333,2,'coupon level',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (334,2,'denomination currency',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (335,2,'outcome scenarios',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (336,2,'scenario',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (337,2,'is at or above',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (338,2,'Receive',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (339,2,'market expectation',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (340,2,'underlying',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (341,2,'product description',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (342,2,'product details',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (345,2,'can be increase at any time',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (347,2,'settlement currency',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (349,2,'Yes',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (350,2,'No',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (351,2,'dates',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (357,2,'Redemption',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (358,2,'simulation',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (361,2,'general information',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (362,2,'depositor',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (363,2,'lead manager',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (364,2,'calculation agent',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (365,2,'paying agent',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (366,2,'secondary market',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (367,2,'quoting type',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (368,2,'quotation type',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (369,2,'settlement type',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (370,2,'minimum investment',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (371,2,'product documentation',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (372,2,'prospect for profit and loss',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (373,2,'significant risks',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (374,2,'risk fact relating product',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (375,2,'additional risk factors',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (429,2,'€',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (430,2,'about',str_to_date('09/01/2017 15:39.15','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (431,2,'my account',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (432,2,'articles',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (433,2,'products',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (327,2,'strike level',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (332,2,'final fixing date',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (343,2,'issue price',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (344,2,'issue size',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (346,2,'denomination',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (348,2,'quanto',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (352,2,'subscription end date',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (353,2,'initial fixing date',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (354,2,'first exchange trading date',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (355,2,'last trading date',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (359,2,'final fixing level',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (360,2,'cash settlement',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (561,2,'EUR',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (571,2,'S&P 500 Index',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (572,2,'SMI Index',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (573,2,'FTSE 100 Index',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (574,2,'Nikkei 225',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (575,2,'DAX Index',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (576,2,'Euro stoxx 50',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (577,2,'Euro stoxx Bank Index',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (578,2,'CAC 40 Index',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (579,2,'Dow Jones Indus Avg',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (580,2,'Nasdaq 100 Composite Index',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (581,2,'Porsche',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (582,2,'Daimler',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (583,2,'LinkedIn',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (584,2,'Apple',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (585,2,'Alibaba',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (586,2,'Netflix',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (587,2,'Amazon',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (588,2,'Twitter',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (589,2,'Facebook',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (590,2,'Credit Suisse',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (591,2,'UBS',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (592,2,'Deutsche Bank',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (593,2,'Julius Baer',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (594,2,'Novartis ',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (595,2,'Roche',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (596,2,'Swatch',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (597,2,'Swiss Re',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (598,2,'AXA',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (599,2,'ING',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (600,2,'Allianz',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (601,2,'BMW',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (602,2,'Volkswagen',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (603,2,'Microsoft',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (604,2,'Adidas',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (605,2,'Coca-Cola',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (606,2,'Cisco',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (607,2,'Paypal',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (608,2,'Citi Group',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (609,2,'Bank Of America',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (610,2,'Goldman Sachs',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (611,2,'Pfizer',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (612,2,'Procter&gamble',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (613,2,'AIG',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (614,2,'Gold',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (615,2,'WTI Crude Oil',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (616,2,'Silver',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (617,2,'US 10Y IRS',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (618,2,'EUR/USD',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (619,2,'GBP/USD',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (620,2,'USD/JPY',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (681,2,'Capital Protection Cartificate with Participation',str_to_date('14/09/2016 16:17.04','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (682,2,'Barrier Capital Protection Certificate',str_to_date('14/09/2016 16:17.04','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (683,2,'Capital Protection Certificate with Coupon',str_to_date('14/09/2016 16:17.04','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (684,2,'Reverse Convertible',str_to_date('14/09/2016 16:17.04','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (685,2,'Barrier Reverse Convertible',str_to_date('14/09/2016 16:17.04','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (686,2,'Inverse Barrier Reverse Convertible',str_to_date('14/09/2016 16:17.04','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (687,2,'Express Certificate',str_to_date('14/09/2016 16:17.04','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (688,2,'Outperformance Certificate',str_to_date('14/09/2016 16:17.04','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (689,2,'Bonus Certificate',str_to_date('14/09/2016 16:17.04','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (690,2,'Bonus Outperformance Certificate',str_to_date('14/09/2016 16:17.04','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (712,2,'30U/360',str_to_date('15/09/2016 09:21.43','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (713,2,'3M',str_to_date('15/09/2016 10:56.48','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (714,2,'6M',str_to_date('15/09/2016 10:56.48','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (715,2,'9M',str_to_date('15/09/2016 10:56.48','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (716,2,'1Y',str_to_date('15/09/2016 10:56.48','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (717,2,'18M',str_to_date('15/09/2016 10:56.48','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (718,2,'2Y',str_to_date('15/09/2016 10:56.48','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (719,2,'3Y',str_to_date('15/09/2016 10:56.48','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (720,2,'5Y',str_to_date('15/09/2016 10:56.48','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (721,2,'American',str_to_date('15/09/2016 14:06.10','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (722,2,'European',str_to_date('15/09/2016 14:06.10','%d/%m/%Y %H:%i.%s'),null);


Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (47,2,'bbb',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);

Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (61,2,'TESTING',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (62,2,'Login',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (63,2,'Logout',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (64,2,'User name',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (65,2,'Password',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (66,2,'Home',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (67,2,'Administrator',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (68,2,'Products',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (69,2,'Translations',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (70,2,'Search',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (71,2,'Save',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (72,2,'Remove',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (73,2,'Add',str_to_date('10/01/2017 13:16.20','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (74,2,'Key',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (75,2,'Edit product',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (76,2,'ID',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (77,2,'Product type',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (78,2,'Issue price',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (79,2,'Currency id',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (80,2,'Denomination',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (81,2,'Coupon frequency',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (82,2,'Day count convention id',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (83,2,'Max yield',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (84,2,'Max yield pa',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (85,2,'Level of protection',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (86,2,'Level of participation',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (87,2,'Barrier start',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (88,2,'Barrier end',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (89,2,'Strike level',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (90,2,'Bonus precentage',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (91,2,'Subscription start date',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (92,2,'Subscription end date',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (93,2,'Initial fixing date',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (94,2,'Issue date',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (95,2,'First exchange trading date',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (96,2,'Last trading date',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (97,2,'Final fixing date',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (98,2,'Redemption date',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (99,2,'Quanto',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (100,2,'Product insurance type',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (101,2,'Bond floor at issuance',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (102,2,'Issue size',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (103,2,'Priority',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (104,2,'Markets',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (105,2,'Product scenarios',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (106,2,'Final fixing level',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (107,2,'Text',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (108,2,'Product coupons',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (109,2,'Observation date',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (110,2,'Payment date',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (111,2,'Trigger level',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (112,2,'Coupon rate',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (113,2,'Product autocalls',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (114,2,'Examination date',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (115,2,'Condition',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (116,2,'Product callables',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (117,2,'Product simulation',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (118,2,'Cash settlement',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (628,2,'You have to be logged in',str_to_date('10/09/2016 16:26.04','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (627,2,'Login',str_to_date('10/09/2016 16:19.10','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (629,2,'Email',str_to_date('10/09/2016 16:26.04','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (630,2,'Password',str_to_date('10/09/2016 16:26.04','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (631,2,'Remember me',str_to_date('10/09/2016 16:26.04','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (632,2,'Forgot password',str_to_date('10/09/2016 16:26.04','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (633,2,'Support',str_to_date('10/09/2016 16:26.04','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (634,2,'Hello',str_to_date('10/09/2016 16:26.04','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (635,2,'Logout',str_to_date('10/09/2016 16:28.06','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (636,2,'Other',str_to_date('10/09/2016 16:30.15','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (637,2,'no coupon',str_to_date('10/09/2016 16:32.43','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (638,2,'Monthly',str_to_date('10/09/2016 16:32.43','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (639,2,'Quaterly',str_to_date('10/09/2016 16:32.43','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (640,2,'Semi-Annual',str_to_date('10/09/2016 16:32.43','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (641,2,'Annual',str_to_date('10/09/2016 16:32.43','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (642,2,'In Fine',str_to_date('10/09/2016 16:32.43','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (643,2,'EUR',str_to_date('10/09/2016 16:33.56','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (644,2,'reset',str_to_date('11/09/2016 11:58.17','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (645,2,'back',str_to_date('11/09/2016 11:58.17','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (646,2,'Please enter the email you used when registering and opening your account with us.',str_to_date('11/09/2016 11:58.17','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (647,2,'email',str_to_date('11/09/2016 11:58.17','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (122,2,'Users',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (123,2,'Transactions',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (124,2,'Investments',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (125,2,'Totals',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (155,2,'Capital Protection Cartificate with Participation',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (156,2,'Barrier Capital Protection Certificate',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (119,2,'Add product',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (120,2,'Product category',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (121,2,'Currency',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (157,2,'Capital Protection Certificate with Coupon',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (158,2,'Reverse Convertible',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (159,2,'Barrier Reverse Convertible',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (160,2,'Inverse Barrier Reverse Convertible',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (161,2,'Express Certificate',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (162,2,'Outperformance Certificate',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (163,2,'Bonus Certificate',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (164,2,'Bonus Outperformance Certificate',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (300,2,'test3333',str_to_date('04/09/2016 06:26.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (879,2,'up to Cap level',str_to_date('30/09/2016 09:22.06','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (880,2,'The Investor is entitled to receive from the Issuer on the Redemption Date per Product:',str_to_date('30/09/2016 09:22.12','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (881,2,'The Investor is entitled to receive from the Issuer on the Redemption Date per Product:',str_to_date('30/09/2016 09:22.37','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (882,2,'The Investor is entitled to receive the Conditional Coupon Amount on the Redemption Date if the Final Fixing Level is above the Coupon Trigger Level on the Final Fixing Date. In addition the Investor is entitled to receive from the Issuer on the Redemption Date per Product:',str_to_date('30/09/2016 09:22.45','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (883,2,'The Coupon Amount(s) per Product will be paid in any case at the respective Coupon Payment Date(s). In addition the Investor is entitled to receive from the Issuer on the Redemption Date per Product:',str_to_date('30/09/2016 09:22.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (884,2,'The Coupon Amount(s) per Product will be paid in any case at the respective Coupon Payment Date(s). In addition the Investor is entitled to receive from the Issuer on the Redemption Date per Product:',str_to_date('30/09/2016 09:22.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (885,2,'The Coupon Amount(s) per Product will be paid in any case at the respective Coupon Payment Date(s). In addition the Investor is entitled to receive from the Issuer on the Redemption Date per Product:',str_to_date('30/09/2016 09:22.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (886,2,'The Investor is entitled to receive the Conditional Coupon Amount if the Final Fixing Level is above the Initial Fixing Level. In addition the Investor is entitled to receive from the Issuer on the Redemption Date per Product:',str_to_date('30/09/2016 09:22.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (887,2,'The Investor is entitled to receive from the Issuer on the Redemption Date per Product:',str_to_date('30/09/2016 09:22.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (888,2,'The Investor is entitled to receive from the Issuer on the Redemption Date per Product:',str_to_date('30/09/2016 09:22.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (889,2,'The Investor is entitled to receive from the Issuer on the Redemption Date per Product:',str_to_date('30/09/2016 09:22.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (890,2,'coupon trigger event',str_to_date('30/09/2016 09:22.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (891,2,'cap level',str_to_date('30/09/2016 09:22.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (892,2,'An observed price of the Underlying on the Initial Fixing Date, as reasonably determined by the Calculation Agent',str_to_date('30/09/2016 09:22.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (893,2,'An observed price of the Underlying on the Final Fixing Date, as reasonably determined by the Calculation Agent',str_to_date('30/09/2016 09:22.58','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (894,2,'A Barrier Event shall be deemed to occur if at any time on any Exchange Business Day during the Barrier Observation Period the level of the Underlying''s price has been traded at or below the Barrier Level, as reasonably determined by the Calculation Agent',str_to_date('30/09/2016 09:22.58','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (897,2,'A Coupon Trigger Event shall be deemed to occur, if on any Coupon Observation Date all Underlyings close above their respective Coupon Trigger Level, as reasonably determined by the Calculation Agent.',str_to_date('30/09/2016 09:22.58','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (899,2,'Maximum participation of the positive performance of the underlying',str_to_date('30/09/2016 09:22.58','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (950,2,'My Notes',str_to_date('22/01/2017 15:00.36','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (951,2,'Funding',str_to_date('22/01/2017 15:00.50','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (952,2,'open',str_to_date('13/10/2016 12:16.51','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (953,2,'closed',str_to_date('13/10/2016 12:16.51','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (954,2,'deposit',str_to_date('13/10/2016 12:16.51','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (955,2,'withdraw',str_to_date('13/10/2016 12:16.51','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (956,2,'cancel withdraw',str_to_date('13/10/2016 12:16.51','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (957,2,'banking history',str_to_date('13/10/2016 12:16.51','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (958,2,'personal details',str_to_date('13/10/2016 12:16.51','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (959,2,'password',str_to_date('13/10/2016 12:16.51','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (960,2,'Username',str_to_date('13/10/2016 12:16.51','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (961,2,'First name',str_to_date('13/10/2016 12:16.51','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (962,2,'Last name',str_to_date('13/10/2016 12:16.51','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (963,2,'Day of birth',str_to_date('13/10/2016 12:16.51','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (964,2,'Gender',str_to_date('13/10/2016 12:16.51','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (965,2,'Email',str_to_date('13/10/2016 12:16.51','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (966,2,'Street address',str_to_date('13/10/2016 12:16.51','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (967,2,'House / Flat number',str_to_date('13/10/2016 12:16.51','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (968,2,'City',str_to_date('13/10/2016 12:16.51','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (969,2,'Postal Code/Zip Code',str_to_date('13/10/2016 12:16.51','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (970,2,'Country',str_to_date('13/10/2016 12:16.51','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (971,2,'Mobile',str_to_date('13/10/2016 12:16.51','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (972,2,'Phone',str_to_date('13/10/2016 12:16.51','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (973,2,'Thanks, but don''t email me newsletters, updates and special offers',str_to_date('13/10/2016 12:16.51','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (974,2,'Thanks, but don''t send me text messages with updates and offers',str_to_date('13/10/2016 12:16.51','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (975,2,'submit',str_to_date('13/10/2016 12:16.51','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (976,2,'Search',str_to_date('13/10/2016 12:16.51','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1423,2,'How do you calculate the Redemption Amount?',str_to_date('16/01/2017 14:47.09','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1471,2,'If you have forgotten your password, press "Forgot password?" on the Homepage by the login button, fill in the form that opens, and press "Send". Your password will be sent to the email indicated in the form as long as it matches the original address that was provided when you registered. If you have forgotten your username, please contact our Customer Services team by phone or by email:support@anyoption.com',str_to_date('16/01/2017 15:33.22','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1472,2,'All your deposits, withdrawals and investments history can be viewed in the My Account section. Just login to your account and select the information you wish to view.',str_to_date('16/01/2017 15:33.29','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1473,2,'You can update the information in My Account>Personal Details or contact us by email support@anyoption.com',str_to_date('16/01/2017 15:33.45','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1474,2,'No. To invest at anycapital, all you need to do is register an account and deposit. No download is required. If you wish to trade via your mobile phone or tablet, you can download our trading app from the App Store and/or Google Play and enjoy trading anytime, anywhere.',str_to_date('16/01/2017 15:33.50','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1475,2,'You can easily change language at any time by going to the languages drop-down menu on the homepage. When logged in, you will view the site in the same language you selected when you opened your account. If you want to change this selection please contact Customer Services.',str_to_date('16/01/2017 15:33.54','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (773,2,'American',str_to_date('16/09/2016 13:29.27','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (774,2,'European',str_to_date('16/09/2016 13:29.27','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (775,2,'{asset} closes at {marketPrice}',str_to_date('16/09/2016 14:44.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (776,2,'{asset} closes at {marketPrice}',str_to_date('16/09/2016 14:44.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (777,2,'{asset} closes below {marketPrice}',str_to_date('16/09/2016 14:44.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (778,2,'{asset} closes at {marketPrice}',str_to_date('16/09/2016 14:44.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (779,2,'{asset} closes above {marketPrice}',str_to_date('16/09/2016 14:44.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (780,2,'{asset} closes below {marketPrice}',str_to_date('16/09/2016 14:44.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (781,2,'{asset} closes above {marketPrice}',str_to_date('16/09/2016 14:44.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (782,2,'{asset} closes below {marketPrice}',str_to_date('16/09/2016 14:44.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (783,2,'{asset} closes at {marketPrice}',str_to_date('16/09/2016 14:44.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (784,2,'{asset} closes above {marketPrice}',str_to_date('16/09/2016 14:44.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (785,2,'{asset} closes at {marketPrice}',str_to_date('16/09/2016 14:44.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (786,2,'{asset} closes at {marketPrice}',str_to_date('16/09/2016 14:44.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (787,2,'{asset} never touches {marketPrice}',str_to_date('16/09/2016 14:44.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (788,2,'{asset} closes at {marketPrice}',str_to_date('16/09/2016 14:44.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (789,2,'{asset} closes at {marketPrice}',str_to_date('16/09/2016 14:44.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (790,2,'{asset} never touches {marketPrice}',str_to_date('16/09/2016 14:44.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (791,2,'{asset} closes at {marketPrice}',str_to_date('16/09/2016 14:44.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (792,2,'{asset} closes at {marketPrice}',str_to_date('16/09/2016 14:44.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (793,2,'{asset} closes above {marketPrice}',str_to_date('16/09/2016 14:44.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (794,2,'{asset} closes at {marketPrice}',str_to_date('16/09/2016 14:44.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (795,2,'{asset} closes at {marketPrice}',str_to_date('16/09/2016 14:44.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (796,2,'{asset} closes at {marketPrice}',str_to_date('16/09/2016 14:44.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (797,2,'{asset} closes at {marketPrice}',str_to_date('16/09/2016 14:44.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (798,2,'{asset} closes at {marketPrice}',str_to_date('16/09/2016 14:44.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (799,2,'{asset} closes at {marketPrice}',str_to_date('16/09/2016 14:44.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (800,2,'{asset} closes at {marketPrice}',str_to_date('16/09/2016 14:44.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (801,2,'{asset} closes at {marketPrice}',str_to_date('16/09/2016 14:44.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (802,2,'{asset} closes at {marketPrice}',str_to_date('16/09/2016 14:44.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (803,2,'{asset} closes at {marketPrice}',str_to_date('16/09/2016 14:44.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (804,2,'{asset} closes at {marketPrice}',str_to_date('16/09/2016 14:44.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (805,2,'Barrier level',str_to_date('16/09/2016 15:21.39','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (806,2,'Bonus level',str_to_date('16/09/2016 15:21.39','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (931,2,'<b>* Estimate.</b> The final number that is binding is based on the actual closing price on the fixing date.',str_to_date('07/10/2016 07:41.16','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (932,2,'all notes',str_to_date('07/10/2016 07:41.16','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (933,2,'Last',str_to_date('07/10/2016 07:41.16','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (934,2,'outcome scenarios for',str_to_date('07/10/2016 07:41.16','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (935,2,'continue',str_to_date('07/10/2016 07:41.16','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (936,2,'none',str_to_date('07/10/2016 07:41.16','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (937,2,'<b>* Estimate.</b> The final number that is binding is based on the actual closing price on the fixing date. Hit the + sign for full details',str_to_date('07/10/2016 07:41.16','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (938,2,'discount',str_to_date('07/10/2016 07:41.16','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (939,2,'premium',str_to_date('07/10/2016 07:41.16','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (941,2,'Publish',str_to_date('09/10/2016 11:22.39','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1176,2,'Beneficiary Name',str_to_date('05/12/2016 08:45.33','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1178,2,'Swift',str_to_date('05/12/2016 08:46.02','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1179,2,'Iban',str_to_date('05/12/2016 08:46.12','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1180,2,'Account Number',str_to_date('05/12/2016 08:46.20','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1181,2,'Account Info',str_to_date('09/01/2017 15:53.12','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1182,2,'Bank Name',str_to_date('05/12/2016 08:46.36','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1183,2,'Branch Address',str_to_date('05/12/2016 08:46.51','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1184,2,'Amount',str_to_date('05/12/2016 08:46.58','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1185,2,'Bank Fee Amount',str_to_date('05/12/2016 08:47.04','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1186,2,'Comments',str_to_date('05/12/2016 08:47.10','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1187,2,'Payment Type',str_to_date('05/12/2016 08:47.16','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1188,2,'Created At',str_to_date('05/12/2016 08:47.21','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1189,2,'Settled At',str_to_date('05/12/2016 08:47.26','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1190,2,'Credit / Debit',str_to_date('05/12/2016 08:47.37','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1191,2,'Ip',str_to_date('05/12/2016 08:47.41','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1192,2,'Email',str_to_date('05/12/2016 08:47.48','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1193,2,'Current Password',str_to_date('05/12/2016 08:47.54','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1194,2,'New Password',str_to_date('05/12/2016 08:47.58','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1195,2,'Verify Password',str_to_date('05/12/2016 08:48.03','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1196,2,'Submit',str_to_date('05/12/2016 08:48.08','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1466,2,'Yes. There is a minimal withdrawal amount that is equal to the minimum deposit amount of your country.
If you wish to withdraw a smaller amount than the minimum, you will be charged with a fee. Withdrawal fees are applicable based on anyoption''s™ general terms and conditions. Prior to carrying out your withdrawal request you will be asked to send us identification papers. For full details see the Banking page.
',str_to_date('16/01/2017 00:00.00','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1476,2,'Capital Guarantee products',str_to_date('17/01/2017 13:13.52','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1477,2,'Yield Enhancement products',str_to_date('17/01/2017 13:13.58','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1478,2,'Participation products',str_to_date('17/01/2017 13:14.04','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1479,2,'Capital Guarantee Certificate with participation
This kind of product allow you to participate in the increase of an asset without risking your entire investment like it is the case if you buy the asset itself. It creates an asymmetric profit profile in the sense that your downside is limited and your upside is unlimited. When buying such product, you think the asset will increase strongly but don’t want to be exposed in case the asset goes down.
 Barrier capital guarantee certificate
This product allows you to participate in the increase of an asset but up to a certain level called the Barrier, without risking your entire investment like it is the case if you buy the asset itself.If the Barrier is breached you don’t participate in the increase of the asset anymore.It creates an asymmetric profit profile in the sense that your downside is much more limited than your upside.When buying such product, you think the asset’s increase will be limited and the barrier will never be breached.
 Capital guarantee certificate with coupon 
This product allows you to receive a fixed amount of money on your investment called the coupon, simply if the asset goes up no matter how much. Your investment is protected and the level of protection is a function of the level of the coupon: the higher is the coupon the lower is the protection.When buying such product, you think the asset cannot go down but you are not sure how much it can goes up.',str_to_date('17/01/2017 13:18.01','%d/%m/%Y %H:%i.%s'),null);

Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1482,2,'Outperformance Certificate 
This product allows you to participate in a disproportionate way to the positive performance of the asset. If the asset goes down your investment performance is similar than if you buy the asset itself. When buying such product, you strongly believe that the asset will goes up.  
Bonus certificate 
This product allows you to receive a fixed amount of money on your investment called the Bonus if the asset never breach a certain level called the Barrier and allows you to participate on the asset increase if it goes up more than the bonus offers you. If the Barrier is breached your investment performance is similar than if you buy the asset itself. The Barrier level is always set much lower than the current market price When buying such product, you think the asset will stagnate or goes up strongly, but will in any case breached the Barrier.',str_to_date('17/01/2017 13:28.32','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1484,2,'Structured Products, Structured Deposits and Structured Investments',str_to_date('18/01/2017 07:57.26','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1487,2,'<p>A structured product ties up your money for a set time and might be designed to give you income, growth or both. Structured products are complex and can be more risky than they seem, so get professional financial advice if you’re not sure whether they’re right for you.</p><p>What is a structured product?</p><p>How structured deposits work</p><p>How structured investments work</p><p>Risk of structured products</p><p>Information you should be given for structured products</p>',str_to_date('18/01/2017 07:58.00','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1488,2,'Won''t Somebody Please Think of the Asian Structured Products?!',str_to_date('18/01/2017 08:02.58','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1490,2,'<div class="article-info"><div>by Tracy Alloway</div><time datetime="2016-01-15T13:21:00.000Z">January 15, 2016 — 8:21 AM EST</time></div><p>Spot the link?</p><p>Risk.net reported earlier this week that the HSCEI, which closed at 8,236.28 on Friday, is nearing a "danger level" that would lead to a bunch of such autocallable products being "knocked in." In other words, a point at which investors in many of these structured products, which allow them to make sizable short-term bets on the direction of certain stock markets, could lose a chunk of their principal.</p>',str_to_date('18/01/2017 08:03.41','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (817,2,'Maturity',str_to_date('21/09/2016 10:23.10','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (818,2,'Barrier Type',str_to_date('21/09/2016 10:23.10','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (841,2,'Cap level',str_to_date('28/09/2016 06:41.07','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (842,2,'TBA',str_to_date('28/09/2016 06:55.48','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (843,2,'Continuous barrier monitoring',str_to_date('28/09/2016 06:55.48','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (844,2,'Barrier monitoring only at Final Fixing Date',str_to_date('28/09/2016 06:55.48','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (845,2,'Exchange Market Close',str_to_date('28/09/2016 06:55.48','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (846,2,'10 NY time',str_to_date('28/09/2016 06:55.48','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (847,2,'purchase this note',str_to_date('28/09/2016 06:55.48','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (848,2,'unlimited',str_to_date('28/09/2016 06:55.48','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (849,2,'cap',str_to_date('28/09/2016 06:55.48','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1028,2,'Sorry. We don''t recognize any of the details you provided.',str_to_date('26/10/2016 11:31.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1029,2,'Set up a new Any.Capital account',str_to_date('26/10/2016 11:31.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1030,2,'You can have access to our full product range.',str_to_date('26/10/2016 11:31.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1031,2,'I accept the <a ng-href="{terms-link}">Terms & Conditions</a> and confirm that I am 18 years old or older.',str_to_date('26/10/2016 11:31.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1032,2,'sign up',str_to_date('26/10/2016 11:31.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1033,2,'We can send a reset link to help you prove that you are the owner of this account.',str_to_date('26/10/2016 11:31.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1034,2,'Send me via',str_to_date('26/10/2016 11:31.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1035,2,'SMS',str_to_date('26/10/2016 11:31.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1036,2,'contact',str_to_date('26/10/2016 11:31.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1037,2,'privacy policy',str_to_date('26/10/2016 11:31.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1038,2,'terms of use and legal information',str_to_date('26/10/2016 11:31.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1108,2,'Please change your selected amount, the minimum Note size is {min-inv-amount}',str_to_date('21/11/2016 15:42.35','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1109,2,'Please change your selected amount, the maximum Note size is {max-inv-amount}',str_to_date('21/11/2016 15:42.35','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1110,2,'Your available funds are lower than the minimal Note size. Please proceed to fund your account first.',str_to_date('21/11/2016 15:42.35','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1111,2,'We have already secured for you this Note. If you wish to secure more Notes you will need to fund your account.',str_to_date('21/11/2016 15:42.35','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1112,2,'This Note is currently unavailable.',str_to_date('21/11/2016 15:42.35','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1113,2,'This Note is currently unavailable.',str_to_date('21/11/2016 15:42.35','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1114,2,'This Note is already issued. You can try to buy it in secondary market.',str_to_date('21/11/2016 15:42.35','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1115,2,'Price has been changed. Please refresh the page and try again.',str_to_date('21/11/2016 15:42.35','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1116,2,'view available notes',str_to_date('21/11/2016 15:42.35','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1117,2,'show me updated terms',str_to_date('21/11/2016 15:42.36','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1118,2,'refresh',str_to_date('21/11/2016 15:42.36','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1119,2,'if a barrier event HAS not occur then if the Final Fixing Level is at or below the Initial Fixing Level, the Investor will receive:',str_to_date('21/11/2016 15:48.45','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1120,2,'if a barrier event HAS not occur then if the Final Fixing Level is above the Initial Fixing Level, the Investor will receive:',str_to_date('21/11/2016 15:48.45','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1121,2,'if a Barrier Event HAS occurred, the Investor will receive:',str_to_date('21/11/2016 15:48.45','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1122,2,'If a Barrier Event has NOT occurred, the Investor will receive a Cash Settlement in the Settlement Currency equal to:',str_to_date('21/11/2016 15:48.45','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1123,2,'If a Barrier Event HAS occurred, the investor will receive a cash settlement in the settlement currency equal to:',str_to_date('21/11/2016 15:48.45','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1124,2,'If a Barrier Event HAS occurred and If the Final Fixing Level is above the Initial Fixing Level, the Investor will receive a Cash Settlement in the Settlement Currency equal to:',str_to_date('21/11/2016 15:48.46','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1125,2,'If a Barrier Event has NOT occurred, the Investor will receive a Cash Settlement in the Settlement Currency equal to:',str_to_date('21/11/2016 15:48.46','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1126,2,'If a Barrier Event HAS occurred, the investor will receive a cash settlement in the settlement currency equal to:',str_to_date('21/11/2016 15:48.46','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1127,2,'If a Barrier Event HAS occurred and If the Final Fixing Level is below the Initial Fixing Level, the Investor will receive a Cash Settlement in the Settlement Currency equal to:',str_to_date('21/11/2016 15:48.46','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1128,2,'If a Barrier Event has NOT occurred, the Investor will receive a Cash Settlement in the Settlement Currency equal to:',str_to_date('21/11/2016 15:48.46','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1129,2,'If a Barrier Event HAS occurred, the investor will receive a cash settlement in the settlement currency equal to:',str_to_date('21/11/2016 15:48.46','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1130,2,'If a Barrier Event HAS occurred and If the Final Fixing Level is above the Initial Fixing Level, the Investor will receive a Cash Settlement in the Settlement Currency equal to:',str_to_date('21/11/2016 15:48.46','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1131,2,'If a Barrier Event has NOT occurred and if the Final Fixing Level is at or below the Initial Fixing Level multiplied with the Bonus Level (in %), the Investor will receive:',str_to_date('21/11/2016 15:48.46','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1132,2,'If a Barrier Event has NOT occurred and if the Final Fixing Level is above the Initial Fixing Level multiplied with the Bonus Level (in %), the Investor will receive:',str_to_date('21/11/2016 15:48.46','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1133,2,'If a Barrier Event HAS occurred, the Investor will receive:',str_to_date('21/11/2016 15:48.46','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1134,2,'If a Barrier Event has NOT occurred and if the Final Fixing Level is at or below the Initial Fixing Level multiplied with the Bonus Level (in %), the Investor will receive:',str_to_date('21/11/2016 15:48.46','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1135,2,'If a Barrier Event has NOT occurred and if the Final Fixing Level is above the Initial Fixing Level multiplied with the Bonus Level (in %), the Investor will receive:',str_to_date('21/11/2016 15:48.46','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1136,2,'If a Barrier Event HAS occurred and If the Final Fixing Level is at or below the Initial Fixing Level multiplied with the Bonus Level (in %), the Investor will receive:',str_to_date('21/11/2016 15:48.46','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1137,2,'If a Barrier Event HAS occurred and if the Final Fixing Level is above the Initial Fixing Level multiplied with the Bonus Level (in %), the Investor will receive:',str_to_date('21/11/2016 15:48.46','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1139,2,'A Barrier Event should be deemed to occur if at the Final Fixing Date the Final Fixing Level is at or above the Barrier Level, as reasonably determined by the calculation Agent',str_to_date('21/11/2016 15:48.46','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1140,2,'A Barrier Event should be deemed to occur if at the Final Fixing Date the Final Fixing Level is at or below the Barrier Level, as reasonably determined by the calculation Agent',str_to_date('21/11/2016 15:48.46','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1141,2,'A Barrier Event should be deemed to occur if at the Final Fixing Date the Final Fixing Level is at or above the Barrier Level, as reasonably determined by the calculation Agent',str_to_date('21/11/2016 15:48.46','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1142,2,'A Barrier Event should be deemed to occur if at the Final Fixing Date the Final Fixing Level is at or below the Barrier Level, as reasonably determined by the calculation Agent',str_to_date('21/11/2016 15:48.46','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1143,2,'A Barrier Event should be deemed to occur if at the Final Fixing Date the Final Fixing Level is at or below the Barrier Level, as reasonably determined by the calculation Agent',str_to_date('21/11/2016 15:48.46','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1144,2,'A Barrier Event should be deemed to occur if at the Final Fixing Date the Final Fixing Level is at or below the Barrier Level, as reasonably determined by the calculation Agent',str_to_date('21/11/2016 15:48.46','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1093,2,'Your note was purchased!',str_to_date('11/11/2016 12:35.06','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1094,2,'For information on your purchased notes go to <a href="{my-notes-link}">My Notes</a>.',str_to_date('11/11/2016 12:35.06','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1095,2,'Your note was secured!',str_to_date('11/11/2016 12:35.06','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1096,2,'Please proceed to fund your account according to your investment amount prior to the product''s initial fixing date {initial-fixing-date} in order to ensure your subscription',str_to_date('11/11/2016 12:35.06','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1097,2,'fund your account',str_to_date('11/11/2016 12:35.06','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1098,2,'You have only {balance} available. We can purchase this Note for you at this amount.',str_to_date('11/11/2016 12:35.06','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1099,2,'ok',str_to_date('11/11/2016 12:35.06','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1100,2,'No, thanks. I want to <a href="{increase-balance-link}">increase my balance</a> first',str_to_date('11/11/2016 12:35.06','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (940,2,'original lifetime',str_to_date('07/10/2016 12:54.11','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1005,2,'How to invest in protected Capital Notes?',str_to_date('25/10/2016 09:07.02','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1006,2,'Find a market assumption that sounds probable to you',str_to_date('17/01/2017 13:10.05','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1007,2,'Check the best and worst case scenarios for your note',str_to_date('25/10/2016 09:07.02','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1008,2,'Choose the right deposit amount for your personal needs',str_to_date('25/10/2016 09:07.02','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1009,2,'Secure a Note before it''s issued or buy an existing Live Note',str_to_date('25/10/2016 09:07.02','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1010,2,'Deposit length is highlighted but an early exit may be offered',str_to_date('25/10/2016 09:07.02','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1011,2,'about us',str_to_date('25/10/2016 09:07.02','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1012,2,'more',str_to_date('25/10/2016 09:07.25','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1066,2,'Type',str_to_date('03/11/2016 09:02.09','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1065,2,'Category',str_to_date('03/11/2016 09:02.33','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1067,2,'Market',str_to_date('03/11/2016 09:03.06','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1068,2,'Status',str_to_date('03/11/2016 09:03.12','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1069,2,'Subscription date',str_to_date('03/11/2016 09:03.18','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1157,2,'Access denied',str_to_date('21/11/2016 16:02.03','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1158,2,'Started',str_to_date('28/11/2016 14:56.03','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1159,2,'Succeed',str_to_date('28/11/2016 14:56.03','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1160,2,'Failed',str_to_date('28/11/2016 14:56.03','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1161,2,'transaction.class.1',str_to_date('28/11/2016 14:56.03','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (928,2,'End trade level',str_to_date('05/10/2016 12:28.40','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (929,2,'Suspend',str_to_date('05/10/2016 12:29.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (930,2,'Barrier occur',str_to_date('05/10/2016 12:29.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1001,2,'Current password',str_to_date('21/10/2016 11:55.42','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1002,2,'New password',str_to_date('21/10/2016 11:55.42','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1003,2,'Retype new password',str_to_date('21/10/2016 11:55.42','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1004,2,'view all notes',str_to_date('21/10/2016 11:55.42','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1162,2,'transaction.class.2',str_to_date('28/11/2016 14:56.03','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1163,2,'Real',str_to_date('28/11/2016 14:56.03','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1164,2,'Not Real',str_to_date('28/11/2016 14:56.03','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1165,2,'Bank Wire',str_to_date('28/11/2016 14:56.03','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1166,2,'Started',str_to_date('28/11/2016 14:57.10','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1167,2,'Succeed',str_to_date('28/11/2016 14:57.10','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1168,2,'Failed',str_to_date('28/11/2016 14:57.10','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1169,2,'Real',str_to_date('28/11/2016 14:57.10','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1170,2,'Not Real',str_to_date('28/11/2016 14:57.10','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1171,2,'Credit',str_to_date('28/11/2016 14:57.10','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1172,2,'Debit',str_to_date('28/11/2016 14:57.10','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1173,2,'Bank Wire',str_to_date('28/11/2016 14:57.10','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1197,2,'USD',str_to_date('06/12/2016 08:56.28','%d/%m/%Y %H:%i.%s'),null);
-- Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1198,2,'transaction.operation.2',now(),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1226,2,'Open account',str_to_date('12/12/2016 13:29.31','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1227,2,'Any.Capital team is at your service.<br>Please contact us with any query',str_to_date('12/12/2016 13:29.31','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1228,2,'support@anycapital.com',now(),null); 
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1229,2,'Phone number',str_to_date('12/12/2016 13:29.31','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1230,2,'This is a toll line',str_to_date('12/12/2016 13:29.31','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1231,2,'Fax number',str_to_date('12/12/2016 13:29.31','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1232,2,'Working hours',str_to_date('12/12/2016 13:29.31','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1233,2,'24 hours a day.',str_to_date('12/12/2016 13:29.31','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1234,2,'Address',str_to_date('12/12/2016 13:29.31','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1235,2,'Ouroboros Derivatives Trading Ltd, 42-44 Griva Digeni Avenue, 1096, Nicosia, Cyprus',str_to_date('12/12/2016 13:29.31','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1236,2,'Issue',str_to_date('12/12/2016 13:29.31','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1237,2,'Comments',str_to_date('12/12/2016 13:29.32','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1238,2,'Send',str_to_date('12/12/2016 13:29.32','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1239,2,'Balance',str_to_date('12/12/2016 13:29.32','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1240,2,'Use same password as your anyoption''s one',str_to_date('12/12/2016 13:29.32','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1241,2,'Load more',str_to_date('12/12/2016 13:29.32','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1242,2,'You pay',str_to_date('12/12/2016 13:29.32','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1243,2,'product',str_to_date('12/12/2016 13:29.32','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1244,2,'Purchased at',str_to_date('12/12/2016 13:29.32','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1245,2,'Sell now for',str_to_date('12/12/2016 13:29.32','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1246,2,'Buy more at',str_to_date('12/12/2016 13:29.32','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1247,2,'Prod ID',str_to_date('12/12/2016 13:29.32','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1248,2,'Inv ID',str_to_date('12/12/2016 13:29.32','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1291,2,'Approve',str_to_date('01/01/2017 16:50.31','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1292,2,'Cancel',str_to_date('01/01/2017 16:56.52','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1293,2,'First Approval',str_to_date('01/01/2017 16:57.00','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1294,2,'Second Approval',str_to_date('01/01/2017 16:57.08','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1295,2,'Ask',str_to_date('01/01/2017 16:57.38','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1296,2,'Bid',str_to_date('01/01/2017 16:57.45','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1297,2,'All',str_to_date('02/01/2017 07:53.55','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1298,2,'Bid/Ask',str_to_date('02/01/2017 07:55.17','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1299,2,'Observation Level',str_to_date('02/01/2017 08:02.21','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1300,2,'Web Page',str_to_date('02/01/2017 08:02.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1301,2,'View',str_to_date('02/01/2017 08:06.32','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1302,2,'From',str_to_date('02/01/2017 08:08.20','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1303,2,'To',str_to_date('02/01/2017 08:08.28','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1304,2,'Sources',str_to_date('02/01/2017 08:09.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1305,2,'Landing Pages',str_to_date('02/01/2017 08:10.03','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1306,2,'Contents',str_to_date('02/01/2017 08:10.10','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1307,2,'Name',str_to_date('02/01/2017 08:11.17','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1308,2,'Add Source',str_to_date('02/01/2017 08:11.23','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1309,2,'Path',str_to_date('02/01/2017 08:12.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1310,2,'Add Landing Page',str_to_date('02/01/2017 08:12.18','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1311,2,'Add Content',str_to_date('02/01/2017 08:12.59','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1312,2,'Deposits',str_to_date('02/01/2017 08:13.35','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1313,2,'Deposit',str_to_date('02/01/2017 08:14.23','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1314,2,'Edit User',str_to_date('02/01/2017 08:24.45','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1317,2,'First Name',str_to_date('02/01/2017 08:48.16','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1318,2,'Last Name',str_to_date('02/01/2017 08:48.21','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1319,2,'Day Of Birth',str_to_date('02/01/2017 08:48.26','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1320,2,'Gender',str_to_date('02/01/2017 08:48.31','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1321,2,'Wire Withdraw',str_to_date('02/01/2017 08:48.40','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1322,2,'Change Password',str_to_date('02/01/2017 08:48.54','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1323,2,'Load User',str_to_date('02/01/2017 08:48.58','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1324,2,'Balance',str_to_date('02/01/2017 08:49.03','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1325,2,'Mobile Phone',str_to_date('02/01/2017 08:49.11','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1326,2,'Utc Offset',str_to_date('02/01/2017 08:49.15','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1327,2,'Branch Number',str_to_date('02/01/2017 08:49.21','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1329,2,'Add User',str_to_date('02/01/2017 08:55.51','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1332,2,'{asset} close above {marketPrice}',str_to_date('03/01/2017 14:00.35','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1333,2,'{asset} close below {marketPrice}',str_to_date('03/01/2017 14:00.35','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1334,2,'You will receive',str_to_date('03/01/2017 14:00.35','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1335,2,'You have a {amount} note',str_to_date('03/01/2017 14:00.35','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1336,2,'USD',str_to_date('03/01/2017 14:00.35','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1337,2,'Unavailable',str_to_date('03/01/2017 14:00.35','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1338,2,'Confirmed',str_to_date('03/01/2017 14:00.35','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1339,2,'Sell confirmation',str_to_date('03/01/2017 14:00.35','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1340,2,'Sell',str_to_date('03/01/2017 14:00.35','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1341,2,'Writer ID',str_to_date('11/01/2017 15:23.18','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1342,2,'Time Modified',str_to_date('03/01/2017 15:35.32','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1343,2,'pagination',str_to_date('03/01/2017 15:35.38','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1344,2,'Next',str_to_date('03/01/2017 15:35.43','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1345,2,'Previous',str_to_date('03/01/2017 15:35.48','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1346,2,'Previous Group',str_to_date('03/01/2017 15:35.54','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1347,2,'Next Group',str_to_date('03/01/2017 15:35.59','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1348,2,'Url',str_to_date('03/01/2017 15:36.03','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1357,2,'<a href="{fund-your-account-link}">Fund your account</a> to ensure your subscription',str_to_date('05/01/2017 09:51.44','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1358,2,'Redemption amount',str_to_date('05/01/2017 09:51.45','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1359,2,'Coupon amount',str_to_date('05/01/2017 09:51.45','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1360,2,'In subscription',str_to_date('05/01/2017 09:51.45','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1424,2,'Can I get out of my SP before the end of the product life?',str_to_date('16/01/2017 14:47.18','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1425,2,'How do I know of events affecting my product?',str_to_date('16/01/2017 14:47.25','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1426,2,'Can I buy a SP in a different currency than my Account Currency?',str_to_date('16/01/2017 14:47.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1427,2,'How to invest in SP?',str_to_date('16/01/2017 14:47.41','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1428,2,'Does the Issuer knows that I have bought his SP?',str_to_date('16/01/2017 14:47.49','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1429,2,'What happen in case of credit event?',str_to_date('16/01/2017 14:47.56','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1430,2,'Is there a way to get insured against credit event?',str_to_date('16/01/2017 14:48.01','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1431,2,'What is a secondary market price?',str_to_date('16/01/2017 14:48.06','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1432,2,'What is the initial fixing level?',str_to_date('16/01/2017 14:48.12','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1433,2,'What is the final fixing level?',str_to_date('16/01/2017 14:48.17','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1434,2,'What is the issue price?',str_to_date('16/01/2017 14:48.22','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1435,2,'What means Quanto?',str_to_date('16/01/2017 14:48.27','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1436,2,'What are my deposit options?',str_to_date('16/01/2017 15:02.01','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1437,2,'Which currency can I use in my account?',str_to_date('16/01/2017 15:02.10','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1438,2,'Is there a minimal withdrawal amount?',str_to_date('16/01/2017 15:02.19','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1439,2,'How can I withdraw my profit?',str_to_date('16/01/2017 15:02.24','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1440,2,'Must I make a deposit in order to register?',str_to_date('16/01/2017 15:02.30','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1441,2,'Can I cancel my withdrawal request?',str_to_date('16/01/2017 15:02.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1442,2,'Why can’t I request a wire withdrawal?',str_to_date('16/01/2017 15:02.39','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1443,2,'What should I do if I have forgotten or misplaced my password?',str_to_date('16/01/2017 15:02.44','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1444,2,'Where can I view my transaction history?',str_to_date('16/01/2017 15:02.48','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1445,2,'How can I update my personal details?',str_to_date('16/01/2017 15:02.53','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1446,2,'Do I have to download any software in order to trade at anycapital?',str_to_date('16/01/2017 15:02.58','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1447,2,'How do I change my language?',str_to_date('16/01/2017 15:03.03','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1448,2,'Yield Enhancement Products offer a limited (capped) upside, usually in the form of a fixed coupon (or a discount). Investors forgo an unlimited participation in favor of a recurring or one-off payment. Yield Enhancement Products may offer a conditional capital protection, in which case the protection is granted only if a predefined “condition” is met (i.e. a barrier has not been touched). These products are suitable for investors with a moderate to increased risk appetite and the expectation of markets moving sideways over the lifetime of the product.',str_to_date('16/01/2017 15:24.44','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1449,2,'Participation Products offer usually unleveraged participation in the performance of one or multiple underlyings. Participation Products may offer a conditional capital protection, in which case the protection is granted only if a predefined “condition” is met (i.e. a barrier has not been touched). Participation Products are subject to credit risk. These products are suitable for investors with a moderate to increased risk appetite. Market expectation should be a directional move (up or down) over the lifetime of the product.',str_to_date('16/01/2017 15:24.49','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1450,2,'Investors bear the credit risk of the Issuing Parties of the Product. The Products constitute unsubordinated and unsecured obligations of the relevant Issuing Party and rank pari passu with each and all other current and future unsubordinated and unsecured obligations of the relevant Issuing Party. The insolvency of an Issuing Party may lead to a partial or total loss of the invested capital.',str_to_date('16/01/2017 15:24.55','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1451,2,'It depends on the kind of product that the investor have subscribe to. It is always defined in details in the Term Sheet of the product that you can download at your convenience.',str_to_date('16/01/2017 15:25.00','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1452,2,'Yes, at the secondary market price available once a day.',str_to_date('16/01/2017 15:25.06','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1453,2,'Every event affecting a product will be updated on the relevant product on the same day of the event, the investor also will receive an email describing the event and the consequences on the relevant product.',str_to_date('16/01/2017 15:25.11','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1454,2,'Yes, but your account currency will be converted in the currency of the product at the market rate at the time you subscribe to the product. At the redemption date, all proceeds will be converted back to your Account currency at the market rate of that date.',str_to_date('16/01/2017 15:25.16','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (850,2,'User ID',str_to_date('11/01/2017 15:23.08','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (851,2,'Beneficiary name',str_to_date('28/09/2016 10:13.35','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (853,2,'Swift / BIC Code',str_to_date('28/09/2016 10:20.00','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (854,2,'IBAN',str_to_date('28/09/2016 10:22.20','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (855,2,'Account number',str_to_date('28/09/2016 10:23.06','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (856,2,'Bank name',str_to_date('28/09/2016 10:25.40','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (858,2,'Branch number',str_to_date('28/09/2016 10:26.37','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (859,2,'Branch address',str_to_date('28/09/2016 10:28.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (860,2,'Deposit amount',str_to_date('28/09/2016 10:28.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (861,2,'Product id',str_to_date('28/09/2016 10:30.33','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (862,2,'Market name',str_to_date('28/09/2016 10:31.31','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (863,2,'Time created',str_to_date('28/09/2016 10:32.02','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (864,2,'Ask/Bid',str_to_date('28/09/2016 10:32.35','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (865,2,'Start trade level',str_to_date('28/09/2016 10:33.15','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (866,2,'Update',str_to_date('28/09/2016 11:38.07','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (986,2,'from',str_to_date('20/10/2016 20:14.12','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (987,2,'to',str_to_date('20/10/2016 20:14.12','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (988,2,'transaction ID',str_to_date('20/10/2016 20:14.12','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (989,2,'time/date',str_to_date('20/10/2016 20:14.12','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (990,2,'description',str_to_date('20/10/2016 20:14.12','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (991,2,'credit',str_to_date('20/10/2016 20:14.12','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (992,2,'debit',str_to_date('20/10/2016 20:14.12','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (993,2,'previous',str_to_date('20/10/2016 20:14.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (994,2,'next',str_to_date('20/10/2016 20:14.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (995,2,'example',str_to_date('20/10/2016 20:14.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (996,2,'positive',str_to_date('20/10/2016 20:14.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (997,2,'negative',str_to_date('20/10/2016 20:14.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1081,2,'assets',str_to_date('04/11/2016 09:32.47','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1082,2,'type',str_to_date('04/11/2016 09:32.47','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1083,2,'appetite',str_to_date('04/11/2016 09:32.47','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1084,2,'All',str_to_date('04/11/2016 09:32.47','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1085,2,'primary',str_to_date('04/11/2016 09:32.47','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1086,2,'secondary',str_to_date('04/11/2016 09:32.47','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1087,2,'Months and up',str_to_date('04/11/2016 09:32.47','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1088,2,'Select the withdrawal you wish to reverse, and click <b>Sumbit</b>. The amount will be able to added to your balance immediately',str_to_date('04/11/2016 09:32.47','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1089,2,'Amount',str_to_date('04/11/2016 09:32.47','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1090,2,'Withdraw amount',str_to_date('04/11/2016 09:32.47','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1091,2,'select',str_to_date('04/11/2016 09:32.47','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1092,2,'glossary',str_to_date('04/11/2016 09:32.47','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1361,2,'Sold at',str_to_date('07/01/2017 15:12.38','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1366,2,'Not now',str_to_date('07/01/2017 15:24.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1368,2,'Status',str_to_date('07/01/2017 15:24.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1369,2,'Return amount',str_to_date('07/01/2017 15:24.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1370,2,'Pending',str_to_date('07/01/2017 15:24.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1371,2,'Open',str_to_date('07/01/2017 15:24.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1372,2,'Settled',str_to_date('07/01/2017 15:24.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1373,2,'Canceled',str_to_date('07/01/2017 15:24.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1374,2,'Canceled insufficient funds',str_to_date('07/01/2017 15:24.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1375,2,'{asset} closes at {marketPrice}',str_to_date('07/01/2017 15:24.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1376,2,'{asset} closes above {marketPrice}',str_to_date('07/01/2017 15:24.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1377,2,'{asset} closes below {marketPrice}',str_to_date('07/01/2017 15:24.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1378,2,'{asset} close above {marketPrice}',str_to_date('07/01/2017 15:24.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1379,2,'{asset} closes at {marketPrice}',str_to_date('07/01/2017 15:24.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1380,2,'{asset} closes at {marketPrice}',str_to_date('07/01/2017 15:24.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1381,2,'{asset} never touches {marketPrice}',str_to_date('07/01/2017 15:24.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1382,2,'{asset} closes at {marketPrice}',str_to_date('07/01/2017 15:24.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1383,2,'{asset} closes at {marketPrice}',str_to_date('07/01/2017 15:24.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1384,2,'{asset} closes above {marketPrice}',str_to_date('07/01/2017 15:24.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1385,2,'{asset} closes at {marketPrice}',str_to_date('07/01/2017 15:24.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1386,2,'{asset} closes at {marketPrice}',str_to_date('07/01/2017 15:24.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1387,2,'{asset} closes at {marketPrice}',str_to_date('07/01/2017 15:24.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1388,2,'{asset} closes at {marketPrice}',str_to_date('07/01/2017 15:24.57','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1389,2,'{asset} closes at {marketPrice}',str_to_date('07/01/2017 15:24.58','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1390,2,'{asset} closes at {marketPrice}',str_to_date('07/01/2017 15:24.58','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1391,2,'{asset} closes at {marketPrice}',str_to_date('07/01/2017 15:24.58','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1392,2,'{asset} closes at {marketPrice}',str_to_date('07/01/2017 15:24.58','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1393,2,'{asset} close above {marketPrice}',str_to_date('07/01/2017 15:24.58','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1394,2,'A Barrier Event shall be deemed to occur if at any time on any Exchange Business Day during the Barrier Observation Period the level of the Underlying''s price has been traded at or above the Barrier Level, as reasonably determined by the Calculation Agent',str_to_date('07/01/2017 15:25.44','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1395,2,'A Barrier Event shall be deemed to occur if at any time on any Exchange Business Day during the Barrier Observation Period the level of the Underlying''s price has been traded at or below the Barrier Level, as reasonably determined by the Calculation Agent',str_to_date('07/01/2017 15:25.44','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1396,2,'A Barrier Event shall be deemed to occur if at any time on any Exchange Business Day during the Barrier Observation Period the level of the Underlying''s price has been traded at or below the Barrier Level, as reasonably determined by the Calculation Agent',str_to_date('07/01/2017 15:25.44','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1397,2,'A Barrier Event shall be deemed to occur if at any time on any Exchange Business Day during the Barrier Observation Period the level of the Underlying''s price has been traded at or below the Barrier Level, as reasonably determined by the Calculation Agent',str_to_date('07/01/2017 15:25.44','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1398,2,'A Barrier Event shall be deemed to occur if at any time on any Exchange Business Day during the Barrier Observation Period the level of the Underlying''s price has been traded at or below the Barrier Level, as reasonably determined by the Calculation Agent',str_to_date('07/01/2017 15:25.44','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1399,2,'A Barrier Event shall be deemed to occur if at any time on any Exchange Business Day during the Barrier Observation Period the level of the Underlying''s price has been traded at or below the Barrier Level, as reasonably determined by the Calculation Agent',str_to_date('07/01/2017 15:25.44','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1455,2,'To invest, you need to select a product from the list of products in subscription or from the list of products on the secondary market. A default investment amount is offered, and you can change it if you prefer. Your investment will be immediately processed.',str_to_date('16/01/2017 15:25.21','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1456,2,'No, the issuer doesn’t know the final clients. You buy the product from us and we buy it for you from the issuer.',str_to_date('16/01/2017 15:25.29','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1457,2,'In case of credit event, anycapital will act as the Agent on your behalf in order to get your money back according to the legal settlement reach with the liquidation Agent.',str_to_date('16/01/2017 15:25.33','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1458,2,'A secondary market price is the price at which an investor can resell a structured product he has previously bought.',str_to_date('16/01/2017 15:27.33','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1459,2,'Yes, and in most of the case we will package the product including this insurance. Two mechanism are in place for such insurance: COSI and TCM. Both mechanism allow investors to minimize issuer risk by using collateral secured instruments backed with collateral in the form of securities or cash deposits.',str_to_date('16/01/2017 15:28.03','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1460,2,'Official close of the respective Underlying on the Initial Fixing Date on the Related Exchange, as determined by the Calculation Agent.',str_to_date('16/01/2017 15:28.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1461,2,'Official close of the respective Underlying on the Final Fixing Date on the Related Exchange, as determined by the Calculation Agent.',str_to_date('16/01/2017 15:28.18','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1462,2,'The price of the structured product at the issuance date.',str_to_date('16/01/2017 15:28.23','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1463,2,'Quanto is a feature that protect the investor from currency risk in the case that the underlyings currencies are different than his base currency.',str_to_date('16/01/2017 15:28.30','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1464,2,'A deposit can be made with a credit card (Visa and MasterCard), a bank transfer or several domestic payments. For full details please see our Banking page.',str_to_date('16/01/2017 15:28.37','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1465,2,'US Dollars, Euros, Pounds Sterling, Russian Ruble, Turkish Lira or Swedish Krone may be used in the account. You can select the currency while opening an account. Please note that you cannot change your currency selection after registration.',str_to_date('16/01/2017 15:28.43','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1467,2,'To withdraw money from your anycapital account, please login to the account, click on "My Account">"Withdraw" and follow the instructions. For full details see Banking.',str_to_date('16/01/2017 15:33.00','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1468,2,'No deposit is required and no credit/debit card details are required in order to register for the site.',str_to_date('16/01/2017 15:33.06','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1469,2,'Yes, simply go to "My Account" and look for the "Reverse Withdrawal" tab, select the withdrawal you wish to reverse, and click Submit. The amount will be added to your balance immediately.',str_to_date('16/01/2017 15:33.11','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1470,2,'anycapital makes every effort to enable all withdrawal methods. However, anycapital reserves the right to execute withdrawals by other means, such as credit/debit cards.',str_to_date('16/01/2017 15:33.16','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (900,2,'If the Final Fixing Level is at or below the Initial FixingLevel, the Investor will receive:',str_to_date('30/09/2016 12:45.22','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (901,2,'If the Final Fixing Level is above the Initial FixingLevel, the Investor will receive:',str_to_date('30/09/2016 12:45.22','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (902,2,'if a barrier event HAS not occur then if the Final Fixing Level is at or below the Initial Fixing Level, the Investor will receive:',str_to_date('30/09/2016 12:45.22','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (903,2,'if a barrier event HAS not occur then if the Final Fixing Level is above the Initial Fixing Level, the Investor will receive:',str_to_date('30/09/2016 12:45.37','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (904,2,'if a Barrier Event HAS occurred, the Investor will receive:',str_to_date('30/09/2016 12:45.37','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (905,2,'If the Final Fixing Level is at or below the Strike Level, the Investor will receive:',str_to_date('30/09/2016 12:45.37','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (906,2,'If the Final Fixing Level is above the Strike Level, the Investor will receive:',str_to_date('30/09/2016 12:45.37','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (907,2,'If a Barrier Event has NOT occurred, the Investor will receive a Cash Settlement in the Settlement Currency equal to:',str_to_date('30/09/2016 12:45.53','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (908,2,'If a Barrier Event HAS occurred and If the Final Fixing Level is at or below the Initial Fixing Level, the Investor will receive a cash settlement in the setlement currency equal to:',str_to_date('30/09/2016 12:45.53','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (909,2,'If a Barrier Event HAS occurred and If the Final Fixing Level is above the Initial Fixing Level, the Investor will receive a Cash Settlement in the Settlement Currency equal to:',str_to_date('30/09/2016 12:45.53','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (910,2,'If a Barrier Event has NOT occurred, the Investor will receive a Cash Settlement in the Settlement Currency equal to:',str_to_date('30/09/2016 12:45.53','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (911,2,'If a Barrier Event HAS occurred and If the Final Fixing Level is at or above the Initial Fixing Level, the Investor will receive a cash settlement in the setlement currency equal to:',str_to_date('30/09/2016 12:45.53','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (912,2,'If a Barrier Event HAS occurred and If the Final Fixing Level is below the Initial Fixing Level, the Investor will receive a Cash Settlement in the Settlement Currency equal to:',str_to_date('30/09/2016 12:45.53','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (913,2,'If a Barrier Event has NOT occurred, the Investor will receive a Cash Settlement in the Settlement Currency equal to:',str_to_date('30/09/2016 12:45.53','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (914,2,'If a Barrier Event HAS occurred and If the Final Fixing Level is at or below the Initial Fixing Level, the Investor will receive a cash settlement in the setlement currency equal to:',str_to_date('30/09/2016 12:45.53','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (915,2,'If a Barrier Event HAS occurred and If the Final Fixing Level is above the Initial Fixing Level, the Investor will receive a Cash Settlement in the Settlement Currency equal to:',str_to_date('30/09/2016 12:45.53','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (916,2,'If the Final Fixing Level is at or below the Initial Fixing Level, the Investor will receive a Cash Settlement in the Settlement Currency according to the following formula:',str_to_date('30/09/2016 12:45.53','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (917,2,'If the Final Fixing Level is above the Initial Fixing Level, the Investor will receive a Cash Settlement in the Settlement Currency according to the following formula:',str_to_date('30/09/2016 12:45.53','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (918,2,'If a Barrier Event has NOT occurred and if the Final Fixing Level is at or below the Initial Fixing Level multiplied with the Bonus Level (in %), the Investor will receive:',str_to_date('30/09/2016 12:45.53','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (919,2,'If a Barrier Event has NOT occurred and if the Final Fixing Level is above the Initial Fixing Level multiplied with the Bonus Level (in %), the Investor will receive:',str_to_date('30/09/2016 12:45.53','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (920,2,'If a Barrier Event HAS occurred, the Investor will receive:',str_to_date('30/09/2016 12:45.53','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (921,2,'If a Barrier Event has NOT occurred and if the Final Fixing Level is at or below the Initial Fixing Level multiplied with the Bonus Level (in %), the Investor will receive:',str_to_date('30/09/2016 12:45.53','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (922,2,'If a Barrier Event has NOT occurred and if the Final Fixing Level is above the Initial Fixing Level multiplied with the Bonus Level (in %), the Investor will receive:',str_to_date('30/09/2016 12:45.53','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (923,2,'If a Barrier Event HAS occurred and If the Final Fixing Level is at or below the Initial Fixing Level multiplied with the Bonus Level (in %), the Investor will receive:',str_to_date('30/09/2016 12:45.53','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (924,2,'If a Barrier Event HAS occurred and if the Final Fixing Level is above the Initial Fixing Level multiplied with the Bonus Level (in %), the Investor will receive:',str_to_date('30/09/2016 12:45.53','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (925,2,'in %',str_to_date('30/09/2016 12:48.36','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (979,2,'My Profile',str_to_date('22/01/2017 15:01.08','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1249,2,'unpublished',str_to_date('12/12/2016 16:32.21','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1250,2,'subscription',str_to_date('12/12/2016 16:32.21','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1251,2,'secondary',str_to_date('12/12/2016 16:32.21','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1252,2,'waiting for settle',str_to_date('12/12/2016 16:32.21','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1253,2,'settled',str_to_date('12/12/2016 16:32.21','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1254,2,'canceled',str_to_date('12/12/2016 16:32.21','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1350,2,'Campaigns',str_to_date('04/01/2017 13:48.54','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1351,2,'Writer user name',str_to_date('04/01/2017 13:50.12','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1352,2,'Add campaign',str_to_date('04/01/2017 13:55.46','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1353,2,'Source',str_to_date('04/01/2017 13:55.51','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1354,2,'Landing Page',str_to_date('04/01/2017 13:55.56','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1355,2,'Content',str_to_date('04/01/2017 13:56.02','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1356,2,'Domain',str_to_date('04/01/2017 13:56.07','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1400,2,'Trading',now(),null); 
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1401,2,'Product Type',now(),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1402,2,'Deposit and Withdraw',now(),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1403,2,'My Account',now(),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1404,2,'Technical',now(),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1405,2,'What is a Structured Product?',str_to_date('16/01/2017 10:16.09','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1406,2,'What is an underlying asset?',str_to_date('16/01/2017 10:16.18','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1407,2,'What is a Term Sheet?',str_to_date('16/01/2017 10:16.40','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1408,2,'What is a Barrier event?',str_to_date('16/01/2017 10:16.49','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1409,2,'What is a Capital Protected product?',str_to_date('16/01/2017 10:16.58','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1410,2,'Structured products are innovative, flexible investment instruments and an attractive alternative to direct financial investments such as shares, bonds, currencies and the like. They are sufficiently flexible to accommodate any risk profile, no matter how challenging the markets. Basically, structured products are bearer bonds with the issuer liable to the extent of all his assets. Thus, structured product quality is directly linked to the debtor’s, or issuer’s, creditworthiness. Bearer bonds (bonds and structured products) are subject to issuer risk. In case of issuer bankruptcy, traditional bonds as well as structured products form part of assets under bankruptcy law. To keep issuer risk to a minimum, investors should stick to top-quality issuers, and spread their investments among several issuers. Diversification is, of course, a wise strategy for all forms of investment including time deposits, bonds and structured products. Issuer creditworthiness over time should also be monitored.',str_to_date('16/01/2017 10:17.53','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1411,2,'A commodity, index, stock, currency pair, interest rates or any other financial asset that constitutes the basis for creating a structured product.',str_to_date('16/01/2017 10:20.44','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1412,2,'A term sheet is an agreement setting forth the basic terms and conditions under which an investment will be made. The term sheet include all the details of the investment such as all relevant dates, issue price, coupon to be paid, investment currency, underling(s) and all other mechanism and definition.
The Term sheet also describe the product in details and set the conditions to be met in order to receive the potential return.
',str_to_date('16/01/2017 10:20.51','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1415,2,'A capital guarantee product means that when an investor buys, or "enters", this specific structured product he is guaranteed to get back at maturity a part or the totality of the money he invested on day one.',str_to_date('16/01/2017 10:24.44','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1416,2,'FREQUENTLY ASKED QUESTIONS',str_to_date('16/01/2017 10:38.59','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1413,2,'A Barrier Event shall be deemed to occur if at any time on any Exchange Business Day during the Barrier Observation Period the level of the Underlying''s price has been traded at or below the Barrier Level, as reasonably determined by the Calculation Agent.',str_to_date('16/01/2017 11:08.02','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1420,2,'What is a yield enhancement product?',str_to_date('16/01/2017 14:46.23','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1421,2,'What is a participation product?',str_to_date('16/01/2017 14:46.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1422,2,'What is a credit risk?',str_to_date('16/01/2017 14:47.02','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1635,2,'Investment Amount / Price',str_to_date('25/01/2017 14:46.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1636,2,'Protected',str_to_date('25/01/2017 14:46.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1637,2,'Conditional',str_to_date('25/01/2017 14:46.34','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1638,2,'Unprotected',str_to_date('25/01/2017 14:46.35','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1639,2,'Barrier monitoring',str_to_date('25/01/2017 14:46.35','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1640,2,'Continuously',str_to_date('25/01/2017 14:46.35','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1641,2,'At final fixing date',str_to_date('25/01/2017 14:46.35','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1642,2,'Current price',str_to_date('25/01/2017 14:46.35','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1491,2,'Sales of Structured Products Tell Us a Lot About the Global Search for Yield',str_to_date('18/01/2017 08:09.10','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1492,2,'<div class="article-info"><div>by Tracy Alloway</div><time datetime="2015-11-30T12:27:00.000Z">November 30, 2015 — 7:27 AM EST</time></div><p>What can this year''s sales of structured products tell us about investors'' mindsets?</p><p>A lot, according to the flows and liquidity team at JPMorgan Chase.</p><p>Annual sales of retail structured products are expected to reach their highest level for seven years, according to analysts led by Nikolaos Panigirtzoglou. Global sales of retail structured products reached $466 billion in the first 10 months of the year, on track to reach $560 billion by the time the curtains are drawn on 2015, driven mostly by demand from Asia.</p>',str_to_date('18/01/2017 08:09.51','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1493,2,'Structured products have been with us for a long time – and that’s because they provide certainty',str_to_date('18/01/2017 08:11.01','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1494,2,'<p>For many investors, and even practitioners in the markets, structured products are often not considered to be a necessary part of a well-constructed portfolio.  They can be seen as a new breed of investment yet to stand the test of time. Nothing could be further from the truth.  The history of derivatives, of which structured products are a repackaged form for the retail and institution market, can be traced back to the Old Testament, with the reason they were relevant then the same reason why they are relevant today.</p>',str_to_date('18/01/2017 08:11.11','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1495,2,'Risk managing structured products in a falling market',str_to_date('18/01/2017 08:12.16','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1496,2,'Capital-at-risk products with European-style barriers inherently more vulnerable in a downturn',str_to_date('18/01/2017 08:12.24','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1497,2,'<p>Tim Mortimer is managing director of Future Value Consultants When other investments outperform structured products, it is tempting for investors – or regulators – to apply a curious hindsight when deciding how the products should be judged. Recent months have seen equity markets behave in a volatile fashion, with some significant losses posted during this period and several product types – notably autocalls with exposure to the Hang Seng China Enterprises Index (HSCEI) – becoming vulnerable...</p>',str_to_date('18/01/2017 08:12.32','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1498,2,'Investors benefit from low costs of structured products',now(),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1499,2,'<div class="article-info">Frankfurt am Main, Germany, <time datetime="2013-12-06">6 December 2013</time></div><p>Issuer margins on structured products are much lower than commonly assumed. This was one of the major findings of a study entitled ‘Issuer margins for structured products in Germany’ written by the European Derivatives Group (EDG) on behalf of the Deutscher Derivate Verband (DDV), the German Derivatives Association. The EDG collected a representative sample of 1,650 structured products from nine product categories, taking into account their market volumes as of 31 May 2013.</p>',now(),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1500,2,'Structured products under the microscope',str_to_date('18/01/2017 08:19.52','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1501,2,'<div class="article-info">Last updated = <time datetime="2016-02-17">17 Feb 2016</time><br>First published = <time datetime="2016-02-02">02 Feb 2016</time><br>Feature by Tom Wilson</div><p>Investors have bagged 7% returns from this type of investment. So why are there so many critics? We take a closer look at the sector.</p><p>Where can you go to get a decent return? If you lock your money in a top savings account for a year, you’ll get 2% if you’re lucky. With these paltry rates on offer, it’s easy to understand why savers and investors would want to look elsewhere to see what’s available.</p>',str_to_date('18/01/2017 08:19.59','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1502,2,'Investment guide: structured products',now(),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1503,2,'<div class="article-info">Last updated = <time datetime="2015-06-03">03 Jun 2015</time><br>First published = <time datetime="2015-06-03">03 Jun 2015</time><br>Feature by Laura Whitcombe</div><p>Structured products can produce healthy returns but they''re not for the faint-hearted.</p><p>It''s not difficult to understand why some rate-starved savers are attracted to structured products. They have long been marketed as providing a middle ground between the potential for investment growth and capital protection.</p><p>However, while it''s true that some structured products have handsomely rewarded investors (note the use of the word ''investors'' here and not ''savers''), these are complicated financial products and should not be entered into lightly.</p>',str_to_date('18/01/2017 08:21.45','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1505,2,'Savers are being tempted by complex ''structured products''',str_to_date('18/01/2017 08:21.59','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1506,2,'<div class="article-info">By Harvey Jones<br>Published = <time datetime="2016-04-20T15:01:00.000Z">15:01, Wed, Apr 20, 2016</time><br>Updated = <time datetime="2016-04-21T14:35:00.000Z">14:35, Thu, Apr 21, 2016</time></div><p>Often seen as a halfway house between cash and shares, they aim to deliver a better return than savings accounts while limiting the risks of investing directly in stocks.</p><p>New figures suggest that lower-risk structured deposits have been fairly successful, producing an average return of 3.7 per cent a year.</p><p>But structured products remain controversial as they can be difficult to understand.</p>',str_to_date('18/01/2017 08:22.09','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1507,2,'Beat stormy markets with structured products',str_to_date('18/01/2017 08:22.33','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1508,2,'With interest rate rises on the horizon and ongoing market uncertainty investors face an uphill battle to find investment opportunities that can provide income or growth. However, structured products can offer better returns than investing directly in the market.',str_to_date('18/01/2017 08:22.42','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1509,2,'<p>These are investments backed by a counterparty where the returns are defined by reference to an underlying measurement, such as the FTSE 100, and delivered at a defined date. They aim to provide some protection against market downside and achieve a defined outcome.</p>',str_to_date('18/01/2017 08:23.35','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1510,2,'It’s time to welcome back structured products',str_to_date('18/01/2017 08:23.44','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1511,2,'Not all structured products are evil – here are four to consider.',str_to_date('18/01/2017 08:23.52','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1512,2,'<div class="article-info">By = David C Stevenson<br><time datetime="2015-09-29">29/09/2015</time></div><p>“Structured products.” I suspect I just have to say these two words and more than a few of my colleagues at MoneyWeek will be reaching for their crucifix and garlic. Critics of structured products are legion. There are two main charges against these products = firstly, that they are opaque and designed to ensure a bad deal for the investor.</p>',str_to_date('18/01/2017 08:23.58','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1513,2,'Voices: When Structured Notes Make Sense',now(),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1514,2,'These investment vehicles let people participate in a sector’s upside, while limiting downside exposure, this adviser says',str_to_date('18/01/2017 08:24.12','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1515,2,'<div class="article-info"><time datetime="2016-06-10T20:21:00.000Z">June 10, 2016 3:21 p.m. ET</time></div><p>Steve Skancke is chief investment strategist at Keel Point LLC in Vienna, Va. Voices is an occasional feature of edited excerpts in which wealth managers address issues of interest to the advisory community.</p><p>Given the current market and the fact that 10-year Treasury rates still linger below 2%, it’s challenging for advisers to provide clients with some degree of volatility protection without major sacrifice in returns. One option...</p>',str_to_date('18/01/2017 08:24.20','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1516,2,'An Introduction To Structured Products',str_to_date('18/01/2017 08:24.31','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1517,2,'<div class="article-info">Katrina Lamb from Investopedia<br><time datetime="2011-04-12">April 12, 2011</time></div><p>Once upon a time, the retail investment world was a quiet, rather pleasant place where a small, distinguished cadre of trustees and asset managers devised prudent portfolios for their well-heeled clients within a narrowly defined range of high-quality debt and equity instruments. Financial innovation and the rise of the investor class changed all that.</p><p>One innovation that has gained traction as an addition to retail and institutional portfolios is the class of investments broadly known as structured products. This article provides an introduction to structured products with a particular focus on their applicability in diversified retail portfolios.</p>',str_to_date('18/01/2017 08:24.42','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1520,2,'Open glossary',str_to_date('24/01/2017 17:34.37','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1521,2,'Home',str_to_date('24/01/2017 17:34.37','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1522,2,'Close',str_to_date('24/01/2017 17:34.37','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1523,2,'Available',str_to_date('24/01/2017 17:34.37','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1524,2,'No protection on this product',str_to_date('24/01/2017 17:34.37','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1525,2,'Return',str_to_date('24/01/2017 17:34.38','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1526,2,'Last level',str_to_date('24/01/2017 17:34.38','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1527,2,'in',str_to_date('24/01/2017 17:34.38','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1528,2,'Simulation for {amount} investment',str_to_date('24/01/2017 17:34.38','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1529,2,'Effective',str_to_date('24/01/2017 17:34.38','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1530,2,'Interactive calculator',str_to_date('24/01/2017 17:34.38','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1531,2,'The attraction',str_to_date('24/01/2017 17:34.38','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1532,2,'Worst case',str_to_date('24/01/2017 17:34.38','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1533,2,'Get back {protection-level} of your investment and in addition participate at {participation-level} of {asset} price increase.',str_to_date('24/01/2017 17:34.53','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1534,2,'Get back {protection-level} of your investment and in addition participate at {participation-level} of {asset} price increase as long as {asset} never traded at or above {barrier-level}.',str_to_date('24/01/2017 17:34.53','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1535,2,'Get back {protection-level} of your investment and in addition participate at {participation-level} of {asset} price increase up to {barrier-level}.',str_to_date('24/01/2017 17:34.53','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1536,2,'Get back {protection-level} of your investment and if {asset} rises by more than {coupon-trigger-level} you get a {coupon-level} fixed return.',str_to_date('24/01/2017 17:34.53','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1537,2,'You get a fixed {coupon-level} annual return PLUS your investment as long as {asset} closes at or above the strike level.',str_to_date('24/01/2017 17:34.53','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1538,2,'You get a fixed {coupon-level} annual return PLUS your investmentas as long as {asset} never traded at or below {100% - barrier-level}',str_to_date('24/01/2017 17:34.54','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1539,2,'You get a fixed {coupon-level} annual return PLUS your investment even if {asset} declines by up to {100% - barrier-level}',str_to_date('24/01/2017 17:34.54','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1540,2,'You get a fixed {coupon-level} annual return PLUS your investmentas as long as {asset} never traded at or above the Barrier Level.',str_to_date('24/01/2017 17:34.54','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1541,2,'You get a fixed {coupon-level} annual return PLUS your investment as long as {asset} closes below the Barrier Level.',str_to_date('24/01/2017 17:34.54','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1542,2,'As long as {asset} closes above the initial fixing level you get a fixed {coupon-level} annual return PLUS your investment. In addition, your investment is protected as long as {asset} never traded at or below {100% - barrier-level}',str_to_date('24/01/2017 17:34.54','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1543,2,'As long as {asset} closes above the initial fixing level you get a fixed {coupon-level} annual return PLUS your investment. In addition, your investment is protected even if {asset} declines by up to {100% - barrier-level}',str_to_date('24/01/2017 17:34.54','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1544,2,'You participate at {participation-level} of any {asset} price increase.',str_to_date('24/01/2017 17:34.54','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1545,2,'You get {participation-level} of {asset} price increase (max return: {caplevel - 100%})',str_to_date('24/01/2017 17:34.54','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1546,2,'As long as {asset} never traded at or below {100% - barrier-level} you get the higher of {bonus-level%} or the actual price increase plus your investment.',str_to_date('24/01/2017 17:34.54','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1547,2,'As long as {asset} doesn''t decline by more than {100% - barrier-level} you get the higher of {bonus-level%} or the actual price increase plus your investment.',str_to_date('24/01/2017 17:34.54','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1550,2,'Get back {protection-level} of your investment and in addition participate at {participation-level} of {asset} price increase above {initial-fixing-level}.',str_to_date('24/01/2017 17:35.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1551,2,'Get back {protection-level} of your investment and in addition participate at {participation-level} of {asset} price increase as long as {asset} never traded at or above {market-barrier-level}.',str_to_date('24/01/2017 17:35.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1552,2,'Get back {protection-level} of your investment and in addition participate at {participation-level} of {asset} price increase up to {market-barrier-level}.',str_to_date('24/01/2017 17:35.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1553,2,'Get back {protection-level} of your investment and if {asset} rises above {coupon-trigger-level%*initial-fixing-level} you get a {coupon-level} fixed return.',str_to_date('24/01/2017 17:35.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1554,2,'You get a fixed {coupon-level} annual return PLUS your investment as long as {asset} closes at or above {strike-level}.',str_to_date('24/01/2017 17:35.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1555,2,'You get a fixed {coupon-level} annual return PLUS your investment as long as {asset} never traded at or below {market-barrier-level}.',str_to_date('24/01/2017 17:35.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1556,2,'You get a fixed {coupon-level} annual return PLUS your investment even if {asset} declines up to {market-barrier-level}.',str_to_date('24/01/2017 17:35.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1557,2,'You get a fixed {coupon-level} annual return PLUS your investment as long as {asset} never traded at or above {market-barrier-level}.',str_to_date('24/01/2017 17:35.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1558,2,'You get a fixed {coupon-level} annual return PLUS your investment as long as {asset} closes below {market-barrier-level}.',str_to_date('24/01/2017 17:35.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1559,2,'As long as {asset} closes above {initial-fixing-level} you get a fixed {coupon-level} annual return PLUS your investment. In addition, your investment is protected even if {asset} declines up to {market-barrier-level}.',str_to_date('24/01/2017 17:35.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1560,2,'As long as {asset} closes above {initial-fixing-level} you get a fixed {coupon-level} annual return PLUS your investment. In addition, your investment is protected even if {asset} declines up to {market-barrier-level}.',str_to_date('24/01/2017 17:35.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1561,2,'You participate at {participation-level} of any {asset} price increase above {initial-fixing-level}.',str_to_date('24/01/2017 17:35.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1562,2,'You get {participation-level} of {asset} price increase above {initial-fixing-level} (max return: {caplevel - 100%})',str_to_date('24/01/2017 17:35.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1563,2,'As long as {asset} never traded at or below {market-barrier-level} you get the higher of {bonus-level%} or the actual price increase above {initial-fixing-level} plus your investment.',str_to_date('24/01/2017 17:35.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1564,2,'As long as {asset} doesn''t decline below {market-barrier-level} you get the higher of {bonus-level%} or the actual price increase above {initial-fixing-level} plus your investment.',str_to_date('24/01/2017 17:35.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1567,2,'Get back {protection-level} of your investment and in addition participate at {participation-level} of {asset} price increase.',str_to_date('24/01/2017 17:35.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1568,2,'You lose {100%-protection-level} of your investment.',str_to_date('24/01/2017 17:35.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1569,2,'You lose {100%-protection-level} of your investment.',str_to_date('24/01/2017 17:35.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1570,2,'You lose {100%-protection-level} of your investment.',str_to_date('24/01/2017 17:35.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1571,2,'{asset} price decline below the strike level is deducted from your investment amount but you still get the {coupon-level} annual return.',str_to_date('24/01/2017 17:35.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1572,2,'If {asset} traded at or below {100% - barrier-level} the negative performance is deducted from your investment, but you still get the {coupon-level} annual return.',str_to_date('24/01/2017 17:35.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1573,2,'If {asset} declines by more than {100% - barrier-level} the negative performance is deducted from your investment, but you still get the {coupon-level} annual return.',str_to_date('24/01/2017 17:35.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1574,2,'If {asset} traded at or above {barrier-level - 100%} the positive performance is deducted from your investment, but you still get the {coupon-level} annual return.',str_to_date('24/01/2017 17:35.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1575,2,'If {asset} rises by more than {barrier-level - 100%} the positive performance is deducted from your investment, but you still get the {coupon-level} annual return.',str_to_date('24/01/2017 17:35.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1576,2,'If {asset} traded at or below {100% - barrier-level} the negative performance is deducted from your investment.',str_to_date('24/01/2017 17:35.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1577,2,'If {asset} declines by more than {100% - barrier-level} the negative performance is deducted from your investment.',str_to_date('24/01/2017 17:35.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1578,2,'Any negative performance is deducted from your investment.',str_to_date('24/01/2017 17:35.13','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1579,2,'Any negative performance is deducted from your investment.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1580,2,'If {asset} traded at or below {100% - barrier-level} the negative performance is deducted from your investment.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1581,2,'If {asset} declines by more than {100% - barrier-level} the negative performance is deducted from your investment.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1584,2,'Get back {protection-level} of your investment and in addition participate at {participation-level} of {asset} price increase above {initial-fixing-level}.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1585,2,'You lose {100%-protection-level} of your investment.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1586,2,'You lose {100%-protection-level} of your investment.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1587,2,'You lose {100%-protection-level} of your investment.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1588,2,'{asset} price decline below {strike-level} is deducted from your investment amount but you still get the {coupon-level} annual return.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1589,2,'If {asset} traded at or below {market-barrier-level} the negative performance is deducted from your investment, but you still get the {coupon-level} annual return.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1590,2,'If {asset} declines below {market-barrier-level} the negative performance is deducted from your investment, but you still get the {coupon-level} annual return.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1591,2,'If {asset} traded at or above {market-barrier-level} the positive performance is deducted from your investment, but you still get the {coupon-level} annual return.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1592,2,'If {asset} rises above {market-barrier-level} the positive performance is deducted from your investment, but you still get the {coupon-level} annual return.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1593,2,'If {asset} traded at or below {market-barrier-level} the negative performance is deducted from your investment.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1594,2,'If {asset} declines below {market-barrier-level} the negative performance is deducted from your investment.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1595,2,'Any negative performance below {initial-fixing-level} is deducted from your investment.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1596,2,'Any negative performance below {initial-fixing-level} is deducted from your investment.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1597,2,'If {asset} traded at or below {market-barrier-level} the negative performance is deducted from your investment.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1598,2,'If {asset} declines below {market-barrier-level} the negative performance is deducted from your investment.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1601,2,'At maturity, you always get back {protection-level} of your denomination amount regardless of {asset}''s performance. Furthermore, in the event that {asset} closes above the initial fixing level, you will receive {participation-level} of that price increase.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1602,2,'At maturity, you always get back {protection-level} of your denomination amount regardless of {asset}''s performance. Furthermore, in the event that {asset} never trades at or above {barrier-level} of the initial fixing level until {final-fixing-date} you will receive {participation-level} of any {asset} positive performance.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1603,2,'At maturity, you always get back {protection-level} of your denomination amount regardless of {asset}''s performance. Furthermore, in the event that {asset} closes below {barrier-level} of the initial fixing level, you will receive {participation-level} of any {asset} positive performance.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1604,2,'At maturity, you always get back {protection-level} of your denomination amount regardless of {asset}''s performance. Furthermore, in the event that {asset} closes above {coupon-trigger-level%} of the product''s initial fixing level, you will receive a fixed {coupon-level} payout.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1605,2,'At maturity, you get an annual {coupon-amopunt} on your denomination amount regardless of {asset}''s performance. Furthermore, in the event that {asset} closes above the product''s opening strike level, you will get back your denomination amount. Otherwise, you get your denomination amount reduced by {asset}''s negative performance below the strike level.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1606,2,'At Maturity, you get an annual {coupon-level} on your denomination amount regardless of {asset}''s performance. Furthermore, in the event that {asset} ever trades below {barrier-level} of the initial fixing level and closes at a decrease, you get your denomination amount reduced by {asset}''s negative performance. Otherwise, you will receive your entire denomination amount.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1607,2,'At maturity, you get an annual {coupon-level} on your denomination amount regardless of {asset}''s performance. Furthermore, in the event that {asset} closes below {barrier-level} of the initial fixing level, you get your denomination amount reduced by {asset}''s negative performance. Otherwise, you get back your entire denomination amount.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1608,2,'At Maturity, you get an annual {coupon-level} on your denomination amount regardless of {asset}''s performance. Furthermore, in the event that {asset} ever trades above {barrier-level} of the initial fixing level and closes at an increase, you get your denomination amount reduced by {asset}''s positive performance. Otherwise, you will receive your entire denomination amount.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1609,2,'At maturity, you get an annual {coupon-level} on your denomination amount regardless of {asset}''s performance. Furthermore, in the event that {asset} closes above {barrier-level} of the initial fixing level, you get your denomination amount reduced by {asset}''s positiive performance. Otherwise, you get back your entire denomination amount.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1610,2,'In the event that {asset} closes above the initial fixing level you get an annual {coupon-level} on your denomination amount. In the event that {asset} ever trades at or below {barrier-level} of the initial fixing level and ends up closing at a decrease, you receive only the relative amount of your denomination taking into account the price decrease. Otherwise, you will receive your entire denomination amount.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1611,2,'In the event that {asset} closes above the initial fixing level you get an annual {coupon-level} on your denomination amount. In the event that {asset} closes at or below {barrier-level} of the initial fixing level, you receive only the relative amount of your denomination taking into account the price decrease. Otherwise, you will receive your entire denomination amount.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1612,2,'At maturity, if the Final Fixing Level closes above the Initial Fixing Level, you receive {participation-level} of {asset}''s positive performance. Otherwise, you will receive the denomination reduced by an amount proportional to the negative performance of the underlying below the Initial Fixing level.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1613,2,'At maturity, if the Final Fixing Level closes above the Initial Fixing Level, you receive the minimum between {caplevel - 100%} and {participation-level} of {asset}''s positive performance. Otherwise, you will receive the denomination reduced by an amount proportional to the negative performance of the underlying below the Initial Fixing level.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1614,2,'In the event that {asset} never trades at or below {barrier-level%} of the initial fixing level and closes below {bonus-level%} of the initital fixing level, you get {bonus-level} on your denomination amount. Otherwise, you get your denomination times {asset}''s performance.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1615,2,'In the event that {asset} closes above {barrier-level} of the initial fixing level and below {bonus-level%} of the initital fixing level, you get {bonus-level%} on your denomination amount. Otherwise, you get your denomination times {asset}''s performance.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1618,2,'At maturity, you always get back {protection-level} of your denomination amount regardless of {asset}''s performance. Furthermore, in the event that {asset} closes above {initial-fixing-level}, you will receive {participation-level} of that price increase.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1619,2,'At maturity, you always get back {protection-level} of your denomination amount regardless of {asset}''s performance. Furthermore, in the event that {asset} never trades at or above {barrier-level} until {final-fixing-date} you will receive {participation-level} of any {asset} positive performance.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1620,2,'At maturity, you always get back {protection-level} of your denomination amount regardless of {asset}''s performance. Furthermore, in the event that {asset} closes below {barrier-level} , you will receive {participation-level} of any {asset} positive performance.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1621,2,'At maturity, you always get back {protection-level} of your denomination amount regardless of {asset}''s performance. Furthermore, in the event that {asset} closes above {coupon-trigger-level}, you will receive a fixed {coupon-level} payout.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1622,2,'At maturity, you get an annual {coupon-amount} on your denomination amount regardless of {asset}''s performance. Furthermore, in the event that {asset} closes above {strike-level}, you will get back your denomination amount. Otherwise, you get your denomination amount reduced by {asset}''s negative performance below the strike level.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1623,2,'At Maturity, you get an annual {coupon-level} on your denomination amount regardless of {asset}''s performance. Furthermore, in the event that {asset} ever trades below {barrier-level} and closes at a decrease, you get your denomination amount reduced by {asset}''s negative performance. Otherwise, you will receive your entire denomination amount.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1624,2,'At maturity, you get an annual {coupon-level} on your denomination amount regardless of {asset}''s performance. Furthermore, in the event that {asset} closes below {barrier-level}, you get your denomination amount reduced by {asset}''s negative performance. Otherwise, you get back your entire denomination amount.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1625,2,'At Maturity, you get an annual {coupon-level} on your denomination amount regardless of {asset}''s performance. Furthermore, in the event that {asset} ever trades above {barrier-level} and closes at an increase, you get your denomination amount reduced by {asset}''s positive performance. Otherwise, you will receive your entire denomination amount.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1626,2,'At maturity, you get an annual {coupon-level} on your denomination amount regardless of {asset}''s performance. Furthermore, in the event that {asset} closes above {barrier-level}, you get your denomination amount reduced by {asset}''s positive performance. Otherwise, you get back your entire denomination amount.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1627,2,'In the event that {asset} closes above the initial fixing level you get an annual {coupon-level} on your denomination amount. In the event that {asset} ever trades at or below {barrier-level} and ends up closing at a decrease, you receive only the relative amount of your denomination taking into account the price decrease. Otherwise, you will receive your entire denomination amount.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1628,2,'In the event that {asset} closes above the initial fixing level you get an annual {coupon-level} on your denomination amount. In the event that {asset} closes at or below {barrier-level}, you receive only the relative amount of your denomination taking into account the price decrease. Otherwise, you will receive your entire denomination amount.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1629,2,'At maturity, if the Final Fixing Level closes above {initial-fixing-level}, you receive {participation-level} of {asset}''s positive performance. Otherwise, you will receive the denomination reduced by an amount proportional to the negative performance of the underlying below {initial-fixing-level}.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1630,2,'At maturity, if the Final Fixing Level closes above {initial-fixing-level}, you receive the minimum between {caplevel - 100%} and {participation-level} of {asset}''s positive performance. Otherwise, you will receive the denomination reduced by an amount proportional to the negative performance of the underlying below {initial-fixing-level}.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1631,2,'In the event that {asset} never trades at or below {barrier-level} of the initial fixing level and closes below {bonus-level%}, you get {bonus-level%} on your denomination amount. Otherwise, you get your denomination times {asset}''s performance.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE,TIME_UPDATED,WRITER_ID) values (1632,2,'In the event that {asset} closes above {barrier-level} and below {bonus-level}, you get {bonus-level%} on your denomination amount. Otherwise, you get your denomination times {asset}''s performance.',str_to_date('24/01/2017 17:35.14','%d/%m/%Y %H:%i.%s'),null);

-- INSERTING into MSG_RES_LANGUAGE
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (43,2,NOW(),null,'123');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (569,2,NOW(),null,'very large value!!');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (648,2,NOW(),null,'very large value!!');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (699,2,NOW(),null,'This product falls within the category “Capital Protection”. Depending on whether the Product is capped or not, the profit an Investor could realize
 with this Product at redemption is limited (with cap) or unlimited (without cap). Any profit is composed of the invested capital (excluding any transaction
 or other costs) multiplied by the Capital Protection plus any additional (guaranteed and/or conditional) payments such as coupon or participation
 payments, bonuses or others.
 On the downside the Investor’s exposure to the Underlying(s) is floored at the Capital Protection level.
 Please refer to the sections “Product Description” and “Redemption” for more detailed information on the characteristics of this Product.');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (700,2,NOW(),null,'This Product falls within the category „Yield Enhancement“ which means that there is an upper limit to the profit an Investor
 can realize with this Product. At redemption the Investor could receive a maximum amount corresponding to the invested
 capital (excluding any transaction or other costs) plus any additional (guaranteed and/or conditional) payments such as
 coupon or participation payments, bonuses or others.
 On the downside, especially if the Product has forfeited any contingent capital protection (like e.g. a barrier, strike), the
 Investor is exposed to the negative development of the Underlying(s). This might (even if a stop loss event has occurred)
 lead to a partial or even a total loss of the investment.
 Please refer to the sections “Product Description” and “Redemption” for more detailed information on the characteristics
 of this Product.');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (701,2,NOW(),null,'This product falls within the category “Participation Products”. The profit the Investor could realize with this Product at redemption is unlimited (except
 for bearish products and products with the special feature “capped participation”). The redemption amount is directly linked to the performance of
 the Underlying(s), taking into account any participation rates or other features.
 On the downside, especially if the product has forfeited any contingent capital protection (like e.g. a barrier, strike), the Investor is exposed to the
 negative development of the Underlying(s). This might (even if a stop loss event has occurred) lead to a partial or even a total loss of the investment.
 Please refer to the sections “Product Description” and “Redemption” for more detailed information on the characteristics of this Product.');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (702,2,NOW(),null,'<li>Rising underlying</li><li>Rising volatility</li><li>Sharply falling underlying possible</li>');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (703,2,NOW(),null,'<li>Rising underlying</li><li>Sharply falling underlying possible</li><li>Underlying is not going to touch or go above the barrier during product lifetime</li>');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (704,2,NOW(),null,'<li>Rising underlying</li><li>Sharply falling underlying possible</li>');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (705,2,NOW(),null,'<li>Underlying moving sideways or slightly rising</li><li>Falling volatility</li>');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (706,2,NOW(),null,'<li>Underlying moving sideways or slightly rising</li><li>Falling volatility</li><li>Underlying will not breach Barrier during product lifetime</li>');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (707,2,NOW(),null,'<li>Underlying moving sideways or slightly decreasing</li><li>Falling volatility</li><li>Underlying will not breach Barrier during product lifetime</li>');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (708,2,NOW(),null,'<li>Underlying moving sideways or slightly rising</li><li>Decreasing volatility</li><li>Underlying will not breach Barrier during product lifetime</li>');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (709,2,NOW(),null,'<li>Rising underlying</li><li>Rising volatility</li>');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (710,2,NOW(),null,'<li>Underlying moving sideways or rising</li><li>Underlying will not breach Barrier during product lifetime</li>');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (711,2,NOW(),null,'<li>Rising underlying</li><li>Underlying will not breach Barrier during product lifetime</li>');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (747,2,NOW(),null,'<p>Investors should read the section “<a href="{siginificant-risks-link}">Significant Risks</a>” below as well as the section “<a href="{risk-factors-link}">Risk Factors</a>” of the relevant Program.</p><p>In addition, investors are subject to the credit risk of the Depositor and Guarantor, if any.</p><p>The conditions in this indicative simplified prospectus are indicative and may be adjusted. The definitive simplified prospectus will be made available on the Issue Date.</p>');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (749,2,NOW(),null,'Only the Final Termsheet together with the Derivative Programme of the relevant Depositor valid as per the Initial Fixing Date containing all further relevant terms and conditions, as such is amended from time to time (the "Programme"), shall form the entire and legally binding documentation for this Product ("Product Documentation"), and accordingly the Final Termsheet should always be read together with the Programme. Definitions used in the Final Termsheet, but not defined therein, shall have the meaning given to them in the Programme. Even though translation into other languages might be available, it is only the Final Termsheet and Derivative Programme in English which are legally binding. Notices to Investors in connection with this Product shall be validly given in accordance with the terms and conditions of the Programme. In addition, any changes with regard to the terms and conditions of this Product will be published on the relevant Termsheet on <a href="//www.any.capital">www.any.capital</a> under the section “Products”. Notices to Investors relating to the Issuing Parties will be published under the section “About us” on <a href="//www.any.capital">www.any.capital</a> and/or on the web page of the respective Issuing Party.');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (750,2,NOW(),null,'This Product entitles the Investor on the Redemption Date to a Cash Settlement in the Settlement Currency which equals the Capital Protection multiplied by the Denomination. In addition, the Investor can participate in the appreciation of the Underlying (unlimited), as described under "Redemption".');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (751,2,NOW(),null,'The Investor is entitled to receive from the Issuer on the Redemption Date a Cash Settlement in the Settlement Currency which equals the Capital Protection multiplied by the Denomination. In addition, the Investor can participate in the appreciation of the Underlying (limited), as described under "Redemption".');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (752,2,NOW(),null,'This Product offers the Investor the opportunity to receive a conditional coupon Amount. In addition, the Investor will receive on the Redemption Date a Cash Settlement equal to the Denomination multiplied by the Capital Protection, as further described under "Redemption".');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (753,2,NOW(),null,'This Product offers the Investor a Coupon Rate regardless of the performance of the Underlying during the lifetime. If at the Final Fixing Date the Underlying closes above the Strike Level, the Investor will receive the Denomination on the Redemption Date. Otherwise the redemption of the Product will depend on the value of the Underlying, as described in section “Redemption”.');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (754,2,NOW(),null,'This Product offers the Investor a Coupon Rate regardless of the performance of the Underlying combined with conditional downside protection. If the Barrier Event has not occurred, the Investor will receive the Denomination on the Redemption Date. If a Barrier Event has occurred, the redemption of the Product will depend on the value of the Underlying, as described in section “Redemption”.');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (755,2,NOW(),null,'This Product offers the Investor a Coupon Rate regardless of the performance of the Underlying combined with conditional downside protection. If the Barrier Event has not occurred, the Investor will receive the Denomination on the Redemption Date. If a Barrier Event has occurred, the redemption of the Product will depend on the value of the Underlying, as described in section “Redemption”.');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (756,2,NOW(),null,'This Product offers the Investor the opportunity to receive a Conditional Coupon Amount. If the Barrier Event has not occurred, the Investor will receive the Denomination on the Redemption Date. If a Barrier Event has occurred, the redemption of the Product will depend on the value of the Underlying, as described in section “Redemption”.');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (757,2,NOW(),null,'An Outperformance Certificate offers the Investor a disproportionate participation in a positive performance of the Underlying, as described in the Redemption Section below.');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (758,2,NOW(),null,'This Product offers the Investor at the Redemption Date a Cash Settlement in the Settlement Currency equal to the Denomination multiplied by the Bonus Level (in %), unless a Barrier Event has occurred. In addition, the Investor has - independently of a Barrier Event having occurred - the opportunity to participate in the performance of the Underlying above the Bonus Level (in %). If a Barrier Event has occurred, the redemption of the Product will depend on the value of the Underlying, as described in section "Redemption".');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (759,2,NOW(),null,'This Product offers the Investor at the Redemption Date a Cash Settlement in the Settlement Currency equal to the Denomination multiplied by the Bonus Level (in %), unless a Barrier Event has occurred. In addition, the Investor has - independently of a Barrier Event having occurred - the opportunity to participate in the performance of the Underlying above the Bonus Level (in %), leveraged by the Participation. If a Barrier Event has occurred, the redemption of the Product will depend on the value of the Underlying, as described in section "Redemption".');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (760,2,NOW(),null,'You are protected at {protectedlevel} from {asset} decline below {lastprice} and get {participation/100} times any price increase less {100%-protection level}');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (761,2,NOW(),null,'If {asset} doesn''t touch {barrierlevelXlastprice} until {settlementdate} you get {participation/100} times any price increase less {100%-protection level}. You are protected at {protectedlevel} from any decline below {lastprice}*');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (762,2,NOW(),null,'As long as {asset} is above {lastpriceXcoupontriggerlevel/100} on {settlementdate} you get fixed {couponsize} profit. You are protected at {protectedlevel} from any decline below {lastprice}*');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (763,2,NOW(),null,'You get an annual {couponsize} on your original investment regardless of the asset performance PLUS your original investment as long as on {final-fixing-date} {asset} is above {strikelevelXlastprice}*');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (764,2,NOW(),null,'You get an annual {couponsize} on your original investment regardless of the asset performance PLUS your original investment as long as {asset} doesn''t touch {barrierlevelXlastprice}* by  {final-fixing-date}');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (765,2,NOW(),null,'You get an annual {couponsize} on your original investment regardless of the asset performance PLUS your original investment as long as {asset} doesn''t touch {barrierlevelXlastprice}* by  {final-fixing-date}');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (766,2,NOW(),null,'You get an annual {couponsize} on your original investment as long as on  {final-fixing-date} {asset} is above {strikelevelXlastprice}*.  In addition, you receive back your original investment if  {asset} didn''t touch {barrierlevelXlastprice}*');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (767,2,NOW(),null,'You get {participation/100} times of any price increase of {asset} {upToCapLevel} and 1:1 downside');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (768,2,NOW(),null,'You get 100% of any price increase of {asset} if it closes above {bonuslevelXlastprice} , otherwise if {asset} doesn''t touch {barrierlevelXlastprice}* by {settlementdate}, you receive {bonuslevel} of your original investment');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (769,2,NOW(),null,'You get {participation/100} times of any price increase of {asset} as long as it closes above {bonuslevelXlastprice} , otherwise as long as {asset} doesn''t touch {barrierlevelXlastprice}* by {settlementdate}, you receive {bonuslevel} of your original investment');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (772,2,NOW(),null,'<li><h2 id="riskFactors">Risk fact relating to the product</h2>The risk of loss related to this Product is limited to the difference between the purchase price (if higher than the Capital Protection) and the Capital Protection. However, during the life of the Product, its price can fall below the protection level.</li><li><h2>Additional risk factors</h2>Prospective Investors should ensure that they fully understand the nature of this Product and the extent of their exposure to risks and they should consider the suitability of this Product as an investment in the light of their own circumstances and financial condition. Products involve a high degree of risk, including the potential risk of expiring worthless. Potential Investors should be prepared in certain circumstances to sustain a total loss of the capital invested to purchase this Product. Prospective Investors shall consider the following important risk factors and see the section ""Risk Factors"" of the Programme for details on all other risk factors to be considered. This is a structured product involving derivative components. Investors should make sure that their advisors have verified that this Product is suitable for the portfolio of the investor taking into account the investor''s financial situation, investment experience and investment objectives. The terms and conditions of the Product may be subject to adjustments during the lifetime of the Product as set out in the Programme. Investors whose usual currency is not the currency in which the Product is redeemed should be aware of their possible currency risk. The value of the Product may not correlate with the value of the Underlying(s).</li><li><h2>Market Risks </h2>The general market performance of securities is dependent, in particular, on the development of the capital markets which, for their part, are influenced by the general global economic situation as well as by the economic and political framework conditions in the respective countries (so-called market risk). Changes to market prices such as interest rates, commodity prices or corresponding volatilities may have a negative effect on the valuation of the Underlying(s) or the Product. There is also the risk of market disruptions (such as trading or stock market interruptions or discontinuation of trading) or other unforeseeable occurrences concerning the respective Underlyings and/or their stock exchanges or markets taking place during the term or upon maturity of the Products. Such occurrences can have an effect on the time of redemption and/or on the value of the Products.</li><li><h2>No dividend payment </h2>This Product does not confer any claim to receive rights and/or payments of the underlying, such as dividend payments, unless explicitly stated herein, and therefore, without prejudice to any coupon or dividend payments provided for in this Termsheet, does not yield any current income. This means that potential losses in value of the Product cannot be compensated by other income.</li><li><h2>Credit Risk of Issuing Parties </h2>Investors bear the credit risk of the Issuing Parties of the Product. The Products constitute unsubordinated and unsecured obligations of the relevant Issuing Party and rank pari passu with each and all other current and future unsubordinated and unsecured obligations of the relevant Issuing Party. The insolvency of an Issuing Party may lead to a partial or total loss of the invested capital. Potential Investors should note that the Depositor is not rated by the credit rating agencies, i.e. there is no credit rating for the Depositor.</li><li><h2>Secondary Market </h2>The Depositor and/or the Lead Manager or any third party appointed by the Depositor, as applicable, intends, under normal market conditions, to provide bid and offer prices for the Products on a regular basis (if specified in the section “General Information”). However, the Depositor and/or the Lead Manager, as applicable, make no firm commitment to provide liquidity by means of bid and offer prices for the Products, and assume no legal obligation to quote any such prices or with respect to the level or determination of such prices. In special market situations, where the Depositor and/or the Lead Manager is/are unable to enter into hedging transactions, or where such transactions are very difficult to enter into, the spread between the bid and offer prices may be temporarily expanded, in order to limit the economic risks of the Depositor and/or the Lead Manager.</li><li><h2>Illiquidity Risk </h2>One or, if applicable, more of the Underlyings might be or become illiquid over the life time of the Product. Illiquidity of an Underlying might lead to larger bid/offer spreads of the Product and/or to an extended time period for buying and/or selling the Underlying respective to acquire, unwind or dispose of the hedging transaction(s) or asset(s) or to realise, recover or remit the proceeds of such hedging transaction(s) or asset(s) which might implicate a postponed redemption or delivery and/or a modified redemption amount, as reasonably determined by the Calculation Agent.</li>');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (977,2,NOW(),null,'<h2>Deposit by bank wire transfer</h2><p>As copyop is powered by any.capital, in order to deposit by bank wire, please make a bank transfer to any.capital bank account whose details appear below. Remember to indicate your Username on our platform in the comment field on the bank transfer form. After executing the transfer, please send confirmation of the transfer by fax to number <strong>+44-8081890112</strong> or send a scanned copy of the confirmation by email to <a href=''mailto:support@any.capital''>support@any.capital</a>.</p><br/><h2>Bank details</h2><p><b>Beneficiary name:</b> any.capital payment services limited</p><p><b>Account number:</b> 900910662</p><p><b>IBAN:</b> DE07590100660900910662</p><p><b>Swift / BIC Code:</b> PBNKDEFF590</p><p><b>Bank name:</b> Postbank Saarbruecken</p><p><b>Bank Address:</b> Postgiroamt Zas, D-6600 Saarbruecken 9, Germany</p><br/><p>A wire transfer may take up to 5 business days to arrive at our bank.</p><p>The deposit will be confirmed when our bank receives the wire transfer.</p>');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (1013,2,NOW(),null,'<p>The ANY.CAPITAL Website serves for information purposes only and does not constitute research. This site and all materials, documents and information used therein or distributed in the context of its publication do not constitute or form part of and should not be construed as, an offer (public or private) or a solicitation of offers (public or private) to purchase or subscribe for or sell shares or other securities of the Company or any of its affiliates or subsidiaries in any jurisdiction or an inducement to enter into investment activity in any jurisdiction, and may not be used for such purposes.</p><p>This website may contain specific forward-looking statements, e.g. statements including terms like "believe", "assume", "expect", "forecast", "project", "may", "could", "might", "will" or similar expressions. Such forward-looking statements are subject to known and unknown risks, uncertainties and other factors which may result in a substantial divergence between the actual results, financial situation, development or performance of ANY.CAPITAL or any of its affiliates or subsidiaries and those explicitly or implicitly presumed in these statements.</p><p>These factors include, but are not limited to: (1) general market, macroeconomic, governmental and regulatory trends, (2) movements in securities markets, exchange rates and interest rates and (3) other risks and uncertainties inherent in our business. Against the background of these uncertainties, you should not rely on forwardlooking statements. Neither the Company nor any of its affiliates or subsidiaries or their respective bodies, executives, employees and advisers assume any responsibility to prepare or disseminate any supplement, amendment, update or revision to any of the information, opinions or forward-looking statements contained in this half year report or to adapt them to any change in events, conditions or circumstances, except as required by applicable law or regulation.</p><p>Any - including only partial - reproduction of any article or picture included in this website is solely permitted based on an authorization from ANY.CAPITAL. No responsibility is assumed in case of unsolicited delivery.</p><p>© ANY.CAPITAL 2016. All rights reserved.</p>');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (1014,2,NOW(),null,'<h1 class=''green''>Take a Note</h1><div class=''white-separator-small''></div><p class=''white''>In a world of financial instability, zero interest rates and tumbling markets, it''s time for you to discover new investment opportunities. Capital Notes, AKA Structured Deposits, are a unique way to get a better yield on your savings with limited downside.</p><p class=''white''>Choose the protection level you want, the market scenario that makes sense to you and start benefiting from a form of investment that was kept away from you until now.</p><button type=''button'' class=''button video-button''>Watch our intro video</button>');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (1015,2,NOW(),null,'<div class=''gray-overlay''><h1 class=''white''>Unlocking the World of <span class=''yellow''>Capital Notes</span></h1><p class=''white''>Fund managers and high net worth individuals have been using Capital Notes extensively for years, also known as Structured Deposits, to generate fantastic profits with minimal risk.</p><p class=''white''>Any.Capital is now pioneering a global project to make this form of investment accessible and affordable to every household with savings to protect. Even modest amounts can grow steadily. See some examples below.</p><button type=''button'' class=''button video-button''>Watch our intro video</button></div>');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (1016,2,NOW(),null,'<h1 class=''white''>Your Financial <span class=''yellow''>Safety Net</span></h1><div class=''white-circle-separator''></div><p class=''white''>In a world of political and monetary instability, Any.Capital is introducing a new way for you to protect your investment money.</p><p class=''white''>We carefully selected and simplified for you a set of Capital Notes, that bear a full or partial guarantee on your principal investment funds, and a potential upside if certain market conditions are met.</p><button type=''button'' class=''button video-button''>Watch our intro video</button>');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (1017,2,NOW(),null,'<h1 class=''white''>Unlocking the World of <span class=''yellow''>Capital Notes</span></h1><div class=''gray-overlay''><p class=''white''>Fund managers and high net worth individuals have been using Capital Notes extensively for years, also known as Structured Deposits, to generate fantastic profits with minimal risk.</p><p class=''white''>Any.Capital is now pioneering a global project to make this form of investment accessible and affordable to every household with savings to protect. Even modest amounts can grow steadily. See some examples below.</p><button type=''button'' class=''button video-button''>Watch our intro video</button></div>');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (1103,2,NOW(),null,'<ul><li><strong>Barrier Level</strong><span>An underlying asset''s price level which changes the Note''s return if reached during the Note''s lifetime</span></li><li><strong>Bonus Level</strong><span>An additional predefined return on the nominal investment amount</span></li><li><strong>Cap</strong><span>A maximum return a Note can yield</span></li><li><strong>Coupon</strong><span>A fixed yield on the nominal investment according to pre-set conditions</span></li><li><strong>Discount</strong><span>Buying into a running Note’s original terms at a favorable price</span></li><li><strong>Final Fixing Date</strong><span>The date when the final price level of the underlying asset is recorded</span></li><li><strong>Final Fixing Level</strong><span>The price level of the underlying asset used to calculate the payout at maturity</span></li><li><strong>Initial Fixing Date</strong><span>The date when the first price level of the underlying asset is recorded</span></li><li><strong>Initial Fixing Level</strong><span>The price level of the underlying asset recorded at the Initial Fixing Date</span></li><li><strong>Market Level</strong><span>The current price of an underlying asset</span></li><li><strong>Maturity</strong><span>Lifetime of a Note</span></li><li><strong>Participation</strong><span>The proportion between the Note''s performance and the underlying asset''s price increase</span></li><li><strong>Premium</strong><span>Buying into a running Note’s original terms at a more expensive price</span></li><li><strong>Protection (level)</strong><span>The percentage of your nominal investment returned in a Note''s worst case scenario</span></li><li><strong>Redemption Date</strong><span>Settlement date (equivalent to expiry date)</span></li><li><strong>Strike Level</strong><span>An underlying asset''s price level which changes the Note''s return if breached at the Final Fixing Date</span></li><li><strong>Trigger Level</strong><span> Level of the underlying asset that entitles the investor to a predefined payment</span></li><li><strong>Underlying Asset</strong><span>Any financial class (stock, currency pair, index, interest rate, etc.)</span></li><li><strong>Upside</strong><span>The potential performance of the Note</span></li></ul>');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (1145,2,NOW(),null,'If {asset} doesn''t close above {barrierlevelXlastprice} at the {settlementdate} you get {participation/100} times any price increase less {100%-protection level}. You are protected at {protectedlevel} from any decline below {lastprice}*');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (1146,2,NOW(),null,'You get an annual {couponsize} on your original investment regardless of the asset performance PLUS your original investment as long as {asset} closes above {barrierlevelXlastprice}* on {final-fixing-date}');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (1147,2,NOW(),null,'You get an annual {couponsize} guaranteed profit on your original investment regardless of the asset performance PLUS your original investment amount as long as {asset} closes below {barrierlevelXlastprice}* on {final-fixing-date}');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (1148,2,NOW(),null,'You get an annual {couponsize} on your original investment as long as on {final-fixing-date} {asset} is above {strikelevelXlastprice}*.  In addition, you receive back your original investment if  {asset} closes above {barrierlevelXlastprice}*');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (1149,2,NOW(),null,'You get 100% of any price increase of {asset} if it closes above {bonuslevelXlastprice}, otherwise if {asset} closes above {barrierlevelXlastprice}* at the {settlementdate}, you receive {bonuslevel} of your original investment');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (1150,2,NOW(),null,'You get {participation/100} times of any price increase of {asset} as long as it closes above {bonuslevelXlastprice} , otherwise as long as {asset} doesn''t touch {barrierlevelXlastprice}* by {settlementdate}, you receive {bonuslevel} of your original investment');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (1151,2,NOW(),null,'The Investor is entitled to receive from the Issuer on the Redemption Date a Cash Settlement in the Settlement Currency which equals the Capital Protection multiplied by the Denomination. In addition, the Investor can participate in the appreciation of the Underlying (limited), as described under "Redemption".');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (1152,2,NOW(),null,'This Product offers the Investor a Coupon Rate regardless of the performance of the Underlying combined with a conditional downside protection. On the Redemption Date, the Redemption of the Product will depend if a Barrier Event has occurred or not as described in section “Redemption”.');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (1153,2,NOW(),null,'This Product offers the Investor a Coupon Rate regardless of the performance of the Underlying combined with a conditional upside protection. On the Redemption Date, the Redemption of the Product will depend if a Barrier Event has occurred or not as described in section “Redemption”.');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (1154,2,NOW(),null,'This Product offers the Investor the opportunity to receive a Conditional Coupon Amount. If the Barrier Event has not occurred, the Investor will receive the Denomination on the Redemption Date. If a Barrier Event has occurred, the redemption of the Product will depend on the value of the Underlying, as described in section “Redemption”.');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (1155,2,NOW(),null,'This Product offers the Investor, at the Redemption Date, a cash Settlement Currency equal to the Denomination multiplied by the Bonus Level (in%) unless a Barrier Event has occurred. In addition, the investor has the opportunity to participate in the performance of the underlying above the Bonus Level (in%). If a Barrier Event has occurred, the redemption of the Product is described in the section “Redemption”.');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (1156,2,NOW(),null,'This Product offers the Investor at the Redemption Date a Cash Settlement in the Settlement Currency equal to the Denomination multiplied by the Bonus Level (in %), unless a Barrier Event has occurred. In addition, the Investor has - independently of a Barrier Event having occurred - the opportunity to participate in the performance of the Underlying above the Bonus Level (in %), leveraged by the Participation. If a Barrier Event has occurred, the redemption of the Product will depend on the value of the Underlying, as described in section "Redemption".');
Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,TIME_UPDATED,WRITER_ID,LARGE_VALUE) values (1480,2,NOW(),null,'Barrier Reverse Convertible 
This product allows you to receive a fixed amount of money on your investment called the Coupon, no matter how the asset perform. Your investment is 100% protected if the asset never breach a certain level called the Barrier. If the Barrier is breached and perform negatively, your investment performance is similar than if you buy the asset itself except that you still receive the coupon. The Barrier level is always set much lower than the current market price When buying such product, you don’t know how the asset will perform but you are sure that it will never breached the barrier. 
Inverse Barrier Reverse Convertible
This product allows you to receive a fixed amount of money on your investment called the Coupon, no matter how the asset perform. Your investment is 100% protected if the asset never breach a certain level called the Barrier. If the Barrier is breached and perform positively, your investment performance is similar than if you sell the asset itself except that you still receive the coupon. The Barrier level is always set much higher than the current market price When buying such product, you don’t know how the asset will perform but you are sure that it will never breached the barrier. 
Reverse convertible 
This product allows you to receive a fixed amount of money on your investment called the Coupon, no matter how the asset perform. Your investment is 100% protected if the asset closes above a certain level called the Strike. If the asset closes below the strike your investment performance is similar than if you buy the asset itself at the strike level except that you still receive the coupon. The Strike level is always set much lower than the current market price When buying such product, you don’t know how the asset will perform but you are sure that it will close above the strike. 
Express certificate 
This product allows you to receive a fixed amount of money on your investment called the Coupon, simply if the asset goes up no matter how much. Your investment is 100% protected if the asset never breach a certain level called the Barrier. If the Barrier is breached and perform negatively, your investment performance is similar than if you buy the asset itself.The Barrier level is always set much lower than the current market priceWhen buying such product, you think the asset will not goes down but you are not sure how much it can goes up and you strongly believe that it will not breached the barrier.');


Insert into anycapital.USERS (ID,BALANCE,PASSWORD,CURRENCY_ID,FIRST_NAME,LAST_NAME,STREET,ZIP_CODE,
TIME_CREATED,TIME_LAST_LOGIN,IS_ACTIVE,EMAIL,COMMENTS,IP,TIME_BIRTH_DATE,IS_CONTACT_BY_EMAIL,
IS_CONTACT_BY_SMS,IS_CONTACT_BY_PHONE,IS_ACCEPTED_TERMS,MOBILE_PHONE,LAND_LINE_PHONE,GENDER,
TIME_MODIFIED,CLASS_ID,STREET_NO,LANGUAGE_ID,UTC_OFFSET,COUNTRY_ID,CITY_NAME,UTC_OFFSET_CREATED,
WRITER_ID,AO_USER_ID,ACTION_SOURCE_ID,USER_AGENT,HTTP_REFERER,IS_REGULATED,VIP_STATUS_ID) 
values (1,3632279,'7c4a8d09ca3762af61e59520943dc26494f8941b',3,'1a','3','3','3',now(),null,1,'test@test.com','Junit','127.0.0.1',null,
1,1,1,1,'3','3','F',now(),1,'3',2,'UTC+0',3,'3','UTC+0',3,2189877,1,null,null,0,null);

Insert into anycapital.USERS (ID,BALANCE,PASSWORD,CURRENCY_ID,FIRST_NAME,LAST_NAME,STREET,ZIP_CODE,TIME_CREATED,
TIME_LAST_LOGIN,IS_ACTIVE,EMAIL,COMMENTS,IP,TIME_BIRTH_DATE,IS_CONTACT_BY_EMAIL,IS_CONTACT_BY_SMS,
IS_CONTACT_BY_PHONE,IS_ACCEPTED_TERMS,MOBILE_PHONE,LAND_LINE_PHONE,GENDER,TIME_MODIFIED,CLASS_ID,STREET_NO,
LANGUAGE_ID,UTC_OFFSET,COUNTRY_ID,CITY_NAME,UTC_OFFSET_CREATED,WRITER_ID,AO_USER_ID,ACTION_SOURCE_ID,USER_AGENT,
HTTP_REFERER,IS_REGULATED,VIP_STATUS_ID) values (2,124030,'7c4a8d09ca3762af61e59520943dc26494f8941b',3,'LioR','SoLoMoN','sham','1',
now()
,null,1,'lior@etrader.co.il','lo','127.0.0.1',null,0,0,0,1,'12345','123456789','M',
now(),1,'po',2,'UTC+0',7,'no','UTC+0',3,0,1,null,null,0,null);


Insert into anycapital.USERS (ID,BALANCE,PASSWORD,CURRENCY_ID,FIRST_NAME,LAST_NAME,STREET,ZIP_CODE,TIME_CREATED,
TIME_LAST_LOGIN,IS_ACTIVE,EMAIL,COMMENTS,IP,TIME_BIRTH_DATE,IS_CONTACT_BY_EMAIL,IS_CONTACT_BY_SMS,IS_CONTACT_BY_PHONE,
IS_ACCEPTED_TERMS,MOBILE_PHONE,LAND_LINE_PHONE,GENDER,TIME_MODIFIED,CLASS_ID,STREET_NO,LANGUAGE_ID,
UTC_OFFSET,COUNTRY_ID,CITY_NAME,UTC_OFFSET_CREATED,WRITER_ID,AO_USER_ID,ACTION_SOURCE_ID,USER_AGENT,
HTTP_REFERER,IS_REGULATED,VIP_STATUS_ID) values (10002,460006,'7c4a8d09ca3762af61e59520943dc26494f8941b',3,
'junit','investment','BALANCE_MORE_THEN_MIN','11111',str_to_date('23/07/2016 18:50.42','%d/%m/%Y %H:%i.%s')
,null,1,'junit.inv.test@anyoption.com',null,'127.0.0.1',null,0,0,0,0,'12345678',null,'M',
str_to_date('25/01/2017 14:52.15','%d/%m/%Y %H:%i.%s'),null,'1',2,'UTC+0',2,'aaaaa','UTC+0',3,0,1,null,null,null,null);

Insert into anycapital.USERS (ID,BALANCE,PASSWORD,CURRENCY_ID,FIRST_NAME,LAST_NAME,STREET,ZIP_CODE,TIME_CREATED,
TIME_LAST_LOGIN,IS_ACTIVE,EMAIL,COMMENTS,IP,TIME_BIRTH_DATE,IS_CONTACT_BY_EMAIL,IS_CONTACT_BY_SMS,
IS_CONTACT_BY_PHONE,IS_ACCEPTED_TERMS,MOBILE_PHONE,LAND_LINE_PHONE,GENDER,TIME_MODIFIED,CLASS_ID,STREET_NO,
LANGUAGE_ID,UTC_OFFSET,COUNTRY_ID,CITY_NAME,UTC_OFFSET_CREATED,WRITER_ID,AO_USER_ID,ACTION_SOURCE_ID,
USER_AGENT,HTTP_REFERER,IS_REGULATED,VIP_STATUS_ID) values (10005,5257728,'7c4a8d09ca3762af61e59520943dc26494f8941b',3,
'junit','investments','have money','11111',str_to_date('23/07/2016 18:54.29','%d/%m/%Y %H:%i.%s'),null,1,'junit.with.money@anyoption.com',
null,'127.0.0.1',null,0,0,0,0,'12345678',null,'M',str_to_date('31/01/2017 09:28.24','%d/%m/%Y %H:%i.%s'),null,'1',2,
'UTC+0',2,'aaaaa','UTC+0',3,0,1,null,null,null,null);

Insert into anycapital.USERS (ID,BALANCE,PASSWORD,CURRENCY_ID,FIRST_NAME,LAST_NAME,STREET,ZIP_CODE,TIME_CREATED,TIME_LAST_LOGIN,IS_ACTIVE,EMAIL,COMMENTS,IP,TIME_BIRTH_DATE,IS_CONTACT_BY_EMAIL,IS_CONTACT_BY_SMS,IS_CONTACT_BY_PHONE,IS_ACCEPTED_TERMS,MOBILE_PHONE,LAND_LINE_PHONE,GENDER,TIME_MODIFIED,CLASS_ID,STREET_NO,LANGUAGE_ID,UTC_OFFSET,COUNTRY_ID,CITY_NAME,UTC_OFFSET_CREATED,WRITER_ID,AO_USER_ID,ACTION_SOURCE_ID,USER_AGENT,HTTP_REFERER,IS_REGULATED,VIP_STATUS_ID) values (10094,10037864,'7c4a8d09ca3762af61e59520943dc26494f8941b',3,'3','2','2','22',str_to_date('11/09/2016 11:35.51','%d/%m/%Y %H:%i.%s'),null,1,'chen@etrader.co.il',null,'0:0:0:0:0:0:0:1',str_to_date('01/01/1980 00:00.00','%d/%m/%Y %H:%i.%s'),0,0,1,1,'2','2','F',str_to_date('03/02/2017 12:55.03','%d/%m/%Y %H:%i.%s'),null,'2',2,'UTC+0',219,'2','UTC+0',3,130782,1,null,null,0,null);
Insert into anycapital.USERS (ID,BALANCE,PASSWORD,CURRENCY_ID,FIRST_NAME,LAST_NAME,STREET,ZIP_CODE,TIME_CREATED,TIME_LAST_LOGIN,IS_ACTIVE,EMAIL,COMMENTS,IP,TIME_BIRTH_DATE,IS_CONTACT_BY_EMAIL,IS_CONTACT_BY_SMS,IS_CONTACT_BY_PHONE,IS_ACCEPTED_TERMS,MOBILE_PHONE,LAND_LINE_PHONE,GENDER,TIME_MODIFIED,CLASS_ID,STREET_NO,LANGUAGE_ID,UTC_OFFSET,COUNTRY_ID,CITY_NAME,UTC_OFFSET_CREATED,WRITER_ID,AO_USER_ID,ACTION_SOURCE_ID,USER_AGENT,HTTP_REFERER,IS_REGULATED,VIP_STATUS_ID) values (10095,44600000,'7c4a8d09ca3762af61e59520943dc26494f8941b',3,'Jonagold','Apple','guk',null,str_to_date('11/09/2016 12:19.51','%d/%m/%Y %H:%i.%s'),null,1,'liors@etrader.co.il',null,'0:0:0:0:0:0:0:1',null,1,1,1,1,'111111111',null,'M',str_to_date('31/01/2017 11:06.06','%d/%m/%Y %H:%i.%s'),null,null,2,'UTC+0',2,'kuyhuky','UTC+0',3,1013276,1,null,null,0,null);

Insert into anycapital.marketing_domains(domain) values ('https://www.testenv.anycapital.com/');

use anycapital;

Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.isr', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Israel' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.isr'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.afg', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Afghanistan' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.afg'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.alb', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Albania' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.alb'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.dza', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Algeria' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.dza'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.asm', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'American Samoa' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.asm'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.and', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Andorra' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.and'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.ago', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Angola' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.ago'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.aia', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Anguilla' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.aia'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.ata', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Antarctica' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.ata'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.atg', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Antigua and Barbuda' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.atg'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.arg', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Argentina' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.arg'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.arm', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Armenia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.arm'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.abw', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Aruba' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.abw'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.aus', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Australia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.aus'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.aut', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Austria' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.aut'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.aze', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Azerbaijan' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.aze'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.bhs', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Bahamas' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.bhs'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.bhr', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Bahrain' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.bhr'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.bgd', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Bangladesh' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.bgd'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.brb', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Barbados' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.brb'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.blr', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Belarus' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.blr'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.bel', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Belgium' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.bel'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.blz', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Belize' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.blz'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.ben', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Benin' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.ben'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.bmu', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Bermuda' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.bmu'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.btn', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Bhutan' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.btn'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.bol', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Bolivia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.bol'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.bih', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Bosnia and Herzegowina' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.bih'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.bwa', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Botswana' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.bwa'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.bvt', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Bouvet Island' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.bvt'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.bra', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Brazil' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.bra'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.brn', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Brunei Darussalam' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.brn'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.bgr', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Bulgaria' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.bgr'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.bfa', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Burkina Faso' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.bfa'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.bdi', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Burundi' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.bdi'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.khm', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Cambodia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.khm'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.cmr', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Cameroon' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.cmr'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.can', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Canada' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.can'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.cpv', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Cape Verde' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.cpv'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.cym', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Caymen Islands' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.cym'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.caf', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Central Africa' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.caf'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.tcd', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Chad' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.tcd'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.chl', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Chile' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.chl'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.chn', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'China' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.chn'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.cxr', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Christmas Island' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.cxr'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.cck', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Cocos Islands' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.cck'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.col', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Colombia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.col'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.com', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Comoros' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.com'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.cog', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Congo' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.cog'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.cok', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Cook Islands' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.cok'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.cri', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Costa Rica' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.cri'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.civ', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Ivory coast' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.civ'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.hrv', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Croatia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.hrv'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.cub', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Cuba' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.cub'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.cyp', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Cyprus' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.cyp'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.cze', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Czech Republic' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.cze'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.dnk', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Denmark' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.dnk'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.dji', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Djibouti' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.dji'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.dma', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Dominica' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.dma'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.dom', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Dominican Republic' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.dom'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.tmp', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'East Timor' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.tmp'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.ecu', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Ecuador' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.ecu'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.egy', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Egypt' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.egy'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.slv', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'El Salvador' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.slv'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.gnq', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Equatoirial Guinea' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.gnq'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.eri', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Eritrea' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.eri'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.est', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Estonia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.est'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.eth', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Ethiopia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.eth'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.flk', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Falkland Islands' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.flk'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.fro', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Faroe Islands' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.fro'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.fji', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Fiji' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.fji'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.fin', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Finland' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.fin'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.fra', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'France' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.fra'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.guf', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'French Guinea' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.guf'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.pyf', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'French Polyensia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.pyf'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.atf', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'French Southern Territories' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.atf'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.gab', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Gabon' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.gab'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.gmb', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Gambia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.gmb'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.geo', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Georgia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.geo'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.deu', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Germany' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.deu'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.gha', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Ghana' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.gha'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.gib', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Gibraltar' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.gib'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.grc', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Greece' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.grc'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.grl', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Greenland' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.grl'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.grd', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Grenada' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.grd'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.glp', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Guadeloupe' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.glp'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.gum', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Guam' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.gum'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.gtm', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Guatemala' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.gtm'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.gin', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Guinea' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.gin'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.guy', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Guyana' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.guy'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.hti', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Haiti' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.hti'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.hmd', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Heard and McDonald Islands' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.hmd'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.hnd', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Honduras' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.hnd'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.hkg', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Hong Kong' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.hkg'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.hun', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Hungary' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.hun'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.isl', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Iceland' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.isl'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.ind', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'India' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.ind'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.idn', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Indonesia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.idn'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.irn', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Iran' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.irn'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.irq', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Iraq' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.irq'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.irl', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Ireland' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.irl'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.ita', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Italy' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.ita'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.jam', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Jamaica' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.jam'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.jpn', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Japan' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.jpn'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.jor', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Jordan' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.jor'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.kaz', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Kazakhstan' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.kaz'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.ken', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Kenya' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.ken'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.kir', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Kiribati' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.kir'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.kwt', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Kuwait' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.kwt'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.kgz', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Kyrgyzstan' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.kgz'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.lao', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Laos' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.lao'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.lva', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Latvia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.lva'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.lbn', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Lebanon' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.lbn'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.lso', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Lesotho' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.lso'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.lbr', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Liberia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.lbr'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.lby', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Libya' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.lby'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.lie', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Liechtenstein' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.lie'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.ltu', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Lithuania' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.ltu'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.lux', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Luxembourg' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.lux'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mac', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Macau' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mac'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mkd', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Macedonia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mkd'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mdg', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Madagascar' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mdg'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mwi', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Malawi' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mwi'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mys', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Malaysia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mys'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mdv', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Maldives' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mdv'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mli', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Mali' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mli'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mlt', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Malta' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mlt'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mhl', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Marshal Islands' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mhl'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mtq', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Martinique' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mtq'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mrt', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Mauritania' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mrt'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mus', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Mauritius' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mus'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.myt', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Maotte Island' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.myt'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mex', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Mexico' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mex'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.fsm', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Micronesia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.fsm'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mda', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Moldova' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mda'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mco', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Monaco' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mco'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mng', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Mongolia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mng'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.msr', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Monteserret' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.msr'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mar', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Morocco' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mar'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.moz', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Mozambique' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.moz'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mmr', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Myanmar' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mmr'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.nam', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Namibia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.nam'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.nru', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Nauru' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.nru'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.npl', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Nepal' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.npl'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.nld', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Netherlands' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.nld'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.ant', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Netherlands Antilles' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.ant'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.ncl', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'New caledonia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.ncl'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.nzl', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'New Zealand' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.nzl'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.nic', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Nicaragua' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.nic'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.ner', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Niger' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.ner'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.nga', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Nigeria' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.nga'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.niu', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Niue' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.niu'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.nfk', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Norfolk Island' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.nfk'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.prk', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'North Korea' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.prk'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mnp', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Northern Marianas Islands' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mnp'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.nor', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Norway' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.nor'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.omn', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Oman' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.omn'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.pak', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Pakistan' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.pak'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.plw', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Palau' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.plw'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.pan', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Panama' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.pan'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.png', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Papua New Guinea' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.png'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.pry', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Paraguay' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.pry'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.per', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Peru' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.per'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.phl', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Philippines' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.phl'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.pcn', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Pitcairn' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.pcn'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.pol', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Poland' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.pol'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.prt', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Portugal' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.prt'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.pri', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Pureto Rico' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.pri'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.qat', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Qatar' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.qat'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.reu', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Reunion Island' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.reu'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.rom', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Romania' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.rom'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.rus', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Russia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.rus'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.rwa', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Rwanda' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.rwa'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.kna', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Saint kits and Nevis' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.kna'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.lca', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Saint Lucia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.lca'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.vct', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Saint Vincent and The Grenadlines' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.vct'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.wsm', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Samoa' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.wsm'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.smr', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'San Marino' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.smr'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.stp', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Sao Tome and Principe' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.stp'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.sau', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Saudi Arabia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.sau'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.sen', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Senegal' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.sen'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.syc', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Seychelles' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.syc'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.sle', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Sierra Leone' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.sle'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.sgp', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Singapore' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.sgp'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.svk', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Slovakia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.svk'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.svn', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Slovenia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.svn'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.slb', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Solomon Islands' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.slb'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.som', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Somalia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.som'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.zaf', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'South Africa' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.zaf'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.sgs', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'South Georgia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.sgs'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.kor', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'South Korea' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.kor'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.esp', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Spain' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.esp'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.lka', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Sri Lanka' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.lka'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.shn', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Saint Helena' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.shn'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.spm', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Saint Pierre and Miquelon' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.spm'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.sdn', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Sudan' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.sdn'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.sur', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Suriname' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.sur'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.sjm', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Svalbard and Jan Mayen Islands' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.sjm'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.swz', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Swaziland' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.swz'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.swe', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Sweden' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.swe'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.che', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Switzerland' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.che'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.syr', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Syria' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.syr'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.twn', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Taiwan' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.twn'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.tjk', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Tajikistan' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.tjk'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.tza', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Tanzania' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.tza'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.tha', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Thailand' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.tha'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.tgo', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Togo' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.tgo'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.tkl', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Tokelau' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.tkl'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.ton', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Tonga Islands' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.ton'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.tto', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Trinidad and Tobago' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.tto'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.tun', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Tunisia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.tun'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.tur', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Turkey' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.tur'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.tkm', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Turkmenistan' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.tkm'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.tca', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Turks and Caicos Islands' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.tca'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.tuv', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Tuvalu' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.tuv'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.uga', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Uganda' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.uga'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.are', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'United Arab Emirates' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.are'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.gbr', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'United Kingdom' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.gbr'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.usa', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'United States' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.usa'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.ukr', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Ukraine' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.ukr'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.ury', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Uruguay' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.ury'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.uzb', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Uzbekistan' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.uzb'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.vut', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Vanuato' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.vut'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.vat', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Vatican' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.vat'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.ven', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Venezuela' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.ven'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.vnm', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Vietnam' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.vnm'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.vgb', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Virgin Islands UK' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.vgb'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.vir', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Virgin Islands US' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.vir'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.wlf', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Wallis and Futuna Islands' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.wlf'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.esh', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Western Sahara' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.esh'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.yem', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Yemen' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.yem'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.zar', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Zaire' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.zar'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.zmb', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Zambia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.zmb'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.zwe', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Zimbabwe' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.zwe'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.srb', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Serbia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.srb'and mr.ACTION_SOURCE_ID =2;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mne', 2,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Montenegro' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mne'and mr.ACTION_SOURCE_ID =2;


Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.isr', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Israel' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.isr'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.afg', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Afghanistan' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.afg'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.alb', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Albania' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.alb'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.dza', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Algeria' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.dza'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.asm', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'American Samoa' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.asm'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.and', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Andorra' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.and'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.ago', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Angola' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.ago'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.aia', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Anguilla' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.aia'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.ata', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Antarctica' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.ata'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.atg', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Antigua and Barbuda' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.atg'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.arg', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Argentina' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.arg'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.arm', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Armenia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.arm'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.abw', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Aruba' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.abw'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.aus', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Australia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.aus'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.aut', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Austria' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.aut'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.aze', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Azerbaijan' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.aze'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.bhs', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Bahamas' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.bhs'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.bhr', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Bahrain' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.bhr'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.bgd', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Bangladesh' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.bgd'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.brb', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Barbados' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.brb'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.blr', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Belarus' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.blr'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.bel', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Belgium' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.bel'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.blz', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Belize' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.blz'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.ben', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Benin' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.ben'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.bmu', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Bermuda' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.bmu'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.btn', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Bhutan' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.btn'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.bol', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Bolivia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.bol'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.bih', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Bosnia and Herzegowina' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.bih'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.bwa', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Botswana' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.bwa'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.bvt', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Bouvet Island' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.bvt'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.bra', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Brazil' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.bra'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.brn', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Brunei Darussalam' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.brn'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.bgr', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Bulgaria' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.bgr'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.bfa', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Burkina Faso' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.bfa'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.bdi', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Burundi' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.bdi'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.khm', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Cambodia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.khm'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.cmr', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Cameroon' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.cmr'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.can', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Canada' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.can'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.cpv', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Cape Verde' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.cpv'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.cym', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Caymen Islands' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.cym'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.caf', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Central Africa' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.caf'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.tcd', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Chad' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.tcd'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.chl', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Chile' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.chl'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.chn', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'China' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.chn'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.cxr', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Christmas Island' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.cxr'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.cck', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Cocos Islands' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.cck'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.col', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Colombia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.col'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.com', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Comoros' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.com'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.cog', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Congo' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.cog'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.cok', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Cook Islands' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.cok'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.cri', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Costa Rica' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.cri'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.civ', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Ivory coast' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.civ'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.hrv', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Croatia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.hrv'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.cub', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Cuba' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.cub'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.cyp', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Cyprus' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.cyp'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.cze', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Czech Republic' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.cze'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.dnk', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Denmark' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.dnk'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.dji', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Djibouti' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.dji'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.dma', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Dominica' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.dma'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.dom', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Dominican Republic' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.dom'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.tmp', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'East Timor' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.tmp'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.ecu', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Ecuador' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.ecu'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.egy', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Egypt' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.egy'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.slv', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'El Salvador' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.slv'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.gnq', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Equatoirial Guinea' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.gnq'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.eri', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Eritrea' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.eri'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.est', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Estonia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.est'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.eth', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Ethiopia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.eth'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.flk', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Falkland Islands' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.flk'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.fro', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Faroe Islands' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.fro'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.fji', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Fiji' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.fji'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.fin', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Finland' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.fin'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.fra', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'France' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.fra'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.guf', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'French Guinea' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.guf'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.pyf', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'French Polyensia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.pyf'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.atf', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'French Southern Territories' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.atf'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.gab', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Gabon' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.gab'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.gmb', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Gambia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.gmb'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.geo', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Georgia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.geo'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.deu', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Germany' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.deu'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.gha', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Ghana' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.gha'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.gib', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Gibraltar' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.gib'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.grc', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Greece' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.grc'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.grl', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Greenland' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.grl'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.grd', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Grenada' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.grd'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.glp', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Guadeloupe' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.glp'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.gum', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Guam' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.gum'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.gtm', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Guatemala' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.gtm'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.gin', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Guinea' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.gin'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.guy', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Guyana' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.guy'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.hti', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Haiti' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.hti'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.hmd', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Heard and McDonald Islands' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.hmd'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.hnd', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Honduras' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.hnd'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.hkg', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Hong Kong' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.hkg'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.hun', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Hungary' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.hun'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.isl', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Iceland' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.isl'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.ind', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'India' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.ind'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.idn', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Indonesia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.idn'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.irn', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Iran' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.irn'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.irq', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Iraq' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.irq'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.irl', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Ireland' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.irl'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.ita', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Italy' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.ita'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.jam', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Jamaica' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.jam'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.jpn', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Japan' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.jpn'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.jor', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Jordan' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.jor'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.kaz', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Kazakhstan' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.kaz'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.ken', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Kenya' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.ken'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.kir', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Kiribati' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.kir'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.kwt', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Kuwait' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.kwt'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.kgz', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Kyrgyzstan' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.kgz'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.lao', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Laos' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.lao'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.lva', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Latvia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.lva'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.lbn', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Lebanon' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.lbn'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.lso', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Lesotho' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.lso'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.lbr', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Liberia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.lbr'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.lby', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Libya' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.lby'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.lie', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Liechtenstein' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.lie'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.ltu', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Lithuania' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.ltu'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.lux', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Luxembourg' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.lux'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mac', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Macau' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mac'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mkd', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Macedonia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mkd'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mdg', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Madagascar' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mdg'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mwi', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Malawi' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mwi'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mys', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Malaysia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mys'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mdv', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Maldives' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mdv'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mli', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Mali' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mli'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mlt', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Malta' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mlt'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mhl', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Marshal Islands' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mhl'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mtq', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Martinique' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mtq'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mrt', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Mauritania' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mrt'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mus', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Mauritius' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mus'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.myt', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Maotte Island' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.myt'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mex', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Mexico' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mex'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.fsm', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Micronesia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.fsm'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mda', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Moldova' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mda'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mco', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Monaco' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mco'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mng', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Mongolia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mng'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.msr', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Monteserret' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.msr'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mar', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Morocco' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mar'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.moz', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Mozambique' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.moz'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mmr', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Myanmar' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mmr'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.nam', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Namibia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.nam'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.nru', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Nauru' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.nru'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.npl', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Nepal' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.npl'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.nld', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Netherlands' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.nld'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.ant', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Netherlands Antilles' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.ant'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.ncl', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'New caledonia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.ncl'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.nzl', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'New Zealand' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.nzl'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.nic', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Nicaragua' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.nic'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.ner', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Niger' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.ner'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.nga', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Nigeria' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.nga'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.niu', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Niue' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.niu'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.nfk', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Norfolk Island' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.nfk'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.prk', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'North Korea' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.prk'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mnp', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Northern Marianas Islands' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mnp'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.nor', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Norway' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.nor'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.omn', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Oman' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.omn'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.pak', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Pakistan' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.pak'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.plw', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Palau' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.plw'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.pan', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Panama' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.pan'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.png', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Papua New Guinea' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.png'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.pry', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Paraguay' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.pry'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.per', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Peru' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.per'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.phl', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Philippines' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.phl'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.pcn', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Pitcairn' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.pcn'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.pol', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Poland' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.pol'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.prt', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Portugal' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.prt'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.pri', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Pureto Rico' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.pri'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.qat', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Qatar' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.qat'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.reu', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Reunion Island' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.reu'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.rom', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Romania' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.rom'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.rus', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Russia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.rus'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.rwa', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Rwanda' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.rwa'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.kna', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Saint kits and Nevis' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.kna'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.lca', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Saint Lucia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.lca'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.vct', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Saint Vincent and The Grenadlines' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.vct'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.wsm', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Samoa' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.wsm'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.smr', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'San Marino' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.smr'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.stp', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Sao Tome and Principe' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.stp'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.sau', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Saudi Arabia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.sau'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.sen', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Senegal' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.sen'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.syc', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Seychelles' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.syc'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.sle', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Sierra Leone' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.sle'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.sgp', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Singapore' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.sgp'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.svk', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Slovakia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.svk'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.svn', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Slovenia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.svn'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.slb', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Solomon Islands' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.slb'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.som', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Somalia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.som'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.zaf', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'South Africa' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.zaf'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.sgs', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'South Georgia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.sgs'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.kor', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'South Korea' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.kor'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.esp', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Spain' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.esp'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.lka', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Sri Lanka' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.lka'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.shn', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Saint Helena' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.shn'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.spm', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Saint Pierre and Miquelon' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.spm'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.sdn', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Sudan' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.sdn'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.sur', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Suriname' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.sur'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.sjm', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Svalbard and Jan Mayen Islands' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.sjm'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.swz', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Swaziland' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.swz'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.swe', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Sweden' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.swe'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.che', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Switzerland' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.che'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.syr', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Syria' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.syr'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.twn', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Taiwan' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.twn'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.tjk', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Tajikistan' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.tjk'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.tza', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Tanzania' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.tza'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.tha', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Thailand' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.tha'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.tgo', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Togo' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.tgo'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.tkl', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Tokelau' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.tkl'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.ton', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Tonga Islands' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.ton'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.tto', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Trinidad and Tobago' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.tto'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.tun', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Tunisia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.tun'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.tur', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Turkey' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.tur'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.tkm', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Turkmenistan' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.tkm'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.tca', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Turks and Caicos Islands' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.tca'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.tuv', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Tuvalu' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.tuv'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.uga', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Uganda' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.uga'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.are', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'United Arab Emirates' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.are'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.gbr', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'United Kingdom' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.gbr'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.usa', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'United States' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.usa'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.ukr', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Ukraine' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.ukr'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.ury', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Uruguay' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.ury'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.uzb', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Uzbekistan' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.uzb'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.vut', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Vanuato' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.vut'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.vat', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Vatican' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.vat'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.ven', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Venezuela' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.ven'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.vnm', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Vietnam' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.vnm'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.vgb', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Virgin Islands UK' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.vgb'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.vir', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Virgin Islands US' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.vir'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.wlf', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Wallis and Futuna Islands' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.wlf'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.esh', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Western Sahara' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.esh'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.yem', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Yemen' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.yem'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.zar', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Zaire' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.zar'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.zmb', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Zambia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.zmb'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.zwe', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Zimbabwe' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.zwe'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.srb', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Serbia' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.srb'and mr.ACTION_SOURCE_ID =1;
Insert into anycapital.MSG_RES (`KEY`,ACTION_SOURCE_ID,type_id) values ('countries.mne', 1,1);Insert into anycapital.MSG_RES_LANGUAGE (MSG_RES_ID,MSG_RES_LANGUAGE_ID,VALUE) SELECT mr.id as MSG_RES_ID, 2 MSG_RES_LANGUAGE_ID,'Montenegro' VALUE FROM MSG_RES mr WHERE `KEY` like'countries.mne'and mr.ACTION_SOURCE_ID =1;

---------------------------------------------------------
-- SP-81 [Technical][General] - Issues handling to user
-- Eyal O
---------------------------------------------------------

-- issue_channels
INSERT INTO `anycapital`.`issue_channels` (`name`) VALUES ('issue.channel.email');
INSERT INTO `anycapital`.`issue_channels` (`name`) VALUES ('issue.channel.activity');
INSERT INTO `anycapital`.`issue_channels` (`name`) VALUES ('issue.channel.call');
INSERT INTO `anycapital`.`issue_channels` (`name`) VALUES ('issue.channel.comment');
INSERT INTO `anycapital`.`issue_channels` (`name`) VALUES ('issue.channel.sms');

-- issue_directions
INSERT INTO `anycapital`.`issue_directions` (`name`) VALUES ('issue.direction.outbound');
INSERT INTO `anycapital`.`issue_directions` (`name`) VALUES ('issue.direction.inbound');

-- issue_reached_statuses
INSERT INTO `anycapital`.`issue_reached_statuses` (`name`) VALUES ('issue.reached.status.reached');
INSERT INTO `anycapital`.`issue_reached_statuses` (`name`) VALUES ('issue.reached.status.not.reached');

-- issue_reactions
INSERT INTO `anycapital`.`issue_reactions` (`name`, `reached_status_id`) VALUES ('issue.reaction.cooperative', '1');
INSERT INTO `anycapital`.`issue_reactions` (`name`, `reached_status_id`) VALUES ('issue.reaction.uncooperative', '1');
INSERT INTO `anycapital`.`issue_reactions` (`name`, `reached_status_id`) VALUES ('issue.reaction.line.busy', '2');
INSERT INTO `anycapital`.`issue_reactions` (`name`, `reached_status_id`) VALUES ('issue.reaction.no.answer', '2');
INSERT INTO `anycapital`.`issue_reactions` (`name`, `reached_status_id`) VALUES ('issue.reaction.third.party', '2');
INSERT INTO `anycapital`.`issue_reactions` (`name`, `reached_status_id`) VALUES ('issue.reaction.wrong.number', '2');

-- issue_subjects
INSERT INTO `anycapital`.`issue_subjects` (`name`) VALUES ('issue.subject.general');


---------------------------------------------------------
-- SP-81 [Technical][General] - Issues handling to user
-- Eyal O
---------------------------------------------------------

---------------------------------------------------------
-- SP-391 - Registration + Questionnaire
-- Eyal O
---------------------------------------------------------

-- regulation_questionnaire_statuses
INSERT INTO `anycapital`.`regulation_questionnaire_statuses` (`name`, `display_name`) VALUES ('missing', 'regulation.questionnaire.status.missing');
INSERT INTO `anycapital`.`regulation_questionnaire_statuses` (`name`, `display_name`) VALUES ('pass', 'regulation.questionnaire.status.pass');
INSERT INTO `anycapital`.`regulation_questionnaire_statuses` (`name`, `display_name`) VALUES ('complete', 'regulation.questionnaire.status.complete');

update
	regulation_questionnaire_statuses rqs
set
	rqs.name = 'Unfitting',
    rqs.display_name = 'regulation.questionnaire.status.1'
where
	rqs.id = 1;
    
update
	regulation_questionnaire_statuses rqs
set
	rqs.name = 'Fitting',
    rqs.display_name = 'regulation.questionnaire.status.2'
where
	rqs.id = 2;
    
update
	regulation_questionnaire_statuses rqs
set
	rqs.name = 'Fitting after training',
    rqs.display_name = 'regulation.questionnaire.status.3'
where
	rqs.id = 3;
	
INSERT INTO `anycapital`.`regulation_questionnaire_statuses` (`name`, `display_name`) VALUES ('Unfitting Restricted', 'regulation.questionnaire.status.4');

---------------------------------------------------------
-- END
-- SP-391 - Registration + Questionnaire
-- Eyal O
---------------------------------------------------------

---------------------------------------------------------
-- SP-454 [Technical][Regulation] - update user step to approve
-- Eyal O
---------------------------------------------------------

Insert into anycapital.USER_STEPS (id,DISPLAY_NAME,NAME) values (4,'user.step.4','pending');
Insert into anycapital.USER_STEPS (id,DISPLAY_NAME,NAME) values (5,'user.step.5','approve');

---------------------------------------------------------
-- END
-- SP-454 [Technical][Regulation] - update user step to approve
-- Eyal O
---------------------------------------------------------

---------------------------------------------------------
-- SP-452 [Technical][Regulation] - Add country regulation risk identification
-- Eyal O
---------------------------------------------------------

Insert into anycapital.country_risk (id,DISPLAY_NAME,NAME) values (1,'country.risk.1','low');
Insert into anycapital.country_risk (id,DISPLAY_NAME,NAME) values (2,'country.risk.2','medium');
Insert into anycapital.country_risk (id,DISPLAY_NAME,NAME) values (3,'country.risk.3','high');

update  
	countries co
set
	co.risk_id = 1
where
	co.COUNTRY_NAME like 'Denmark'
    or co.COUNTRY_NAME like 'Finland'
    or co.COUNTRY_NAME like 'Sweden'
    or co.COUNTRY_NAME like 'New Zealand'
    or co.COUNTRY_NAME like 'Netherlands'
    or co.COUNTRY_NAME like 'Norway'
    or co.COUNTRY_NAME like 'Switzerland'
    or co.COUNTRY_NAME like 'Singapore'
    or co.COUNTRY_NAME like 'Canada'
    or co.COUNTRY_NAME like 'Luxembourg'
    or co.COUNTRY_NAME like 'Germany'
    or co.COUNTRY_NAME like 'United Kingdom'
    or co.COUNTRY_NAME like 'Australia'
    or co.COUNTRY_NAME like 'Iceland'
    or co.COUNTRY_NAME like 'Belgium'
    or co.COUNTRY_NAME like 'Austria'
    or co.COUNTRY_NAME like 'United States'
    or co.COUNTRY_NAME like 'Hong Kong'
    or co.COUNTRY_NAME like 'Japan'
    or co.COUNTRY_NAME like 'Ireland'
    or co.COUNTRY_NAME like 'Uruguay'
    or co.COUNTRY_NAME like 'Qatar'
    or co.COUNTRY_NAME like 'Chile'
    or co.COUNTRY_NAME like 'United Arab Emirates'
    or co.COUNTRY_NAME like 'Estonia'
    or co.COUNTRY_NAME like 'France'
    or co.COUNTRY_NAME like 'Bhutan'
    or co.COUNTRY_NAME like 'Botswana'
    or co.COUNTRY_NAME like 'Portugal'
    or co.COUNTRY_NAME like 'Taiwan'
    or co.COUNTRY_NAME like 'Poland';
    
update  
	countries co
set
	co.risk_id = 2
where
	co.COUNTRY_NAME like 'Cyprus'
    or co.COUNTRY_NAME like 'Israel'
    or co.COUNTRY_NAME like 'Lithuania'
    or co.COUNTRY_NAME like 'Slovenia'
    or co.COUNTRY_NAME like 'Spain'
    or co.COUNTRY_NAME like 'Czech Republic'
    or co.COUNTRY_NAME like 'Malta'
    or co.COUNTRY_NAME like 'South Korea';

update  
	countries co
set
	co.risk_id = 3
where
	co.COUNTRY_NAME in (
'Costa Rica'
,'Latvia'
,'Seychelles'
,'Cape Verde'
,'Rwanda'
,'Jordan'
,'Namibia'
,'Mauritius'
,'Saudi Arabia'
,'Georgia'
,'Hungary'
,'Slovakia'
,'Bahrain'
,'Croatia'
,'Malaysia'
,'Kuwait'
,'Ghana'
,'Cuba'
,'Greece'
,'Romania'
,'Oman'
,'Lesotho'
,'Montenegro'
,'Senegal'
,'South Africa'
,'Italy'
,'The FYR of Macedonia'
,'Sao Tome and Principe'
,'Turkey'
,'Jamaica'
,'Bulgaria'
,'Serbia'
,'Mongolia'
,'Panama'
,'Trinidad and Tobago'
,'El Salvador'
,'Bosnia and Herzegovina'
,'Burkina Faso'
,'India'
,'Thailand'
,'Tunisia'
,'Zambia'
,'Brazil'
,'Benin'
,'China'
,'Colombia'
,'Liberia'
,'Sri Lanka'
,'Albania'
,'Algeria'
,'Indonesia'
,'Morocco'
,'Peru'
,'Suriname'
,'Egypt'
,'Mali'
,'Philippines'
,'Armenia'
,'Mexico'
,'Bolivia'
,'Djibouti'
,'Gabon'
,'Niger'
,'Ethiopia'
,'Kosovo'
,'Moldova'
,'Dominican Republic'
,'Belarus'
,'Ecuador'
,'Argentina'
,'Togo'
,'Cote dIvoire'
,'Malawi'
,'Vietnam'
,'Mozambique'
,'Honduras'
,'Mauritania'
,'Pakistan'
,'Tanzania'
,'Azerbaijan'
,'Guyana'
,'Russia'
,'Sierra Leone'
,'Gambia'
,'Guatemala'
,'Kazakhstan'
,'Kyrgyzstan'
,'Lebanon'
,'Madagascar'
,'Timor-Leste'
,'Cameroon'
,'Iran'
,'Nicaragua'
,'Paraguay'
,'Ukraine'
,'Nepal'
,'Nigeria'
,'Comoros'
,'Tajikistan'
,'Guinea'
,'Kenya'
,'Bangladesh'
,'Papua New Guinea'
,'Uganda'
,'Laos'
,'Central African Republic'
,'Congo Republic'
,'Chad'
,'Democratic Republic of the Congo'
,'Myanmar'
,'Burundi'
,'Zimbabwe'
,'Cambodia'
,'Uzbekistan'
,'Eritrea'
,'Syria'
,'Turkmenistan'
,'Yemen'
,'Haiti'
,'Guinea-Bissau'
,'Venezuela'
,'Iraq'
,'Libya'
,'Angola'
,'South Sudan'
,'Sudan'
,'Afghanistan'
,'Korea (North)'
,'Somalia'
,'North Korea'
);

update
	countries co
set
	co.risk_id = 3
where 
	co.risk_id is null;
    
---------------------------------------------------------
-- END
-- SP-452 [Technical][Regulation] - Add country regulation risk identification
-- Eyal O
---------------------------------------------------------
	
--------------------------------------------
-- Eyal O
-- SP-498 ODT Questionnaire + Logic
--------------------------------------------

INSERT INTO `anycapital`.`qm_questions` (`name`, `display_name`, `is_active`, `order_id`, `question_type_id`) VALUES ('What is your annual income?', 'q.1',  1, 1, 1);
INSERT INTO `anycapital`.`qm_questions` (`name`, `display_name`, `is_active`, `order_id`, `question_type_id`) VALUES ('What is your main source of income?', 'q.2',  1, 2, 1);
INSERT INTO `anycapital`.`qm_questions` (`name`, `display_name`, `is_active`, `order_id`, `question_type_id`) VALUES ('How much do you think you will trade each year?', 'q.3',  1, 3, 1);
INSERT INTO `anycapital`.`qm_questions` (`name`, `display_name`, `is_active`, `order_id`, `question_type_id`) VALUES ('What is the anticipated frequency of your transactions?', 'q.4',  1, 4, 1);
INSERT INTO `anycapital`.`qm_questions` (`name`, `display_name`, `is_active`, `order_id`, `question_type_id`) VALUES ('What is your level of education?', 'q.5',  1, 5, 1);
INSERT INTO `anycapital`.`qm_questions` (`name`, `display_name`, `is_active`, `order_id`, `question_type_id`) VALUES ('What is your current employment status?', 'q.6',  1, 6, 1);
INSERT INTO `anycapital`.`qm_questions` (`name`, `display_name`, `is_active`, `order_id`, `question_type_id`) VALUES ('What is your profession?', 'q.7',  1, 7, 1);
INSERT INTO `anycapital`.`qm_questions` (`name`, `display_name`, `is_active`, `order_id`, `question_type_id`) VALUES ('The extent of your trading experience can be estimated as:', 'q.8',  1, 8, 1);
INSERT INTO `anycapital`.`qm_questions` (`name`, `display_name`, `is_active`, `order_id`, `question_type_id`) VALUES ('Have you ever traded financial derivatives such as futures, options or commodities?', 'q.9',  1, 9, 1);
INSERT INTO `anycapital`.`qm_questions` (`name`, `display_name`, `is_active`, `order_id`, `question_type_id`) VALUES ('How many trades including derivatives, options, futures and commodities did you trade in the last 12 (twelve) months?', 'q.10',  1, 10, 1);
INSERT INTO `anycapital`.`qm_questions` (`name`, `display_name`, `is_active`, `order_id`, `question_type_id`) VALUES ('What was the average monthly volume of your past transactions in the above products?', 'q.11',  1, 11, 1);
INSERT INTO `anycapital`.`qm_questions` (`name`, `display_name`, `is_active`, `order_id`, `question_type_id`) VALUES ('What is your nature of trading:', 'q.12',  1, 12, 1);
INSERT INTO `anycapital`.`qm_questions` (`name`, `display_name`, `is_active`, `order_id`, `question_type_id`) VALUES ('I am the individual that is the beneficial owner of all the income in this account', 'q.13',  1, 13, 2);
INSERT INTO `anycapital`.`qm_questions` (`name`, `display_name`, `is_active`, `order_id`, `question_type_id`) VALUES ('I am at least 18 years of age', 'q.14',  1, 14, 2);
INSERT INTO `anycapital`.`qm_questions` (`name`, `display_name`, `is_active`, `order_id`, `question_type_id`) VALUES ('I am not a U.S. citizen or a U.S. resident, including a resident alien individual or other U.S. person (IRS Definitions)', 'q.15',  1, 15, 2);
INSERT INTO `anycapital`.`qm_questions` (`name`, `display_name`, `is_active`, `order_id`, `question_type_id`) VALUES ('I am not a politician or a holder of a politically exposed position (for example: Minister, Ambassador, Presiding Judge)', 'q.16',  1, 16, 2);
INSERT INTO `anycapital`.`qm_questions` (`name`, `display_name`, `is_active`, `order_id`, `question_type_id`) VALUES ('I understand the risks of binary options trading, as stated in the Risk Disclosure Notice', 'q.17',  1, 17, 2);


INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Up To €20,000', 'q1.a1', 1, 1);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('€20,001-€50,000', 'q1.a2', 1, 2);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('€50,001-€100,000', 'q1.a3', 1, 3);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('More Than €100,000', 'q1.a4', 1, 4);

INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Employment', 'q2.a1', 2, 1);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Business', 'q2.a2', 2, 2);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Inheritance', 'q2.a3', 2, 3);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Investment', 'q2.a4', 2, 4);

INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('€0-€5,000', 'q3.a1', 3, 1);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('€5,001-€20,000', 'q3.a2', 3, 2);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('€20,001-€50,000', 'q3.a3', 3, 3);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('€50,001-€200,000', 'q3.a4', 3, 4);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('€200,001+', 'q3.a5', 3, 5);

INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Daily', 'q4.a1', 4, 1);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Weekly', 'q4.a2', 4, 2);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Monthly', 'q4.a3', 4, 3);

INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('None', 'q5.a1', 5, 1);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('High School Diploma', 'q5.a2', 5, 2);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Bachelor’s Degree', 'q5.a3', 5, 3);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Master’s Degree', 'q5.a4', 5, 4);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Doctorate', 'q5.a5', 5, 5);

INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Employed', 'q6.a1', 6, 1);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Self-Employed', 'q6.a2', 6, 2);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Retired', 'q6.a3', 6, 3);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Student', 'q6.a4', 6, 4);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Unemployed', 'q6.a5', 6, 5);

INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Accounting & Finance', 'q7.a1', 7, 1);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Administration', 'q7.a2', 7, 2);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Air, Sea & Land Transport', 'q7.a3', 7, 3);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Architecture, Building and Construction', 'q7.a4', 7, 4);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Arts & Entertainment', 'q7.a5', 7, 5);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Consulting and Business Analysis', 'q7.a6', 7, 6);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Education & Teaching', 'q7.a7', 7, 7);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Engineering', 'q7.a8', 7, 8);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Health & Medical Services', 'q7.a9', 7, 9);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Hospitality & Tourism', 'q7.a10', 7, 10);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Legal & Compliance', 'q7.a11', 7, 11);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Sales, Marketing & Advertising', 'q7.a12', 7, 12);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Science & Research', 'q7.a13', 7, 13);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Other', 'q7.a14', 7, 14);

INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('None', 'q8.a1', 8, 1);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Less Than A Year', 'q8.a2', 8, 2);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('1 To 3 Years', 'q8.a3', 8, 3);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('More Than 3 Years', 'q8.a4', 8, 4);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Professional Financial Experience', 'q8.a5', 8, 5);

INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Yes', 'q9.a1', 9, 1);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('No', 'q9.a2', 9, 2);

INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('None', 'q10.a1', 10, 1);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('1-20', 'q10.a2', 10, 2);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('21-50', 'q10.a3', 10, 3);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('51-100', 'q10.a4', 10, 4);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('More Than 100', 'q10.a5', 10, 5);

INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('€0-€20', 'q11.a1', 11, 1);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('€21-€100', 'q11.a2', 11, 2);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('€101-€500', 'q11.a3', 11, 3);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('€501+', 'q11.a4', 11, 4);

INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Investment', 'q12.a1', 12, 1);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Hedging', 'q12.a2', 12, 2);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Speculation', 'q12.a3', 12, 3);

INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Yes', 'q13.a1', 13, 1);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('No', 'q13.a2', 13, 2);

INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Yes', 'q14.a1', 14, 1);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('No', 'q14.a2', 14, 2);

INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Yes', 'q15.a1', 15, 1);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('No', 'q15.a2', 15, 2);

INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Yes', 'q16.a1', 16, 1);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('No', 'q16.a2', 16, 2);

INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('Yes', 'q17.a1', 17, 1);
INSERT INTO `anycapital`.`qm_answers` (`name`, `display_name`, `question_id`, `order_id`) VALUES ('No', 'q17.a2', 17, 2);

UPDATE `anycapital`.`qm_question_types` SET `description`='Radio button' WHERE `id`='1';

INSERT INTO `anycapital`.`qm_question_types` (`description`) VALUES ('Drop down');

UPDATE `anycapital`.`qm_questions` SET `question_type_id`='4' WHERE `id`='7';

UPDATE `anycapital`.`qm_answers` SET `score`='1' WHERE `id`='1';
UPDATE `anycapital`.`qm_answers` SET `score`='10' WHERE `id`='26';
UPDATE `anycapital`.`qm_answers` SET `score`='100' WHERE `id`='41';
UPDATE `anycapital`.`qm_answers` SET `score`='1000' WHERE `id`='47';

--------------------------------------------
-- End
-- Eyal O
-- SP-498 ODT Questionnaire + Logic
--------------------------------------------

--------------------------------------------
-- Eyal O
-- SP-450 [Technical][Regulation] - Logic for questionnaire pass or failed
--------------------------------------------

INSERT INTO `anycapital`.`regulation_questionnaire_status_score` (`score`, `status_id`) VALUES ('1111', '1');
INSERT INTO `anycapital`.`regulation_questionnaire_status_score` (`score`, `status_id`) VALUES ('101', '4');
INSERT INTO `anycapital`.`regulation_questionnaire_status_score` (`score`, `status_id`) VALUES ('1001', '4');
INSERT INTO `anycapital`.`regulation_questionnaire_status_score` (`score`, `status_id`) VALUES ('111', '4');
INSERT INTO `anycapital`.`regulation_questionnaire_status_score` (`score`, `status_id`) VALUES ('1011', '4');
INSERT INTO `anycapital`.`regulation_questionnaire_status_score` (`score`, `status_id`) VALUES ('1101', '4');
INSERT INTO `anycapital`.`regulation_questionnaire_status_score` (`score`, `status_id`) VALUES ('1110', '4');

--------------------------------------------
-- END
-- Eyal O
-- SP-450 [Technical][Regulation] - Logic for questionnaire pass or failed
--------------------------------------------
--------------------------------------------
-- START
-- Eran
-- SP-501 Blocked Countries 
--------------------------------------------
UPDATE anycapital.countries t2,
(  
SELECT id FROM anycapital.countries c where
lower(c.COUNTRY_NAME) = lower('Afghanistan') or 
lower(c.COUNTRY_NAME) = lower('Albania') or 
lower(c.COUNTRY_NAME) = lower('Algeria') or 
lower(c.COUNTRY_NAME) = lower('American Samoa') or 
lower(c.COUNTRY_NAME) = lower('Andorra') or 
lower(c.COUNTRY_NAME) = lower('Angola') or 
lower(c.COUNTRY_NAME) = lower('Anguilla') or 
lower(c.COUNTRY_NAME) = lower('Antarctica') or 
lower(c.COUNTRY_NAME) = lower('Antigua and Barbuda') or 
lower(c.COUNTRY_NAME) = lower('Argentina') or 
lower(c.COUNTRY_NAME) = lower('Armenia') or 
lower(c.COUNTRY_NAME) = lower('Aruba') or 
lower(c.COUNTRY_NAME) = lower('Australia') or 
lower(c.COUNTRY_NAME) = lower('Azerbaijan') or 
lower(c.COUNTRY_NAME) = lower('Bahamas') or 
lower(c.COUNTRY_NAME) = lower('Bahrain') or 
lower(c.COUNTRY_NAME) = lower('Bangladesh') or 
lower(c.COUNTRY_NAME) = lower('Barbados') or 
lower(c.COUNTRY_NAME) = lower('Belarus') or 
lower(c.COUNTRY_NAME) = lower('Belize') or 
lower(c.COUNTRY_NAME) = lower('Benin') or 
lower(c.COUNTRY_NAME) = lower('Bermuda') or 
lower(c.COUNTRY_NAME) = lower('Bhutan') or 
lower(c.COUNTRY_NAME) = lower('Bolivia') or 
lower(c.COUNTRY_NAME) = lower('Bosnia') or 
lower(c.COUNTRY_NAME) = lower('Botswana') or 
lower(c.COUNTRY_NAME) = lower('Bouvet Island') or 
lower(c.COUNTRY_NAME) = lower('Brazil') or 
lower(c.COUNTRY_NAME) = lower('Brunei Darussalam') or 
lower(c.COUNTRY_NAME) = lower('Burkina Faso') or 
lower(c.COUNTRY_NAME) = lower('Burundi') or 
lower(c.COUNTRY_NAME) = lower('Cambodia') or 
lower(c.COUNTRY_NAME) = lower('Cameroon') or 
lower(c.COUNTRY_NAME) = lower('Canada') or 
lower(c.COUNTRY_NAME) = lower('Cape Verde') or 
lower(c.COUNTRY_NAME) = lower('Cayman Islands') or 
lower(c.COUNTRY_NAME) = lower('Central Africa') or 
lower(c.COUNTRY_NAME) = lower('Chad') or 
lower(c.COUNTRY_NAME) = lower('Chile') or 
lower(c.COUNTRY_NAME) = lower('China') or 
lower(c.COUNTRY_NAME) = lower('Christmas Island') or 
lower(c.COUNTRY_NAME) = lower('Cocos Islands') or 
lower(c.COUNTRY_NAME) = lower('Colombia') or 
lower(c.COUNTRY_NAME) = lower('Comoros') or 
lower(c.COUNTRY_NAME) = lower('Congo') or 
lower(c.COUNTRY_NAME) = lower('Cook Islands') or 
lower(c.COUNTRY_NAME) = lower('Costa Rica') or 
lower(c.COUNTRY_NAME) = lower('Cuba') or 
lower(c.COUNTRY_NAME) = lower('Djibouti') or 
lower(c.COUNTRY_NAME) = lower('Dominica') or 
lower(c.COUNTRY_NAME) = lower('Dominican Republic') or 
lower(c.COUNTRY_NAME) = lower('East Timor') or 
lower(c.COUNTRY_NAME) = lower('Ecuador') or 
lower(c.COUNTRY_NAME) = lower('Egypt') or 
lower(c.COUNTRY_NAME) = lower('El Salvador') or 
lower(c.COUNTRY_NAME) = lower('Equatorial Guinea') or 
lower(c.COUNTRY_NAME) = lower('Eritrea') or 
lower(c.COUNTRY_NAME) = lower('Ethiopia') or 
lower(c.COUNTRY_NAME) = lower('Falkland Islands') or 
lower(c.COUNTRY_NAME) = lower('Faroe Islands') or 
lower(c.COUNTRY_NAME) = lower('Fiji') or 
lower(c.COUNTRY_NAME) = lower('French Guiana') or 
lower(c.COUNTRY_NAME) = lower('French Polynesia') or 
lower(c.COUNTRY_NAME) = lower('Gabon') or 
lower(c.COUNTRY_NAME) = lower('Gambia') or 
lower(c.COUNTRY_NAME) = lower('Georgia') or 
lower(c.COUNTRY_NAME) = lower('Ghana') or 
lower(c.COUNTRY_NAME) = lower('Gibraltar') or 
lower(c.COUNTRY_NAME) = lower('Greenland') or 
lower(c.COUNTRY_NAME) = lower('Grenada') or 
lower(c.COUNTRY_NAME) = lower('Guadeloupe') or 
lower(c.COUNTRY_NAME) = lower('Guam') or 
lower(c.COUNTRY_NAME) = lower('Guatemala') or 
lower(c.COUNTRY_NAME) = lower('Guinea') or 
lower(c.COUNTRY_NAME) = lower('Guyana') or 
lower(c.COUNTRY_NAME) = lower('Haiti') or 
lower(c.COUNTRY_NAME) = lower('Honduras') or 
lower(c.COUNTRY_NAME) = lower('Hong Kong') or 
lower(c.COUNTRY_NAME) = lower('Iceland') or 
lower(c.COUNTRY_NAME) = lower('India') or 
lower(c.COUNTRY_NAME) = lower('Indonesia') or 
lower(c.COUNTRY_NAME) = lower('Iran') or 
lower(c.COUNTRY_NAME) = lower('Iraq') or 
lower(c.COUNTRY_NAME) = lower('Israel') or 
lower(c.COUNTRY_NAME) = lower('Ivory Coast') or 
lower(c.COUNTRY_NAME) = lower('Jamaica') or 
lower(c.COUNTRY_NAME) = lower('Japan') or 
lower(c.COUNTRY_NAME) = lower('Jordan') or 
lower(c.COUNTRY_NAME) = lower('Kazakhstan') or 
lower(c.COUNTRY_NAME) = lower('Kenya') or 
lower(c.COUNTRY_NAME) = lower('Kiribati') or 
lower(c.COUNTRY_NAME) = lower('Kuwait') or 
lower(c.COUNTRY_NAME) = lower('Kyrgyzstan') or 
lower(c.COUNTRY_NAME) = lower('Laos') or 
lower(c.COUNTRY_NAME) = lower('Lebanon') or 
lower(c.COUNTRY_NAME) = lower('Lesotho') or 
lower(c.COUNTRY_NAME) = lower('Liberia') or 
lower(c.COUNTRY_NAME) = lower('Libya') or 
lower(c.COUNTRY_NAME) = lower('Macau') or 
lower(c.COUNTRY_NAME) = lower('Macedonia') or 
lower(c.COUNTRY_NAME) = lower('Madagascar') or 
lower(c.COUNTRY_NAME) = lower('Malawi') or 
lower(c.COUNTRY_NAME) = lower('Malaysia') or 
lower(c.COUNTRY_NAME) = lower('Maldives') or 
lower(c.COUNTRY_NAME) = lower('Mali') or 
lower(c.COUNTRY_NAME) = lower('Marshal Islands') or 
lower(c.COUNTRY_NAME) = lower('Martinique') or 
lower(c.COUNTRY_NAME) = lower('Mauritania') or 
lower(c.COUNTRY_NAME) = lower('Mauritius') or 
lower(c.COUNTRY_NAME) = lower('Mayotte Island') or 
lower(c.COUNTRY_NAME) = lower('Mexico') or 
lower(c.COUNTRY_NAME) = lower('Micronesia') or 
lower(c.COUNTRY_NAME) = lower('Moldova') or 
lower(c.COUNTRY_NAME) = lower('Monaco') or 
lower(c.COUNTRY_NAME) = lower('Mongolia') or 
lower(c.COUNTRY_NAME) = lower('Montenegro') or 
lower(c.COUNTRY_NAME) = lower('Montserrat') or 
lower(c.COUNTRY_NAME) = lower('Morocco') or 
lower(c.COUNTRY_NAME) = lower('Mozambique') or 
lower(c.COUNTRY_NAME) = lower('Myanmar') or 
lower(c.COUNTRY_NAME) = lower('Namibia') or 
lower(c.COUNTRY_NAME) = lower('Nauru') or 
lower(c.COUNTRY_NAME) = lower('Nepal') or 
lower(c.COUNTRY_NAME) = lower('Netherlands Antilles') or 
lower(c.COUNTRY_NAME) = lower('New Caledonia') or 
lower(c.COUNTRY_NAME) = lower('New Zealand') or 
lower(c.COUNTRY_NAME) = lower('Nicaragua') or 
lower(c.COUNTRY_NAME) = lower('Niger') or 
lower(c.COUNTRY_NAME) = lower('Nigeria') or 
lower(c.COUNTRY_NAME) = lower('Niue') or 
lower(c.COUNTRY_NAME) = lower('Norfolk Island') or 
lower(c.COUNTRY_NAME) = lower('North Korea') or 
lower(c.COUNTRY_NAME) = lower('Norway') or 
lower(c.COUNTRY_NAME) = lower('Oman') or 
lower(c.COUNTRY_NAME) = lower('Pakistan') or 
lower(c.COUNTRY_NAME) = lower('Palau') or 
lower(c.COUNTRY_NAME) = lower('Panama') or 
lower(c.COUNTRY_NAME) = lower('Papua New Guinea') or 
lower(c.COUNTRY_NAME) = lower('Paraguay') or 
lower(c.COUNTRY_NAME) = lower('Peru') or 
lower(c.COUNTRY_NAME) = lower('Philippines') or 
lower(c.COUNTRY_NAME) = lower('Pitcairn') or 
lower(c.COUNTRY_NAME) = lower('Puerto Rico') or 
lower(c.COUNTRY_NAME) = lower('Qatar') or 
lower(c.COUNTRY_NAME) = lower('Reunion Island') or 
lower(c.COUNTRY_NAME) = lower('Russia') or 
lower(c.COUNTRY_NAME) = lower('Rwanda') or 
lower(c.COUNTRY_NAME) = lower('ST. Helena') or 
lower(c.COUNTRY_NAME) = lower('Saint Kitts and Nevis') or 
lower(c.COUNTRY_NAME) = lower('Saint Lucia') or 
lower(c.COUNTRY_NAME) = lower('Samoa') or 
lower(c.COUNTRY_NAME) = lower('San Marino') or 
lower(c.COUNTRY_NAME) = lower('Saudi Arabia') or 
lower(c.COUNTRY_NAME) = lower('Senegal') or 
lower(c.COUNTRY_NAME) = lower('Serbia') or 
lower(c.COUNTRY_NAME) = lower('Seychelles') or 
lower(c.COUNTRY_NAME) = lower('Sierra Leone') or 
lower(c.COUNTRY_NAME) = lower('Singapore') or 
lower(c.COUNTRY_NAME) = lower('Solomon Islands') or 
lower(c.COUNTRY_NAME) = lower('Somalia') or 
lower(c.COUNTRY_NAME) = lower('South Korea') or 
lower(c.COUNTRY_NAME) = lower('Sri Lanka') or 
lower(c.COUNTRY_NAME) = lower('Sudan') or 
lower(c.COUNTRY_NAME) = lower('Suriname') or 
lower(c.COUNTRY_NAME) = lower('Swaziland') or 
lower(c.COUNTRY_NAME) = lower('Switzerland') or 
lower(c.COUNTRY_NAME) = lower('Syria') or 
lower(c.COUNTRY_NAME) = lower('Taiwan') or 
lower(c.COUNTRY_NAME) = lower('Tajikistan') or 
lower(c.COUNTRY_NAME) = lower('Tanzania') or 
lower(c.COUNTRY_NAME) = lower('Thailand') or 
lower(c.COUNTRY_NAME) = lower('Togo') or 
lower(c.COUNTRY_NAME) = lower('Tokelau') or 
lower(c.COUNTRY_NAME) = lower('Tonga Islands') or 
lower(c.COUNTRY_NAME) = lower('Trinidad and Tobago') or 
lower(c.COUNTRY_NAME) = lower('Tunisia') or 
lower(c.COUNTRY_NAME) = lower('Turkey') or 
lower(c.COUNTRY_NAME) = lower('Turkmenistan') or 
lower(c.COUNTRY_NAME) = lower('Tuvalu') or 
lower(c.COUNTRY_NAME) = lower('Uganda') or 
lower(c.COUNTRY_NAME) = lower('Ukraine') or 
lower(c.COUNTRY_NAME) = lower('United Arab Emirates') or 
lower(c.COUNTRY_NAME) = lower('United States') or 
lower(c.COUNTRY_NAME) = lower('Uruguay') or 
lower(c.COUNTRY_NAME) = lower('Uzbekistan') or 
lower(c.COUNTRY_NAME) = lower('Vanuato') or 
lower(c.COUNTRY_NAME) = lower('Vatican') or 
lower(c.COUNTRY_NAME) = lower('Venezuela') or 
lower(c.COUNTRY_NAME) = lower('Vietnam') or 
lower(c.COUNTRY_NAME) = lower('Virgin Islands UK') or 
lower(c.COUNTRY_NAME) = lower('Virgin Islands US') or 
lower(c.COUNTRY_NAME) = lower('Western Sahara') or 
lower(c.COUNTRY_NAME) = lower('Yemen') or 
lower(c.COUNTRY_NAME) = lower('Zaire') or 
lower(c.COUNTRY_NAME) = lower('Zambia') or 
lower(c.COUNTRY_NAME) = lower('Zimbabwe') 
) t1
SET t2.is_blocked = 1
WHERE t1.id = t2.id;
--------------------------------------------
-- END
-- Eran
-- SP-501 Blocked Countries 
--------------------------------------------

--------------------------------------------
-- Eyal O
-- Users history
--------------------------------------------

INSERT INTO `anycapital`.`users_history_actions` (`id`, `name`) VALUES ('1', 'Insert user');
INSERT INTO `anycapital`.`users_history_actions` (`id`, `name`) VALUES ('2', 'Update user');
INSERT INTO `anycapital`.`users_history_actions` (`id`, `name`) VALUES ('3', 'Update step');

UPDATE `anycapital`.`users_history_actions` SET `name`='Update additional info' WHERE `id`='2';
INSERT INTO `anycapital`.`users_history_actions` (`id`, `name`) VALUES ('4', 'Update user');
INSERT INTO `anycapital`.`users_history_actions` (`id`, `name`) VALUES ('5', 'Change password');

--------------------------------------------
-- End
-- Eyal O
-- Users history
--------------------------------------------

--------------------------------------------
-- Sendgrid
-- Eyal O
--------------------------------------------

INSERT INTO `anycapital`.`email_actions` (`id`, `name`) VALUES ('1', 'register');
INSERT INTO `anycapital`.`email_actions` (`id`, `name`) VALUES ('2', 'forget password');
INSERT INTO `anycapital`.`email_actions` (`id`, `name`) VALUES ('3', 'market daily history job');
INSERT INTO `anycapital`.`email_actions` (`id`, `name`) VALUES ('4', 'product settle');
INSERT INTO `anycapital`.`email_actions` (`id`, `name`) VALUES ('5', 'change product status');
INSERT INTO `anycapital`.`email_actions` (`id`, `name`) VALUES ('6', 'daily report');

INSERT INTO `anycapital`.`email_templates` (`id`, `action_id`, `language_id`, `template_id`) VALUES ('5', '1', '2', '90633575-f226-4690-ac8f-3085bb8e5d88');
INSERT INTO `anycapital`.`email_templates` (`action_id`, `language_id`, `template_id`) VALUES ('2', '2', 'b74135ae-96e0-477b-8530-b6670dfe6765');

UPDATE `anycapital`.`email_templates` SET `template_id`='da9bf98e-7977-4e15-9d31-09a67ca681a1' WHERE `id`='6';

--------------------------------------------
-- End
-- Sendgrid
-- Eyal O
--------------------------------------------

--------------------------------------------
-- Analytics
-- Eyal O
--------------------------------------------

INSERT INTO `anycapital`.`db_parameters` (`NAME`, `NUM_VALUE`, `COMMENTS`) VALUES ('HC TTL analytics', '600', 'Time to left analytics info in hazelcast');

--------------------------------------------
-- END
-- Analytics
-- Eyal O
--------------------------------------------

--------------------------------------------
-- Eyal O
-- market place
--------------------------------------------
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='NYSE' WHERE `ID`='1';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='London Stock Exchange' WHERE `ID`='2';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='Tokyo Stock Exchange' WHERE `ID`='3';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='Xetra' WHERE `ID`='4';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='Euronext Paris' WHERE `ID`='5';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='NYSE' WHERE `ID`='6';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='NASDAQ' WHERE `ID`='7';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='NASDAQ' WHERE `ID`='8';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='NYSE' WHERE `ID`='9';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='Nasdaq' WHERE `ID`='10';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='NYSE' WHERE `ID`='11';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='Nasdaq' WHERE `ID`='12';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='Xetra' WHERE `ID`='13';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='Xetra' WHERE `ID`='14';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='Xetra' WHERE `ID`='15';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='NASDAQ' WHERE `ID`='16';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='Xetra' WHERE `ID`='17';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='NYSE' WHERE `ID`='18';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='NASDAQ' WHERE `ID`='19';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='NASDAQ' WHERE `ID`='20';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='NYSE' WHERE `ID`='21';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='NYSE' WHERE `ID`='22';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='NYSE' WHERE `ID`='23';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='NYSE' WHERE `ID`='24';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='NYSE' WHERE `ID`='25';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='NYSE' WHERE `ID`='26';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='NYSE' WHERE `ID`='27';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='NASDAQ' WHERE `ID`='28';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='NASDAQ' WHERE `ID`='29';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='London Stock Exchange' WHERE `ID`='30';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='London Stock Exchange' WHERE `ID`='31';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='Madrid Stock Exchange' WHERE `ID`='32';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='NYSE' WHERE `ID`='33';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='Euronext Paris' WHERE `ID`='34';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='NYSE' WHERE `ID`='35';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='NASDAQ' WHERE `ID`='36';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='NYSE' WHERE `ID`='37';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='NYSE' WHERE `ID`='38';
UPDATE `anycapital`.`markets` SET `MARKET_PLACE`='OTC' WHERE `ID`='39';
--------------------------------------------
-- End
-- Eyal O
-- market place
--------------------------------------------

--------------------------------------------
-- Eyal O
-- CRM permissions
--------------------------------------------

INSERT INTO `anycapital`.`be_spaces` (`NAME`) VALUES ('administrator');

INSERT INTO `anycapital`.`writer_groups` (`NAME`) VALUES ('trader');
INSERT INTO `anycapital`.`writer_groups` (`NAME`) VALUES ('content');

INSERT INTO `anycapital`.`writers` (`USER_NAME`, `PASSWORD`, `FIRST_NAME`, `LAST_NAME`, `EMAIL`, `UTC_OFFSET`, `GROUP_ID`) VALUES ('admin', '123456', 'admin', 'admin', 'admin@anycapital.com', '-180', '1');
INSERT INTO `anycapital`.`writers` (`USER_NAME`, `PASSWORD`, `FIRST_NAME`, `LAST_NAME`, `EMAIL`, `UTC_OFFSET`, `GROUP_ID`) VALUES ('trader', '123456', 'trader', 'trader', 'trader@anycapital.com', '-180', '3');
INSERT INTO `anycapital`.`writers` (`USER_NAME`, `PASSWORD`, `FIRST_NAME`, `LAST_NAME`, `EMAIL`, `UTC_OFFSET`, `GROUP_ID`) VALUES ('content', '123456', 'content', 'content', 'content@anycapital.com', '-180', '4');

ALTER TABLE `anycapital`.`be_screens` 
CHANGE COLUMN `NAME` `NAME` VARCHAR(50) NOT NULL ;

UPDATE `anycapital`.`be_screens` SET `NAME`='products products' WHERE `ID`='1';
INSERT INTO `anycapital`.`be_screens` (`NAME`, `SPACE_ID`) VALUES ('products priority', '3');
UPDATE `anycapital`.`be_screens` SET `NAME`='content translations' WHERE `ID`='2';
INSERT INTO `anycapital`.`be_screens` (`NAME`, `SPACE_ID`) VALUES ('administrator investments investments', '5');
INSERT INTO `anycapital`.`be_screens` (`NAME`, `SPACE_ID`) VALUES ('administrator transactions transactions', '5');
INSERT INTO `anycapital`.`be_screens` (`NAME`, `SPACE_ID`) VALUES ('administrator pw first', '5');
INSERT INTO `anycapital`.`be_screens` (`NAME`, `SPACE_ID`) VALUES ('administrator pw second', '5');
UPDATE `anycapital`.`be_screens` SET `NAME`='users' WHERE `ID`='9';
UPDATE `anycapital`.`be_screens` SET `NAME`='users_user' WHERE `ID`='10';
INSERT INTO `anycapital`.`be_screens` (`NAME`, `SPACE_ID`) VALUES ('users client space', '1');
INSERT INTO `anycapital`.`be_screens` (`NAME`, `SPACE_ID`) VALUES ('users edit', '1');
INSERT INTO `anycapital`.`be_screens` (`NAME`, `SPACE_ID`) VALUES ('users issues', '1');
INSERT INTO `anycapital`.`be_screens` (`NAME`, `SPACE_ID`) VALUES ('users answers', '1');
INSERT INTO `anycapital`.`be_screens` (`NAME`, `SPACE_ID`) VALUES ('users files', '1');
INSERT INTO `anycapital`.`be_screens` (`NAME`, `SPACE_ID`) VALUES ('users regulation', '1');
INSERT INTO `anycapital`.`be_screens` (`NAME`, `SPACE_ID`) VALUES ('users_deposit administrator', '1');
UPDATE `anycapital`.`be_screens` SET `NAME`='users_change_password' WHERE `ID`='4';
UPDATE `anycapital`.`be_screens` SET `NAME`='users_transactions' WHERE `ID`='3';
UPDATE `anycapital`.`be_screens` SET `NAME`='users_investments' WHERE `ID`='13';
UPDATE `anycapital`.`be_screens` SET `NAME`='users_deposit wire' WHERE `ID`='11';
UPDATE `anycapital`.`be_screens` SET `NAME`='users_withdraw wire' WHERE `ID`='12';
INSERT INTO `anycapital`.`be_screens` (`NAME`, `SPACE_ID`) VALUES ('users withdrawal administrator', '1');

--------------------------------------------
-- End
-- Eyal O
-- CRM permissions
--------------------------------------------

--------------------------------------------
-- Eyal O
-- email insert investment
--------------------------------------------

INSERT INTO `anycapital`.`email_actions` (`id`, `name`) VALUES ('7', 'insert investment open');
INSERT INTO `anycapital`.`email_actions` (`id`, `name`) VALUES ('8', 'insert investment pending');

INSERT INTO `anycapital`.`email_templates` (`action_id`, `language_id`, `template_id`) VALUES ('8', '2', 'cc84f3dd-bc31-4fc0-b561-e61f2dda6eff');
INSERT INTO `anycapital`.`email_templates` (`action_id`, `language_id`, `template_id`) VALUES ('7', '2', 'cf51238c-0758-4ea8-bf8e-246003fc3979');


--------------------------------------------
-- END
-- Eyal O
-- email insert investment
--------------------------------------------
--------------------------------------------
-- START
-- Eran
-- writers group permissions
--------------------------------------------
-- insert admin writer_group_permissions

-- products
	-- products (view/add/edit)
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 1, 1);
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 1, 2);
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 1, 3);
	-- product-priorty
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 2, 1);

-- content
	-- translations (view/add/edit)
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 3, 1);
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 3, 2);
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 3, 3);

-- administrator
	-- investments
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 4, 1);
	-- transactions
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 5, 1);
	-- pending-withdraws-fa
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 6, 1);
	-- pending-withdraws-sa
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 7, 1);
-- marketing
	-- marketing-sources (view/add/edit)
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 8, 1);
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 8, 2);
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 8, 3);
	-- marketing-lps (view/add/edit)
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 9, 1);
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 9, 2);
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 9, 3);
	-- marketing-contents (view/add/edit)
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 10, 1);
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 10, 2);
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 10, 3);
	-- marketing-campaigns (view/add/edit)
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 11, 1);
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 11, 2);
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 11, 3);
-- users
	-- users (view/add/edit)
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 12, 1);
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 12, 2);
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 12, 3);
	-- user-issues (view/add)
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 13, 1);
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 13, 3);
	-- user-questionnaire 
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 14, 1);
	-- files (view/add/edit) 
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 15, 1);
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 15, 2);
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 15, 3);
	-- regulation
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 16, 1);
	-- user-change-pass
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 17, 1);
	-- user-transactions
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 18, 1);
	-- user-investments
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 19, 1);
	-- user-wire-deposit
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 20, 1);
	-- user-admin-deposit
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 21, 1);
	-- user-wire-withdraw
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 22, 1);
	-- user-admin-withdraw
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 23, 1);
	-- client-space
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 24, 1);
--------------------------------------------
-- END
-- Eran
-- writers group permissions
--------------------------------------------

--------------------------------------------
-- Eyal O
-- email templates
--------------------------------------------

UPDATE `anycapital`.`email_templates` SET `template_id`='60acb4ca-37b8-44d7-b533-5540e787186a' WHERE `id`='5';
UPDATE `anycapital`.`email_templates` SET `template_id`='dc7cb311-be82-4670-98b7-cd610584c842' WHERE `id`='6';
UPDATE `anycapital`.`email_templates` SET `template_id`='105bd4c1-368d-4a8d-b175-e4790e12c518' WHERE `id`='7';
UPDATE `anycapital`.`email_templates` SET `template_id`='67729213-bd3e-4a33-902c-aade1010fee8' WHERE `id`='8';

UPDATE `anycapital`.`db_parameters` SET `STRING_VALUE`='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' WHERE `ID`='7';

--------------------------------------------
-- END
-- Eyal O
-- email templates
--------------------------------------------
--------------------------------------------
-- Eran 
-- db parameters screen
-- START
--------------------------------------------
INSERT INTO `anycapital`.`db_parameters` (`ID`, `NAME`, `STRING_VALUE`, `COMMENTS`) VALUES ('20', 'documents_email', 'documents@anycapital.com', 'Documents email');

INSERT INTO `anycapital`.`be_screens` (`NAME`, `SPACE_ID`) VALUES ('db-parameters', '5');    
-- db-parameters (add/edit/view)
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 25, 1);
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 25, 2);
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 25, 3);
--------------------------------------------
-- Eran 
-- db parameters screen
-- END
--------------------------------------------
--------------------------------------------
-- Eran 
-- writers screen
-- START
--------------------------------------------
INSERT INTO `anycapital`.`be_screens` (`NAME`, `SPACE_ID`) VALUES ('writers', '5');    
-- writers (add/edit/view)
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 26, 1);
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 26, 2);
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 26, 3);
--------------------------------------------
-- Eran 
-- writers screen
-- END
--------------------------------------------
---------------------------------------------
-- 15.08.2017
-- Eyal O
-- Email Action
---------------------------------------------

ALTER TABLE `anycapital`.`email_actions` 
ADD COLUMN `display_name` VARCHAR(60) NULL AFTER `name`,
ADD COLUMN `class_path` VARCHAR(100) NULL COMMENT 'path for handler class' AFTER `display_name`,
ADD COLUMN `is_display` BINARY NULL DEFAULT 1 COMMENT 'Display on CRM when equals to 1' AFTER `class_path`;

update 
	email_actions ea
set
	ea.is_display = 0
where
	ea.id in (3,4,5,6);
	
update 
	email_actions ea
set
	ea.display_name = 'email.action.register',
	ea.class_path = 'capital.any.service.base.emailAction.handler.RegisterEmailActionHandler'
where
	ea.id = 1;
	
update 
	email_actions ea
set
	ea.display_name = 'email.action.forget.password',
	ea.class_path = 'capital.any.service.base.emailAction.handler.ForgetPasswordEmailActionHandler'
where
	ea.id = 2;
	
update 
	email_actions ea
set
	ea.display_name = 'email.action.investment.open',
	ea.class_path = 'capital.any.service.base.emailAction.handler.InvestmentOpenEmailActionHandler'
where
	ea.id = 7;
	
update 
	email_actions ea
set
	ea.display_name = 'email.action.investment.pending',
	ea.class_path = 'capital.any.service.base.emailAction.handler.InvestmentPendingEmailActionHandler'
where
	ea.id = 8;

UPDATE `anycapital`.`email_actions` SET `class_path`='capital.any.service.base.emailAction.handler.InvestmentOpenEmailActionHandler' WHERE `id`='7';
UPDATE `anycapital`.`email_actions` SET `class_path`='capital.any.service.base.emailAction.handler.InvestmentPendingEmailActionHandler' WHERE `id`='8';

---------------------------------------------
-- END.
-- Eyal O
-- Email Action
---------------------------------------------
--------------------------------------------
-- 15-08-2017
-- Eran 
-- writers change pass screen
-- START
--------------------------------------------
INSERT INTO `anycapital`.`be_screens` (`NAME`, `SPACE_ID`) VALUES ('writer-change-pass', '5');    
-- writers change pass screen - add
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 27, 1);
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (2, 27, 1);
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (3, 27, 1);
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (4, 27, 1);
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (5, 27, 1);
--------------------------------------------
-- Eran 
-- writers screen
-- END
--------------------------------------------
--------------------------------------------
-- 15-08-2017
-- Eran 
-- templates
-- START
--------------------------------------------
INSERT INTO `anycapital`.`be_screens` (`NAME`, `SPACE_ID`) VALUES ('templates', '1');
-- templates view
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 28, 1);
--------------------------------------------
-- 15-08-2017
-- Eran 
-- templates
-- END
--------------------------------------------
-------------------------------------------------------------
-- Eyal O
-- email template for transactions
-------------------------------------------------------------

UPDATE `anycapital`.`email_actions` SET `name`='Deposit confirmation bank wire', `class_path`='capital.any.service.base.emailAction.handler.TransactionWireEmailActionHandler' WHERE `id`='10';

-------------------------------------------------------------
-- END
-- Eyal O
-- email template for transactions
-------------------------------------------------------------

--------------------------------------------
-- 26-09-2017
-- Eran 
-- Admin panel
-- START
--------------------------------------------
INSERT INTO `anycapital`.`be_screens` (`NAME`, `SPACE_ID`) VALUES ('admin-panel', '5');    
-- admin panel (view)
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 30, 1);
--------------------------------------------
-- 26-09-2017
-- Eran 
-- Admin panel
-- END
-------------------------------------------------------------

-------------------------------------------------------------
-- Eyal O
-- upload docs via web & add status for files
-- 26-09-2017
-------------------------------------------------------------

-- file_statuses
CREATE TABLE `anycapital`.`file_statuses` (
  `id` SMALLINT(6) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `display_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));
  
INSERT INTO `anycapital`.`file_statuses` (`id`, `name`, `display_name`) VALUES ('1', 'Waiting For Approve', 'file.status.1');
INSERT INTO `anycapital`.`file_statuses` (`id`, `name`, `display_name`) VALUES ('2', 'Rejected', 'file.status.2');
INSERT INTO `anycapital`.`file_statuses` (`id`, `name`, `display_name`) VALUES ('3', 'Approved', 'file.status.3');

ALTER TABLE `anycapital`.`file_statuses` 
ADD COLUMN `rank` SMALLINT(6) NOT NULL AFTER `display_name`;

UPDATE `anycapital`.`file_statuses` SET `rank`='1' WHERE `id`='3';
UPDATE `anycapital`.`file_statuses` SET `rank`='2' WHERE `id`='1';
UPDATE `anycapital`.`file_statuses` SET `rank`='3' WHERE `id`='2';

-- file_groups
CREATE TABLE `anycapital`.`file_groups` (
  `id` SMALLINT(6) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));

INSERT INTO `anycapital`.`file_groups` (`id`, `name`) VALUES ('1', 'Group 1');
INSERT INTO `anycapital`.`file_groups` (`id`, `name`) VALUES ('2', 'Group 2');

ALTER TABLE `anycapital`.`file_groups` 
ADD COLUMN `display_name` VARCHAR(45) NOT NULL AFTER `name`;

UPDATE `anycapital`.`file_groups` SET `display_name`='file.group.1' WHERE `id`='1';
UPDATE `anycapital`.`file_groups` SET `display_name`='file.group.2' WHERE `id`='2';

-- file_group_types
CREATE TABLE `anycapital`.`file_group_types` (
  `id` SMALLINT(6) NOT NULL AUTO_INCREMENT,
  `group_id` SMALLINT(6) NOT NULL,
  `type_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_file_group_idx` (`group_id` ASC),
  INDEX `fk_file_type_idx` (`type_id` ASC),
  CONSTRAINT `fk_file_group`
    FOREIGN KEY (`group_id`)
    REFERENCES `anycapital`.`file_groups` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_file_type`
    FOREIGN KEY (`type_id`)
    REFERENCES `anycapital`.`file_types` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
INSERT INTO `anycapital`.`file_group_types` (`group_id`, `type_id`) VALUES ('1', '1');
INSERT INTO `anycapital`.`file_group_types` (`group_id`, `type_id`) VALUES ('1', '2');
INSERT INTO `anycapital`.`file_group_types` (`group_id`, `type_id`) VALUES ('1', '3');
INSERT INTO `anycapital`.`file_group_types` (`group_id`, `type_id`) VALUES ('2', '4');

-- files
ALTER TABLE `anycapital`.`files` 
ADD COLUMN `updated_by_writer_id` SMALLINT(6) NULL DEFAULT NULL AFTER `status_id`,
ADD INDEX `updated_by_writer_files_fk_idx` (`updated_by_writer_id` ASC);

ALTER TABLE `anycapital`.`files` 
ADD COLUMN `status_id` SMALLINT(6) NOT NULL DEFAULT 1 AFTER `is_primary`;

ALTER TABLE `anycapital`.`files` 
DROP FOREIGN KEY `writer_files_fk`;
ALTER TABLE `anycapital`.`files` 
CHANGE COLUMN `writer_id` `writer_id` SMALLINT(4) NULL DEFAULT NULL COMMENT 'File added by this writer' ;
ALTER TABLE `anycapital`.`files` 
ADD CONSTRAINT `writer_files_fk`
  FOREIGN KEY (`writer_id`)
  REFERENCES `anycapital`.`writers` (`ID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


-------------------------------------------------------------
-- Eyal O
-- upload docs via web & add status for files
-- 26-09-2017
-------------------------------------------------------------

-------------------------------------------------------------
-- Eyal O
-- Questionnaire group
-- 26-09-2017
-------------------------------------------------------------

CREATE TABLE `anycapital`.`qm_question_groups` (
  `id` SMALLINT(6) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `display_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));
  
INSERT INTO `anycapital`.`qm_question_groups` (`id`, `name`, `display_name`) VALUES ('1', 'general', 'question.group.1');

ALTER TABLE `anycapital`.`qm_questions` 
ADD COLUMN `question_group_id` SMALLINT(6) NOT NULL AFTER `question_type_id`,
ADD INDEX `fk_question_group_idx` (`question_group_id` ASC);
ALTER TABLE `anycapital`.`qm_questions` 
ADD CONSTRAINT `fk_question_group`
  FOREIGN KEY (`question_group_id`)
  REFERENCES `anycapital`.`qm_question_groups` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
  
UPDATE `anycapital`.`qm_questions` SET `question_group_id`='1' WHERE `id`='1';
UPDATE `anycapital`.`qm_questions` SET `question_group_id`='1' WHERE `id`='2';
UPDATE `anycapital`.`qm_questions` SET `question_group_id`='1' WHERE `id`='3';
UPDATE `anycapital`.`qm_questions` SET `question_group_id`='1' WHERE `id`='4';
UPDATE `anycapital`.`qm_questions` SET `question_group_id`='1' WHERE `id`='5';
UPDATE `anycapital`.`qm_questions` SET `question_group_id`='1' WHERE `id`='6';
UPDATE `anycapital`.`qm_questions` SET `question_group_id`='1' WHERE `id`='7';
UPDATE `anycapital`.`qm_questions` SET `question_group_id`='1' WHERE `id`='8';
UPDATE `anycapital`.`qm_questions` SET `question_group_id`='1' WHERE `id`='9';
UPDATE `anycapital`.`qm_questions` SET `question_group_id`='1' WHERE `id`='10';
UPDATE `anycapital`.`qm_questions` SET `question_group_id`='1' WHERE `id`='11';
UPDATE `anycapital`.`qm_questions` SET `question_group_id`='1' WHERE `id`='12';
UPDATE `anycapital`.`qm_questions` SET `question_group_id`='1' WHERE `id`='13';
UPDATE `anycapital`.`qm_questions` SET `question_group_id`='1' WHERE `id`='14';
UPDATE `anycapital`.`qm_questions` SET `question_group_id`='1' WHERE `id`='15';
UPDATE `anycapital`.`qm_questions` SET `question_group_id`='1' WHERE `id`='16';
UPDATE `anycapital`.`qm_questions` SET `question_group_id`='1' WHERE `id`='17';


-------------------------------------------------------------
-- END
-- Eyal O
-- Questionnaire group
-- 26-09-2017
-------------------------------------------------------------
--------------------------------------------
-- 26-09-2017
-- Eran 
-- marketing content
-- START
--------------------------------------------
ALTER TABLE `anycapital`.`marketing_contents` 
ADD COLUMN `HTML_FILE_PATH` VARCHAR(70) NULL AFTER `TIME_MODIFIED`;
--------------------------------------------
-- 26-09-2017
-- Eran 
-- marketing contents
-- END
--------------------------------------------

--------------------------------------------
-- 18-10-2017
-- Eyal O 
-- languages
-- Start
--------------------------------------------
-- languages - de & nl
INSERT INTO `anycapital`.`languages` (`ID`, `CODE`, `DISPLAY_NAME`, `DEFAULT_COUNTRY_ID`) VALUES ('3', 'nl', 'languages.nl', '133');
INSERT INTO `anycapital`.`languages` (`ID`, `CODE`, `DISPLAY_NAME`, `DEFAULT_COUNTRY_ID`) VALUES ('1', 'de', 'languages.de', '86');

update 
	anycapital.msg_res_language mrl
set
	mrl.MSG_RES_LANGUAGE_ID = 1
where
	mrl.MSG_RES_LANGUAGE_ID = 8;
	
update 
	anycapital.msg_res_language_history mrlh
set
	mrlh.LANGUAGE_ID = 1
where
	mrlh.LANGUAGE_ID = 8;
	
update
	anycapital.users u
set
	u.LANGUAGE_ID = 1
where 
	u.LANGUAGE_ID = 8;
	
update
	anycapital.contacts c
set
	c.LANGUAGE_ID = 1
where 
	c.LANGUAGE_ID = 8;
	
UPDATE `anycapital`.`countries` SET `DEFAULT_LANGUAGE_ID`='1' WHERE `ID`='86';
	
DELETE FROM `anycapital`.`languages` WHERE `ID`='8';

UPDATE `anycapital`.`countries` SET `DEFAULT_LANGUAGE_ID`='3' WHERE `ID`='133';

-- languages - is_active
ALTER TABLE `anycapital`.`languages` 
ADD COLUMN `is_active` BINARY NOT NULL DEFAULT 1 AFTER `DEFAULT_COUNTRY_ID`;

UPDATE `anycapital`.`languages` SET `is_active`='0' WHERE `ID`='3';

-- marketing_landing_pages
UPDATE `anycapital`.`marketing_landing_pages` SET `PATH`='' WHERE `ID`='1';
UPDATE `anycapital`.`marketing_landing_pages` SET `PATH`='register' WHERE `ID`='2';
UPDATE `anycapital`.`marketing_landing_pages` SET `PATH`='login' WHERE `ID`='3';
UPDATE `anycapital`.`marketing_landing_pages` SET `PATH`='products' WHERE `ID`='4';

-- foreign key for contacts
ALTER TABLE `anycapital`.`contacts` 
ADD INDEX `contacts_ibfk_7_idx` (`LANGUAGE_ID` ASC);
ALTER TABLE `anycapital`.`contacts` 
ADD CONSTRAINT `contacts_ibfk_7`
  FOREIGN KEY (`LANGUAGE_ID`)
  REFERENCES `anycapital`.`languages` (`ID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

  -- email_templates for DE
-- #######    For test    ##########
-- # INSERT INTO `anycapital`.`email_templates` (`action_id`, `language_id`, `template_id`, `is_active`) VALUES ('1', '1', '92bc2b5a-7041-46cb-97e6-0d6bd0f398cb', '1');
-- # INSERT INTO `anycapital`.`email_templates` (`action_id`, `language_id`, `template_id`, `is_active`) VALUES ('2', '1', '524f0f67-eaf8-4668-b18b-5b8de8a3790c', '1');
-- # INSERT INTO `anycapital`.`email_templates` (`action_id`, `language_id`, `template_id`, `is_active`) VALUES ('8', '1', 'd57802f8-975c-444d-9627-32a2d75c9cf9', '1');
-- # INSERT INTO `anycapital`.`email_templates` (`action_id`, `language_id`, `template_id`, `is_active`) VALUES ('7', '1', '52e323d1-a381-4b6c-bfca-7cc47b6393ec', '1');
-- # INSERT INTO `anycapital`.`email_templates` (`action_id`, `language_id`, `template_id`, `is_active`) VALUES ('9', '1', 'bd542918-f1eb-43c5-9207-9458bd957906', '1');
-- # INSERT INTO `anycapital`.`email_templates` (`action_id`, `language_id`, `template_id`, `is_active`) VALUES ('10', '1', 'b512cbe4-a4b5-4c48-bacb-e33b0b7ea3cf', '1');
-- # INSERT INTO `anycapital`.`email_templates` (`action_id`, `language_id`, `template_id`, `is_active`) VALUES ('11', '1', '55698896-fe19-47d2-95fb-2decaae4fe60', '1');
-- # INSERT INTO `anycapital`.`email_templates` (`action_id`, `language_id`, `template_id`, `is_active`) VALUES ('12', '1', '0394762d-78af-4db9-b931-a70c8dd5e03f', '1');
-- # INSERT INTO `anycapital`.`email_templates` (`action_id`, `language_id`, `template_id`, `is_active`) VALUES ('13', '1', '68171892-ba4b-4bde-a94e-92f1cb9660f0', '1');

-- For production
INSERT INTO `anycapital`.`email_templates` (`action_id`, `language_id`, `template_id`, `is_active`) VALUES ('1', '1', 'f37e398e-eb1e-4975-90aa-ca5a3875f1ef', '1');
INSERT INTO `anycapital`.`email_templates` (`action_id`, `language_id`, `template_id`, `is_active`) VALUES ('2', '1', '17276f62-2b9d-4159-a961-49504d4e0b83', '1');
INSERT INTO `anycapital`.`email_templates` (`action_id`, `language_id`, `template_id`, `is_active`) VALUES ('8', '1', 'e68b7999-d3e6-40cf-bf8d-b15d1f4c917e', '1');
INSERT INTO `anycapital`.`email_templates` (`action_id`, `language_id`, `template_id`, `is_active`) VALUES ('7', '1', '69e8e383-8b66-496d-8f1f-64abd3d30806', '1');
INSERT INTO `anycapital`.`email_templates` (`action_id`, `language_id`, `template_id`, `is_active`) VALUES ('9', '1', 'f1053a07-de96-42f5-a8c9-ee2e2c0bb748', '1');
INSERT INTO `anycapital`.`email_templates` (`action_id`, `language_id`, `template_id`, `is_active`) VALUES ('10', '1', '7e73604b-5f92-462a-8bb0-2d9686fc9182', '1');
INSERT INTO `anycapital`.`email_templates` (`action_id`, `language_id`, `template_id`, `is_active`) VALUES ('11', '1', '1f1ab230-0467-412e-a6c1-89f793a7108d', '1');
INSERT INTO `anycapital`.`email_templates` (`action_id`, `language_id`, `template_id`, `is_active`) VALUES ('12', '1', 'e486c646-c5af-4856-a49e-819f2fc0ecb9', '1');
INSERT INTO `anycapital`.`email_templates` (`action_id`, `language_id`, `template_id`, `is_active`) VALUES ('13', '1', '20a7dacd-7f4a-45f7-b188-6d00f996a71a', '1');

--------------------------------------------
-- 18-10-2017
-- Eyal O 
-- languages
-- End
--------------------------------------------
  
--------------------------------------------
-- 29-10-2017
-- Eyal O 
-- Move action source & msg res type to DB.
-- Start
--------------------------------------------

ALTER TABLE `anycapital`.`msg_res_type` 
ADD COLUMN `DISPLAY_NAME` VARCHAR(45) NOT NULL AFTER `NAME`;

UPDATE `anycapital`.`msg_res_type` SET `DISPLAY_NAME`='msg.res.type.regular' WHERE `ID`='1';
UPDATE `anycapital`.`msg_res_type` SET `DISPLAY_NAME`='msg.res.type.large.value' WHERE `ID`='2';

ALTER TABLE `anycapital`.`action_source` 
ADD COLUMN `DISPLAY_NAME` VARCHAR(45) NOT NULL AFTER `NAME`;

UPDATE `anycapital`.`action_source` SET `DISPLAY_NAME`='action.source.web' WHERE `ID`='1';
UPDATE `anycapital`.`action_source` SET `DISPLAY_NAME`='action.source.crm' WHERE `ID`='2';
UPDATE `anycapital`.`action_source` SET `DISPLAY_NAME`='action.source.job' WHERE `ID`='3';

--------------------------------------------
-- 29-10-2017
-- Eyal O 
-- Move action source & msg res type to DB.
-- End
--------------------------------------------
--------------------------------------------
-- 30-10-2017
-- Eran 
-- Bulk translations
-- START
--------------------------------------------
INSERT INTO `anycapital`.`be_screens` (`NAME`, `SPACE_ID`) VALUES ('bulk-translations', '2');    
-- admin panel (view)
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 31, 1);
--------------------------------------------
-- 30-10-2017
-- Eran 
-- Bulk translations
-- END
--------------------------------------------

--------------------------------------------
-- 02-11-2017
-- Eran 
-- contact types
-- START
--------------------------------------------
INSERT INTO `anycapital`.`contact_types` (`ID`, `NAME`) VALUES ('3', 'Email form');
INSERT INTO `anycapital`.`contact_types` (`ID`, `NAME`) VALUES ('4', 'Short form');

ALTER TABLE `anycapital`.`contacts` 
ADD COLUMN `MARKETING_TRACKING_ID` BIGINT NULL AFTER `COMMENTS`,
ADD INDEX `contacts_ibfk_7_idx1` (`MARKETING_TRACKING_ID` ASC);

--------------------------------------------
-- 02-11-2017
-- Eran 
-- contact types
-- END
--------------------------------------------
--------------------------------------------
-- 06-11-2017
-- Eran 
-- upload contacts
-- START
--------------------------------------------
INSERT INTO `anycapital`.`contact_types` (`ID`, `NAME`) VALUES ('5', 'Imported');
INSERT INTO `anycapital`.`be_screens` (`NAME`, `SPACE_ID`) VALUES ('upload-contacts', '4');    
-- upload contacts (view)
INSERT INTO `anycapital`.`writer_group_permissions` (`GROUP_ID`, `BE_SCREEN_ID`, `BE_PERMISSION_ID`) VALUES (1, 32, 1);
--------------------------------------------
-- 06-11-2017
-- Eran 
-- upload contacts
-- END
-------------------------------------------------------------

------------------------------------------
-- START
-- email High risk products.
-- Eyal O
------------------------------------------

INSERT INTO `anycapital`.`email_actions` (`name`, `display_name`, `class_path`) VALUES ('High risk products', 'email.action.high.risk.products', 'capital.any.service.base.emailAction.handler.GeneralEmailActionHandler');

INSERT INTO `anycapital`.`email_templates` (`action_id`, `language_id`, `template_id`) VALUES ('14', '2', '4134bc4d-c3c9-45bb-bfcc-55f60dc7dca3');
INSERT INTO `anycapital`.`email_templates` (`action_id`, `language_id`, `template_id`) VALUES ('14', '1', '5ad52e32-c9ca-4b64-ac66-5e01fecde039');

------------------------------------------
-- START
-- email High risk products.
-- Eyal O
------------------------------------------
------------------------------------------
-- START
-- Add KID to product.
-- Eyal O
------------------------------------------

ALTER TABLE `anycapital`.`products` 
ADD COLUMN `KID_NAME` VARCHAR(100) NULL AFTER `PRODUCT_MATURITY_ID`;

INSERT INTO `anycapital`.`products_history_actions` (`NAME`) VALUES ('update product kid');

------------------------------------------
-- END
-- Add KID to product.
-- Eyal O
------------------------------------------