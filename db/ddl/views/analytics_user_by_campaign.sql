CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `anycapital`@`%` 
    SQL SECURITY DEFINER
VIEW `analytics_user_by_campaign` AS
    SELECT 
        `mt`.`CAMPAIGN_ID` AS `CAMPAIGN_ID`,
        `mc`.`NAME` AS `CAMPAIGN_NAME`,
        COUNT(`u`.`ID`) AS `count_users`
    FROM
        (((`users` `u`
        JOIN `marketing_attribution` `ma`)
        JOIN `marketing_tracking` `mt`)
        JOIN `marketing_campaigns` `mc`)
    WHERE
        ((`u`.`ID` = `ma`.`REFERENCE_ID`)
            AND (`ma`.`TABLE_ID` = 3)
            AND (`ma`.`TRACKING_ID` = `mt`.`ID`)
            AND (`mt`.`CAMPAIGN_ID` = `mc`.`ID`)
            AND (CAST(`u`.`TIME_CREATED` AS DATE) = CURDATE())
            AND (`u`.`CLASS_ID` <> 1))
    GROUP BY `mt`.`CAMPAIGN_ID`