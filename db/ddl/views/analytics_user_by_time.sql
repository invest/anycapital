CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `anycapital`@`%` 
    SQL SECURITY DEFINER
VIEW `analytics_user_by_time` AS
    SELECT 
        HOUR(`u`.`TIME_CREATED`) AS `time_of_day`,
        COUNT(`u`.`ID`) AS `count_users`
    FROM
        `users` `u`
    WHERE
        (CAST(`u`.`TIME_CREATED` AS DATE) = CURDATE()
			AND (`u`.`CLASS_ID` <> 1))
    GROUP BY HOUR(`u`.`TIME_CREATED`)