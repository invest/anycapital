CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `anycapital`@`%` 
    SQL SECURITY DEFINER
VIEW `analytics_login_by_time` AS
    SELECT 
        HOUR(`l`.`TIME_CREATED`) AS `time_of_day`,
        COUNT(`l`.`ID`) AS `count_logins`
    FROM
        (`logins` `l`
        JOIN `users` `u`)
    WHERE
        ((`u`.`ID` = `l`.`USER_ID`)
            AND (`u`.`CLASS_ID` <> 1)
            AND (CAST(`l`.`TIME_CREATED` AS DATE) = CURDATE()))
    GROUP BY HOUR(`l`.`TIME_CREATED`)