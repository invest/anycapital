USE `anycapital`;
CREATE  OR REPLACE VIEW `analytics_investment_by_product` AS
select
	p.product_type_id,
    pt.DISPLAY_NAME as product_type_name,
	count(i.id) as investment_count,
    ROUND((SUM(`i`.`AMOUNT`) / 100), 2) AS `investment_sum`
from
	investments i,
    users u,
    products p,
    product_types pt
where
	i.USER_ID = u.ID
    and i.PRODUCT_ID = p.id
    and p.PRODUCT_TYPE_ID = pt.ID
	and i.INVESTMENT_STATUS_ID in (2,3)
    and u.CLASS_ID <> 1
    and (CAST(`i`.`TIME_CREATED` AS DATE) = CURDATE())
group by
	p.PRODUCT_TYPE_ID;