CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `anycapital`@`%` 
    SQL SECURITY DEFINER
VIEW `analytics_user` AS
    SELECT 
        COUNT(`u`.`ID`) AS `count_users`
    FROM
        `users` `u`
    WHERE
        ((CAST(`u`.`TIME_CREATED` AS DATE) = CURDATE())
            AND (`u`.`CLASS_ID` <> 1))