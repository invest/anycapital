USE `anycapital`;
CREATE 
     OR REPLACE ALGORITHM = UNDEFINED 
    DEFINER = `anycapital`@`%` 
    SQL SECURITY DEFINER
VIEW `analytics_deposit` AS
    SELECT 
        COUNT((CASE
            WHEN
                ((`ma`.`ID` IS NOT NULL))
            THEN
                1
            ELSE NULL
        END)) AS `ftd_count`,
        ROUND((SUM((CASE
                    WHEN
                        ((`ma`.`ID` IS NOT NULL))
                    THEN
                        `t`.`AMOUNT`
                    ELSE 0
                END)) / 100),
                2) AS `ftd_sum`,
        COUNT(t.id) AS `desposites_count`,
        ROUND((SUM((`t`.`AMOUNT`)) / 100),
                2) AS `desposites_sum`
    FROM
        (`users` `u`
        JOIN (`transactions` `t`
        LEFT JOIN `marketing_attribution` `ma` ON (((`ma`.`REFERENCE_ID` = `t`.`ID`)
            AND (`ma`.`TABLE_ID` = 2)))))
    WHERE
        ((CAST(`t`.`TIME_CREATED` AS DATE) = CURDATE())
            AND (`t`.`TRANSACTION_PAYMENT_TYPE_ID` = 1)
            AND (`t`.`TRANSACTION_OPERATION_ID` = 1)
            AND (`u`.`ID` = `t`.`USER_ID`)
            AND (`u`.`CLASS_ID` <> 1));
