USE `anycapital`;
CREATE 
     OR REPLACE ALGORITHM = UNDEFINED 
    DEFINER = `anycapital`@`%` 
    SQL SECURITY DEFINER
VIEW `analytics_login` AS
    SELECT 
        COUNT(`l`.`ID`) AS `count_logins`
    FROM
        (`logins` `l`
        JOIN `users` `u`)
    WHERE
        ((`u`.`ID` = `l`.`USER_ID`)
            AND (`u`.`CLASS_ID` <> 1)
            AND (`l`.`TIME_CREATED` > (NOW() - INTERVAL 30 MINUTE)));