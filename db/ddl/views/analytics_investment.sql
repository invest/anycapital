CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `anycapital`@`%` 
    SQL SECURITY DEFINER
VIEW `analytics_investment` AS
    SELECT 
        COUNT(`i`.`ID`) AS `investment_count`,
        ROUND((SUM(`i`.`AMOUNT`) / 100), 2) AS `investment_sum`
    FROM
        (`investments` `i`
        JOIN `users` `u`)
    WHERE
        ((`i`.`USER_ID` = `u`.`ID`)
            AND (`i`.`INVESTMENT_STATUS_ID` IN (2 , 3))
            AND (`u`.`CLASS_ID` <> 1)
            AND (CAST(`i`.`TIME_CREATED` AS DATE) = CURDATE()))