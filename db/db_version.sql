-- db_version												|
--  	                                   _ _        _ 	|
--		                                  (_) |      | |	|
--		  __ _ _ __  _   _  ___ __ _ _ __  _| |_ __ _| |	|
--		 / _` | '_ \| | | |/ __/ _` | '_ \| | __/ _` | |	|
--		| (_| | | | | |_| | (_| (_| | |_) | | || (_| | |	|
--		 \__,_|_| |_|\__, |\___\__,_| .__/|_|\__\__,_|_|	|
--		              __/ |         | |                 	|
--		             |___/          |_|						|
-- 															|
-------------------------------------------------------------

-------------------------------------------------------------
-- product language
-- Eyal O
-------------------------------------------------------------

CREATE TABLE `anycapital`.`product_languages` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `product_id` BIGINT(20) NOT NULL,
  `language_id` SMALLINT(6) NOT NULL,
  `time_created` VARCHAR(45) NOT NULL DEFAULT 'CURRENT_TIMESTAMP(3)',
  PRIMARY KEY (`id`),
  INDEX `fk_product_language_p_idx` (`product_id` ASC),
  INDEX `fk_product_language_l_idx` (`language_id` ASC),
  CONSTRAINT `fk_product_language_p`
    FOREIGN KEY (`product_id`)
    REFERENCES `anycapital`.`products` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_language_l`
    FOREIGN KEY (`language_id`)
    REFERENCES `anycapital`.`languages` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
ALTER TABLE `anycapital`.`product_languages` 
CHANGE COLUMN `time_created` `time_created` TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ;

ALTER TABLE `anycapital`.`product_languages` 
RENAME TO  `anycapital`.`product_kid_languages` ;



-------------------------------------------------------------
-- End.
-- product language
-- Eyal O
-------------------------------------------------------------