package capital.any.service.base.qmUserAnswer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.qmUserAnswer.IQmUserAnswerDao;
import capital.any.model.base.QmQuestion;
import capital.any.model.base.QmUserAnswer;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service
public class QmUserAnswerService implements IQmUserAnswerService {
	private static final Logger logger = LoggerFactory.getLogger(QmUserAnswerService.class);

	@Autowired
	private IQmUserAnswerDao qmUserAnswerDao;
	
	@Override
	public void insertBatch(List<QmUserAnswer> qmUserAnswerList) {
		logger.debug("QmUserAnswerService.insertBatch - START");
		qmUserAnswerDao.insertBatch(qmUserAnswerList);
		logger.debug("QmUserAnswerService.insertBatch - END");
	}
	
	@Override
	public Map<Integer, QmQuestion> get(long userId) {
		return qmUserAnswerDao.get(userId);
	}
	
	public long getSumScoreByAnswers(List<Integer> answers) {
		return qmUserAnswerDao.getSumScoreByAnswers(answers);
	}
	
	@Override
	public long getSumScore(List<QmUserAnswer> qmUserAnswerList) {
		List<Integer> answers = new ArrayList<Integer>();
		for (QmUserAnswer qmUserAnswer : qmUserAnswerList) {
			answers.add(qmUserAnswer.getAnswerId());
		}
		return getSumScoreByAnswers(answers);
	}
}
