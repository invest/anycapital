package capital.any.hazelcast.update.base.product;

import java.math.BigDecimal;
import capital.any.base.enums.ProductStatusEnum;
import capital.any.hazelcast.update.base.UpdateEntry;
import capital.any.model.base.MarketPrice;
import capital.any.model.base.Product;
import capital.any.model.base.ProductMarket;

/**
 * @author LioR SoLoMoN
 *
 */
public class UpdateProductMarketPriceEntry implements UpdateEntry {
	private Product product;
	private MarketPrice marketPrice;
	
	public UpdateProductMarketPriceEntry(MarketPrice marketPrice) {
		this.marketPrice = marketPrice;
	}

	@Override
	public Product update(Object data) {
		Product product = (Product)data;
		for (ProductMarket productMarket : product.getProductMarkets()) {
			if (productMarket.getMarket().getId() == marketPrice.getMarketId()) {
				productMarket.getMarket().getLastPrice().setPrice(marketPrice.getPrice());
				break;
			}
		}
		product.calculateScenarios();
		if (product.getProductBarrier() != null) {
			if (ProductStatusEnum.SUBSCRIPTION.getId() < product.getProductStatus().getId()) {
				product.getProductBarrier().calculateDistanceToBarrier(new BigDecimal(String.valueOf(product.getProductMarkets().get(0).getMarket().getLastPrice().getPrice())), 
																				new BigDecimal(String.valueOf(product.getProductMarkets().get(0).getStartTradeLevel())));
			}
		}
		this.product = product;
		return product;	
	}

	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}
}
