package capital.any.base.enums;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author LioR SoLoMoN
 *
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ActionSource {
	WEB(1), CRM(2), JOB(3);
	
	private static final Logger logger = LoggerFactory.getLogger(ActionSource.class);
	private static final Map<Integer, ActionSource> ID_ACTION_SOURCE_MAP = new HashMap<Integer, ActionSource>(ActionSource.values().length);
	static {
		for (ActionSource g : ActionSource.values()) {
			ID_ACTION_SOURCE_MAP.put(g.getId(), g);
		}
	}

	private int id;
	
	public static ActionSource get(int id) {
		ActionSource actionSource = ID_ACTION_SOURCE_MAP.get(id);
		if (actionSource == null) {
			throw new IllegalArgumentException("No ActionSource with id: " + id);
		} 
		return actionSource;
	}
	
	/**
	 * @return id of the actionSource. null if not found.
	 */
	@JsonValue
    public Integer toValue() {
		for (Entry<Integer, ActionSource> entry : ID_ACTION_SOURCE_MAP.entrySet()) {
            if (entry.getValue() == this)
                return entry.getKey();
        }
        return null; 
    }
	
	
	/**
	 * @param id
	 * @return ActionSource by a given id
	 */
	@JsonCreator 
    public static ActionSource get(String id) {
		ActionSource actionSource = null;
		try {
			actionSource = ID_ACTION_SOURCE_MAP.get(Integer.valueOf(id));
	        if (actionSource == null) {
	            throw new IllegalArgumentException(id + " has no corresponding value for ActionSource");
	        }
		} catch (NumberFormatException ne) {
			logger.error("ERROR! ActionSource; send a number!");
			throw new IllegalArgumentException(id + " has no corresponding value for ActionSource");
		}
		
        return actionSource;
    }
	
	/**
	 * @param id
	 */
	private ActionSource(int id) {
		this.setId(id);
	}
	
	/**
	 * @return name
	 */
	public String getName() {
		return this.name();
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
}
