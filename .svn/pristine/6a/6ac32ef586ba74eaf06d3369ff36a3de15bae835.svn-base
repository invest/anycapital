package capital.any.hazelcast.processor.base.product;

import java.util.Map.Entry;
import java.util.function.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.hazelcast.map.EntryBackupProcessor;
import com.hazelcast.map.EntryProcessor;
import capital.any.hazelcast.processor.base.CopyValueToBackupEntryBackupProcessor;
import capital.any.model.base.Product;
import capital.any.model.base.ProductCoupon;

/**
 * @author Eyal Goren
 *
 */
public class ProductCouponIsPaidEntryProcessor implements EntryProcessor<Long, Product> {
	
	private static final long serialVersionUID = -5935128287511743729L;
	private static final Logger logger = LoggerFactory.getLogger(ProductCouponIsPaidEntryProcessor.class);
	private Product product;
	private ProductCoupon productCoupon;

	public ProductCouponIsPaidEntryProcessor(ProductCoupon productCoupon) {
		this.productCoupon = productCoupon;
	}

	@Override
	public Product process(Entry<Long, Product> entry) {
		Product product = entry.getValue();
		logger.debug("start ProductCouponIsPaidEntryProcessor process; product: " + product.getId());
		Predicate<ProductCoupon> predicate = c-> c.getId() == productCoupon.getId();
		ProductCoupon productCoupon = product.getProductCoupons().stream().filter(predicate).findFirst().get();
		productCoupon.setPaid(productCoupon.isPaid());
		this.product = product;
		entry.setValue(product);
		logger.debug("End ProductCouponIsPaidEntryProcessor process; product: " + product.getId());
		return product;
	}

	@Override
	public EntryBackupProcessor<Long, Product> getBackupProcessor() {
		logger.debug("ProductCouponIsPaidEntryProcessor; getBackupProcessor; " + this.product.getId()); 
		return new CopyValueToBackupEntryBackupProcessor<Product>(this.product);
	}	
}