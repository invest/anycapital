package capital.any.model.base;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.validation.constraints.Min;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import capital.any.base.enums.ProductTypeEnum;

/**
 * @author eranl
 *
 */
public class ProductCoupon implements Serializable {
	// empty interface for validation
	public interface UpdateObservationLevelValidatorGroup{};
	
	private static final Logger logger = LoggerFactory.getLogger(ProductCoupon.class);
	private static final long serialVersionUID = -419674858732270147L;
	
	@Min(groups = UpdateObservationLevelValidatorGroup.class, message ="error.emty", value = 1)
	private int id;
	@Min(groups = UpdateObservationLevelValidatorGroup.class, message ="error.emty", value = 1)
	private long productId;
	private Date observationDate;
	private Date paymentDate;
	private double triggerLevel;
	private double payRate;
	private boolean isPaid;
	@Min(groups = UpdateObservationLevelValidatorGroup.class, message ="error.emty", value = 1)
	private double observationLevel;
		
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	public long getProductId() {
		return productId;
	}
	
	public void setProductId(long productId) {
		this.productId = productId;
	}
	
	/**
	 * @return the observationDate
	 */
	public Date getObservationDate() {
		return observationDate;
	}
	/**
	 * @param observationDate the observationDate to set
	 */
	public void setObservationDate(Date observationDate) {
		this.observationDate = observationDate;
	}
	/**
	 * @return the paymentDate
	 */
	public Date getPaymentDate() {
		return paymentDate;
	}
	/**
	 * @param paymentDate the paymentDate to set
	 */
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}
	/**
	 * @return the triggerLevel
	 */
	public double getTriggerLevel() {
		return triggerLevel;
	}
	/**
	 * @param triggerLevel the triggerLevel to set
	 */
	public void setTriggerLevel(double triggerLevel) {
		this.triggerLevel = triggerLevel;
	}
	/**
	 * @return the payRate
	 */
	public double getPayRate() {
		return payRate;
	}
	/**
	 * @param payRate the payRate to set
	 */
	public void setPayRate(double payRate) {
		this.payRate = payRate;
	}
	/**
	 * @return the isPaid
	 */
	public boolean isPaid() {
		return isPaid;
	}
	/**
	 * @param isPaid the isPaid to set
	 */
	public void setPaid(boolean isPaid) {
		this.isPaid = isPaid;
	}
	
	/**
	 * @return the observationLevel
	 */
	public double getObservationLevel() {
		return observationLevel;
	}

	/**
	 * @param observationLevel the observationLevel to set
	 */
	public void setObservationLevel(double observationLevel) {
		this.observationLevel = observationLevel;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ProductCoupon [id=" + id + ", productId=" + productId + ", observationDate=" + observationDate
				+ ", paymentDate=" + paymentDate + ", triggerLevel=" + triggerLevel + ", payRate=" + payRate
				+ ", isPaid=" + isPaid + ", observationLevel=" + observationLevel + "]";
	}
	
	public double getCouponValue(double finalFixingLevel, BigDecimal initialFixingLevel, Date issueDate, DayCountConvention dayCountConvention, int productTypeId) {
		BigDecimal hundred = new BigDecimal("100");
		logger.info("final fixing level = " + finalFixingLevel + " initial fixing level = " + initialFixingLevel.doubleValue() + " issueDate = " + issueDate.toString() + " dayCountConvention " + dayCountConvention);
		logger.info(toString());
		logger.info("condition = " + finalFixingLevel + " > " + initialFixingLevel.multiply((new BigDecimal(String.valueOf(triggerLevel)).divide(hundred))).doubleValue());
		if (finalFixingLevel > initialFixingLevel.multiply((new BigDecimal(String.valueOf(triggerLevel)).divide(hundred))).doubleValue()) {
			logger.info("got coupon value = " + new BigDecimal(String.valueOf(payRate)).divide(hundred).doubleValue());
			double days = 1;
			if (productTypeId != ProductTypeEnum.CAPITAL_PROTECTION_CERTIFICATE_WITH_COUPON.getId()) {
				days = dayCountConvention.getDayCountConvintionDaycountFraction(issueDate, paymentDate);
				logger.info("number of days " + days);
			}
			return new BigDecimal(String.valueOf(payRate)).divide(hundred).multiply(new BigDecimal(String.valueOf(days))).doubleValue();
		}
		return 0;
	}
	
}
