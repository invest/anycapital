package capital.any.dao.base.qmUserAnswer;

import java.util.List;
import java.util.Map;

import capital.any.model.base.QmQuestion;
import capital.any.model.base.QmUserAnswer;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IQmUserAnswerDao {
	public static final String INSERT_QM_USER_ANSWER =
			"  INSERT " +
		    "  INTO qm_user_answers " +
			"  ( " +
		    "     user_id, " +
		    "     question_id, " +
		    " 	  answer_id, " +
		    "     text_answer, " +
		    "     action_source_id " +
		    "  ) " +
		    "  VALUES " +
		    "  ( " +
		    " 	  :userId, " +
		    " 	  :questionId, " +
		    " 	  :answerId, " +
		    " 	  :textAnswer, " +
		    " 	  :actionSourceId " +
		    "  ) ";
	
	public static final String GET =
			" 	SELECT " + 
			" 		ua.id as ua_id, " +
			"		ua.user_id as ua_user_id, " +
			"		ua.question_id as ua_question_id, " +
			"		ua.answer_id as ua_answer_id, " +
			" 		ua.text_answer as ua_text_answer, " +
			"		ua.action_source_id as ua_action_source_id, " +
			"		ua.time_created as ua_time_created, " +
			"		q.id as q_id, " +
			"		q.name as q_name, " +
			"		q.display_name as q_display_name, " +
			"		q.is_active as q_is_active, " +
			"		q.order_id as q_order_id, " +
			"		q.question_type_id as q_question_type_id, " +
			"		a.*, " +
			"		qg.id as qg_id, " +
			"		qg.name as qg_name, " +
			"		qg.display_name as qg_display_name, " +
			"		(case when a.id = ua.answer_id then 1 else 0 end) as selected_answer " +
			"	FROM  " +
			"		qm_questions q LEFT JOIN qm_user_answers ua ON ua.question_id = q.id," +
			"		qm_question_groups qg, " +
			"		qm_answers a " +
			"	where " +
			"		q.id = a.question_id " +
			"		and q.question_group_id = qg.id " +
			" 		and q.is_active = 1 " +			
			" 		and ua.user_id = :userId ";
	
	public static final String GET_SUM_SCORE_BY_ANSWERS = 
			" 	SELECT " + 
			"		sum(a.score) as sum_score" +
			"	FROM " + 
			"	 	qm_answers a " +
			"	WHERE " +
			"		a.id in (:answerIds) ";

	void insertBatch(List<QmUserAnswer> qmUserAnswerList);

	Map<Integer, QmQuestion> get(long userId);
	
	long getSumScoreByAnswers(List<Integer> answers);
}
