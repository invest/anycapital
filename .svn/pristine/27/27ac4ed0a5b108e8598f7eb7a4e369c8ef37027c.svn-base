package capital.any.service.base.anyoption.api;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.concurrent.Future;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import capital.any.model.base.HttpRequest;
import capital.any.model.base.User;
import capital.any.model.base.UserHistory;
import capital.any.model.base.anyoption.AnyoptionAPIRequest;
import capital.any.model.base.anyoption.AnyoptionAPIResponse;
import capital.any.model.base.anyoption.LastCurrencyRate;
import capital.any.model.base.anyoption.ResponseLogin;
import capital.any.model.base.anyoption.UserExtraFields;
import capital.any.service.IHttpClientService;
import capital.any.service.base.user.IUserService;

/**
 * @author LioR SoLoMoN
 *
 */
@Service
public class AnyoptionAPIService implements  IAnyoptionAPIService {
	private static final Logger logger = LoggerFactory.getLogger(AnyoptionAPIService.class);
	private static final String SERVICE_GET_USER = "getUser";
	private static final String SERVICE_GET_CAPITAL_USER_EXTRA_FIELDS = "getCapitalUserExtraFields";
	private static final String SERVICE_CURRENCY_RATE = "getLastCurrenciesRate";
	@Autowired
	private IHttpClientService httpClientService;
	@Autowired
	private IUserService userService;
	@Value("${api.gateway.url}")
	private String SERVICE_URL;

	/**
	 * @param loginInput
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ResponseLogin getAnyoptionUser(AnyoptionAPIRequest data) throws Exception {
		HttpRequest request = new HttpRequest<>(SERVICE_URL + SERVICE_GET_USER, 
				data, 
				ResponseLogin.class);
		ResponseLogin response = (ResponseLogin)httpClientService.doPost(request);
		return response;
	}
	
	/**
	 * @param loginInput
	 * @return
	 * @throws Exception
	 */
	/*@SuppressWarnings({ "unchecked", "rawtypes" })
	public AnyoptionAPIResponse<User> getAnyoptionUser(AnyoptionAPIRequest data) throws Exception {
		HttpRequest request = new HttpRequest<>(SERVICE_URL + SERVICE_GET_USER, 
				data, 
				new AnyoptionAPIResponse<User>().getClass());
		AnyoptionAPIResponse<User> response = (AnyoptionAPIResponse<User>)httpClientService.doPost(request);	
		return response;
	}*/
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Async("taskExecutor")
	public Future<AnyoptionAPIResponse<UserExtraFields>> getAnyoptionUserExtraFields(User user, UserHistory userHistory) {
		AnyoptionAPIResponse<UserExtraFields> response = null;
		try {
	        logger.info("START getAnyoptionUserExtraFields; Looking up " + user.getAoUserId());
	        logger.info("Execute method asynchronously. " + Thread.currentThread().getName());
	        AnyoptionAPIRequest data = new AnyoptionAPIRequest(String.valueOf(user.getEmail()), "");
			HttpRequest httpRequest = new HttpRequest<>(SERVICE_URL + SERVICE_GET_CAPITAL_USER_EXTRA_FIELDS,
					data, 
					AnyoptionAPIResponse.class);        	        
			response = (AnyoptionAPIResponse<UserExtraFields>) httpClientService.doPost(httpRequest);
			if (response != null && response.getApiCode().equals("G000")) {//FIXME & all the lines below
				logger.info("response data: " + response.getData());
				LinkedHashMap responseData = ((LinkedHashMap)response.getData());  
				user.setRegulated(Boolean.valueOf(responseData.get("isRegulated").toString()));
				userService.update(user, userHistory);
			}
		} catch (Exception e) {
			logger.error("ERROR! getAnyoptionUserExtraFields; userId: " + user.getId(), e);
		}
        return new AsyncResult<>(response);
    }

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public AnyoptionAPIResponse<ArrayList<LastCurrencyRate>> getCurrencyRate() throws Exception {
		AnyoptionAPIResponse<ArrayList<LastCurrencyRate>> response = null;
		AnyoptionAPIRequest data = new AnyoptionAPIRequest();
		HttpRequest httpRequest = new HttpRequest<>(SERVICE_URL + SERVICE_CURRENCY_RATE, 
				data, 
				AnyoptionAPIResponse.class);		
		response = (AnyoptionAPIResponse<ArrayList<LastCurrencyRate>>) httpClientService.doPost(httpRequest);	
		return response;
	}
}
