package capital.any.dao.base.qmUserAnswer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Repository;

import capital.any.model.base.QmQuestion;
import capital.any.model.base.QmUserAnswer;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository
public class QmUserAnswerDao implements IQmUserAnswerDao, ApplicationContextAware {
	
	private ApplicationContext appContext;
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	@Override
	public void insertBatch(List<QmUserAnswer> qmUserAnswerList) {
		SqlParameterSource[] batch = 
				SqlParameterSourceUtils.createBatch(qmUserAnswerList.toArray());
		jdbcTemplate.batchUpdate(INSERT_QM_USER_ANSWER, batch);
	}
	
	@Override
	public Map<Integer, QmQuestion> get(long userId) {
		QmUserAnswerCallbackHandler callbackHandler = appContext.getBean(QmUserAnswerCallbackHandler.class);
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("userId", userId);
		jdbcTemplate.query(GET, params, callbackHandler);
		return callbackHandler.getResult();
	}

	@Override
	public long getSumScoreByAnswers(List<Integer> answers) {
		MapSqlParameterSource parameters =	new MapSqlParameterSource();
		parameters.addValue("answerIds", answers);
		return jdbcTemplate.query(GET_SUM_SCORE_BY_ANSWERS, parameters, new ResultSetExtractor<Long>() {
			@Override
			public Long extractData(ResultSet rs) throws SQLException  {
				long score = 0;
				if(rs.next()){
					score = rs.getInt("sum_score");
				}
				return score;
			}
		});
	}
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.appContext = applicationContext;
	}
}
