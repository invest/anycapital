package capital.any.hazelcast.processor.base.product;

import java.math.BigDecimal;
import java.util.Map.Entry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.hazelcast.map.EntryBackupProcessor;
import com.hazelcast.map.EntryProcessor;
import capital.any.base.enums.ProductStatusEnum;
import capital.any.hazelcast.processor.base.CopyValueToBackupEntryBackupProcessor;
import capital.any.model.base.MarketPrice;
import capital.any.model.base.Product;
import capital.any.model.base.ProductMarket;

/**
 * 
 * @author eyal.goren
 * update the product market last price and recalculate the scenario
 */
public class ProductMarketPriceEntryProcessor implements EntryProcessor<Long, Product> {

	private static final long serialVersionUID = -8914784693198349486L;
	private static final Logger logger = LoggerFactory.getLogger(ProductMarketPriceEntryProcessor.class);
	private Product product;
	private MarketPrice marketPrice;
	

	public ProductMarketPriceEntryProcessor(MarketPrice marketPrice) {
		this.marketPrice = marketPrice;
	}

	@Override
	public Product process(Entry<Long, Product> entry) {
		Product product = entry.getValue();
		logger.debug("start ProductMarketPriceEntryProcessor process; Product: " + product.getId());
		for (ProductMarket productMarket : product.getProductMarkets()) {
			if (productMarket.getMarket().getId() == marketPrice.getMarketId()) {
				logger.info("update last price for market id " + productMarket.getMarket().getId() );
				productMarket.getMarket().getLastPrice().setPrice(marketPrice.getPrice());
				break;
			}
		}
		product.calculateScenarios();
		if (product.getProductBarrier() != null) {
			if (ProductStatusEnum.SUBSCRIPTION.getId() < product.getProductStatus().getId()) {
				product.getProductBarrier().calculateDistanceToBarrier(new BigDecimal(String.valueOf(product.getProductMarkets().get(0).getMarket().getLastPrice().getPrice())), 
																				new BigDecimal(String.valueOf(product.getProductMarkets().get(0).getStartTradeLevel())));
			}
		}
		this.product = product;
		entry.setValue(product);		
		logger.debug("End ProductMarketPriceEntryProcessor process; Product: " + product.getId());
		return product;
	}

	@Override
	public EntryBackupProcessor<Long, Product> getBackupProcessor() {
		logger.debug("ProductMarketPriceEntryProcessor; getBackupProcessor; " + this.product.getId()); 
		return new CopyValueToBackupEntryBackupProcessor<Product>(this.product); 
	}	
}