package capital.any;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import capital.any.model.base.RegulationUserInfo;
import capital.any.model.base.User;
import capital.any.dao.base.writer.BaseWriterFactory;
import capital.any.model.base.AbstractWriter;
import capital.any.model.base.Country;
import capital.any.model.base.CountryRisk;
import capital.any.model.base.RegulationQuestionnaireStatus;
import capital.any.service.base.regulationUserInfo.IRegulationUserInfoService;
import capital.any.service.base.user.IUserService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class RegulationUserInfoServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(RegulationUserInfoServiceTest.class);
	
	@Autowired
	private IRegulationUserInfoService regulationUserInfoService;
	@Autowired
	private BaseWriterFactory writerFactory;
	@Autowired
	private IUserService userService;
	
	@Value("${user.regulated.no.money}")
	private String userRegulatedNoMoney;
	@Value("${user.exist}")
	private String userExist;
	
	private User user;
	
	@Before
	public void init() {
		user = userService.getUserByEmail(userExist);	
	}
	
	@Test
	@Transactional
	public void insert() {
		logger.info("About to test insert into regulationUserInfoService");
		RegulationUserInfo regulationUserInfo = new RegulationUserInfo();
		regulationUserInfo.setUserId(user.getId());
		regulationUserInfo.setRegulationQuestionnaireStatus(new RegulationQuestionnaireStatus(1));
		try {
			regulationUserInfoService.insert(regulationUserInfo);
		} catch (Exception e) {
			logger.error("Problem with insert into regulationUserInfoService. ", e);
		}
	}
	
	@Test
	@Transactional
	public void update() {
		logger.info("About to test update regulationUserInfoService");
		AbstractWriter writer = writerFactory.createWriter(6);
		writer.setId(6);
		RegulationUserInfo regulationUserInfo = new RegulationUserInfo();
		regulationUserInfo.setUserId(user.getId());
		regulationUserInfo.setCountryRisk(new CountryRisk(1));
		regulationUserInfo.setRegulationQuestionnaireStatus(new RegulationQuestionnaireStatus(2));
		regulationUserInfo.setWriter(writer);
		try {
			regulationUserInfoService.update(regulationUserInfo);
		} catch (Exception e) {
			logger.error("Problem with update regulationUserInfoService. ", e);
		}
	}
	
	@Test
	public void get() {
		logger.info("About to test get in RegulationUserInfoService");
		User user = userService.getUserByEmail(userRegulatedNoMoney);
		Assert.assertNotNull(regulationUserInfoService.get(user.getId()));
	}
}
