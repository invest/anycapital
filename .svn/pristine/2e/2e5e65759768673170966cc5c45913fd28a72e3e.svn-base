package capital.any.model.base;

import java.io.Serializable;
import java.util.Map;

/**
 * @author eran.levy
 *
 */
public class HttpRequest<T> implements Serializable {

	private static final long serialVersionUID = -3326002617711976647L;
	
	private String url;
	private Object data; 
	private Class<T> clazz;
	private Map<String, String> headers;
	private boolean json;
	
	/**
	 * 
	 */
	public HttpRequest() {
		
	}	
	/**
	 * @param url
	 * @param data
	 * @param clazz
	 */
	public HttpRequest(String url, Object data, Class<T> clazz) {
		this.url = url;
		this.data = data;
		this.clazz = clazz;
	}
	/**
	 * @param url
	 * @param data
	 * @param clazz
	 * @param headers
	 */
	public HttpRequest(String url, Object data, Class<T> clazz, Map<String, String> headers) {
		this.url = url;
		this.data = data;
		this.clazz = clazz;
		this.headers = headers;
	}	
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * @return the data
	 */
	public Object getData() {
		return data;
	}
	/**
	 * @param data the data to set
	 */
	public void setData(Object data) {
		this.data = data;
	}
	/**
	 * @return the clazz
	 */
	public Class<T> getClazz() {
		return clazz;
	}
	/**
	 * @param clazz the clazz to set
	 */
	public void setClazz(Class<T> clazz) {
		this.clazz = clazz;
	}
	/**
	 * @return the headers
	 */
	public Map<String, String> getHeaders() {
		return headers;
	}
	/**
	 * @param headers the headers to set
	 */
	public void setHeaders(Map<String, String> headers) {
		this.headers = headers;
	}
	/**
	 * @return the json
	 */
	public boolean isJson() {
		return json;
	}
	/**
	 * @param json the json to set
	 */
	public void setJson(boolean json) {
		this.json = json;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "HttpRequest [url=" + url + ", data=" + data + ", clazz=" + clazz + ", headers=" + headers + ", json="
				+ json + "]";
	}
		
}
